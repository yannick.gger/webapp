# Change log

## 22.05.10.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1375
* Fix Engagement view by @hoki0713 in https://github.com/collabsapp/webapp/pull/1377
* #1300 Integrated inbox - Campaign manager view by @KimBjurling in https://github.com/collabsapp/webapp/pull/1376
* Bugfix - Integrated inbox by @KimBjurling in https://github.com/collabsapp/webapp/pull/1378
* Traffic api and view by @hoki0713 in https://github.com/collabsapp/webapp/pull/1379
* Integrated Inbox - Change conversation list, refactor, scroll behavior by @KimBjurling in https://github.com/collabsapp/webapp/pull/1382
* Updated to camelCase by @KimBjurling in https://github.com/collabsapp/webapp/pull/1385
* Fix gender age country endpoint by @hoki0713 in https://github.com/collabsapp/webapp/pull/1381
* Initial attempt to deploy Staging to AWS by @jonathan-franzen in https://github.com/collabsapp/webapp/pull/1388
* Run on ubuntu 20.04 by @Nyholm in https://github.com/collabsapp/webapp/pull/1392
* Replace envars when deploying to AWS staging by @Nyholm in https://github.com/collabsapp/webapp/pull/1396
* Remove environment vars from serverless.yaml by @Nyholm in https://github.com/collabsapp/webapp/pull/1395
* Bugfix for reports and bugfix for latestMessages in Integrated inbox by @KimBjurling in https://github.com/collabsapp/webapp/pull/1391
* Fix audience view by @hoki0713 in https://github.com/collabsapp/webapp/pull/1394

## New Contributors
* @jonathan-franzen made their first contribution in https://github.com/collabsapp/webapp/pull/1388

**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.05.05.1...22.05.10.1

## 22.05.05.1

* Fix/data library summary by @hoki0713 in https://github.com/collabsapp/webapp/pull/1370
* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1367
* Fix summary component fetching data function by @hoki0713 in https://github.com/collabsapp/webapp/pull/1374


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.05.04.1...22.05.05.1

## 22.05.04.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1354
* Slow summary chart rendering by @hoki0713 in https://github.com/collabsapp/webapp/pull/1356


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.05.02...22.05.04.1

## 22.05.02.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1344
* Change main dashboard by @hoki0713 in https://github.com/collabsapp/webapp/pull/1345
* WIP Change main dashboard by @hoki0713 in https://github.com/collabsapp/webapp/pull/1347
* Apply dashboard period into chart Item by @hoki0713 in https://github.com/collabsapp/webapp/pull/1349
* Impression view by @hoki0713 in https://github.com/collabsapp/webapp/pull/1350
* Fix shrunken chart items by @hoki0713 in https://github.com/collabsapp/webapp/pull/1351
* Staging feature flag data library by @hoki0713 in https://github.com/collabsapp/webapp/pull/1352
* Bugfix for reports when updating stats by @KimBjurling in https://github.com/collabsapp/webapp/pull/1353


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.27.2...22.05.02

## 22.04.27.2

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1340
* Rename the dashboard function by @hoki0713 in https://github.com/collabsapp/webapp/pull/1339
* Sale permission update by @KimBjurling in https://github.com/collabsapp/webapp/pull/1343


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.27.1...22.04.27.2

## 22.04.27.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1328
* Add modal to save chart in dashboard by @hoki0713 in https://github.com/collabsapp/webapp/pull/1329
* Create new dashboard without chart item by @hoki0713 in https://github.com/collabsapp/webapp/pull/1331
* Remove Chart item from dashboard by @hoki0713 in https://github.com/collabsapp/webapp/pull/1333
* Delete dashboard by @hoki0713 in https://github.com/collabsapp/webapp/pull/1335
* Allow sales users to add/remove influencer in active campagins by @KimBjurling in https://github.com/collabsapp/webapp/pull/1338


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.25.2...22.04.27.1

## 22.04.25.2

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1323
* #1326 adjustments for sales by @KimBjurling in https://github.com/collabsapp/webapp/pull/1327


**Full Changelog**: https://github.com/collabsapp/webapp/compare/25.04.22.1...22.04.25.2

## 22.04.25.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1316
* Fetch dashboards by @hoki0713 in https://github.com/collabsapp/webapp/pull/1311
* Changed permissions for Sale users by @KimBjurling in https://github.com/collabsapp/webapp/pull/1321
* Fixed prettier errors by @KimBjurling in https://github.com/collabsapp/webapp/pull/1322


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.22.2...25.04.22.1

## 22.04.22.2

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1315
* #1309 add x switch user header by @hoki0713 in https://github.com/collabsapp/webapp/pull/1313


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.22.1...22.04.22.2

## 22.04.22.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1307
* Updated invoice text by @KimBjurling in https://github.com/collabsapp/webapp/pull/1314


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.20.2...22.04.22.1

## 22.04.20.2

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1292
* Bugfix for tiktok for RequestScreenshots by @KimBjurling in https://github.com/collabsapp/webapp/pull/1294


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.20.1...22.04.20.2

## 22.04.20.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1284
* Bugfix for overflow hidden in the influencer campagin list view by @KimBjurling in https://github.com/collabsapp/webapp/pull/1286
* Bugfix RequestScreenshots in stories by @KimBjurling in https://github.com/collabsapp/webapp/pull/1288


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.19.2...22.04.20.1

## 22.04.19.2

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1282
* Enable Facebook Auth in production by @KimBjurling in https://github.com/collabsapp/webapp/pull/1283


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.19.1...22.04.19.2

## 22.04.19.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1226
* #1221 generic card component by @hoki0713 in https://github.com/collabsapp/webapp/pull/1227
* #1216 Facebook API error handling and fetch button by @KimBjurling in https://github.com/collabsapp/webapp/pull/1228
* #1229 Hide screenshot upload when it's not needed by @KimBjurling in https://github.com/collabsapp/webapp/pull/1231
* #1170 Updated readme by @KimBjurling in https://github.com/collabsapp/webapp/pull/1232
* Add ChartCard by @hoki0713 in https://github.com/collabsapp/webapp/pull/1233
* Add Data library Influencer Performance list by @hoki0713 in https://github.com/collabsapp/webapp/pull/1234
* #1100 Handle referral url by @KimBjurling in https://github.com/collabsapp/webapp/pull/1235
* Implement gender ratio design by @hoki0713 in https://github.com/collabsapp/webapp/pull/1237
* Updated scopes by @KimBjurling in https://github.com/collabsapp/webapp/pull/1242
* #1100 Grid system by @KimBjurling in https://github.com/collabsapp/webapp/pull/1240
* Add Influencer service by @hoki0713 in https://github.com/collabsapp/webapp/pull/1241
* Fetch username and propfile, removed scope by @KimBjurling in https://github.com/collabsapp/webapp/pull/1248
* Bugfix for loading username by @KimBjurling in https://github.com/collabsapp/webapp/pull/1251
* Fix Reach view by @hoki0713 in https://github.com/collabsapp/webapp/pull/1249
* Implement line graph design by @hoki0713 in https://github.com/collabsapp/webapp/pull/1252
* Connected pages, added scope by @KimBjurling in https://github.com/collabsapp/webapp/pull/1256
* Connected pages on settings page by @KimBjurling in https://github.com/collabsapp/webapp/pull/1258
* #1245 Double scroll by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1257
* Add Reach view summary card by @hoki0713 in https://github.com/collabsapp/webapp/pull/1259
* Updated env files by @KimBjurling in https://github.com/collabsapp/webapp/pull/1260
* Fetch audience data by @hoki0713 in https://github.com/collabsapp/webapp/pull/1263
* Add campaign service by @hoki0713 in https://github.com/collabsapp/webapp/pull/1267
* Campaign overview return object Object by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1265
* Fix apply campaign filter to each card in reach view by @hoki0713 in https://github.com/collabsapp/webapp/pull/1271
* close user menu on click by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1273
* Login button keeps spinning on validation error by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1272
* Add feature flags reach view by @hoki0713 in https://github.com/collabsapp/webapp/pull/1269
* Dont update stripe countries at each deployment by @Nyholm in https://github.com/collabsapp/webapp/pull/1264
* Fixed prettier error by @KimBjurling in https://github.com/collabsapp/webapp/pull/1276
* #1254 facebook rest work by @KimBjurling in https://github.com/collabsapp/webapp/pull/1278
* Reach view chart show 'no data' by @hoki0713 in https://github.com/collabsapp/webapp/pull/1279
* #1280 bugfix to get pages by @KimBjurling in https://github.com/collabsapp/webapp/pull/1281


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.01.2...22.04.19.1

## 22.04.01.2

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1223
* Bugfix in Instagram post form (Reports) by @KimBjurling in https://github.com/collabsapp/webapp/pull/1220
* Fix reset password path by @hoki0713 in https://github.com/collabsapp/webapp/pull/1225


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.04.01.1...22.04.01.2

## 22.04.01.1

* #1117 engagement influencer data by @hoki0713 in https://github.com/collabsapp/webapp/pull/1174
* Upgrade react router dom to v5 by @hoki0713 in https://github.com/collabsapp/webapp/pull/1184
* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1194
* Fetch budget cpm data by @hoki0713 in https://github.com/collabsapp/webapp/pull/1196
* Remove mutation "instagramUserData" to prevent making a instagram http request by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1183
* Local certificate and readme by @KimBjurling in https://github.com/collabsapp/webapp/pull/1206
* Fix flickering number in campaign tab menu by @hoki0713 in https://github.com/collabsapp/webapp/pull/1212
* Fix influencer update email route by @hoki0713 in https://github.com/collabsapp/webapp/pull/1214
* #1161 cannot load campaigns by @hoki0713 in https://github.com/collabsapp/webapp/pull/1215
* Fix/missing route by @hoki0713 in https://github.com/collabsapp/webapp/pull/1218
* #1207 Adjustments for the Facebook review by @KimBjurling in https://github.com/collabsapp/webapp/pull/1216


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.03.30.2...22.04.01.1

## 22.03.30.2

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1182
* Let ghost salers create new campaigns by @KimBjurling in https://github.com/collabsapp/webapp/pull/1187


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.03.30.1...22.03.30.2

## 22.03.30.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1173
* #1163 iframe blocking hot reload by @KimBjurling in https://github.com/collabsapp/webapp/pull/1172
* #1136 Fullstory script in production only by @KimBjurling in https://github.com/collabsapp/webapp/pull/1177
* Removed hotjar script by @KimBjurling in https://github.com/collabsapp/webapp/pull/1178
* Bugfix for blank page and enabled facebook api on staging by @KimBjurling in https://github.com/collabsapp/webapp/pull/1180


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.03.28.15...22.03.30.1

## 22.03.28.15

* Feature flag collabs api token by @KimBjurling in https://github.com/collabsapp/webapp/pull/1167
* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1166
* Bugfix for signUpVisibillity by @KimBjurling in https://github.com/collabsapp/webapp/pull/1169


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.03.28.14...22.03.28.15

## 22.03.28.14

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/1145
* #1143 radar scatter chart by @hoki0713 in https://github.com/collabsapp/webapp/pull/1146
* Run CI on self-hosted runner by @Nyholm in https://github.com/collabsapp/webapp/pull/1147
* #1116 budget data fetching by @hoki0713 in https://github.com/collabsapp/webapp/pull/1148
* Fix fetchPolicy no-cache by @hoki0713 in https://github.com/collabsapp/webapp/pull/1154
* Syntax fix by @Nyholm in https://github.com/collabsapp/webapp/pull/1157
* #1117 engagement data fetching by @hoki0713 in https://github.com/collabsapp/webapp/pull/1153
* Comment Feature Testing Feedback  by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1156
* #1008 Facebook API Authentication with Insights data in reports by @KimBjurling in https://github.com/collabsapp/webapp/pull/1162
* Fix find influencer 404 page by @hoki0713 in https://github.com/collabsapp/webapp/pull/1165


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.03.21.1...22.03.28.14

## 22.03.21.1

* #996 Updated old packages by @KimBjurling in https://github.com/collabsapp/webapp/pull/1139
* Added stripe package by @KimBjurling in https://github.com/collabsapp/webapp/pull/1144
* Fix Data Library by @hoki0713 in https://github.com/collabsapp/webapp/pull/1140


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.03.17.1...22.03.21.1

## Release 22.03.04.1
Remove Swedish translations

## 22.03.03.1

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/985
* Organization error msg bugfix by @KimBjurling in https://github.com/collabsapp/webapp/pull/984
* Func to check if admin or ghost, hide commission and payments for non… by @KimBjurling in https://github.com/collabsapp/webapp/pull/987
* add pagination and refactor query by @KimBjurling in https://github.com/collabsapp/webapp/pull/988
* Add Kim as Codeowner by @Nyholm in https://github.com/collabsapp/webapp/pull/989
* Sytanx fix by @Nyholm in https://github.com/collabsapp/webapp/pull/991
* Remove standard react docs from readme by @Nyholm in https://github.com/collabsapp/webapp/pull/992
* Hide menu items by @KimBjurling in https://github.com/collabsapp/webapp/pull/995
* Change find influencers button route to discover(#895) by @hoki0713 in https://github.com/collabsapp/webapp/pull/998
* Added comment about github workflow by @Nyholm in https://github.com/collabsapp/webapp/pull/993
* Mabel.goldenowl/main/gh 894 by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1013
* fix routing path "recommendations" to "discover" by @hoki0713 in https://github.com/collabsapp/webapp/pull/1016
* Fix/i18next by @hoki0713 in https://github.com/collabsapp/webapp/pull/1017
* #997 testing framework by @KimBjurling in https://github.com/collabsapp/webapp/pull/1021
* Fixed typos by @Nyholm in https://github.com/collabsapp/webapp/pull/1027
* #916 remove rewired by @KimBjurling in https://github.com/collabsapp/webapp/pull/1030
* #916 remove rewired by @KimBjurling in https://github.com/collabsapp/webapp/pull/1032
* Add UserContext by @hoki0713 in https://github.com/collabsapp/webapp/pull/1028
* Added hot reloader by @KimBjurling in https://github.com/collabsapp/webapp/pull/1035
* #1036 Fix bug: import SVG files in handle.js  by @hoki0713 in https://github.com/collabsapp/webapp/pull/1037
* Remove unnecessary user context update function call by @hoki0713 in https://github.com/collabsapp/webapp/pull/1039
* Fix replace require to import svg by @hoki0713 in https://github.com/collabsapp/webapp/pull/1040
* Removed unnecessary refetches, fixed misaligned text in the step 9 in… by @KimBjurling in https://github.com/collabsapp/webapp/pull/1043
* Add Typescript and implement it by @hoki0713 in https://github.com/collabsapp/webapp/pull/1044
* show error creating product by @mabel-golden-owl in https://github.com/collabsapp/webapp/pull/1041
* Fix/broken css by @hoki0713 in https://github.com/collabsapp/webapp/pull/1046
* Remove unnecessary refetching request by @hoki0713 in https://github.com/collabsapp/webapp/pull/1045
* Fix tiktokFragment bug by @hoki0713 in https://github.com/collabsapp/webapp/pull/1048
* Change component to functional and apply tsx by @hoki0713 in https://github.com/collabsapp/webapp/pull/1047
* Antd plugin for craco + config by @KimBjurling in https://github.com/collabsapp/webapp/pull/1050
* #1051 fix campaign card style, member could access to commission in campaign by @hoki0713 in https://github.com/collabsapp/webapp/pull/1053
* Fix/translate by @hoki0713 in https://github.com/collabsapp/webapp/pull/1054
* Removed old parameter by @KimBjurling in https://github.com/collabsapp/webapp/pull/1057
* #1055 new format style by @hoki0713 in https://github.com/collabsapp/webapp/pull/1058
* Fix use t function from i18next by @hoki0713 in https://github.com/collabsapp/webapp/pull/1077
* [CI] Deploy to prod on tags by @Nyholm in https://github.com/collabsapp/webapp/pull/972

## New Contributors
* @hoki0713 made their first contribution in https://github.com/collabsapp/webapp/pull/998

**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.02.10.9...22.03.03.1

## 22.02.10.9

* Update changelog by @bot-syncro in https://github.com/collabsapp/webapp/pull/981
* [CI] Make sure we push to production by @Nyholm in https://github.com/collabsapp/webapp/pull/982

## New Contributors
* @bot-syncro made their first contribution in https://github.com/collabsapp/webapp/pull/981

**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.02.10.8...22.02.10.9

## 22.02.10.8

* Make sure CI has all the tokens by @Nyholm in https://github.com/collabsapp/webapp/pull/979
* [CI] Fixed release notes by @Nyholm in https://github.com/collabsapp/webapp/pull/980


**Full Changelog**: https://github.com/collabsapp/webapp/compare/22.02.10.6...22.02.10.8

## Release 22.02.10.3

- First entry on updated change log

### [1.0.14 (2021-11-24)](https://github.com/collabsapp/webapp/pull/877/files) - [danny-nguyen-goldenowl]

### Fix bug

- **main:** Edit todo 1 - fix: only change caption but can't save

### [1.0.13 (2021-11-18)](https://github.com/collabsapp/webapp/pull/870/files) - [danny-nguyen-goldenowl]

### Feature

- **main:** Enable a possibility for influencers to change their content that is under review

### [1.0.12 (2021-11-12)](https://github.com/collabsapp/webapp/pull/866/files) - [danny-nguyen-goldenowl]

### Feature

- **main:** Assignment: should this be a slideshow - should also be visible within the brief for the profiles

### [1.0.11 (2021-11-04)](https://github.com/collabsapp/webapp/pull/858/files) - [danny-nguyen-goldenowl]

### Fix bug

- **main:** Temporary: For issue tracking not show the options in Rejection Modal form

### [1.0.10 (2021-10-20)](https://github.com/collabsapp/webapp/pull/836/files) - [mabel-nguyen-goldenowl]

### Features

- **main:** As an Organization, I want new assignment (TikTok)

### [1.0.9 (2021-10-19)](https://github.com/collabsapp/webapp/pull/840/files) - [danny-nguyen-goldenowl]

### Features

- **main:** Handle the list of which profiles are added or removed for list created on current client

* Admin/saler to see the internal comments of the list shared to clients

### [1.0.8 (2021-10-19)](https://github.com/collabsapp/webapp/pull/839/files) - [danny-nguyen-goldenowl]

### Features

- **main:** Add possibility to delete and edit a comment in each profile of the influencer

### [1.0.7 (2021-10-01)](https://github.com/collabsapp/webapp/pull/831/files) - [danny-nguyen-goldenowl]

### Fix

- **main:** Add new influencers to active campaign through list as well - Via add new influencers button in campaign

### [1.0.6 (2021-10-01)]() - [danny-nguyen-goldenowl]

### Features

- **main:** Update git change log file

### [1.0.5 (2021-09-30)](https://github.com/collabsapp/webapp/pull/827/files) - [danny-nguyen-goldenowl]

### Features

- **main:** Add new influencers to active campaign throught list as well

### [1.0.4 (2021-09-30)](https://github.com/collabsapp/webapp/pull/826/files) - [danny-nguyen-goldenowl]

### Fix

- **main:** Fix build cercleci failed (cont.)

### [1.0.3 (2021-09-30)](https://github.com/collabsapp/webapp/pull/825/files) - [danny-nguyen-goldenowl]

### Fix

- **main:** Fix build cercleci failed

### [1.0.2 (2021-09-30)](https://github.com/collabsapp/webapp/pull/824/files) - [danny-nguyen-goldenowl]

### Features

- **main:** enable commenting on profiles within a list like marking to remove or add to campaign but with a comment

### [1.0.1 (2021-09-30)](https://github.com/collabsapp/webapp/pull/823/files) - [danny-nguyen-goldenowl]

### Features

- **main:** enable sharing list from sale account to managed clients account

### [1.0.0 (2021-09-30)](https://github.com/collabsapp/webapp/blob/master/CHANGELOG.md) - [danny-nguyen-goldenowl]

### Features

- **main:** Create git change log file
