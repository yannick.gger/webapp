<?php

require __DIR__ . '/vendor/autoload.php';

if (2 !== $argc) {
    exit('Usage: ./release-notes.php 1.2.3');
}

function getRelease(string $tag): array
{
    $github = Github\Client::createWithHttpClient(new \Symfony\Component\HttpClient\Psr18Client());
    $github->authenticate(getenv('GITHUB_TOKEN'), Github\Client::AUTH_ACCESS_TOKEN);

    $repoFullName = getenv('GITHUB_REPOSITORY');
    list($username, $repoName) = explode('/', $repoFullName, 2);

    try {
        $release = $github->repository()->releases()->tag($username, $repoName, $tag);
    } catch (\Throwable $e) {
        exit(sprintf('Release for tag "%s" was not found. "%s"', $tag, $e->getMessage()));
    }

    return $release;
}

function writeChangelog(string $releaseNotes): void
{
    $changelogPath = \dirname(__DIR__, 2) . '/CHANGELOG.md';
    $changelog = file_get_contents($changelogPath);

    if (false !== strpos($changelog, $releaseNotes)) {
        return;
    }

    $changelog = str_replace('# Change log', '# Change log' . \PHP_EOL . \PHP_EOL . $releaseNotes, $changelog);
    file_put_contents($changelogPath, $changelog);
}

function pingSlack(string $title, string $body): void
{
    $url = 'https://hooks.slack.com/services/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';
    $httpClient = \Symfony\Component\HttpClient\HttpClient::create();

    $body = preg_replace('| by @.*|i', '', $body);
    $body = preg_replace('|^\* |m', '- ', $body);

    $text = 'Webapp updated to '.$title . ' :tada:' . \PHP_EOL . $body .\PHP_EOL . \PHP_EOL;
    $text .= 'We have just started the release process. If everything goes well, it will be live in 15 minutes.';

    $httpClient->request('POST', $url, [
        'json' => [
            'channel' => 'deploy',
            'text' => $text,
            'username' => 'Deploy bot',
        ],
    ]);
}

$release = getRelease($argv[1]);
$changelog = str_replace("## What's Changed", '', $release['body']);
writeChangelog('## ' . $release['name'] . \PHP_EOL . $changelog);
pingSlack($release['name'], $changelog);
