import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { addDecorator } from '@storybook/react';
import { ThemeProvider } from 'styled-components';
import GlobalStyle from '../src/styles/global';
import { collabs } from '../src/styles/themes';
import '!style-loader!css-loader!sass-loader!../src/app.scss';
import 'antd/dist/antd.css';

const newViewports = {
  tablet: {
    name: 'Tablet',
    styles: {
      width: '1366px',
      height: '768px'
    }
  },
  mobile: {
    name: 'Mobile',
    styles: {
      width: '1080px',
      height: '1920px'
    }
  },
  mobileLowRes: {
    name: 'Mobile Low Res',
    styles: {
      width: '750px',
      height: '1334px'
    }
  }
};

export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
		viewport: {
			viewports: newViewports,
			defaultViewport: INITIAL_VIEWPORTS
		},
		layout: 'padded'
};


addDecorator(s => <ThemeProvider theme={collabs}><GlobalStyle />{s()}</ThemeProvider>);
