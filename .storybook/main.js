const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CracoAntDesignPlugin = require('craco-antd');

module.exports = {
  addons: [
    '@storybook/addon-actions/register',
    '@storybook/addon-links/register',
    '@storybook/addon-viewport/register',
    '@storybook/addon-controls'
  ],
  framework: '@storybook/react',
  stories: ['../src/**/**/*.stories.@(js|ts|tsx)'],
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Make whatever fine-grained changes you need

    config.plugins.push(
      new MiniCssExtractPlugin(),
    );

    // Return the altered config
    return config;
  },
};
