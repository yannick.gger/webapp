module.exports = {
	extends: ['plugin:prettier/recommended'],
	plugins: ['react', 'graphql'],
	parserOptions: {
		ecmaVersion: 6,
		sourceType: 'module'
	},
	env: {
		browser: true,
		node: true,
		es6: true
	},
	globals: {
		Set: true
	},
	parser: '@typescript-eslint/parser',
	rules: {
		'react/style-prop-object': 'off',
		'jsx-a11y/href-no-hash': 'off',
		'jsx-a11y/anchor-is-valid': 'off',
		'array-callback-return': 'off',
		'no-restricted-globals': 'off',
		'no-extend-native': 'off',
		'react/prop-types': 'off',
		'react/no-unescaped-entities': 'off',
		'no-console': 'off',
		semi: ['off', 'always'],
		quotes: ['off', 'single', { allowTemplateLiterals: true }],
		indent: ['off', 'tab', { VariableDeclarator: 2 }],
		'max-len': [
			0,
			160,
			2,
			{
				ignoreUrls: true,
				ignoreComments: true,
				ignoreStrings: true,
				ignoreTemplateLiterals: true,
				ignoreRegExpLiterals: true
			}
		],
		'react/display-name': 'off',
		'no-debugger': 'off',
		'prettier/prettier': [
			'warn',
			{
				endOfLine: 'auto'
			}
		],
		'graphql/template-strings': [
			'error',
			{
				env: 'literal',
				schemaJsonFilepath: './schema.json'
			}
		]
	}
};
