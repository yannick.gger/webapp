const cracoGraphqlLoader = require('craco-graphql-loader');
const CracoEslintWebpackPlugin = require('craco-eslint-webpack-plugin');
const reactHotReloadPlugin = require('craco-plugin-react-hot-reload');
const CracoAntDesignPlugin = require('craco-antd');

// Craco config
module.exports = {
	plugins: [
		{
			plugin: CracoAntDesignPlugin,
			options: {
				customizeTheme: {
					'@primary-color': '#157ff1',
					'@font-size-base': '16px',
					'@border-color-base': '#D9E0EB',
					'font-family-no-number':
						'"Nunito Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", "Helvetica Neue", Helvetica, Arial, sans-serif',
					'font-family':
						'"Nunito Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", "Helvetica Neue", Helvetica, Arial, sans-serif'
				},
				babelPluginImportOptions: {
					libraryDirectory: 'es'
				}
			}
		},
		{
			plugin: CracoEslintWebpackPlugin,
			options: {
				skipPreflightCheck: true,
				eslintOptions: {
					files: 'src/**/*.{js,jsx,ts,tsx}',
					lintDirtyModulesOnly: true
				}
			}
		},
		{plugin: cracoGraphqlLoader},
		{plugin: reactHotReloadPlugin}
	],
	webpack: {
		configure: {
			module: {
				rules: [
					{
						type: 'javascript/auto',
						test: /\.mjs$/,
						use: []
					}
				]
			}
		},
		alias: { 'react-dom': '@hot-loader/react-dom' }
	},
	babel: {
		plugins: [
			['@babel/plugin-proposal-decorators', { version: 'legacy' }],
			['@quickbaseoss/babel-plugin-styled-components-css-namespace', { cssNamespace: '&&&' }],
			'babel-plugin-styled-components',
			['@babel/plugin-proposal-class-properties', { loose: true }],
			['@babel/plugin-proposal-private-property-in-object', { loose: true }],
			['@babel/plugin-proposal-private-methods', { loose: true }]
		],
		presets: [['@babel/preset-env', { useBuiltIns: 'entry', corejs: '3.22' }]]
	},
	style: {
		postcss: {
			plugins: [require('autoprefixer')],
			env: {
				autoprefixer: {}
			}
		}
	}
};
