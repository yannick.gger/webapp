import { takeLatest, setContext } from 'redux-saga/effects';
import * as TYPES from 'actions/constants';
import * as CURRENTUSERWATCHER from './current-user';

export default (client) =>
	function* root() {
		yield setContext({ client });
		yield takeLatest(TYPES.CURRENTUSER.CURRENTUSER_SEARCH_FEATURE_REQUEST, CURRENTUSERWATCHER.onSearchFeatureRequest);
		yield takeLatest(TYPES.CURRENTUSER.CURRENTUSER_SWITCH_FEATURE_REQUEST, CURRENTUSERWATCHER.onSwitchFeatureRequest);
	};
