import { put, call, getContext } from 'redux-saga/effects';
import * as ACTION from 'actions/auth';
import getCurrentUser from 'graphql/get-current-user.graphql';

export function* onRefetchRequest({ options }) {
	try {
		const client = yield getContext('client');
		const { data } = yield call(client.query, { query: getCurrentUser, ...(options || {}) });

		yield put(ACTION.onRefetchSuccess(data));
	} catch (err) {
		yield put(ACTION.onRefetchFailure({ error: err }));
	}
}
