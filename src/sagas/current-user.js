import React from 'react';
import { put, call, getContext } from 'redux-saga/effects';
import * as ACTION from 'actions/current-user';
import { notification } from 'antd';
import setFeatureFlagMutation from 'graphql/set-feature-flag.graphql';
import getCurrentUserFeature from 'graphql/get-current-user-feature.graphql';

export function* onSearchFeatureRequest({ options, type }) {
	try {
		const client = yield getContext('client');
		const { data } = yield call(client.query, { query: getCurrentUserFeature, fetchPolicy: 'no-cache', ...(options || {}) });

		yield put(ACTION.onSearchFeatureSuccess(data.currentUser));
	} catch (err) {
		yield put(ACTION.onSearchFeatureFailure({ error: err }));
	}
}

export function* onSwitchFeatureRequest({ key, enable, userId, options }) {
	try {
		const client = yield getContext('client');
		const { data } = yield call(client.mutate, { mutation: setFeatureFlagMutation, variables: { switchFeature: { key, enable, userId } }, ...(options || {}) });
		const mode = enable ? 'Enable' : 'Disable';

		if (!data.switchFeature) {
			throw new Error('Please check your feature');
		}

		if (data.switchFeature.errors.length) {
			throw data.switchFeature.errors;
		}

		yield put(ACTION.onSwitchFeatureSuccess(data.switchFeature));
		yield call(onSearchFeatureRequest, {});

		notification.success({
			message: `${mode} feature ${key}`,
			description: `Feature ${key} is ${mode.toLowerCase()}d`
		});
	} catch (errors) {
		yield put(ACTION.onSwitchFeatureFailure({ errors }));

		notification.error({
			message: `${enable ? 'Enable' : 'Disable'} feature ${key}`,
			description: <div>{errors.message ? errors.message : errors.map(({ message }, index) => <p key={index}>{message}</p>)}</div>
		});
	}
}
