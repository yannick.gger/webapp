import { Campaign, Conversation } from 'components/IntegratedInbox/types';
import {
	GET_CAMPAIGNS_SUCCESS,
	GET_CAMPAIGNS_FAILURE,
	ACTIVE_CAMPAIGN_SUCCESS,
	GET_CAMPAIGNS_REQUEST,
	ACTIVE_CAMPAIGN_REMOVED,
	GET_CONVERSATIONS_FAILURE,
	GET_CONVERSATIONS_SUCCESS,
	GET_CONVERSATIONS_REQUEST,
	ACTIVE_CONVERSATION_REMOVED,
	ACTIVE_CONVERSATION_SUCCESS,
	FETCH_BY_URL_SUCCESS
} from './Action.types';

export const CampaignsRequest = () => {
	return {
		type: GET_CAMPAIGNS_REQUEST,
		payload: {
			campaigns: [],
			loading: true,
			loaded: false,
			error: false,
			errorResponse: {}
		}
	};
};

export const CampaignsSuccess = (campaigns: Array<Campaign>) => {
	return {
		type: GET_CAMPAIGNS_SUCCESS,
		payload: {
			campaigns: campaigns,
			loading: false,
			loaded: true,
			error: false,
			errorResponse: {}
		}
	};
};

export const GetCampaignsListFailure = (errors: any) => {
	return {
		type: GET_CAMPAIGNS_FAILURE,
		payload: {
			campaigns: [],
			loading: false,
			loaded: true,
			error: true,
			errorResponse: errors
		}
	};
};

export const ConversationsRequest = () => {
	return {
		type: GET_CONVERSATIONS_REQUEST,
		payload: {
			conversations: [],
			loading: true,
			loaded: false,
			error: false,
			errorResponse: {}
		}
	};
};

export const ConversationsSuccess = (conversations: Array<Conversation>) => {
	return {
		type: GET_CONVERSATIONS_SUCCESS,
		payload: {
			conversations: conversations,
			loading: false,
			loaded: true,
			error: false,
			errorResponse: {}
		}
	};
};

export const ConversationsFailure = (errors: any) => {
	return {
		type: GET_CONVERSATIONS_FAILURE,
		payload: {
			conversations: [],
			loading: false,
			loaded: true,
			error: true,
			errorResponse: errors
		}
	};
};

export const ActiveCampaignSuccess = (campaign: Campaign) => {
	return {
		type: ACTIVE_CAMPAIGN_SUCCESS,
		payload: campaign
	};
};

export const ActiveConversationRemoved = () => {
	return {
		type: ACTIVE_CONVERSATION_REMOVED,
		payload: {}
	};
};

export const ActiveConversationSuccess = (conversation: Conversation) => {
	return {
		type: ACTIVE_CONVERSATION_SUCCESS,
		payload: conversation
	};
};

export const ActiveCampaignRemoved = () => {
	return {
		type: ACTIVE_CAMPAIGN_REMOVED,
		payload: {}
	};
};

export const ConversationRequest = () => {
	return {
		type: GET_CONVERSATIONS_REQUEST,
		payload: {
			conversations: [],
			loading: true,
			loaded: true,
			error: true,
			errorResponse: {}
		}
	};
};

export const ConversationSuccess = (conversations: Array<Conversation>) => {
	return {
		type: GET_CONVERSATIONS_SUCCESS,
		payload: {
			conversations: conversations,
			loading: false,
			loaded: true,
			error: true,
			errorResponse: {}
		}
	};
};

export const ConversationFailure = (errors: any) => {
	return {
		type: GET_CONVERSATIONS_FAILURE,
		payload: {
			conversations: [],
			loading: false,
			loaded: true,
			error: true,
			errorResponse: errors
		}
	};
};

export const FetchByUrl = (loading: boolean) => {
	return {
		type: FETCH_BY_URL_SUCCESS,
		payload: {
			loading: loading
		}
	};
};
