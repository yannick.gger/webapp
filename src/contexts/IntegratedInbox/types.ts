import { Campaign, Conversation } from 'components/IntegratedInbox/types';

export interface IIntegratedInboxContext {
	Campaigns: ICampaign[];
	Conversations: IConversation[];
	UpdateCampaigns: (campaigns: ICampaign[]) => void;
	UpdateCconversations: (conversations: Conversation[]) => void;
}

export interface ICampaignList {
	campaigns: Array<Campaign>;
	loading: boolean;
	loaded: boolean;
	error: boolean;
	errorMessage: boolean;
}

export interface ICampaign {
	id: number;
	coverPhotoUrl: string;
	name: string;
	latestMessage: ILatestMessage;
	unreadMessages: any;
}

export interface IConversationList {
	conversations: Array<Conversation>;
	loading: boolean;
	loaded: boolean;
	error: boolean;
	errorMessage: boolean;
}

export interface IConversation {
	id: string;
	blast: boolean;
	date: string;
	rawDate: Date;
	messages: Array<any>;
	users: Array<any>;
	latestMessage: any;
}

export interface ILatestMessage {
	date: string;
	rawDate: Date;
	message: string;
	readAt: string;
}

export interface ILoading {
	conversation: boolean;
	conversations: boolean;
}
