//
// Integrated Inbox Reducer
// ===================================================

import * as ACTION_TYPES from './Action.types';

export const initialState: any = {
	campaignList: {
		campaigns: [],
		loading: false,
		loaded: true
	},
	activeCampaign: {},
	conversationList: {
		conversations: [],
		loading: false,
		loaded: true
	},
	fetchByUrl: {
		loading: false
	}
};

interface IAction {
	type: string;
	payload: object;
}

export const IntegratedInboxReducer = (state = initialState, action: IAction) => {
	switch (action.type) {
		case ACTION_TYPES.GET_CAMPAIGNS_REQUEST:
			return {
				...state,
				campaignList: action.payload
			};
		case ACTION_TYPES.GET_CAMPAIGNS_SUCCESS:
			return {
				...state,
				campaignList: action.payload
			};
		case ACTION_TYPES.GET_CAMPAIGNS_FAILURE:
			return {
				...state,
				campaignList: action.payload
			};
		case ACTION_TYPES.ACTIVE_CAMPAIGN_SUCCESS:
			return {
				...state,
				activeCampaign: action.payload
			};
		case ACTION_TYPES.ACTIVE_CAMPAIGN_REMOVED:
			return {
				...state,
				activeCampaign: {}
			};
		case ACTION_TYPES.ACTIVE_CONVERSATION_SUCCESS:
			return {
				...state,
				activeConversation: action.payload
			};
		case ACTION_TYPES.ACTIVE_CONVERSATION_REMOVED:
			return {
				...state,
				activeConversation: {}
			};
		case ACTION_TYPES.GET_CONVERSATIONS_REQUEST:
			return {
				...state,
				conversationList: action.payload
			};
		case ACTION_TYPES.GET_CONVERSATIONS_SUCCESS:
			return {
				...state,
				conversationList: action.payload
			};
		case ACTION_TYPES.GET_CONVERSATIONS_FAILURE:
			return {
				...state,
				conversationList: action.payload
			};
		case ACTION_TYPES.FETCH_BY_URL_SUCCESS:
			return {
				...state,
				fetchByUrl: action.payload
			};
		default: {
			throw new Error(`Unsupported action type: ${action.type}`);
		}
	}
};
