import { ReactNode, useReducer } from 'react';
import Context from './Context';
import { initialState, IntegratedInboxReducer } from './Reducer';
import * as ACTIONS from './Actions';
import { Campaign, Conversation } from 'components/IntegratedInbox/types';

export interface IProps {
	children: ReactNode;
}

const IntegratedInboxContextProvider = (props: IProps) => {
	const [state, dispatch] = useReducer(IntegratedInboxReducer, initialState);

	return (
		<>
			<Context.Provider
				value={{
					campaignList: state.campaignList,
					conversationList: state.conversationList,
					activeCampaign: state.activeCampaign || {},
					activeConversation: state.activeConversation || {},
					fetchByUrl: state.fetchByUrl || { loading: false },

					UpdateCampaignsListRequest: () => dispatch(ACTIONS.CampaignsRequest()),
					UpdateCampaignsListSuccess: (campaigns: Array<Campaign>) => dispatch(ACTIONS.CampaignsSuccess(campaigns)),
					UpdateCampaignsListFailure: (error: any) => dispatch(ACTIONS.GetCampaignsListFailure(error)),

					UpdateConversationsRequest: () => dispatch(ACTIONS.ConversationRequest()),
					UpdateConversationsSuccess: (conversations: Array<Conversation>) => dispatch(ACTIONS.ConversationSuccess(conversations)),
					UpdateConversationsFailure: (error: any) => dispatch(ACTIONS.ConversationFailure(error)),

					SetActiveCampaign: (campaign: Campaign) => dispatch(ACTIONS.ActiveCampaignSuccess(campaign)),
					RemoveActiveCampaign: () => dispatch(ACTIONS.ActiveCampaignRemoved()),

					SetActiveConversation: (conversation: Conversation) => dispatch(ACTIONS.ActiveConversationSuccess(conversation)),
					RemoveActiveConversation: () => dispatch(ACTIONS.ActiveConversationRemoved()),

					FetchByUrl: (loading: boolean) => dispatch(ACTIONS.FetchByUrl(loading))
				}}
			>
				{props.children}
			</Context.Provider>
		</>
	);
};

export default IntegratedInboxContextProvider;
