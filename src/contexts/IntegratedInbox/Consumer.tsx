import React from 'react';
import IntegratedInboxContext from './Context';

export default function withIntegratedInboxContext(Component: any) {
	return function contextComponent(props: Record<string, any>) {
		return <IntegratedInboxContext.Consumer>{(context) => <Component {...props} context={context} />}</IntegratedInboxContext.Consumer>;
	};
}
