import { Campaign, Conversation } from 'components/IntegratedInbox/types';
import React from 'react';
import { ICampaignList, IConversationList, ILoading } from './types';

export type IntegratedInboxContextType = {
	campaignList: ICampaignList;
	activeCampaign: Campaign;
	conversationList: IConversationList;
	activeConversation: Conversation;
	fetchByUrl: { loading: boolean };

	// Campaigns list
	UpdateCampaignsListRequest: () => void;
	UpdateCampaignsListSuccess: (campaigns: Array<Campaign>) => void;
	UpdateCampaignsListFailure: (error: any) => void;

	// Conversations
	UpdateConversationsRequest: () => void;
	UpdateConversationsSuccess: (conversations: Array<Conversation>) => void;
	UpdateConversationsFailure: (error: any) => void;

	// Active campaign
	SetActiveCampaign: (campaign: Campaign) => void;
	RemoveActiveCampaign: () => void;

	// Active conversation
	SetActiveConversation: (conversation: Conversation) => void;
	RemoveActiveConversation: () => void;

	FetchByUrl: (loading: boolean) => void;
};

export const Context = React.createContext<IntegratedInboxContextType | null>(null);
export const ContextProvider = Context.Provider;
export const ContextConsumer = Context.Consumer;
export default Context;
