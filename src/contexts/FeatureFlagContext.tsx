import React from 'react';
import { EnabledFeatures } from './type';

export const FeatureToggleContext = React.createContext({
	enabledFeatures: [] as EnabledFeatures[]
});
