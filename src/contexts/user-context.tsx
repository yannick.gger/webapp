import React, { useState, useEffect } from 'react';

import * as authService from '../services/auth-service';
import { User, IUserContext, UserContextProps } from './type';

const UserContext = React.createContext<IUserContext>({
	user: null,
	ghostUser: null,
	updateLoginState: () => {},
	updateIsGhostUser: () => {},
	currentUser: () => {}
});

export const UserContextProvider: React.FC<UserContextProps> = ({ children }) => {
	const [token, setToken] = useState<string | null>();
	const [isLoggedIn, setIsLoggedIn] = useState(false);
	const [isGhostUser, setIsGhostUser] = useState(false);
	const [user, setUser] = useState<User | null>(null);
	const [ghostUser, setGhostUser] = useState<User | null>(null);

	useEffect(() => {
		updateLoginState();
	}, []);

	useEffect(() => {
		if (isLoggedIn) {
			setToken(authService.getToken());
		}
	}, [isLoggedIn]);

	useEffect(() => {
		if (token && isLoggedIn) {
			saveUserInfoInLocalStorage();
		} else {
			setUser(null);
			setGhostUser(null);
			setIsGhostUser(false);
		}
	}, [token, isLoggedIn, isGhostUser]);

	useEffect(() => {
		if (user) localStorage.setItem('user', JSON.stringify(user));
		if (ghostUser) localStorage.setItem('ghostUser', JSON.stringify(ghostUser));
	}, [user, ghostUser]);

	const updateIsGhostUser = () => {
		setIsGhostUser(authService.checkGhostUser());
	};

	const updateLoginState = () => {
		setIsLoggedIn(authService.isLoggedIn());
	};

	const saveUserInfoInLocalStorage = () => {
		const tokenPayload = authService.getTokenPayload();
		const ghostUser = authService.hasGhostToken();

		const getUserRole = () => {
			let roles: { [key: string]: boolean } = {
				admin: ghostUser ? authService.isAdmin() : authService.isAdminOrGhostAdmin(),
				publisher: authService.isPublisher(),
				influencer: authService.isInfluencer(),
				saleUser: authService.isSaler(),
				testUser: authService.isTester()
			};

			const availableRoles: Array<string> = Object.keys(roles).filter((role: string) => roles[role]);
			return availableRoles.length ? availableRoles.join('/') : '';
		};

		const getOrganisation = () => {
			if (tokenPayload.user.organizations.length) {
				return tokenPayload.user.organizations[0].name;
			} else {
				return '';
			}
		};

		const getOrganizationBySlug = () => {
			if (tokenPayload.user.organizations.length) {
				return tokenPayload.user.organizations[0].slug;
			} else {
				return '';
			}
		};

		const userInfo: User = {
			UserId: tokenPayload.user.id,
			Username: tokenPayload.user.name,
			Organization: getOrganisation(),
			Role: getUserRole(),
			BasePath: getOrganizationBySlug()
		};

		if (ghostUser) {
			const copyUser: User = { ...user };
			setGhostUser(copyUser);
			setUser(userInfo);
		} else {
			setUser(userInfo);
			setGhostUser(null);
		}
	};

	const currentUser = () => {
		return user;
	};

	const context = {
		user,
		ghostUser,
		updateLoginState,
		updateIsGhostUser
	};

	const userContextProps = {
		value: context
	};

	return <UserContext.Provider {...userContextProps}>{children}</UserContext.Provider>;
};

export default UserContext;
