import { ReactNode } from 'react';

export interface DiscoveryContextProps {
	children: ReactNode;
}

export type DiscoveryContextType = {
	loading: boolean;
	lookALikeLoading: boolean;
	filter: FilterType | null;
	searchHandler: () => Promise<{ influencers: any[]; categoryTags: any[] }>;
	searchResult: InfluencerListItemType[] | null;
	categoryTags: SearchCateogryType[] | null;
	changeSearchValueHandler: (param: { text: string | null; categoryId: string }) => void;
	addSearchCategoryHandler: (param: { text: string; categoryId: string }) => void;
	searchText: string | null;
	setFilter: (filter: FilterType) => void;
	resetFilter: () => void;
	changeBrandMentions: (value: string, option: string) => void;
	removeBrandMentions: (value: string, option: string) => void;
	updateGenderHandler: (value: string, option: string) => void;
	updateAudienceAgeRange: (value: string) => void;
	lookALikeHandler: (influencer: InfluencerListItemType) => void;
	lookALikeInfluencerName: string | null;
	removeLookALikeHandler: () => void;
	searchTotalResult: { totalResultCount: number | null; executionTimeMs: number | null } | null;
	getOneInfluencerExtraData: (influencerId: string) => any;
	getReason: (url: string) => any;
	removeOneFilter: (filterKey: string) => void;
};

export type FilterType = {
	minAge: number | null;
	maxAge: number | null;
	genders: string | null;
	countries: string | null;
	minFollowers: number | null;
	maxFollowers: number | null;
	brandMentions: string | null;
	excludeBrandMentions: string | null;
	audienceAgeRanges: string | null;
	audienceGenders: string | null;
	audienceCountries: string | null;
};

export type AudienceAgesType = {
	'13-17': CountRatioData;
	'18-24': CountRatioData;
	'25-34': CountRatioData;
	'35-44': CountRatioData;
	'45-54': CountRatioData;
	'55-64': CountRatioData;
	'65+': CountRatioData;
};

export type GendersType = {
	female: CountRatioData;
	male: CountRatioData;
	unknown: CountRatioData;
};

export type AudienceCountriesType = {
	[key: string]: CountRatioData;
};

export type InfluencerCategoryType = {
	id: string;
	name: string;
	score: number;
};

export type SearchCateogryType = {
	id: string;
	name: string;
	level: number;
};

export type CountRatioData = {
	name?: string | null;
	count: number | null;
	ratio: number | null;
};

export type BrandAffiliation = {
	username: string;
	url: string;
	postedAt: string;
};

export type InfluencerListItemType = {
	id: string;
	age: string | null;
	country: string | null;
	gender: string | null;
	followersCount: number;
	network: string;
	networkId: string;
	networkLinks: { [key: string]: string };
	profileImageUrl: string | null;
	username: string;
	relevancy: number | null;
	topicScore: number | null;
	engagement: number | null;
	audienceBrief: { age: CountRatioData; gender: CountRatioData; country: CountRatioData };
	averageViews?: number | null;
	categories: InfluencerCategoryType[];
	audienceDetail?: { ages: AudienceAgesType; countries: AudienceCountriesType | null; genders: GendersType };
	brandAffiliations?: BrandAffiliation[];
	engagementOrganic?: number | null;
	engagementPaid?: number | null;
	paidPostsRatio?: number | null;
	instagramPostsLink?: string | null;
};
