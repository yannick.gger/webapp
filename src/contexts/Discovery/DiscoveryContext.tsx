import { createContext, useState, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Store } from 'json-api-models';

import DiscoveryService from 'services/DiscoveryApi';
import { DiscoveryContextType, DiscoveryContextProps, FilterType, InfluencerListItemType, SearchCateogryType } from './types';
/**
 * @todo i18next
 */
const DiscoveryContext = createContext<DiscoveryContextType>({
	loading: false,
	lookALikeLoading: false,
	filter: null,
	searchHandler: async () => ({ influencers: [], categoryTags: [] }),
	searchResult: null,
	categoryTags: null,
	changeSearchValueHandler: () => {},
	addSearchCategoryHandler: () => {},
	searchText: null,
	setFilter: () => {},
	resetFilter: () => {},
	changeBrandMentions: () => {},
	removeBrandMentions: () => {},
	updateGenderHandler: () => {},
	updateAudienceAgeRange: () => {},
	lookALikeHandler: () => {},
	lookALikeInfluencerName: null,
	removeLookALikeHandler: () => {},
	searchTotalResult: null,
	getOneInfluencerExtraData: async (influencerId: string) => {},
	getReason: (url: string) => {},
	removeOneFilter: (filterKey: string) => {}
});

export const DiscoveryContextProvider: React.FC<DiscoveryContextProps> = ({ children }) => {
	const models = new Store();
	const { search, pathname } = useLocation();
	const history = useHistory();

	const [loading, setLoading] = useState<boolean>(true);
	const [lookALikeLoading, setLookALikeLoading] = useState<boolean>(false);
	const [lookALikeInfluencerName, setLookALikeInfluencerName] = useState<string | null>(null);
	const [lookALikeInfluencerId, setLookALikeInfluencerId] = useState<string | null>(null);

	const [searchText, setSearchText] = useState<string | null>(null);
	const [categories, setCategories] = useState<string | null>(null);
	const [filter, setFilter] = useState<FilterType | null>(null);
	const [searchResult, setSearchResult] = useState<InfluencerListItemType[] | null>(null);
	const [searchTotalResult, setSearchTotalResult] = useState<{ totalResultCount: number | null; executionTimeMs: number | null } | null>(null);
	const [categoryTags, setCategoryTags] = useState<SearchCateogryType[] | null>(null);

	useEffect(() => {
		parseUrlHandler();
	}, []);

	const parseUrlHandler = async () => {
		if (search) {
			const queryString = new URLSearchParams(search);
			const qFilter: FilterType = {
				minAge: numberOrNull(queryString.get('minAge')),
				maxAge: numberOrNull(queryString.get('maxAge')),
				genders: queryString.get('genders') ? queryString.get('genders') : 'f,m',
				countries: queryString.get('countries'),
				minFollowers: numberOrNull(queryString.get('minFollowers')),
				maxFollowers: numberOrNull(queryString.get('maxFollowers')),
				brandMentions: queryString.get('brandMentions'),
				excludeBrandMentions: queryString.get('excludeBrandMentions'),
				audienceAgeRanges: queryString.get('audienceAgeRanges'),
				audienceGenders: queryString.get('audienceGenders'),
				audienceCountries: queryString.get('audienceCountries')
			};

			setSearchText(queryString.get('q'));
			setCategories(queryString.get('categories'));
			setFilter(qFilter);
			setLookALikeInfluencerId(queryString.get('lookalike'));
		}
	};

	const numberOrNull = (value: string | null): number | null => {
		return value ? Number(value) : null;
	};

	useEffect(() => {
		setLoading(true);
		if (searchText || categories || filter || lookALikeInfluencerId) {
			urlChangeHandler();
			searchHandler().then(({ influencers, categoryTags, lookALike, totalResult }) => {
				setSearchResult(influencers);
				setCategoryTags(categoryTags);
				setLookALikeInfluencerName(lookALike ? lookALike.username : null);
				setSearchTotalResult(totalResult);
				setLoading(false);
			});
		} else {
			setLoading(false);
		}
	}, [searchText, categories, filter, lookALikeInfluencerId]);

	const searchHandler = async () => {
		const result: {
			influencers: any[];
			categoryTags: any[];
			lookALike: { id: string; username: string } | null;
			totalResult: { totalResultCount: number | null; executionTimeMs: number | null } | null;
		} = {
			influencers: [],
			categoryTags: [],
			lookALike: null,
			totalResult: null
		};

		let filterQueryParam: string | null = null;

		if (filter) {
			if (Object.values(filter).some((value) => value)) {
				filterQueryParam = Object.entries(filter)
					.filter(([key, value]) => value)
					.map(([key, value]) => `${key}=${value}`)
					.join('&');
			}
		}

		await DiscoveryService.searchInfluencer({
			searchWord: searchText,
			categories: categories,
			filter: filterQueryParam,
			lookALike: lookALikeInfluencerId
		})
			.then((res) => {
				result.totalResult = res.data.attributes;
				models.sync(res);
				result.influencers = models.findAll('searchResult')[0].influencers.map((influencer: any) => {
					return {
						id: influencer.id,
						followersCount: influencer.followersCount,
						network: influencer.network,
						networkId: influencer.networkId,
						networkLinks: influencer.network_links,
						profileImageUrl: influencer.profileImageUrl,
						username: influencer.username,
						relevancy: influencer.relevancy,
						topicScore: influencer.topicScore,
						engagement: influencer.engagement,
						audienceBrief: influencer.audience,
						instagramPostsLink: influencer.links.explain
					};
				});
				result.categoryTags = models.findAll('searchResult')[0].categories.map((category: any) => {
					return {
						id: category.id,
						name: category.name,
						level: category.level
					};
				});
				const lookALikeInfluencer = models.findAll('searchResult')[0].lookalike;
				result.lookALike = lookALikeInfluencer
					? {
							id: lookALikeInfluencer.id,
							username: lookALikeInfluencer.username
					  }
					: null;
			})
			.catch((err) => {
				console.log(err);
			});

		return result;
	};

	const getOneInfluencerExtraData = async (influencerId: string) => {
		let result = {};
		await DiscoveryService.getInfluencerExtraData(influencerId)
			.then((res) => {
				models.sync(res);
				const influencer = models.find('influencer', influencerId);
				result = {
					age: influencer.extra.age,
					country: influencer.extra.country,
					gender: influencer.extra.gender,
					categories: influencer.extra.categories,
					averageViews: influencer.extra.averageViews,
					audienceDetail: influencer.extra.audience,
					brandAffiliations: influencer.extra.brandAffiliations,
					engagementOrganic: influencer.extra.engagementOrganic,
					engagementPaid: influencer.extra.engagementPaid,
					paidPostsRatio: influencer.extra.paidPostsRatio
				};
			})
			.catch((err) => {
				console.log(err);
			});
		return result;
	};

	const urlChangeHandler = () => {
		let newUrl = [];

		if (searchText) {
			newUrl.push(`q=${searchText}`);
		}

		if (categories) {
			newUrl.push(`categories=${categories}`);
		}
		if (filter) {
			Object.entries(filter)
				.filter(([key, value]) => value)
				.map(([key, value]) => `${key}=${value}`)
				.forEach((filterItem) => {
					newUrl.push(filterItem);
				});
		}
		if (lookALikeInfluencerId) {
			newUrl.push(`lookalike=${lookALikeInfluencerId}`);
		}

		history.push(`${pathname}?${newUrl.join('&')}`);
	};

	const changeSearchValueHandler = (param: { text: string | null; categoryId: string }) => {
		setSearchText(param.text);
		setCategories(param.categoryId);
	};

	const addSearchCategoryHandler = (param: { text: string; categoryId: string }) => {
		setSearchText((prev) => `${param.text} ${prev}`);
		setCategories((prev) => `${param.categoryId},${prev}`);
	};

	const changeBrandMentions = (value: string, option: string) => {
		const brandMetionKey = option === 'include' ? 'brandMentions' : 'excludeBrandMentions';
		setFilter((prev: any) => {
			if (prev) {
				return { ...prev, [brandMetionKey]: prev[brandMetionKey] ? prev[brandMetionKey].concat(`,${value}`) : value };
			} else {
				return { [brandMetionKey]: value };
			}
		});
	};

	const removeBrandMentions = (value: string, option: string) => {
		const brandMetionKey = option === 'include' ? 'brandMentions' : 'excludeBrandMentions';
		setFilter((prev: any) => {
			const newBrandMetionValue = prev[brandMetionKey]
				? prev[brandMetionKey]
						.split(',')
						.filter((brand: string) => brand !== value)
						.join(',')
				: null;
			return { ...prev, [brandMetionKey]: newBrandMetionValue };
		});
	};

	const updateGenderHandler = (value: string, option: string) => {
		const key = option === 'influencer' ? 'genders' : 'audienceGenders';
		let newGenders: string | null = null;
		if (filter) {
			if (filter[key]) {
				const currentGenders = filter[key]!.split(',');
				if (currentGenders.some((gender) => gender === getGender(value))) {
					newGenders = currentGenders.filter((gender) => gender !== getGender(value)).join(',');
				} else {
					newGenders = currentGenders.concat([getGender(value)]).join(',');
				}
			} else {
				newGenders = getGender(value);
			}
		}

		setFilter((prev: any) => {
			return {
				...prev,
				[key]: newGenders
			};
		});
	};

	const getGender = (gender: string) => {
		switch (gender) {
			case 'female':
				return 'f';
			case 'male':
				return 'm';
			case 'unknown':
				return 'x';
			default:
				return '';
		}
	};

	const resetFilter = () => {
		setFilter({
			minAge: null,
			maxAge: null,
			genders: null,
			countries: null,
			minFollowers: null,
			maxFollowers: null,
			brandMentions: null,
			excludeBrandMentions: null,
			audienceAgeRanges: null,
			audienceGenders: null,
			audienceCountries: null
		});
	};

	const updateAudienceAgeRange = (value: string) => {
		let newAudienceAgeRange: string | null = null;
		if (filter) {
			if (filter.audienceAgeRanges) {
				const currentAgeRanges = filter.audienceAgeRanges.split(',');
				if (currentAgeRanges.some((range) => range === value)) {
					newAudienceAgeRange = currentAgeRanges.filter((range) => range !== value).join(',');
				} else {
					newAudienceAgeRange = currentAgeRanges.concat([value]).join(',');
				}
			} else {
				newAudienceAgeRange = value;
			}
		}

		setFilter((prev: any) => {
			return {
				...prev,
				audienceAgeRanges: newAudienceAgeRange
			};
		});
	};

	const lookALikeHandler = (influencer: InfluencerListItemType) => {
		setLookALikeLoading(true);
		setSearchText(null);
		setLookALikeInfluencerId(influencer.id);
		setLookALikeInfluencerName(influencer.username);
		setTimeout(() => {
			setLookALikeLoading(false);
		}, 3000);
	};

	const removeLookALikeHandler = () => {
		setLookALikeInfluencerId(null);
		setLookALikeInfluencerName(null);
	};

	const getReason = (url: string) => {
		return DiscoveryService.getInstagramPostAndDate(url);
	};

	const removeOneFilter = (filterKey: string) => {
		console.log(filterKey);
		setFilter((prev: FilterType | null) => {
			if (prev !== null) {
				if (filterKey === 'minAge' || filterKey === 'maxAge') {
					return { ...prev, minAge: null, maxAge: null };
				} else if (filterKey === 'minFollowers' || filterKey === 'maxFollowers') {
					return { ...prev, minFollowers: null, maxFollowers: null };
				} else {
					return { ...prev, [filterKey]: null };
				}
			} else {
				return null;
			}
		});
	};

	const context = {
		loading: loading,
		lookALikeLoading: lookALikeLoading,
		filter: filter,
		searchHandler: searchHandler,
		searchResult: searchResult,
		categoryTags: categoryTags,
		changeSearchValueHandler: changeSearchValueHandler,
		addSearchCategoryHandler: addSearchCategoryHandler,
		searchText: searchText,
		setFilter: setFilter,
		resetFilter: resetFilter,
		changeBrandMentions: changeBrandMentions,
		removeBrandMentions: removeBrandMentions,
		updateGenderHandler: updateGenderHandler,
		updateAudienceAgeRange: updateAudienceAgeRange,
		lookALikeHandler: lookALikeHandler,
		lookALikeInfluencerName: lookALikeInfluencerName,
		removeLookALikeHandler: removeLookALikeHandler,
		searchTotalResult: searchTotalResult,
		getOneInfluencerExtraData: getOneInfluencerExtraData,
		getReason: getReason,
		removeOneFilter: removeOneFilter
	};

	const DiscoveryContextProps = {
		value: context
	};
	return <DiscoveryContext.Provider {...DiscoveryContextProps}>{children}</DiscoveryContext.Provider>;
};

export default DiscoveryContext;
