export interface User {
	UserId?: number | string;
	Username?: string;
	Organization?: string;
	Role?: string;
	BasePath?: string;
}

export interface IUserContext {
	user: User | null;
	ghostUser: User | null;
	updateLoginState: () => void;
	updateIsGhostUser: () => void;
	currentUser: () => void;
}

export interface UserContextProps {
	children: JSX.Element;
}

export type EnabledFeatures = {
	name: string;
	enabled: string;
};

export interface ToastContextProps {
	children: JSX.Element;
}

export interface IToast {
	id: string;
	message: string;
	mode: string;
}

export interface IToastContext {
	toasts: IToast[];
	addToast: (toast: IToast) => void;
	removeToast: (id: string) => void;
}
