import { createContext, useState, useEffect } from 'react';
import uuid from 'shared/helpers/uuid';
import { ToastContextProps, IToastContext, IToast } from './type';

const ToastContext = createContext<IToastContext>({
	toasts: [],
	addToast: () => {},
	removeToast: () => {}
});

export const ToastContextProvider: React.FC<ToastContextProps> = ({ children }) => {
	const [toasts, setToasts] = useState<IToast[]>([]);

	const addToast = (toast: { mode: string; message: string }) => {
		setToasts((prevToast) => [...prevToast, { ...toast, id: uuid() }]);
	};

	const removeToast = (id: string) => {
		setToasts((prevToasts) => prevToasts.filter((t) => t.id !== id));
	};

	const context = {
		toasts: toasts,
		addToast: addToast,
		removeToast: removeToast
	};

	const ToastContextProps = {
		value: context
	};
	return <ToastContext.Provider {...ToastContextProps}>{children}</ToastContext.Provider>;
};

export default ToastContext;
