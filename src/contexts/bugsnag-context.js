import React from 'react';

const BugsnagContext = React.createContext({ client: null });
export default BugsnagContext;
