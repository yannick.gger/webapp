import { CURRENTUSER } from './constants';

export const onSearchFeatureRequest = (params) => ({ type: CURRENTUSER.CURRENTUSER_SEARCH_FEATURE_REQUEST, ...(params || {}) });
export const onSearchFeatureSuccess = (params) => ({ type: CURRENTUSER.CURRENTUSER_SEARCH_FEATURE_SUCCESS, ...(params || {}) });
export const onSearchFeatureFailure = (params) => ({ type: CURRENTUSER.CURRENTUSER_SEARCH_FEATURE_FAILURE, ...(params || {}) });

export const onSwitchFeatureRequest = (params) => ({ type: CURRENTUSER.CURRENTUSER_SWITCH_FEATURE_REQUEST, ...(params || {}) });
export const onSwitchFeatureSuccess = (params) => ({ type: CURRENTUSER.CURRENTUSER_SWITCH_FEATURE_SUCCESS, ...(params || {}) });
export const onSwitchFeatureFailure = (params) => ({ type: CURRENTUSER.CURRENTUSER_SWITCH_FEATURE_FAILURE, ...(params || {}) });
