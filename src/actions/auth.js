import { AUTH } from './constants';

export const onLoginRequest = (params) => ({ type: AUTH.AUTH_LOGIN_REQUEST, ...(params || {}) });
export const onLoginSuccess = (params) => ({ type: AUTH.AUTH_LOGIN_SUCCESS, ...(params || {}) });
export const onLoginFailure = (params) => ({ type: AUTH.AUTH_LOGIN_FAILURE, ...(params || {}) });

export const onLogoutRequest = (params) => ({ type: AUTH.AUTH_LOGOUT_REQUEST, ...(params || {}) });
export const onLogoutSuccess = (params) => ({ type: AUTH.AUTH_LOGOUT_SUCCESS, ...(params || {}) });
export const onLogoutFailure = (params) => ({ type: AUTH.AUTH_LOGOUT_FAILURE, ...(params || {}) });

export const onRefetchRequest = (params) => ({ type: AUTH.AUTH_REFETCH_REQUEST, ...(params || {}) });
export const onRefetchSuccess = (params) => ({ type: AUTH.AUTH_REFETCH_SUCCESS, ...(params || {}) });
export const onRefetchFailure = (params) => ({ type: AUTH.AUTH_REFETCH_FAILURE, ...(params || {}) });
