import React from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { message } from 'antd';
import { PropTypes } from 'prop-types';
import { withApollo } from 'react-apollo';
import EmptyLayout from 'layouts/empty/';
import { isLoggedIn, checkAccessToPublisher } from 'services/auth-service';
import ReferralUrlService from 'services/ReferralUrlService';

const UserPublisherRoute = ({ ...props }) => {
	const Layout = props.layout || EmptyLayout;
	if (!isLoggedIn()) {
		message.info('You need to be signed in to access this page');
		ReferralUrlService.set(window.location.href);

		return <Redirect to={'/login'} />;
	}

	if (!checkAccessToPublisher({ history: props.history, apolloClient: props.client, match: props.computedMatch })) {
		return null;
	}

	return (
		<Layout {...props}>
			<Route {...props} />
		</Layout>
	);
};

UserPublisherRoute.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}),
	match: PropTypes.object.isRequired
};

export default withApollo(withRouter(UserPublisherRoute));
