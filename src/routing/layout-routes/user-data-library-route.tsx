import { Route, Redirect, withRouter, RouteComponentProps } from 'react-router-dom';
import { message } from 'antd';
import { withApollo } from 'react-apollo';
import EmptyLayout from 'layouts/empty/';
import { isLoggedIn } from 'services/auth-service';
import { IUserDataLibraryRoute } from './types';
import { FEATURE_FLAG_DATA_LIBRARY } from 'constants/feature-flag-keys';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import GenericNotFoundView from 'views/generic-not-found';
import ReferralUrlService from 'services/ReferralUrlService';

const UserDataLibraryRoute = ({ ...props }: IUserDataLibraryRoute & RouteComponentProps) => {
	const Layout: any = props.layout || EmptyLayout;
	const [isEnabled] = useFeatureToggle();

	if (!isLoggedIn()) {
		message.info('You need to be signed in to access this page');
		ReferralUrlService.set(window.location.href);
		return <Redirect to={'/login'} />;
	}

	return (
		<>
			{isEnabled(FEATURE_FLAG_DATA_LIBRARY) ? (
				<Layout {...props}>
					<Route {...props} />
				</Layout>
			) : (
				<GenericNotFoundView />
			)}
		</>
	);
};

export default withApollo(withRouter(UserDataLibraryRoute));
