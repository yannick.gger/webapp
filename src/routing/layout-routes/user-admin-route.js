import React from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { message } from 'antd';
import { withApollo } from 'react-apollo';
import EmptyLayout from 'layouts/empty/';
import { isLoggedIn, checkAdmin } from 'services/auth-service';
import ReferralUrlService from 'services/ReferralUrlService';

const UserAdminRoute = ({ ...props }) => {
	const Layout = props.layout || EmptyLayout;
	if (!isLoggedIn()) {
		message.info('You need to be signed in to access this page');
		ReferralUrlService.set(window.location.href);
		return <Redirect to={'/login'} />;
	}

	if (!checkAdmin({ history: props.history, apolloClient: props.client })) {
		return null;
	}

	return (
		<Layout {...props}>
			<Route {...props} />
		</Layout>
	);
};

export default withApollo(withRouter(UserAdminRoute));
