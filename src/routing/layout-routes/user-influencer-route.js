import React from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { message } from 'antd';
import { PropTypes } from 'prop-types';
import { withApollo } from 'react-apollo';
import EmptyLayout from 'layouts/empty/';
import { isLoggedIn, isInfluencer } from 'services/auth-service';
import ReferralUrlService from 'services/ReferralUrlService';

const UserInfluencerRoute = ({ ...props }) => {
	const Layout = props.layout || EmptyLayout;
	if (!isLoggedIn()) {
		message.info('You need to be signed in to access this page');
		ReferralUrlService.set(window.location.href);

		return (
			<Redirect
				to={{
					pathname: '/login',
					state: { redirectTo: props.history.location.pathname }
				}}
			/>
		);
	}

	if (!isInfluencer()) {
		return null;
	}

	return (
		<Layout>
			<Route {...props} />
		</Layout>
	);
};

UserInfluencerRoute.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}),
	match: PropTypes.object.isRequired
};

export default withApollo(withRouter(UserInfluencerRoute));
