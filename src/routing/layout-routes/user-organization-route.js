import React from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { message } from 'antd';
import { withApollo } from 'react-apollo';
import EmptyLayout from 'layouts/empty/';
import { isLoggedIn, checkAccessToOrganization } from 'services/auth-service';
import ReferralUrlService from 'services/ReferralUrlService';

const UserOrganizationRoute = ({ ...props }) => {
	const Layout = props.layout || EmptyLayout;
	if (!isLoggedIn()) {
		message.info('You need to be signed in to access this page');
		ReferralUrlService.set(window.location.href);
		return <Redirect to={'/login'} />;
	}

	if (!checkAccessToOrganization({ history: props.history, apolloClient: props.client, match: props.computedMatch })) {
		return null;
	}

	return (
		<Layout {...props}>
			<Route {...props} />
		</Layout>
	);
};

export default withApollo(withRouter(UserOrganizationRoute));
