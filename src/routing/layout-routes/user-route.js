import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { message } from 'antd';
import EmptyLayout from 'layouts/empty/';
import { isLoggedIn } from 'services/auth-service';
import ReferralUrlService from 'services/ReferralUrlService';

// Inspired of: https://gist.github.com/fdidron/ebcf52dc1ed62ff7d80725854d631a9e

const UserRoute = ({ ...props }) => {
	const Layout = props.layout || EmptyLayout;
	if (isLoggedIn()) {
		return (
			<Layout>
				<Route {...props} />
			</Layout>
		);
	} else {
		message.info('You need to be signed in to access this page');
		ReferralUrlService.set(window.location.href);

		return <Redirect to={'/login'} />;
	}
};

export default UserRoute;
