import React from 'react';
import { Switch } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { UserAdminRoute } from './layout-routes';
import OrganizationLayout from 'layouts/organization/';

import AdminOrganizationsView from 'views/admin/organizations';
import AdminOrganizationView from 'views/admin/organization';
import AdminCampaignView from 'views/admin/campaign';
import CampaignReport from 'views/organization/campaign/report';
import AdminUsersView from 'views/admin/users';
import AdminAccountingView from 'views/admin/accounting';
import AdminCampaignsReview from 'views/admin/campaigns/review';
import AdminCampaignsReport from 'views/admin/campaigns/report';
import AdminCampaignsOverview from 'views/admin/campaigns/overview';

import AdminCampaignsReviewDecline from 'views/admin/campaigns/review/decline';
import AdminOrganizationInviteView from 'views/admin/organization-invite';
import AdminPublisherInviteView from 'views/admin/publisher-invite';
import GrantAccessClientModal from 'views/admin/users/grant-access-client-modal.js';
import ClientAccessModal from 'views/admin/users/remove-users-access.js';
import AdminAgencyListView from '../views/admin/agency/AdminAgencyListView';
import LoadingSpinner from 'components/LoadingSpinner';
const CreatePublisher = React.lazy(() => import('views/admin/publishers/CreatePublisher'));

const AdminRoute = (props) => {
	const { path } = props.match;

	return (
		<React.Suspense fallback={LoadingSpinner}>
			<Switch>
				<UserAdminRoute path={`${path}/organization/:organizationSlug`} component={AdminOrganizationView} layout={OrganizationLayout} />
				<UserAdminRoute exact path={`${path}/organizations`} component={AdminOrganizationsView} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/organizations/:organizationSlug/campaigns/:campaignId`} component={AdminCampaignView} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/campaigns/:campaignId/report`} component={CampaignReport} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/users`} component={AdminUsersView} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/accounting`} component={AdminAccountingView} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/campaigns/review`} exact component={AdminCampaignsReview} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/campaigns/report`} exact component={AdminCampaignsReport} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/campaigns/overview`} exact component={AdminCampaignsOverview} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/agencies`} component={AdminAgencyListView} layout={OrganizationLayout} />
				<UserAdminRoute path={`${path}/publishers/create`} component={CreatePublisher} layout={OrganizationLayout} />
			</Switch>
			<ModalRoute
				path={`${path}/campaigns/review/:reviewId/decline`}
				component={AdminCampaignsReviewDecline}
				parentPath='/admin/campaigns/review'
				closeModal='closeModal'
			/>
			<ModalRoute path={`${path}/organizations/invite`} component={AdminOrganizationInviteView} parentPath='/admin/organizations' closeModal='closeModal' />
			<ModalRoute path={`${path}/publishers/invite`} component={AdminPublisherInviteView} parentPath='/admin/publishers' closeModal='closeModal' />
			<ModalRoute path={`${path}/users/grant-access-client`} component={GrantAccessClientModal} parentPath='/admin/users' closeModal='closeModal' />
			<ModalRoute path={`${path}/users/remove-access`} component={ClientAccessModal} parentPath='/admin/users' closeModal='closeModal' />
		</React.Suspense>
	);
};

export default AdminRoute;
