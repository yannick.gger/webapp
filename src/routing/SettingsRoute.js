import { Switch, useLocation } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { UserOrganizationRoute } from './layout-routes';
import OrganizationLayout from 'layouts/organization/';

import TeamInviteModal from 'views/organization/settings/components/team-settings/team-invite-modal';
import OrganizationSettingsCompanyView from 'views/organization/settings/components/company-settings';
import OrganizationSettingsTeamView from 'views/organization/settings/components/team-settings';
import OrganizationSettingsUserView from 'views/organization/settings/components/user-settings';

const SettingsRoute = (props) => {
	const location = useLocation();
	const { path } = props.match;
	return (
		<>
			<Switch>
				<UserOrganizationRoute exact path={`${path}`} component={OrganizationSettingsCompanyView} layout={OrganizationLayout} />
			</Switch>
		</>
	);
};

export default SettingsRoute;
