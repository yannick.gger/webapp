import { Switch, useLocation } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { UserOrganizationRoute } from './layout-routes';
import OrganizationLayout from 'layouts/organization/';
import CreateCampaignModal from '../components/create-campaign-modal/';

import DiscoverView from 'views/organization/discover/';
import ListsIndexView from 'views/organization/discover/lists-view/index';
import { InfluencerFolders } from 'views/organization/discover/folders-list-view';
import MyListPageView from 'views/organization/discover/list-view/index';
import { DiscoveryLandingPage, DiscoveryPage } from 'views/Discovery';

import AddSelectionToList from 'views/organization/discover/my-lists/add-selection-to-list/index';
import AddSelectionToOtherList from 'views/organization/discover/my-lists/add-selection-to-other-list/index';
import AddSelectionToCampaign from 'views/organization/discover/my-lists/add-selection-to-campaign/index';
import AddInfluencersToCampaign from 'views/organization/discover/add-influencers-to-campaign';
import AddListsToExistingCampaign from 'views/organization/discover/add-lists-to-existing-campaign-modal';
import ShareListWithClients from 'views/organization/discover/share-list-with-clients-modal';
import ProfileCommentModal from 'views/organization/discover/profile-comment-modal';
import CreateListModal from 'views/organization/discover/my-lists/create-list-modal';

const DiscoverRoute = (props) => {
	const location = useLocation();
	const { path } = props.match;
	return (
		<>
			<Switch>
				<UserOrganizationRoute exact path={`${path}`} component={DiscoverView} layout={OrganizationLayout} />
				<UserOrganizationRoute exact path={`${path}/lists`} component={InfluencerFolders} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/list/:id`} component={MyListPageView} layout={OrganizationLayout} />
				<UserOrganizationRoute exact path={`${path}/new-discovery`} component={DiscoveryLandingPage} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/new-discovery/search`} component={DiscoveryPage} layout={OrganizationLayout} />
			</Switch>

			<ModalRoute
				path={`${path}/lists/create-campaign-from-multiple-lists`}
				component={CreateCampaignModal}
				parentPath={`/${location.pathname.split('/')[1]}/discover/lists`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/list/:listId/create-campaign-from-single-list`}
				component={CreateCampaignModal}
				parentPath={`/${location.pathname.split('/')[1]}/discover/list/${location.pathname.split('/')[4]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/add-influencers-to-campaign`}
				component={AddInfluencersToCampaign}
				parentPath={`/${location.pathname.split('/')[1]}/discover`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/list/:id/create-new-list`}
				component={CreateListModal}
				parentPath={`/${location.pathname.split('/')[1]}/discover/list/${location.pathname.split('/')[4]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/create-new-list`}
				component={CreateListModal}
				parentPath={`/${location.pathname.split('/')[1]}/discover`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/add-selection-to-list`}
				component={AddSelectionToList}
				parentPath={`/${location.pathname.split('/')[1]}/discover`}
				closeModal='closeModal'
			/>

			<ModalRoute
				path={`${path}/list/:id/add-selection-to-other-list`}
				component={AddSelectionToOtherList}
				parentPath={`/${location.pathname.split('/')[1]}/discover/list/${location.pathname.split('/')[4]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/list/:id/add-selection-to-campaign`}
				component={AddSelectionToCampaign}
				parentPath={`/${location.pathname.split('/')[1]}/discover/list/${location.pathname.split('/')[4]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/lists/create-new-list`}
				component={CreateListModal}
				parentPath={`/${location.pathname.split('/')[1]}/discover/lists`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/lists/add-lists-to-existing-campaign`}
				component={AddListsToExistingCampaign}
				parentPath={`/${location.pathname.split('/')[1]}/discover/lists`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/list/:id/share-with-clients`}
				component={ShareListWithClients}
				parentPath={`/${location.pathname.split('/')[1]}/discover/lists`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/list/:id/profile-comments`}
				component={ProfileCommentModal}
				parentPath={`/${location.pathname.split('/')[1]}/discover/lists`}
				closeModal='closeModal'
			/>
		</>
	);
};

export default DiscoverRoute;
