import React, { Suspense } from 'react';
import { Switch, useLocation } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { UserOrganizationRoute } from './layout-routes';
import OrganizationLayout from 'layouts/organization/';
import EmptyLayout from 'layouts/empty';
import BlankLayout from 'layouts/blank';
import CreateCampaignModal from '../components/create-campaign-modal/';
import LoadingSpinner from 'components/LoadingSpinner';

import CampaignsView from 'views/organization/campaigns/';
import CampaignMaterial from 'views/organization/campaign/material';
import CampaignResult from 'views/organization/campaign/result';
import CampaignResultAssignment from 'views/organization/campaign/result/assignment-name';
import CampaignAssignments from 'views/organization/campaign/assignments';
import CampaignReview from 'views/organization/campaign/review';
import CampaignReport from 'views/organization/campaign/report';
import CampaignSendReviewModal from 'views/organization/campaign/send-review';
import CampaignSendProductsModal from 'views/organization/campaign/send-products';
import CampaignInfluencerInformationModal from 'views/organization/campaign/influencer-information';
import CreateAssignmentModal from 'views/organization/campaign/assignments/create-assignment-modal';
import EditAssignmentModal from 'views/organization/campaign/assignments/edit-assignment-modal';
import EditProductModal from 'views/organization/campaign/products/edit-product-modal';
import CreateProductModal from 'views/organization/campaign/products/create-product-modal';
import EditPaymentModal from 'views/organization/campaign/products/edit-payment-modal';
import EditInvoiceModal from 'views/organization/campaign/products/edit-invoice-modal.js';
import CreatePaymentInvoiceModal from 'views/organization/campaign/products/create-payment-invoice-modal.js';
import PaymentTabs from 'views/organization/campaign/products/payment-tabs.js';
import CampaignProducts from 'views/organization/campaign/products';
import CampaignTodo from 'views/organization/campaign/todo';
import CampaignSettings from 'views/organization/campaign/settings';
import AddSelectionToList from 'views/organization/discover/my-lists/add-selection-to-list/index';
import LandingPageByInvite from 'views/landingpage/by-invite';
import AdminCampaignsReviewDecline from 'views/admin/campaigns/review/decline';
import ContentReview from 'views/Campaign/ContentReview';

// Lazy loaded
const CampaignFormContainer = React.lazy(() => import('components/CampaignForm/CampaignFormContainer'));
const CampaignDashboard = React.lazy(() => import('views/Campaign/Dashboard/CampaignDashboard'));
const InfluencerList = React.lazy(() => import('views/Campaign/Influencer/InfluencerList'));

const CampaignRoute = (props) => {
	const location = useLocation();
	const { path } = props.match;
	return (
		<Suspense fallback={<LoadingSpinner />}>
			<Switch>
				<UserOrganizationRoute exact path={`${path}`} component={CampaignsView} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/create`} component={CampaignFormContainer} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/shared`} component={CampaignsView} layout={OrganizationLayout} />
				<UserOrganizationRoute exact path={`${path}/:campaignId`} component={CampaignDashboard} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/preview/:influencerId`} component={LandingPageByInvite} layout={EmptyLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/material`} component={CampaignMaterial} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/influencers`} component={InfluencerList} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/assignments`} component={CampaignAssignments} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/products`} component={CampaignProducts} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/tasks`} component={CampaignTodo} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/settings`} component={CampaignSettings} layout={OrganizationLayout} />
				<UserOrganizationRoute exact path={`${path}/:campaignId/report`} component={CampaignReport} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/report/empty`} component={CampaignReport} layout={EmptyLayout} />
				<UserOrganizationRoute exact path={`${path}/:campaignId/result`} component={CampaignResult} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/result/assignment/:assignmentId`} component={CampaignResultAssignment} layout={OrganizationLayout} />
				<UserOrganizationRoute exact path={`${path}/:campaignId/edit`} component={CampaignFormContainer} layout={OrganizationLayout} />
				<UserOrganizationRoute path={`${path}/:campaignId/review/:reviewId?`} component={ContentReview} layout={OrganizationLayout} />

				<ModalRoute
					path={`${path}/:campaignId/preview/:reviewId/decline`}
					component={AdminCampaignsReviewDecline}
					parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/preview`}
					closeModal='closeModal'
				/>
			</Switch>
			<ModalRoute
				path={`${path}/:campaignId/report/add-top-performers`}
				component={CreateCampaignModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/report`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/influencers/add-to-list`}
				component={AddSelectionToList}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/influencers`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/assignments/create`}
				component={CreateAssignmentModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/assignments`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/assignments/:assignmentId/edit`}
				component={EditAssignmentModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/assignments`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/products/create`}
				component={CreateProductModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/products`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/products/create-payment`}
				component={PaymentTabs}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/products`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/products/create-payment-invoice`}
				component={CreatePaymentInvoiceModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/products`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/send-campaign-review`}
				component={CampaignSendReviewModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/send-products`}
				component={CampaignSendProductsModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/influencer-information`}
				component={CampaignInfluencerInformationModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/products/:campaignCompensationId/edit`}
				component={EditProductModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/products`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/products/:campaignCompensationId/edit-payment`}
				component={EditPaymentModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/products`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/:campaignId/products/:campaignCompensationId/edit-invoice`}
				component={EditInvoiceModal}
				parentPath={`/${location.pathname.split('/')[1]}/campaigns/${location.pathname.split('/')[3]}/products`}
				closeModal='closeModal'
			/>
		</Suspense>
	);
};

export default CampaignRoute;
