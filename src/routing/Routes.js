import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { UserOrganizationRoute, PublicRoute } from './layout-routes';
import EmptyLayout from 'layouts/empty/';
import OrganizationLayout from 'layouts/organization/';

import GenericNotFoundView from 'views/generic-not-found';
import UpdateEmail from 'views/update-email';
import DashboardView from 'views/organization/dashboard/';
import InboxView from 'views/organization/inbox/';
import SendSignUpWithInstagramRequest from 'views/landingpage/login/components/send-sign-up-with-instagram-request';
import SendSignInWithInstagramRequest from 'views/login/components/send-sign-in-with-instagram-request';

import LoginRoute from './LoginRoute';
import InvitesRoute from './InvitesRoute';
import InfluencerRoute from './InfluencerRoute';
import AdminRoute from './AdminRoute';
import SaleRoute from './SaleRoute';
import PublisherRoute from './PublisherRoute';
import CampaignRoute from './CampaignRoute';
import DiscoverRoute from './DiscoverRoute';
import SettingsRoute from './SettingsRoute';
import IntegratedInbox from 'views/IntegratedInbox';
import { UserDataLibraryRoute } from './layout-routes';
import DataLibrary from 'views/DataLibrary';

const Routes = () => {
	return (
		<>
			<Switch>
				<Route exact path='/' render={() => <Redirect to='/login' />} />
				<Route path='/login' component={LoginRoute} />
				<Route path='/invites' component={InvitesRoute} />
				<Route path='/influencer' component={InfluencerRoute} />
				<Route path='/admin' component={AdminRoute} />
				<Route path='/sale' component={SaleRoute} />
				<Route path='/p' component={PublisherRoute} />
				<Route path='/:organizationSlug/campaigns' component={CampaignRoute} />
				<Route path='/:organizationSlug/discover' component={DiscoverRoute} />
				<Route path='/:organizationSlug/settings' component={SettingsRoute} />

				<UserDataLibraryRoute path='/data-library' component={DataLibrary} layout={OrganizationLayout} />
				<UserOrganizationRoute path='/:organizationSlug/dashboard' component={DashboardView} layout={OrganizationLayout} />
				<UserOrganizationRoute path='/:organizationSlug/inbox' component={InboxView} layout={OrganizationLayout} />
				<UserOrganizationRoute
					path='/:organizationSlug/integrated-inbox/:campaignId?/:conversationId?'
					component={IntegratedInbox}
					layout={OrganizationLayout}
				/>
				<UserOrganizationRoute path='/:organizationSlug/update-email/:token' component={UpdateEmail} layout={EmptyLayout} />

				<PublicRoute component={GenericNotFoundView} layout={EmptyLayout} />
			</Switch>

			<ModalRoute path='/instagram/authorization/signup/' component={SendSignUpWithInstagramRequest} parentPath='/login' closeModal='closeModal' />
			<ModalRoute path='/instagram/authorization/login/' component={SendSignInWithInstagramRequest} parentPath='/login' closeModal='closeModal' />
		</>
	);
};

export default withRouter(Routes);
