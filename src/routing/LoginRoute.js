import { Switch } from 'react-router-dom';

import { PublicRoute } from './layout-routes';
import EmptyLayout from 'layouts/empty/';

import LoginView from 'views/login/';
import LoginForgotView from 'views/login/forgot';
import LoginResetView from 'views/login/reset';

const LoginRoute = (props) => {
	const { path } = props.match;
	return (
		<Switch>
			<PublicRoute exact path={path} component={LoginView} layout={EmptyLayout} />
			<PublicRoute path={`${path}/forgot`} component={LoginForgotView} layout={EmptyLayout} />
			<PublicRoute path={`${path}/reset/:token`} component={LoginResetView} layout={EmptyLayout} />
		</Switch>
	);
};

export default LoginRoute;
