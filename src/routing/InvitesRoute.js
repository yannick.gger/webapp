import { Switch, useLocation } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { PublicRoute } from './layout-routes';
import EmptyLayout from 'layouts/empty/';

import UpdateEmail from 'views/update-email';
import SignUpOrganizationWithToken from 'views/sign-up/organization-with-token/';
import SignUpPublisherWithToken from 'views/sign-up/publisher-with-token/';
import SignUpTeamMemberWithToken from 'views/sign-up/team-member-with-token/';
import InfluencerSignUpPage from 'views/landingpage/InfluencerSignUpPage';
import LandingPageByInvite from 'views/landingpage/by-invite';
import SorryToSeeYouGoView from 'views/sorrytoseeyougo/';

import CampaignInviteLogin from 'views/landingpage/login/';

const InvitesRoute = (props) => {
	const location = useLocation();
	const { path } = props.match;
	return (
		<>
			<Switch>
				<PublicRoute path={`${path}/org/:inviteToken`} component={SignUpOrganizationWithToken} layout={EmptyLayout} />
				<PublicRoute path={`${path}/influencer/sign-up/:inviteToken`} component={InfluencerSignUpPage} layout={EmptyLayout} />
				<PublicRoute path={`${path}/publisher/:inviteToken`} component={SignUpPublisherWithToken} layout={EmptyLayout} />
				<PublicRoute path={`${path}/team/:inviteToken`} component={SignUpTeamMemberWithToken} layout={EmptyLayout} />
				<PublicRoute path={`${path}/campaign/:inviteToken`} component={LandingPageByInvite} layout={EmptyLayout} />
			</Switch>

			<ModalRoute
				path={`${path}/campaign/:inviteToken/login`}
				component={CampaignInviteLogin}
				parentPath={`${path}/campaign/${location.pathname.split('/')[3]}`}
				closeModal='closeModal'
			/>
		</>
	);
};

export default InvitesRoute;
