import { Switch, useHistory, useLocation } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { UserInfluencerRoute, PublicRoute } from './layout-routes';
import InfluencerLayout from 'layouts/influencer/';
import EmptyLayout from 'layouts/empty';
import CampaignsContainer from 'views/influencer/campaigns/components/CampaignsContainer';

import LandingPageById from 'views/landingpage/by-id.tsx';
import InfluencerTasks from 'views/influencer/tasks';
import InfluencerSettings from 'views/influencer/settings';
import UpdateEmail from 'views/update-email';
import GenericNotFoundView from 'views/generic-not-found';

import InfluencerCampaignJoin from 'views/influencer/campaign-join';
import InfluencerAssignmentReport from 'views/influencer/assignment-report';
import InfluencerAssignmentInfo from 'views/influencer/assignment-info';
import InfluencerAssignmentReview from 'views/influencer/assignment-review/create-modal';
import EditInfluencerAssignmentReview from 'views/influencer/assignment-review/edit-modal';
import InfluencerCampaignInvoicing from 'views/influencer/campaign-invoicing';
import InfluencerCampaignInviteDeclineById from 'views/influencer/campaign-decline/by-id';
import InfluencerCampaignInviteDeclineByToken from 'views/influencer/campaign-decline/by-token';
import RequestScreenshot from 'views/influencer/assignment-report/request-insight.js';
import DeauthorizeInstagram from 'views/Deauthorize/Instagram/DeauthorizeInstagram';

const InfluencerRoute = (props) => {
	const location = useLocation();
	const history = useHistory();

	const { path } = props.match;

	return (
		<>
			<Switch>
				<UserInfluencerRoute exact path={`${path}/campaigns/:campaignId`} component={LandingPageById} layout={InfluencerLayout} />
				<UserInfluencerRoute path={`${path}/campaigns`} component={CampaignsContainer} layout={InfluencerLayout} />
				<UserInfluencerRoute path={`${path}/tasks`} component={InfluencerTasks} layout={InfluencerLayout} />
				<UserInfluencerRoute path={`${path}/settings`} component={InfluencerSettings} layout={InfluencerLayout} />
				<UserInfluencerRoute path={`${path}/instagram/deauthorize`} component={DeauthorizeInstagram} layout={InfluencerLayout} />
				<PublicRoute path={`${path}/update-email/:token`} component={UpdateEmail} layout={EmptyLayout} />
				<PublicRoute component={GenericNotFoundView} layout={InfluencerLayout} />
			</Switch>
			<ModalRoute
				path={`${path}/campaigns/:campaignId/join`}
				component={InfluencerCampaignJoin}
				parentPath={`/influencer/campaigns/${location.pathname.split('/')[3]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/dashboard/:campaignId/assignments/:assignmentId/report/modal`}
				component={InfluencerAssignmentReport}
				parentPath='/influencer/dashboard'
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/dashboard/:campaignId/assignments/:assignmentId/info/modal`}
				component={InfluencerAssignmentInfo}
				parentPath='/influencer/dashboard'
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/dashboard/:campaignId/assignments/:assignmentId/review/modal`}
				component={InfluencerAssignmentReview}
				parentPath='/influencer/dashboard'
				closeModal='closeModal'
			/>

			<ModalRoute
				path={`${path}/campaigns/:campaignId/assignments/:assignmentId/info/modal`}
				component={InfluencerAssignmentInfo}
				parentPath='/influencer/campaigns'
				closeModal='closeModal'
				props={{
					onCompleted: () => {
						history.push('/influencer/campaigns?instagramPostJobCompleted=true');
					}
				}}
			/>
			<ModalRoute
				path={`${path}/campaigns/:campaignId/assignments/:assignmentId/report/modal`}
				component={InfluencerAssignmentReport}
				parentPath='/influencer/campaigns'
				closeModal='closeModal'
				props={{
					onCompleted: () => {
						history.push('/influencer/campaigns?assignmentReportCompleted=true');
					}
				}}
			/>

			<ModalRoute
				path={`${path}/campaigns/:campaignId/assignments/:assignmentId/review/modal`}
				component={InfluencerAssignmentReview}
				parentPath='/influencer/campaigns'
				closeModal='closeModal'
				props={{
					onCompleted: () => {
						history.push('/influencer/campaigns?assignmentReviewCompleted=true');
					}
				}}
			/>
			<ModalRoute
				path={`${path}/campaigns/:campaignId/assignments/:assignmentId/edit-post-modal`}
				component={EditInfluencerAssignmentReview}
				parentPath='/influencer/campaigns'
				closeModal='closeModal'
				props={{
					onCompleted: () => {
						history.push('/influencer/campaigns?assignmentReviewCompleted=true');
					}
				}}
			/>

			<ModalRoute
				path={`${path}/campaigns/:campaignId/campaign-invoice-info/modal`}
				component={InfluencerCampaignInvoicing}
				parentPath='/influencer/campaigns'
				closeModal='closeModal'
			/>

			<ModalRoute
				path={`${path}/campaigns/:campaignId/decline/modal`}
				component={InfluencerCampaignInviteDeclineById}
				parentPath='/influencer/campaigns'
				closeModal='closeModal'
				stackOrder={1}
			/>
			<ModalRoute
				path={`${path}/campaigns/:campaignId/decline/modal`}
				component={InfluencerCampaignInviteDeclineById}
				parentPath='/influencer/dashboard'
				closeModal='closeModal'
				stackOrder={2}
			/>
			<ModalRoute
				path={`${path}/campaigns/:campaignId/decline/modal`}
				component={InfluencerCampaignInviteDeclineById}
				parentPath={`/influencer/campaigns/${location.pathname.split('/')[3]}`}
				closeModal='closeModal'
				stackOrder={3}
			/>
			<ModalRoute
				path={`${path}/campaign/:inviteToken/decline`}
				component={InfluencerCampaignInviteDeclineByToken}
				parentPath={`/invites/campaign/${location.pathname.split('/')[3]}`}
				closeModal='closeModal'
			/>
			<ModalRoute
				path={`${path}/campaigns/:campaignId/assignments/:assignmentId/report/request-screenshots`}
				component={RequestScreenshot}
				parentPath='/'
				closeModal='closeModal'
			/>
		</>
	);
};

export default InfluencerRoute;
