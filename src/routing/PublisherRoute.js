import { Switch, useLocation } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import { UserPublisherRoute, PublicRoute } from './layout-routes';
import PublisherLayout from 'layouts/publisher/';
import EmptyLayout from 'layouts/empty';

import PublisherSettingsView from 'views/publisher/settings';
import PublisherOrganizationsView from 'views/publisher/organizations';
import PublisherOrganizationInviteView from 'views/publisher/organization-invite';
import GenericNotFoundView from 'views/generic-not-found';

const PublisherRoute = (props) => {
	const location = useLocation();

	const { path } = props.match;
	return (
		<>
			<Switch>
				<UserPublisherRoute exact path={`${path}/:publisherSlug/settings`} component={PublisherSettingsView} layout={PublisherLayout} />
				<UserPublisherRoute path={`${path}/:publisherSlug/organizations`} component={PublisherOrganizationsView} layout={PublisherLayout} />

				<PublicRoute component={GenericNotFoundView} layout={EmptyLayout} />
			</Switch>
			<ModalRoute
				path={`${path}/:publisherSlug/organizations/invite`}
				component={PublisherOrganizationInviteView}
				parentPath={`${path}/${location.pathname.split('/')[2]}/organizations`}
				closeModal='closeModal'
			/>
		</>
	);
};

export default PublisherRoute;
