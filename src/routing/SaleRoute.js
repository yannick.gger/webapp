import { Switch } from 'react-router-dom';

import { UserAdminRoute, PublicRoute } from './layout-routes';
import OrganizationLayout from 'layouts/organization/';
import EmptyLayout from 'layouts/empty';

import SaleOrganizations from 'views/admin/organizations/organizations-granted.js';
import SaleOrganizationCampaigns from 'views/admin/organizations/organization-campaigns.js';
import GenericNotFoundView from 'views/generic-not-found';

const SaleRoute = (props) => {
	const { path } = props.match;
	return (
		<Switch>
			<UserAdminRoute exact path={`${path}/organizations`} exact component={SaleOrganizations} layout={OrganizationLayout} />
			<UserAdminRoute exact path={`${path}/organization/:organizationSlug`} component={SaleOrganizationCampaigns} layout={OrganizationLayout} />
			<PublicRoute component={GenericNotFoundView} layout={EmptyLayout} />
		</Switch>
	);
};

export default SaleRoute;
