// import original module declarations
import 'styled-components';
import { IThemeProps } from './styles/themes/types';

declare module 'styled-components' {
	export interface DefaultTheme extends IThemeProps {}
}
