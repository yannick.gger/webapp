import { combineReducers } from 'redux-immutable';
import currentUser from './current-user';

export default combineReducers({ currentUser });
