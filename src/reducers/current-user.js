import { fromJS } from 'immutable';
import { createReducer } from 'reduxsauce';
import { AUTH, CURRENTUSER, RESET } from 'actions/constants';

export const INITIAL_STATE = fromJS({
	info: {},
	flags: {
		isNew: true
	},
	init: false,
	isLoading: false
});

const userFeatureFlagFormat = (userFeatureFlags, extra = {}) =>
	(userFeatureFlags || []).reduce((obj, { featureFlag, enabled }) => ({ ...obj, [featureFlag.key]: enabled }), extra);

export const onReset = () => INITIAL_STATE;

export const onRefetchRequest = (state) => state.set('isLoading', true).set('info', fromJS({}));

export const onRefetchFailure = (state) => state.set('isLoading', false).set('info', fromJS({}));

export const onLoginSuccess = (state, { currentUser }) => {
	const { userFeatureFlags, ...others } = currentUser || {};

	return state
		.set('isLoading', false)
		.set('flags', fromJS(userFeatureFlagFormat(userFeatureFlags, state.get('flags').toJS())))
		.set('info', fromJS({ ...state.get('info').toJS(), ...others }));
};

export const onSearchFeatureSuccess = (state, { userFeatureFlags }) => state.set('flags', fromJS(userFeatureFlagFormat(userFeatureFlags, {})));

export const onSwitchFeatureSuccess = (state, { userFeatureFlag }) => state.setIn(['flags', userFeatureFlag.featureFlag.key], userFeatureFlag.enabled);

export const ACTION_HANDLERS = {
	[RESET]: onReset,
	[AUTH.AUTH_LOGIN_SUCCESS]: onLoginSuccess,
	[AUTH.AUTH_LOGIN_FAILURE]: onRefetchFailure,
	[AUTH.AUTH_REFETCH_SUCCESS]: onLoginSuccess,
	[AUTH.AUTH_REFETCH_REQUEST]: onRefetchRequest,
	[AUTH.AUTH_REFETCH_FAILURE]: onRefetchFailure,
	[CURRENTUSER.CURRENTUSER_SWITCH_FEATURE_SUCCESS]: onSwitchFeatureSuccess,
	[CURRENTUSER.CURRENTUSER_SEARCH_FEATURE_SUCCESS]: onSearchFeatureSuccess
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
