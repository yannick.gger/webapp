import { createStore, applyMiddleware } from 'redux';
import sagas from 'sagas';
import reducers from 'reducers';
import { fromJS } from 'immutable';
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

export default (client, initialState = {}) => {
	const store = createStore(reducers, fromJS(initialState), applyMiddleware(sagaMiddleware));

	store.runSaga = sagaMiddleware.run;
	store.runSaga(sagas(client));

	return store;
};
