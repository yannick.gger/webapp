import React, { Component } from 'react';
import { connect } from 'react-redux';

export const maptStateToProps = (state) => ({
	flags: state.getIn(['currentUser', 'flags']).toJS()
});

export default (flag = null, isRequired = true) => (WrappedComponent, ReplacedComponent = null, props = false) => {
	@connect(maptStateToProps)
	class NewComponent extends Component {
		render() {
			const { flags } = this.props;

			if (isRequired && flag && flags && !flags[flag]) {
				return !!ReplacedComponent && <ReplacedComponent {...this.props} flags={flags} />;
			}

			return <WrappedComponent {...this.props} flags={flags} />;
		}
	}

	return props && typeof props === 'object' ? <NewComponent {...props} /> : NewComponent;
};
