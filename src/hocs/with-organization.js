import React from 'react';
import { Query } from 'react-apollo';
import LoadingSpinner from 'components/LoadingSpinner';

import getOrganizationBySlugQuery from 'graphql/get-organization-by-slug.graphql';

/* Usage:
  import { withBugsnag } from "hocs"

  without slug automatically uses props.match.params.organizationSlug
    - export default withOrganization(OrganizationLayout)
  or with custom slug
    - export default withOrganization(OrganizationLayout, props => props.computedMatch.params.organizationSlug)

  Gives props: organization
*/

/**
 * @todo i18next
 */
export default function withOrganization(WrappedComponent, slugFn = (props) => props.match.params.organizationSlug) {
	return class extends React.Component {
		render() {
			if (slugFn(this.props) === undefined) {
				return <WrappedComponent {...this.props} />;
			}

			return (
				<Query query={getOrganizationBySlugQuery} variables={{ slug: slugFn(this.props) }}>
					{({ data, loading, error }) => {
						if (loading)
							return (
								<div className='text-center mt-30'>
									<LoadingSpinner size='md' position='center' />
								</div>
							);
						if (error) {
							return (
								<div className='text-center mt-30'>
									<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
										Could not load organization
									</h1>
								</div>
							);
						}
						return <WrappedComponent organization={data.organization} {...this.props} />;
					}}
				</Query>
			);
		}
	};
}
