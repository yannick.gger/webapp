import { withRouter, RouteComponentProps } from 'react-router-dom';

function withRouterWorkaround<P extends RouteComponentProps<any>>(Inner: React.ComponentType<P>) {
	const Wrapped: React.FunctionComponent<P> = (props: P) => <Inner {...props} />;
	Wrapped.displayName = `WithRouterWorkaround(${Inner.displayName || Inner.name || '?'})`;
	return withRouter(Wrapped);
}

export default withRouterWorkaround;
