import React from 'react';
import { JsonApiDocument, Store, Model } from 'json-api-models';
import { RouteComponentProps } from 'react-router-dom';
import useSWR, { Key, Fetcher } from 'swr';
import { AxiosRequestConfig } from 'axios';

import { createClient } from '../shared/ApiClient/ApiClient';
import LoadingSpinner from '../components/LoadingSpinner';

type RouteParams = {
	campaignId: string;
	organizationSlug: string;
};

type RequestDefinition = [string, AxiosRequestConfig<JsonApiDocument>?];

export interface WithCampaignProps {
	campaign: Model;
	models: Store;
	campaignUrl: string;
	isValidating: boolean;
	refresh: () => void;
}

const CAMPAIGN_INCLUDES = [
	'conversations',
	'latestMessage',
	'unreadMessages',
	'commissions',
	'commissions.campaignInstagramOwnerCommissions',
	'assignments',
	'assignments.groups',
	'assignments.groups.deadlines',
	'assignments.groups.campaignInstagramOwnerAssignments',
	'invites',
	'invites.campaignInstagramOwner',
	'invites.influencer',
	'campaignInstagramOwners',
	'campaignStatistics'
];

const fetcher: Fetcher<Pick<WithCampaignProps, 'campaign' | 'models'>> = async (id: Key) => {
	const client = createClient();
	// @ts-ignore
	const models = new Store();

	const requests: RequestDefinition[] = [
		[`/campaigns/${id}`, { params: { includes: CAMPAIGN_INCLUDES.join(',') } }],
		[`/campaigns/${id}/instagram-owners`, { params: { includes: 'influencer,campaignInstagramOwnerCommissions.commission,invite' } }],
		[`/campaigns/${id}/contents`],
		[`/campaigns/${id}/products?includes=influencers`],
		[`/assignment-reviews?includes=influencer,assignment`]
	];

	const responses = await Promise.all(requests.map(([url, config]) => client.get<JsonApiDocument>(url, config)));

	for (const { data } of responses) {
		try {
			models.sync(data as JsonApiDocument);
		} catch (error) {
			console.log(error)
		}
	}
	return {
		models,
		campaign: models.find('campaign', id as string)
	};
};

export default function withCampaign<T extends WithCampaignProps = WithCampaignProps>(WrappedComponent: React.ComponentType<T>) {
	// Try to create a nice displayName for React Dev Tools.
	const displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';

	// Creating the inner component. The calculated Props type here is the where the magic happens.
	const ComponentWithCampaign = (props: Omit<T, keyof WithCampaignProps> & RouteComponentProps<RouteParams>) => {
		const { campaignId, organizationSlug } = props.match.params;
		const { data, error, mutate, isValidating } = useSWR(campaignId, fetcher);
		const campaignUrl = `/${organizationSlug}/campaigns/${campaignId}`;

		if (error) {
			return <p>{error.message}</p>;
		}

		// eslint-disable-next-line prettier/prettier
		if (!error && !data?.campaign) {
			return <LoadingSpinner />;
		}

		const campaignProps = {
			campaignUrl,
			refresh: () => mutate(campaignId),
			isValidating,
			...data
		};

		// props comes afterwards so the can override the default ones.
		return <WrappedComponent {...campaignProps} {...props as T} />;
	};

	ComponentWithCampaign.displayName = `withCampaign(${displayName})`;

	return ComponentWithCampaign;
}
