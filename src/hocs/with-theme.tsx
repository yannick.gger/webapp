import { useContext, useEffect } from 'react';
import { ThemeContext } from 'contexts';

/**
 * withTheme HOC
 * @param Component
 * @param theme
 * @returns {(props: P) => JSX.Element}
 */
function withTheme<P>(Component: React.ComponentType<P>, theme: string): (props: P) => JSX.Element {
	const ThemedComponent = (props: P) => {
		const themeContext = useContext(ThemeContext);

		useEffect(() => {
			themeContext.setTheme(theme);
			return () => themeContext.setTheme('default');
		}, []);

		return <ThemeContext.Consumer>{() => <Component {...props} />}</ThemeContext.Consumer>;
	};

	return ThemedComponent;
}

export default withTheme;
