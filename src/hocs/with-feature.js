import React, { Component } from 'react';
import qs from 'query-string';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { onSwitchFeatureRequest, onSearchFeatureRequest } from 'actions/current-user';
import { hasGhostToken } from 'services/auth-service';

export const maptStateToProps = (state) => ({
	flags: state.getIn(['currentUser', 'flags']).toJS()
});

export const maptDispatchToProps = (dispatch) => ({
	onRefetch: () => dispatch(onSearchFeatureRequest()),
	onSwitchFeature: (params) => dispatch(onSwitchFeatureRequest(params))
});

export default (WrappedComponent) => {
	@withRouter
	@connect(
		maptStateToProps,
		maptDispatchToProps
	)
	class NewComponent extends Component {
		componentDidMount() {
			const { location } = this.props;
			const { enableFeature, disableFeature } = qs.parse(location.search);

			if ((enableFeature || disableFeature) && enableFeature !== disableFeature) {
				enableFeature && this.props.onSwitchFeature({ enable: true, key: enableFeature });
				disableFeature && this.props.onSwitchFeature({ enable: false, key: disableFeature });
			} else {
				!hasGhostToken() && this.props.onRefetch();
			}
		}

		render() {
			return <WrappedComponent {...this.props} />;
		}
	}

	return NewComponent;
};
