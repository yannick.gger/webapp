import { ICollabsResponse } from 'services/Response.types';

export interface IAssignmentReviewService {
	getReviews: (includes?: Array<string>) => Promise<ICollabsResponse>;
	ApproveReview: (reviewId: string) => Promise<ICollabsResponse>;
	RejectReview: (reviewId: string) => Promise<ICollabsResponse>;
	Comments: (reviewId: string, text: string) => Promise<ICollabsResponse>;
}
