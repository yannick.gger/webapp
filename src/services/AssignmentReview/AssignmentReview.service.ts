import axios, { AxiosError, AxiosResponse } from 'axios';
import { ICollabsResponse } from 'services/Response.types';
import Client from 'shared/ApiClient';
import { IAssignmentReviewService } from './types';

class AssignmentReviewService implements IAssignmentReviewService {
	ENDPOINT_URL: string;

	constructor() {
		this.ENDPOINT_URL = '/assignment-reviews';
	}

	async getReviews(includes?: string[] | undefined): Promise<ICollabsResponse> {
		let result: ICollabsResponse = {};
		await Client('get', `${this.ENDPOINT_URL}?includes=${(includes && includes.join(',')) || ''}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				}
			});

		return result;
	}

	async ApproveReview(reviewId: string): Promise<ICollabsResponse> {
		let result: ICollabsResponse = {};
		await Client('post', `${this.ENDPOINT_URL}/${reviewId}/approve`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				}
			});

		return result;
	}

	async RejectReview(reviewId: string): Promise<ICollabsResponse> {
		let result: ICollabsResponse = {};
		await Client('post', `${this.ENDPOINT_URL}/${reviewId}/reject`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				}
			});

		return result;
	}
	async Comments(reviewId: string, text: string): Promise<ICollabsResponse> {
		let result: ICollabsResponse = {};
		await Client('post', `${this.ENDPOINT_URL}/${reviewId}/comments?include=comments.user`, { text: text })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				}
			});

		return result;
	}
}

export default new AssignmentReviewService();
