import { KEY_REFERRAL_URL } from 'constants/localStorage-keys';
import { redirectToHome } from 'services/auth-service';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';

class ReferralUrlService {
	storage: BrowserStorage;

	constructor() {
		this.storage = new BrowserStorage(StorageType.SESSION);
	}

	set(url: string) {
		this.storage.setItem(KEY_REFERRAL_URL, url);
	}

	get() {
		return this.storage.getItem(KEY_REFERRAL_URL);
	}

	remove() {
		return this.storage.removeItem(KEY_REFERRAL_URL);
	}

	redirect(history: History) {
		const referralUrl = this.get();
		if (referralUrl) {
			this.remove();
			location.replace(referralUrl);
		} else {
			redirectToHome({ history: history });
		}
	}
}

export default new ReferralUrlService();
