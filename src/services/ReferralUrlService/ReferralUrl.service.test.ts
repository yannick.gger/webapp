import ReferralUrlService from './ReferralUrl.service';
//import { redirectToHome } from 'services/auth-service';


type windowMirror = { location?: any; history: any };

describe('Referral url service', () => {
	const _window: windowMirror = window;

	beforeAll(() => {
		delete _window.location;
		_window.location = { replace: jest.fn() };
	});

	it('should be created', () => {
		expect(ReferralUrlService).toBeTruthy();
	});

	it('should set url into session storage', () => {
		const url = 'https://test.me';
		expect.assertions(1);
		ReferralUrlService.set(url);

		expect(ReferralUrlService.get()).toEqual(url);
	});

	it('should remove url from session storage', () => {
		const url = 'https://test.me';
		expect.assertions(1);
		ReferralUrlService.set(url);
		ReferralUrlService.remove();

		expect(ReferralUrlService.get()).toEqual(null);
	});

	it('should redirect and remove from session storage', () => {
		const url = 'https://test.me';
		ReferralUrlService.set(url);
		ReferralUrlService.redirect(_window.history);

		expect(_window.location.replace).toHaveBeenCalled();
		expect(ReferralUrlService.get()).toEqual(null);
	});
});
