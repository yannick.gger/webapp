import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { ICollabsResponse, StatusCode } from 'services/Response.types';
import { createClient } from 'shared/ApiClient/ApiClient';
import { ICampaignsService } from './types';

class CampaignsService implements ICampaignsService {
	ENDPOINT_URL: string;
	private client: AxiosInstance;

	constructor() {
		this.ENDPOINT_URL = '/campaigns';
		this.client = createClient();
	}

	async getCampaigns(includes?: Array<string>) {
		let result: ICollabsResponse = {};
		// @todo remove latestMessages as default
		const join = includes ? includes.join(',') : 'latestMessage';

		await this.client
			.get(`${this.ENDPOINT_URL}?includes=${join}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				}
			});

		return result;
	}

	async getCampaign(id: string, includes?: string, cancelSignal?: AbortSignal) {
		let result: ICollabsResponse = {};
		const url = `${this.ENDPOINT_URL}/${id}${includes ? `?includes=${includes}` : ''}`;
		await this.client
			.get(url, { signal: cancelSignal })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				}
			});

		return result;
	}

	async getCampaignUsers(campaignId: number) {
		let result: ICollabsResponse = {};

		await this.client
			.get(`${this.ENDPOINT_URL}/${campaignId}/users`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async getCampaignInstagramOwners(campaignId: number) {
		let result: ICollabsResponse = {};

		await this.client
			.get(`${this.ENDPOINT_URL}/${campaignId}/instagram-owners`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async getCampaignConversations(conversationId: number, username?: string, signal?: AbortSignal) {
		let result: ICollabsResponse = {};

		await this.client
			.get(`${this.ENDPOINT_URL}/${conversationId}/conversations${username ? `?username=${username}` : ''}`, { signal })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async getCampaignMessages(campaignId: number) {
		let result: ICollabsResponse = {};

		await this.client
			.get(`${this.ENDPOINT_URL}/${campaignId}/messages`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async uploadCampaignCoverImage(media: any) {
		let result;
		const formData = new FormData();
		if (media) {
			formData.append('file', media);
		}
		await this.client
			.post('/campaign-cover-photos', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				throw new Error('error', err);
			});

		return result;
	}

	async setCampaignImage(campaignId: string, imageId: string) {
		let result;
		await this.client
			.patch(`${this.ENDPOINT_URL}/${campaignId}`, { campaignCoverPhoto: imageId })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				throw new Error('error', err);
			});

		return result;
	}

	async getCampaignCoverImages() {
		let result;

		await this.client
			.get('/campaign-cover-photos')
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				throw new Error('error', err);
			});

		return result;
	}

	async deleteCampaign(campaignId: string | number) {
		let result;
		await this.client
			.delete(`${this.ENDPOINT_URL}/${campaignId}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				if (response.status === StatusCode.DELETE_SUCCESS) {
					result = StatusCode.DELETE_SUCCESS;
				}
			})
			.catch((err: Error | AxiosError) => {
				throw new Error('error', err);
			});

		return result;
	}

	async deleteCampaignInfluencer(link: string) {
		let result;
		await this.client
			.delete(`${link}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				if (response.status === StatusCode.DELETE_SUCCESS) {
					result = StatusCode.DELETE_SUCCESS;
				}
			})
			.catch((err: Error | AxiosError) => {
				throw new Error('error', err);
			});

		return result;
	}
}

export default new CampaignsService();
