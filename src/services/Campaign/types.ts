import { ICollabsResponse } from 'services/Response.types';

export interface ICampaignsService {
	getCampaigns: (includes?: Array<string>) => Promise<ICollabsResponse>;
	getCampaign: (id: string, includes?: string, cancelSignal?: any) => Promise<ICollabsResponse>;
	getCampaignUsers: (campaignId: number) => Promise<ICollabsResponse>;
	getCampaignInstagramOwners: (campaignId: number) => Promise<ICollabsResponse>;
	getCampaignConversations: (conversationId: number, username?: string) => Promise<ICollabsResponse>;
	deleteCampaign: (campaignId: string | number) => Promise<any>;
}

export interface ICampaign {
	id: number;
	name: string;
	coverPhotoUrl: string;
	latestMessage: latestMessage;
	unreadMessages: any; // @todo type
}

export interface ICampaignUser {
	id: number;
	avatar: string;
	name: string;
	username: string;
	conversation?: Conversation;
	latestMessage?: latestMessage;
	unreadMessages?: any; // @todo type
	messages?: any;
}

export type latestMessage = {
	date: string;
	rawDate: Date;
	message: string;
	readAt: string;
};

export type Conversation = {
	id: string;
	createdAt: string;
	blast: boolean;
	campaignId: number;
	createdBy: number;
};

export type Message = {
	userId: number;
	username: string;
	avatar: string;
	date: string;
	message: string;
};
