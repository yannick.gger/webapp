import axios, { AxiosError, AxiosResponse } from 'axios';
import { ICollabsResponse } from 'services/Response.types';
import Client from 'shared/ApiClient';

interface ICreateList {
	name: string;
	folder: string;
	collabsIds?: string[];
	users?: string[];
}

class ListsService {
	LISTS_ENDPOINT: string;
	FOLDERS_ENDPOINT: string;

	constructor() {
		this.LISTS_ENDPOINT = '/lists';
		this.FOLDERS_ENDPOINT = '/folders';
	}

	/* 
FOLDERS
	*/
	async getFolders() {
		let folders: ICollabsResponse = {};
		let lists: ICollabsResponse = {};

		await Client('get', `${this.FOLDERS_ENDPOINT}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				folders = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					folders = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});
		await Client('get', `${this.LISTS_ENDPOINT}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				lists = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					lists = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});
		return { folders, lists };
	}

	createFolder = async (parentId: string, name: string) => {
		let result: ICollabsResponse = {};

		await Client('post', `${this.FOLDERS_ENDPOINT}`, { name: name, parent: parentId })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	};
	updateFolder = async (folderId: string, name: string, parentId?: string): Promise<any> => {
		let result: any;
		await Client('patch', `${this.FOLDERS_ENDPOINT}/${folderId}`, { name: name, parent: parentId })
			.then((response: AxiosResponse<any>) => {
				result = { data: response.data, included: response.data.included };
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};
	deleteFolder = async (folderId: string): Promise<any> => {
		let result: any;
		await Client('delete', `${this.FOLDERS_ENDPOINT}/${folderId}`)
			.then((response: AxiosResponse<any /* DashboardData */>) => {
				console.log('deleted response: ', response);
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};
	async getFolder(id: string) {
		let result: ICollabsResponse = {};

		await Client('get', `${this.FOLDERS_ENDPOINT}/${id}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included }; // todo: add errors
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});
		return result;
	}
	/* 
LIST
	*/
	async getLists() {
		let result: ICollabsResponse = {};

		await Client('get', `${this.LISTS_ENDPOINT}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}
	async getList(folderId: string) {
		let result: ICollabsResponse = {};

		await Client('get', `${this.LISTS_ENDPOINT}/${folderId}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});
		console.log(result);
		return result;
	}

	async createList(name: string, parentFolderId: string) {
		let result: ICollabsResponse = {};
		await Client('post', `${this.LISTS_ENDPOINT}`, { name: name, folder: parentFolderId })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});
		console.log(result);
		return result;
	}
	async addInfluencerToList(influencers: string[], parentFolderId: string) {
		let result;
		await Client('post', `${this.LISTS_ENDPOINT}/${parentFolderId}/influencers`, { collabsIds: influencers })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.status;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});
		return result;
	}
}

export default new ListsService();
