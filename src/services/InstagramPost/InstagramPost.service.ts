import axios, { AxiosError, AxiosResponse } from 'axios';
import { ICollabsResponse } from 'services/Response.types';
import Client from 'shared/ApiClient';
import { IInstagramPostService, InstagramPost } from './types';

class InstagramPostService implements IInstagramPostService {
	ENDPOINT_URL: string;

	constructor() {
		this.ENDPOINT_URL = '/instagram-posts';
	}

	async getPost(postId: number) {
		let result: ICollabsResponse = {};

		await Client('get', `${this.ENDPOINT_URL}/${postId}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async patchPost(postId: number, body: InstagramPost) {
		let result: ICollabsResponse = {};

		await Client('patch', `${this.ENDPOINT_URL}/${postId}`, body)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}
}

export default new InstagramPostService();
