import { ICollabsResponse } from 'services/Response.types';

export interface IInstagramPostService {
	patchPost: (postId: number, body: InstagramPost) => Promise<ICollabsResponse>;
	getPost: (postId: number) => Promise<ICollabsResponse>;
}

export type InstagramPost = {
	commentsCount: number;
	impressions: number;
	likeCount: number;
	reach: number;
	shortcode: string;
};
