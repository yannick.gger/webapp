import React from 'react';
import { notification, Modal } from 'antd';
import getAdminCampaignReviewQuery from 'graphql/get-admin-campaign-review.graphql';
export { default as updateAdminCampaignReviewMutation } from 'graphql/update-admin-campaign-review.graphql';

export const acceptAdminCampaignReview = (updateAdminCampaignReview, id, numSimilar) => {
	Modal.confirm({
		title: 'Got your final decision?',
		okText: 'Yes',
		okType: 'success',
		cancelText: 'No',
		onOk: () => {
			const variables = {
				id,
				action: 'approved'
			};

			updateAdminCampaignReview({
				variables,
				awaitRefetchQueries: true,
				refetchQueries: [{ query: getAdminCampaignReviewQuery }]
			})
				.then(() => {
					notification.success({
						message: 'Campaign approved',
						description: 'The campaign has been reviewed and approved'
					});
				})
				.catch((err) => {
					notification.error({
						message: 'Campaign approved',
						description: err.toString()
					});
				});
		}
	});
};

export const adminSendReportToCustomer = (sendReportToCustomer, id) => (e) => {
	e.preventDefault();

	Modal.confirm({
		title: 'Send report to customer?',
		okText: 'Yes',
		okType: 'success',
		cancelText: 'No',
		onOk: () => {
			const variables = {
				id
			};
			sendReportToCustomer({ variables })
				.then(() => {
					notification.success({
						message: 'Campaign report sent',
						description: 'The campaign report has been sent'
					});
				})
				.catch((err) => {
					notification.error({
						message: 'Campaign report not sent',
						description: err.toString()
					});
				});
		}
	});
};

export const adminMarkReportAsDoneManually = (action, id) => (e) => {
	e.preventDefault();

	Modal.confirm({
		title: 'Mark report as done manually?',
		okText: 'Yes',
		okType: 'success',
		cancelText: 'No',
		onOk: () => {
			const variables = {
				id
			};
			action({ variables })
				.then(() => {
					notification.success({
						message: 'Campaign report marked as done',
						description: 'The campaign report has been marked as done manually'
					});
				})
				.catch((err) => {
					notification.error({
						message: 'Campaign report not marked as done manually',
						description: err.toString()
					});
				});
		}
	});
};
