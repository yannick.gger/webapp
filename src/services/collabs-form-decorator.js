import React from 'react';
import { Form } from 'antd';
import merge from 'deepmerge';

/* Example usage
import React, { Component } from "react"
import { Form, Input } from "antd"

import withCollabsForm from "services/collabs-form-decorator"

class AssignmentForm extends Component {
  render() {
    const { form } = this.props
    const { getFieldDecorator } = form

    return (
      <Form>
        <Form.Item>
          {getFieldDecorator("assignment.name")(<Input />)}
        </Form.Item>
      </Form>
    )
  }
}

export default withCollabsForm(AssignmentForm)

<AssignmentForm
  fields={{assignment: assignment}}
  wrappedComponentRef={this.saveFormRef}
/>

*/

// Iterates through an object and creates form fields for the fields.
// { assignment: {name: "patrick"} } would create form field "assignment.name". Which
// is then used in the view
function internalMapPropsToFields(propFields, parentNameRef = '') {
	// parentNameRef controls when an object is sent in and makes it into dotted $name
	// { assignment: { name: "Patrick" } } -> getFieldDecorator("assignment.name")
	let formFields = {};

	Object.keys(propFields || {}).forEach((propFieldKey) => {
		if (typeof propFields[propFieldKey] === 'object' && propFields[propFieldKey].value === undefined && propFields[propFieldKey].dirty === undefined) {
			parentNameRef = propFieldKey === '' ? parentNameRef : `${parentNameRef}.${propFieldKey}`;
			const res = internalMapPropsToFields(propFields[propFieldKey], parentNameRef);
			parentNameRef = res.parentNameRef;
			formFields[propFieldKey] = res.formFields;
		} else {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		}
	});

	return { formFields, parentNameRef };
}

function create(args = {}) {
	return Form.create({
		...args,
		onFieldsChange: (props, changedFields) => {
			if (props.onChange) {
				props.onChange(changedFields);
			}
		},
		mapPropsToFields: (props) => {
			const propFields = props.fields || {};
			return internalMapPropsToFields(propFields).formFields;
		}
	});
}

function withCollabsForm(WrappedComponent, { customFieldMap } = {}) {
	return class extends React.Component {
		constructor(props) {
			super(props);
			this.newForm = create()(WrappedComponent);
			this.state = {
				fields: {}
			};
		}

		/* Takes updated props.fields and makes them in to an object. This to perserve state
      of form validation and field values.
      getDerivedStateFromProps({fields: {assignment: {name: "Patrick"}}}, {}) ->
      { fields: { assignment: { name: { value: "Patrick" } } } }
    */
		static getDerivedStateFromProps = (props, state) => {
			if (Object.keys(state.fields).length !== 0) {
				return state;
			}

			const recursiveValueMapping = (propsFields) => {
				let newFields = {};

				Object.keys(propsFields || {}).forEach((key) => {
					if (typeof propsFields[key] === 'object' && propsFields[key] !== null) {
						newFields[key] = recursiveValueMapping(propsFields[key]);
					} else {
						newFields[key] = {
							value: propsFields[key]
						};
					}
				});

				return newFields;
			};

			const newFields = recursiveValueMapping(props.fields);

			/* customFieldMap allows for custom mapping of object fields into form fields.
        Example:
          export default withCollabsForm(AssignmentForm, {
            customFieldMap: {
              dateRange: fields => [apiToMoment(fields.startTime), apiToMoment(fields.endTime)]
            }
          })
      */
			if (customFieldMap) {
				Object.keys(customFieldMap).forEach((key) => (newFields[key] = { value: customFieldMap[key](props.fields) }));
			}

			return { fields: { ...newFields, ...state.fields } };
		};

		handleFormChange = (changedFields) => {
			console.dir(this.state.fields);
			console.dir(merge(this.state.fields, changedFields));
			this.setState((prevState) => {
				const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray;
				return {
					...prevState,
					fields: merge(prevState.fields, changedFields, { arrayMerge: overwriteMerge })
				};
			});
		};

		render() {
			const NewForm = this.newForm;
			return <NewForm {...{ ...this.props, fields: this.state.fields }} onChange={this.handleFormChange} />;
		}
	};
}

export default withCollabsForm;
