import { ICollabsResponse } from 'services/Response.types';

export interface IConversationsService {
	getConversations: () => Promise<ICollabsResponse>;
	createConversation: (data: createConversationData) => Promise<ICollabsResponse>;
	createMessage: (conversationId: string, message: string) => Promise<ICollabsResponse>;
	getMessages: (conversationId: string) => Promise<ICollabsResponse>;
	setReadAtByMessageId: (conversationId: string, conversationMessageId: string) => Promise<ICollabsResponse>;
	getUnreadCount: () => Promise<ICollabsResponse>;
}

export type createConversationData = {
	message: string;
	users?: Array<string>;
	influencer: string;
	campaign: string;
};
