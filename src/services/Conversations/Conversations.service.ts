import axios, { AxiosError, AxiosResponse } from 'axios';
import { ICollabsResponse } from 'services/Response.types';
import Client from 'shared/ApiClient';
import { createConversationData, IConversationsService } from './types';

class ConversationsService implements IConversationsService {
	ENDPOINT_URL: string;

	constructor() {
		this.ENDPOINT_URL = '/conversations';
	}

	async getConversations() {
		let result: ICollabsResponse = {};

		await Client('get', `${this.ENDPOINT_URL}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async createConversation(data: createConversationData) {
		let result: ICollabsResponse = {};

		await Client('post', `${this.ENDPOINT_URL}`, data)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async createMessage(conversationId: string, message: string) {
		let result: ICollabsResponse = {};

		await Client('post', `${this.ENDPOINT_URL}/${conversationId}`, { message: message })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async getMessages(conversationId: string) {
		let result: ICollabsResponse = {};

		await Client('get', `${this.ENDPOINT_URL}/${conversationId}/messages`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async setReadAtByMessageId(conversationId: string, conversationMessageId: string) {
		let result: ICollabsResponse = {};

		await Client('post', `${this.ENDPOINT_URL}/${conversationId}/messages/${conversationMessageId}/reads`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async createBlastConversation(data: createConversationData) {
		let result: ICollabsResponse = {};

		await Client('post', `${this.ENDPOINT_URL}/blasts`, data)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async getUnreadCount() {
		let result: ICollabsResponse = {};

		await Client('get', `${this.ENDPOINT_URL}/unread/total`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}
}

export default new ConversationsService();
