import { message } from 'antd';
import camelcaseKeys from 'camelcase-keys-deep';
import jwtDecode from 'jwt-decode';
import { GHOST_USER_EMAIL } from 'constants/localStorage-keys';
import CollabsAuthService from './Authentication/Collabs-api';
import { ToastContext } from 'contexts';
import { useContext } from 'react';

export function getToken() {
	const ghostToken = localStorage.getItem('ghostToken');
	return ghostToken || localStorage.getItem('token');
}

export function hasGhostToken() {
	return !!localStorage.getItem('ghostToken');
}

export function getOriginalToken() {
	return localStorage.getItem('token');
}

export function getTokenPayload() {
	if (!getToken()) {
		return null;
	}

	const payload = jwtDecode(getToken());
	updateFullstoryTracking(payload);
	return camelcaseKeys(payload);
}

export function getOriginalTokenPayload() {
	if (!getOriginalToken()) {
		return null;
	}

	const payload = jwtDecode(getOriginalToken());
	updateFullstoryTracking(payload);
	return camelcaseKeys(payload);
}

/**
 * This check if ghost user is admin
 * @deprecated use isImpersonating
 */
export function checkGhostUser() {
	const dataUser = getTokenPayload();
	const { ghostUser = {} } = dataUser;

	if (Object.keys(ghostUser).length) {
		return ghostUser.isAdmin;
	}

	return false;
}

export function setToken(token, options = {}) {
	localStorage.setItem('token', token);
	updateFullstoryTracking(jwtDecode(token));
	if (options.bugsnagClient) {
		updateBugsnagTracking(jwtDecode(token), options.bugsnagClient);
	}
}

export function setGhostToken(token, startUrl) {
	localStorage.setItem('ghostStartUrl', startUrl);
	localStorage.setItem('ghostToken', token);
}

function updateFullstoryTracking(tokenPayload) {
	if (window.FS) {
		localStorage.setItem('fullstory-recording', true);
		window.FS.restart();
	}
	if (window.FS && !hasGhostToken()) {
		window.FS.identify(tokenPayload.user ? tokenPayload.user.id : '', {
			displayName: tokenPayload.user ? tokenPayload.user.name : '',
			email: tokenPayload.user ? tokenPayload.user.email : '',
			organizationName_str:
				tokenPayload.user && tokenPayload.user.organizations && tokenPayload.user.organizations[0] ? tokenPayload.user.organizations[0].slug : ''
		});
	}
}

function updateBugsnagTracking(tokenPayload, bugsnagClient) {
	if (!tokenPayload.user) {
		return;
	}
	bugsnagClient.user = {
		id: tokenPayload.user.id,
		email: tokenPayload.user.email,
		name: tokenPayload.user.name
	};
}

export function isLoggedIn() {
	return !!getToken();
}

export function signOut(apolloClient, history) {
	if (hasGhostToken()) {
		signOutAsGhost(apolloClient, history);
	} else {
		localStorage.removeItem('token');
		localStorage.removeItem('user');
		CollabsAuthService.clearAll();
		apolloClient.resetStore();
		history.push('/login');
	}
}

/**
 * @deprecated use stopImpersonating instead
 */
export function signOutAsGhost(apolloClient, history) {
	stopImpersonating(apolloClient, history);
}

export function stopImpersonating(apolloClient, history) {
	const ghostStartUrl = localStorage.getItem('ghostStartUrl');
	localStorage.removeItem('ghostToken');
	localStorage.removeItem('ghostStartUrl');
	localStorage.removeItem('ghostUser');
	localStorage.removeItem(GHOST_USER_EMAIL);
	apolloClient.resetStore();
	if (ghostStartUrl) {
		history.push(ghostStartUrl);
	} else {
		history.push('/admin/organizations');
	}

	// TODO tell PHP API to stop impersonating
	// TODO update CollabsAuthService with new token
}

export function forceSignOut(apolloClient, history) {
	signOut(apolloClient, history);
	const { addToast } = useContext(ToastContext);
	addToast({ id: 'logged-out', mode: 'error', message: 'You have been signed out' });
}

export function isInfluencer() {
	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}

	return tokenPayload.user.instagramOwnerId;
}

export function isPublisher() {
	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}
	return tokenPayload.user.publishers && tokenPayload.user.publishers.length > 0;
}

export function isAdminOrGhostAdmin() {
	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}
	return isAdmin() || checkGhostUser();
}

export function organizationFromSlug(organizationSlug) {
	const tokenPayload = getTokenPayload();
	if (!tokenPayload || !tokenPayload.user || !tokenPayload.user.organizations) {
		return false;
	}

	return tokenPayload.user.organizations.find((organization) => {
		return organization.slug === organizationSlug;
	});
}

export function hasOrganizations() {
	return !!firstOrganization();
}

export function checkAccessToOrganization({ history, match }) {
	if (!isLoggedIn()) return false;

	if (isAdmin() && match.path === '/:organizationSlug/campaigns/:campaignId/preview') {
		return true;
	}

	if (match.params.organizationSlug === 'organization-redirect') {
		history.push(match.path.replace(':organizationSlug', firstOrganizationSlug()));

		return true;
	}

	if (!getOrganizations().find((organization) => organization.slug === match.params.organizationSlug)) {
		const { addToast } = useContext(ToastContext);
		addToast({ id: 'no-organization-access', mode: 'error', message: 'You do not have access to this organization or organization does not exist' });
		redirectToHome({ history });

		return false;
	}

	return true;
}

export function checkAccessToPublisher({ history, match }) {
	if (!isLoggedIn()) return false;

	if (!getPublishers().find((publisher) => publisher.slug === match.params.publisherSlug)) {
		const { addToast } = useContext(ToastContext);
		addToast({ id: 'no-publisher-permissions', mode: 'error', message: 'You do not have access to this publisher or publisher does not exist' });
		redirectToHome({ history });

		return false;
	}

	return true;
}

/**
 * @deprecated use isSuperAdmin
 */
export function isAdmin() {
	if (!isLoggedIn()) return false;

	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}

	return tokenPayload.user.isAdmin === true;
}

export function isSuperAdmin() {
	const loggedInUser = CollabsAuthService.getCollabsUserObject();

	return true === loggedInUser.permissions.isSuperAdmin;
}

export function isImpersonating() {
	return checkGhostUser();

	// TODO let the loggedInUser help with the logic
	// const loggedInUser = CollabsAuthService.getCollabsUserObject();
	// return true === loggedInUser.permissions.isImpersonating;
}

export function isLoginAdminUser() {
	if (!isLoggedIn()) return false;

	const tokenPayload = getOriginalTokenPayload();
	if (!tokenPayload) {
		return false;
	}

	return tokenPayload.user.isAdmin === true;
}

export function isSaler() {
	if (!isLoggedIn()) return false;

	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}

	return tokenPayload.user.isSaleUser === true;
}

export function isTester() {
	if (!isLoggedIn()) return false;

	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}

	return tokenPayload.user.isTestUser === true;
}

export function ghostUserIsSaler() {
	if (!isLoggedIn()) return false;

	if (!hasGhostUser()) return false;

	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}

	return tokenPayload.user.isSaleUser === true;
}

export function hasGhostUser() {
	if (!isLoggedIn()) return false;

	const tokenPayload = getTokenPayload();
	if (!tokenPayload) {
		return false;
	}

	if (tokenPayload.ghostUser) {
		return true;
	}

	return false;
}

export function checkGhostUserIsSaler() {
	return hasGhostUser() && ghostUserIsSaler();
}

export function checkAdmin({ history }) {
	if (!isLoggedIn()) return false;

	if (isAdmin() || isSaler()) {
		return true;
	} else {
		const { addToast } = useContext(ToastContext);
		addToast({ id: 'no-admin-permissions', mode: 'error', message: 'You need to be admin to access this page' });
		redirectToHome({ history });

		return false;
	}
}

export function redirectToHome({ history }) {
	history.push(getHomePath());
}

export function getHomePath() {
	if (firstPublisher()) {
		return `/p/${firstPublisherSlug()}/organizations`;
	} else if (firstOrganization()) {
		return `/${firstOrganizationSlug()}/dashboard`;
	} else {
		return `/influencer/campaigns`;
	}
}

export function getOrganizations() {
	return getTokenPayload().user ? getTokenPayload().user.organizations || getTokenPayload().organizations || [] : [];
}

export function getOrganizationBySlug(slug) {
	return getOrganizations().find((o) => o.slug === slug);
}

export function firstOrganization() {
	return getOrganizations()[0];
}

export function firstOrganizationSlug() {
	const organization = firstOrganization();
	return organization ? organization.slug : null;
}

export function getPublishers() {
	return getTokenPayload().user ? getTokenPayload().user.publishers || [] : [];
}

export function firstPublisher() {
	return getPublishers()[0];
}

export function firstPublisherSlug() {
	const publisher = firstPublisher();
	return publisher && publisher.slug;
}
