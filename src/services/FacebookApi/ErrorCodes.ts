/**
 * Error messages for Instagram Insights
 * @param {string} errorCode
 * @param {number} subErrorCode
 * @returns {string} Error message
 */
export const getIGIErrorMessage = (errorCode: string, subErrorCode: number): string => {
	/**
	 * @todo Handle when user enter wrong link
	 */
	let errorMessage = '';
	switch (`${errorCode}${subErrorCode !== undefined ? '-' + subErrorCode : ''}`) {
		case '1010':
			errorMessage = 'No user found for the Instagram post';
			break;
		case '1020':
			errorMessage = 'No user found for the Instagram story';
			break;
		case '1020':
			errorMessage = 'The link to the post is missing';
			break;
		case '2000-463':
			errorMessage = 'Your session has expired, please authenticate again';
			break;
		case '2000--1':
			errorMessage = 'You need more viewers before we can fetch stats from your post';
			break;
		case '2000':
			errorMessage = 'The post link is not correct';
			break;
		case '2010':
			errorMessage = 'There is no Facebook pages connected to this user';
			break;
		case '2020':
			errorMessage = 'The user is not connected with Instagram';
			break;
		default:
			errorMessage = 'Oops! Something went wrong';
	}
	return errorMessage;
};
