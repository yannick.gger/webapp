import axios, { AxiosResponse } from 'axios';
import { StatusCode } from 'services/Response.types';
import Client from 'shared/ApiClient';
import { logout } from 'shared/facebook/facebook-sdk';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { IFacebookAuthService } from './types';

/**
 * Facebook Authentication Service
 */
class FacebookAuthService implements IFacebookAuthService {
	storage: BrowserStorage;

	ENDPOINT_URL: string;

	constructor() {
		this.storage = new BrowserStorage(StorageType.LOCAL);
		this.ENDPOINT_URL = '/facebook';
	}

	async deleteFacebookToken() {
		let result: number = 0;

		// revoke app permissions to logout completely because FB.logout() doesn't remove FB cookie
		logout();

		await Client('delete', `${this.ENDPOINT_URL}/tokens`)
			.then((response: AxiosResponse<any>) => {
				result = StatusCode.OK;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					result = StatusCode.BAD_REQUEST;
				}
			});

		return result;
	}

	async getBusinessAccounts() {
		let result: any;
		await Client('get', `${this.ENDPOINT_URL}/accounts`)
			.then((response: AxiosResponse<any>) => {
				result = response.data.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					result = StatusCode.BAD_REQUEST;
				}
			});

		return result;
	}

	async storeFacebookAccessToken(accessToken: string) {
		let result: number = 0;
		await Client('post', `${this.ENDPOINT_URL}/tokens`, { token: accessToken })
			.then((response: AxiosResponse<any>) => {
				result = StatusCode.OK;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					result = StatusCode.BAD_REQUEST;
				}
			});

		return result;
	}
}

export default new FacebookAuthService();
