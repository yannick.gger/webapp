import axios, { AxiosError, AxiosResponse } from 'axios';
import { ICollabsResponse } from 'services/Response.types';
import Client from 'shared/ApiClient';
import { IInstagramInsightsService } from './types';

/**
 * InstagramInsightsService
 */
class InstagramInsightsService implements IInstagramInsightsService {
	ENDPOINT_URL: string;

	constructor() {
		this.ENDPOINT_URL = '/facebook';
	}

	async getPostMetrics(postId: number, imageUrl?: string) {
		let result: any = {};

		await Client('post', `${this.ENDPOINT_URL}/instagram-post-insight/${postId}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}

	async getStoryMetrics(postId: number, storyUrl?: string) {
		let result: ICollabsResponse = {};

		await Client('get', `${this.ENDPOINT_URL}/instagram-story-insight/${postId}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = { data: response.data.data };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}
}

export default new InstagramInsightsService();
