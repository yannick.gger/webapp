import { ICollabsResponse, StatusCode } from 'services/Response.types';

export interface IInstagramInsightsService {
	getPostMetrics: (postId: number, imageUrl?: string) => Promise<ICollabsResponse>;
	getStoryMetrics: (postId: number, imageUrl?: string) => Promise<ICollabsResponse>;
}

export interface IFacebookAuthService {
	deleteFacebookToken: () => Promise<StatusCode>;
	getBusinessAccounts: () => Promise<StatusCode>;
	storeFacebookAccessToken: (accesstoken: string) => Promise<StatusCode>;
}
