export interface IInfluencerService {
	getInvitedCampaign: (inviteToken: string) => Promise<any>;
	getInvites: () => Promise<any>;
	joinCampaign: (campaignId: string, influencerId: string) => Promise<any>;
}
