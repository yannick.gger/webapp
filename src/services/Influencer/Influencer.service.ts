import axios, { AxiosError, AxiosResponse } from 'axios';
import { ICollabsResponse } from 'services/Response.types';
import Client from 'shared/ApiClient';
import { IInfluencerService } from './type';

class InfluencerService implements IInfluencerService {
	async getInvitedCampaign(inviteToken: string) {
		let result;

		await Client('get', `/public/campaigns/${inviteToken}`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err) => {
				return Promise.reject(err);
			});

		return result;
	}

	async getCampaigns() {
		let result;

		await Client(
			'get',
			`/campaigns?includes=invites,assignments,assignments.groups,assignments,assignments.groups.deadlines,assignments.reviews,assignments.reviews.media,campaignInstagramOwners,campaignInstagramOwners.campaignInstagramOwnerAssignments,campaignInstagramOwners.campaignInstagramOwnerAssignments.instagramPosts,campaignInstagramOwners.campaignInstagramOwnerAssignments.instagramstories,campaignInstagramOwners.campaignInstagramOwnerCommissions,campaignInstagramOwners.campaignInstagramOwnerCommissions.commission,campaignInstagramOwners.campaignInstagramOwnerProducts,campaignInstagramOwners.campaignInstagramOwnerProducts.product`
		)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = err;
				}
			});

		return result;
	}

	async getInvites() {
		let result;
		await Client('get', `/campaign-invites`)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = err;
				}
			});

		return result;
	}

	async joinCampaign(campaignId: string, influencerId: string) {
		const url = `/campaigns/${campaignId}/influencers/${influencerId}/join`;
		let result;
		await Client('post', url)
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = err;
				}
			});

		return result;
	}

	async createReview(ids: { campaignId: string; assignmentId: string; influencerId: string }, content: string) {
		const url = `/campaigns/${ids.campaignId}/assignments/${ids.assignmentId}/influencers/${ids.influencerId}/reviews`;
		let result;
		await Client('post', url, { text: content })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = err;
				}
			});

		return result;
	}

	async updateText(ids: { campaignId: string; assignmentId: string; assignmentReviewId: string }, content: string) {
		const url = `/campaigns/${ids.campaignId}/assignments/${ids.assignmentId}/reviews/${ids.assignmentReviewId}`;
		let result;
		await Client('patch', url, { text: content })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = err;
				}
			});

		return result;
	}

	async uploadReviewImage(ids: { campaignId: string; assignmentId: string; assignmentReviewId: string }, media: any) {
		const formData = new FormData();
		const url = `/campaigns/${ids.campaignId}/assignments/${ids.assignmentId}/reviews/${ids.assignmentReviewId}/media`;
		let result;
		if (media) {
			formData.append('file', media);
		}
		await Client('post', url, formData, null, { 'Content-Type': 'multipart/form-data' })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = err;
				}
			});

		return result;
	}

	async uploadPostLink(ids: { campaignId: string; assignmentId: string; influencerId: string }, link: string, postedAt: string) {
		const url = `/campaigns/${ids.campaignId}/assignments/${ids.assignmentId}/influencers/${ids.influencerId}/instagram-posts`;
		let result;
		await Client('post', url, { shortcode: link, postedAt: postedAt })
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = response.data;
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = err;
				}
			});

		return result;
	}
}

export default new InfluencerService();
