import { ICollabsResponse } from 'services/Response.types';

export interface IDiscoveryService {
	autoCompleteSearch: (param: string, signal: any) => Promise<any>;
	searchInfluencer: (param: { searchWord: string | null; categories: string | null; filter: string | null; lookALike: string | null }) => Promise<any>;
	getInfluencerExtraData: (influencerId: string) => Promise<any>;
}

export interface IDiscoveryResponse extends ICollabsResponse {}
