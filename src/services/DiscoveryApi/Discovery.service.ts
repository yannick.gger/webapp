import { AxiosError } from 'axios';
import DiscoveryApiClient from 'shared/DiscoveryApiClient';
import { IDiscoveryService, IDiscoveryResponse } from './types';

class DiscoveryService implements IDiscoveryService {
	ENDPOINT_URL: string;

	constructor() {
		this.ENDPOINT_URL = '/api';
	}

	async autoCompleteSearch(param: string, signal: any) {
		let result: any;
		let url = `${this.ENDPOINT_URL}/autocomplete?q=${param}`;

		await DiscoveryApiClient('get', url, undefined, signal)
			.then((response: IDiscoveryResponse) => {
				result = response.data;
			})
			.catch((err: AxiosError) => {
				console.log(err);
			});

		return result;
	}

	async searchInfluencer(param: { searchWord: string | null; categories: string | null; filter: string | null; lookALike: string | null }) {
		let result: any;
		let url = `${this.ENDPOINT_URL}/search?`;

		if (param.searchWord) {
			url = `${url}q=${param.searchWord}`;
		}

		if (param.categories) {
			url = `${url}&categories=${param.categories}`;
		}

		if (param.filter) {
			url = `${url}&${param.filter}`;
		}

		if (param.lookALike) {
			url = `${url}&lookalike=${param.lookALike}`;
		}

		await DiscoveryApiClient('get', url, undefined)
			.then((response: IDiscoveryResponse) => {
				result = response.data;
			})
			.catch((err: AxiosError) => {
				console.log(err);
			});
		return result;
	}

	async getInfluencerExtraData(influencerId: string) {
		let result: any;
		let url = `${this.ENDPOINT_URL}/influencer/${influencerId}`;

		await DiscoveryApiClient('get', url)
			.then((response: IDiscoveryResponse) => {
				result = response.data;
			})
			.catch((err: AxiosError) => {
				console.log(err);
			});
		return result;
	}

	async getInstagramPostAndDate(url: string) {
		let result: any;
		await DiscoveryApiClient('get', url)
			.then((response: IDiscoveryResponse) => {
				result = response.data;
			})
			.catch((err: AxiosError) => {
				console.log(err);
			});
		return result;
	}
}

export default new DiscoveryService();
