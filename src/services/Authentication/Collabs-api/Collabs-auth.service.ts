import axios, { AxiosResponse } from 'axios';
import { KEY_TOKENS, KEY_COLLABS_API_TOKEN, KEY_COLLABS_API_USER_OBJECT, GHOST_USER_EMAIL, GHOST_USER, GHOST_TOKEN } from 'constants/localStorage-keys';
import { ICollabsResponse, StatusCode } from 'services/Response.types';
import Client from 'shared/ApiClient';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { GlobalUserObject, ICollabsAuthService, ICollabsTokenResponse, loginCredentials } from './types';

/**
 * Collabs Authentication Service
 */
class CollabsAuthService implements ICollabsAuthService {
	storage: BrowserStorage;

	constructor() {
		this.storage = new BrowserStorage(StorageType.LOCAL);
	}

	/**
	 * Get the token from the Collabs API and stores it in the localStorage
	 *
	 * @param   {loginCredentials} credentials
	 * @returns {StatusCode} StatusCode
	 */
	requestCollabsToken = async (credentials: loginCredentials): Promise<StatusCode> => {
		let result: number = 0;
		await Client('post', '/tokens', credentials)
			.then((response: AxiosResponse<ICollabsTokenResponse>) => {
				result = StatusCode.OK;
				this.setCollabsToken(response.data.token);
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					result = error.status;
					throw Error(error.message, error.cause);
				}

				console.log(error);
			});

		return result;
	};

	me = async (): Promise<StatusCode> => {
		let result: number = 0;
		await Client('get', '/me')
			.then((response: AxiosResponse<ICollabsResponse>) => {
				result = StatusCode.OK;
				const included = response.data.included;
				const organization = included.find((x: any) => x.type === 'organization');
				const influencer = included.find((x: any) => x.type === 'influencer');
				// @todo type
				this.setCollabsUserObject({
					id: response.data.data.id,
					instagramUsername: response.data.data.attributes.instagramUsername,
					initials: response.data.data.attributes.initials,
					name: response.data.data.attributes.name,
					permissions: {
						entities: response.data.data.attributes.permissions.entities,
						isAgent: response.data.data.attributes.permissions['is_agent'],
						isImpersonating: response.data.data.attributes.permissions['is_impersonating'],
						isSuperAdmin: response.data.data.attributes.permissions['is_super_admin'],
						isInfluencer: response.data.data.attributes.permissions['is_influencer']
					},
					organization: {
						id: organization && organization.id ? organization.id : '',
						name: organization && organization.attributes.name ? organization.attributes.name : ''
					},
					influencer: {
						id: influencer && influencer.id ? influencer.id : ''
					}
				});
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					result = error.status;
					throw Error(error.message, error.cause);
				}

				console.log(error);
			});

		return result;
	};

	/**
	 * Set token in localStorage
	 * @param {string} token
	 */
	setCollabsToken = (token: string) => {
		const tokens = this.storage.getItem(KEY_TOKENS) || '';
		if (tokens !== '') {
			const parsedToken = JSON.parse(tokens);
			parsedToken[KEY_COLLABS_API_TOKEN] = token;
			this.storage.setItem(KEY_TOKENS, JSON.stringify(tokens));
		} else {
			const collabsToken: { [key: string]: string } = {};
			collabsToken[KEY_COLLABS_API_TOKEN] = token;
			this.storage.setItem(KEY_TOKENS, JSON.stringify(collabsToken));
		}
	};

	/**
	 * Get token from localStorage
	 */
	getCollabsToken = () => {
		this.storage.getItem(KEY_TOKENS);
	};

	/**
	 * Remove token from localStorage
	 */
	removeCollabsToken = () => {
		this.storage.removeItem(KEY_TOKENS);
	};

	/**
	 * Set User object in localStorage
	 * @param {GlobalUserObject} object
	 */
	setCollabsUserObject = (object: GlobalUserObject) => {
		this.storage.setItem(KEY_COLLABS_API_USER_OBJECT, JSON.stringify(object));
	};

	/**
	 * Get User object from localStorage
	 */
	getCollabsUserObject = (): GlobalUserObject => {
		const loggedInUser = JSON.parse(this.storage.getItem(KEY_COLLABS_API_USER_OBJECT) || '{}');

		// To make sure we logout legacy sessions
		if (typeof loggedInUser.permissions === 'object' && loggedInUser.permissions !== null) {
			return loggedInUser;
		}

		this.panicLogout();
	};

	/**
	 * Something is really wrong. Just logout the user and hope for the best.
	 */
	panicLogout = () => {
		this.clearAll();
		localStorage.removeItem('token');
		localStorage.removeItem('user');
		window.location.href = '/login';
	};

	/**
	 * Remove token from localStorage
	 */
	clearAll = () => {
		this.storage.removeItem(KEY_COLLABS_API_TOKEN);
		this.storage.removeItem(KEY_TOKENS);
		this.storage.removeItem(KEY_COLLABS_API_USER_OBJECT);
		this.storage.removeItem(GHOST_USER_EMAIL);
		this.storage.removeItem(GHOST_USER);
		this.storage.removeItem(GHOST_TOKEN);
	};
}

export default new CollabsAuthService();
