import { StatusCode } from 'services/Response.types';

export type loginCredentials = {
	username: string;
	password: string;
};

export interface ICollabsTokenResponse {
	token: string;
}

export interface ICollabsAuthService {
	requestCollabsToken: (credentials: loginCredentials) => Promise<StatusCode>;
	setCollabsToken: (token: string) => void;
	getCollabsToken: () => void;
	removeCollabsToken: () => void;
}

export type GlobalUserObject = {
	id: string;
	initials: string;
	instagramUsername: string;
	name: string;
	profilePictureUrl: string;
	organization: {
		id: string;
		name: string;
	};
	permissions: {
		entities: { [key: string]: PermissionsEntity };
		isAgent: boolean;
		isImpersonating: boolean;
		isInfluencer: boolean;
		isSuperAdmin: boolean;
	};
};

export type PermissionsEntity = {
	name: string;
	role: string;
	type: string;
};
