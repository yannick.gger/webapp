import { Moment } from 'moment';
import { StatusCode } from 'services/Response.types';
import { ICollabsResponse } from 'services/Response.types';

export interface IDiscoveryTokenResponse extends ICollabsResponse {
	data: {
		type: 'token';
		id: string;
		attributes: {
			token: string;
			expiresIn: number;
		};
	};
}

export interface IDiscoveryAuthService {
	requestDiscoveryToken: () => Promise<StatusCode>;
	setDiscoveryToken: (token: string, expiresIn: number) => void;
	getDiscoveryToken: () => { token: string; expiresAt: string } | null;
	removeDiscoveryToken: () => void;
}
