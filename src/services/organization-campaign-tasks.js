import React from 'react';
import { Link } from 'react-router-dom';
import { defineMessages } from 'react-intl';
import moment from 'moment';
import { apiToMoment } from 'utils';
import { translate } from 'react-i18next';
import i18n from 'i18next';

import AssignmentsIllu from '../views/organization/campaign/overview/assignments-illu.svg';
import CoverPhotoIllu from '../views/organization/campaign/overview/cover-photo-illu.svg';
import LogoIllu from '../views/organization/campaign/overview/logo-illu.svg';
import DiscoverIllu from '../views/organization/dashboard/select-influencers.svg';
import CreateListsIllu from '../views/organization/dashboard/make-lists.svg';

import { isSaler } from 'services/auth-service';

const messages = defineMessages({
	invite_close: {
		id: 'wait-for-influeners-to-join',
		defaultMessage: `Wait for {count, number} more {count, plural, one {influencer} other {influencers}} to join or campaign to close at {closeTime}`
	},
	checklistStatus: {
		id: 'checklist-status',
		defaultMessage: `Status: {currentCount}/{totalCount} {totalCount, plural, one {Influencer} other {Influencers}} {action}. {remainingInfluencers} {remainingInfluencers, plural, one {Influencer} other {Influencers}} left.`,
		description: 'Shown below campaign/assigment checklist'
	},
	doneChecklist: {
		id: 'done-checklist',
		defaultMessage: `Status: {currentCount}/{totalCount} {totalCount, plural, one {Influencer} other {Influencers}} {action}. All done!`,
		description: 'Shown below campaign/assigment checklist'
	},
	noneInfluencersChecklist: {
		id: 'done-checklist',
		defaultMessage: `Status: {currentCount}/{totalCount} {totalCount, plural, one {Influencer} other {Influencers}} {action}`,
		description: 'Shown below campaign/assigment checklist'
	}
});

const getChecklistMessage = (checklist) => {
	if (checklist.remainingInfluencers === 0 && checklist.currentState === 'disabled') {
		return messages.noneInfluencersChecklist;
	}

	return checklist.currentState === 'done' ? messages.doneChecklist : messages.checklistStatus;
};

const anchorize = function(text, url, separator = new RegExp(/(\d+ Influencers? left)/gi)) {
	const parts = text.split(separator);

	for (let i = 1; i < parts.length; i += 2) {
		parts[i] = (
			<Link key={i} to={url} style={{ zIndex: '10' }}>
				{parts[i]}
			</Link>
		);
	}

	return <p>{parts}</p>;
};

export const getTaskGroups = ({ organizationSlug, campaign, intl }) => {
	const assignmentTasks = [];
	const sortedAssignments = campaign.assignments.edges.slice().sort((a, b) => apiToMoment(a.node.startTime) > apiToMoment(b.node.startTime));
	const firstAssignment = sortedAssignments[0];
	const influencersPath = `/${organizationSlug}/campaigns/${campaign.id}/influencers`;

	campaign.assignments.edges
		.slice()
		.sort((a, b) => apiToMoment(a.node.startTime) > apiToMoment(b.node.startTime))
		.forEach(({ node }) => {
			const remainingInfluencers = node.todoInfluencersDoAssignment.remainingInfluencers;
			const currentInfluencerCount = campaign.influencersCount - remainingInfluencers;
			const description = intl.formatMessage(getChecklistMessage(node.todoInfluencersDoAssignment), {
				currentCount: currentInfluencerCount,
				totalCount: campaign.influencersCount,
				action: 'posted',
				remainingInfluencers: remainingInfluencers
			});

			assignmentTasks.push({
				link: `/${organizationSlug}/discover`,
				illustration: CreateListsIllu,
				title: `Influencer does assignment ${node.name}`,
				description: anchorize(description, `${influencersPath}?order=postsCount`),
				disabled: node.todoInfluencersDoAssignment.currentState === 'disabled',
				completed: node.todoInfluencersDoAssignment.currentState === 'done',
				requiresAction: false
			});
		});

	const hasProduct = campaign.productCompensationsCount > 0;
	const hasOnlyPayment = campaign.productCompensationsCount === 0 && campaign.paymentCompensationsCount > 0;

	let taskGroups = [
		{
			key: 'pre_invitation',
			title: i18n.t('organization:campaign.taskGroup.preInvitation.title', { defaultValue: 'Pre-invitation' }),
			description: i18n.t('organization:campaign.taskGroup.preInvitation.description', {
				defaultValue: 'Complete tasks to send out the invitation to influencers'
			}),
			tasks: [
				{
					title: `${i18n.t('organization:campaign.taskGroup.tasks.findMoreInfluencers.title1', { defaultValue: 'Find' })} ${
						campaign.influencerTargetCount - campaign.instagramOwnersInvitedCount > 0
							? campaign.influencerTargetCount - campaign.instagramOwnersInvitedCount
							: 0
					} ${i18n.t('organization:campaign.taskGroup.tasks.findMoreInfluencers.title2', { defaultValue: 'more influencers' })}`,
					description: i18n.t('organization:campaign.taskGroup.tasks.findMoreInfluencers.description', { defaultValue: 'Invite influencers to your campaign' }),
					illustration: DiscoverIllu,
					completed: campaign.influencerTargetCount - campaign.instagramOwnersInvitedCount <= 0,
					requiresAction: true,
					rule: 'recommendation',
					link: { pathname: `/${organizationSlug}/discover`, state: { prevPath: location.pathname } },
					recommendation: { pathname: `/${organizationSlug}/discover`, state: { prevPath: location.pathname } }
				},
				{
					title: i18n.t('organization:campaign.taskGroup.tasks.createAssignment.title', { defaultValue: 'Create assignment' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.createAssignment.description', {
						defaultValue: 'Add tasks for the influencers to complete'
					}),
					illustration: AssignmentsIllu,
					completed: campaign.assignmentsCount !== 0,
					requiresAction: true,
					link: `/${organizationSlug}/campaigns/${campaign.id}/assignments/create`
				},
				{
					title: i18n.t('organization:campaign.taskGroup.tasks.assignmentsDate.title', { defaultValue: 'Assignments in more than 22 days' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.assignmentsDate.description', {
						defaultValue: 'The assignments start dates need to be earliest 22 days from today.'
					}),
					illustration: AssignmentsIllu,
					completed: campaign.assignmentsCount !== 0,
					requiresAction: false,
					link: firstAssignment
						? `/${organizationSlug}/campaigns/${campaign.id}/assignments/${firstAssignment.node.id}/edit`
						: `/${organizationSlug}/campaigns/${campaign.id}/assignments`
				},
				{
					title: i18n.t('organization:campaign.taskGroup.tasks.addCommission.title', { defaultValue: 'Add commission' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.addCommission.description', {
						defaultValue: 'Add at least one commission in form of a product or payment'
					}),
					illustration: AssignmentsIllu,
					completed: hasProduct || campaign.paymentCompensationsCount !== 0 || campaign.invoiceCompensationsCount !== 0,
					requiresAction: true,
					link: `/${organizationSlug}/campaigns/${campaign.id}/products`,
					type: 'commission'
				},
				{
					title: i18n.t('organization:campaign.taskGroup.tasks.addCoverPhoto.title', { defaultValue: 'Add cover photo' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.addCoverPhoto.description', {
						defaultValue: 'You need it for the campaign page'
					}),
					illustration: CoverPhotoIllu,
					completed: campaign.campaignCoverPhoto,
					requiresAction: true,
					link: `/${organizationSlug}/campaigns/${campaign.id}/material`
				}
			]
		},
		{
			key: 'invitation',
			title: i18n.t('organization:campaign.taskGroup.invitation.title', { defaultValue: 'Invitation' }),
			description: i18n.t('organization:campaign.taskGroup.invitation.description', {
				defaultValue: 'You are ready to send out the invitation to influencers.'
			}),
			tasks: [
				{
					link: `/${organizationSlug}/campaigns/${campaign.id}/send-campaign-review`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.sendCampaignForReview.title', { defaultValue: 'Send campaign for review' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.sendCampaignForReview.description', {
						defaultValue: 'When the campaign is approved influencers will be invited'
					}),
					completed: !campaign.canBeSentForReview,
					requiresAction: true,
					disabled: !campaign.canBeSentForReview || isSaler()
				},
				{
					title: i18n.t('organization:campaign.taskGroup.tasks.addLogo.title', { defaultValue: 'Add logo' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.addLogo.description', {
						defaultValue: 'You need it for company branding'
					}),
					illustration: LogoIllu,
					completed: campaign.campaignLogo,
					requiresAction: false,
					link: `/${organizationSlug}/campaigns/${campaign.id}/material`
				},
				{
					link: `/${organizationSlug}/discover`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.waitForInviteToClose.title', { defaultValue: 'Wait for invite to close' }),
					description: intl.formatMessage(messages.invite_close, {
						count: campaign.influencerTargetCount - campaign.instagramOwnersJoinedCount,
						closeTime: campaign.invitesCloseAt
							? apiToMoment(campaign.invitesCloseAt).format('YYYY-MM-DD HH:mm')
							: i18n.t('organization:campaign.taskGroup.tasks.waitForInviteToClose.description2', {
									defaultValue: 'invite closing date'
							  })
					}),
					disabled: true,
					requiresAction: false,
					completed: apiToMoment(campaign.invitesCloseAt) < moment() || campaign.spotsLeft <= 0
				}
			]
		},
		{
			key: 'products',
			title: i18n.t('organization:campaign.taskGroup.products.title', { defaultValue: 'Products' }),
			condition: () => hasProduct,
			description: i18n.t('organization:campaign.taskGroup.products.description', { defaultValue: 'Time to send out products before posting' }),
			tasks: [
				{
					link: `/${organizationSlug}/campaigns/${campaign.id}/send-products`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.sendOutProducts.title', { defaultValue: 'Send out products' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.sendOutProducts.description', {
						defaultValue: 'Send out products to influencers'
					}),
					requiresAction: true,
					disabled: (apiToMoment(campaign.invitesCloseAt) > moment() && campaign.spotsLeft !== 0) || campaign.campaignDelivery !== null,
					completed: campaign.campaignDelivery !== null
				},
				{
					link: `/${organizationSlug}/discover`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.waitForProductDelivered.title', { defaultValue: 'Wait for products to be delivered' }),
					description: campaign.campaignDelivery
						? `${i18n.t('organization:campaign.taskGroup.tasks.waitForProductDelivered.description1', {
								defaultValue: 'Products are estimated to be delivered at the latest on'
						  })} ${apiToMoment(campaign.campaignDelivery.latestDeliveryAt).format('YYYY-MM-D')}`
						: i18n.t('organization:campaign.taskGroup.tasks.waitForProductDelivered.description2', {
								defaultValue: 'Wait for influencers to receive products'
						  }),
					disabled: true,
					requiresAction: false,
					completed: campaign.campaignDelivery && moment() > apiToMoment(campaign.campaignDelivery.latestDeliveryAt)
				}
			]
		},
		{
			key: 'contact-information',
			title: i18n.t('organization:campaign.taskGroup.influencerContactInfo.title', { defaultValue: 'Influencer Contact Information' }),
			condition: () => hasOnlyPayment,
			description: i18n.t('organization:campaign.taskGroup.influencerContactInfo.description', {
				defaultValue: 'Get the information for the Influencers to be able to contact them'
			}),
			tasks: [
				{
					link: `/${organizationSlug}/campaigns/${campaign.id}/influencer-information`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.downloadInfo.title', { defaultValue: 'Download information' }),
					description: i18n.t('organization:campaign.taskGroup.tasks.downloadInfo.description2', {
						defaultValue: 'Download influencers contact information'
					}),
					requiresAction: false,
					disabled: !(apiToMoment(campaign.invitesCloseAt) < moment() || campaign.spotsLeft === 0),
					completed: apiToMoment(campaign.invitesCloseAt) > moment() || campaign.spotsLeft > 0
				}
			]
		},
		{
			condition: () => campaign.assignments.edges.some(({ node }) => node.hasContentReview),
			key: 'review',
			title: i18n.t('organization:campaign.taskGroup.ReviewInfluencerPosts.title', { defaultValue: 'Review influencers posts' }),
			description: i18n.t('organization:campaign.taskGroup.ReviewInfluencerPosts.description', {
				defaultValue: 'Review content uploaded from influencers before review'
			}),
			tasks: [
				{
					link: `/${organizationSlug}/campaigns/${campaign.id}/review`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.influencersContentReivew.title', { defaultValue: 'Influencers uploads content for review' }),
					description: anchorize(
						intl.formatMessage(getChecklistMessage(campaign.todoInfluencersUploadContent), {
							currentCount: campaign.influencersCount - campaign.todoInfluencersUploadContent.remainingInfluencers,
							totalCount: campaign.influencersCount,
							action: 'uploaded for review',
							remainingInfluencers: campaign.todoInfluencersUploadContent.remainingInfluencers
						}),
						`${influencersPath}?order=totalSentForReview`
					),
					requiresAction: false,
					disabled: campaign.todoInfluencersUploadContent.currentState === 'disabled',
					completed: campaign.assignmentsCount > 0 && campaign.todoInfluencersUploadContent.currentState === 'done'
				},
				{
					link: `/${organizationSlug}/campaigns/${campaign.id}/review`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.ReviewContent.title', { defaultValue: 'Review content' }),
					description: anchorize(
						intl.formatMessage(getChecklistMessage(campaign.todoReviewContent), {
							currentCount: campaign.influencersCount - campaign.todoReviewContent.remainingInfluencers,
							totalCount: campaign.influencersCount,
							action: 'content approved',
							remainingInfluencers: campaign.todoReviewContent.remainingInfluencers
						}),
						`${influencersPath}?order=totalApproved`
					),
					requiresAction: true,
					disabled: campaign.todoReviewContent.currentState === 'disabled',
					completed: campaign.assignmentsCount > 0 && campaign.todoReviewContent.currentState === 'done'
				}
			]
		},
		{
			key: 'time-to-post',
			title: i18n.t('organization:campaign.taskGroup.timeToPost.title', { defaultValue: 'Time to post' }),
			description: i18n.t('organization:campaign.taskGroup.timeToPost.description', {
				defaultValue: 'Everything is ready for the influencers to post'
			}),
			tasks: [
				{
					link: `/${organizationSlug}/discover`,
					illustration: CreateListsIllu,
					title: i18n.t('organization:campaign.taskGroup.tasks.waitInfluencerPost.title', { defaultValue: 'Wait for influencer to post' }),
					description: anchorize(
						intl.formatMessage(getChecklistMessage(campaign.todoInfluencersPostInstagram), {
							currentCount: campaign.influencersCount - campaign.todoInfluencersPostInstagram.remainingInfluencers,
							totalCount: campaign.influencersCount,
							action: 'posted',
							remainingInfluencers: campaign.todoInfluencersPostInstagram.remainingInfluencers
						}),
						`${influencersPath}?order=postsCount`
					),
					disabled: campaign.todoInfluencersPostInstagram.currentState === 'disabled',
					requiresAction: false,
					completed: campaign.todoInfluencersPostInstagram.currentState === 'done'
				},
				...assignmentTasks
			]
		}
	];

	taskGroups = taskGroups.filter((group) => (group.condition ? group.condition() : true));

	const currentTaskGroupId = getCurrentTaskGroupId({ taskGroups });

	taskGroups.forEach((taskGroup, i) => {
		if (i > currentTaskGroupId) {
			taskGroup.tasks.forEach((task) => {
				task.disabled = true;
			});
		}
	});

	return taskGroups;
};

export const getCurrentTaskGroupId = ({ taskGroups }) => {
	let id = 0;
	taskGroups.forEach((taskGroup, i) => {
		if (taskGroup.tasks.filter((task) => task.completed).length === taskGroup.tasks.length) {
			id = i + 1;
		}
	});

	return id > taskGroups.length - 1 ? id - 1 : id;
};

export const getRequiresActionCount = ({ taskGroups }) => {
	const currentTaskGroup = taskGroups[getCurrentTaskGroupId({ taskGroups })];

	return currentTaskGroup.tasks.filter((task) => task.requiresAction && !task.completed).length;
};
