import { ICollabsResponse } from 'services/Response.types';

export interface IBrandsService {
	getAvailableUsers: (brandId: string) => Promise<ICollabsResponse>;
}
