import axios, { AxiosError, AxiosResponse } from 'axios';
import { ICollabsResponse } from 'services/Response.types';
import { IBrandsService } from './types';
import { createClient } from '../../shared/ApiClient/ApiClient';

const client = createClient();
class BrandsService implements IBrandsService {
	ENDPOINT_URL: string;

	constructor() {
		this.ENDPOINT_URL = '/brands';
	}

	async getAvailableUsers(brandId: string) {
		let result: ICollabsResponse = {};

		await client
			.get<ICollabsResponse>(`${this.ENDPOINT_URL}/${brandId}/available-users`)
			.then((response) => {
				result = { data: response.data.data, included: response.data.included };
			})
			.catch((err: Error | AxiosError) => {
				if (axios.isAxiosError(err)) {
					result = {
						errors: err.response && err.response.data.errors
					};
				} else {
					console.error(err);
				}
			});

		return result;
	}
}

export default new BrandsService();
