export const isCard = (paymentMethod) => paymentMethod === 'card';
export const isInvoice = (paymentMethod) => paymentMethod === 'invoice';

export const paymentDetails = (billingPlan, taxPercent, paymentMethod = 'card', subscription) => {
	const currency = billingPlan.currency.toUpperCase();

	const standardizeAmount = (isInvoice(paymentMethod) && subscription ? subscription.amount : billingPlan.amount) / 100;
	const monthlyAmount = (isInvoice(paymentMethod) && subscription ? subscription.amount : billingPlan.monthlyAmount) / 100;

	const subTotal = isCard(paymentMethod) ? standardizeAmount : monthlyAmount;
	const taxAmount = (subTotal * taxPercent) / 100;
	const totalAmount = subTotal + taxAmount;

	return {
		currency,
		standardizeAmount,
		monthlyAmount,
		subTotal,
		taxAmount,
		totalAmount,
		paymentFrequency: isCard(paymentMethod) ? billingPlan.nickname : 'Monthly'
	};
};
