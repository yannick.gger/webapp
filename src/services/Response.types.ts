export enum StatusCode {
	OK = 200,
	CREATED = 201,
	DELETE_SUCCESS = 204,
	BAD_REQUEST = 400,
	UNAUTHORIZED = 401,
	TOO_MANY_REQUESTS = 429,
	INTERNAL_SERVER_ERROR = 500
}

export interface ICollabsResponse {
	data?: any;
	included?: any;
	errors?: ICollabsError[];
}

export interface ICollabsData {
	id?: string;
	type?: string;
	attributes?: any;
	relationships?: any;
	links?: any;
}

interface ICollabsError {
	status: string;
	title: string;
	code: string;
	meta: ICollabsErrorMeta;
}

interface ICollabsErrorMeta {
	message: string;
	subErrorCode: number;
}
