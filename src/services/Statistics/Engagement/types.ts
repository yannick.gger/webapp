export type EngagementParam = {
	from: string;
	to: string;
	platforms?: string;
	frequency?: string;
	campaigns?: Array<number>;
};

export type EnagegementData = {
	type: string;
	id: string;
	attributes: {
		date?: string;
		impressions?: number;
		reach?: number;
		likes?: number;
		comments?: number;
		followers?: number;
		interactions?: number;
		brandClicks?: number;
	};
};

export type EngagementResponse = {
	data: Array<EnagegementData> | EnagegementData;
};

export interface IEngagementService {
	fetchEngagementTotal: (params: EngagementParam) => Promise<any>;
	fetchEngagementTotalByPlatforms: (params: EngagementParam) => Promise<any>;
	fetchEngagements: (params: EngagementParam) => Promise<any>;
}
