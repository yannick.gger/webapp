export type DashboardItem = {
	type: string;
	size: string;
	metaData: {
		period?: string;
	};
};

export type DashboardData = {
	type: string;
	id: string;
	attributes: {
		name: string;
		items: DashboardItem[];
		active: boolean;
		createdAt: string;
	};
};

export type DashboardRequestBody = {
	name?: string;
	active?: boolean;
	items?: DashboardItem[] | DashboardItem;
	metaData?: {
		'dashboard-period'?: string;
		'dashboard-campaigns'?: string;
	};
};

export type DashboardResponse = {
	data: Array<DashboardData> | DashboardData;
};

export interface IDashboardService {
	fetchDashboards: () => Promise<any>;
	fetchOneDashboard: (id: string) => Promise<any>;
	createDashboard: (requestBody: DashboardRequestBody) => Promise<any>;
	appendChartInDashboard: (props: { id: string; data: DashboardItem }) => Promise<any>;
	updateDashboard: (props: { id: string; data: DashboardRequestBody }) => Promise<any>;
	deleteDashboard: (id: string) => Promise<any>;
}
