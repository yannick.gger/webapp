import axios, { AxiosError, AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { IDashboardService, DashboardRequestBody, DashboardResponse, DashboardItem, DashboardData } from './types';

class DashboardService implements IDashboardService {
	fetchDashboards = async (): Promise<any> => {
		const url = `/dashboards`;
		let result: any;
		await Client('get', url)
			.then((response: AxiosResponse<DashboardResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchOneDashboard = async (id: string): Promise<any> => {
		const url = `/dashboards/${id}`;
		let result: any;
		await Client('get', url)
			.then((response: AxiosResponse<DashboardResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	createDashboard = async (dashboardData: DashboardRequestBody): Promise<any> => {
		const url = `/dashboards`;
		let result: any;

		await Client('post', url, dashboardData)
			.then((response: AxiosResponse<{ data: { type: string; id: string; attributes: { name: string } } }>) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	appendChartInDashboard = async (props: { id: string; data: DashboardItem }): Promise<any> => {
		const url = `/dashboards/${props.id}/items`;
		let result: any;

		await Client('post', url, props.data)
			.then((response: AxiosResponse<DashboardData>) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	updateDashboard = async (props: { id: string; data: DashboardRequestBody }): Promise<any> => {
		const url = `/dashboards/${props.id}`;
		let result: any;

		await Client('patch', url, props.data)
			.then((response: AxiosResponse<DashboardData>) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	deleteDashboard = async (dashboardId: string): Promise<any> => {
		const url = `/dashboards/${dashboardId}`;
		let result: any;

		await Client('delete', url)
			.then((response: AxiosResponse<DashboardData>) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};
}

export default new DashboardService();
