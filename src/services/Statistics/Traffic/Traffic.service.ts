import axios, { AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { ITrafficService, TrafficParam, TrafficResponse } from './types';

class TrafficService implements ITrafficService {
	fetchTrafficLinkTotal = async (param: TrafficParam): Promise<any> => {
		let url = `/statistics/clicks/total?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/clicks/total?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchTrafficBrandMentionTotal = async (param: TrafficParam): Promise<any> => {
		//@todo: BrandMention traffic
		let url = `/statistics/clicks/total?from=${param.from}&to=${param.to}`;
		let result = 0;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/clicks/total?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				// result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchTrafficLinkCounts = async (param: TrafficParam): Promise<any> => {
		let url = `/statistics/clicks/${param.frequency}?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/clicks/${param.frequency}?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchTrafficBrandMentionCounts = async (param: TrafficParam): Promise<any> => {
		//@todo: BrandMention traffic
		let url = `/statistics/clicks/${param.frequency}?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/clicks/${param.frequency}?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchTrafficGender = async (param: TrafficParam): Promise<any> => {
		let url = `/statistics/traffic/gender?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/traffic/gender?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchTrafficGenderByPlatforms = async (param: TrafficParam): Promise<any> => {
		let url = `/statistics/traffic/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/traffic/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchTrafficCountry = async (param: TrafficParam): Promise<any> => {
		let url = `/statistics/traffic/country?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/traffic/country?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchTrafficCountryByPlatforms = async (param: TrafficParam): Promise<any> => {
		let url = `/statistics/traffic/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/traffic/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<TrafficResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};
}

export default new TrafficService();
