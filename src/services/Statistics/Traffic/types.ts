export type TrafficParam = {
	from: string;
	to: string;
	frequency?: string;
	campaigns?: Array<number>;
	platforms?: 'instagram-story' | 'instagram-post';
};

export type DateAndCount = {
	type: string;
	id: string;
	attributes: {
		year: number | string;
		month?: number | string;
		date?: number | string;
		week?: number | string;
		count: number;
	};
};

export type TrafficResponse = {
	data: Array<DateAndCount>;
};
export interface ITrafficService {
	fetchTrafficLinkTotal: (params: TrafficParam) => Promise<any>;
	fetchTrafficLinkCounts: (params: TrafficParam) => Promise<any>;
}
