import axios, { AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { IReachService, ReachParam, ReachResponse } from './types';

class ReachService implements IReachService {
	fetchReachTotal = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/total?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/total?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchReachTotalByPlatforms = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/total?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/total?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchGrossReachTotal = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/gross-total?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/gross-total?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchGrossReachTotalByPlatforms = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/gross-total?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/gross-total?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	/**
	 * Get reach count per date in defined period
	 *
	 * @param {ReachParam} param
	 * @returns
	 */

	fetchReachCounts = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/${param.frequency}?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/${param.frequency}?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	/**
	 * Get reach count per date in defined period and platforms (instagram-post or instagram-story)
	 *
	 * @param {ReachParam} param
	 * @returns
	 */
	fetchReachCountsByPlatforms = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/${param.frequency}?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/${param.frequency}?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchReachGender = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/gender?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/gender?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchReachGenderByPlatforms = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchReachCountry = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/country?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/country?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchReachCountryByPlatforms = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchReachAge = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/age?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/age?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchReachAgeByPlatforms = async (param: ReachParam): Promise<any> => {
		let url = `/statistics/reach/age?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/reach/age?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ReachResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};
}

export default new ReachService();
