export type ReachParam = {
	from: string;
	to: string;
	platforms?: string;
	frequency?: string;
	campaigns?: Array<number>;
};

export type DateAndCount = {
	type: string;
	id: string;
	attributes: {
		year: number | string;
		month?: number | string;
		date?: number | string;
		week?: number | string;
		count: number;
	};
};

export type ReachResponse = {
	data: Array<DateAndCount>;
};
export interface IReachService {
	fetchReachTotal: (params: ReachParam) => Promise<any>;
	fetchReachTotalByPlatforms: (params: ReachParam) => Promise<any>;
	fetchGrossReachTotal: (params: ReachParam) => Promise<any>;
	fetchGrossReachTotalByPlatforms: (params: ReachParam) => Promise<any>;
	fetchReachCounts: (params: ReachParam) => Promise<any>;
	fetchReachCountsByPlatforms: (params: ReachParam) => Promise<any>;
	fetchReachGender: (params: ReachParam) => Promise<any>;
	fetchReachGenderByPlatforms: (params: ReachParam) => Promise<any>;
	fetchReachCountry: (params: ReachParam) => Promise<any>;
	fetchReachCountryByPlatforms: (params: ReachParam) => Promise<any>;
	fetchReachAge: (params: ReachParam) => Promise<any>;
	fetchReachAgeByPlatforms: (params: ReachParam) => Promise<any>;
}
