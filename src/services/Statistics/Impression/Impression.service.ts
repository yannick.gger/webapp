import axios, { AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { IImpressionService, ImpressionParam, ImpressionResponse } from './types';

class ImpressionService implements IImpressionService {
	fetchImpressionTotal = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impressions/total?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impressions/total?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchImpressionTotalByPlatforms = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impressions/total?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impressions/total?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	/**
	 * Get impression count per date in defined period
	 *
	 * @param {ImpressionParam} param
	 * @returns
	 */
	fetchImpressionCounts = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impressions/${param.frequency}?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impressions/${param.frequency}?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	/**
	 * Get count per date in defined period and platforms (instagram-post or instagram-story)
	 *
	 * @param {ImpressionParameter} param
	 * @returns
	 */
	fetchImpressionCountsByPlatforms = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impressions/${param.frequency}?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impressions/${param.frequency}?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchImpressionGender = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impression/gender?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impression/gender?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchImpressionGenderByPlatforms = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impression/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impression/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchImpressionCountry = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impression/country?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impression/country?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchImpressionCountryByPlatforms = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impression/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impression/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchImpressionAge = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impression/age?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impression/age?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchImpressionAgeByPlatforms = async (param: ImpressionParam): Promise<any> => {
		let url = `/statistics/impression/age?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/impression/age?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<ImpressionResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};
}

export default new ImpressionService();
