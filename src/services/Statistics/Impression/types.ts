export type ImpressionParam = {
	from: string;
	to: string;
	platforms?: string;
	frequency?: string;
	campaigns?: Array<number>;
};

export type DateAndCount = {
	type: string;
	id: string;
	attributes: {
		year: number | string;
		month?: number | string;
		date?: number | string;
		week?: number | string;
		count: number;
	};
};

export type ImpressionResponse = {
	data: Array<DateAndCount> | DateAndCount;
};

export interface IImpressionService {
	fetchImpressionTotal: (params: ImpressionParam) => Promise<any>;
	fetchImpressionTotalByPlatforms: (params: ImpressionParam) => Promise<any>;
	fetchImpressionCounts: (params: ImpressionParam) => Promise<any>;
	fetchImpressionCountsByPlatforms: (params: ImpressionParam) => Promise<any>;
	fetchImpressionGender: (params: ImpressionParam) => Promise<any>;
	fetchImpressionGenderByPlatforms: (params: ImpressionParam) => Promise<any>;
	fetchImpressionCountry: (params: ImpressionParam) => Promise<any>;
	fetchImpressionCountryByPlatforms: (params: ImpressionParam) => Promise<any>;
	fetchImpressionAge: (params: ImpressionParam) => Promise<any>;
	fetchImpressionAgeByPlatforms: (params: ImpressionParam) => Promise<any>;
}
