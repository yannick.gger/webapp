export type BudgetParam = {
	from: string;
	to: string;
	frequency?: string;
	campaigns?: Array<number>;
	currency?: string;
};

export type BudgetData = {
	type: string;
	id: string;
	attributes: {
		country?: string;
		year?: number;
		month?: number;
		date?: string | number;
		amount: number;
		currency: string;
	};
};

export type BudgetResponse = {
	data: Array<BudgetData> | BudgetData;
};

export interface IBudgetService {
	fetchBudgetTotal: (params: BudgetParam) => Promise<any>;
	fetchBudgets: (params: BudgetParam) => Promise<any>;
	fetchBudgetsPerCountry: (params: BudgetParam) => Promise<any>;
	fetchBudgetCPM: (params: BudgetParam) => Promise<any>;
}
