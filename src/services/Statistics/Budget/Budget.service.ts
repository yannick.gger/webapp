import axios, { AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { IBudgetService, BudgetParam, BudgetResponse } from './types';

class BudgetService implements IBudgetService {
	fetchBudgetTotal = async (param: BudgetParam): Promise<any> => {
		let url = `/statistics/campaign-compensations/total?from=${param.from}&to=${param.to}&currency=${param.currency ? param.currency : 'SEK'}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/campaign-compensations/total?from=${param.from}&to=${param.to}&campaigns=${campaigns}&currency=${
				param.currency ? param.currency : 'SEK'
			}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<BudgetResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchBudgets = async (param: BudgetParam): Promise<any> => {
		let url = `/statistics/campaign-compensations/${param.frequency}?from=${param.from}&to=${param.to}&currency=${param.currency ? param.currency : 'SEK'}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/campaign-compensations/${param.frequency}?from=${param.from}&to=${param.to}&campaigns=${campaigns}&currency=${
				param.currency ? param.currency : 'SEK'
			}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<BudgetResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchBudgetsPerCountry = async (param: BudgetParam): Promise<any> => {
		let url = `/statistics/campaign-compensations/country?from=${param.from}&to=${param.to}&currency=${param.currency ? param.currency : 'SEK'}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/campaign-compensations/country?from=${param.from}&to=${param.to}&campaigns=${campaigns}&currency=${
				param.currency ? param.currency : 'SEK'
			}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<BudgetResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchBudgetCPM = async (param: BudgetParam): Promise<any> => {
		let url = `/statistics/cpm/total?from=${param.from}&to=${param.to}&currency=${param.currency ? param.currency : 'SEK'}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/cpm/total?from=${param.from}&to=${param.to}&currency=${param.currency ? param.currency : 'SEK'}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<BudgetResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};
}

export default new BudgetService();
