export type InfluencerParam = {
	from: string;
	to: string;
	platforms?: string;
	campaigns?: Array<number>;
};

export type InfluencerData = {
	type: string;
	id: string;
	links: {
		profileImage: string;
	};
	attributes: {
		ownerId: string;
		username: string;
		name: string | null;
		impressions: number;
		reach: number;
		likes: number;
		comments: number;
		followers: number;
	};
};

export type InfluencerResponse = {
	data: Array<InfluencerData>;
};

export interface IInfluencerService {
	fetchInfluencerTotal: (params: InfluencerParam) => Promise<any>;
	fetchInfluencerTotalByPlatforms: (params: InfluencerParam) => Promise<any>;
}
