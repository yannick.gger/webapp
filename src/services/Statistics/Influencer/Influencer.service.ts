import axios, { AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { IInfluencerService, InfluencerParam, InfluencerResponse } from './types';

class InfluencerService implements IInfluencerService {
	fetchInfluencerTotal = async (param: InfluencerParam): Promise<any> => {
		let url = `/statistics/influencers/total?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/influencers/total?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<InfluencerResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchInfluencerTotalByPlatforms = async (param: InfluencerParam): Promise<any> => {
		let url = `/statistics/influencers/total?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/influencers/total?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<InfluencerResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};
}

export default new InfluencerService();
