import axios, { AxiosError, AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { ICollabsResponse, StatusCode } from 'services/Response.types';
import { ICampaignService, CampaignParam, CampaignResponse } from './types';

class CampaignService implements ICampaignService {
	ENDPOINT_URL: string;

	constructor() {
		this.ENDPOINT_URL = '/campaigns';
	}

	fetchCampaigns = async (param: CampaignParam): Promise<ICollabsResponse> => {
		let url = this.ENDPOINT_URL;
		let result: ICollabsResponse = {};
		if (param.statuses) {
			const statuses = param.statuses.join(',');
			url = `${this.ENDPOINT_URL}/${statuses}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<CampaignResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchOneCampaign = async (param: CampaignParam): Promise<ICollabsResponse> => {
		let url = this.ENDPOINT_URL;
		let result: ICollabsResponse = {};
		if (param.id) {
			const campaignId = param.id;
			url = `${this.ENDPOINT_URL}/${campaignId}`;
		}
		await Client('get', url)
			.then((response) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchExtraCampaignData = async (url: string): Promise<ICollabsResponse> => {
		let result: ICollabsResponse = {};
		await Client('get', url)
			.then((response) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	setInfluencersToCampaign = async (campaignId: string, influencers: string[]): Promise<number> => {
		let result: number = 0;

		await Client('post', `${this.ENDPOINT_URL}/${campaignId}/influencers`, { collabsIds: influencers })
			.then((response: AxiosResponse<any>) => {
				result = StatusCode.CREATED;
			})
			.catch((err: Error | AxiosError) => {
				console.error(err);
			});

		return result;
	};

	fetchInfluencerPreviewData = async (campaignId: string, influencerId: string) => {
		let result;
		await Client('get', `${this.ENDPOINT_URL}/${campaignId}/invite/${influencerId}/preview`)
			.then((response) => {
				result = response.data;
			})
			.catch((error: AxiosError) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};
}

export default new CampaignService();
