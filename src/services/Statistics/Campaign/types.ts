import { ICollabsResponse } from 'services/Response.types';
export interface CampaignParam {
	id?: string;
	statuses?: string[];
}

export type CampaignData = {
	type: string;
	id: string;
	attributes: {
		name: string;
	};
};

export type CampaignResponse = {
	data: CampaignData[];
};

export interface ICampaignService {
	fetchCampaigns: (params: CampaignParam) => Promise<ICollabsResponse>;
	setInfluencersToCampaign: (campaignId: string, influencers: string[]) => Promise<number>;
}
