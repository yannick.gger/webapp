import axios, { AxiosResponse } from 'axios';
import Client from 'shared/ApiClient';
import { IAudienceService, AudienceParam, AudienceResponse } from './types';

class AudienceService implements IAudienceService {
	fetchAudienceAge = async (param: AudienceParam): Promise<any> => {
		let url = `/statistics/audience/age?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/audience/age?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<AudienceResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchAudienceAgeByPlatforms = async (param: AudienceParam): Promise<any> => {
		let url = `/statistics/audience/age?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/audience/age?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<AudienceResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});
		return result;
	};

	fetchAudienceGender = async (param: AudienceParam): Promise<any> => {
		let url = `/statistics/audience/gender?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/audience/gender?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<AudienceResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchAudienceGenderByPlatforms = async (param: AudienceParam): Promise<any> => {
		let url = `/statistics/audience/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/audience/gender?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<AudienceResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchAudienceCountry = async (param: AudienceParam): Promise<any> => {
		let url = `/statistics/audience/country?from=${param.from}&to=${param.to}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/audience/country?from=${param.from}&to=${param.to}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<AudienceResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};

	fetchAudienceCountryByPlatforms = async (param: AudienceParam): Promise<any> => {
		let url = `/statistics/audience/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}`;
		let result;
		if (param.campaigns && param.campaigns.length > 0) {
			const campaigns = param.campaigns.join(',');
			url = `/statistics/audience/country?from=${param.from}&to=${param.to}&platforms=${param.platforms}&campaigns=${campaigns}`;
		}
		await Client('get', url)
			.then((response: AxiosResponse<AudienceResponse>) => {
				result = response.data;
			})
			.catch((error: AxiosResponse) => {
				if (axios.isAxiosError(error)) {
					throw Error(error.message, error.cause);
				}
				console.log(error);
			});

		return result;
	};
}

export default new AudienceService();
