export type AudienceParam = {
	from: string;
	to: string;
	campaigns?: Array<number>;
	platforms?: 'instagram-story' | 'instagram-post';
};

export type AudienceAgeData = {
	'13-17'?: number;
	'18-24'?: number;
	'25-34'?: number;
	'35-44'?: number;
	'45-54'?: number;
	'55-64'?: number;
	'65+'?: number;
};
export type AudienceCountryData = {
	name: string;
	alpha2code: string;
	alpha3code: string;
	followers: number;
};

export type AudienceGenderData = {
	male: number;
	female: number;
	unknown: number;
};

export type AudienceData = {
	type: string;
	id: string;
	attributes: AudienceAgeData | AudienceCountryData | AudienceGenderData;
};

export type AudienceResponse = {
	data: Array<AudienceData> | AudienceData;
};

export interface IAudienceService {
	fetchAudienceAge: (params: AudienceParam) => Promise<any>;
	fetchAudienceAgeByPlatforms: (params: AudienceParam) => Promise<any>;
	fetchAudienceGender: (params: AudienceParam) => Promise<any>;
	fetchAudienceGenderByPlatforms: (params: AudienceParam) => Promise<any>;
	fetchAudienceCountry: (params: AudienceParam) => Promise<any>;
	fetchAudienceCountryByPlatforms: (params: AudienceParam) => Promise<any>;
}
