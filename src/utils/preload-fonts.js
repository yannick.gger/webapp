import CircularAirBoldWoff from '../assets/fonts/Circular-Air-Bold.woff';
import CircularAirBoldWoff2 from '../assets/fonts/Circular-Air-Bold.woff2';
import CircularAirBookWoff from '../assets/fonts/Circular-Air-Book.woff';
import CircularAirBookWoff2 from '../assets/fonts/Circular-Air-Book.woff2';

const preloadFonts = () => {
	const fonts = {
		CircularAirBook: [
			{ face: CircularAirBookWoff, options: { style: 'normal', weight: 400 } },
			{ face: CircularAirBookWoff2, options: { style: 'normal', weight: 400 } }
		],
		CircularAirBold: [
			{ face: CircularAirBoldWoff, options: { style: 'normal', weight: 700 } },
			{ face: CircularAirBoldWoff2, options: { style: 'normal', weight: 700 } }
		]
	};

	if (window['FontFace']) {
		Object.keys(fonts).forEach((key) => {
			fonts[key].forEach((font) => {
				const ff = new FontFace(key, `url(${font.face})`, font.options);

				ff.load().then((res) => {
					document['fonts'].add(ff);
				});
			});
		});
	} else {
		require('../assets/fonts/fonts.scss');
		const FontFaceObserver = require('fontfaceobserver');
		const ffo = new FontFaceObserver('CircularAirBook', { weight: 400 });
		ffo.load();
	}
};

export default preloadFonts;
