import axios, { AxiosResponse } from 'axios';
import { ErrorResponse } from '../types/http';
import { set } from 'lodash';
import { FormikErrors } from 'formik/dist/types';

export default function(e: unknown, setErrors: (errors: FormikErrors<any>) => void) {
	if (!axios.isAxiosError(e) || !e.response) {
		console.error('Unexpected error %O', e);
		return;
	}

	const { data, status } = e.response as AxiosResponse<ErrorResponse>;
	const errors: Record<string, string> = {};

	if (400 !== status) {
		console.error('Unexpected HTTP %d response: %O', status, data);
		return;
	}

	for (const { source } of data.errors) {
		set(errors, source.parameter, source.message);
	}

	setErrors(errors);
}
