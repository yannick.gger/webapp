import moment from 'moment';

// TODO: Maybe use ISO8601 to support timezones.
export const API_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export const apiToMoment = (dateString) => {
	return moment(dateString, API_DATE_FORMAT);
};

export const getInstagramUrl = (username) => {
	return `https://instagram.com/${username}/`;
};

function findRules(rules, rate, defaultValue, range = false) {
	return (
		(Object.entries(rules)
			.sort((a, b) => (a[1] < b[1] ? -1 : a[1] > b[1] ? 1 : 0))
			.find((item) => (range ? item[1].includes(rate) : rate < item[1])) || [])[0] || defaultValue
	);
}

export const renderScoreText = (value) => {
	return (
		{
			poor: 'Poor',
			baverage: 'Below average',
			neat: 'Pretty neat',
			average: 'Average',
			good: 'Good',
			great: 'Great!',
			amazing: 'Amazing',
			legendary: 'Legendary',
			unicorn: 'Unicorn'
		}[value] || value
	);
};

export const getEngagementRate = (rate) => {
	const rules = {
		poor: 1,
		baverage: 2,
		neat: 3,
		average: 4,
		good: 6,
		great: 8,
		amazing: 10,
		legendary: 15
	};

	return findRules(rules, rate, 'unicorn');
};

export const getInfluencerScore = (rate) => {
	const rules = {
		poor: [1, 2],
		baverage: [3, 4],
		average: [5, 6],
		good: [7],
		great: [8],
		amazing: [9],
		legendary: [10]
	};

	return findRules(rules, rate, 'unknown', true);
};

export const getAudienceMatch = (rate) => {
	const rules = {
		poor: 5,
		baverage: 10,
		average: 15,
		good: 20,
		great: 30,
		amazing: 40,
		legendary: 50
	};

	return findRules(rules, rate, 'legendary');
};

export const getEstimatedImpressions = (instagramOwner, followedByCount) => {
	return `${Math.floor((followedByCount * 0.8 * 0.7) / 1000)}k - ${Math.ceil((followedByCount * 0.8 * 0.9) / 1000)}k`;
};

export const uuid = () => {
	var temp_url = URL.createObjectURL(new Blob());
	var uuid = temp_url.toString();
	URL.revokeObjectURL(temp_url);
	return uuid.substr(uuid.lastIndexOf('/') + 1); // remove prefix (e.g. blob:null/, blob:www.test.com/, ...)
};
