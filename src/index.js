import 'core-js';
import 'services/prototype';
import { message } from 'antd';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { createUploadLink } from 'apollo-upload-client';
import { toIdValue } from 'apollo-utilities';
import bugsnag from 'bugsnag-js';
import createPlugin from 'bugsnag-react';
import { BugsnagContext, UserContextProvider, ToastContextProvider, ThemeContextProvider } from 'contexts';
import React from 'react';
import { ApolloProvider } from 'react-apollo';
import ReactDOM from 'react-dom';
import { I18nextProvider } from 'react-i18next';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import initStore from 'reducers/store';
import preloadFonts from 'utils/preload-fonts';
import { initFacebookSdk } from './shared/facebook/facebook-sdk';
import './services/moment';
import { getOriginalTokenPayload } from 'services/auth-service';

import App from './app';
import i18n from './i18n';
// Cache problems of index.html when using service worker. Try to fix and restore later.
//import registerServiceWorker from "./registerServiceWorker"
import { unregister } from './registerServiceWorker';

const authLink = setContext((_, { headers }) => {
	// get the authentication token from local storage if it exists
	const token = localStorage.getItem('ghostToken') || localStorage.getItem('token');
	// return the headers to the context so httpLink can read them
	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : ''
		}
	};
});

const bugsnagClient = bugsnag({
	apiKey: 'c2e978af0b1dc628376202ef81d40e47',
	appVersion: process.env.REACT_APP_VERSION || 'no-version',
	releaseStage: process.env.REACT_APP_RELEASE_STAGE,
	notifyReleaseStages: ['live', 'staging']
});

const apolloLink = ApolloLink.from([
	onError(({ graphQLErrors, networkError, response, operation }) => {
		const tokenPayload = getOriginalTokenPayload();

		const userInfo = tokenPayload
			? {
					id: tokenPayload.user.id,
					email: tokenPayload.user.email,
					name: tokenPayload.user.name
			  }
			: 'Token payload is null';

		const operationInfo = {
			name: operation.operationName,
			variables: operation.variables,
			query: operation.query
		};

		if (process.env.NODE_ENV !== 'production') {
			if (graphQLErrors)
				graphQLErrors.map(({ message, locations, path }) =>
					console.error(`[GraphQL error]: Message: ${JSON.stringify(message, null, 4)}, Location: ${JSON.stringify(locations, null, 4)}, Path: ${path}`)
				);
			if (networkError) console.error(`[Network error]: ${JSON.stringify(networkError, null, 4)}`);

			if (networkError && networkError.statusCode === 500) {
				message.error('A backend error occured while loading data. Our developers have been notified.');
			} else if (networkError && networkError.message === 'Failed to fetch') {
				message.error('Failed to fetch data from API.');
			}
		} else if (networkError || !(response && response.data)) {
			message.error('Something unpredicted occurred. Our developers have been notified.');
			networkError &&
				bugsnagClient.notify(networkError, {
					beforeSend: (report) => {
						report.metaData = {
							operation: operationInfo
						};

						report.user = userInfo;
					}
				});
		} else if (graphQLErrors) {
			graphQLErrors.forEach((item) =>
				bugsnagClient.notify(new Error(item.message), {
					beforeSend: (report) => {
						report.metaData = {
							graphql: item,
							operation: operationInfo
						};

						report.user = userInfo;
					}
				})
			);
		}
	}),
	createUploadLink({ uri: process.env.REACT_APP_API_URL, credentials: 'same-origin' })
]);

const cache = new InMemoryCache({
	cacheRedirects: {
		Query: {
			campaign: (_, args) => toIdValue(cache.config.dataIdFromObject({ __typename: 'Campaign', id: args.id }))
		}
	}
});

const client = new ApolloClient({
	link: authLink.concat(apolloLink),
	cache,
	connectToDevTools: process.env.NODE_ENV !== 'production'
});

const ErrorBoundary = bugsnagClient.use(createPlugin(React));

preloadFonts();

const content = (
	<BugsnagContext.Provider value={{ client: bugsnagClient }}>
		<ErrorBoundary>
			<ApolloProvider client={client}>
				<Provider store={initStore(client)}>
					<I18nextProvider i18n={i18n}>
						<IntlProvider locale={'en'}>
							<BrowserRouter>
								<ThemeContextProvider>
									<UserContextProvider>
										<ToastContextProvider>
											<App />
										</ToastContextProvider>
									</UserContextProvider>
								</ThemeContextProvider>
							</BrowserRouter>
						</IntlProvider>
					</I18nextProvider>
				</Provider>
			</ApolloProvider>
		</ErrorBoundary>
	</BugsnagContext.Provider>
);

ReactDOM.render(content, document.getElementById('root'));

// Cache problems of index.html when using service worker. Try to fix and restore later.
//registerServiceWorker()
unregister();
