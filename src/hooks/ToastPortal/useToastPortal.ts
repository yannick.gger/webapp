import { useState, useEffect } from 'react';

import uuid from 'shared/helpers/uuid';

const useToastPortal = () => {
	const [loaded, setLoaded] = useState(false);
	const [portalId] = useState(`toast-portal-${uuid()}`);

	const removeDiv = (div: HTMLDivElement) => {
		document.getElementsByTagName('body')[0].removeChild(div);
	};

	useEffect(() => {
		const div = document.createElement('div') as HTMLDivElement;
		div.id = portalId;
		div.setAttribute('style', 'position: fixed; bottom: 24px; left: 24px; z-index: 1');
		document.getElementsByTagName('body')[0].append(div);

		setLoaded(true);

		return () => removeDiv(div);
	}, [portalId]);

	return { loaded, portalId };
};

export default useToastPortal;
