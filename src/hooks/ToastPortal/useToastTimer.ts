import { useState, useEffect } from 'react';

const useToastTimer = (delay: number) => {
	const [timerId, setTimerId] = useState<any>();
	const [startTime, setStartTime] = useState<any>();
	const [remaining, setRemaining] = useState<any>(delay);
	const [finished, setFinished] = useState(false);

	const pause = () => {
		window.clearTimeout(timerId);
		setRemaining(remaining - (Date.now() - startTime));
	};

	const resume = () => {
		if (!finished) {
			setTimerId(
				setTimeout(() => {
					setFinished(true);
				}, remaining)
			);
			setStartTime(Date.now());
		}
	};

	useEffect(() => {
		const timer = setTimeout(() => {
			setFinished(true);
		}, delay);

		setTimerId(timer);
		setStartTime(Date.now());
	}, []);

	return { finished, pause, resume };
};

export default useToastTimer;
