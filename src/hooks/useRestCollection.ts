import useSWR from 'swr';
import { CollectionResponse } from '../types/http';
import { createClient } from '../shared/ApiClient/ApiClient';

const client = createClient();

export default function useRestCollection<T>(url: string) {
	const fetcher = async (url: string) => {
		const { data } = await client.get<CollectionResponse<T>>(url);

		return data;
	};

	const { data, error } = useSWR(url, fetcher);

	return {
		data,
		error,
		loading: !data && !error
	};
}
