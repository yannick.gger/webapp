import ImpressionService from 'services/Statistics/Impression';
import TrafficService from 'services/Statistics/Traffic';
import EngagementService from 'services/Statistics/Engagement';
import { formatDate } from 'shared/helpers/Chart/chart-util';

const useTrafficData = () => {
	const getTrafficLinkTotal = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return TrafficService.fetchTrafficLinkTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return res.data.attributes.count;
		});
	};

	const getTrafficBrandMentionTotal = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagementTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data.attributes.brandClicks;
		});
	};

	const getTrafficLinkCounts = (props: { from: any; to: any; frequancy: any; campaigns?: Array<number> }) => {
		return TrafficService.fetchTrafficLinkCounts({
			from: formatDate(props.from),
			to: formatDate(props.to),
			frequency: props.frequancy,
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data;
		});
	};

	const getTrafficBrandMentionCounts = (props: { from: any; to: any; frequancy: any; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagements({
			from: formatDate(props.from),
			to: formatDate(props.to),
			frequency: props.frequancy,
			campaigns: props.campaigns
		}).then((res: any) => {
			const data = res.data.map((item: { attributes: { date: string; brandClicks: number } }) => {
				return {
					attributes: {
						date: item.attributes.date,
						count: item.attributes.brandClicks
					}
				};
			});
			return data;
		});
	};

	const getTrafficCTRTotal = (props: { from: Date; to: Date; campaigns?: Array<number>; platforms?: any }) => {
		const getLinkTotal = TrafficService.fetchTrafficLinkTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then(
			(res: any) => {
				return res.data.attributes.count;
			}
		);
		const getImpressionTotal = ImpressionService.fetchImpressionTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			platforms: props.platforms
		}).then((res: any) => {
			return res.data.attributes.count;
		});

		return Promise.all([getLinkTotal, getImpressionTotal]).then((values) => {
			return values[0] > 0 ? values[0] / values[1] : 0;
		});
	};

	const getTrafficGenderByPlatforms = (props: { from: Date; to: Date; campaigns?: Array<number>; platforms?: any }) => {
		return TrafficService.fetchTrafficGenderByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			platforms: props.platforms
		}).then((res: any) => {
			const data = Object.values(res.data.attributes);
			const labels = Object.keys(res.data.attributes).map((el) => {
				switch (el.toLocaleLowerCase()) {
					case 'male':
					case 'man':
					case 'men':
						return 'Men';
					case 'female':
					case 'woman':
					case 'women':
						return 'Women';
					case 'unknown':
						return 'Others';
					default:
						return 'Others';
				}
			});
			const total = data.reduce((prev: number, current: any) => prev + current, 0);
			return { total: total, labels: labels, data: data };
		});
	};

	const getTrafficCountry = (props: { from: any; to: any; campaigns?: Array<number> }) => {
		return TrafficService.fetchTrafficCountry({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			const result = res.data.map(
				(countrySummary: { type: string; id: string; attributes: { name: string; alpha2code: string; alpha3code: string; followers: number } }) => {
					return countrySummary.attributes;
				}
			);
			return result;
		});
	};

	return {
		getTrafficLinkTotal,
		getTrafficBrandMentionTotal,
		getTrafficLinkCounts,
		getTrafficBrandMentionCounts,
		getTrafficCTRTotal,
		getTrafficGenderByPlatforms,
		getTrafficCountry
	};
};

export default useTrafficData;
