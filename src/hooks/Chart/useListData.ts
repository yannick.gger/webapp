import ListsService from 'services/Lists/Lists.service';

const useListData = () => {
	const getLists = () => {
		return ListsService.getLists().then((res: any) => {
			const result: { id: string; itemName: string }[] = res.data.map((data: { type: string; id: string; attributes: { name: string } }) => {
				return { id: data.id, itemName: data.attributes.name };
			});
			result.sort((a: any, b: any) => Number(b.id) - Number(a.id));
			return result;
		});
	};

	const addInfluencersToList = (listId: string, influencers: string[]) => {
		return ListsService.addInfluencerToList(influencers, listId).then((res: any) => {
			return res;
		});
	};

	return {
		getLists,
		addInfluencersToList
	};
};

export default useListData;
