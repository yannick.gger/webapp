import DashboardService from 'services/Statistics/Dashboard';
import { DashboardItem, DashboardRequestBody } from 'services/Statistics/Dashboard/types';

const useDataLibraryDashboard = () => {
	const getDashboards = () => {
		return DashboardService.fetchDashboards().then((res: any) => {
			const result = res.data;
			return result;
		});
	};

	const getOneDashboard = (id: string) => {
		return DashboardService.fetchOneDashboard(id).then((res: any) => {
			const result = res.data;
			return result;
		});
	};

	const createDashboard = (props: { name: string }) => {
		return DashboardService.createDashboard({ name: props.name }).then((res: any) => {
			const result = res;
			return result;
		});
	};

	const appendChartInDashboard = (props: { id: string; data: DashboardItem }) => {
		return DashboardService.appendChartInDashboard({ id: props.id, data: props.data }).then((res: any) => {
			const result = res;
			return result;
		});
	};

	const updateDashboard = (props: { id: string; data: DashboardRequestBody }) => {
		return DashboardService.updateDashboard({ id: props.id, data: props.data }).then((res: any) => {
			const result = res;
			return result;
		});
	};

	const deleteDashboard = (id: string) => {
		return DashboardService.deleteDashboard(id).then((res: any) => {
			const result = res;
			console.log(res);
			return result;
		});
	};

	return {
		getDashboards,
		getOneDashboard,
		createDashboard,
		appendChartInDashboard,
		updateDashboard,
		deleteDashboard
	};
};

export default useDataLibraryDashboard;
