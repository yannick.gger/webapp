import InfluencerService from 'services/Statistics/Influencer';
import { formatDate } from 'shared/helpers/Chart/chart-util';

class InfluencerData {
	userId: number;
	name: string;
	username: string;
	profileImage: string;
	followers: number;
	reach: number;
	impressions: number;
	comments: number;
	likes: number;
	constructor(
		userId: string,
		name: string,
		profileImage: string,
		followers: number,
		reach: number,
		impressions: number,
		comments: number,
		likes: number,
		username: string
	) {
		this.userId = Number(userId);
		this.name = name;
		this.username = username;
		this.profileImage = profileImage;
		this.followers = followers;
		this.reach = reach;
		this.impressions = impressions;
		this.comments = comments;
		this.likes = likes;
	}
}

const useInfluencerData = () => {
	const combineDataByUserId = (uniqueIds: string[], list: any[]) => {
		const result: InfluencerData[] = [];

		uniqueIds.forEach((userId) => {
			const influencerData = list.filter((influencer: any) => influencer.id === userId);
			const influencer = {
				userId: userId,
				name: influencerData[0].attributes.name,
				profileImage: influencerData[0].links.profileImage,
				followers: influencerData[0].attributes.followers,
				username: influencerData[0].attributes.username
			};
			const combinedReach = influencerData.reduce((prev: number, current: any) => prev + current.attributes.reach, 0);
			const combinedImpressions = influencerData.reduce((prev: number, current: any) => prev + current.attributes.impressions, 0);
			const combinedComments = influencerData.reduce((prev: number, current: any) => prev + current.attributes.comments, 0);
			const combinedLikes = influencerData.reduce((prev: number, current: any) => prev + current.attributes.likes, 0);

			result.push(
				new InfluencerData(
					influencer.userId,
					influencer.name,
					influencer.profileImage,
					influencer.followers,
					combinedReach,
					combinedImpressions,
					combinedComments,
					combinedLikes,
					influencer.username
				)
			);
		});

		return result;
	};

	const getInfluencerTotal = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return InfluencerService.fetchInfluencerTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			const uniqueInfluencerIds: string[] = Array.from(new Set(res.data.map((influencer: any) => influencer.id)));
			return combineDataByUserId(uniqueInfluencerIds, res.data);
		});
	};

	const getInfluencerTotalByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return InfluencerService.fetchInfluencerTotalByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			const uniqueInfluencerIds: string[] = Array.from(new Set(res.data.map((influencer: any) => influencer.id)));
			return combineDataByUserId(uniqueInfluencerIds, res.data);
		});
	};

	return {
		getInfluencerTotal,
		getInfluencerTotalByPlatforms
	};
};

export default useInfluencerData;
