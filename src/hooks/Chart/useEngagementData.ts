import EngagementService from 'services/Statistics/Engagement';
import InfluencerService from 'services/Statistics/Influencer';
import { formatDate } from 'shared/helpers/Chart/chart-util';

const useEngagementData = () => {
	const getEngagementTotal = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagementTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return res.data.attributes;
		});
	};

	const getEngagementTotalByPlatforms = (props: { from: Date; to: Date; campaigns?: Array<number>; platforms: string }) => {
		return EngagementService.fetchEngagementTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			platforms: props.platforms
		}).then((res: any) => {
			return res.data.attributes;
		});
	};

	const getEngagementRate = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagementTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			const engagementTotal = res.data.attributes;
			const rate = ((engagementTotal.likes + engagementTotal.comments) / engagementTotal.followers) * 100;
			return `${rate ? rate.toFixed(2) : 0}%`;
		});
	};

	const getEngagementActualRate = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagementTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			const engagementTotal = res.data.attributes;
			const actualRate = ((engagementTotal.likes + engagementTotal.comments) / engagementTotal.reach) * 100;
			return `${actualRate ? actualRate.toFixed(2) : 0}%`;
		});
	};

	const getEngagementLikes = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagementTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return res.data.attributes.likes;
		});
	};

	const getEngagementComments = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagementTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return res.data.attributes.comments;
		});
	};

	const getEngagementFollowers = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagementTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return res.data.attributes.followers;
		});
	};

	const getEngagementImpressionsByPlatforms = (props: { from: Date; to: Date; campaigns?: Array<number>; platforms: string }) => {
		return EngagementService.fetchEngagementTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			platforms: props.platforms
		}).then((res: any) => {
			return res.data.attributes.impressions;
		});
	};

	const getEngagementCounts = (props: { from: any; to: any; frequancy: any; campaigns?: Array<number> }) => {
		return EngagementService.fetchEngagements({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			frequency: props.frequancy
		}).then((res) => {
			let result = [];
			if (res.data && res.data.length > 0) {
				result = res.data.map((item: { attributes: { date: string; impressions: number; reach: number; likes: number; comments: number } }) => ({
					attributes: {
						date: item.attributes.date,
						count: item.attributes.likes + item.attributes.comments
					}
				}));
			}
			return result;
		});
	};

	const getEngagementTotalPerInfluencer = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return InfluencerService.fetchInfluencerTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res) => {
			return res.data;
		});
	};

	const getEngagementTotalPerInfluencerByPlatforms = (props: { from: Date; to: Date; campaigns?: Array<number>; platforms: string }) => {
		return InfluencerService.fetchInfluencerTotalByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			platforms: props.platforms
		}).then((res) => {
			return res.data;
		});
	};

	return {
		getEngagementTotal,
		getEngagementTotalByPlatforms,
		getEngagementRate,
		getEngagementActualRate,
		getEngagementLikes,
		getEngagementComments,
		getEngagementFollowers,
		getEngagementImpressionsByPlatforms,
		getEngagementCounts,
		getEngagementTotalPerInfluencer,
		getEngagementTotalPerInfluencerByPlatforms
	};
};

export default useEngagementData;
