import CampaignService from 'services/Statistics/Campaign';

const useCampaignData = () => {
	const getCampaigns = (props?: { statuses?: string[] }) => {
		let statuses: Array<string> = [];

		if (props && props.statuses) {
			statuses = props.statuses;
		}

		return CampaignService.fetchCampaigns({ statuses: statuses }).then((res: any) => {
			const result: { id: string; itemName: string }[] = res.data.map((data: { type: string; id: string; attributes: { name: string } }) => {
				return { id: data.id, itemName: data.attributes.name };
			});
			result.sort((a: any, b: any) => Number(b.id) - Number(a.id));
			return result;
		});
	};

	const setInfluencersToCampaign = (camapignId: string, influencers: string[]) => {
		return CampaignService.setInfluencersToCampaign(camapignId, influencers).then((res) => {
			return res;
		});
	};

	return {
		getCampaigns,
		setInfluencersToCampaign
	};
};

export default useCampaignData;
