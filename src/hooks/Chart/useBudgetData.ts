import BudgetService from 'services/Statistics/Budget';
import TrafficService from 'services/Statistics/Traffic';
import { formatDate, formatNumber } from 'shared/helpers/Chart/chart-util';

const useBudgetData = () => {
	const getBudgetTotal = (props: { from: Date; to: Date; campaigns?: Array<number>; currency?: string }) => {
		return BudgetService.fetchBudgetTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			currency: props.currency
		}).then((res: any) => {
			const totalBugets = {
				currency: res.data.attributes.currency,
				value: `${formatNumber(res.data.attributes.amount)} ${res.data.attributes.currency}`
			};

			return totalBugets.value;
		});
	};

	const getBudgets = (props: { from: any; to: any; frequancy: any; campaigns?: Array<number>; currency?: string }) => {
		return BudgetService.fetchBudgets({
			from: formatDate(props.from),
			to: formatDate(props.to),
			frequency: props.frequancy,
			campaigns: props.campaigns,
			currency: props.currency
		}).then((res: any) => {
			return res.data;
		});
	};

	const getBudgetsByCountry = (props: { from: any; to: any; campaigns?: Array<number>; currency?: string }) => {
		return BudgetService.fetchBudgetsPerCountry({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			currency: props.currency
		}).then((res: any) => {
			return res.data.map((item: { type: string; id: string; attributes: { country: string; amount: number; currency: string } }) => {
				return item.attributes;
			});
		});
	};

	const getBudgetCPM = (props: { from: any; to: any; campaigns?: Array<number>; currency?: string }) => {
		return BudgetService.fetchBudgetCPM({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			currency: props.currency
		}).then((res: any) => {
			const totalCPM = {
				currency: res.data.attributes.currency,
				value: `${formatNumber(res.data.attributes.amount)} ${res.data.attributes.currency}`
			};
			return totalCPM.value;
		});
	};

	const getBudgetCPC = (props: { from: any; to: any; campaigns?: Array<number>; currency?: string }) => {
		const totalBudget = BudgetService.fetchBudgetTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns,
			currency: props.currency
		}).then((res: any) => {
			return {
				currency: res.data.attributes.currency,
				value: formatNumber(res.data.attributes.amount)
			};
		});

		const totalClicks = TrafficService.fetchTrafficLinkTotal({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res) => {
			return res.data.attributes.count;
		});

		return Promise.all([totalBudget, totalClicks]).then((values) => {
			if (values[0].value > 0 && values[1] > 0) {
				return `${formatNumber(values[0].value / values[1])} ${values[0].currency}`;
			} else if (values[0].value === 0) {
				return '0';
			} else {
				return 'N/A';
			}
		});
	};

	return {
		getBudgetTotal,
		getBudgets,
		getBudgetsByCountry,
		getBudgetCPM,
		getBudgetCPC
	};
};

export default useBudgetData;
