import ReachService from 'services/Statistics/Reach';
import { formatDate } from 'shared/helpers/Chart/chart-util';

const useReachData = () => {
	const getReachTotal = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return ReachService.fetchReachTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return res.data.attributes.count;
		});
	};

	const getReachTotalByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return ReachService.fetchReachTotalByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data.attributes.count;
		});
	};

	const getGrossReachTotal = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return ReachService.fetchGrossReachTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return `${(res.data.attributes.decimal * 100).toFixed(2)}%`;
		});
	};

	const getGrossReachTotalByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return ReachService.fetchGrossReachTotalByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			return `${(res.data.attributes.decimal * 100).toFixed(2)}%`;
		});
	};

	const getReachCounts = (props: { from: any; to: any; frequancy: any; campaigns?: Array<number> }) => {
		return ReachService.fetchReachCounts({
			from: formatDate(props.from),
			to: formatDate(props.to),
			frequency: props.frequancy,
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data;
		});
	};

	const getReachCountsByPlatforms = (props: { from: any; to: any; frequancy: any; platforms: any; campaigns?: Array<number> }) => {
		return ReachService.fetchReachCountsByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			frequency: props.frequancy,
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data;
		});
	};

	const getReachGender = (props: { from: any; to: any; campaigns?: Array<number> }) => {
		return ReachService.fetchReachGender({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			const data = Object.values(res.data.attributes);
			const labels = Object.keys(res.data.attributes).map((el) => {
				switch (el.toLocaleLowerCase()) {
					case 'male':
					case 'man':
					case 'men':
						return 'Men';
					case 'female':
					case 'woman':
					case 'women':
						return 'Women';
					case 'unknown':
						return 'Others';
					default:
						return 'Others';
				}
			});
			const total = data.reduce((prev: number, current: any) => prev + current, 0);
			return { total: total, labels: labels, data: data };
		});
	};

	const getReachGenderByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return ReachService.fetchReachGenderByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			const data = Object.values(res.data.attributes);
			const labels = Object.keys(res.data.attributes).map((el) => el.toUpperCase());
			const total = data.reduce((prev: number, current: any) => prev + current, 0);
			return { total: total, labels: labels, data: data };
		});
	};

	const getReachCountry = (props: { from: any; to: any; campaigns?: Array<number> }) => {
		return ReachService.fetchReachCountry({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			const result = res.data.map(
				(countrySummary: { type: string; id: string; attributes: { name: string; alpha2code: string; alpha3code: string; followers: number } }) => {
					return countrySummary.attributes;
				}
			);
			return result;
		});
	};

	const getReachCountryByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return ReachService.fetchReachCountryByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			const result = res.data.map(
				(countrySummary: { type: string; id: string; attributes: { name: string; alpha2code: string; alpha3code: string; followers: number } }) => {
					return countrySummary.attributes;
				}
			);
			return result;
		});
	};

	return {
		getReachTotal,
		getReachTotalByPlatforms,
		getGrossReachTotal,
		getGrossReachTotalByPlatforms,
		getReachCounts,
		getReachCountsByPlatforms,
		getReachGender,
		getReachGenderByPlatforms,
		getReachCountry,
		getReachCountryByPlatforms
	};
};

export default useReachData;
