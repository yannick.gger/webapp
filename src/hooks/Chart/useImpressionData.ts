import ImpressionService from 'services/Statistics/Impression';
import { formatDate } from 'shared/helpers/Chart/chart-util';

const useImpressionData = () => {
	const getImpressionTotal = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionTotal({ from: formatDate(props.from), to: formatDate(props.to), campaigns: props.campaigns }).then((res: any) => {
			return res.data.attributes.count;
		});
	};

	const getImpressionTotalByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionTotalByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data.attributes.count;
		});
	};

	const getImpressionCounts = (props: { from: any; to: any; frequancy: any; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionCounts({
			from: formatDate(props.from),
			to: formatDate(props.to),
			frequency: props.frequancy,
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data;
		});
	};

	const getImpressionCountsByPlatforms = (props: { from: any; to: any; frequancy: any; platforms: any; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionCountsByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			frequency: props.frequancy,
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			return res.data;
		});
	};

	const getImpressionGender = (props: { from: any; to: any; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionGender({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			const data = Object.values(res.data.attributes);
			const labels = Object.keys(res.data.attributes).map((el) => {
				switch (el.toLocaleLowerCase()) {
					case 'male':
					case 'man':
					case 'men':
						return 'Men';
					case 'female':
					case 'woman':
					case 'women':
						return 'Women';
					case 'unknown':
						return 'Others';
					default:
						return 'Others';
				}
			});
			const total = data.reduce((prev: number, current: any) => prev + current, 0);
			return { total: total, labels: labels, data: data };
		});
	};

	const getImpressionGenderByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionGenderByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			const data = Object.values(res.data.attributes);
			const labels = Object.keys(res.data.attributes).map((el) => el.toUpperCase());
			const total = data.reduce((prev: number, current: any) => prev + current, 0);
			return { total: total, labels: labels, data: data };
		});
	};

	const getImpressionCountry = (props: { from: any; to: any; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionCountry({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			const result = res.data.map(
				(countrySummary: { type: string; id: string; attributes: { name: string; alpha2code: string; alpha3code: string; followers: number } }) => {
					return countrySummary.attributes;
				}
			);
			return result;
		});
	};

	const getImpressionCountryByPlatforms = (props: { from: any; to: any; platforms: any; campaigns?: Array<number> }) => {
		return ImpressionService.fetchImpressionCountryByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			const result = res.data.map(
				(countrySummary: { type: string; id: string; attributes: { name: string; alpha2code: string; alpha3code: string; followers: number } }) => {
					return countrySummary.attributes;
				}
			);
			return result;
		});
	};

	return {
		getImpressionTotal,
		getImpressionTotalByPlatforms,
		getImpressionCounts,
		getImpressionCountsByPlatforms,
		getImpressionGender,
		getImpressionGenderByPlatforms,
		getImpressionCountry,
		getImpressionCountryByPlatforms
	};
};

export default useImpressionData;
