import AudienceService from 'services/Statistics/Audience';
import { formatDate } from 'shared/helpers/Chart/chart-util';

const useAudienceData = () => {
	const getAudienceAge = (props: { from: Date; to: Date; campaigns?: Array<number> }) => {
		return AudienceService.fetchAudienceAge({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: { data: { attributes: { [key: string]: number } } }) => {
			let result = {
				total: Object.values(res.data.attributes).reduce((prev: number, current: number) => prev + current, 0),
				ages: Object.keys(res.data.attributes).map((item: string) => {
					return { age: item, value: res.data.attributes[item] };
				})
			};
			return result;
		});
	};

	const getAudienceAgeByPlatforms = (props: { from: any; to: any; platforms: 'instagram-post' | 'instagram-story'; campaigns?: Array<number> }) => {
		return AudienceService.fetchAudienceAgeByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: { data: { attributes: { [key: string]: number } } }) => {
			let result = {
				total: Object.values(res.data.attributes).reduce((prev: number, current: number) => prev + current, 0),
				ages: Object.keys(res.data.attributes).map((item: string) => {
					return { age: item, value: res.data.attributes[item] };
				})
			};
			return result;
		});
	};

	const getAudienceCountry = (props: { from: any; to: any; campaigns?: Array<number> }) => {
		return AudienceService.fetchAudienceCountry({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			const result = res.data.map(
				(countrySummary: { type: string; id: string; attributes: { name: string; alpha2code: string; alpha3code: string; followers: number } }) => {
					return countrySummary.attributes;
				}
			);
			return result;
		});
	};

	const getAudienceCountryByPlatforms = (props: { from: any; to: any; platforms: 'instagram-post' | 'instagram-story'; campaigns?: Array<number> }) => {
		return AudienceService.fetchAudienceCountryByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			const result = res.data.map(
				(countrySummary: { type: string; id: string; attributes: { name: string; alpha2code: string; alpha3code: string; followers: number } }) => {
					return countrySummary.attributes;
				}
			);
			return result;
		});
	};

	const getAudienceGender = (props: { from: any; to: any; campaigns?: Array<number> }) => {
		return AudienceService.fetchAudienceGender({
			from: formatDate(props.from),
			to: formatDate(props.to),
			campaigns: props.campaigns
		}).then((res: any) => {
			const data = Object.values(res.data.attributes);
			const labels = Object.keys(res.data.attributes).map((el) => {
				switch (el.toLocaleLowerCase()) {
					case 'male':
					case 'man':
					case 'men':
						return 'Men';
					case 'female':
					case 'woman':
					case 'women':
						return 'Women';
					case 'unknown':
						return 'Others';
					default:
						return 'Others';
				}
			});
			const total = data.reduce((prev: number, current: any) => prev + current, 0);
			return { total: total, labels: labels, data: data };
		});
	};

	const getAudienceGenderByPlatforms = (props: { from: any; to: any; platforms: 'instagram-post' | 'instagram-story'; campaigns?: Array<number> }) => {
		return AudienceService.fetchAudienceCountryByPlatforms({
			from: formatDate(props.from),
			to: formatDate(props.to),
			platforms: props.platforms,
			campaigns: props.campaigns
		}).then((res: any) => {
			const data = Object.values(res.data.attributes);
			const labels = Object.keys(res.data.attributes).map((el) => el.toUpperCase());
			const total = data.reduce((prev: number, current: any) => prev + current, 0);
			return { total: total, labels: labels, data: data };
		});
	};

	return {
		getAudienceAge,
		getAudienceAgeByPlatforms,
		getAudienceCountry,
		getAudienceCountryByPlatforms,
		getAudienceGender,
		getAudienceGenderByPlatforms
	};
};

export default useAudienceData;
