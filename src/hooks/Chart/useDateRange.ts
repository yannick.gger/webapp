import { getMonthBeforeDate } from 'shared/helpers/Chart/chart-util';
import { TODAY, WEEK, MONTH } from 'constants/data-library';

const useDateRange = (isDashboard: boolean | undefined, defaultPeriod: string | undefined, dashboardPeriod: string | undefined) => {
	let from: Date = getMonthBeforeDate(new Date());
	let to: Date = new Date();
	let dates;

	if (isDashboard && dashboardPeriod) {
		dates = dashboardPeriod.split('~');
		from = new Date(dates[0]);
		to = new Date(dates[1]);
	} else if (!dashboardPeriod && defaultPeriod) {
		if (defaultPeriod === TODAY) {
			from = new Date();
			to = new Date();
		} else if (defaultPeriod === WEEK) {
			const today = new Date();
			const latestWeek = new Date();
			latestWeek.setDate(today.getDate() - 6);

			from = latestWeek;
			to = today;
		} else if (defaultPeriod === MONTH) {
			from = getMonthBeforeDate(new Date());
			to = new Date();
		} else if (defaultPeriod.includes('~')) {
			dates = defaultPeriod.split('~');

			from = new Date(dates[0]);
			to = new Date(dates[1]);
		}
	} else {
		from = getMonthBeforeDate(new Date());
		to = new Date();
	}
	return { defaultFrom: from, defaultTo: to };
};

export default useDateRange;
