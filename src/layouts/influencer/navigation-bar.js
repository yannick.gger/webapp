import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Avatar, Dropdown } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { ApolloConsumer } from 'react-apollo';
import { translate } from 'react-i18next';
import i18next from 'i18next';
import styled from 'styled-components';

import { ButtonLink } from 'components';
import { signOut } from 'services/auth-service';
import { CollabIcon, CampaignIcon, DashboardIcon, SettingIcon, LogoutIcon } from 'components/icons';

const NavWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0 1rem;
`;

const Styled = {
	NavWrapper
};

const SubMenu = Menu.SubMenu;

class NavigationBar extends Component {
	componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			this.onRouteChanged();
		}
	}

	organizationSlug = this.props.location.pathname.split('/')[1];

	menuItems = [
		{
			key: 'campaigns',
			to: `/influencer/campaigns`,
			svg: <CampaignIcon />,
			text: 'Campaigns'
		}
	];

	getActiveRoute = () => {
		const primaryActiveRoute = [
			...this.menuItems.filter((menuItem) => !menuItem.subItems),
			...this.menuItems.map((menuItem) => (menuItem.subItems ? menuItem.subItems : []))
		]
			.reduce((accumulator, currentValue) => accumulator.concat(currentValue), [])
			.find(({ to }) => to === this.props.location.pathname);

		const secondaryActiveRoute = [...this.menuItems, ...this.menuItems.map((menuItem) => (menuItem.subItems ? menuItem.subItems : []))]
			.reduce((accumulator, currentValue) => accumulator.concat(currentValue), [])
			.find(({ to }) => this.props.location.pathname.includes(to));

		return primaryActiveRoute ? primaryActiveRoute : secondaryActiveRoute;
	};

	getActiveCategory = () => {
		const primaryActiveCategory = [...this.menuItems].find(({ subItems }) => subItems && subItems.find(({ to }) => to === this.props.location.pathname));

		const secondaryActiveCategory = [...this.menuItems, ...this.menuItems.map((menuItem) => (menuItem.subItems ? menuItem.subItems : []))]
			.reduce((accumulator, currentValue) => accumulator.concat(currentValue), [])
			.find(({ to }) => this.props.location.pathname.includes(to));

		return primaryActiveCategory ? primaryActiveCategory : secondaryActiveCategory;
	};

	getActiveCategoryKey = () => (this.getActiveCategory() ? this.getActiveCategory().key : undefined);

	state = {
		openKeys: [this.getActiveCategoryKey()],
		visible: false
	};

	onRouteChanged() {
		this.setState({
			openKeys: [this.getActiveCategoryKey()]
		});
	}

	handleMenuClick = () => {
		this.setState({ visible: false });
	};

	handleVisibleChange = (flag) => {
		this.setState({ visible: flag });
	};

	render() {
		const { currentUser } = this.props;

		const userMenu = (
			<ApolloConsumer>
				{(client) => (
					<Menu className='dropdown-menu-in-navigation-bar' onClick={this.handleMenuClick}>
						<Menu.Item className='hide-md-and-larger'>
							<Link to='/influencer/campaigns'>
								<i className='mr-15'>
									<CampaignIcon />
								</i>
								{i18next.t('default:layout.navBar.menu.campaigns', { defaultValue: 'Campaigns' })}
							</Link>
						</Menu.Item>
						<Menu.Divider className='hide-md-and-larger' />
						<Menu.Item>
							<Link to='/influencer/settings'>
								<i className='mr-15'>
									<SettingIcon />
								</i>
								{i18next.t('default:layout.navBar.menu.settings', { defaultValue: 'Settings' })}
							</Link>
						</Menu.Item>
						<Menu.Divider />
						<Menu.Item>
							<ButtonLink onClick={() => signOut(client, this.props.history)} className='d-flex'>
								<i className='mr-15'>
									<LogoutIcon />
								</i>
								{i18next.t('default:layout.navBar.menu.logout', { defaultValue: 'Logout' })}
							</ButtonLink>
						</Menu.Item>
					</Menu>
				)}
			</ApolloConsumer>
		);

		return (
			<Styled.NavWrapper className='navigation-bar-influencer-layouts dark-mode influencer-view'>
				<Link to='/influencer/campaigns' className='logo-navigation-bar'>
					<CollabIcon />
				</Link>

				<Menu
					theme='dark'
					mode='horizontal'
					openKeys={this.state.openKeys}
					defaultOpenKeys={this.state.openKeys}
					selectedKeys={[this.getActiveRoute() ? this.getActiveRoute().key : this.getActiveCategory() ? this.getActiveCategoryKey() : 'none']}
				>
					{this.menuItems.map((menuItem) => {
						return (
							<Menu.Item key={menuItem.key}>
								<Link to={menuItem.to}>
									<i className='lg-nav-icon'>{menuItem.svg}</i>
									<span>{menuItem.text}</span>
								</Link>
							</Menu.Item>
						);
					})}
				</Menu>

				<React.Fragment>
					<Dropdown
						className='fr'
						overlay={userMenu}
						placement='bottomRight'
						trigger={['click']}
						onVisibleChange={this.handleVisibleChange}
						visible={this.state.visible}
					>
						<Avatar src='#' />
					</Dropdown>
				</React.Fragment>
			</Styled.NavWrapper>
		);
	}
}

NavigationBar.contextTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}),
	location: PropTypes.object
};

export default withRouter(translate('default')(NavigationBar));
