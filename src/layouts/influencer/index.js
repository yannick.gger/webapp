import React, { Component } from 'react';
import { Layout } from 'antd';
import { ApolloConsumer, Query } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import getInfluencerAvatarQuery from 'graphql/get-influencer-avatar.graphql';
import { UserContext } from '../../contexts';
import { translate } from 'react-i18next';

import NavigationBar from './navigation-bar';
import DontForgetTheAppModal from 'components/dont-forget-the-app';
import { hasGhostToken, signOutAsGhost } from 'services/auth-service';

import './styles.scss';

const { Content, Header } = Layout;

class LoggedInLayout extends Component {
	static contextType = UserContext;

	state = {
		modalVisible: !!(localStorage && localStorage.getItem('signedUp') && localStorage.getItem('joinedCampaign'))
	};

	componentDidUpdate = () => {
		if (localStorage && localStorage.getItem('signedUp') && localStorage.getItem('joinedCampaign'))
			this.setState({ modalVisible: true }, () => {
				localStorage.removeItem('signedUp');
				localStorage.removeItem('joinedCampaign');
			});
	};

	onCloseModal = () => {
		localStorage.removeItem('signedUp');
		localStorage.removeItem('joinedCampaign');
		this.setState({ modalVisible: false });
	};

	render() {
		const { modalVisible } = this.state;
		const { t } = this.props;

		return (
			<Layout className='layout'>
				<DontForgetTheAppModal visible={modalVisible} onCancel={this.onCloseModal} />
				{hasGhostToken() && (
					<ApolloConsumer>
						{(client) => (
							<Header className='header-ghosting'>
								<span>{t('influencer:layoutsInfluencerGhostUserComment')}</span>
								<a
									onClick={() => {
										signOutAsGhost(client, this.props.history);
										this.context.updateIsGhostUser();
									}}
								>
									{t('default:signout')}
								</a>
							</Header>
						)}
					</ApolloConsumer>
				)}
				<Query query={getInfluencerAvatarQuery}>
					{({ data }) => {
						return <NavigationBar currentUser={data && data.currentUser} />;
					}}
				</Query>
				<Layout>
					<Content className='content-view'>
						<div>{this.props.children}</div>
					</Content>
				</Layout>
			</Layout>
		);
	}
}

export default withRouter(translate('influencer')(LoggedInLayout));
