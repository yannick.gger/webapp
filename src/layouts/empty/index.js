import React, { Component } from 'react';
import { Layout } from 'antd';

import './styles.scss';

const { Content } = Layout;

class EmptyLayout extends Component {
	render() {
		return (
			<Layout style={{ minHeight: '100vh' }}>
				<Content>{this.props.children}</Content>
			</Layout>
		);
	}
}

export default EmptyLayout;
