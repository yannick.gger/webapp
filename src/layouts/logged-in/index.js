import React, { useContext, useEffect } from 'react';
import { ApolloConsumer } from 'react-apollo';
import { connect } from 'react-redux';
import { Layout } from 'antd';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import { withOrganization } from 'hocs';
import { isImpersonating, stopImpersonating } from 'services/auth-service';
import { onSearchFeatureRequest } from 'actions/current-user';
import { setOrganization } from 'shared/utils';
import { UserContext } from 'contexts';
import Header from 'components/Header';
import './styles.scss';

export const mapDispatchToProps = (dispatch) => ({
	onRefetch: () => dispatch(onSearchFeatureRequest())
});

const LoggedInLayout = (props: any) => {
	const userContext = useContext(UserContext);

	useEffect(() => {
		const { organizationSlug } = props.match.params;

		props.onRefetch();
		setOrganization(organizationSlug); // set org
	}, []);

	return (
		<Layout className='layout main-content'>
			<Header menuItems={props.sidebarMenuItems} location={props.location} fixed={true} />
			{isImpersonating() && (
				<ApolloConsumer>
					{(client) => (
						<div className='header-ghosting'>
							<span>You're impersonating a user...</span>
							<a
								onClick={() => {
									stopImpersonating(client, props.history);
									userContext.updateIsGhostUser();
								}}
							>
								{i18next.t('default:signout')}
							</a>
						</div>
					)}
				</ApolloConsumer>
			)}
			<Layout className={'organization-layout'}>{props.children}</Layout>
		</Layout>
	);
};

export default withOrganization(
	connect(
		null,
		mapDispatchToProps
	)(translate('default')(LoggedInLayout))
);
