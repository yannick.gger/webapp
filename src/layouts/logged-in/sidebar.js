import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Spin, Badge } from 'antd';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { isMenuItemHidden } from 'shared/utils';

const { Sider } = Layout;

const SubMenu = Menu.SubMenu;

/**
 * @deprecated do not use this component anymore!
 */
class Sidebar extends Component {
	componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			this.onRouteChanged();
		}
	}

	onCollapse = (collapsed) => {
		localStorage.setItem('menu-collapsed', collapsed);
		this.setState({ collapsed });
	};

	getActiveRoute = () => {
		const { menuItems } = this.props;
		const primaryActiveRoute = [
			...menuItems.filter((menuItem) => !menuItem.subItems),
			...menuItems.map((menuItem) => (menuItem.subItems ? menuItem.subItems : []))
		]
			.reduce((accumulator, currentValue) => accumulator.concat(currentValue), [])
			.find(({ to }) => to === this.props.location.pathname);

		const secondaryActiveRoute = [...menuItems, ...menuItems.map((menuItem) => (menuItem.subItems ? menuItem.subItems : []))]
			.reduce((accumulator, currentValue) => accumulator.concat(currentValue), [])
			.find(({ to }) => this.props.location.pathname.includes(to));

		return primaryActiveRoute ? primaryActiveRoute : secondaryActiveRoute;
	};

	getActiveCategory = () => {
		const { menuItems } = this.props;
		const primaryActiveCategory = [...menuItems].find(({ subItems }) => subItems && subItems.find(({ to }) => to === this.props.location.pathname));

		const secondaryActiveCategory = [...menuItems, ...menuItems.map((menuItem) => (menuItem.subItems ? menuItem.subItems : []))]
			.reduce((accumulator, currentValue) => accumulator.concat(currentValue), [])
			.find(({ to }) => this.props.location.pathname.includes(to));

		return primaryActiveCategory ? primaryActiveCategory : secondaryActiveCategory;
	};

	getActiveCategoryKey = () => (this.getActiveCategory() ? this.getActiveCategory().key : undefined);

	state = {
		collapsed: localStorage.getItem('menu-collapsed') === 'true',
		openKeysNonCollapsed: [this.getActiveCategoryKey()],
		openKeysCollapsed: []
	};

	onRouteChanged() {
		this.setState({
			openKeysNonCollapsed: [this.getActiveCategoryKey()]
		});
	}

	render() {
		const logoStyle = this.props.logo ? { backgroundImage: `url(${this.props.logo})`, backgroundSize: 'auto 50%' } : {};
		return (
			<Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse} width={280}>
				{this.props.logoLoading && (
					<div className='logo' style={{ background: 'none' }}>
						<Spin className='collabspin' style={{ background: 'none' }} />
					</div>
				)}
				{!this.props.logoLoading && <div className='logo' style={logoStyle} />}
				<Menu
					theme='dark'
					selectedKeys={[this.getActiveRoute() ? this.getActiveRoute().key : this.getActiveCategory() ? this.getActiveCategoryKey() : 'none']}
					defaultOpenKeys={this.state.collapsed ? [] : this.state.openKeys}
					openKeys={this.state.collapsed ? this.state.openKeysCollapsed : this.state.openKeysNonCollapsed}
					mode='inline'
					onOpenChange={(openKeys) => {
						if (this.state.collapsed) {
							this.setState({ openKeysCollapsed: openKeys });
						}
					}}
					inlineCollapsed={this.state.collapsed}
				>
					{this.props.menuItems.map((menuItem) => {
						if (menuItem.condition && !menuItem.condition()) {
							return null;
						}

						return menuItem.subItems && !isMenuItemHidden(menuItem.key) ? (
							<SubMenu
								className={menuItem.key === this.getActiveCategoryKey() ? 'ant-menu-submenu-act-as-open' : ''}
								key={menuItem.key}
								title={
									<span data-key={menuItem.key}>
										<i className='lg-nav-icon'>{menuItem.svg}</i>
										<span>{menuItem.text}</span>
									</span>
								}
								onTitleClick={() => this.props.history.push(menuItem.to)}
							>
								{menuItem.subItems.map((subItem) => {
									if (subItem.condition === false) {
										return null;
									}
									return (
										<Menu.Item key={subItem.key}>
											<Link to={subItem.to}>
												<span style={{ flex: 1 }}>{subItem.text}</span>
												{!!subItem.bagde && <Badge className='bagde-in-menuitem' count={subItem.bagde} />}
											</Link>
										</Menu.Item>
									);
								})}
							</SubMenu>
						) : (
							<Menu.Item key={menuItem.key}>
								{!isMenuItemHidden(menuItem.key) ? (
									<Link to={menuItem.to}>
										<i className='lg-nav-icon'>{menuItem.svg}</i>
										<span data-key={menuItem.key}>{menuItem.text}</span>
									</Link>
								) : null}
							</Menu.Item>
						);
					})}
				</Menu>
			</Sider>
		);
	}
}

Sidebar.contextTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}),
	location: PropTypes.object
};

export default withRouter(Sidebar);
