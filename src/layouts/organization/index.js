import { Query } from 'react-apollo';

import getAdminCampaignReviewQuery from 'graphql/get-admin-campaign-review.graphql';
import getOrganizationBySlug from 'graphql/get-organization-by-slug.graphql';
import { isSuperAdmin, isSaler } from 'services/auth-service';
import { getCurrentOrganization } from 'shared/utils';
import {
	FEATURE_FLAG_DATA_LIBRARY,
	FEATURE_FLAG_DATA_LIBRARY_DASHBOARD,
	FEATURE_FLAG_DATA_LIBRARY_REACH,
	FEATURE_FLAG_DATA_LIBRARY_IMPRESSIONS,
	FEATURE_FLAG_DATA_LIBRARY_TRAFFIC,
	FEATURE_FLAG_DATA_LIBRARY_AUDIENCE,
	FEATURE_FLAG_DATA_LIBRARY_BUDGET,
	FEATURE_FLAG_DATA_LIBRARY_ENGAGEMENT,
	FEATURE_FLAG_INTEGRATED_INBOX,
	FEATURE_FLAG_DISCOVERY_VIEW,
	FEATURE_FLAG_DISCOVERY_LISTS_VIEW
} from 'constants/feature-flag-keys';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';

import userImage from '../../assets/img/app/user-green.png';
import LoggedInLayout from '../logged-in';

const OrganizationLayout = (props: any) => {
	const organizationSlug = () => {
		const { organizationSlug } = props.match.params;
		return getCurrentOrganization(organizationSlug);
	};

	const sidebarMenuItems = () => {
		const { data } = props;
		const [isEnabled] = useFeatureToggle();

		return [
			{
				url: `/${organizationSlug()}/dashboard`,
				icon: '',
				title: 'Dashboard'
			},
			{
				url: isEnabled(FEATURE_FLAG_DISCOVERY_VIEW) ? `/${organizationSlug()}/discover/new-discovery` : `/${organizationSlug()}/discover`,
				icon: '',
				title: 'Influencers',
				children: [
					{
						url: isEnabled(FEATURE_FLAG_DISCOVERY_VIEW) ? `/${organizationSlug()}/discover/new-discovery` : `/${organizationSlug()}/discover`,
						title: 'Discover'
					},
					{
						url: `/${organizationSlug()}/discover/lists`,
						title: 'Lists',
						condition: () => isEnabled(FEATURE_FLAG_DISCOVERY_LISTS_VIEW)
					}
				]
			},
			{
				url: `/${organizationSlug()}/integrated-inbox`,
				icon: '',
				title: 'Integrated Inbox',
				condition: () => isEnabled(FEATURE_FLAG_INTEGRATED_INBOX)
			},
			{
				url: '/data-library',
				icon: '',
				title: 'Data Library',
				children: [
					{
						key: 'data-library-dashboard',
						url: '/data-library/dashboard',
						title: 'Dashboard',
						condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY_DASHBOARD)
					},
					{
						key: 'data-library-reach',
						url: '/data-library/reach',
						title: 'Reach',
						condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY_REACH)
					},
					{
						key: 'data-library-impressions',
						url: '/data-library/impressions',
						title: 'Impressions',
						condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY_IMPRESSIONS)
					},
					{
						key: 'data-library-traffic',
						url: '/data-library/traffic',
						title: 'Traffic',
						condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY_TRAFFIC)
					},
					{
						key: 'data-library-audience',
						url: '/data-library/audience',
						title: 'Audience',
						condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY_AUDIENCE)
					},
					{
						key: 'data-library-budget',
						url: '/data-library/budget',
						title: 'Budget',
						condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY_BUDGET)
					},
					{
						key: 'data-library-engagement',
						url: '/data-library/engagement',
						title: 'Engagement',
						condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY_ENGAGEMENT)
					}
				],
				condition: () => isEnabled(FEATURE_FLAG_DATA_LIBRARY)
			},
			{
				key: 'admin',
				url: `/admin/organizations`,
				icon: '',
				title: 'Admin',
				children: [
					{
						url: `/admin/organizations`,
						title: 'Organizations'
					},
					{
						url: `/admin/users`,
						title: 'Users'
					},
					{
						url: `/admin/agencies`,
						title: 'Agencies'
					},
					{
						url: `/admin/publishers/create`,
						title: 'Create publisher'
					}
				],
				condition: isSuperAdmin
			},
			{
				url: `/sale/organizations`,
				icon: '',
				title: 'Sale',
				children: [
					{
						key: 'sale-organizations',
						url: `/sale/organizations`,
						title: 'Organizations'
					}
				],
				condition: isSaler
			}
		];
	};
	const logo = props.loading && props.data && props.data.organization && props.data.organization.publisher && props.data.organization.publisher.logo;
	return (
		<LoggedInLayout sidebarMenuItems={sidebarMenuItems()} match={props.computedMatch} history={props.history} logo={logo} logoLoading={props.loading}>
			{props.children}
		</LoggedInLayout>
	);
};

const OrganizationLayoutWithOrganization = (props) => {
	const renderAdmin = (data, loading) => (
		<Query query={getAdminCampaignReviewQuery}>
			{({ data: datAdmin }) => (
				<OrganizationLayout
					{...{ ...props, data, loading }}
					count={(((datAdmin || {}).admin || {}).campaignReviews || []).filter(({ status }) => status === 'pending').length}
				/>
			)}
		</Query>
	);

	const organizationSlug = props.computedMatch.params.organizationSlug;

	if (organizationSlug) {
		return (
			<Query query={getOrganizationBySlug} variables={{ slug: organizationSlug }}>
				{({ data, loading }) => (isSuperAdmin() ? renderAdmin(data, loading) : <OrganizationLayout {...{ ...props, data, loading }} count={0} />)}
			</Query>
		);
	}

	return isSuperAdmin() ? renderAdmin(null, false) : <OrganizationLayout {...{ ...props, data: null, loading: false }} />;
};

export default OrganizationLayoutWithOrganization;
