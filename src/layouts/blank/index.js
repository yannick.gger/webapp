import { Component } from 'react';

class BlankLayout extends Component {
	render() {
		return this.props.children;
	}
}

export default BlankLayout;
