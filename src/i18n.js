import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';
import { initReactI18next } from 'react-i18next/hooks';

i18next
	.use(LanguageDetector)
	.use(reactI18nextModule)
	.use(initReactI18next)
	.init(
		{
			appendNamespaceToCIMode: true,
			updateMissing: true,
			ns: ['default', 'campaign'],
			defaultNS: 'default',
			backend: {
				projectId: 'ecd5e845-e12d-4aa6-b2f0-af2d70d7a2cb',
				referenceLng: 'en',
				version: 'latest',
				apiKey: '2ebdc6c0-e6f0-4d1a-90d9-5601c7660c1c'
			},
			resource: {
				loadPath: './i18n/{{lng}}/{{ns}}.json',
				savePath: './i18n/{{lng}}/{{ns}}.json'
			},
			fallbackLng: 'en',
			interpolation: {
				escapeValue: false, // React already does escaping
				formatSeparator: ',',
				format: function(value, format) {
					if (format === 'uppercase') return value.toUpperCase();
					return value;
				}
			},
			react: {
				wait: true
			}
		},
		() => {
			i18next.addResourceBundle('en', 'default', require('./i18n/en/default.json'));
			i18next.addResourceBundle('en', 'campaign', require('./i18n/en/campaign.json'));
			i18next.addResourceBundle('en', 'landingPage', require('./i18n/en/landingPage.json'));
			i18next.addResourceBundle('en', 'influencer', require('./i18n/en/influencer.json'));
			i18next.addResourceBundle('en', 'organization', require('./i18n/en/organization.json'));
		}
	);
export default i18next;
