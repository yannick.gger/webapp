// API platforms param option
export const INSTAGRAM_POST = 'instagram-post';
export const INSTAGRAM_STORY = 'instagram-story';

// Dashboard MetaData key
export const DASHBOARD_CAMPAIGNS = 'dashboard-campaigns';
export const DASHBOARD_PERIOD = 'dashboard-period';

// Date filter period
export const TODAY = 'today';
export const WEEK = 'week';
export const MONTH = 'month';

// Chart size options
export const LARGE = 'large';
export const MEDIUM = 'medium';
export const SMALL = 'small';

// Chart Items
export const REACH = {
	SUMMARY: 'reach-summary',
	OVERALL: 'reach-overall',
	GROSS: 'reach-gross',
	OVERALL_MEDIA_OBJECTS: 'reach-overall-media-objects',
	OVERTIME_ALL: 'reach-overtime-all',
	OVERTIME_POST: 'reach-overtime-post',
	OVERTIME_STORY: 'reach-overtime-story',
	INFLUENCER: 'reach-influencer',
	GENDER: 'reach-gender',
	COUNTRY: 'reach-country'
};

export const IMPRESSIONS = {
	SUMMARY: 'impressions-summary',
	OVERALL: 'impressions-overall',
	OVERALL_MEDIA_OBJECTS: 'impressions-overall-media-objects',
	OVERTIME_ALL: 'impressions-overtime-all',
	OVERTIME_POST: 'impressions-overtime-post',
	OVERTIME_STORY: 'impressions-overtime-story',
	INFLUENCER: 'impressions-influencer',
	GENDER: 'impressions-gender',
	COUNTRY: 'impressions-country'
};

export const ENGAGEMENT = {
	SUMMARY: 'engagement-summary',
	OVERALL_RATE: 'engagement-overall-rate',
	OVERALL_ACTUAL_RATE: 'engagement-overall-actual-rate',
	OVERALL_LIKES: 'engagement-overall-likes',
	OVERALL_COMMENTS: 'engagement-overall-comments',
	OVERALL_STORY_VIEW: 'engagement-overall-story-view',
	INFLUENCER: 'engagement-influencer'
};

export const TRAFFIC = {
	SUMMARY: 'traffic-summary',
	OVERALL_LINK: 'traffic-overall-link',
	OVERALL_BRAND: 'traffic-overall-brand',
	OVERTIME_LINK: 'traffic-overtime-link',
	OVERTIME_BRAND: 'traffic-overtime-brand',
	CTR: 'traffic-ctr',
	GENDER: 'impressions-gender',
	COUNTRY: 'impressions-country'
};

export const AUDIENCE = {
	AGE: 'audience-age',
	GENDER: 'audience-gender',
	COUNTRY: 'audience-country'
};

export const BUDGET = {
	SUMMARY: 'budget-summary',
	OVERALL: 'budget-overall',
	OVERALL_MARKETS: 'budget-overall-market',
	OVERALL_CPM: 'budget-overall-cpm',
	OVERALL_CPC: 'budget-overall-cpc',
	OVERTIME: 'budget-overtime'
};
