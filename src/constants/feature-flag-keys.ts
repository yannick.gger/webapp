export const FEATURE_FLAG_DATA_LIBRARY = 'DataLibrary';
export const FEATURE_FLAG_DATA_LIBRARY_DASHBOARD = 'DataLibraryDashboard';
export const FEATURE_FLAG_DATA_LIBRARY_REACH = 'DataLibraryReach';
export const FEATURE_FLAG_DATA_LIBRARY_IMPRESSIONS = 'DataLibraryImpressions';
export const FEATURE_FLAG_DATA_LIBRARY_TRAFFIC = 'DataLibraryTraffic';
export const FEATURE_FLAG_DATA_LIBRARY_AUDIENCE = 'DataLibraryAudience';
export const FEATURE_FLAG_DATA_LIBRARY_BUDGET = 'DataLibraryBudget';
export const FEATURE_FLAG_DATA_LIBRARY_ENGAGEMENT = 'DataLibraryEngagement';
export const FEATURE_FLAG_DATA_LIBRARY_CARD_PIN_BUTTON = 'DataLibraryCardPinButton';
export const FEATURE_FLAG_DATA_LIBRARY_CARD_MORE_BUTTON = 'DataLibraryCardMoreButton';
export const FEATURE_FLAG_DATA_LIBRARY_CARD_CAMPAIGN_BUTTON = 'DataLibraryCardCampaignButton';

export const FEATURE_FLAG_DISCOVERY_VIEW = 'DiscoveryView';
export const FEATURE_FLAG_DISCOVERY_LISTS_VIEW = 'DiscoveryListsView';

export const FEATURE_FLAG_INTEGRATED_INBOX = 'IntegratedInbox';

export const FEATURE_FLAG_FACEBOOK_AUTH = 'FacebookAuth';
export const FEATURE_COLLABS_AUTH = 'CollabsAPIAuth';
