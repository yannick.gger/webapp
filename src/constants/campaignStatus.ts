export enum CampaignStatus {
	Done = 'done',
	Draft = 'draft',
	Active = 'active',
	NotCreated = 'not created'
}
