import axios, { AxiosRequestConfig, Method } from 'axios';
import { trim } from 'lodash';

const fs = require('fs');
const crypto = require('crypto');

interface AxiosResponseMock {
	data: object;
}

function hashObject(input: object): string {
	return crypto.createHash('md5').update(JSON.stringify(input)).digest('hex').slice(0, 5).toUpperCase();
}

export class MockClient {
	private readonly mocksFolder: string;

	public interceptors: {
		response: { use: () => void };
		request: { use: () => void };
	};

	constructor() {
		this.mocksFolder = `${__dirname}/requests`;
		this.interceptors = {
			response: {
				use: jest.fn(),
			},
			request: {
				use: jest.fn(),
			},
		};
	}

	post(url: string, data?: object) {
		return this.fetchFile('POST', url, data);
	}

	get(url: string, config?: AxiosRequestConfig) {
		// eslint-disable-next-line prettier/prettier
		return this.fetchFile('GET', url, config?.params);
	}

	delete() {
		return Promise.resolve(undefined);
	}

	request(config: AxiosRequestConfig) {
		return this.fetchFile(config.method!, config.url!, config.data ?? config.params);
	}

	private fetchFile(method: Method, path: string, payload?: object): Promise<AxiosResponseMock> {
		const fileNameParts = [method.toUpperCase(), trim(path.replace(/\W/g, '_'), '_')];

		if (payload) {
			fileNameParts.push(hashObject(payload));
		}

		const filePath = `${this.mocksFolder}/${fileNameParts.join('_')}.json`;

		if (!fs.existsSync(filePath)) {
			if (!process.env.TEST_X_AUTH_TOKEN) {
				console.error('Please create "%s".', filePath);

				throw new Error(`Please create "${filePath}"`);
			}

			return new Promise(async (resolve, reject) => {
				console.info('Trying to create "%s".', filePath);
				const config : AxiosRequestConfig = {
					method,
					url: process.env.REACT_APP_COLLABS_API + path,
					headers: { 'X-Auth-Token': process.env.TEST_X_AUTH_TOKEN as string }
				};

				config['GET' === method.toUpperCase() ? 'params' : 'data'] = payload;

				const response = await axios.request(config);

				fs.writeFile(filePath, JSON.stringify(response.data, null, 4), (err?: Error) => {
					if (err) {
						return reject(err);
					}

					resolve(response);
				});
			});
		}

		return new Promise((resolve, reject) => {
			fs.readFile(filePath, (err: NodeJS.ErrnoException | null, data: Buffer) => {
				if (err) {
					return reject(err);
				}

				resolve({ data: JSON.parse(data.toString()) });
			});
		});
	}
}

module.exports = {
	create() {
		return new MockClient();
	},
	CancelToken: {
		source: () => {
			return {
				token: 'cancel_token',
				cancel: jest.fn(),
			}
		}
	}
};
