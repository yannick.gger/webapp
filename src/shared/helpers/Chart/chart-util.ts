import colors from 'styles/variables/colors';

export const getSavedCharts = () => {
	return JSON.parse(window.localStorage.getItem('dashboardCharts') || '[]');
};

export const saveChartInLocalStorage = (value: any) => {
	window.localStorage.setItem('dashboardCharts', JSON.stringify(value));
};

export const isInDashboard = (title: string) => {
	const savedCharts = getSavedCharts();
	if (savedCharts.length) {
		return savedCharts.some((item: any) => item.title === title);
	} else {
		return false;
	}
};

export const getKeyByValue = (object: any, value: any) => {
	return Object.keys(object).find((key) => object[key] === value);
};

export const getTotalCount = (data: Array<number>) => {
	let totalCount = 0;
	if (data.length > 0) {
		totalCount = data.reduce((prev, current) => prev + current, 0);
	}
	return totalCount;
};

export const formatNumberWithDecimals = (value: number, suffix?: string) => {
	let result;
	const targetValue = value * 100;
	if (targetValue >= 100) {
		result = 100;
	} else if (targetValue >= 10) {
		result = targetValue.toFixed(1);
	} else if (targetValue === 0) {
		result = 0;
	} else {
		result = targetValue.toFixed(2);
	}
	return suffix ? `${result}${suffix}` : result;
};

export const formatDate = (date: Date) => {
	let month = '' + (date.getMonth() + 1);
	let day = '' + date.getDate();
	const year = date.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

export const formatDateAndMonth = (date: string) => {
	const newDate = new Date(date);
	const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	let month = months[newDate.getMonth()];
	let day = '' + newDate.getDate();

	return [day, month].join(' ');
};

export const getMonthBeforeDate = (date: Date) => {
	const monthBefore = date;
	monthBefore.setMonth(monthBefore.getMonth() - 1);
	monthBefore.setDate(monthBefore.getDate() - 1);
	return monthBefore;
};

export const transformToLineChartData = (props: { arr: Array<any>; color?: any }) => {
	let labels: string[] = [];
	let value = [];
	if (props.arr.length) {
		const attributesArr = props.arr.map((node) => node.attributes);
		labels = attributesArr.map((node) => {
			if (!node.year && node.date) {
				return formatDateAndMonth(node.date);
			}
			return formatDate(new Date(node.year, node.month, node.date));
		});
		value = attributesArr.map((node) => {
			if (node.count !== undefined) {
				return node.count;
			}
			if (node.amount !== undefined) {
				return node.amount;
			}
		});
	}

	return { labels: labels, datasets: [{ data: value, borderColor: props.color }] };
};

export const transfromEngagementBarChart = (data: any, platforms: string) => {
	const labels: Array<{ username: string; profileImage: string }> = [];
	let datasets: Array<{ label: string; data: Array<number>; borderColor?: string; backgroundColor?: string }> = [];
	const images: Array<string> = [];

	const sortedData: any = {
		reach: [],
		impressions: [],
		likes: [],
		comments: []
	};

	if (platforms === 'instagram-story') {
		if (Array.isArray(data)) {
			data.forEach((node: any) => {
				sortedData.reach.push(node.attributes.reach);
				sortedData.impressions.push(node.attributes.impressions);
				labels.push(node.attributes.username);
				images.push(node.links.profileImage);
			});
		}
		datasets = ['reach', 'impressions'].map((label, index) => {
			return { label: label, data: sortedData[label], backgroundColor: colors[`brandColor${index + 1}`] };
		});
	}

	if (platforms === 'instagram-post') {
		if (Array.isArray(data)) {
			data.forEach((node: any) => {
				sortedData.reach.push(node.attributes.reach);
				sortedData.impressions.push(node.attributes.impressions);
				sortedData.likes.push(node.attributes.likes);
				sortedData.comments.push(node.attributes.comments);
				labels.push(node.attributes.username);
				images.push(node.links.profileImage);
			});
		}
		datasets = ['reach', 'impressions', 'likes', 'comments'].map((label, index) => {
			return { label: label, data: sortedData[label], backgroundColor: colors[`brandColor${index + 1}`] };
		});
	}

	return {
		labels: labels,
		datasets: datasets,
		images: images
	};
};

export const transformPieData = (props: any) => {
	const labels: Array<string> = [];
	const data: Array<number> = [];
	const backgroundColors: Array<string> = [];

	if (Array.isArray(props)) {
		props.forEach((el) => {
			if (labels.indexOf(el.attributes.country) === -1) {
				labels.push(el.attributes.country);
			}
		});
		labels.forEach((country) => {
			const amountsByCountry = props.filter((node) => node.attributes.country === country);
			data.push(amountsByCountry.reduce((prev, current) => prev + current.attributes.amount, 0));
		});
	}

	data.forEach((_, index) => {
		const brandColors = [];
		for (const color in colors) {
			if (color.includes('brandColor')) {
				brandColors.push(colors[color]);
			}
		}
		backgroundColors.push(brandColors[index % brandColors.length]);
	});

	return { labels: labels, datasets: [{ data: data, backgroundColor: backgroundColors }] };
};

export const formatNumber = (value: number | string): string => {
	let result: string = '0';
	if (typeof value === 'string') {
		result = value;
	} else if (typeof value === 'number') {
		if (value < 1000) {
			if (value > 100) {
				result = Math.floor(value).toLocaleString();
			} else if (value >= 10) {
				result = value.toFixed(1);
			} else if (value === 0) {
				result = '0';
			} else {
				result = value.toFixed(2);
			}
		} else if (value >= 1000 && value < 1000000) {
			result = value / 1000 > 10 ? `${(value / 1000).toFixed(1)}K` : `${(value / 1000).toFixed(2)}K`;
		} else if (value >= 1000000) {
			result = value / 1000000 > 10 ? `${(value / 1000000).toFixed(1)}M` : `${(value / 1000000).toFixed(2)}M`;
		}
	} else {
		result = 'N/A';
	}

	return result;
};
