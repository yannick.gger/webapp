import { formatNumber } from './chart-util';

describe('chart-util - formatNumber test', () => {
	it('should return 100%', () => {
		expect(formatNumber('100%')).toBe('100%');
	});

	it('should return number as string', () => {
		expect(formatNumber(123)).toBe('123');
	});

	it('should return xK', () => {
		expect(formatNumber(2000)).toBe('2.00K');
	});

	it('should return xM', () => {
		expect(formatNumber(2000000)).toBe('2.00M');
	});

	it('should return N/A', () => {
		expect(formatNumber([123123])).toBe('N/A');
	});
});
