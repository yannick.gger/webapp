import moment from 'moment';

/**
 * mapWeekDay
 * @todo i18next
 * @param {string} date
 * @returns Date
 */
export const mapWeekDay = (date: string): string => {
	const result = moment()
		.endOf('day')
		.diff(date, 'days');

	if (result > 1 && result <= 7) {
		return moment()
			.subtract(result, 'd')
			.format('dddd');
	}

	switch (result) {
		case 0:
			return 'Today';
		case 1:
			return 'Yesterday';
		default:
			return moment(date).format('YYYY-MM-DD');
	}
};
