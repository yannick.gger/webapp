import BrowserStorage, { StorageType } from './BrowserStorage';

export const mockWindowProperty = (property: any, value: any) => {
	const { [property]: originalProperty } = window;
	delete window[property];
	beforeAll(() => {
		Object.defineProperty(window, property, {
			configurable: true,
			writable: true,
			value
		});
	});
	afterAll(() => {
		window[property] = originalProperty;
	});
};

describe('BrowserStorage as LocalStorage', () => {
	const localStorage = new BrowserStorage(StorageType.LOCAL);
	const key = 'TEST';
	const val = 'testValue';

	mockWindowProperty('localStorage', {
		setItem: jest.fn(),
		getItem: jest.fn()
	});

	it('should be created', () => {
		expect(localStorage).toBeTruthy();
	});

	it('should set and get item', () => {
		expect.assertions(1);
		localStorage.setItem(key, val);

		return expect(localStorage.getItem(key)).toEqual(val);
	});

	it('should remove item', () => {
		expect.assertions(1);
		localStorage.removeItem(key);
		return expect(localStorage.getItem(key)).toEqual(null);
	});

	it('should clear local storage', () => {
		localStorage.setItem('key1', 'value1');
		localStorage.setItem('key2', 'value2');
		localStorage.clear();
		expect.assertions(2);
		expect(localStorage.getItem('key1')).toEqual(null);
		expect(localStorage.getItem('key2')).toEqual(null);
	});

	it('should set multiple values', () => {
		const data = [['key1', 'value1'], ['key2', 'value2'], ['key3', 'value3']];

		expect.assertions(1);
		return expect(localStorage.multiSet(data)).toEqual(data);
	});

	it('should get multiple values', () => {
		const data = [['key1', 'value1'], ['key2', 'value2'], ['key3', 'value3']];

		expect.assertions(1);
		return expect(localStorage.multiGet(['key1', 'key2', 'key3'])).toEqual(data);
	});
});

describe('BrowserStorage as SessionStorage', () => {
	const sessionStorage = new BrowserStorage(StorageType.SESSION);
	const key = 'TEST';
	const val = 'testValue';

	mockWindowProperty('sessionStorage', {
		setItem: jest.fn(),
		getItem: jest.fn()
	});

	it('should be created', () => {
		expect(sessionStorage).toBeTruthy();
	});

	it('should set and get item', () => {
		expect.assertions(1);
		sessionStorage.setItem(key, val);
		return expect(sessionStorage.getItem(key)).toEqual(val);
	});

	it('should remove item', () => {
		expect.assertions(1);
		sessionStorage.removeItem(key);
		return expect(sessionStorage.getItem(key)).toEqual(null);
	});

	it('should clear session storage', () => {
		sessionStorage.setItem('key1', 'value1');
		sessionStorage.setItem('key2', 'value2');
		sessionStorage.clear();
		expect.assertions(2);
		expect(sessionStorage.getItem('key1')).toEqual(null);
		expect(sessionStorage.getItem('key2')).toEqual(null);
	});

	it('should set multiple values', () => {
		const data = [['key1', 'value1'], ['key2', 'value2'], ['key3', 'value3']];

		expect.assertions(1);
		return expect(sessionStorage.multiSet(data)).toEqual(data);
	});

	it('should get multiple values', () => {
		const data = [['key1', 'value1'], ['key2', 'value2'], ['key3', 'value3']];

		expect.assertions(1);
		return expect(sessionStorage.multiGet(['key1', 'key2', 'key3'])).toEqual(data);
	});
});
