/* eslint-disable prettier/prettier */
import axios, { Method, AxiosPromise, AxiosRequestConfig, AxiosInstance } from 'axios';
import { KEY_TOKENS, KEY_COLLABS_API_TOKEN, GHOST_USER_EMAIL } from 'constants/localStorage-keys';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import CollabsAuthService from 'services/Authentication/Collabs-api';

const DEBUG = process.env.NODE_ENV === 'development';

export function createClient(): AxiosInstance {
	const apiClient = axios.create({
		baseURL: process.env.REACT_APP_COLLABS_API
	});

	apiClient.interceptors.request.use(
		async (config: AxiosRequestConfig) => {
			const storage = new BrowserStorage(StorageType.LOCAL);
			const tokens = storage.getItem(KEY_TOKENS) || '';
			const ghostUserEmail = storage.getItem(GHOST_USER_EMAIL) || '';

			if (ghostUserEmail !== '') {
				config.headers!['X-Switch-User'] = `${ghostUserEmail}`;
			}

			// Check if we have a token and add it to the header during the calls
			if (tokens !== '') {
				const parsedToken = JSON.parse(tokens);
				if (parsedToken[KEY_COLLABS_API_TOKEN]) {
					config.headers!['X-Auth-Token'] = `${parsedToken[KEY_COLLABS_API_TOKEN]}`;
				}
			}

			return config;
		},
		(error: any) => {
			if (DEBUG) {
				console.error('✉️ ', error);
			}
			Promise.reject(error);
		}
	);

	apiClient.interceptors.response.use(
		(response) => response,
		(error) => {
			if (error.response.status === 401) {
				CollabsAuthService.panicLogout();
			} else {
				return Promise.reject(error);
			}
		}
	);

	return apiClient;
}

const apiClient = createClient();

export const Client = (requestMethod: Method, url: string, data: Record<string, any> = {}, signal: any = null, headers: any = {}): AxiosPromise => {
	return apiClient.request({ method: requestMethod, url: url, data: data, signal: signal, headers: headers });
};
