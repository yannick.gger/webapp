/* eslint-disable prettier/prettier */
import axios, { Method, AxiosPromise } from 'axios';
import { KEY_TOKENS, KEY_DISCOVERY_API_TOKEN } from 'constants/localStorage-keys';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';

const discoveryApiClient = axios.create({
	withCredentials: true // because we are using Authorization in headers
});
const DISCOVERY_URL = process.env.REACT_APP_DISCOVERY_API;
const DEBUG = process.env.NODE_ENV === 'development';

discoveryApiClient.interceptors.request.use(
	async (config: any) => {
		const storage = new BrowserStorage(StorageType.LOCAL);
		const tokens = storage.getItem(KEY_TOKENS) || '';

		// Check if we have a token and add it to the header during the calls
		if (tokens !== '') {
			const parsedToken = JSON.parse(tokens);
			if (parsedToken[KEY_DISCOVERY_API_TOKEN]) {
				const discoveryToken = parsedToken[KEY_DISCOVERY_API_TOKEN].token;
				config.headers['Authorization'] = `${discoveryToken}`;
			}
		}

		if (DEBUG) {
			console.info('✉️ ', config);
		}

		return config;
	},
	(error: any) => {
		if (DEBUG) {
			console.error('✉️ ', error);
		}
		Promise.reject(error);
	}
);

export const DiscoveryApiClient = (requestMethod: Method, url: string, data: Record<string, any> = {}, signal: any = null): AxiosPromise => {
	return discoveryApiClient({
		method: requestMethod,
		url: `${DISCOVERY_URL}${url}`,
		data: data,
		signal: signal
	});
};
