export type permissionResponse = {
	success: boolean;
};

export type loginResponse = {
	authResponse: authResponse;
	status: status;
};

type authResponse = {
	accessToken: string;
	data_access_expiration_time: Date;
	expiresIn: Date;
	graphDomain: string;
	signedRequest: string;
	userID: string;
};

type status = {
	status: string;
};
