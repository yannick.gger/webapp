/**
 * Function to Initialize the Facebook SDK script
 */

import { loginResponse, permissionResponse } from './types';

export function initFacebookSdk() {
	const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;
	window.fbAsyncInit = function() {
		window.FB.init({
			appId: facebookAppId,
			cookie: true,
			xfbml: true,
			version: 'v13.0'
		});
	};

	// load facebook sdk script
	(function(d, s, id) {
		var js,
			fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = 'https://connect.facebook.net/en_US/sdk.js';
		if (fjs.parentNode) {
			fjs.parentNode.insertBefore(js, fjs);
		}
	})(document, 'script', 'facebook-jssdk');
}

export const logout = () => {
	return (
		window.FB &&
		window.FB.api('/me/permissions', 'delete', (response: permissionResponse) => {
			if (response.success) {
				window.FB.logout();
			}
		})
	);
};

export const loginStatus = async () => {
	let status = '';
	if (window.FB) {
		window.FB.getLoginStatus((response: any) => {
			status = response.status;
		});
	}

	return status;
};

export const login = async (): Promise<loginResponse> => {
	const SCOPES = process.env.REACT_APP_FACEBOOK_API_SCOPES;
	return await new Promise((resolve) => window.FB.login(resolve, { scope: SCOPES }));
};
