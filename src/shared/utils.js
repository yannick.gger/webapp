import { hasGhostUser } from 'services/auth-service';
import { ORGANIZATION_SLUG, GHOST_ORGANIZATION_SLUG, KEY_HIDDEN_MENU_ITEMS } from 'constants/localStorage-keys';

export const getCurrentOrganization = (organizationSlug) => {
	// Get slug from localstorage, user slug or ghost user slug
	const key = !hasGhostUser() ? ORGANIZATION_SLUG : GHOST_ORGANIZATION_SLUG;
	let organization = JSON.parse(window.localStorage.getItem(key));

	return organizationSlug || organization;
};

export const setOrganization = (organizationSlug) => {
	if (organizationSlug) {
		const setJSON = JSON.stringify(organizationSlug);
		const key = !hasGhostUser() ? ORGANIZATION_SLUG : GHOST_ORGANIZATION_SLUG;
		localStorage.setItem(key, setJSON);
	}

	return null;
};

// Temporary function to hide menu items
export const isMenuItemHidden = (itemKey) => {
	const ls = window.localStorage.getItem(KEY_HIDDEN_MENU_ITEMS);
	if (!isValidJson(ls)) return false;

	const menuItems = JSON.parse(ls);
	if (!menuItems || menuItems === '') return false;

	if (Array.isArray(menuItems)) {
		return menuItems.includes(itemKey);
	}

	return menuItems === itemKey;
};

export const isValidJson = (jsonString) => {
	try {
		return JSON.parse(jsonString);
	} catch (e) {}
};
