import CollabsAuthService from 'services/Authentication/Collabs-api';

// @todo this should be a part of userContext
export const getUserFriendlyRoleName = () => {
	const loggedInUser = CollabsAuthService.getCollabsUserObject();
	const permissions = loggedInUser.permissions;

	if (permissions.entities && !permissions.isSuperAdmin) return 'Project manager';

	if (permissions.isSuperAdmin) return 'Administrator';

	if (permissions.isAgent) return 'Agent';

	if (permissions.isInfluencer) return 'Influencer';

	return '';
};
