import { MemoryRouter, Route, useLocation } from 'react-router-dom';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';

import CreationForm from './CreationForm';

const LocationDisplay = () => {
	const location = useLocation();

	return <div data-testid='location-display'>{location.pathname}</div>;
};

test('displays the correct step', async () => {
	const { asFragment } = render(
		<MemoryRouter initialEntries={['/admin/publishers/create']}>
			<ThemeProvider theme={themes.default}>
				<Route path='/admin/publishers/create'>
					<CreationForm />
				</Route>
				<Route path='*'>
					<LocationDisplay />
				</Route>
			</ThemeProvider>
		</MemoryRouter>
	);

	expect(asFragment()).toMatchSnapshot();
	userEvent.type(await screen.findByLabelText('Name'), 'Bond, James Bond');
	userEvent.type(await screen.findByLabelText('Email'), '007@mi6.uk');
	userEvent.click(await screen.findByText('Save'));

	await waitFor(async () => {
		const location = await screen.findByTestId('location-display');
		expect(location.innerHTML).toBe('/admin/users');
	});
});
