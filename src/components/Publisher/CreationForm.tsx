import React, { useContext } from 'react';
import FormGroup from 'views/Campaign/Dashboard/Modal/FormGroup';
import { Formik } from 'formik';
import { Button } from '../Button';
import * as Yup from 'yup';
import { createClient } from 'shared/ApiClient/ApiClient';
import Styled from './CreationForm.style';
import { useHistory } from 'react-router-dom';
import handleErrors from 'utils/formik_error_handler';
import { ToastContext } from 'contexts';

const VALIDATION_SCHEMA = Yup.object({
	name: Yup.string().required(),
	email: Yup.string()
		.email()
		.required()
});

/**
 * @todo i18next
 */
const CreationForm = () => {
	const history = useHistory();
	const { addToast } = useContext(ToastContext);
	return (
		<Formik
			initialValues={{ name: '', email: '' }}
			enableReinitialize
			validationSchema={VALIDATION_SCHEMA}
			onSubmit={async (values, { setErrors }) => {
				try {
					await createClient().post('/publishers', values);
					history.push('/admin/users');
					addToast({ id: 'publisher-created', mode: 'success', message: 'Publisher created!' });
				} catch (e) {
					handleErrors(e, setErrors);
				}
			}}
		>
			{({ isSubmitting, isValid }) => (
				<Styled.Form noValidate>
					<FormGroup label='Name' name='name' required placeholder='Company Name' />
					<FormGroup label='Email' name='email' required placeholder='john.doe@gmail.com' />
					<Button type='submit' disabled={isSubmitting || !isValid}>
						Save
					</Button>
				</Styled.Form>
			)}
		</Formik>
	);
};

export default CreationForm;
