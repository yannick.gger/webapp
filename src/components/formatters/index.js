import React from 'react';
import { FormattedNumber } from 'react-intl';
import { Tooltip } from 'antd';

const FormattedCurrencyNumber = ({ value, currency }) => <FormattedNumber value={value} style='currency' currency={currency} />;

const ShortAmountFormatter = ({ value, maximumFractionDigits }) => {
	const Present = ({ amount, exactAmount, maximumFractionDigits, character }) => (
		<Tooltip title={<FormattedNumber value={exactAmount} />}>
			<FormattedNumber value={amount} maximumFractionDigits={maximumFractionDigits} />
			{character}
		</Tooltip>
	);

	if (value > 900000) {
		return <Present exactAmount={value} amount={value / 1000000} maximumFractionDigits={maximumFractionDigits || 2} character='m' />;
	} else if (value > 900) {
		return <Present exactAmount={value} amount={value / 1000} maximumFractionDigits={maximumFractionDigits || 0} character='k' />;
	} else {
		return <Present exactAmount={value} amount={value} maximumFractionDigits={maximumFractionDigits || 0} character='' />;
	}
};

const shortAmountFormatter = ({ value }) => {
	if (value > 900000) {
		return `${parseInt(value / 1000000, 10)}m`;
	} else if (value > 900) {
		return `${parseInt(value / 1000, 10)}k`;
	} else {
		return `${parseInt(value, 10)}`;
	}
};

export { ShortAmountFormatter, shortAmountFormatter, FormattedCurrencyNumber };
