import React from 'react';

const ghostUserAccess = ({ ...props }) => {
	if (!props.ghostUserIsSaler) {
		return <React.Fragment>{props.children}</React.Fragment>;
	}

	return null;
};

export default ghostUserAccess;
