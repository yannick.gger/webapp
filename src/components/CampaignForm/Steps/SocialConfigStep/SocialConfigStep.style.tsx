import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import Icon from 'components/Icon';
import { InputField } from 'styles/formElements/input';

const Wrapper = styled.div``;

const NumberInputWrapper = styled.div`
	& > label {
		font-size: 1.2rem;
		font-weight: 700;
		margin-bottom: ${guttersWithRem.l};
	}

	& > input {
		${InputField};
		width: fit-content;
	}

	margin-bottom: ${guttersWithRem.l};
`;

const ButtonsWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	margin: ${guttersWithRem.m} 0;
`;

const CustomIcon = styled(Icon)`
	line-height: 0;
	&.icon {
		margin-left: 0;
	}

	&.reflect {
		transform: rotate(180deg);
	}
`;

const Styled = {
	Wrapper,
	ButtonsWrapper,
	CustomIcon,
	NumberInputWrapper
};

export default Styled;
