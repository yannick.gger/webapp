import axios, { AxiosRequestConfig } from 'axios';
import { useState, useContext } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next/hooks';

import { useAppDispatch, useAppSelector } from '../../hooks';
import { setInfluencerNumber, setHashtags, setMentions, removeHashtag, removeMention, resetSocialConfig } from '../../reducers/campaignSocialConfigSlice';
import { resetCampainInfo } from 'components/CampaignForm/reducers/campaignInfoSlice';
import { resetCamapaignDetails } from 'components/CampaignForm/reducers/campaignDetailsSlice';
import { Button } from 'components/Button';
import { previousStep, setValidationErrors, createErrorSelector, selectCampaignId, nextStep } from '../../reducers/uiSlice';
import { RootState } from '../../store';
import { CampaignDraft, CONFIG_STEP } from '../../index.d';
import { createClient } from 'shared/ApiClient/ApiClient';
import { ToastContext } from 'contexts';
import { ErrorResponse } from 'types/http';
import TagField from 'components/TagField/TagField';
import Styled from './SocialConfigStep.style';

enum CONFIG {
	INFLUENCERS_MIN = 1,
	INFLUENCERS_MAX = 100,
	HASHTAG_MAX = 50,
	MENTION_MAX = 50
}

function stateToDraft({ info, details, socialConfig }: RootState): CampaignDraft {
	const { name, brand, users } = info;
	const { background, purpose, target, guidelines } = details;
	const { influencerNumber, hashtags, mentions } = socialConfig;

	return {
		name,
		users: users.map((user) => user.id),
		background,
		purpose,
		target,
		guidelines,
		hashtags,
		mentions,
		brand: brand ? brand.id : '',
		influencerTargetCount: influencerNumber,
		language: 'en',
		currency: 'SEK'
	};
}

type SuccessfulResponse = {
	data: {
		id: string;
	};
};

type RouteParams = {
	organizationSlug: string;
};

const SocialConfigStep = () => {
	const dispatch = useAppDispatch();
	const rootState = useAppSelector((state) => state);
	const errors = useAppSelector(createErrorSelector(CONFIG_STEP.SOCIAL_CONFIG));
	const [t] = useTranslation('campaign');
	const history = useHistory();
	const [isSubmitting, setSubmitting] = useState(false);
	const { addToast } = useContext(ToastContext);
	const { organizationSlug } = useParams<RouteParams>();
	const { influencerNumber, hashtags, mentions } = rootState.socialConfig;
	const isReadyToSubmit = influencerNumber > 0;
	const campaignId = useAppSelector(selectCampaignId);

	return (
		<Styled.Wrapper>
			<form
				onSubmit={async (e) => {
					e.preventDefault();

					const client = createClient();
					const draft = stateToDraft(rootState);
					const requestConfig: AxiosRequestConfig = {
						url: '/campaigns',
						data: draft,
						method: 'POST'
					};

					if (campaignId) {
						requestConfig.url += `/${campaignId}`;
						requestConfig.method = 'PATCH';
					}

					setSubmitting(true);

					try {
						const { data } = await client.request<SuccessfulResponse>(requestConfig);

						if (campaignId) {
							dispatch(nextStep());
						} else {
							dispatch(resetSocialConfig());
							dispatch(resetCampainInfo());
							dispatch(resetCamapaignDetails());
							history.push(`/${organizationSlug}/campaigns/${data.data.id}`);
						}
					} catch (e) {
						console.error(e);
						if (!axios.isAxiosError(e)) {
							console.error('Unexpected error: %O', e);
							addToast({ id: 'ise', mode: 'error', message: 'Something went wrong!' });
							return;
						}

						addToast({ id: 'validation-error', mode: 'error', message: t('validation.invalid_request') });

						dispatch(setValidationErrors(e.response!.data as ErrorResponse));

						setSubmitting(false);
					}
				}}
			>
				<Styled.NumberInputWrapper size='lg'>
					<label htmlFor='influencerNumber' className='form-label'>
						{t('form_label.influencerNumber')}
					</label>
					<input
						type='number'
						step={1}
						className='form-control'
						id='influencerNumber'
						placeholder={`Select between ${CONFIG.INFLUENCERS_MIN} and ${CONFIG.INFLUENCERS_MAX}`}
						max={CONFIG.INFLUENCERS_MAX}
						min={CONFIG.INFLUENCERS_MIN}
						onChange={({ target }) => dispatch(setInfluencerNumber(+target.value))}
						value={influencerNumber}
					/>
					{errors.influencerTargetCount && <p>{errors.influencerTargetCount}</p>}
				</Styled.NumberInputWrapper>
				<div className='mb-3'>
					<TagField
						name={'hashtags'}
						heading={t('form_label.hashtags')}
						tags={hashtags}
						onChange={(tag) => {
							dispatch(setHashtags(tag));
						}}
						onRemove={(tag) => {
							dispatch(removeHashtag(tag));
						}}
					/>
					{errors.hashtags && <p>{errors.hashtags}</p>}
				</div>
				<div className='mb-3'>
					<TagField
						name={'mentions'}
						heading={t('form_label.mentions')}
						tags={mentions}
						onChange={(tag) => {
							dispatch(setMentions(tag));
						}}
						onRemove={(tag) => {
							dispatch(removeMention(tag));
						}}
					/>
					{errors.mentions && <p>{errors.mentions}</p>}
				</div>
				<Styled.ButtonsWrapper>
					<Button type='button' size='sm' onClick={() => dispatch(previousStep())}>
						<Styled.CustomIcon icon='arrow-right' size='24' className='reflect' />
						{t('action.back')}
					</Button>

					<Button size='sm' type='submit' disabled={isSubmitting || !isReadyToSubmit} data-testid='create-campaign-button'>
						{isSubmitting ? (
							'...'
						) : (
							<>
								{t(`action.${campaignId ? 'update' : 'create'}-campaign`)}
								<Styled.CustomIcon icon='arrow-right' size='24' />
							</>
						)}
					</Button>
				</Styled.ButtonsWrapper>
			</form>
		</Styled.Wrapper>
	);
};

export default SocialConfigStep;
