import React, { useState, useEffect } from 'react';
import { Form, Formik } from 'formik';
import { useTranslation } from 'react-i18next/hooks';
import * as Yup from 'yup';

import { useAppDispatch, useAppSelector } from '../../hooks';
import { Button } from 'components/Button';
import { setBackground, setPurpose, setTarget, setGuidelines, updateGuidelines } from '../../reducers/campaignDetailsSlice';
import { createErrorSelector, previousStep, nextStep, selectCampaignId } from '../../reducers/uiSlice';
import Textarea from 'components/Form/Elements/Textarea';
import { CONFIG_STEP } from '../../index.d';
import Styled from './DetailsStep.style';
import TogglePanel from 'components/TogglePanel';
import GuidelineItem from 'components/CampaignForm/Components/GuidelineItem';
import NextStepButton from '../NextStepButton';
import { createClient } from '../../../../shared/ApiClient/ApiClient';

const DetailsStep = () => {
	const [isGuidelineOpen, setIsGuidelineOpen] = useState<boolean>(false);
	const [enteredGuideline, setEnteredGuideline] = useState('');
	const dispatch = useAppDispatch();
	const initialValues = useAppSelector(({ details }) => details);
	const { guidelines } = useAppSelector(({ details }) => details);
	const initialErrors = useAppSelector(createErrorSelector(CONFIG_STEP.DETAILS));
	const campaignId = useAppSelector(selectCampaignId);
	const [t] = useTranslation('campaign');

	const validationSchema = Yup.object().shape({
		background: Yup.string().required(t('validation.required')),
		purpose: Yup.string().required(t('validation.required')),
		target: Yup.string().required(t('validation.required')),
		guidelines: Yup.array()
			.min(1)
			.of(Yup.string())
			.required(t('validation.required'))
	});

	const enterAddGuideline = (e: React.KeyboardEvent<HTMLInputElement>) => {
		if (enteredGuideline && e.code === 'Enter') {
			e.preventDefault();
			dispatch(setGuidelines(enteredGuideline));
			setEnteredGuideline('');
		}
	};

	const updateExistGuideline = (newText: string, targetIndex: number) => {
		const newGuidelines = [...guidelines];
		newGuidelines.splice(targetIndex, 1, newText);

		dispatch(updateGuidelines(newGuidelines));
	};

	const removeOneGuideline = (targetIndex: number) => {
		const newGuidelines = [...guidelines].filter((guideline, index) => index !== targetIndex);
		dispatch(updateGuidelines(newGuidelines));
	};

	useEffect(() => {
		if (guidelines.length) {
			setIsGuidelineOpen(true);
		}
	}, [guidelines]);

	return (
		<Formik
			enableReinitialize={true}
			initialValues={initialValues}
			initialErrors={initialErrors}
			validationSchema={validationSchema}
			onSubmit={async (values) => {
				dispatch(nextStep());
				if (campaignId) {
					await createClient().patch(`/campaigns/${campaignId}`, values);
				}
			}}
		>
			{({ errors }) => (
				<Form>
					<fieldset>
						<legend>What's the campaign about?</legend>
						<Textarea
							heading={t('form_label.background')}
							id='background'
							name='background'
							placeholder={t('form_placeholder.background')}
							required
							onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
								dispatch(setBackground(e.target.value));
							}}
						/>
						<Textarea
							heading={t('form_label.purpose')}
							id='purpose'
							name='purpose'
							required
							onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
								dispatch(setPurpose(e.target.value));
							}}
						/>
						<Textarea
							heading={t('form_label.target')}
							id='target'
							name='target'
							required
							onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
								dispatch(setTarget(e.target.value));
							}}
						/>
						<TogglePanel heading={t('form_label.guidelines')} isOpen={isGuidelineOpen} onClick={() => setIsGuidelineOpen((prev) => !prev)}>
							{guidelines.length > 0 ? (
								<Styled.GuidelineWrapper>
									{guidelines.map((guideline, index) => (
										<li key={`${guideline}-${index}`}>
											<GuidelineItem
												value={guideline}
												changeSaveHandler={(newText) => {
													updateExistGuideline(newText, index);
												}}
												removeItem={() => {
													removeOneGuideline(index);
												}}
											/>
										</li>
									))}
								</Styled.GuidelineWrapper>
							) : null}
							<Styled.InputWrapper size='md'>
								<span>&#8226;</span>
								<input
									data-testid={`form-control-guidelines`}
									value={enteredGuideline}
									onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
										setEnteredGuideline(e.target.value);
									}}
									onKeyPress={enterAddGuideline}
								/>
								<code title='Press "enter" to submit'>[enter]</code>
							</Styled.InputWrapper>
							<Styled.ErrorMessage>{errors.guidelines}</Styled.ErrorMessage>
						</TogglePanel>
					</fieldset>

					<Styled.ButtonsWrapper>
						<Button type='button' size='sm' onClick={() => dispatch(previousStep())}>
							<Styled.CustomIcon icon='arrow-right' size='24' className='reflect' />
							Back
						</Button>

						<NextStepButton />
					</Styled.ButtonsWrapper>
				</Form>
			)}
		</Formik>
	);
};

export default DetailsStep;
