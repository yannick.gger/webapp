import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import Icon from 'components/Icon';

import { InputField } from 'styles/formElements/input';

const InputWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;

	& > input {
		${InputField};
		border: none;
		background-color: ${colors.transparent};

		&:hover:not(:disabled),
		&:focus:not(:disabled) {
			border: none;
			box-shadow: none;
		}
	}
`;

const GuidelineWrapper = styled.ul`
	padding: ${guttersWithRem.xxs};
`;

const ButtonsWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	margin: ${guttersWithRem.m} 0;
`;

const CustomIcon = styled(Icon)`
	line-height: 0;
	&.icon {
		margin-left: 0;
	}

	&.reflect {
		transform: rotate(180deg);
	}
`;

const ErrorMessage = styled.p`
	font-size: 0.75rem;
	color: ${colors.error};
`;

const Styled = {
	InputWrapper,
	GuidelineWrapper,
	ButtonsWrapper,
	CustomIcon,
	ErrorMessage
};

export default Styled;
