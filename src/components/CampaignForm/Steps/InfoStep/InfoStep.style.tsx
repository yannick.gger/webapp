import styled from 'styled-components';
import typography from 'styles/variables/typography';
import fontSize from 'styles/variables/font-size';
import { guttersWithRem } from 'styles/variables/gutter';
import Icon from 'components/Icon';
import colors from 'styles/variables/colors';

const FormFieldset = styled.fieldset`
	margin-bottom: ${guttersWithRem.l};
`;

const SelectTeamWrapper = styled.div``;
const SectionTitle = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-size: ${fontSize.l};
	margin-bottom: ${guttersWithRem.l};
`;

const FormFooter = styled.div`
	width: 100%;
	display: flex;
	justify-content: flex-end;
`;

const CustomIcon = styled(Icon)`
	line-height: 0;
	&.icon {
		margin-left: 0;
	}
`;

const InputWrapper = styled.div`
	margin-bottom: ${guttersWithRem.xl};
`;

const Label = styled.label`
	font-family: ${typography.label.fontFamily};
	font-size: ${typography.label.small.fontSize};
	color: ${colors.labelColor};
	margin-bottom: ${guttersWithRem.xxs};
`;

const Styled = {
	FormFieldset,
	SelectTeamWrapper,
	SectionTitle,
	FormFooter,
	CustomIcon,
	InputWrapper,
	Label
};

export default Styled;
