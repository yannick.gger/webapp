import { useEffect, useState } from 'react';
import { Formik, Form } from 'formik';
import { useTranslation } from 'react-i18next/hooks';
import { find } from 'lodash';
import * as Yup from 'yup';

import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { createClient } from 'shared/ApiClient/ApiClient';
import { KEY_COLLABS_API_USER_OBJECT } from 'constants/localStorage-keys';
import BrandsService from 'services/Brands';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { setName, setBrand, setInfo, addTeamMember, removeTeamMember, setAvailableUsers } from '../../reducers/campaignInfoSlice';
import LoadingSpinner from 'components/LoadingSpinner';
import useRestCollection from '../../../../hooks/useRestCollection';
import InputText from 'components/Form/Elements/Text';
import Select from 'components/Form/Elements/Select/Select';
import TeamMemberList from '../../Components/TeamMemberList';
import { Brand, CONFIG_STEP, TeamMember } from '../../index.d';
import { createErrorSelector, selectCampaignId } from '../../reducers/uiSlice';
import Styled from './InfoStep.style';
import NextStepButton from '../NextStepButton';

/**
 * @todo i18next
 */
const InfoStep = () => {
	const [availableUsersLoading, setAvailableUsersLoading] = useState(false);
	const dispatch = useAppDispatch();
	const initialValues = useAppSelector(({ info }) => ({
		name: info.name,
		brand: info && info.brand ? info.brand.id : ''
	}));
	const { availableUsers, users } = useAppSelector(({ info }) => info);
	const initialErrors = useAppSelector(createErrorSelector(CONFIG_STEP.INFO));
	const campaignId = useAppSelector(selectCampaignId);
	const [t] = useTranslation('campaign');
	const { data, error, loading } = useRestCollection<Brand>('/brands');

	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.min(2, t('validation.min'))
			.max(255, t('validation.max'))
			.required(t('validation.required')),
		brand: Yup.string().required(t('validation.required'))
	});
	const options = data ? data!.data.map(({ id, attributes }) => ({ label: attributes.name, value: id })) : [];

	const storage = new BrowserStorage(StorageType.LOCAL);

	const getBrandAvailableUsers = (brandId: string) => {
		setAvailableUsersLoading(true);
		const currentUser = storage.getItem(KEY_COLLABS_API_USER_OBJECT) || '';
		let currentUserId = '';
		if (currentUser !== '') {
			currentUserId = JSON.parse(currentUser).id;
		}
		BrandsService.getAvailableUsers(brandId)
			.then((res) => {
				const users = res.data.map((user: any) => {
					return {
						id: user.id,
						name: user.attributes.name,
						profileImage: user.attributes.profilePictureUrl,
						role: t(`${user.attributes.title}`),
						isAssigned: user.id === currentUserId
					};
				});
				dispatch(setAvailableUsers(users));

				const assignedUsers = users.filter((user: TeamMember) => user.isAssigned);
				assignedUsers.forEach((user: TeamMember) => {
					dispatch(addTeamMember(user));
				});
			})
			.finally(() => {
				setAvailableUsersLoading(false);
			});
	};

	useEffect(() => {
		if (initialValues.brand) {
			getBrandAvailableUsers(initialValues.brand);
		}
	}, [initialValues.brand]);

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={async (values) => {
				const brand = find(data!.data, { id: values.brand as string });

				dispatch(
					setInfo({
						brand,
						name: values.name
					})
				);

				if (campaignId) {
					await createClient().patch(`/campaigns/${campaignId}`, {
						...values,
						users: users.map(({ id }) => id)
					});
				}
			}}
			initialErrors={initialErrors}
		>
			<Form>
				<Styled.FormFieldset>
					<legend>Create your campaign</legend>
					<Styled.InputWrapper>
						<Styled.Label htmlFor='name' className='mt-2'>
							{t('form_label.name')}
						</Styled.Label>
						<InputText
							id='name'
							name='name'
							placeholder={t('form_placeholder.name')}
							required
							max={255}
							size='lg'
							onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
								dispatch(setName(e.target.value));
							}}
							autocomplete='off'
						/>
					</Styled.InputWrapper>

					<Styled.InputWrapper>
						<Styled.Label htmlFor='brand' className='mt-2'>
							{t('form_label.brand')}
						</Styled.Label>
						{loading && <LoadingSpinner />}
						{error && <p className='error'>{error}</p>}
						{data && (
							<Select
								id='brand'
								name='brand'
								label={t('form_label.brand')}
								placeholder='Sub brand'
								size='lg'
								options={options}
								onChangeCallable={(brandId: string) => {
									const brand = find(data!.data, { id: brandId });
									dispatch(setBrand(brand));
									getBrandAvailableUsers(brandId);
								}}
							/>
						)}
					</Styled.InputWrapper>
				</Styled.FormFieldset>

				{availableUsersLoading ? (
					<LoadingSpinner />
				) : availableUsers.length ? (
					<Styled.SelectTeamWrapper>
						<Styled.SectionTitle>
							<strong>Select your team</strong>
						</Styled.SectionTitle>
						<TeamMemberList
							members={availableUsers || []}
							onAssign={(selectedTeamMember: TeamMember) => {
								const newTeamMembers = availableUsers.map((member: TeamMember) => {
									if (member.id === selectedTeamMember.id) {
										return { ...member, isAssigned: !selectedTeamMember.isAssigned };
									} else {
										return member;
									}
								});
								dispatch(setAvailableUsers(newTeamMembers));

								if (selectedTeamMember.isAssigned) {
									dispatch(removeTeamMember(selectedTeamMember));
								} else {
									dispatch(addTeamMember(selectedTeamMember));
								}
							}}
						/>
					</Styled.SelectTeamWrapper>
				) : null}
				<Styled.FormFooter>
					<NextStepButton />
				</Styled.FormFooter>
			</Form>
		</Formik>
	);
};

export default InfoStep;
