import styled from 'styled-components';

import colors from 'styles/variables/colors';
import { Flex, Borders } from 'styles/utils/UtilityClasses';

const Wrapper = styled.section`
	${Flex};
	${Borders};

	ul {
		list-style: none;

		li {
			padding: 1.5rem 0;
			margin-bottom: 1.5rem;
			border-bottom: 1px solid ${colors.borderColor};
		}
	}
`;

export default {
	Wrapper
};
