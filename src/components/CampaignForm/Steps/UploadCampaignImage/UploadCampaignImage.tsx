import React, { useContext, useState } from 'react';
import Styled from './UploadCampaignImage.style';
import CampaignsService from 'services/Campaign/Campaigns.service';
import { Button } from '../../../Button';
import useRestCollection from '../../../../hooks/useRestCollection';
import LoadingSpinner from '../../../LoadingSpinner';
import ToastContext from 'contexts/ToastContext';
import { useSWRConfig } from 'swr';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { setCampaignCover } from '../../reducers/uiSlice';
import { Link, useParams } from 'react-router-dom';

type RouteParams = {
	campaignId: string;
	organizationSlug: string;
};

type CampaignCoverPhoto = {
	createdAt: string;
	contentType: string;
	uuid: string;
};

const UploadCampaignImage = () => {
	const { campaignId, organizationSlug } = useParams<RouteParams>();
	const [file, setFile] = useState<File | null>(null);
	const { data, loading } = useRestCollection<CampaignCoverPhoto>('/campaign-cover-photos');
	const campaignCover = useAppSelector((state) => state.ui.campaignCover);
	const { mutate } = useSWRConfig();
	const { addToast } = useContext(ToastContext);
	const dispatch = useAppDispatch();

	return (
		<Styled.Wrapper>
			<form
				onSubmit={async (e) => {
					e.preventDefault();
					try {
						await CampaignsService.uploadCampaignCoverImage(file);
						await mutate('/campaign-cover-photos');
						setFile(null);
					} catch (e) {
						addToast({ id: 'err-update-campaign', message: 'Something went wrong', mode: 'err' });
						console.error('Unexpected error: %O', e);
					}
				}}
			>
				<fieldset>
					<legend>Upload new cover image</legend>
					<div className='d-flex justify-between border-1'>
						<input type='file' onChange={({ target }) => setFile(target.files![0])} />
						<Button type='submit' disabled={null === file}>
							Upload
						</Button>
					</div>
				</fieldset>
			</form>
			{loading && <LoadingSpinner position='center' />}
			{data && (
				<ul>
					{data.data.map(({ id, links, attributes }) => (
						<li key={id} className='d-flex justify-between align-center'>
							<img src={links!.smallCoverPhoto} alt={attributes.uuid} width={120} />
							{campaignCover !== links!.horizontalCoverPhoto ? (
								<Button
									size='sm'
									onClick={async () => {
										await CampaignsService.setCampaignImage(campaignId!, id);
										dispatch(setCampaignCover(links!.horizontalCoverPhoto));
										addToast({ id: 'success-update-campaign', message: 'Success', mode: 'success' });
									}}
								>
									Use as cover
								</Button>
							) : (
								<span>Cover image</span>
							)}
						</li>
					))}
				</ul>
			)}
		</Styled.Wrapper>
	);
};

export default UploadCampaignImage;
