import React from 'react';
import Styled from './InfoStep/InfoStep.style';
import { Button } from '../../Button';
import { useAppSelector } from '../hooks';
import { selectCampaignId } from '../reducers/uiSlice';
import { useFormikContext } from 'formik';

const NextStepButton = () => {
	const campaignId = useAppSelector(selectCampaignId);
	const submitButtonText = campaignId ? 'Save' : 'Next';
	const { isSubmitting, isValid } = useFormikContext();

	return (
		<Button size='sm' type='submit' className='ml-auto mt-2' disabled={isSubmitting || !isValid}>
			{isSubmitting ? '...' : submitButtonText}
			<Styled.CustomIcon icon='arrow-right' size='24' />
		</Button>
	);
};

export default NextStepButton;
