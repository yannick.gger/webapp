import { useState, useEffect } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import LoadingSpinner from 'components/LoadingSpinner';
import Grid from 'styles/grid/grid';
import useRestCollection from '../../../../hooks/useRestCollection';
import CreateCampaignIcon from 'assets/img/app/icon-new-large.svg';
import OnHoverCreateCampaignIcon from 'assets/img/app/icon-duplicate-large-endstate.svg';
import CopyCampaignIcon from 'assets/img/app/icon-duplicate-large.svg';
import OnHoverCopyCampaignIcon from 'assets/img/app/icon-duplicate.svg';
import Styled from './SelectActionStep.style';
import Dropdown from 'components/Dropdown';
import { DraftCampaigns, CampaignItem } from '../../index.d';

/**
 * @todo i18next
 */
const SelectActionStep = (props: { onCreateCampaign: () => void }) => {
	const [createCampaignIcon, setCreateCampaignIcon] = useState(CreateCampaignIcon);
	const [copyCampaignIcon, setCopyCampaignIcon] = useState(CopyCampaignIcon);
	const [draftCampaigns, setDraftCampaigns] = useState<CampaignItem[] | null>(null);

	const history = useHistory();
	const { params } = useRouteMatch();
	const { data: campaigns, error: draftCampaignsError, loading: draftCampaignsLoading } = useRestCollection<DraftCampaigns>('/campaigns?statues=draft');

	useEffect(() => {
		if (!draftCampaignsLoading && !draftCampaignsError) {
			if (campaigns) {
				setDraftCampaigns(campaigns.data);
			}
		}
	}, [draftCampaignsLoading]);

	return (
		<Styled.Wrapper>
			<Styled.Title>Create a new campaign</Styled.Title>
			<Styled.Phrase>How would you like to start?</Styled.Phrase>
			<Styled.SelectsWrapper>
				<Grid.Container>
					<Grid.Column sm={6}>
						<Styled.SelectWrapper
							onMouseEnter={() => setCreateCampaignIcon(OnHoverCreateCampaignIcon)}
							onMouseLeave={() => setCreateCampaignIcon(CreateCampaignIcon)}
							onClick={props.onCreateCampaign}
						>
							<Styled.IconBox>
								<img src={createCampaignIcon} alt='create a campaign' />
							</Styled.IconBox>
							<div>
								<Styled.SelectTitle data-testid='create-new'>New campaign</Styled.SelectTitle>
								<Styled.SelectPhrase>Start from scratch</Styled.SelectPhrase>
							</div>
						</Styled.SelectWrapper>
					</Grid.Column>
					<Grid.Column sm={6}>
						<Styled.SelectWrapper onMouseEnter={() => setCopyCampaignIcon(OnHoverCopyCampaignIcon)} onMouseLeave={() => setCopyCampaignIcon(CopyCampaignIcon)}>
							<Styled.IconBox>
								<img src={copyCampaignIcon} alt='copy a campaign' />
							</Styled.IconBox>
							<div>
								<Styled.SelectTitle>Copy campaign</Styled.SelectTitle>
								<Styled.SelectPhrase>Use a previous campaign</Styled.SelectPhrase>
							</div>
						</Styled.SelectWrapper>
					</Grid.Column>
				</Grid.Container>
			</Styled.SelectsWrapper>
			<Styled.DraftsWrapper>
				<Styled.DraftButton title='Drafts' icon='chevron-down'>
					<Dropdown.Menu>
						<Styled.DraftDropdownInnerWrapper className='scrollbar'>
							{draftCampaignsLoading ? (
								<LoadingSpinner size='sm' />
							) : draftCampaigns ? (
								draftCampaigns.map((campaign) => {
									return (
										<div
											key={campaign.id}
											onClick={() => {
												history.push(`/${params.organizationSlug}/campaigns/${campaign.id}`);
											}}
										>
											<Dropdown.Item>{campaign.attributes.name}</Dropdown.Item>
										</div>
									);
								})
							) : (
								<></>
							)}
						</Styled.DraftDropdownInnerWrapper>
					</Dropdown.Menu>
				</Styled.DraftButton>
			</Styled.DraftsWrapper>
		</Styled.Wrapper>
	);
};

export default SelectActionStep;
