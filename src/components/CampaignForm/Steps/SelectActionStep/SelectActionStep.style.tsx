import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import Dropdown from 'components/Dropdown';

const Wrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const Title = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-size: ${typography.headings.h4.fontSize};
	font-weight: ${typography.headings.h4.fontWeight};
	margin-bottom: ${guttersWithRem.l};
`;

const Phrase = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: ${typography.button.large.fontSize};
	margin-bottom: ${guttersWithRem.xxxl};
`;

const SelectsWrapper = styled.div`
	width: 100%;
	margin-bottom: ${guttersWithRem.xxxl};
`;

const IconBox = styled.div`
	border: 2px solid ${colors.black};
	padding: 3rem 1rem;
	margin-bottom: ${guttersWithRem.m};
	& > img {
		width: 100%;
		max-width: 150px;
		max-height: 150px;
	}
`;

const SelectWrapper = styled.div`
	max-height: 358px;
	height: 100%;
	width: 100%;
	text-align: center;
	cursor: pointer;
	padding: 0.5rem 0;
	transition: all 0.2s ease;

	&:hover {
		background-color: ${colors.createCampaign.widgetHoverBackground};
		border-radius: 2px;
		padding: 0.3125rem 0.3125rem 0.6875rem;

		${IconBox} {
			animation: shrink 0.2s ease-in-out both;

			@keyframes shrink {
				0% {
					-webkit-transform: scale(1);
					transform: scale(1);
				}
				50% {
					-webkit-transform: scale(0.99);
					transform: scale(0.99);
				}
				100% {
					-webkit-transform: scale(0.98);
					transform: scale(0.98);
				}
			}
			img {
				animation: rotate-center 0.2s ease-in-out both;

				@keyframes rotate-center {
					0% {
						-webkit-transform: rotate(0);
						transform: rotate(0);
					}
					100% {
						-webkit-transform: rotate(180deg);
						transform: rotate(180deg);
					}
				}
			}
		}
	}
`;

const SelectTitle = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-size: ${typography.button.medium.fontSize};
`;

const SelectPhrase = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: ${typography.button.small.fontSize};
	color: ${colors.createCampaign.subTextColor};
`;

const DraftsWrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const DraftButton = styled(Dropdown)`
	button {
		color: ${colors.button.dark.color};
		padding-left: ${guttersWithRem.m};
		padding-right: ${guttersWithRem.m};
		background-color: ${colors.button.dark.background};
		border-radius: unset;

		i {
			fill: ${colors.button.dark.color};
		}

		&:hover,
		&.show {
			color: ${colors.button.dark.background};
			i {
				fill: ${colors.button.dark.background};
			}
		}
	}
`;

const DraftDropdownInnerWrapper = styled.div`
	max-height: 200px;
	overflow-y: auto;
`;

const Styled = {
	Wrapper,
	Title,
	Phrase,
	SelectsWrapper,
	SelectWrapper,
	IconBox,
	SelectTitle,
	SelectPhrase,
	DraftsWrapper,
	DraftButton,
	DraftDropdownInnerWrapper
};

export default Styled;
