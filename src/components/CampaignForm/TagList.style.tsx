import styled from 'styled-components';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const Wrapper = styled.ul`
	padding-left: 0;
	list-style: none;

	li:not(:last-child) {
		margin-right: 0.5rem;
	}

	li {
		display: inline-block;
		font-family: ${typography.label.fontFamily};
		font-size: ${typography.label.small.fontSize};
		background-color: ${colors.buttonGray};
		border: 1px solid ${colors.black};
		border-radius: 2px;
		padding: 0.25rem 0.5rem;
	}

	button {
		display: inline-block;
		border: 0 none;
		background: transparent;
		margin-left: 0.25rem;
		cursor: pointer;
	}
`;

export default {
	Wrapper
};
