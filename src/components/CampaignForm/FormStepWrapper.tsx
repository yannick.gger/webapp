import { useEffect } from 'react';
import { useAppSelector, useAppDispatch } from './hooks';
import { resetCampaignForm, setFirstRender } from './reducers/uiSlice';
import { resetCampainInfo } from './reducers/campaignInfoSlice';
import { resetSocialConfig } from './reducers/campaignSocialConfigSlice';
import { resetCamapaignDetails } from './reducers/campaignDetailsSlice';
import InfoStep from './Steps/InfoStep';
import DetailsStep from './Steps/DetailsStep';
import SocialConfigStep from './Steps/SocialConfigStep';
import SelectActionStep from './Steps/SelectActionStep';
import Styled from './FormStepWrapper.style';
import UploadCampaignImage from './Steps/UploadCampaignImage/UploadCampaignImage';

const FormStepWrapper = () => {
	const { step, isFirstRender, campaignId } = useAppSelector((state) => state.ui);
	const dispatch = useAppDispatch();
	const steps = [<InfoStep />, <DetailsStep />, <SocialConfigStep />];

	if (campaignId) {
		steps.push(<UploadCampaignImage />);
	}

	const createCampaignHandler = () => dispatch(setFirstRender(false));

	useEffect(() => {
		return () => {
			dispatch(resetCampaignForm());
			dispatch(resetCampainInfo());
			dispatch(resetSocialConfig());
			dispatch(resetCamapaignDetails());
		};
	}, []);

	return (
		<Styled.Wrapper>
			<p className='creation-step'>
				Step {step + 1} / {steps.length}
			</p>
			{isFirstRender ? <SelectActionStep onCreateCampaign={createCampaignHandler} /> : steps[step]}
		</Styled.Wrapper>
	);
};

export default FormStepWrapper;
