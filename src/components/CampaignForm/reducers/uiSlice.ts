import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { pick } from 'lodash';

import { setInfo } from './campaignInfoSlice';
import { RootState } from '../store';
import { ErrorResponse, ItemResponse } from 'types/http';
import { CampaignItem, CONFIG_STEP } from '../index.d';

type SliceState = {
	isFirstRender: boolean;
	campaignCover?: string;
	campaignId?: string;
	step: number;
	errors: Record<string, string>;
};

export const initialState: SliceState = {
	isFirstRender: true,
	step: CONFIG_STEP.INFO,
	errors: {}
};

const FIELD_STEP_MAP: Record<string, CONFIG_STEP> = {
	name: CONFIG_STEP.INFO,
	brand: CONFIG_STEP.INFO,
	users: CONFIG_STEP.INFO,
	background: CONFIG_STEP.DETAILS,
	purpose: CONFIG_STEP.DETAILS,
	target: CONFIG_STEP.DETAILS,
	guidelines: CONFIG_STEP.DETAILS,
	influencerNumber: CONFIG_STEP.SOCIAL_CONFIG,
	hashtags: CONFIG_STEP.SOCIAL_CONFIG,
	mentions: CONFIG_STEP.SOCIAL_CONFIG
};

const uiSlice = createSlice({
	name: 'ui',
	initialState,
	reducers: {
		setStep(state, action: PayloadAction<number>) {
			state.step = action.payload;
		},
		nextStep(state) {
			++state.step;
		},
		previousStep(state) {
			--state.step;
		},
		setValidationErrors(state, action: PayloadAction<ErrorResponse>) {
			state.errors = {};

			for (const { source } of action.payload.errors) {
				const { parameter, message } = source;

				state.errors[parameter] = message;

				if (parameter in FIELD_STEP_MAP) {
					state.step = Math.min(state.step, FIELD_STEP_MAP[parameter]);
				}
			}
		},
		setFirstRender(state, { payload }: PayloadAction<boolean>) {
			state.isFirstRender = payload;
		},
		setCampaignData(state, action: PayloadAction<ItemResponse<CampaignItem>>) {
			const { id, links } = action.payload.data;

			state.step = CONFIG_STEP.UPLOAD_IMAGE;
			state.campaignId = id;
			state.campaignCover = links ? links.horizontalCoverPhoto : undefined;
			state.isFirstRender = false;
		},
		setCampaignCover(state, { payload }: PayloadAction<string>) {
			state.campaignCover = payload;
		},
		resetCampaignForm(state) {
			state.isFirstRender = true;
			state.campaignCover = undefined;
			state.campaignId = undefined;
			state.step = CONFIG_STEP.INFO;
			state.errors = {};
		}
	},
	// See: https://redux-toolkit.js.org/api/createSlice#extrareducers
	extraReducers: (builder) => {
		builder.addCase(setInfo, (state) => {
			++state.step;
		});
	}
});

export const { setStep, previousStep, nextStep, setValidationErrors, resetCampaignForm, setCampaignData, setFirstRender, setCampaignCover } = uiSlice.actions;

export const selectCurrentStep = (state: RootState) => state.ui.step;
export const selectCampaignId = (state: RootState) => state.ui.campaignId;
export const createErrorSelector = (step: CONFIG_STEP) => (state: RootState) => {
	const keys: string[] = [];

	for (const name in FIELD_STEP_MAP) {
		if (step === FIELD_STEP_MAP[name]) {
			keys.push(name);
		}
	}

	return pick(state.ui.errors, keys);
};

export default uiSlice.reducer;
