import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { setCampaignData } from './uiSlice';

type SliceState = {
	background: string;
	purpose: string;
	target: string;
	guidelines: string[];
};

const initialState: SliceState = {
	background: '',
	purpose: '',
	target: '',
	guidelines: []
};

const campaignDetailsSlice = createSlice({
	name: 'details',
	initialState,
	reducers: {
		setBackground(state, action: PayloadAction<string>) {
			state.background = action.payload;
		},
		setPurpose(state, action: PayloadAction<string>) {
			state.purpose = action.payload;
		},
		setTarget(state, action: PayloadAction<string>) {
			state.target = action.payload;
		},
		setGuidelines(state, action: PayloadAction<string>) {
			const newGuidelines = [...state.guidelines];
			state.guidelines = newGuidelines.concat(action.payload);
		},
		updateGuidelines(state, action: PayloadAction<string[]>) {
			state.guidelines = action.payload;
		},
		resetCamapaignDetails(state) {
			state.background = '';
			state.purpose = '';
			state.target = '';
			state.guidelines = [];
		}
	},
	// See: https://redux-toolkit.js.org/api/createSlice#extrareducers
	extraReducers: (builder) => {
		builder.addCase(setCampaignData, (state, { payload }) => {
			const { background, purpose, target, guidelines } = payload.data.attributes;

			state.background = background || '';
			state.purpose = purpose || '';
			state.target = target || '';
			state.guidelines = guidelines || [];
		});
	}
});

export const { resetCamapaignDetails, setBackground, setPurpose, setTarget, setGuidelines, updateGuidelines } = campaignDetailsSlice.actions;

export default campaignDetailsSlice.reducer;
