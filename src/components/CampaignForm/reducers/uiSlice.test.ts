import reducer, { initialState } from './uiSlice';
import { setInfo } from './campaignInfoSlice';

test.each([[setInfo({ name: 'foo', brand: 'bar' }), 1]])('Action %p set step to %d', (action, expected) => {
	const next = reducer(initialState, action);

	expect(next.step).toBe(expected);
});
