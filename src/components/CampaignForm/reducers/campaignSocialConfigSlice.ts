import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { filter, uniq } from 'lodash';
import { setCampaignData } from './uiSlice';

type SliceState = {
	influencerNumber: number;
	hashtags: string[];
	mentions: string[];
};

export const initialState: SliceState = {
	influencerNumber: 10,
	hashtags: [],
	mentions: []
};

const campaignSocialConfigSlice = createSlice({
	name: 'info',
	initialState,
	reducers: {
		setInfluencerNumber(state, action: PayloadAction<number>) {
			state.influencerNumber = action.payload;
		},
		setHashtags(state, action: PayloadAction<string>) {
			const newHashtags = [...state.hashtags];
			state.hashtags = uniq(newHashtags.concat(action.payload).map((tag) => (tag.startsWith('#') ? tag : `#${tag}`)));
		},
		removeHashtag(state, action: PayloadAction<string>) {
			state.hashtags = filter(state.hashtags, (hashtag) => hashtag !== action.payload);
		},
		setMentions(state, action: PayloadAction<string>) {
			const newMentions = [...state.mentions];

			state.mentions = uniq(newMentions.concat(action.payload).map((tag) => (tag.startsWith('@') ? tag : `@${tag}`)));
		},
		removeMention(state, action: PayloadAction<string>) {
			state.mentions = filter(state.mentions, (mention) => mention !== action.payload);
		},
		resetSocialConfig(state) {
			state.influencerNumber = 10;
			state.hashtags = [];
			state.mentions = [];
		}
	},
	// See: https://redux-toolkit.js.org/api/createSlice#extrareducers
	extraReducers: (builder) => {
		builder.addCase(setCampaignData, (state, { payload }) => {
			const { influencerTargetCount, hashtags, mentions } = payload.data.attributes;

			state.influencerNumber = influencerTargetCount;
			state.hashtags = hashtags;
			state.mentions = mentions;
		});
	}
});

export const { setInfluencerNumber, setHashtags, setMentions, removeHashtag, removeMention, resetSocialConfig } = campaignSocialConfigSlice.actions;

export default campaignSocialConfigSlice.reducer;
