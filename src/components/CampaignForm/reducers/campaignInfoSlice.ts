import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { filter, uniqBy } from 'lodash';
import { BrandItem, TeamMember } from '../index';
import { setCampaignData } from './uiSlice';
import { ItemAttributes } from '../../../types/http';

type SliceState = {
	name: string;
	brand?: BrandItem;
	users: TeamMember[];
	availableUsers: TeamMember[];
};

export const initialState: SliceState = {
	name: '',
	users: [],
	availableUsers: []
};

const campaignInfoSlice = createSlice({
	name: 'info',
	initialState,
	reducers: {
		setName(state, action: PayloadAction<string>) {
			state.name = action.payload;
		},
		setBrand(state, action: PayloadAction<any>) {
			state.brand = action.payload;
		},
		setInfo(state, action: PayloadAction<Pick<SliceState, 'brand' | 'name'>>) {
			const { name, brand } = action.payload;

			// Here state is mutable thanks to Immer, which is included out-of-the-box with slices.
			// Outside of slices, state should never be mutated directly.
			state.name = name;
			state.brand = brand;
		},
		addTeamMember(state, action: PayloadAction<TeamMember>) {
			state.users = uniqBy([...state.users, action.payload], 'id');
		},
		removeTeamMember(state, action: PayloadAction<TeamMember>) {
			state.users = filter(state.users, ({ id }) => id !== action.payload.id);
		},
		setAvailableUsers(state, action: PayloadAction<TeamMember[]>) {
			state.availableUsers = action.payload;
		},
		resetCampainInfo(state) {
			state.name = '';
			state.users = [];
			state.brand = undefined;
			state.availableUsers = [];
		}
	},
	// See: https://redux-toolkit.js.org/api/createSlice#extrareducers
	extraReducers: (builder) => {
		builder.addCase(setCampaignData, (state, { payload }) => {
			state.name = payload.data.attributes.name;
			state.brand = payload.included!.find(({ type }) => 'brand' === type);
			state.users = payload.included!.filter(({ type }) => 'user' === type).map(({ id, attributes }: ItemAttributes<TeamMember>) => ({ ...attributes, id }));
		});
	}
});

export const { setInfo, addTeamMember, removeTeamMember, setName, setBrand, setAvailableUsers, resetCampainInfo } = campaignInfoSlice.actions;

export default campaignInfoSlice.reducer;
