import reducer, { setInfo, initialState } from './campaignInfoSlice';

test('setInfo() update the state', () => {
	const next = reducer(initialState, setInfo({ brand: 'foo', name: 'bar' }));

	expect(next.brand).toBe('foo');
	expect(next.name).toBe('bar');
});
