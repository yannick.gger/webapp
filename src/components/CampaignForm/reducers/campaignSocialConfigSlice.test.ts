import reducer, { setInfluencerNumber, setHashtags, setMentions, removeHashtag, removeMention, initialState } from './campaignSocialConfigSlice';

test('setInfluncencerNumber() update the state', () => {
	const next = reducer(initialState, setInfluencerNumber(42));

	expect(next.influencerNumber).toBe(42);
});

test('setMentions() works as expected.', () => {
	const next = reducer(initialState, setMentions('foo'));

	expect(next.mentions).toEqual(['@foo']);
});

test('setHashtags() works as expected.', () => {
	const next = reducer(initialState, setHashtags('#foo'));

	expect(next.hashtags).toEqual(['#foo']);
});

test('removeMention()... removes mention!', () => {
	const next = reducer(
		{
			...initialState,
			mentions: ['@foo', '@bar', '@baz']
		},
		removeMention('@foo')
	);

	expect(next.mentions).toEqual(['@bar', '@baz']);
});

test('removeHashtag()... removes hastag!', () => {
	const next = reducer(
		{
			...initialState,
			hashtags: ['#foo', '#bar', '#baz']
		},
		removeHashtag('#foo')
	);

	expect(next.hashtags).toEqual(['#bar', '#baz']);
});
