import { configureStore } from '@reduxjs/toolkit';
import info from './reducers/campaignInfoSlice';
import details from './reducers/campaignDetailsSlice';
import socialConfig from './reducers/campaignSocialConfigSlice';
import ui from './reducers/uiSlice';

export const store = configureStore({
	reducer: {
		info,
		details,
		socialConfig,
		ui
	}
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
