import { useState, useEffect } from 'react';
import { Provider } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import withTheme from 'hocs/with-theme';
import { createClient } from 'shared/ApiClient/ApiClient';
import { isNil } from 'lodash';
import { store } from './store';

// Styles
import Grid from 'styles/grid/grid';

import CampaignSummary from './CampaignSummary';
import FormStepWrapper from './FormStepWrapper';
import TopControls from './Components/TopControls';
import Styled from './CampaignFormContainer.style';
import LoadingSpinner from '../LoadingSpinner';
import axios from 'axios';
import { setCampaignData } from './reducers/uiSlice';
import { ItemResponse } from '../../types/http';
import { CampaignItem } from './index';

type RouteParams = {
	campaignId: string;
	organizationSlug: string;
};
type Props = RouteComponentProps<RouteParams>;

const CampaignFormContainer = (props: Props) => {
	const { campaignId } = props.match.params;
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		if (isNil(campaignId)) {
			return;
		}
		const controller = new AbortController();

		setLoading(true);
		createClient()
			.get<ItemResponse<CampaignItem>>(`/campaigns/${campaignId}`, { params: { includes: 'brand,users' }, signal: controller.signal })
			.then(({ data }) => {
				store.dispatch(setCampaignData(data));
				setLoading(false);
			})
			.catch((e) => {
				if (!axios.isCancel(e)) {
					console.error('Unexpected response: %O', e);
				}
			});

		return () => controller.abort();
	}, [campaignId]);

	if (loading) {
		return <LoadingSpinner position='center' />;
	}

	return (
		<Provider store={store}>
			<Styled.Wrapper>
				<TopControls />
				<Grid.Container>
					<Grid.Column sm={3}>
						<CampaignSummary />
					</Grid.Column>
					<Grid.Column sm={6}>
						<FormStepWrapper />
					</Grid.Column>
				</Grid.Container>
			</Styled.Wrapper>
		</Provider>
	);
};

export default withTheme(CampaignFormContainer, 'createCampaign');
