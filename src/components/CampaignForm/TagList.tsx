import React from 'react';
import Styled from './TagList.style';

type Props = {
	items: string[];
	onDismiss: (item: string) => void;
};

const TagList = ({ items, onDismiss }: Props) => {
	return (
		<Styled.Wrapper>
			{items.map((item) => (
				<li key={item}>
					{item}
					<button onClick={() => onDismiss(item)} type='button' data-testid={`remove-tag${item.replace(/\W+/, '_')}`}>
						&times;
					</button>
				</li>
			))}
		</Styled.Wrapper>
	);
};

export default TagList;
