import { useTranslation } from 'react-i18next/hooks';
import { isNil } from 'lodash';

import { useAppDispatch, useAppSelector } from './hooks';
import Styled from './CampaignSummary.style';
import { setStep } from './reducers/uiSlice';
import SummaryList from './SummaryList';
import { CONFIG_STEP } from './index.d';
import { removeHashtag, removeMention } from './reducers/campaignSocialConfigSlice';
import TagList from './TagList';

const CampaignSummary = () => {
	const dispatch = useAppDispatch();
	const { info, details, socialConfig, ui } = useAppSelector((state) => state);
	const [t] = useTranslation();

	const { name, brand, users } = info;
	const { mentions, hashtags, influencerNumber } = socialConfig;
	const { step, campaignId } = ui;
	const isTouched = name || brand || users.length;

	if (!isTouched) {
		return null;
	}

	return (
		<Styled.Wrapper>
			<Styled.Title>{t(isNil(campaignId) ? 'campaign:summary.title' : 'campaign:summary.edit')}</Styled.Title>
			{(!isNil(campaignId) || step >= CONFIG_STEP.INFO) && (
				<>
					<Styled.SubTitle>{t('campaign:summary.info')}</Styled.SubTitle>
					<SummaryList item={{ name, brand: brand ? brand.attributes.name : '', team: users }} />
					<Styled.LinkButton onClick={() => dispatch(setStep(0))}>{t('default:actions.edit')}</Styled.LinkButton>
				</>
			)}
			{(!isNil(campaignId) || step >= CONFIG_STEP.DETAILS) && (
				<>
					<Styled.SubTitle>{t('campaign:summary.details')}</Styled.SubTitle>
					<SummaryList item={details} />
					<Styled.LinkButton onClick={() => dispatch(setStep(1))}>{t('default:actions.edit')}</Styled.LinkButton>
				</>
			)}
			{(!isNil(campaignId) || step >= CONFIG_STEP.SOCIAL_CONFIG) && (
				<>
					<Styled.SubTitle>{t('campaign:summary.social')}</Styled.SubTitle>
					<SummaryList item={{ influencerNumber }} />
					<Styled.DefinitionList>
						<dt>{t('campaign:summary.hashtags')}</dt>
						<dd>
							<TagList items={hashtags} onDismiss={(hashtag) => dispatch(removeHashtag(hashtag))} />
						</dd>
						<dt>{t('campaign:summary.mentions')}</dt>
						<dd>
							<TagList items={mentions} onDismiss={(mention) => dispatch(removeMention(mention))} />
						</dd>
						{CONFIG_STEP.SOCIAL_CONFIG !== step && (
							<Styled.LinkButton onClick={() => dispatch(setStep(CONFIG_STEP.SOCIAL_CONFIG))}>{t('default:actions.edit')}</Styled.LinkButton>
						)}
					</Styled.DefinitionList>
				</>
			)}
		</Styled.Wrapper>
	);
};

export default CampaignSummary;
