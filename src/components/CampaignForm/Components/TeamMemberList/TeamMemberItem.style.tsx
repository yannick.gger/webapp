import styled from 'styled-components';

import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import fontSize from 'styles/variables/font-size';
import colors from 'styles/variables/colors';

import Icon from 'components/Icon';
import Avatar from 'components/Avatar';

const Wrapper = styled.div`
	width: 100%;
	display: flex;
	align-items: center;
	padding: ${guttersWithRem.xxs} 0;
`;

const MemberImageWrapper = styled.div``;

const MemberInfo = styled.div`
	&.member-name {
		font-family: ${typography.BaseFontFamiliy};
		font-size: ${fontSize.m};
	}
	&.member-role {
		font-family: ${typography.SecondaryFontFamiliy};
		font-size: ${fontSize.s};
		color: ${colors.createCampaign.subTextColor};
	}
`;

const CustomAvatar = styled(Avatar)`
	margin-left: 0;
	margin-right: ${guttersWithRem.l};
`;

const MemberInfoWrapper = styled.div`
	flex: 1;
	display: flex;
	justify-content: space-between;
	align-items: center;

	position: relative;
	&:after {
		content: ' ';
		width: 100%;
		border-bottom: 1px solid black;
		position: absolute;
		bottom: -2px;
	}
`;

const AssignButton = styled.div`
	display: flex;
	align-items: center;
	cursor: pointer;
`;

const AssignState = styled.div<{ isAssigned: boolean }>`
&:after {
	content: '${(props) => (props.isAssigned ? 'Assigned' : 'Assign')}';
}
${AssignButton}:hover &:after {
	content: '${(props) => (props.isAssigned ? 'Unassign' : 'Assign')}';
	font-weight: 600;
}

`;
const CustomIcon = styled(Icon)<{ isAssigned: boolean }>`
	margin-left: 0;
	line-height: 0;
	transform: rotate(45deg);
	visibility: ${(props) => props.isAssigned && 'hidden'};

	${AssignButton}:hover & {
		visibility: ${(props) => props.isAssigned && 'visible'};
		transform: ${(props) => props.isAssigned && 'rotate(0)'};
	}
`;

const Styled = {
	Wrapper,
	MemberImageWrapper,
	MemberInfo,
	CustomAvatar,
	MemberInfoWrapper,
	CustomIcon,
	AssignState,
	AssignButton
};

export default Styled;
