import Styled from './TeamMemberItem.style';
import { TeamMember } from '../../index.d';

const TeamMemberItem = (props: { member: TeamMember; onAssign: () => void }) => {
	const { member } = props;
	return (
		<Styled.Wrapper>
			<Styled.MemberImageWrapper>
				<Styled.CustomAvatar imageUrl={member.profileImage} name={member.name} />
			</Styled.MemberImageWrapper>
			<Styled.MemberInfoWrapper>
				<Styled.MemberInfo>
					<Styled.MemberInfo className='member-name'>
						<strong>{member.name}</strong>
					</Styled.MemberInfo>
					<Styled.MemberInfo className='member-role'>{member.role}</Styled.MemberInfo>
				</Styled.MemberInfo>
				<Styled.AssignButton onClick={props.onAssign} data-testid={`test-member-${member.id}`}>
					<Styled.AssignState isAssigned={member.isAssigned} />
					<Styled.CustomIcon isAssigned={member.isAssigned} icon={'cancel-circle'} size='32' />
				</Styled.AssignButton>
			</Styled.MemberInfoWrapper>
		</Styled.Wrapper>
	);
};

export default TeamMemberItem;
