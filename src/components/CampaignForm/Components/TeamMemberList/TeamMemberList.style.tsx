import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const Wrapper = styled.div`
	width: 100%;
`;

const ListWrapper = styled.div`
	display: flex;
	flex-direction: column;
	row-gap: ${guttersWithRem.xs};
	margin-bottom: ${guttersWithRem.xxl};
`;

const AddMemberButton = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	color: ${colors.button.sencondary};
	cursor: pointer;
`;

const Styled = {
	Wrapper,
	ListWrapper,
	AddMemberButton
};

export default Styled;
