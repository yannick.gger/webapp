import TeamMemberItem from './TeamMemberItem';
import Styled from './TeamMemberList.style';
import { TeamMember } from '../../index.d';

const TeamMemberList = (props: { members: TeamMember[]; onAssign: (param: any) => void }) => {
	const {
		members = [
			{ id: 'abc', profileImage: '', name: 'John Doe', role: 'Manager', isAssigned: false },
			{ id: 'abcd', profileImage: '', name: 'John Doe', role: 'Manager', isAssigned: false },
			{ id: 'abce', profileImage: '', name: 'John Doe', role: 'Manager', isAssigned: false }
		]
	} = props;
	return (
		<Styled.Wrapper>
			<Styled.ListWrapper>
				{members.map((member) => {
					return (
						<TeamMemberItem
							key={member.id}
							member={member}
							onAssign={() => {
								props.onAssign(member);
							}}
						/>
					);
				})}
			</Styled.ListWrapper>
		</Styled.Wrapper>
	);
};

export default TeamMemberList;
