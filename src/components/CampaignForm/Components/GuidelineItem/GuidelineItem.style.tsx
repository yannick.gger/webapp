import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	padding: 0 ${guttersWithRem.xxs};
	display: flex;
	align-items: center;

	& textarea {
		display: inline-block;
		width: 100%;
		border: none;
		resize: none;
		background-color: ${colors.transparent};
		outline: none;

		&:focus {
			box-shadow: 0 0 1px ${colors.black};
		}
	}

	margin-bottom: ${guttersWithRem.s};

	& span {
		&.remover {
			cursor: pointer;
			&:hover {
				color: ${colors.error};
			}
		}
	}
`;

const Styled = {
	Wrapper
};

export default Styled;
