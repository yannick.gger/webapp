import React, { useState, useRef, useEffect } from 'react';
import Styled from './GuidelineItem.style';

const GuidelineItem = (props: { value: string; changeSaveHandler: (newValue: string) => void; removeItem: () => void }) => {
	const [textValue, setTextValue] = useState(props.value);
	const textareaRef = useRef<any>(null);

	useEffect(() => {
		textareaRef.current.style.height = '0px';
		const scrollHeight = textareaRef.current.scrollHeight;
		textareaRef.current.style.height = scrollHeight + 'px';
	}, [textValue]);

	const enterSaveUpdate = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
		if (e.code === 'Enter' && !e.shiftKey) {
			e.preventDefault();
			e.currentTarget.blur();
		}
	};

	const saveUpdate = () => {
		props.changeSaveHandler(textValue);
	};

	return (
		<Styled.Wrapper>
			<span>&#8226;</span>
			<textarea ref={textareaRef} value={textValue} onChange={(e) => setTextValue(e.target.value)} onKeyDown={enterSaveUpdate} onBlur={saveUpdate} />
			<span className='remover' onClick={props.removeItem}>
				&times;
			</span>
		</Styled.Wrapper>
	);
};

export default GuidelineItem;
