import styled from 'styled-components';

const Wrapper = styled.div`
	display: flex;
	align-items: center;
`;

const NavigateBack = styled.button`
	background-color: transparent;
	border: none;
	padding: 0;
	cursor: pointer;
`;

const Close = styled.button`
	background-color: transparent;
	border: none;
	padding: 0;
	margin-left: auto;
	cursor: pointer;
`;

const Styled = {
	Wrapper,
	NavigateBack,
	Close
};

export default Styled;
