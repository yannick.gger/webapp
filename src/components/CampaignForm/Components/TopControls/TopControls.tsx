import Styled from './TopControls.style';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { previousStep, selectCurrentStep } from '../../reducers/uiSlice';
import { useHistory } from 'react-router-dom';
import Icon from 'components/Icon';

const TopControls = () => {
	const history = useHistory();
	const dispatch = useAppDispatch();
	const currentStep = useAppSelector(selectCurrentStep);

	return (
		<Styled.Wrapper>
			{currentStep > 0 && (
				<Styled.NavigateBack onClick={() => dispatch(previousStep())}>
					<Icon icon='chevron-left' />
				</Styled.NavigateBack>
			)}
			<Styled.Close onClick={() => history.goBack()}>
				<Icon icon='cross' />
			</Styled.Close>
		</Styled.Wrapper>
	);
};

export default TopControls;
