import { useTranslation } from 'react-i18next/hooks';
import Styled from './CampaignSummary.style';
import { map, truncate } from 'lodash';
import { TeamMember } from './index';

type Props = {
	item: Record<string, string | number | TeamMember[] | string[]>;
};

const SummaryList = (props: Props) => {
	const [t] = useTranslation('campaign');

	const renderTeamMemberSummary = (value: TeamMember[]) => {
		return value.map((user) => <dd key={user.id}>{user.name}</dd>);
	};

	const renderGuidelines = (value: string[]) => {
		return value.map((guideline, index) => (
			<dd key={index}>
				<span>&#8226;</span>
				{guideline}
			</dd>
		));
	};

	return (
		<Styled.DefinitionList>
			{map(props.item, (value, key) => (
				<Styled.DefinitionList key={key}>
					<dt>{t(`form_label.${key}`)}</dt>
					{key === 'team' ? (
						renderTeamMemberSummary(value as TeamMember[])
					) : key === 'guidelines' ? (
						renderGuidelines(value as string[])
					) : (
						<dd>
							{truncate(value ? value.toString() : '', {
								length: 42
							})}
						</dd>
					)}
				</Styled.DefinitionList>
			))}
		</Styled.DefinitionList>
	);
};

export default SummaryList;
