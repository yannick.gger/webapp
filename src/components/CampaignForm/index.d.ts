import { ItemAttributes } from 'types/http';

export interface TeamMember {
	id: string;
	name: string;
	role: string;
	profileImage: string;
	isAssigned: boolean;
}

export enum CONFIG_STEP {
	INFO,
	DETAILS,
	SOCIAL_CONFIG,
	UPLOAD_IMAGE
}

export interface CampaignDraft {
	name: string;
	users: string[];
	brand: string;
	background: string;
	purpose: string;
	target: string;
	guidelines: string[];
	influencerTargetCount: number;
	hashtags: string[];
	mentions: string[];
	language: string;
	currency: string;
	startDate?: string;
	endDate?: string;
	inviteClosedAt?: string;
}

export interface Brand {
	name: string;
}

export type BrandItem = ItemAttributes<T>;
export interface DraftCampaigns {
	data: CampaignItem[];
}

export type CampaignItem = ItemAttributes<T>;
