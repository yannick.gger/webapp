import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';

const Wrapper = styled.div`
	width: 100%;
	margin-left: auto;
	margin-right: auto;
	padding: 0 ${guttersWithRem.xxl};
`;

const Styled = {
	Wrapper
};

export default Styled;
