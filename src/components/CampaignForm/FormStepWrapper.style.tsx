import styled from 'styled-components';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';

const Wrapper = styled.section`
	max-width: 500px;
	margin: 0 auto;

	legend {
		font-family: ${typography.headings.fontFamily};
		font-size: 2.074rem;
		font-weight: 900;
		text-align: center;
	}

	.creation-step {
		color: ${colors.black};
		font-family: ${typography.label.fontFamily};
		font-size: ${typography.label.medium.fontSize};
		line-height: ${typography.label.medium.lineHeight};
		text-align: center;
		text-transform: uppercase;
		margin-bottom: 48px;
	}

	label {
		display: block;
	}

	.mt-2 {
		margin-top: ${guttersWithRem.s};
	}
`;

export default {
	Wrapper
};
