import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';

import { store } from './store';
import CampaignSummary from './CampaignSummary';
import { setInfo } from './reducers/campaignInfoSlice';
import { nextStep } from './reducers/uiSlice';
import { setBackground, setGuidelines, setPurpose, setTarget } from './reducers/campaignDetailsSlice';
import { setInfluencerNumber, setHashtags, setMentions } from './reducers/campaignSocialConfigSlice';

test('it matches the snapshot', async () => {
	const { asFragment } = render(
		<Provider store={store}>
			<CampaignSummary />
		</Provider>
	);

	store.dispatch(
		setInfo({
			name: 'My Campaign Name',
			brand: {
				id: '1234',
				type: 'Brand',
				attributes: {
					name: 'My brand'
				}
			}
		})
	);

	await screen.findByText('My Campaign Name');

	expect(asFragment()).toMatchSnapshot('Step #1');

	store.dispatch(setBackground('The background.'));
	store.dispatch(setPurpose('The purpose.'));
	store.dispatch(setTarget('The target.'));
	store.dispatch(setGuidelines('The guideline.'));

	expect(asFragment()).toMatchSnapshot('Step #2');
	store.dispatch(nextStep());

	store.dispatch(setInfluencerNumber(42));
	store.dispatch(setHashtags('foo'));
	store.dispatch(setMentions('me'));
	store.dispatch(setMentions('you'));
	store.dispatch(setMentions('everyone'));
	store.dispatch(setMentions('else'));
	expect(asFragment()).toMatchSnapshot('Step #3');

	fireEvent.click(screen.getByTestId('remove-tag_else'));

	expect(store.getState().socialConfig.mentions).toEqual(['@me', '@you', '@everyone']);
});
