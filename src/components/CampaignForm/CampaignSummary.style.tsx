import styled from 'styled-components';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';

const Wrapper = styled.aside`
	font-size: 0.9rem;
	padding: 0 ${guttersWithRem.xxl};
	max-width: 25rem;
`;

const Title = styled.h5`
	font-size: 1.2rem;
	line-height: 1.23;
	font-weight: 500;
`;
const SubTitle = styled.span`
	display: block;
	width: 100%;
	font-weight: normal;
	font-family: ${typography.label.fontFamily};
	font-size: 1rem;
	line-height: 1.5;
	border-bottom: 1px solid ${colors.black};
	margin-top: ${guttersWithRem.m};
	margin-bottom: 0.688rem;
	color: #888;
`;

const DefinitionList = styled.dl`
	font-family: ${typography.label.fontFamily};
	font-size: 0.75rem;

	& dt {
		font-weight: normal;
		line-height: 2;
	}

	& dd {
		font-family: ${typography.BaseFontFamiliy};
		font-size: 0.875rem;
		font-weight: bold;
		color: ${colors.black};
	}
`;

const LinkButton = styled.button`
	color: ${colors.mainBlue};
	border: 0 none;
	background-color: transparent;
	cursor: pointer;
`;

const Styled = {
	Wrapper,
	Title,
	SubTitle,
	DefinitionList,
	LinkButton
};

export default Styled;
