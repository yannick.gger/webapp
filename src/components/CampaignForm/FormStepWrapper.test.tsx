import { MemoryRouter, Route, useLocation } from 'react-router-dom';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';
import { CONFIG_STEP } from './index.d';

import FormStepWrapper from './FormStepWrapper';

import { store } from './store';

const LocationDisplay = () => {
	const location = useLocation();

	return <div data-testid='location-display'>{location.pathname}</div>;
};

test('displays the correct step', async () => {
	const { asFragment } = render(
		<MemoryRouter initialEntries={['/foo-bar/campaigns/create']}>
			<Provider store={store}>
				<ThemeProvider theme={themes.default}>
					<Route path='/:organizationSlug/campaigns/create'>
						<FormStepWrapper />
					</Route>
					<Route path='*'>
						<LocationDisplay />
					</Route>
				</ThemeProvider>
			</Provider>
		</MemoryRouter>
	);

	userEvent.click(await screen.findByTestId('create-new'));
	userEvent.type(await screen.findByLabelText('form_label.name'), 'Bond, James Bond');
	userEvent.type(await screen.findByRole('combobox'), 'My');
	userEvent.click(await screen.findByText('My brand'));
	// Wait for team members to be loaded
	await screen.findByText('Select your team');
	userEvent.click(await screen.findByTestId('test-member-3'));
	expect(asFragment()).toMatchSnapshot('Step #1');

	await waitFor(async () => {
		const btn = await screen.findByText<HTMLButtonElement>('Next');
		expect(btn.disabled).toBeFalsy();
		userEvent.click(btn);
	});

	expect(store.getState().ui.step).toBe(CONFIG_STEP.DETAILS);
	const detailsValues: Record<string, string> = {
		background: 'The background.',
		purpose: 'The purpose.',
		target: 'The target.'
	};

	for (const name in detailsValues) {
		userEvent.type(await screen.findByTestId(`form-control-${name}`), detailsValues[name]);
	}

	userEvent.type(await screen.findByTestId(`form-control-guidelines`), 'test{enter}');

	await waitFor(async () => {
		const btn = await screen.findByText<HTMLButtonElement>('Next');
		expect(btn.disabled).toBeFalsy();
		userEvent.click(btn);
	});
	expect(store.getState().ui.step).toBe(CONFIG_STEP.SOCIAL_CONFIG);

	userEvent.type(await screen.findByLabelText('form_label.influencerNumber'), '42');

	userEvent.type(await screen.findByTestId(`form-control-hashtags`), 'test{enter}');
	userEvent.type(await screen.findByTestId(`form-control-mentions`), 'test{enter}');

	userEvent.click(await screen.findByTestId('create-campaign-button'));

	await waitFor(async () => {
		const location = await screen.findByTestId('location-display');
		expect(location.innerHTML).toBe('/foo-bar/campaigns/1234');
	});
});
