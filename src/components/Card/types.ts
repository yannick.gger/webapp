export interface ICardStyle {
	width?: string;
	height?: string;
	display?: 'flex' | 'grid' | 'block';
	alignContent?: 'center' | 'space-between' | 'space-around' | 'space-evenly' | 'stretch';
	className?: string;
}

export interface ICard extends ICardStyle {
	children?: any;
}
