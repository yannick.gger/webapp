import { render, screen } from '@testing-library/react';
import renderer from 'react-test-renderer';
import '@testing-library/jest-dom';
import 'jest-styled-components';
import Card from './Card';
import { Header, Body, Footer } from './components';

describe('Card Component', () => {
	it('should render without children', () => {
		const CardComponent = render(<Card />);

		expect(CardComponent).toMatchSnapshot();
	});

	it('should render with Card Header, Body, Footer', () => {
		const CardCompoenet = render(
			<Card>
				<Header>Header</Header>
				<Body>Body</Body>
				<Footer>Footer</Footer>
			</Card>
		);
		expect(CardCompoenet).toMatchSnapshot();
	});

	it('should render with text Default Card', () => {
		render(<Card>Default Card</Card>);
		expect(screen.getByText('Default Card')).toBeInTheDocument();
	});

	it('should render with width 50%', () => {
		const CardComponent = renderer.create(<Card width='50%' />).toJSON();
		expect(CardComponent).toMatchSnapshot();
	});
});
