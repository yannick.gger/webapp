import { ComponentStory, ComponentMeta } from '@storybook/react';
import Card from './Card';
import { ICard } from './types';

export default {
	title: 'Card',
	component: Card,
	argTypes: {
		width: {
			control: { type: 'text' }
		},
		height: {
			control: { type: 'text' }
		},
		display: {
			options: ['grid', 'flex', 'block'],
			control: { type: 'select' }
		},
		alignContent: {
			options: ['center', 'space-between', 'space-around', 'space-evenly', 'stretch'],
			control: { type: 'select' }
		}
	}
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args: ICard) => (
	<Card {...args}>
		<Card.Header>{args.children.header}</Card.Header>
		<Card.Body>{args.children.body}</Card.Body>
		<Card.Footer>{args.children.footer}</Card.Footer>
	</Card>
);

export const CardComponent = Template.bind({});

CardComponent.args = {
	width: '50%',
	height: '300px',
	display: 'grid',
	alignContent: 'space-between',
	children: {
		header: 'Default Header',
		body: 'Default Body',
		footer: 'Default Footer'
	}
};
