import Card from './Card';
import { Header, Body, Footer } from './components';

export default Object.assign(Card, {
	Header: Header,
	Body: Body,
	Footer: Footer
});
