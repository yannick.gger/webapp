import styled from 'styled-components';
import { ICardStyle } from './types';
import colors from 'styles/variables/colors';

const Wrapper = styled.div<ICardStyle>`
	width: ${(props) => props.width || '100%'};
	height: ${(props) => props.height || 'auto'};
	padding: 10px 15px;

	border: 1px solid ${colors.borderGray};
	background-color: ${colors.white};
`;

const InnerWrapper = styled.div<ICardStyle>`
	width: 100%;
	height: 100%;

	display: ${(props) => props.display};
	align-content: ${(props) => props.alignContent};
`;

const Styled = {
	Wrapper,
	InnerWrapper
};

export default Styled;
