export interface IFooterStyle {
	display?: string;
	alignItems?: string;
	justifyContent?: string;
}

export interface IFooter extends IFooterStyle {
	children: any;
}
