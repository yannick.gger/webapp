export interface IHeaderStyle {
	display?: string;
	alignItems?: string;
	justifyContent?: string;
	fontSize?: string;
	margin?: string;
}

export interface IHeader extends IHeaderStyle {
	children: any;
}
