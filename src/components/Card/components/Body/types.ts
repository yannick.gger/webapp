export interface IBodyStyle {
	display?: string;
	alignItems?: string;
	justifyContent?: string;
}

export interface IBody extends IBodyStyle {
	children: any;
}
