import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Query } from 'react-apollo';
import { FormattedNumber } from 'react-intl';
import { Avatar, Drawer, Progress, Row, Col, Collapse, Icon, Button, Skeleton } from 'antd';
import { lookup } from 'country-data';
import { hasGhostUser } from 'services/auth-service.js';
import { getEngagementRate, getInfluencerScore, getEstimatedImpressions, renderScoreText, getInstagramUrl } from 'utils';
import { MaleIcon, FemaleIcon, ArrowOutlineIcon } from 'components/icons';
import emojiMan from './man-pouting.png';
import emojiHundred from './hundred.png';
import emojiFamily from './family.png';
import emojiArrow from './arrow.png';

import './style.scss';

function InfluencerData(props) {
	return (
		<div className='influencer-data-row'>
			{props.label}
			<span>
				<strong>{props.stat ? props.stat : ''}</strong>
				{props.rating && (
					<span style={{ textTransform: 'uppercase' }}>
						<h5 className={`recommendation-color-${props.rating}`}>{renderScoreText(props.rating)}</h5>
					</span>
				)}
			</span>
		</div>
	);
}

function ProgressWithLabel(props) {
	return (
		<div className='mb-5'>
			{props.label}
			<span style={{ width: '50%' }} className='fr'>
				<Progress percent={props.percent} strokeWidth={8} size='small' />
			</span>
		</div>
	);
}

function CustomProgress(props) {
	return (
		<svg height='30' width='100%'>
			<clipPath id='round-corner'>
				<rect x='0' y='0' rx='10' ry='10' width='100%' height='30' className='custom-progress-inner' />
			</clipPath>
			<rect x='0' y='0' rx='10' ry='10' width='100%' height='30' className='custom-progress-inner' />
			<rect x='0' y='0' clipPath='url(#round-corner)' width={`${props.percent}%`} height='30' style={{ fill: props.strokeColor }} />
			<text x='10' y='55%' dominantBaseline='middle' style={{ fill: '#fff', fontWeight: 'bold', fontSize: '14xpx' }}>
				{props.label}
			</text>
		</svg>
	);
}

class InstagramProfileDrawer extends React.Component {
	state = {
		visible: false
	};

	render() {
		const { instagramOwner, content } = this.props;

		let countries = [];
		let ages = [];
		let credibility = null;

		const engagementRate = getEngagementRate(instagramOwner.interactionRate);
		const estimatedImpressions = getEstimatedImpressions(instagramOwner, instagramOwner.followedByCount);
		const influencerScore = getInfluencerScore(Math.ceil(credibility / 10));

		return (
			<React.Fragment>
				<span className='drawerLink' onClick={() => this.setState({ visible: true })}>
					{content}
				</span>

				<Drawer
					className='instagram-preview-wrapper'
					width={498}
					placement='right'
					closable={false}
					onClose={() => this.setState({ visible: false })}
					visible={this.state.visible}
				>
					<div className='instagram-preview' style={{ paddingTop: hasGhostUser() ? 25 : 0 }}>
						<Row className='mb-10'>
							<Button data-ripple='rgba(0, 0, 0, 0.2)' size='small' className='ant-btn-gray' onClick={() => this.setState({ visible: false })}>
								Close
							</Button>
							<Button
								data-ripple='rgba(0, 0, 0, 0.2)'
								size='small'
								className='fr ant-btn-gray'
								href={getInstagramUrl(instagramOwner.instagramOwnerUser.username)}
								target='_blank'
								rel='noopener noreferrer'
							>
								Open on Instagram
								<img src={emojiArrow} className='emoji arrow' alt='' />
							</Button>
						</Row>
						<Row className='mt-40'>
							<Col span={5}>
								<Avatar className='influencer-avatar' size={90} src={instagramOwner.instagramOwnerUser.avatar} icon='user' />
							</Col>
							<Col xs={19} className='pl-5'>
								<h3 className='mt-10 mb-0'>
									@{instagramOwner.instagramOwnerUser.username}&nbsp;
									{instagramOwner.countryCode && (
										<React.Fragment>
											<span
												className={`header-flag flag-icon flag-icon-${instagramOwner.countryCode.toLowerCase()} ml-15 mr-10`}
												style={{ fontSize: '13px', transform: 'translate(0, -3px)' }}
											/>
											<span style={{ fontSize: '16px' }}>
												{lookup.countries({ alpha2: instagramOwner.countryCode })[0]
													? lookup.countries({ alpha2: instagramOwner.countryCode })[0].name
													: instagramOwner.countryCode}
											</span>
										</React.Fragment>
									)}
								</h3>
								<div className='quick-stats mb-10'>
									<p>
										<span>
											<FormattedNumber
												value={instagramOwner.followedByCount > 1000 ? instagramOwner.followedByCount / 1000 : instagramOwner.followedByCount}
												maximumFractionDigits={1}
											/>
											{instagramOwner.followedByCount > 1000 ? 'k ' : ' '}
										</span>
										followers&nbsp;
										{instagramOwner.interactionRate && (
											<React.Fragment>
												<span>{`${instagramOwner.interactionRate}% `}</span>
												engagement
											</React.Fragment>
										)}
									</p>
								</div>
							</Col>
						</Row>
						<Row className='influencer-tools mb-10'>
							<Col className='pr-10' xs={12} style={{ justifyContent: 'flex-end' }}>
								<Button block data-ripple='rgba(0, 0, 0, 0.2)' size='small' className='ant-btn-gray'>
									<Link
										to={{
											pathname: `/${this.props.match.params.organizationSlug}/discover/add-selection-to-list`,
											state: { instagramOwners: [{ id: instagramOwner.id }] }
										}}
									>
										Add to list
									</Link>
								</Button>
							</Col>
							<Col className='pl-10' xs={12} style={{ justifyContent: 'flex-start' }}>
								<Button block data-ripple='rgba(0, 0, 0, 0.2)' size='small' className='ant-btn-gray'>
									<Link
										to={{
											pathname: `/${this.props.match.params.organizationSlug}/discover/add-influencers-to-campaign`,
											state: { instagramOwners: [instagramOwner.id] }
										}}
									>
										Add to campaign
									</Link>
								</Button>
							</Col>
							{/* TODO: Add this back and make all columns span 8, no offset when feature is implemented
                    <Col xs={8} style={{ justifyContent: "flex-end" }}>
                      <Button>Find similar</Button>
                    </Col> */}
						</Row>
						<Collapse defaultActiveKey={['0', '1', '2']} bordered={false} ghost>
							<Collapse.Panel
								header={
									<React.Fragment>
										<img src={emojiHundred} className='emoji' alt='' />
										Collabs
										<Icon className='ml-10' component={ArrowOutlineIcon} style={{ fontSize: '11px' }} />
									</React.Fragment>
								}
								key='0'
								className='innerBorderPanel'
								showArrow={false}
							>
								<Row className='collabs-stats mb-10'>
									<Col className='pr-10' xs={12}>
										<h5>Score</h5>
										<CustomProgress
											percent={credibility ? credibility : 100}
											label={credibility ? `${Math.ceil(credibility / 10)}/10` : 'Unknown'}
											strokeColor='#1ebd66'
										/>
									</Col>
									<Col className='pl-10' xs={12}>
										<h5>Campaigns completed</h5>
										<CustomProgress
											percent={instagramOwner.totalJoinedCampaigns ? (instagramOwner.totalCompletedCampaigns / instagramOwner.totalJoinedCampaigns) * 100 : 100}
											label={
												instagramOwner.totalJoinedCampaigns > 0
													? `${instagramOwner.totalCompletedCampaigns}/${instagramOwner.totalJoinedCampaigns}`
													: 'New on Collabs'
											}
											strokeColor='#5d5dec'
										/>
									</Col>
								</Row>
							</Collapse.Panel>
							<Collapse.Panel
								header={
									<React.Fragment>
										<img src={emojiMan} className='emoji' alt='' />
										Influencer data
										<Icon className='ml-10' component={ArrowOutlineIcon} style={{ fontSize: '11px' }} />
									</React.Fragment>
								}
								key='1'
								className='innerBorderPanel'
								showArrow={false}
							>
								<div className='mb-5'>
									<InfluencerData label='Followers' stat={<FormattedNumber value={instagramOwner.followedByCount} unitDisplay='narrow' />} />
									<InfluencerData
										label='Engagement'
										stat={instagramOwner.interactionRate ? `${instagramOwner.interactionRate}%` : 'Unknown'}
										rating={engagementRate}
									/>
									<InfluencerData label='Estimated impressions' stat={estimatedImpressions} rating='estimated' />
									<InfluencerData label='Overall score' stat={credibility ? `${credibility / 10} / 10` : 'Not scored'} rating={influencerScore} />
								</div>
							</Collapse.Panel>
							<Collapse.Panel
								header={
									<React.Fragment>
										<img src={emojiFamily} className='emoji' alt='' />
										Engaged Audience
										<Icon className='ml-10' component={ArrowOutlineIcon} style={{ fontSize: '11px' }} />
									</React.Fragment>
								}
								key='2'
								className='innerBorderPanel pb-75'
								showArrow={false}
							>
								<p style={{ marginBottom: '10px' }}>No engaged audience data found.</p>
							</Collapse.Panel>
						</Collapse>
					</div>
				</Drawer>
			</React.Fragment>
		);
	}
}

export default withRouter(InstagramProfileDrawer);
