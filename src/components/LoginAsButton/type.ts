export interface ILoginAsButtonProps {
	mutationVariables: {
		[key: string]: string;
	};
	match: any;
	location: {
		pathname: string;
	};
	history: any;
}
