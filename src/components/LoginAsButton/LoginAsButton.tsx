import { useState, useContext } from 'react';
import { Button } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import { RouteComponentProps } from 'react-router-dom';

import { UserContext } from '../../contexts';
import { withRouterWorkaround } from 'hocs';
import { ILoginAsButtonProps } from './type';
import { setGhostToken, redirectToHome } from 'services/auth-service';
import signInAsGhostMutation from 'graphql/sign-in-as-ghost.graphql';
import getCurrentUser from 'graphql/get-current-user.graphql';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { GHOST_USER_EMAIL } from 'constants/localStorage-keys';

const LoginAsButton = (props: ILoginAsButtonProps & RouteComponentProps): JSX.Element => {
	const [loading, setLoading] = useState<boolean>(false);
	const context = useContext(UserContext);
	const { location, history, mutationVariables } = props;
	const storage = new BrowserStorage(StorageType.LOCAL);

	const loginAs = (client: any) => {
		setLoading(true);

		client
			.mutate({
				mutation: signInAsGhostMutation,
				variables: mutationVariables
			})
			.then(({ data }: any) => {
				setGhostToken(data.signInAsGhost.token, location.pathname);
				context.updateIsGhostUser();
				setLoading(false);
				client
					.query({
						query: getCurrentUser
					})
					.then(({ data }: { data: { currentUser: { email: string } } }) => {
						console.log(data);
						storage.setItem(GHOST_USER_EMAIL, data.currentUser.email);
					});
				redirectToHome({ history: history });
			});
	};

	return (
		<ApolloConsumer>
			{(client) => (
				<Button size='small' loading={loading} onClick={() => loginAs(client)}>
					Login as
				</Button>
			)}
		</ApolloConsumer>
	);
};

export default withRouterWorkaround(LoginAsButton);
