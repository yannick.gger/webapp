import React, { useState } from 'react';
import { isNil } from 'lodash';

import CopyIcon from 'assets/img/app/icon-duplicate-large.svg';
import { Button } from '../Button';

type Props = {
	description: string;
	value: string;
};
const CopyToClipboard = ({ description, value }: Props) => {
	const [copied, setCopied] = useState(false);
	const altText = `Copy ${description} to clipboard`;

	// Old navigator
	if (isNil(navigator.permissions)) {
		return null;
	}

	return (
		<Button
			title={altText}
			onClick={async () => {
				try {
					await navigator.clipboard.writeText(value);
					setCopied(true);
					setTimeout(() => setCopied(false), 3000);
				} catch (e) {
					console.error('Unable to access clipboard: %O', e);
				}
			}}
			size='sm'
		>
			{copied ? (
				<>Copied!</>
			) : (
				<>
					Copy <img src={CopyIcon} height={20} alt={altText} />
				</>
			)}
		</Button>
	);
};

export default CopyToClipboard;
