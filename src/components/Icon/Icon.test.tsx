import { render } from '@testing-library/react';
import Icon from './Icon';

const mockedOnClick = jest.fn();

const mockedProps = {
	icon: 'calendar'
};

describe('<Icon name="calendar" /> test', () => {
	it('renders the component', () => {
		const button = render(<Icon {...mockedProps} />);
		expect(button).toMatchSnapshot();
	});
});
