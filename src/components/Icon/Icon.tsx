import classNames from 'classnames';
import Styled from './Icon.style';
import { SvgIconComponents } from './SvgComponents';
import { IIconProps } from './types';

const Icon = (props: IIconProps) => {
	const { size, icon, className, testId } = props;
	const formattedClassName = classNames('icon', className);

	const resolveSvgByType = (type: string) => {
		type = type.toLowerCase();
		if (!SvgIconComponents[type]) {
			return null;
		}

		return SvgIconComponents[type];
	};

	const SvgIcon = resolveSvgByType(icon);
	return (
		<Styled.Icon title={props.title} role={props.role} className={formattedClassName} data-testid={testId}>
			{SvgIcon ? <SvgIcon width={size} height={size} aria-hidden={true} {...props.svgProps} /> : ''}
		</Styled.Icon>
	);
};

Icon.defaultProps = {
	size: '24',
	role: 'img'
};

export default Icon;
