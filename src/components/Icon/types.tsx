import { SVGProps, DetailedHTMLProps, HTMLAttributes } from 'react';

export interface IIconProps extends DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement> {
	icon: string;
	svgProps?: ISvgProps;
	iconFocusable?: boolean;
	iconAriaHidden?: boolean;
	size?: '10' | '16' | '24' | '32' | '40';
	testId?: string;
}

export interface ISvgProps extends SVGProps<SVGSVGElement> {
	pathClassName?: string;
	viewBox?: string;
}

export type svgIconComponents = {
	[key: string]: any;
};
