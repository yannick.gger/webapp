import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgMyLists(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<g id='icon-my-lists' transform='translate(1.5 1.5)'>
				<path
					id='Union_25'
					data-name='Union 25'
					d='M12700.7,10793.481a.7.7,0,0,1-.7-.7v-3.6a.7.7,0,0,1,.7-.7h3.606a.7.7,0,0,1,.7.7v3.6a.7.7,0,0,1-.7.7Zm.3-1h3v-3h-3Zm6.381-1.4a.5.5,0,0,1,0-1h11.744a.5.5,0,1,1,0,1Zm-6.684-5.334a.7.7,0,0,1-.7-.7v-3.6a.7.7,0,0,1,.7-.7h3.606a.7.7,0,0,1,.7.7v3.6a.7.7,0,0,1-.7.7Zm.3-1h3v-3h-3Zm6.381-1.408a.5.5,0,0,1,0-1h11.744a.5.5,0,1,1,0,1Zm-6.684-5.334a.7.7,0,0,1-.7-.7v-3.6a.7.7,0,0,1,.7-.7h3.606a.7.7,0,0,1,.7.7v3.6a.7.7,0,0,1-.7.7Zm.3-1h3v-3h-3Zm6.381-1.4a.5.5,0,0,1,0-1h11.744a.5.5,0,1,1,0,1Z'
					transform='translate(-12700.001 -10773)'
					stroke='rgba(0,0,0,0)'
					strokeWidth='1'
				/>
			</g>
		</svg>
	);
}
