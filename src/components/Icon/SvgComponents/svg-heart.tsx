import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 40 40',
	pathClassName: 'st0'
};

export function SvgHeart(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<rect id='Rectangle_2236' data-name='Rectangle 2236' fill='none' />
			<g id='Group_3270' data-name='Group 3270' transform='translate(5 5)'>
				<path
					id='Path_3832'
					data-name='Path 3832'
					d='M15.377,27.779c-2.624-1.9-5.34-3.775-7.948-5.787a14.808,14.808,0,0,1-4.6-5.712,9.43,9.43,0,0,1-.523-7.317,7.637,7.637,0,0,1,12.9-2.425l.309.343a1.779,1.779,0,0,1,.189-.352,7.575,7.575,0,0,1,8.437-2.069,7.708,7.708,0,0,1,4.935,7.169A12,12,0,0,1,25.833,19.7a28.74,28.74,0,0,1-5.663,4.8c-1.573,1.077-3.149,2.149-4.793,3.272m.071-18.993c-.247-.356-.439-.66-.657-.943A6.286,6.286,0,0,0,8.2,5.283a6.539,6.539,0,0,0-5.127,7.609,11.775,11.775,0,0,0,3.372,6.54c2.632,2.78,5.972,4.633,8.985,6.875,1.488-1.027,2.953-2.035,4.413-3.049.483-.336.957-.684,1.427-1.037a20.162,20.162,0,0,0,4.816-4.82,9.683,9.683,0,0,0,1.744-6.641A6.5,6.5,0,0,0,16.112,7.83c-.216.3-.421.607-.664.956'
					transform='translate(0.596 1.316)'
				/>
			</g>
		</svg>
	);
}
