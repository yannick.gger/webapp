import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgOptions(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<defs>
				<clipPath id='clipPath'>
					<rect id='Rectangle_1818' data-name='Rectangle 1818' width='24' height='24' />
				</clipPath>
			</defs>
			<g id='Group_2113' data-name='Group 2113' clipPath='url(#clipPath)'>
				<path id='Path_2209' data-name='Path 2209' d='M5.737,11.994a2,2,0,1,1-2-2,2,2,0,0,1,2,2' />
				<path id='Path_2210' data-name='Path 2210' d='M14,11.994a2,2,0,1,1-2-2,2,2,0,0,1,2,2' />
				<path id='Path_2211' data-name='Path 2211' d='M22.255,11.994a2,2,0,1,1-2-2,2,2,0,0,1,2,2' />
			</g>
		</svg>
	);
}
