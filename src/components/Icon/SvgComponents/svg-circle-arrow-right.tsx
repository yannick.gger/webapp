import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 21 21',
	pathClassName: 'st0'
};

export function SvgCircleArrowRight(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;
	return (
		<svg id='icon-circle-arrow-right' xmlns='http://www.w3.org/2000/svg' width='21' height='21' viewBox='0 0 21 21' {...svgProps}>
			<path
				id='Path_4986'
				data-name='Path 4986'
				d='M193.059,318.39l-5.61-5.61-.707.707,4.9,4.9H179.474v1h12.168l-4.9,4.9.707.707,5.61-5.61a.718.718,0,0,0,0-.99'
				transform='translate(-175.764 -308.384)'
				fill='#333'
			/>
			<path
				id='Path_4987'
				data-name='Path 4987'
				d='M196.764,318.884a10.5,10.5,0,1,0-10.5,10.5,10.5,10.5,0,0,0,10.5-10.5m-20,0a9.5,9.5,0,1,1,9.5,9.5,9.5,9.5,0,0,1-9.5-9.5'
				transform='translate(-175.764 -308.384)'
				fill='#333'
			/>
		</svg>
	);
}
