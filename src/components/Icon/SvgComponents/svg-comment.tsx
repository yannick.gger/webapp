import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 40 40',
	pathClassName: 'st0'
};

export function SvgComment(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<rect id='Rectangle_2197' data-name='Rectangle 2197' fill='none' />
			<g id='Group_2944' data-name='Group 2944' transform='translate(5 5)'>
				<path
					id='Path_3679'
					data-name='Path 3679'
					d='M28.123,3.392H2.7a.932.932,0,0,0-.929.932V22.765A.931.931,0,0,0,2.7,23.7H7.611a7.177,7.177,0,0,1-1.54,3.179.936.936,0,0,0-.115,1.089.922.922,0,0,0,.8.465.911.911,0,0,0,.179-.017C8.4,28.124,11.921,27.1,13.428,23.7H28.123a.932.932,0,0,0,.931-.931V4.324a.933.933,0,0,0-.931-.932m-.4,18.971H13.159a.93.93,0,0,0-.864.589,6.847,6.847,0,0,1-4.529,3.877,9.293,9.293,0,0,0,1.249-3.379.95.95,0,0,0-.212-.76.941.941,0,0,0-.715-.328H3.1V4.725H27.72Z'
					transform='translate(0.589 1.131)'
				/>
				<path id='Path_3680' data-name='Path 3680' d='M5.618,9.18H23.1a.667.667,0,0,0,0-1.333H5.618a.667.667,0,1,0,0,1.333' transform='translate(1.65 2.616)' />
				<path
					id='Path_3681'
					data-name='Path 3681'
					d='M16.2,11.124H5.619a.667.667,0,1,0,0,1.333H16.2a.667.667,0,0,0,0-1.333'
					transform='translate(1.651 3.708)'
				/>
				<path
					id='Path_3682'
					data-name='Path 3682'
					d='M18.293,11.124H15.555a.667.667,0,1,0,0,1.333h2.739a.667.667,0,0,0,0-1.333'
					transform='translate(4.963 3.708)'
				/>
			</g>
		</svg>
	);
}
