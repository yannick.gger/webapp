import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgSend(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<defs>
				<clipPath id='clipPath'>
					<rect id='Rectangle_1907' data-name='Rectangle 1907' width='24' height='24' fill='none' />
				</clipPath>
			</defs>
			<g id='Group_2265' data-name='Group 2265' clipPath='url(#clipPath)'>
				<path
					id='Path_2333'
					data-name='Path 2333'
					d='M21.8,11.327,5.3,4.868a.731.731,0,0,0-.781.164.718.718,0,0,0-.158.77L6.8,11.931,4.356,18.066a.718.718,0,0,0,.156.766.726.726,0,0,0,.518.216A.743.743,0,0,0,5.291,19l16.5-6.333a.718.718,0,0,0,0-1.34M5.523,17.84l2.159-5.408h6.64v-1H7.682l-2.155-5.4,15.227,5.962Z'
					fill='#333'
				/>
			</g>
		</svg>
	);
}
