import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 40 40',
	pathClassName: 'st0'
};

export function SvgNewMessage(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<defs>
				<clipPath id='clipPath'>
					<rect id='Rectangle_1937' data-name='Rectangle 1937' width='34.533' height='33.808' fill='none' />
				</clipPath>
			</defs>
			<g id='Rectangle_1938' data-name='Rectangle 1938' fill='#fff' stroke='#707070' strokeWidth='1' opacity='0'>
				<rect width='40' height='40' stroke='none' />
				<rect x='0.5' y='0.5' width='39' height='39' fill='none' />
			</g>
			<g id='Group_2470' data-name='Group 2470' transform='translate(2.734 3.096)'>
				<path
					id='Path_2404'
					data-name='Path 2404'
					d='M0,1.941V32.514H31.422V13.152H29.395V30.5H2.027V3.957h18.16V1.941Z'
					transform='translate(0 1.295)'
					fill='#333'
				/>
				<path
					id='Path_2405'
					data-name='Path 2405'
					d='M23.487,2.15l-17.2,17.2-.522,4.68,4.385-.875L27.3,6l-1.178-1.18L9.737,21.21,8.26,19.733l16.4-16.4Z'
					transform='translate(3.842 1.434)'
					fill='#333'
				/>
				<g id='Group_2471' data-name='Group 2471' transform='translate(0 0)'>
					<g id='Group_2470-2' data-name='Group 2470' clipPath='url(#clipPath)'>
						<path
							id='Path_2406'
							data-name='Path 2406'
							d='M22.8,2.516,20.63.345a1.174,1.174,0,0,0-1.66,0L17.078,2.236l3.833,3.833L22.8,4.178a1.176,1.176,0,0,0,0-1.662'
							transform='translate(11.385 0.001)'
							fill='#333'
						/>
					</g>
				</g>
			</g>
		</svg>
	);
}
