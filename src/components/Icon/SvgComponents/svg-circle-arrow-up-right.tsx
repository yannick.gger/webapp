import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 21 21',
	pathClassName: 'st0'
};

export function SvgCircleArrowUpRight(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;
	return (
		<svg id='icon-circle-arrow-up-right' xmlns='http://www.w3.org/2000/svg' width='21' height='21' viewBox='0 0 21 21' {...svgProps}>
			<path
				id='Path_4982'
				data-name='Path 4982'
				d='M146.719,313.73h-7.934v1h6.924l-8.6,8.6.707.707,8.6-8.6v6.93h1V314.43a.718.718,0,0,0-.7-.7'
				transform='translate(-131.764 -308.384)'
			/>
			<path
				id='Path_4983'
				data-name='Path 4983'
				d='M142.264,308.384a10.5,10.5,0,1,0,10.5,10.5,10.5,10.5,0,0,0-10.5-10.5m0,20a9.5,9.5,0,1,1,9.5-9.5,9.5,9.5,0,0,1-9.5,9.5'
				transform='translate(-131.764 -308.384)'
			/>
		</svg>
	);
}
