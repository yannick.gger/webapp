import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 40 40',
	pathClassName: 'st0'
};

export function SvgCancelCircle(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<g id='Path_3837' data-name='Path 3837' transform='translate(10 10)' fill='none'>
				<path d='M10,0A10,10,0,1,1,0,10,10,10,0,0,1,10,0Z' stroke='none' />
				<path
					d='M 10 1 C 5.037380218505859 1 1 5.037380218505859 1 10 C 1 14.96261978149414 5.037380218505859 19 10 19 C 14.96261978149414 19 19 14.96261978149414 19 10 C 19 5.037380218505859 14.96261978149414 1 10 1 M 10 0 C 15.52285003662109 0 20 4.477149963378906 20 10 C 20 15.52285003662109 15.52285003662109 20 10 20 C 4.477149963378906 20 0 15.52285003662109 0 10 C 0 4.477149963378906 4.477149963378906 0 10 0 Z'
					stroke='none'
					fill='#333'
				/>
			</g>
			<g id='Group_2692' data-name='Group 2692' transform='translate(12 20) rotate(-45)'>
				<path
					id='Path_1605'
					data-name='Path 1605'
					d='M-14883.285,318.893v11.313'
					transform='translate(14888.941 -318.893)'
					fill='none'
					stroke='#333'
					strokeWidth='1'
				/>
				<path id='Path_1606' data-name='Path 1606' d='M0,0V11.313' transform='translate(11.313 5.656) rotate(90)' fill='none' stroke='#333' strokeWidth='1' />
			</g>
			<rect id='Rectangle_2239' data-name='Rectangle 2239' fill='none' />
			<rect id='Rectangle_2240' data-name='Rectangle 2240' fill='none' />
		</svg>
	);
}
