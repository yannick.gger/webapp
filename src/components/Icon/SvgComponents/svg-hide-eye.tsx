import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 40 40',
	pathClassName: 'st0'
};

export function SvgHideEye(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<rect id='Rectangle_2233' data-name='Rectangle 2233' fill='none' />

			<g id='Group_3034' data-name='Group 3034' transform='translate(5 5)'>
				<path
					id='Path_3829'
					data-name='Path 3829'
					d='M18.389,19.062a6.355,6.355,0,0,1-3.231.9A6.526,6.526,0,0,1,8.64,13.439a6.355,6.355,0,0,1,.9-3.231l-.977-.977a7.765,7.765,0,0,0,.107,8.616,29.1,29.1,0,0,1-5.584-4.553A27.565,27.565,0,0,1,8.444,9.116l-.977-.977a28,28,0,0,0-5.8,4.7.666.666,0,0,0-.007.881c.269.311,6.68,7.6,13.495,7.6a12.216,12.216,0,0,0,4.529-.961Z'
					transform='translate(0.5 2.713)'
				/>
				<path
					id='Path_3830'
					data-name='Path 3830'
					d='M28.085,14.623c-.237-.3-5.887-7.441-13.176-7.441-.053,0-.107.008-.16.009s-.12-.009-.183-.009a7.8,7.8,0,0,0-1.74.2,15.414,15.414,0,0,0-3.52,1.076l-4.9-4.9a.667.667,0,0,0-.943,0A.658.658,0,0,0,3.4,4.4a.5.5,0,0,1,.1.064L25.26,26.232a.5.5,0,0,1,.064.1.66.66,0,0,0,.848-1.007l-4.587-4.585a29.364,29.364,0,0,0,6.472-5.26.668.668,0,0,0,.028-.853M16.721,15.872a2.307,2.307,0,0,0-2.993-2.993L10.68,9.832a6.457,6.457,0,0,1,2.5-1.16c.109-.019.219-.035.329-.051a6.3,6.3,0,0,1,.7-.069c.16-.012.319-.021.48-.025A6.479,6.479,0,0,1,19.769,18.92ZM21,19.521a7.812,7.812,0,0,0-.308-9.388,23.079,23.079,0,0,1,5.975,4.875A28.972,28.972,0,0,1,21,19.521'
					transform='translate(1.091 1.12)'
				/>
				<path id='Path_3831' data-name='Path 3831' d='M12.237,14.4l-2.224-2.224A2.306,2.306,0,0,0,12.237,14.4' transform='translate(3.338 4.059)' />
			</g>
		</svg>
	);
}
