import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgCross(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg' {...svgProps}>
			<path
				id='Path_4938'
				data-name='Path 4938'
				d='M196.264,221.567l-.683-.683-9.319,9.317-9.315-9.315-.683.683,9.316,9.315-9.316,9.315.683.683,9.316-9.315,9.319,9.318.683-.683-9.319-9.318Z'
				transform='translate(-176.263 -220.884)'
				fill='#888'
			/>
		</svg>
	);
}
