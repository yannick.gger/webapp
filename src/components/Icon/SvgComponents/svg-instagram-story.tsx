import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgInstagramStory(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<g id='icon-story' transform='translate(0.501 0.5)'>
				<path
					id='Union_29'
					data-name='Union 29'
					d='M12705.867,10792.928a.5.5,0,0,1,.444-.9,9.524,9.524,0,0,0,6.089.782v0a.5.5,0,1,1,.2.978,10.555,10.555,0,0,1-2.1.21h-.021A10.447,10.447,0,0,1,12705.867,10792.928Zm9.6-.715a.5.5,0,0,1,.131-.693,9.5,9.5,0,0,0-5.1-17.52.5.5,0,1,1,0-1,10.5,10.5,0,0,1,5.639,19.361.515.515,0,0,1-.258.07A.5.5,0,0,1,12715.469,10792.213Zm-11.954-.872a10.464,10.464,0,0,1-3.206-5.294.5.5,0,1,1,.971-.242,9.451,9.451,0,0,0,2.9,4.792.5.5,0,0,1,.041.681.5.5,0,0,1-.705.063Zm6.484-3.841v-3.5h-3.5v-1h3.5v-3.5h1v3.5h3.5v1h-3.5v3.5Zm-9.515-3.8a.5.5,0,0,1-.485-.515,10.387,10.387,0,0,1,1.082-4.336.5.5,0,1,1,.9.444,9.385,9.385,0,0,0-.983,3.924.5.5,0,0,1-.5.485Zm2.631-6.894a.5.5,0,0,1-.067-.7,10.945,10.945,0,0,1,.892-.8.4.4,0,0,1,.073-.058.5.5,0,1,1,.552.833,9.767,9.767,0,0,0-.8.725.514.514,0,0,1-.328.119A.491.491,0,0,1,12703.115,10776.8Z'
					transform='translate(-12699.999 -10773.001)'
					stroke='rgba(0,0,0,0)'
					stroke-width='1'
				/>
			</g>
		</svg>
	);
}
