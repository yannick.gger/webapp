import { svgIconComponents } from '../types';
import { SvgStar } from './svg-star';
import { SvgCalendar } from './svg-calendar';
import { SvgChevronDown } from './svg-chevron-down';
import { SvgFullscreen } from './svg-fullscreen';
import { SvgPin } from './svg-pin';
import { SvgOptions } from './svg-options';
import { SvgSend } from './svg-send';
import { SvgNewMessage } from './svg-new-message';
import { SvgCancel } from './svg-cancel';
import { SvgChevronLeft } from './svg-chevron-left';
import { SvgHeart } from './svg-heart';
import { SvgHideEye } from './svg-hide-eye';
import { SvgAddToFolder } from './svg-add-to-folder';
import { SvgComment } from './svg-comment';
import { SvgCancelCircle } from './svg-cancel-circle';
import { SvgSearch } from './svg-search';
import { SvgLookalike } from './svg-lookalike';
import { SvgChevronRight } from './svg-chevron-right';
import { SvgInstagram } from './svg-instagram';
import { SvgReels } from './svg-reels';
import { SvgTiktok } from './svg-tiktok';
import { SvgTooltip } from './svg-tooltip';
import { SvgChevronUp } from './svg-chevron-up';
import { SvgCircleArrowUpRight } from './svg-circle-arrow-up-right';
import { SvgCirclePlus } from './svg-circle-plus';
import { SvgCross } from './svg-cross';
import { SvgArrowRight } from './svg-arrow-right';
import { SvgFilter } from './svg-filter';
import { SvgSort } from './svg-sort';
import { SvgMyLists } from './svg-my-lists';
import { SvgMail } from './svg-mail';
import { SvgProduct } from './svg-product';
import { SvgCircleArrowRight } from './svg-circle-arrow-right';
import { SvgInstagramPost } from './svg-instagram-post';
import { SvgInstagramStory } from './svg-instagram-story';
import { SvgMoney } from './svg-money';
import { SvgTrashBin } from './svg-trash-bin';
import { SvgUnHideEye } from './svg-unhide-eye';

export const SvgIconComponents: svgIconComponents = {
	star: SvgStar,
	calendar: SvgCalendar,
	fullscreen: SvgFullscreen,
	pin: SvgPin,
	options: SvgOptions,
	send: SvgSend,
	cancel: SvgCancel,
	'chevron-left': SvgChevronLeft,
	'chevron-right': SvgChevronRight,
	'chevron-down': SvgChevronDown,
	'chevron-up': SvgChevronUp,
	'new-message': SvgNewMessage,
	like: SvgHeart,
	hide: SvgHideEye,
	unhide: SvgUnHideEye,
	'add-new-folder': SvgAddToFolder,
	comment: SvgComment,
	search: SvgSearch,
	'cancel-circle': SvgCancelCircle,
	'look-a-like': SvgLookalike,
	instagram: SvgInstagram,
	reels: SvgReels,
	tiktok: SvgTiktok,
	tooltip: SvgTooltip,
	'circle-arrow-up-right': SvgCircleArrowUpRight,
	'circle-arrow-right': SvgCircleArrowRight,
	'circle-plus': SvgCirclePlus,
	cross: SvgCross,
	'arrow-right': SvgArrowRight,
	filter: SvgFilter,
	sort: SvgSort,
	'my-lists': SvgMyLists,
	mail: SvgMail,
	product: SvgProduct,
	'instagram-post': SvgInstagramPost,
	'instagram-story': SvgInstagramStory,
	money: SvgMoney,
	'trash-bin': SvgTrashBin
};
