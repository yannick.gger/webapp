import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 40 40',
	pathClassName: 'st0'
};

export function SvgSearch(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<rect id='Rectangle_1841' data-name='Rectangle 1841' fill='none' />

			<g id='Group_2106' data-name='Group 2106' transform='translate(10 10)'>
				<path
					id='Path_2205'
					data-name='Path 2205'
					d='M8.939,17.879a8.939,8.939,0,1,1,8.939-8.94,8.949,8.949,0,0,1-8.939,8.94M8.939,1a7.939,7.939,0,1,0,7.939,7.939A7.949,7.949,0,0,0,8.939,1'
					transform='translate(0 0)'
				/>
				<path
					id='Path_2206'
					data-name='Path 2206'
					d='M19.8,20.315a.5.5,0,0,1-.354-.146l-4.874-4.875a.5.5,0,1,1,.707-.707l4.875,4.874a.5.5,0,0,1-.354.854'
					transform='translate(0 0)'
				/>
			</g>

			<g id='Rectangle_1842' data-name='Rectangle 1842' fill='#fff' stroke='#707070' strokeWidth='1' opacity='0'>
				<rect width='24' height='24' stroke='none' />
				<rect x='0.5' y='0.5' width='23' height='23' fill='none' />
			</g>
		</svg>
	);
}
