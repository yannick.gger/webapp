import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 40 40',
	pathClassName: 'st0'
};

export function SvgAddToFolder(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<rect id='Rectangle_2172' data-name='Rectangle 2172' fill='none' width='100%' height='100%' />
			<g id='Group_2896-2' data-name='Group 2896' transform='translate(5 5)'>
				<path
					id='Path_3644'
					data-name='Path 3644'
					d='M19.375,13.939a5.709,5.709,0,1,0,5.709,5.709,5.715,5.715,0,0,0-5.709-5.709m0,10.085a4.376,4.376,0,1,1,4.376-4.376,4.381,4.381,0,0,1-4.376,4.376'
					transform='translate(4.555 4.646)'
				/>
				<path
					id='Path_3645'
					data-name='Path 3645'
					d='M19.488,18.388V15.719H18.155v2.669H15.5v1.333h2.655v2.524h1.333V19.721h2.539V18.388Z'
					transform='translate(5.167 5.24)'
				/>
				<path
					id='Path_3646'
					data-name='Path 3646'
					d='M16.557,25.124H3.121V4.682H8.768l1.972,1.96a.943.943,0,0,0,.657.271H26.609V8.448H6.339a.667.667,0,0,0,0,1.333H26.609v7.653a.667.667,0,1,0,1.333,0V6.514a.936.936,0,0,0-.936-.935H11.561L9.6,3.625a.935.935,0,0,0-.664-.276H2.724a.936.936,0,0,0-.936.935V25.522a.936.936,0,0,0,.936.935H16.557a.667.667,0,0,0,0-1.333'
					transform='translate(0.596 1.116)'
				/>
			</g>
		</svg>
	);
}
