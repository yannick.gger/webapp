import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 32 32',
	pathClassName: 'st0'
};

export function SvgCalendar(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<defs>
				<clipPath id='clipPath'>
					<rect id='Rectangle_1814' data-name='Rectangle 1814' width='32' height='32' fill='none' />
				</clipPath>
			</defs>
			<rect id='Rectangle_1806' data-name='Rectangle 1806' width='25.84' height='1.313' transform='translate(2.834 9.51)' />
			<rect id='Rectangle_1807' data-name='Rectangle 1807' width='13.128' height='1.313' transform='translate(7.22 20.742)' />
			<rect id='Rectangle_1808' data-name='Rectangle 1808' width='17.068' height='1.313' transform='translate(7.22 15.798)' />
			<g id='Group_2104' data-name='Group 2104'>
				<g id='Group_2103' data-name='Group 2103' clipPath='url(#clipPath)'>
					<path
						id='Path_2201'
						data-name='Path 2201'
						d='M28.631,4.82a1.611,1.611,0,0,0-.553-1.109,1.276,1.276,0,0,0-.915-.315H23.985V4.7h3.232a.238.238,0,0,1,.1.168l0,21.728c-.012.14-.083.21-.163.214l-23.979,0a.234.234,0,0,1-.1-.164l0-21.73a.285.285,0,0,1,.1-.219l.028,0H6.373V3.4H3.267a1.316,1.316,0,0,0-.944.314,1.643,1.643,0,0,0-.555,1.163V26.708a1.627,1.627,0,0,0,.555,1.109,1.318,1.318,0,0,0,.815.315.862.862,0,0,0,.1-.005l23.854,0a1.3,1.3,0,0,0,.979-.3,1.643,1.643,0,0,0,.561-1.164Z'
						transform='translate(0.553 1.06)'
					/>
					<rect id='Rectangle_1809' data-name='Rectangle 1809' width='9.735' height='1.311' transform='translate(10.865 4.455)' />
					<path id='Path_2202' data-name='Path 2202' d='M9.215,6.527V4.393H7.9v.822H6.589V4.393H5.276V6.527Z' transform='translate(1.651 1.374)' />
					<path id='Path_2203' data-name='Path 2203' d='M6.589,3.078H7.9V3.9H9.215V1.765H5.276V3.9H6.589Z' transform='translate(1.651 0.552)' />
					<rect id='Rectangle_1810' data-name='Rectangle 1810' width='1.313' height='1.312' transform='translate(6.927 4.456)' />
					<rect id='Rectangle_1811' data-name='Rectangle 1811' width='1.313' height='1.312' transform='translate(9.553 4.456)' />
					<path id='Path_2204' data-name='Path 2204' d='M17,3.078h1.313V3.9H19.63V1.765H15.691V3.9H17Z' transform='translate(4.909 0.552)' />
					<path id='Path_2205' data-name='Path 2205' d='M18.317,5.215H17V4.392H15.691V6.528H19.63V4.391H18.317Z' transform='translate(4.909 1.373)' />
					<rect id='Rectangle_1812' data-name='Rectangle 1812' width='1.313' height='1.31' transform='translate(20.6 4.455)' />
					<rect id='Rectangle_1813' data-name='Rectangle 1813' width='1.313' height='1.31' transform='translate(23.226 4.455)' />
				</g>
			</g>
		</svg>
	);
}
