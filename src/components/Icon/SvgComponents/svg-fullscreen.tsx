import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgFullscreen(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<path id='Path_2207' data-name='Path 2207' d='M3.857,10.141v10h10' transform='translate(0.143 0.152)' fill='none' stroke='#888' strokeWidth='1' />
			<line id='Line_4' data-name='Line 4' y1='16.265' x2='16.314' transform='translate(4.001 4.013)' fill='none' stroke='#888' strokeWidth='1' />
			<path id='Path_2208' data-name='Path 2208' d='M20.138,13.848v-10h-10' transform='translate(0.143 0.152)' fill='none' stroke='#888' strokeWidth='1' />
			<g id='Rectangle_1817' data-name='Rectangle 1817' fill='#fff' stroke='#707070' strokeWidth='1' opacity='0'>
				<rect width='24' height='24' stroke='none' />
				<rect x='0.5' y='0.5' width='23' height='23' fill='none' />
			</g>
		</svg>
	);
}
