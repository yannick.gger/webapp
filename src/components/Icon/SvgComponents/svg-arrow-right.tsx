import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgArrowRight(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<g id='icon-arrow-right'>
				<path
					id='Path_4994'
					data-name='Path 4994'
					d='M105.059,362.39l-5.61-5.61-.707.707,4.9,4.9H91.474v1h12.168l-4.9,4.9.707.707,5.61-5.61a.718.718,0,0,0,0-.99'
					transform='translate(-86.474 -350.78)'
				/>
			</g>
		</svg>
	);
}
