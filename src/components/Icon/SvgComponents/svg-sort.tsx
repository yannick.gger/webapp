import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgSort(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<g id='icon-sort' transform='translate(1.5 1.5)'>
				<path id='Path_5001' data-name='Path 5001' d='M313.5,326.506h-5a.5.5,0,0,0,0,1h5a.5.5,0,1,0,0-1' transform='translate(-308.003 -310.262)' />
				<path id='Path_5002' data-name='Path 5002' d='M318.5,321.092h-10a.5.5,0,0,0,0,1h10a.5.5,0,1,0,0-1' transform='translate(-308.003 -310.262)' />
				<path id='Path_5003' data-name='Path 5003' d='M323.5,315.678h-15a.5.5,0,0,0,0,1h15a.5.5,0,1,0,0-1' transform='translate(-308.003 -310.262)' />
				<path id='Path_5004' data-name='Path 5004' d='M328.025,310.262H308.5a.5.5,0,0,0,0,1h19.521a.5.5,0,0,0,0-1' transform='translate(-308.003 -310.262)' />
			</g>
		</svg>
	);
}
