import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 20.756 14.766',
	pathClassName: 'st0'
};

export function SvgMail(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg width='24' height='24' viewBox='0 0 20.756 14.766' xmlns='http://www.w3.org/2000/svg' {...svgProps}>
			<path
				id='Path_4964'
				data-name='Path 4964'
				d='M239.958,179.5H220.6a.7.7,0,0,0-.7.7v13.366a.7.7,0,0,0,.7.7h19.356a.7.7,0,0,0,.7-.7V180.2a.7.7,0,0,0-.7-.7m-.3,1v.748l-9,5.259a.75.75,0,0,1-.763,0l-9-5.259V180.5ZM220.9,193.268v-1.552l4.3-5.015a.5.5,0,1,0-.76-.651l-3.54,4.129v-7.773l8.493,4.965a1.757,1.757,0,0,0,1.773,0l8.5-4.965v7.773l-3.54-4.129a.5.5,0,1,0-.76.651l4.3,5.015v1.552Z'
				transform='translate(-219.902 -179.5)'
				fill='#333'
			/>
		</svg>
	);
}
