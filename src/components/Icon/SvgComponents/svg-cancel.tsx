import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgCancel(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<g id='Rectangle_1815' data-name='Rectangle 1815' strokeWidth='1' opacity='0'>
				<rect width='24' height='24' stroke='none' />
				<rect x='0.5' y='0.5' width='23' height='23' />
			</g>
			<path
				id='Path_2206'
				data-name='Path 2206'
				d='M22.354,2.354l-.707-.707L12,11.293,2.354,1.649l-.707.707L11.292,12,1.646,21.644l.707.707L12,12.707l9.648,9.647.707-.707L12.706,12Z'
				transform='translate(0.354 0.354)'
			/>
		</svg>
	);
}
