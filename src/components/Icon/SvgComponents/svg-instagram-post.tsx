import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgInstagramPost(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<path
				id='Path_4940'
				data-name='Path 4940'
				d='M107.8,264.662H88.718a.7.7,0,0,0-.7.7V284.44a.7.7,0,0,0,.7.7H107.8a.7.7,0,0,0,.7-.7V265.359a.7.7,0,0,0-.7-.7m-.3,19.477H89.02V265.662H107.5Z'
				transform='translate(-88.018 -264.659)'
			/>
			<path id='Path_4941' data-name='Path 4941' d='M97.759,278.9h1v-3.5h3.5v-1h-3.5v-3.5h-1v3.5h-3.5v1h3.5Z' transform='translate(-88.018 -264.659)' />
		</svg>
	);
}
