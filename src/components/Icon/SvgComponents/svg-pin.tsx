import { ISvgProps } from '../types';

const defaultProps: ISvgProps = {
	viewBox: '0 0 24 24',
	pathClassName: 'st0'
};

export function SvgPin(props: ISvgProps) {
	props = { ...defaultProps, ...props };
	const { pathClassName, ...svgProps } = props;

	return (
		<svg {...svgProps}>
			<defs>
				<clipPath id='clipPath'>
					<rect id='Rectangle_1805' data-name='Rectangle 1805' width='24' height='24' fill='none' />
				</clipPath>
			</defs>
			<g id='Group_2110' data-name='Group 2110' transform='translate(-1086 -1227)'>
				<g id='Group_1772' data-name='Group 1772' transform='translate(1078 1219)'>
					<g id='Group_2102' data-name='Group 2102' transform='translate(8 8)'>
						<g id='Group_2101' data-name='Group 2101' clipPath='url(#clipPath)'>
							<path
								id='Path_2200'
								data-name='Path 2200'
								d='M22.122,6.714l-4.638-4.7a.832.832,0,0,0-.591-.252h-.008a.831.831,0,0,0-.58.233L12.861,7.008h-2.39A5.232,5.232,0,0,0,6.852,8.471a.851.851,0,0,0,0,1.2l3.385,3.423-6.7,6.7a.5.5,0,0,0,.707.707l6.7-6.7,3.495,3.535a.821.821,0,0,0,.593.25.877.877,0,0,0,.611-.254,5.16,5.16,0,0,0,1.439-3.653v-2.42l4.838-3.371a.849.849,0,0,0,.207-1.174m-5.683,3.771a.844.844,0,0,0-.362.694v2.507a4.165,4.165,0,0,1-1.047,2.83L7.67,9.074a4.314,4.314,0,0,1,2.808-1.066h2.467a.84.84,0,0,0,.694-.366l3.267-4.8,4.285,4.327Z'
								fill='#333'
							/>
						</g>
					</g>
				</g>
			</g>
		</svg>
	);
}
