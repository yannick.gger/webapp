import React, { Component } from 'react';
import { Col } from 'antd';
import UserEmailForm from './user-email-form';
import UserPasswordForm from './user-password-form';
import SocialMediaConnect from './social-media-connect';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { FEATURE_FLAG_FACEBOOK_AUTH } from 'constants/feature-flag-keys';

import './styles.scss';
import { isAdmin, isInfluencer } from 'services/auth-service';

const UserSettingsForm = (props: any) => {
	const { fields } = props;
	const [isEnabled] = useFeatureToggle();

	return (
		<React.Fragment>
			<UserEmailForm fields={fields} />
			<Col xs={24} md={22} xl={20} xxl={18}>
				<hr />
			</Col>
			<UserPasswordForm />
			{(isAdmin() || isInfluencer()) && isEnabled(FEATURE_FLAG_FACEBOOK_AUTH) ? (
				<>
					<Col xs={24} md={22} xl={20} xxl={18}>
						<hr />
					</Col>
					<SocialMediaConnect facebookConnected={fields.organization.connectedWithFacebook} />
				</>
			) : null}
		</React.Fragment>
	);
};

export default UserSettingsForm;
