import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { Row, Col, Card, Form, Button, Input, Icon, message } from 'antd';
import withCollabsForm from 'services/collabs-form-decorator';
import sendUpdateEmailConfirmation from 'graphql/send-update-email-confirmation.graphql';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';

class UserEmailForm extends Component {
	handleSubmit = (sendUpdateEmailConfirmation) => (e) => {
		e.preventDefault();
		const { form } = this.props;

		form.validateFieldsAndScroll(async (err) => {
			if (err) return null;
			const graphQlValues = {
				email: (form.getFieldsValue().organization || {}).email
			};
			try {
				const res = await sendUpdateEmailConfirmation({
					variables: graphQlValues
				});
				if (res.data.sendUpdateEmailConfirmation) {
					message.success('A confirmation email has been sent to your new email. Please check your mailbox');
				} else {
					message.error('Could not send the confirmation email at this time');
				}
			} catch ({ graphQLErrors }) {
				graphQLErrors.forEach((error) => {
					message.error(error.message);
				});
			}
		});
	};

	render() {
		const { form, fields, bordered } = this.props;
		const { getFieldDecorator } = form;

		return (
			<Row>
				<Col xs={24} md={24} lg={24} xl={22} xxl={20}>
					<Card className={bordered ? '' : 'ant-card-naked overflow-fix'}>
						<Mutation mutation={sendUpdateEmailConfirmation}>
							{(mutation, { loading }) => (
								<Row gutter={15}>
									<Col xs={24} md={19}>
										<Form layout='vertical'>
											<Form.Item label='Change email'>
												{getFieldDecorator('organization.email', {
													initialValue: fields.organization.email,
													rules: [
														{
															required: true,
															message: 'Please enter an email address. '
														},
														{
															validator: (rule, value, callback) => {
																if (
																	/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
																		value
																	)
																)
																	callback();
																else callback('Valid format is example@email.com');
															},
															message: 'Valid format is example@email.com'
														}
													]
												})(
													<Input
														size='large'
														prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />}
														placeholder='example@email.com'
														disabled={checkGhostUserIsSaler()}
													/>
												)}
											</Form.Item>
										</Form>
									</Col>
									<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
										<Col xs={24} md={5} className='btn-save'>
											<Button
												size='large'
												key='submit'
												type='primary'
												disabled={form.getFieldError('organization.email')}
												loading={loading}
												onClick={this.handleSubmit(mutation)}
											>
												Save
											</Button>
										</Col>
									</GhostUserAccess>
								</Row>
							)}
						</Mutation>
					</Card>
				</Col>
			</Row>
		);
	}
}

export default withCollabsForm(UserEmailForm);
