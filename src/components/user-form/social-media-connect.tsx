import { Col, Form, Row } from 'antd';
import React from 'react';
import InstagramAuth from '../instagram-auth';
import i18next from 'i18next';
import { InstagramIcon } from '../icons';
import { ISocialMediaConnect } from './types';

const SocialMediaConnect = (props: ISocialMediaConnect) => {
	return (
		<Row gutter={15}>
			<Col xs={24} md={12}>
				<Form className='social-media-connect' layout='vertical'>
					<Form.Item label={i18next.t('connectToSocialMedia', { defaultValue: 'Connect social media' })}>
						<p className='text-small'>By connecting to social media it will make it easier for you when you are working with the campaigns.</p>
						<div className='social-media-connect__switch'>
							<div className='social-media-connect__switch__inner'>
								<label className='social-media-connect__switch__label'>
									<i>
										<InstagramIcon />
									</i>
									Instagram
								</label>
								<InstagramAuth type='switch' isConnected={props.facebookConnected} />
							</div>
						</div>
					</Form.Item>
				</Form>
			</Col>
		</Row>
	);
};

export default SocialMediaConnect;
