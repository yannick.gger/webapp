import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { Row, Col, Card, Form, Button, Input, Icon, message } from 'antd';
import withCollabsForm from 'services/collabs-form-decorator';
import updateUserPassword from 'graphql/update-user-password.graphql';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';

class UserPasswordForm extends Component {
	validatePasswordReq = (_, value, callback) => {
		if (value && value.length < 8) callback('Your password must be at least 8 characters long.');
		callback();
	};

	compareToFirstPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && value !== form.getFieldValue('newPassword')) callback('Two passwords that you enter is inconsistent!');
		else callback();
	};

	validateToNextPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && form.getFieldValue('confirm')) form.validateFields(['confirm'], { force: true });
		callback();
	};

	handleSubmit = (updateUserPassword) => (e) => {
		e.preventDefault();
		const form = this.props.form;

		form.validateFieldsAndScroll(async (err) => {
			if (err) return null;
			const graphQlValues = form.getFieldsValue();

			try {
				const res = await updateUserPassword({
					variables: graphQlValues
				});
				if (res.data.updatePassword.successful) message.success('Your password has been updated');
				else
					res.data.updatePassword.errors.forEach((error) => {
						message.error(error.message);
					});
			} catch ({ graphQLErrors }) {
				if (graphQLErrors)
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
			}
		});
	};

	render() {
		const { form, bordered } = this.props;
		const { getFieldDecorator } = form;

		const errors = form.getFieldsError();
		const numErrors = Object.keys(errors).filter((key) => errors[key]).length;

		return (
			<Row>
				<Col xs={24} md={24} lg={24} xl={22} xxl={20}>
					<Card className={bordered ? '' : 'ant-card-naked overflow-fix'}>
						<Mutation mutation={updateUserPassword}>
							{(mutation, { loading }) => (
								<Row gutter={15}>
									<Col xs={24} md={19}>
										<Form layout='vertical'>
											<Form.Item label='Current password'>
												{getFieldDecorator('oldPassword', {
													initialValue: '',
													rules: [
														{
															required: true,
															message: 'Please input your password!'
														},
														{
															validator: (_, value) => value !== null
														}
													]
												})(<Input.Password size='large' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Old password' />)}
											</Form.Item>
											<Row gutter={15}>
												<Col xs={24} md={12}>
													<Form.Item label='New password'>
														{getFieldDecorator('newPassword', {
															rules: [
																{
																	required: true,
																	message: 'Please input your password!'
																},
																{
																	validator: this.validatePasswordReq
																},
																{
																	validator: this.validateToNextPassword
																}
															]
														})(<Input.Password size='large' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='New password' />)}
													</Form.Item>
												</Col>
												<Col xs={24} md={12}>
													<Form.Item label='Confirm new password'>
														{getFieldDecorator('confirm', {
															rules: [
																{
																	required: true,
																	message: 'Please confirm your password!'
																},
																{
																	validator: this.compareToFirstPassword
																}
															]
														})(
															<Input.Password
																size='large'
																prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
																onBlur={this.handleConfirmBlur}
																placeholder='Confirm new password'
															/>
														)}
													</Form.Item>
												</Col>
											</Row>
										</Form>
									</Col>
									<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
										<Col
											xs={24}
											md={5}
											className='btn-save-password'
											style={{
												height: form.getFieldError('oldPassword') ? 214 : 192,
												transition: form.getFieldError('oldPassword') ? 'none' : '0s height 0.35s'
											}}
										>
											<Button size='large' key='submit' type='primary' disabled={numErrors} loading={loading} onClick={this.handleSubmit(mutation)}>
												Save
											</Button>
										</Col>
									</GhostUserAccess>
								</Row>
							)}
						</Mutation>
					</Card>
				</Col>
			</Row>
		);
	}
}

export default withCollabsForm(UserPasswordForm);
