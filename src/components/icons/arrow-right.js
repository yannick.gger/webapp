import React from 'react';

export const ArrowRightIcon = ({ fill = '#026BFA', ...props }) => (
	<svg width='14px' height='10px' viewBox='0 0 14 10' version='1.1' className='arrow-right-icon-components' {...props}>
		<g id='Influencers' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
			<g id='Influencer-dashboard' transform='translate(-1479.000000, -186.000000)' fill={fill}>
				<g id='Group-4-Copy-2' transform='translate(1080.000000, 165.000000)'>
					<g id='CTA' transform='translate(330.000000, 0.000000)'>
						<g id='Group-3' transform='translate(69.000000, 21.000000)'>
							<polygon fill={fill} id='Fill-1' points='0 4.11764706 11.5294118 4.11764706 11.5294118 5.76470588 0 5.76470588' />
							<polygon
								fill={fill}
								id='Fill-2'
								points='8.55097645 9.88235294 7.41176471 8.84945964 11.7231187 4.94117647 7.41176471 1.0328933 8.55097645 0 14 4.94119417'
							/>
						</g>
					</g>
				</g>
			</g>
		</g>
	</svg>
);
