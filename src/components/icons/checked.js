import React from 'react';

export const CheckedIcon = ({ fill = '#FFFFFF', ...props }) => (
	<svg width='10px' height='8px' viewBox='0 0 10 8' className='checked-icon-components' {...props}>
		<polygon
			fill={fill}
			points='10 1.34582088 8.63511089 0 3.33866236 5.2909871 1.3820494 3.33563027 0 4.6852791 2.04111645 6.74456269 3.33876618 8.02463356 4.61951182 6.74456269'
		/>
	</svg>
);
