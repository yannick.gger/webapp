import React from 'react';

export const NotificationIcon = (props) => (
	<svg width='17px' height='18px' viewBox='0 0 17 18' version='1.1' className='notification-icon-components' {...props}>
		<g id='Page-1' stroke='none' strokeWidth='1' fillRule='evenodd'>
			<g id='dont-forget-the-app' transform='translate(-397.000000, -367.000000)' fill='#026BFA'>
				<g id='Group-2' transform='translate(396.000000, 192.000000)'>
					<path
						d='M17.204504,188.123613 C13.5234808,184.444189 17.5425474,175.201461 9.33581236,175 C1.12975807,175.201419 5.14734442,184.444189 1.46792014,188.123613 C-0.223854959,189.815389 2.92472033,190.684221 6.85794694,190.931048 C7.05858649,192.104447 8.07271656,193 9.3030353,193 C10.5318551,193 11.5468645,192.106865 11.7489231,190.935844 C15.7125484,190.693834 18.9049137,189.824902 17.204504,188.123813 L17.204504,188.123613 Z'
						id='Notificatoins'
					/>
				</g>
			</g>
		</g>
	</svg>
);
