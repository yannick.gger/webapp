import React from 'react';

export const ArrowOutlineIcon = ({ ...props }) => (
	<svg width='10px' height='5px' viewBox='0 0 10 5' {...props} className='arrow-outline-icon'>
		<polygon points='4.7339428 5 5.13530326 4.65666644 9.46797844 0.942944857 8.66527299 0 4.73398922 3.37035707 0.802705446 0 2.66453526e-14 0.942944857 4.33267518 4.65666644' />
	</svg>
);
