import React from 'react';

export const ClosedIcon = ({ fill = '#FFFFFF', ...props }) => (
	<svg width='13px' height='13px' viewBox='0 0 13 13' className='checked-icon-components' {...props}>
		<g id='webapp-fixes' transform='translate(-649.000000, -1757.000000)' fill='#F1365E'>
			<g id='Group-3' transform='translate(625.000000, 1728.000000)'>
				<polygon
					fill={fill}
					points='34.8333333 29 37 31.1666667 32.6666667 35.5 37 39.8333333 34.8333333 42 30.5 37.6666667 26.1666667 42 24 39.8333333 28.3333333 35.5 24 31.1666667 26.1666667 29 30.5 33.3333333'
				/>
			</g>
		</g>
	</svg>
);
