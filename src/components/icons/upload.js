import React from 'react';

export const UploadIcon = ({ fill = '#026BFA', ...props }) => (
	<svg width='20px' height='23px' viewBox='0 0 20 23' version='1.1' className='upload-icon-components' {...props}>
		<g id='Influencers' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
			<g id='Influencer-dashboard' transform='translate(-1000.000000, -187.000000)' fill={fill}>
				<g id='Group-4-Copy' transform='translate(627.000000, 165.000000)'>
					<g id='CTA' transform='translate(330.000000, 0.000000)'>
						<g id='Group-5' transform='translate(27.000000, 22.000000)'>
							<g id='upload-icon-2' transform='translate(16.000000, 0.000000)'>
								<g id='upload-icon'>
									<g id='Group-3'>
										<polygon
											id='Fill-1'
											fill={fill}
											points='2.5 10.1871011 4.29876752 11.7593349 8.74478549 7.90349437 8.74478549 23 11.2898271 23 11.2898271 7.90349437 15.7012325 11.7593349 17.5 10.1871011 9.99993213 3.63157895'
										/>
										<polygon fill={fill} id='Fill-2' points='0 0 20 0 20 2.42105263 0 2.42105263' />
									</g>
								</g>
							</g>
						</g>
					</g>
				</g>
			</g>
		</g>
	</svg>
);
