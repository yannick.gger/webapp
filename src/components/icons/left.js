import React from 'react';

export const LeftIcon = ({ fill = '#383B3F', fillRule = 'evenodd', ...props }) => (
	<svg width='10px' height='16px' viewBox='0 0 10 16' version='1.1' className='left-icon-components' {...props}>
		<g id='Influencers' stroke='none' strokeWidth='1' fill='none' fillRule={fillRule}>
			<g id='Influencer-dashboard' transform='translate(-1474.000000, -116.000000)' fill={fill}>
				<polygon
					transform='translate(1479.000000, 124.000000) scale(-1, 1) translate(-1479.000000, -124.000000) '
					points='1474.79662 131.25312 1474.01195 130.507488 1480.84957 124.010049 1474 117.501248 1475.58116 116 1484 124 1475.58116 132 1474.79649 131.254368'
				/>
			</g>
		</g>
	</svg>
);
