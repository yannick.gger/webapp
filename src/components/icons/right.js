import React from 'react';

export const RightIcon = ({ fill = '#383B3F', fillRule = 'evenodd', ...props }) => (
	<svg width='10px' height='16px' viewBox='0 0 10 16' version='1.1' {...props}>
		<g id='Influencers' stroke='none' strokeWidth='1' fill='none' fillRule={fillRule}>
			<g id='Influencer-dashboard' transform='translate(-1502.000000, -116.000000)' fill={fill}>
				<polygon points='1502.79662 131.25312 1502.01195 130.507488 1508.84957 124.010049 1502 117.501248 1503.58116 116 1512 124 1503.58116 132 1502.79649 131.254368' />
			</g>
		</g>
	</svg>
);
