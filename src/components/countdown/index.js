import React, { Component } from 'react';
import { Tooltip } from 'antd';
import CountDown from 'ant-design-pro/lib/CountDown';
import moment from 'moment';
import { translate } from 'react-i18next';

class CollabsCountDown extends Component {
	render() {
		// variant can be: h2, span
		const { expiryDate, variant = 'span', t, campaignFilled } = this.props;

		const StyleTag = ({ children, ...props }) => React.createElement(`${variant}`, props, children);

		const formatTimeLeft = (time) => {
			const duration = moment.duration(time, 'milliseconds');

			if (duration.days() >= 1 || duration.weeks() >= 1 || duration.months() >= 1 || duration.years() >= 1) {
				return `${duration.days()}d ${duration.hours()}h`;
			} else if (duration.hours() >= 1) {
				return `${duration.hours()}h ${duration.minutes()}m`;
			} else {
				return `${duration.minutes()}m ${duration.minutes()}s`;
			}
		};

		const tooltipTitle = expiryDate._isValid
			? campaignFilled
				? t('landingPage:closeDateTooltipFilled', {
						defaultValue: 'Campaign is full'
				  })
				: expiryDate.format('YYYY-MM-DD HH:mm')
			: t('landingPage:closeDateTooltip', { defaultValue: 'A close date will be set at 23:59 7 days after invite is sent' });

		return (
			<Tooltip title={tooltipTitle}>
				{expiryDate < moment() || campaignFilled ? (
					<StyleTag className='brand-color' style={{ color: 'rgba(0, 0, 0, 0.85)' }}>
						{t('landingPage:closed', { defaultValue: `Stängd` })}
					</StyleTag>
				) : expiryDate._isValid ? (
					<StyleTag>
						<CountDown className='brand-color' format={(time) => formatTimeLeft(time)} style={{ color: 'rgba(0, 0, 0, 0.85)' }} target={expiryDate} />
					</StyleTag>
				) : (
					<StyleTag className='brand-color'>--</StyleTag>
				)}
			</Tooltip>
		);
	}
}

export default translate('landingPage')(CollabsCountDown);
