import { ComponentMeta, ComponentStory } from '@storybook/react';
import Accordion from './Accordion';
import { IAccordionProps } from './types';

export default {
	title: 'Accordion',
	component: Accordion,
	argTypes: {
		title: {
			control: { type: 'Text' }
		}
	}
} as ComponentMeta<typeof Accordion>;

const Template: ComponentStory<typeof Accordion> = (args: IAccordionProps) => (
	<div style={{ maxWidth: '700px', margin: '0 auto' }}>
		<Accordion title={args.title || 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'} open={true}>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet euismod turpis, a molestie metus. Vestibulum viverra sit amet quam eget
				consectetur. Aenean egestas est sapien, a vulputate risus varius at. Maecenas tempor egestas ipsum vitae dapibus. Sed consectetur arcu quis orci
				tristique vehicula quis quis libero. Morbi lacinia, mi eu interdum gravida, neque elit ultricies nibh, bibendum viverra ex ipsum quis quam. Aliquam non
				metus in eros scelerisque mattis. Cras turpis leo, lacinia sodales placerat et, scelerisque vel dui. Phasellus commodo lorem in est pulvinar, nec
				facilisis metus gravida. Nulla elementum velit ut dolor malesuada, eu euismod odio viverra. Ut interdum at leo in ultrices. Pellentesque habitant morbi
				tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus ac volutpat tortor. Curabitur sapien nisi, vulputate ut tempor finibus,
				suscipit et massa.
			</p>
		</Accordion>
		<Accordion title={args.title || 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'}>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet euismod turpis, a molestie metus. Vestibulum viverra sit amet quam eget
				consectetur. Aenean egestas est sapien, a vulputate risus varius at. Maecenas tempor egestas ipsum vitae dapibus. Sed consectetur arcu quis orci
				tristique vehicula quis quis libero. Morbi lacinia, mi eu interdum gravida, neque elit ultricies nibh, bibendum viverra ex ipsum quis quam. Aliquam non
				metus in eros scelerisque mattis. Cras turpis leo, lacinia sodales placerat et, scelerisque vel dui. Phasellus commodo lorem in est pulvinar, nec
				facilisis metus gravida. Nulla elementum velit ut dolor malesuada, eu euismod odio viverra. Ut interdum at leo in ultrices. Pellentesque habitant morbi
				tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus ac volutpat tortor. Curabitur sapien nisi, vulputate ut tempor finibus,
				suscipit et massa.
			</p>
		</Accordion>
	</div>
);
export const RegularButton = Template.bind({});
