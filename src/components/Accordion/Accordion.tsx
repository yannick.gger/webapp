import Icon from 'components/Icon';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import Styled from './Accordion.style';
import { IAccordionProps } from './types';

const Accordion = (props: IAccordionProps) => {
	const { title, children, open, border, className } = props;
	const [isOpen, setIsOpen] = useState<boolean>(open);

	useEffect(() => {
		setIsOpen(open);
	}, [open]);

	return (
		<Styled.Wrapper className={classNames(className, { collapsed: !isOpen })} aria-expanded={isOpen} border={border}>
			<Styled.Header onClick={() => setIsOpen(!isOpen)} iconPosition={props.toggleIconPosition || 'right'}>
				<span className='accordion__title'>{title}</span>
				<Styled.ToggleIcon>
					<Icon icon='chevron-down' size='16' />
				</Styled.ToggleIcon>
			</Styled.Header>
			<Styled.Body>{children}</Styled.Body>
		</Styled.Wrapper>
	);
};

Accordion.defaultProps = {
	title: 'Accordion title',
	open: false
};

export default Accordion;
