import styled from 'styled-components';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const ToggleIcon = styled.div`
	margin-left: auto;
`;

const Header = styled.div<{ iconPosition: 'left' | 'right' }>`
	display: flex;
	align-items: center;
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 1rem;
	margin-bottom: 0.688rem;
	cursor: pointer;

	span {
		padding-right: 4px;
	}

	div {
		margin-left: ${(p) => (p.iconPosition !== 'left' ? 'auto' : '0')};
	}

	.icon {
		display: flex;
		align-items: center;
		width: auto;
		height: auto;
		transition: all 0.2s ease;
		transform: rotateZ(180deg);
	}
`;

const Body = styled.div`
	overflow: hidden;
	max-height: 1000px;
	transition: max-height 1s ease-in-out;

	p:last-child {
		margin-bottom: 0;
	}
`;

const Wrapper = styled.div<{ border?: boolean }>`
	display: flex;
	flex-direction: column;
	width: 100%;
	border-bottom: ${(p) => p.border && '1px solid #e2e2e2'};
	margin-bottom: ${(p) => p.border && '1.5rem'};
	padding-bottom: ${(p) => p.border && '1.5rem'};

	&[aria-expanded='false'] {
		${Body} {
			max-height: 0;
			transition: max-height 0.5s cubic-bezier(0, 1, 0, 1);
		}

		${Header} {
			.icon {
				transform: rotateZ(0deg);
			}
		}
	}

	&.brief-page {
		border-bottom: 1px solid #dedec9;

		${Header} {
			padding: 0 2rem;
			span {
				font-family: ${typography.BaseFontFamiliy};
				font-size: 1.5rem;
				font-weight: 600;
			}
		}
	}

	&.brief-assignment {
		${Header} {
			flex-direction: row-reverse;
			justify-content: flex-end;
			color: ${colors.button.sencondary.color};

			.icon {
				fill: ${colors.button.sencondary.color};
				transform: rotate(0deg);
			}
		}

		&[aria-expanded='false'] {
			${Header} {
				.icon {
					transform: rotateZ(-90deg);
				}
			}
		}
	}

	&.brief-product {
		${Header} {
			flex-direction: row-reverse;
			justify-content: center;
			color: ${colors.button.sencondary.color};

			.icon {
				fill: ${colors.button.sencondary.color};
				transform: rotate(0deg);
			}
		}

		&[aria-expanded='false'] {
			${Header} {
				.icon {
					transform: rotateZ(-90deg);
				}
			}
		}
	}
`;

const Styled = {
	Wrapper,
	Header,
	Body,
	ToggleIcon
};

export default Styled;
