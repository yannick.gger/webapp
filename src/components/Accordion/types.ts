export interface IAccordionProps {
	title: string;
	open: boolean;
	toggleIconPosition?: 'left' | 'right';
	children: React.ReactNode;
	border?: boolean;
	className?: string;
}
