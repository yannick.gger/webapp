import { IAvatarProps } from './types';
import Styled from './Avatar.style';
import classNames from 'classnames';

/**
 * Avatar Component
 * @todo i18next
 * @param {IAvatarProps} props
 * @returns {JSX.Element}
 */
const Avatar = (props: IAvatarProps) => {
	const { imageUrl, name, cssClass, backgroundColor, size } = props;

	const resolveName = (name: string) => {
		if (!name) return '';

		if (name.length === 2) {
			return name;
		} else if (name.indexOf(' ') >= 0) {
			return name
				.split(' ')
				.map((word: string) => word[0])
				.join('')
				.slice(0, 2);
		} else {
			return name.charAt(0);
		}
	};

	return (
		<>
			{imageUrl && imageUrl !== null ? (
				<Styled.AvatarFigure title={name} className={classNames('avatar', cssClass)} aria-label={`Avatar of: ${name}`} {...props}>
					<img src={imageUrl} alt={name} className='avatar__img' aria-label={`Avatar of: ${name}`} />
				</Styled.AvatarFigure>
			) : (
				<Styled.FallbackAvatar
					className={classNames('avatar', cssClass)}
					backgroundColor={backgroundColor}
					aria-label={`Avatar of: ${name}`}
					title={name}
					{...props}
				>
					<figcaption>
						<span>{resolveName(name)}</span>
					</figcaption>
				</Styled.FallbackAvatar>
			)}
		</>
	);
};

export default Avatar;
