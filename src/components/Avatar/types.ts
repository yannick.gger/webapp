export interface IAvatarProps {
	imageUrl?: string;
	name: string;
	cssClass?: string;
	backgroundColor?: string;
	size?: 'xs' | 'sm' | 'md' | 'lg';
}
