import styled from 'styled-components';
import { IAvatarProps } from './types';

const getSize = (size?: string) => {
	switch (size) {
		case 'sm':
			return '24px';
		case 'md':
			return '40px';
		case 'lg':
			return '64px';
		default:
			return '40px';
	}
};

const getFontSize = (size?: string) => {
	switch (size) {
		case 'sm':
			return '0.75rem';
		case 'md':
			return '1rem';
		case 'lg':
			return '1rem';
		default:
			return '1rem';
	}
};

const getPadding = (size?: string) => {
	switch (size) {
		case 'sm':
			return '0.188rem 0.063rem 0.25rem';
		case 'md':
			return '0.75rem 0.563rem';
		case 'lg':
			return '0.75rem 0.563rem';
		default:
			return '0.75rem 0.563rem';
	}
};

const AvatarFigure = styled.figure<IAvatarProps>`
	width: ${(props) => getSize(props.size)};
	height: ${(props) => getSize(props.size)};
	border-radius: 100px;
	overflow: hidden;
	margin: 0;
`;

const FallbackAvatar = styled.figure<IAvatarProps>`
	display: flex;
	justify-content: center;
	align-items: center;
	position: relative;
	width: ${(props) => getSize(props.size)};
	height: ${(props) => getSize(props.size)};
	margin: 0 0 0 11px;
	padding: ${(props) => getPadding(props.size)};
	border: solid 1px #333;
	background-color: ${(props) => props.backgroundColor || '#e9fcee'};
	border-radius: 50px;
	overflow: hidden;

	> figcaption {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translateX(-50%) translateY(-50%);
		font-size: ${(props) => getFontSize(props.size)};
		font-weight: 700;
		text-align: center;
		color: #333;
		text-transform: uppercase;
	}
`;

const Styled = {
	AvatarFigure,
	FallbackAvatar
};

export default Styled;
