import styled from 'styled-components';

const Wrapper = styled.div`
	width: 100%;
	background-color: transparent;
`;

const TagField = styled.input`
	background: transparent;
	margin: 0;
	padding: 0;
	border: none;
	outline: none;
	appearance: none;
	width: auto;
	min-width: 4.1px;
	height: 0;
	min-height: 30px;
	margin-left: 4px;
`;

const Tag = styled.div`
	display: flex;
	align-items: center;
	padding: 0 0.25rem;
	white-space: nowrap;
`;

const TagFieldContainer = styled.div`
	display: flex;
	flex-wrap: wrap;
	align-items: center;
	width: 100%;
	margin: 0 -0.25rem -0.25rem 0;
`;

const Styled = {
	Wrapper,
	TagFieldContainer,
	TagField,
	Tag
};

export default Styled;
