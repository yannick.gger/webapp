export interface ITagFieldProps {
	tags: Array<string>;
	placeholder?: string;
	prefix?: string;
	heading?: string;
	onChange: (tag: string) => void;
	onRemove: (tag: string) => void;
	name?: string;
}
