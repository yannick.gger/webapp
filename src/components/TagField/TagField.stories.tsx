import { ComponentMeta, ComponentStory } from '@storybook/react';
import TagField from './TagField';
import { ITagFieldProps } from './types';

export default {
	title: 'Tags',
} as ComponentMeta<typeof TagField>;

const Template: ComponentStory<typeof TagField> = (args: ITagFieldProps) => (
	<div>
		<div style={{ maxWidth: 1200, margin: 'auto', padding: '1rem 2rem' }}>
			<TagField heading='Add hashtags' tags={[]} placeholder='#myhashtag' />
		</div>
		<div style={{ maxWidth: 1200, margin: 'auto', padding: '1rem 2rem' }}>
			<TagField heading='Add hashtags' placeholder='#myhashtag' tags={['foo', 'bar', 'me', 'john', 'doe']} />
		</div>
	</div>
);
export const TagFieldComponent = Template.bind({});
