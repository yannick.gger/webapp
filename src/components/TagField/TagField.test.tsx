import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import themes from 'styles/themes';
import TagField from './TagField';

describe('<TtagField /> test', () => {
	it('renders tagField', () => {
		const tagField = render(
			<ThemeProvider theme={themes['default']}>
				<TagField heading='Mocked heading' tags={[]} />{' '}
			</ThemeProvider>
		);
		expect(tagField).toMatchSnapshot();
	});
});
