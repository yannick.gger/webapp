import React, { useEffect, useRef, useState } from 'react';
import { ITagFieldProps } from './types';
import Styled from './TagField.style';
import TogglePanel from 'components/TogglePanel';

const TagField = (props: ITagFieldProps) => {
	const [input, setInput] = useState<string>('');
	const [active, setActive] = useState<boolean>(false);

	const inputRef = useRef<HTMLInputElement>(null);

	const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		const { value } = e.target;
		setInput(value.replace(/\s/g, ''));
	};

	const onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>): void => {
		const { key } = e;
		const trimmedInput = input.trim();
		const keys = [',', 'Enter', ' '];

		e.currentTarget.style.width = input ? (input.length + 2) * 8 + 'px' : 'auto';

		if (input !== '' && keys.includes(key) && trimmedInput.length && !props.tags.includes(trimmedInput)) {
			e.preventDefault();
			props.onChange && props.onChange(trimmedInput);
			setInput('');
		}

		if (input === '' && key === 'Backspace') {
			const latestTag = props.tags.length > 0 ? props.tags.slice(-1).pop() : '';
			if (latestTag !== undefined && latestTag !== '') {
				props.onRemove(latestTag);
			}
		}
	};

	useEffect(() => {
		setActive(props.tags.length > 0);
	}, []);

	useEffect(() => {
		if (inputRef && inputRef.current) {
			const ref = inputRef.current;
			active ? ref.focus() : ref.blur();
		}
	}, [active]);

	return (
		<TogglePanel heading={props.heading} isOpen={active} onClick={() => setActive(!active)} toolTip={true}>
			<Styled.Wrapper>
				<Styled.TagFieldContainer>
					{props.tags.map((tag, index) => (
						<Styled.Tag key={index}>
							<span>{tag}</span>
						</Styled.Tag>
					))}
					<Styled.TagField
						data-testid={`form-control-${props.name}`}
						ref={inputRef}
						value={input}
						onKeyDown={onKeyDown}
						onChange={onChange}
						placeholder={!input && !props.tags.length ? props.placeholder : ''}
					/>
				</Styled.TagFieldContainer>
			</Styled.Wrapper>
		</TogglePanel>
	);
};

export default TagField;
