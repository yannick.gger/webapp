import React, { Component } from 'react';
import { Row, Col, Divider, Card, Tag } from 'antd';
import Moment from 'react-moment';

class ActivityFeedCard extends Component {
	cardTitle() {
		return (
			<div className='ant-card-meta-title'>
				<b className='color-blue'>{this.props.title}</b>
				&nbsp;&nbsp;
				<span style={{ fontWeight: 'bold' }}>{this.props.subTitle}</span>
			</div>
		);
	}
	render() {
		return (
			<div>
				<Row>
					<Col sm={{ span: 24 }}>
						<Card data-ripple='rgba(132,146, 164, 0.2)'>
							{this.cardTitle()}
							<p className='mt-5 mb-4'>{this.props.children}</p>
							<Tag className='tag grey'>{<Moment fromNow>{this.props.time}</Moment>}</Tag>
							{this.props.tagStatus}
						</Card>
						<Divider className='mb-0 mt-0' />
					</Col>
				</Row>
			</div>
		);
	}
}

export default ActivityFeedCard;
