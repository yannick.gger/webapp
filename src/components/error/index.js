import React, { Component } from 'react';

import './styles.scss';

export default class ErrorView extends Component {
	render() {
		return (
			<div className='error-view'>
				<div className='blue-box'>
					<h1>Error</h1>
					<p>We are not sure what happened, but that didn't work</p>
					<a href='/' className='btn'>
						Go back home
					</a>
				</div>
			</div>
		);
	}
}
