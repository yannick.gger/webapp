import React, { Component } from 'react';

class CreateCard extends Component {
	render() {
		return (
			<React.Fragment>
				<svg width='100' height='100' viewBox='-0.2 -0.2 100.5 100.4' preserveAspectRatio='none' className='svg-dashed-radius'>
					<path
						vectorEffect='non-scaling-stroke'
						fill='none'
						d='M 1 0 L 99 0 C 99.5 0 100 0.5 100 1 L 100 99 C 100 99.5 99.5 100 99 100 L 1 100 C 0.5 100 0 99.5 0 99 L 0 1 C 0 0.5 0.5 0 1 0 Z '
					/>
				</svg>
				<i className='ion ion-plus' />
				<span>{this.props.children}</span>
			</React.Fragment>
		);
	}
}

export default CreateCard;
