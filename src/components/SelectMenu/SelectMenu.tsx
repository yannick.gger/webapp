import Icon from 'components/Icon';
import { components, GroupBase, Props } from 'react-select';
import { SelectField } from './SelectMenu.style';

const SelectMenu = <Option, IsMulti extends boolean = false, Group extends GroupBase<Option> = GroupBase<Option>>(props: Props<Option, IsMulti, Group>) => {
	const DropdownIndicator = (props: any) => {
		return (
			components.DropdownIndicator && (
				<components.DropdownIndicator {...props}>
					<Icon icon={props.selectProps.menuIsOpen ? 'chevron-up' : 'chevron-down'} />
				</components.DropdownIndicator>
			)
		);
	};

	return <SelectField {...props} components={{ DropdownIndicator }} />;
};

export default SelectMenu;
