import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import SelectMenu from './SelectMenu';
import theme from 'styles/themes';

describe('<SelectMenu /> test', () => {
	it('renders selectMenu', () => {
		const button = render(
			<ThemeProvider theme={theme['default']}>
				<SelectMenu options={[{ label: 'Fooo', value: 'baar' }]} />
			</ThemeProvider>
		);
		expect(button).toMatchSnapshot();
	});
});
