import styled from 'styled-components';
import Select from 'react-select';
import { scrollbarY } from 'styles/utils/scrollbar';

/**
 * Override react select styles
 */
const SelectMenu = styled(Select)<{ size: 'md' | 'lg' }>`
	font-family: ${({ theme }) => theme.select.fontFamily};
	font-size: ${({ theme }) => theme.select.fontSize};

	.select-field {
		&__placeholder {
			color: ${({ theme }) => theme.select.placeholderColor};
		}

		&__indicator-separator {
			display: none;
		}

		&__dropdown-indicator {
			color: ${({ theme }) => theme.select.indicatorColor};

			.icon {
				line-height: 0;
			}
		}

		&__menu {
			border-radius: 0;
			box-shadow: 0px 5px 10px ${({ theme }) => theme.select.menuBoxshadowColor};
		}

		&__menu-list {
			${scrollbarY};
		}

		&__control {
			cursor: pointer;
			border-radius: 0;
			border: ${({ theme }) => theme.select.controlBorder};
			padding: ${(props) => (props.size === 'md' ? '0.125rem 0.25rem' : '0.5rem 0.5rem')};
			box-shadow: none;
			transition: box-shadow 0.1s ease-in-out;

			&--menu-is-open,
			&:hover {
				border: ${({ theme }) => theme.select.controlBorder};
				box-shadow: 0 0 0 1px ${({ theme }) => theme.select.controlBoxshadowColor};
			}

			&--is-focused {
				box-shadow: none;
			}
		}

		&__option {
			cursor: pointer;
			transition: background-color 0.1s ease-in-out;

			&--is-focused,
			&--is-selected,
			&:active {
				background-color: ${({ theme }) => theme.select.optionBackground};
			}

			&--is-selected {
				color: ${({ theme }) => theme.select.optionSelectedColor};
			}
		}
	}
`;

export const SelectField = (props: any) => <SelectMenu classNamePrefix='select-field' {...props} />;
