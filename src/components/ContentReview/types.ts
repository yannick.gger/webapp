import { Model } from 'json-api-models';

export interface IContentReviewContainer {
	campaignId: string;
	reviewId: string;
}

export type Image = {
	links: { screenshot: string };
};

export enum REVIEW_STATUS {
	REVIEW = 0,
	APPROVED = 1,
	REJECTED = 2
}

export type Review = {
	createdAt: Date;
	endAt: Date;
	reviewAt: string;
	status: REVIEW_STATUS;
	text: string;
	updatedAt: Date;
	waivedDeadline: string;
};

export type InfluencerReviewListItem = {
	id: string;
	username: string;
	avatar: string;
	date: Date;
	status: REVIEW_STATUS;
};

export type InfluencerReviewLists = {
	review: Array<InfluencerReviewListItem>;
	approved: Array<InfluencerReviewListItem>;
	rejected: Array<InfluencerReviewListItem>;
};

export type AssingmentReviewItem = {
	id: string;
	title: string;
	date: Date; // @todo rename to createdAt
	endAt: Date;
	images: Model;
	caption: string;
	links?: any;
	comments: Array<Comment>;
	influencer: Influencer;
	status: REVIEW_STATUS;
	type: string;
};

export type Comment = {
	id: string;
	createdAt: Date;
	text: string;
	user: {
		id: string;
		profilePictureUrl: string;
		name: string;
		instagramUsername: string;
	};
};

export type Influencer = {
	id: string;
	username: string;
	avatar: string;
	followers: string;
};
