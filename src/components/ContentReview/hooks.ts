import AssignmentReviewService from 'services/AssignmentReview/AssignmentReview.service';
import { ICollabsResponse } from 'services/Response.types';
import { ItemAttributes } from 'types/http';
import { Model, Store } from 'json-api-models';
import { useContext } from 'react';
import { ToastContext } from 'contexts';
import { AssingmentReviewItem, Comment, InfluencerReviewListItem, InfluencerReviewLists, Review, REVIEW_STATUS } from './types';

export type GetAssignmentReviews = {
	influencerLists: InfluencerReviewLists;
	assignmentList: Array<AssingmentReviewItem>;
};

export default function useContentReviewsData(campaignId: string) {
	const { addToast } = useContext(ToastContext);

	const mapComments = (comments: Model): Array<Comment> => {
		let commentsObj: Array<any> = [];

		comments &&
			comments.map((comment: Model) => {
				commentsObj = [
					...commentsObj,
					{
						id: comment.id,
						createdAt: comment.createdAt,
						text: comment.text,
						user: {
							id: comment.user.id,
							profilePictureUrl: comment.user.profilePictureUrl,
							name: comment.user.name,
							instagramUsername: comment.user.instagramUsername
						}
					}
				];
			});

		return commentsObj;
	};

	const getAssignmentReviews = async () => {
		return await AssignmentReviewService.getReviews(['assignment', 'assignment.campaign', 'influencer', 'comments', 'comments.user', 'medias']).then(
			(response: any) => {
				const models = new Store();
				models.sync(response);

				const reviews = response.data;

				let influencerLists: InfluencerReviewLists = {
					review: [],
					approved: [],
					rejected: []
				};

				let assignmentList: Array<AssingmentReviewItem> = [];

				if (!reviews) return null;

				reviews.map((review: ItemAttributes<Review>) => {
					const influencerRelationship: any = review.relationships && review.relationships.influencer;
					const influencerObject = models.find(influencerRelationship.data);

					const assigmentRelationship: any = review.relationships && review.relationships.assignment;
					const assignment = models.find(assigmentRelationship.data);

					const commentsRelationship: any = review.relationships && review.relationships.comments;
					const comments = models.find(commentsRelationship.data);

					const mediaRelationship: any = review.relationships && review.relationships.medias;
					const medias = models.find(mediaRelationship.data);

					// Influencers
					if (influencerRelationship && influencerObject && assignment.campaign.id === campaignId) {
						const influencer: InfluencerReviewListItem = {
							id: influencerObject.id,
							username: influencerObject.username,
							avatar: influencerObject.links.profilePictureUrl,
							date: review.attributes.updatedAt,
							status: review.attributes.status
						};

						influencer.status === REVIEW_STATUS.REVIEW && influencerLists.review.push(influencer);
						influencer.status === REVIEW_STATUS.APPROVED && influencerLists.approved.push(influencer);
						influencer.status === REVIEW_STATUS.REJECTED && influencerLists.rejected.push(influencer);
					}

					// Assignment reviews
					if (assignment.campaign.id === campaignId) {
						assignmentList = [
							...assignmentList,
							{
								id: review.id,
								title: assignment.name,
								date: new Date(review.attributes.createdAt),
								endAt: new Date(review.attributes.endAt),
								images: medias,
								caption: review.attributes.text,
								comments: mapComments(comments),
								type: assignment.type,
								influencer: influencerObject && {
									id: influencerObject.id,
									username: influencerObject.username,
									avatar: influencerObject.links.profilePictureUrl,
									followers: influencerObject.followersCount
								},
								status: review.attributes.status
							}
						].sort((a, b) => a.date.getTime() - b.date.getTime());
					}
				});

				return {
					influencerLists: influencerLists,
					assignmentList: assignmentList
				};
			}
		);
	};

	const postComment = async (reviewId: string, text: string) => {
		return await AssignmentReviewService.Comments(reviewId, text).then((response: ICollabsResponse) => {
			return response.data;
		});
	};

	const ApproveReview = async (reviewId: string) => {
		return await AssignmentReviewService.ApproveReview(reviewId)
			.then((response: ICollabsResponse) => {
				if (!response.data) addToast({ id: 'ar-approved', mode: 'error', message: `Something went wrong!` });
				return response.data;
			})
			.catch(() => {
				addToast({ id: 'ar-approved', mode: 'error', message: `Something went wrong!` });
			});
	};

	const RejectReview = async (reviewId: string) => {
		return await AssignmentReviewService.RejectReview(reviewId)
			.then((response: ICollabsResponse) => {
				if (!response.data) addToast({ id: 'ar-approved', mode: 'error', message: `Something went wrong!` });
				return response.data;
			})
			.catch(() => {
				addToast({ id: 'ar-approved', mode: 'error', message: `Something went wrong!` });
			});
	};

	return {
		getAssignmentReviews,
		postComment,
		ApproveReview,
		RejectReview
	};
}
