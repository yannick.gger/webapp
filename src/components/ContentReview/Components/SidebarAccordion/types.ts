import { InfluencerReviewLists, InfluencerReviewListItem } from 'components/ContentReview/types';

export interface ISidebarAccordion {
	influencerLists: InfluencerReviewLists;
	isLoading: boolean;
	onClick: (item: InfluencerReviewListItem) => void;
	selectedId: string;
	reviewId?: string;
}
