import styled from 'styled-components';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const Item = styled.div`
	transition: background-color 0.2s ease-in-out;
	padding: 0.5rem;
	cursor: pointer;

	.avatar {
		margin-right: 16px;
	}

	&:hover,
	&.selected {
		background-color: ${colors.contentReview.sidebarItemActiveBackgroundColor};
	}
`;

const Username = styled.div`
	font-weight: 900;
`;

const Row = styled.div`
	display: flex;
`;

const Date = styled.span`
	display: block;
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 0.75rem;
	font-weight: normal;
	color: ${colors.contentReview.sidebarItemColor};
`;

const AccordionWrapper = styled.div`
	margin-bottom: 32px;
`;

const Styled = {
	Item,
	Row,
	Username,
	Date,
	AccordionWrapper
};

export default Styled;
