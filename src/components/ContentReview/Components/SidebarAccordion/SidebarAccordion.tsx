import Accordion from 'components/Accordion';
import Avatar from 'components/Avatar';
import Styled from './SidebarAccordion.style';
import LoadingSpinner from 'components/LoadingSpinner';
import { mapWeekDay } from 'shared/helpers/dates';
import { ISidebarAccordion } from './types';
import { InfluencerReviewListItem } from 'components/ContentReview/types';

/**
 * SidebarAccordion
 * @todo i18next
 * @param {ISidebarAccordion} props
 * @returns {JSX.Element}
 */
const SidebarAccordion = (props: ISidebarAccordion): JSX.Element => {
	const { influencerLists, isLoading, onClick, selectedId } = props;

	const handleOnClick = (item: InfluencerReviewListItem): void => {
		onClick && onClick(item);
	};

	return (
		<>
			<Styled.AccordionWrapper>
				<Accordion title='Under review' toggleIconPosition='left' open={influencerLists.review.length > 0}>
					{!isLoading &&
						influencerLists.review.map((item: InfluencerReviewListItem, index: number) => {
							return (
								<Styled.Item key={`review_${index}`} onClick={() => handleOnClick(item)} className={selectedId === item.id ? 'selected' : ''}>
									<Styled.Row>
										<Avatar imageUrl={item.avatar} size='md' name={item.username} />
										<Styled.Username>
											<span>{item.username}</span> <Styled.Date>{mapWeekDay(item.date.toString())}</Styled.Date>
										</Styled.Username>
									</Styled.Row>
									<Styled.Row />
								</Styled.Item>
							);
						})}
					{!isLoading && influencerLists.review.length === 0 && <span>Nothing to review yet!</span>}
					{isLoading && <LoadingSpinner size='sm' />}
				</Accordion>
			</Styled.AccordionWrapper>
			<Styled.AccordionWrapper>
				<Accordion title='Approved' toggleIconPosition='left' open={influencerLists.approved.length > 0}>
					{!isLoading &&
						influencerLists.approved.map((item: InfluencerReviewListItem, index: number) => {
							return (
								<Styled.Item key={`Approved_${index}`} onClick={() => handleOnClick(item)} className={selectedId === item.id ? 'selected' : ''}>
									<Styled.Row>
										<Avatar imageUrl={item.avatar} size='md' name={item.username} />
										<Styled.Username>
											<span>{item.username}</span> <Styled.Date>{mapWeekDay(item.date.toString())}</Styled.Date>
										</Styled.Username>
									</Styled.Row>
									<Styled.Row />
								</Styled.Item>
							);
						})}
					{!isLoading && influencerLists.approved.length === 0 && <span>No approved reviews yet!</span>}

					{isLoading && <LoadingSpinner size='sm' />}
				</Accordion>
			</Styled.AccordionWrapper>
			<Styled.AccordionWrapper>
				<Accordion title='Not approved' toggleIconPosition='left' open={influencerLists.rejected.length > 0}>
					{!isLoading &&
						influencerLists.rejected.map((item: InfluencerReviewListItem, index: number) => {
							return (
								<Styled.Item key={`rejected_${index}`} onClick={() => handleOnClick(item)} className={selectedId === item.id ? 'selected' : ''}>
									<Styled.Row>
										<Avatar imageUrl={item.avatar} size='md' name={item.username} />
										<Styled.Username>
											<span>{item.username}</span> <Styled.Date>{mapWeekDay(item.date.toString())}</Styled.Date>
										</Styled.Username>
									</Styled.Row>
									<Styled.Row />
								</Styled.Item>
							);
						})}
					{!isLoading && influencerLists.rejected.length === 0 && <span>No declined reviews yet!</span>}

					{isLoading && <LoadingSpinner size='sm' />}
				</Accordion>
			</Styled.AccordionWrapper>
		</>
	);
};

export default SidebarAccordion;
