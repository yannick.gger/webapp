import styled from 'styled-components';
import { CTALink } from 'styles/utils/CTALink';
import { scrollbarY } from 'styles/utils/scrollbar';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const ReviewItem = styled.div``;

const Name = styled.span`
	font-size: 1.5rem;
	font-weight: 900;
	line-height: 1.41;
	display: block;
	margin-bottom: 8px;
`;

const Date = styled.span`
	display: block;
	font-size: 1rem;
	color: ${colors.contentReview.dateColor};
	margin-bottom: 1rem;
`;

const ImageContainer = styled.div<{ backgroundUrl: string }>`
	width: 100%;
	padding-bottom: 90%;
	background-color: ${colors.contentReview.imageBackgroundColor};
	background-image: url(${(props) => props.backgroundUrl});
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center;
	opacity: 0.75;
	transition: opacity 0.2s ease-in-out;
	cursor: pointer;

	&:hover {
		opacity: 1;
	}
`;

const ModalHeader = styled.div`
	display: flex;

	button {
		background: none;
		border: none;
		padding: 0;
		margin-left: auto;
		cursor: pointer;
	}
`;

const ModalHeaderInfluencer = styled.div`
	display: flex;

	.avatar {
		opacity: 0.75;
		margin-right: 24px;
	}
`;

const Username = styled.div`
	font-weight: 900;

	span {
		display: block;
		color: ${colors.contentReview.usernameColor};
	}
`;

const Followers = styled.span`
	font-family: ${typography.SecondaryFontFamiliy};
	font-weight: normal;
`;

const Textarea = styled.textarea`
	width: 100%;
	min-height: 100px;
	border: 2px solid ${colors.contentReview.textareaBorderColor};
	${scrollbarY};
`;

const ReviewHeading = styled.div`
	display: flex;
	align-items: center;
`;

const Title = styled.h2`
	font-size: 1.5rem;
	margin-bottom: 0;
	margin-right: 1rem;
`;

const ReviewDate = styled.span`
	font-size: 1.25rem;
`;

const Medias = styled.div`
	position: relative;
`;

const ModalFooterInner = styled.div`
	display: flex;
	align-items: center;
	border-top: 1px solid ${colors.contentReview.modalBorderTopColor};
	padding-top: 1.5rem;

	.btn-cancel {
		color: ${colors.contentReview.cancelButtonColor};
	}
`;

const ActionButtonGroup = styled.div`
	display: flex;
	gap: 1rem;
	margin-left: auto;
`;

const AccordionWrapper = styled.div`
	.accordion__title {
		font-family: ${typography.BaseFontFamiliy};
		font-weight: 900;
		color: ${colors.contentReview.accordionTitleColor};
		resize: none;
	}
`;

const CommentList = styled.div`
	flex-direction: column;
	margin-bottom: 1rem;
	padding-bottom: 1rem;
	padding-right: 0.5rem;
	border-bottom: 1px solid ${colors.contentReview.commentListBorderColor};
	max-height: 210px;
	${scrollbarY};
`;

const CommentInner = styled.div`
	display: flex;
	align-items: center;
	gap: 16px;

	.avatar {
		margin: 0;
	}
`;

const CommentText = styled.span`
	padding: 1rem 1rem 0.6875rem;
	border-radius: 2px;
	background-color: ${colors.contentReview.commentBackgroundColor};
`;

const CommentDate = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 0.875rem;
	line-height: 1.93;
	text-align: left;
	color: ${colors.contentReview.commentDateColor};
	text-align: center;
	margin-bottom: 8px;
`;

const Comment = styled.div`
	margin-bottom: 1.5rem;

	&:last-child {
		margin-bottom: 0;
	}

	&.reverse {
		${CommentInner} {
			flex-direction: row-reverse;
		}
	}
`;

const NoCommentsText = styled.span`
	font-size: 0.75rem;
	font-weight: 900;
	display: block;
	margin-bottom: 1.5rem;
`;

const filterContainer = styled.div`
	display: flex;
`;

const Status = styled.span`
	position: absolute;
	top: 20px;
	left: 16px;
	display: block;
	background-color: ${colors.contentReview.statusBackgroundColor};
	color: ${colors.contentReview.statusColor};
	font-weight: 900;
	font-size: 1rem;
	line-height: 1.5;
	padding: 0 0.25rem;
	height: 22px;
	z-index: 3090;
`;

const FilterContainer = styled.div`
	display: flex;
	gap: 32px;
	margin-bottom: 24px;
`;

const FilterButton = styled.button`
	${CTALink};
	font-size: 1.5rem;
	background-color: transparent;
	border: 0;
	cursor: pointer;
`;

const Styled = {
	ReviewItem,
	Name,
	Date,
	ImageContainer,
	ModalHeader,
	ModalHeaderInfluencer,
	Username,
	Followers,
	Textarea,
	ReviewHeading,
	Title,
	ReviewDate,
	Medias,
	ModalFooterInner,
	ActionButtonGroup,
	AccordionWrapper,
	CommentList,
	Comment,
	CommentDate,
	CommentInner,
	CommentText,
	NoCommentsText,
	filterContainer,
	Status,
	FilterContainer,
	FilterButton
};

export default Styled;
