import { Model } from 'json-api-models';

export interface IImageSliderProps {
	images: Model;
}

export interface IArrowProps {
	className?: string;
	style?: React.CSSProperties;
	onClick?: () => void;
}
