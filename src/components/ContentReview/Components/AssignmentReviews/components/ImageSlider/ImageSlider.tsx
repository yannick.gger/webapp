import Slider from 'react-slick';
import Styled from './ImageSlider.style';
import Icon from 'components/Icon';
import { IArrowProps, IImageSliderProps } from './types';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

function SampleNextArrow(props: IArrowProps) {
	const { className, style, onClick } = props;
	return (
		<div className={className} style={{ ...style }} onClick={onClick}>
			<Icon icon='chevron-right' />
		</div>
	);
}

function SamplePrevArrow(props: IArrowProps) {
	const { className, style, onClick } = props;
	return (
		<div className={className} style={{ ...style }} onClick={onClick}>
			<Icon icon='chevron-left' />
		</div>
	);
}

const ImageSlider = (props: IImageSliderProps) => {
	const { images } = props;

	const settings = {
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		infinite: true,
		dots: true,
		nextArrow: <SampleNextArrow />,
		prevArrow: <SamplePrevArrow />,
		customPaging: (i: number) => {
			return <Styled.SliderThumbnail backgroundImageUrl={images[i].links.screenshot} />;
		}
	};

	return (
		<Styled.AssingmentSlider>
			<Slider {...settings}>
				{images.map((image: any, index: number) => {
					return <Styled.SliderImage key={index} backgroundImageUrl={image.links.screenshot} />;
				})}
			</Slider>
		</Styled.AssingmentSlider>
	);
};

export default ImageSlider;
