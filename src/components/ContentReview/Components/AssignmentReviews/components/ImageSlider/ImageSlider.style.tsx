import styled from 'styled-components';
import colors from 'styles/variables/colors';

const AssingmentSlider = styled.div`
	.slick-arrow {
		top: auto;
		bottom: 27px;
		transform: translate(0, 0);

		&.slick-prev {
			left: -7px;
		}

		&.slick-next {
			right: -4px;
		}

		&::before {
			content: none;
		}
	}

	.slick-dots {
		position: static;
		padding-top: 0.5938rem;

		li {
			width: 64px;
			height: 64px;
			opacity: 0.75;
			transition: opacity 0.2s ease-in-out;

			&.slick-active,
			&:hover {
				opacity: 1;
			}
		}
	}
`;

const SliderImage = styled.div<{ backgroundImageUrl: string }>`
	width: 100%;
	min-height: 400px;
	background-color: ${colors.contentReview.imageBackgroundColor};
	background-image: url(${(p) => p.backgroundImageUrl});
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
`;

const SliderThumbnail = styled.div<{ backgroundImageUrl: string }>`
	width: 64px;
	height: 64px;
	background-color: ${colors.contentReview.imageBackgroundColor};
	background-image: url(${(p) => p.backgroundImageUrl});
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	cursor: pointer;
`;

const Styled = {
	SliderImage,
	AssingmentSlider,
	SliderThumbnail
};

export default Styled;
