import Grid from 'styles/grid/grid';
import Styled from './AssignmentReviews.style';
import Modal from 'components/Modal';
import { useEffect, useRef, useState } from 'react';
import Avatar from 'components/Avatar';
import { nFormatter } from 'shared/utils/numbers';
import Accordion from 'components/Accordion';
import Icon from 'components/Icon';
import moment from 'moment';
import { Button, LinkButton } from 'components/Button';
import CollabsAuthService from 'services/Authentication/Collabs-api';
import classNames from 'classnames';
import { IAssingmentReviews } from './types';
import ImageSlider from './components/ImageSlider';
import { Model } from 'json-api-models';
import { AssingmentReviewItem, REVIEW_STATUS } from 'components/ContentReview/types';
import { statusName } from 'components/ContentReview/utils';
import { AssignmentType } from '../../../Assigment/types';

/**
 * AssignmentReviews
 * @todo i18next, split to smaller components.
 * @param {IAssingmentReviews} props
 * @returns {JSX.Element}
 */
const AssignmentReviews = (props: IAssingmentReviews): JSX.Element => {
	const { assignmentReviews, baseUrl } = props;
	const [isOpen, setIsOpen] = useState<boolean>(false);
	const [activeAssignment, setActiveAssignment] = useState<AssingmentReviewItem>();
	const [textareaValue, setTextareaValue] = useState<string>('');
	const loggedInUser = CollabsAuthService.getCollabsUserObject();
	const commentEndRef = useRef<HTMLDivElement>(null);
	const [filteredReviews, setFilteredReviews] = useState<Array<AssingmentReviewItem>>(assignmentReviews);
	const [activeFilter, setActiveFilter] = useState<string>();

	const handleOnClick = (item: AssingmentReviewItem) => {
		setIsOpen(true);
		setActiveAssignment(item);
		props.history.push(`${baseUrl}/${item.id}`);
	};

	const handleModalClose = () => {
		setIsOpen(false);
		setActiveAssignment(undefined);
		setTextareaValue('');
		props.history.push(baseUrl);
	};

	const postComment = () => {
		if (activeAssignment && textareaValue.trim().length !== 0 && !props.isLoading) {
			props.postComment && props.postComment(activeAssignment.id, textareaValue);
			setTextareaValue('');
		}
	};

	const handleOnKeyUp = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
		if (e.key === 'Enter' && !e.shiftKey) {
			e.preventDefault();
			postComment();
			return false;
		}
		return true;
	};

	const handleOnClickApprove = () => {
		if (activeAssignment) {
			props.approveReview && props.approveReview(activeAssignment.id);
		}
	};

	const handleOnClickReject = () => {
		if (activeAssignment) {
			props.rejectReview && props.rejectReview(activeAssignment.id);
		}
	};

	const formatAssignmentDate = (createdAt: Date, endAt: Date) => {
		const dateFormat = 'DD MMMM';
		const startDate = moment(createdAt);
		const endDate = moment(endAt);

		return `${startDate.isValid() ? startDate.format(dateFormat) : ''} - ${endDate.isValid() ? endDate.format(dateFormat) : ''}`;
	};

	const handleFilterClick = (type: AssignmentType) => {
		if (type !== AssignmentType.ALL) {
			setFilteredReviews(assignmentReviews.filter((i) => i.type === type));
		} else {
			setFilteredReviews(assignmentReviews);
		}
		setActiveFilter(type);
	};

	useEffect(() => {
		commentEndRef.current && commentEndRef.current.scrollIntoView({ behavior: 'smooth' });
	}, [activeAssignment]);

	useEffect(() => {
		if (activeAssignment) {
			setActiveAssignment(assignmentReviews.find((x) => x.id === activeAssignment.id));
		}

		if (props.reviewId !== '') {
			setActiveAssignment(assignmentReviews.find((x) => x.id === props.reviewId));
			setIsOpen(true);
		}
	}, [assignmentReviews]);

	return (
		<>
			<Styled.FilterContainer>
				<Styled.FilterButton className={classNames({ selected: activeFilter === AssignmentType.ALL })} onClick={() => handleFilterClick(AssignmentType.ALL)}>
					All
				</Styled.FilterButton>
				<Styled.FilterButton
					className={classNames({ selected: activeFilter === AssignmentType.INSTAGRAM_POST })}
					onClick={() => handleFilterClick(AssignmentType.INSTAGRAM_POST)}
				>
					Post
				</Styled.FilterButton>
				<Styled.FilterButton
					className={classNames({ selected: activeFilter === AssignmentType.INSTAGRAM_STORY })}
					onClick={() => handleFilterClick(AssignmentType.INSTAGRAM_STORY)}
				>
					Story
				</Styled.FilterButton>
				<Styled.FilterButton
					className={classNames({ selected: activeFilter === AssignmentType.INSTAGRAM_REEL })}
					onClick={() => handleFilterClick(AssignmentType.INSTAGRAM_REEL)}
				>
					Reels
				</Styled.FilterButton>
			</Styled.FilterContainer>
			<Grid.Container>
				{filteredReviews.length === 0 && (
					<Grid.Column>
						<h2>You don't have any assignments to review yet!</h2>
					</Grid.Column>
				)}
				{filteredReviews.map((item: AssingmentReviewItem, index: number) => {
					return (
						<Grid.Column xl={4} key={`${item.id}_${index}`} data-type={item.type}>
							<Styled.ReviewItem onClick={() => handleOnClick(item)}>
								<Styled.Name>{item.title}</Styled.Name>
								<Styled.Date>{formatAssignmentDate(item.date, item.endAt)}</Styled.Date>
								<Styled.ImageContainer backgroundUrl={item.images.length > 0 && item.images[0].links.screenshot} />
							</Styled.ReviewItem>
						</Grid.Column>
					);
				})}
			</Grid.Container>
			<Modal open={isOpen} handleClose={() => handleModalClose()}>
				{activeAssignment !== undefined && (
					<>
						<Modal.Header>
							<Styled.ModalHeader>
								<Styled.ModalHeaderInfluencer>
									<Avatar imageUrl={activeAssignment.influencer.avatar} name={activeAssignment.influencer.username} />
									<Styled.Username>
										<span>{activeAssignment.influencer.username}</span>
										<Styled.Followers>{nFormatter(activeAssignment.influencer.followers, 1)} followers</Styled.Followers>
									</Styled.Username>
								</Styled.ModalHeaderInfluencer>
								<button onClick={() => handleModalClose()}>
									<Icon icon='cross' />
								</button>
							</Styled.ModalHeader>
						</Modal.Header>
						<Modal.Body>
							<Grid.Container>
								<Grid.Column xl={12}>
									<Styled.ReviewHeading>
										<Styled.Title>{activeAssignment.title}</Styled.Title>
										<Styled.ReviewDate>{moment(activeAssignment.date).format('DD MMMM')}</Styled.ReviewDate>
									</Styled.ReviewHeading>
								</Grid.Column>
								<Grid.Column xl={6}>
									<Styled.Medias>
										<Styled.Status>{statusName(activeAssignment.status)}</Styled.Status>
										<ImageSlider images={activeAssignment.images} />
									</Styled.Medias>
								</Grid.Column>
								<Grid.Column xl={6}>
									<Styled.AccordionWrapper>
										<Accordion title='Caption' open={true} border={true}>
											<p>{activeAssignment.caption}</p>
										</Accordion>
										<Accordion title='Comments'>
											{activeAssignment.comments && (
												<Styled.CommentList>
													{activeAssignment.comments.map((comment: any, index: number) => {
														return (
															<Styled.Comment key={index} className={classNames({ reverse: loggedInUser.id })}>
																<Styled.CommentDate>{comment.createdAt}</Styled.CommentDate>
																<Styled.CommentInner>
																	<Avatar imageUrl={comment.user.profilePictureUrl} name={comment.user.instagramUsername || comment.user.name} />
																	<Styled.CommentText>{comment.text}</Styled.CommentText>
																</Styled.CommentInner>
															</Styled.Comment>
														);
													})}
													<div ref={commentEndRef} />
												</Styled.CommentList>
											)}
											{!activeAssignment.comments && <Styled.NoCommentsText>No comments yet!</Styled.NoCommentsText>}
											<Styled.Textarea
												placeholder='Write a comment'
												value={textareaValue}
												onKeyUp={handleOnKeyUp}
												onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => setTextareaValue(e.target.value)}
											/>
											<Button size='sm' onClick={postComment} disabled={props.isLoading}>
												Send comment
											</Button>
										</Accordion>
									</Styled.AccordionWrapper>
								</Grid.Column>
							</Grid.Container>
						</Modal.Body>
						<Modal.Footer>
							<Styled.ModalFooterInner>
								<LinkButton className='btn-cancel' onClick={() => handleModalClose()} disabled={props.isLoading}>
									Cancel
								</LinkButton>
								{activeAssignment.status === REVIEW_STATUS.REVIEW && (
									<Styled.ActionButtonGroup>
										<Button onClick={() => handleOnClickReject()} disabled={props.isLoading}>
											Deny
										</Button>
										<Button onClick={() => handleOnClickApprove()} disabled={props.isLoading}>
											Approve
										</Button>
									</Styled.ActionButtonGroup>
								)}
							</Styled.ModalFooterInner>
						</Modal.Footer>
					</>
				)}
			</Modal>
		</>
	);
};

export default AssignmentReviews;
