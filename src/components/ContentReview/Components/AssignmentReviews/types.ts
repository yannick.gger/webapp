export interface IAssingmentReviews {
	assignmentReviews: Array<any>;
	postComment: (reviewId: string, text: string) => void;
	isLoading: boolean;
	approveReview: (reviewId: string) => void;
	rejectReview: (reviewId: string) => void;
	reviewId?: string;
	history: any;
	baseUrl: string;
}
