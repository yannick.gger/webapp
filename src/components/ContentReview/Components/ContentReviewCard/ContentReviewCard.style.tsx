import Card from 'components/Card';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { CTALink } from 'styles/utils/CTALink';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const Heading = styled.h4`
	font-size: 1.5rem;
	margin-bottom: 16px;
`;

const FooterLinks = styled.div`
	display: flex;

	a {
		${CTALink};
		margin-left: auto;
	}
`;

const CardWrapper = styled(Card)`
	padding: 2rem;
	height: 513px;

	.card__inner {
		display: flex;
		flex-direction: column;
	}

	.card__body {
		flex: 1 0 auto;
	}

	.card__footer {
		align-self: flex-end;
	}
`;

const CardContent = styled.div`
	display: flex;
	min-height: 204px;

	h5 {
		display: block;
		width: 100%;
		text-align: center;
	}
`;

const noResultHeading = styled.h3`
	margin-bottom: 0.25rem;
`;

const List = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
	width: 100%;
	margin-bottom: 3rem;
`;

const ListItem = styled.li`
	padding: 0.125rem 0px;
	border-bottom: 1px ${colors.integratedInboxCard.listItemBorderColor} solid;

	a {
		padding: 1rem 0.5rem 0.75rem 0.5rem;
	}
`;

const ListItemTitle = styled.span`
	position: relative;
	display: block;
	font-weight: 900;
	line-height: 1.41;
	color: ${colors.integratedInboxCard.titleColor};
`;

const ListItemLink = styled(Link)`
	position: relative;
	display: flex;
	background-color: transparent;
	transition: background-color 0.2s ease-in-out;

	&:hover {
		background-color: ${colors.integratedInboxCard.listItemBackgroundHover};
	}
`;

const ListItemDate = styled.span`
	display: inline-block;
	font-family: ${typography.SecondaryFontFamiliy};
	color: ${colors.integratedInboxCard.subTitleColor};
	font-size: 0.875rem;
`;

const NotificationCircle = styled.div`
	width: 0.5rem;
	height: 0.5rem;
	border: 1px solid ${colors.integratedInboxCard.newMessageCircleBorderColor};
	position: absolute;
	top: 5px;
	left: -16px;
	background-color: ${colors.integratedInboxCard.newMessageCircleBackground};
	border-radius: 20px;
`;

const TextWrapper = styled.div``;
const AvatarWrapper = styled.div`
	display: flex;
	gap: 8px;
	align-items: center;
	margin-left: auto;

	.icon {
		line-height: 0;
	}
`;

const Styled = {
	Heading,
	FooterLinks,
	CardWrapper,
	CardContent,
	noResultHeading,
	List,
	ListItem,
	ListItemTitle,
	ListItemLink,
	ListItemDate,
	NotificationCircle,
	TextWrapper,
	AvatarWrapper
};

export default Styled;
