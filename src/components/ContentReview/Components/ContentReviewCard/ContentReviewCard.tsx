import Avatar from 'components/Avatar';
import Card from 'components/Card';
import useContentReviewsData from 'components/ContentReview/hooks';
import { AssingmentReviewItem } from 'components/ContentReview/types';
import { statusName } from 'components/ContentReview/utils';
import Icon from 'components/Icon';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getCurrentOrganization } from 'shared/utils';
import Styled from './ContentReviewCard.style';
import { IContentReviewCard } from './types';

/**
 * ContentReviewCard
 * @todo i18next
 * @param {IContentReviewCard} props
 * @returns {JSX.Element}
 */
const ContentReviewCard = (props: IContentReviewCard) => {
	const orgSlug: string = getCurrentOrganization();

	const { getAssignmentReviews } = useContentReviewsData(props.campaignId);
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const [assignmentReviews, setAssignmentReviews] = useState<Array<AssingmentReviewItem>>([]);

	useEffect(() => {
		setIsLoading(true);
		getAssignmentReviews()
			.then((data: any) => {
				const reviews = data.assignmentList.slice(0, 4);
				setAssignmentReviews(reviews);
			})
			.catch(() => {
				console.log('Cannot get influencer reviews');
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, []);

	return (
		<Styled.CardWrapper>
			<Card.Header>
				<Styled.Heading>Content review</Styled.Heading>
			</Card.Header>
			<Card.Body>
				<Styled.CardContent>
					{!isLoading && assignmentReviews.length > 0 && (
						<Styled.List>
							{assignmentReviews.map((review: AssingmentReviewItem) => (
								<Styled.ListItem key={review.id}>
									<Styled.ListItemLink to={`/${orgSlug}/campaigns/${props.campaignId}/review/${review.id}`}>
										<Styled.TextWrapper>
											<Styled.ListItemTitle>
												{review.title} {statusName(review.status).toLocaleLowerCase()}
											</Styled.ListItemTitle>
											<Styled.ListItemDate>{moment(review.date).format('ddd. DD MMMM. HH:MM')}</Styled.ListItemDate>
										</Styled.TextWrapper>
										<Styled.AvatarWrapper>
											<Avatar imageUrl={review.influencer.avatar} name={review.influencer.username} size='sm' />
											<Icon icon='circle-arrow-right' />
										</Styled.AvatarWrapper>
									</Styled.ListItemLink>
								</Styled.ListItem>
							))}
						</Styled.List>
					)}
					{!isLoading && assignmentReviews.length === 0 && (
						<div>
							<Styled.noResultHeading>You have no reviews yet!</Styled.noResultHeading>
						</div>
					)}
					{isLoading && <h5>Loading...</h5>}
				</Styled.CardContent>
			</Card.Body>
			<Card.Footer>
				<Styled.FooterLinks>
					<Link to={`/${orgSlug}/campaigns/${props.campaignId}/review`}>See all</Link>
				</Styled.FooterLinks>
			</Card.Footer>
		</Styled.CardWrapper>
	);
};

export default ContentReviewCard;
