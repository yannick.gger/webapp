import { REVIEW_STATUS } from './types';

export const statusName = (status: REVIEW_STATUS) => {
	switch (status) {
		case REVIEW_STATUS.REVIEW:
			return 'Under review';
		case REVIEW_STATUS.APPROVED:
			return 'Approved';
		case REVIEW_STATUS.REJECTED:
			return 'Declined';
	}
};
