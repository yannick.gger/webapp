import { useContext, useEffect, useState } from 'react';
import Grid from 'styles/grid/grid';
import useContentReviewsData from './hooks';
import SidebarAccordion from './Components/SidebarAccordion/SidebarAccordion';
import AssignmentReviews from './Components/AssignmentReviews';
import LoadingSpinner from 'components/LoadingSpinner';
import { ToastContext } from 'contexts';
import { useHistory } from 'react-router-dom';
import { getCurrentOrganization } from 'shared/utils';
import { InfluencerReviewLists, AssingmentReviewItem, InfluencerReviewListItem, IContentReviewContainer } from './types';
import { getLoggedInUser } from 'components/IntegratedInbox/Helpers';

/**
 * ContentReviewContainer
 * @todo i18next and tests
 * @param {IContentReviewContainer} props
 * @returns {JSX.Element}
 */
const ContentReviewContainer = (props: IContentReviewContainer): JSX.Element => {
	const { getAssignmentReviews, postComment, ApproveReview, RejectReview } = useContentReviewsData(props.campaignId);
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const [selectedInfluencer, setSelectedInfluencer] = useState<string>('');
	const [influencerLists, setInfluencerLists] = useState<InfluencerReviewLists>({
		review: [],
		approved: [],
		rejected: []
	});
	const [assignmentReviewsResult, setAssignmentReviewsResult] = useState<Array<AssingmentReviewItem>>([]);
	const [assignmentReviews, setAssignmentReviews] = useState<Array<AssingmentReviewItem>>(assignmentReviewsResult);
	const [reviewIsLoading, setReviewIsLoading] = useState<boolean>(false);
	const { addToast } = useContext(ToastContext);
	const history = useHistory();
	const orgSlug: string = getCurrentOrganization();
	const loggedinUser = getLoggedInUser();

	const baseUrl = `/${orgSlug}/campaigns/${props.campaignId}/review`;

	const getSidebarReviews = async () => {
		setIsLoading(true);
		await getAssignmentReviews()
			.then((data: any) => {
				setInfluencerLists(data.influencerLists);
				setAssignmentReviewsResult(data.assignmentList);
				setAssignmentReviews(data.assignmentList);
			})
			.catch(() => {
				console.log('Cannot get influencer reviews');
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const handleOnClickSidebarItem = (item: InfluencerReviewListItem) => {
		setSelectedInfluencer(item.id !== selectedInfluencer ? item.id : '');
	};

	const filterAssignmentReviewsByInfluencer = () => {
		setAssignmentReviews(assignmentReviewsResult.filter((x: AssingmentReviewItem) => x.influencer.id === selectedInfluencer));
	};

	const handlePostComment = (reviewId: string, text: string) => {
		setReviewIsLoading(true);
		postComment(reviewId, text)
			.then((data: any) => {
				if (reviewId) {
					setAssignmentReviews((current: any) => {
						return current.map((obj: AssingmentReviewItem) => {
							if (obj.id === reviewId) {
								return {
									...obj,
									comments: [
										...obj.comments,
										{
											id: data.id,
											createdAt: data.attributes.createdAt,
											text: data.attributes.text,
											user: {
												id: loggedinUser.id,
												profilePictureUrl: loggedinUser.profilePictureUrl,
												name: loggedinUser.name,
												instagramUsername: loggedinUser.instagramUsername
											}
										}
									]
								};
							}
							return obj;
						});
					});
				}
			})
			.finally(() => setReviewIsLoading(false));
	};

	const handleApprove = (reviewId: string) => {
		setReviewIsLoading(true);
		ApproveReview(reviewId)
			.then((response) => {
				if (response) addToast({ id: 'ar-approved', mode: 'success', message: `Review Approved!` });
			})
			.finally(() => setReviewIsLoading(false));
	};

	const handleReject = (reviewId: string) => {
		setReviewIsLoading(true);
		RejectReview(reviewId)
			.then((repsonse) => {
				if (repsonse) addToast({ id: 'ar-approved', mode: 'success', message: `Review denied!` });
			})
			.finally(() => setReviewIsLoading(false));
	};

	useEffect(() => {
		getSidebarReviews();
	}, []);

	useEffect(() => {
		if (selectedInfluencer !== '') {
			filterAssignmentReviewsByInfluencer();
		} else {
			setAssignmentReviews(assignmentReviewsResult);
		}
	}, [selectedInfluencer]);

	return (
		<Grid.Container>
			<Grid.Column xl={4}>
				<SidebarAccordion
					reviewId={props.reviewId}
					influencerLists={influencerLists}
					isLoading={isLoading}
					onClick={(item: InfluencerReviewListItem) => handleOnClickSidebarItem(item)}
					selectedId={selectedInfluencer}
				/>
			</Grid.Column>
			<Grid.Column xl={8}>
				{isLoading ? (
					<LoadingSpinner position='center' />
				) : (
					<AssignmentReviews
						baseUrl={baseUrl}
						reviewId={props.reviewId}
						history={history}
						assignmentReviews={assignmentReviews}
						postComment={(reviewId: string, text: string) => handlePostComment(reviewId, text)}
						approveReview={(reviewId: string) => handleApprove(reviewId)}
						rejectReview={(reviewId: string) => handleReject(reviewId)}
						isLoading={reviewIsLoading}
					/>
				)}
			</Grid.Column>
		</Grid.Container>
	);
};

export default ContentReviewContainer;
