import React, { Component } from 'react';
import dotProp from 'dot-prop-immutable';
import { Button, Spin } from 'antd';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import classes from './PaginatedItems.module.scss';

class PaginatedItems extends Component {
	state = {
		initialLoading: false,
		moreLoading: false
	};
	loadMore = (data, fetchMore) => {
		this.setState({ moreLoading: true });
		fetchMore({
			variables: {
				cursor: dotProp.get(data, `${this.props.dataRef}.pageInfo.endCursor`)
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				this.setState({ moreLoading: false });

				const newEdges = dotProp.get(fetchMoreResult, `${this.props.dataRef}.edges`);
				const pageInfo = dotProp.get(fetchMoreResult, `${this.props.dataRef}.pageInfo`);

				return newEdges.length
					? dotProp.set(previousResult, this.props.dataRef, {
							__typename: dotProp.get(previousResult, `${this.props.dataRef}.__typename`),
							edges: [...dotProp.get(previousResult, `${this.props.dataRef}.edges`), ...newEdges],
							pageInfo
					  })
					: previousResult;
			}
		});
	};
	componentDidUpdate = (prevProps) => {
		if (this.props.queryRes && prevProps.queryRes && prevProps.queryRes.loading !== this.props.queryRes.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.queryRes.loading });
		}
	};
	componentDidMount = () => {
		if (this.props.queryRes && this.props.queryRes.loading) {
			this.setState({ initialLoading: true });
		}
	};
	render() {
		const { queryRes, children } = this.props;
		const { error, data, fetchMore } = queryRes;
		const { initialLoading, moreLoading } = this.state;
		const listData = dotProp.get(data, `${this.props.dataRef}`);

		if (initialLoading || !data) {
			return <Spin className='collabspin' />;
		}
		if (error) {
			return <p>{i18next.t('default:components.paginatedItem.errorMessage', { defaultValue: 'Something went wrong' })}</p>;
		}
		if (!initialLoading && !listData) {
			console.error(`No data found at dataRef: ${this.props.dataRef}. Got ${JSON.stringify(data)}`);
		}
		if (!listData || listData.edges.length === 0) {
			return this.props.noResults || 'No result';
		}

		return (
			<>
				{children({ ...(queryRes || {}), loading: initialLoading, data: listData })}
				{!error && !initialLoading && data && listData && listData.pageInfo.hasNextPage && (
					<div className='pt-30 pb-30 text-center' className={classes.buttonWrapper}>
						<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(data, fetchMore)}>
							{i18next.t('default:components.paginatedItem.loadMore', { defaultValue: 'Load more' })}
						</Button>
					</div>
				)}
			</>
		);
	}
}

export default translate('default')(PaginatedItems);
