import React from 'react';
import { Link, withRouter } from 'react-router-dom';

class OrganizationLink extends React.Component {
	render() {
		const organizationSlug = this.props.match.params.organizationSlug || this.props.location.pathname.split('/')[1];

		return (
			<Link replace={this.props.replace} innerRef={this.props.innerRef} to={`/${organizationSlug}${this.props.to}`}>
				{this.props.children}
			</Link>
		);
	}
}

export default withRouter(OrganizationLink);
