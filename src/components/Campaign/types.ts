export interface CampaignAttributes {
	name: string;
	influencerTargetCount: number;
	language: string;
	background: string;
	purpose: string;
	target: string;
	guidelines: string;
	hashtags: string[];
	mentions: string[];
	startDate: string;
	endDate: string;
	inviteClosedAt: string;
	legacy: boolean;
}

export interface InstagramOwnerAttributes {
	createdAt: string;
	declined: boolean;
	joined: boolean;
}
