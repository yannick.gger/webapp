import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'antd';
import classnames from 'classnames';
import './styles.scss';

class TodoCard extends Component {
	buttonRender = (item, half, className) => {
		return (
			<Button onClick={item.props.onClick} className={`action-item ${half} ${className || ''}`} key={item.props.children}>
				<div className={`action-name color-${item.props.color}`}>{item.props.children}</div>
			</Button>
		);
	};

	linkRender = (item, half, className) => {
		const ALink = ({ children, to, ...props }) => (
			<a {...props} href={to}>
				{children}
			</a>
		);
		const Redirect = item.props.target ? ALink : Link;

		return (
			<Redirect target={item.props.target} to={item.props.targetUrl} className={`action-item ${half} ${className || ''}`} key={item.props.children}>
				{item.props.icon && (
					<div className={`action-icon color-${item.props.color}`}>
						{typeof item.props.icon === 'object' ? item.props.icon : <img src={item.props.icon} alt='icon' />}
					</div>
				)}
				<div className={`action-name color-${item.props.color}`}>{item.props.children}</div>
			</Redirect>
		);
	};

	undefinedRender = (item, half, className) => {
		return (
			<div className={`action-item ${half} ${className || ''}`} key={item.props.children}>
				{item.props.icon && (
					<div className={`action-icon color-${item.props.color}`}>
						{typeof item.props.icon === 'object' ? item.props.icon : <img src={item.props.icon} alt='icon' />}
					</div>
				)}
				<div className={`action-name color-${item.props.color}`}>{item.props.children}</div>
			</div>
		);
	};

	metaDescription = (actions, focused) => {
		let half = actions.length > 1 && 'half';
		return (
			<div className={`actions ${focused ? '' : 'out-of-range'}`}>
				{focused
					? actions.map((item) => this[`${item.props.domType}Render`](item, half, item.props.className || ''))
					: actions.map((item) => this.undefinedRender(item, half, item.props.className))}
			</div>
		);
	};

	infoSection = (infos) => {
		return (
			<div>
				{infos.map((item, index) => (
					<div key={index} className='info-item'>
						{item.text}
					</div>
				))}
			</div>
		);
	};

	render() {
		const { actions, children, className, extra, extraColor, focused, icon, info } = this.props;
		const iconResult = typeof icon === 'object' ? icon : <img src={icon} alt='' />;

		return (
			<div className={extra ? 'justify-content-end' : ''} style={{ display: 'flex' }}>
				{extra && <span className={`extra-text color-${extraColor} tag ${extraColor} ${focused ? '' : 'outside-range'}`}>{extra}</span>}
				<Card className={classnames('mini-card has-icon-left', className, focused ? '' : 'outside-range')} cover={iconResult}>
					<Card.Meta
						title={
							<div className='flexbox'>
								<h4>{children}</h4>
							</div>
						}
						description={this.metaDescription(actions, focused)}
					/>
					<div className='info-text'>{this.infoSection(info)}</div>
				</Card>
			</div>
		);
	}
}

export default TodoCard;
