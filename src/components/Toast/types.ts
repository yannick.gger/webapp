import React from 'react';

export interface IToast {
	mode: any;
	onClose: () => void;
	message: string;
}
