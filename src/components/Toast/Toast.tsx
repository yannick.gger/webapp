import { useEffect } from 'react';

import useToastTimer from 'hooks/ToastPortal/useToastTimer';

import Styled from './Toast.style';
import { IToast } from './types';

const Toast = (props: IToast) => {
	const { mode, onClose, message } = props;

	const { finished, pause, resume } = useToastTimer(4000);

	useEffect(() => {
		if (finished) {
			onClose();
		}
	}, [finished]);

	return (
		<Styled.ToastCard className={mode} onMouseOver={pause} onMouseOut={resume}>
			<Styled.Dot className={mode} />
			<Styled.Content>{message}</Styled.Content>
			<Styled.Close onClick={onClose}>&times;</Styled.Close>
		</Styled.ToastCard>
	);
};

export default Toast;
