import styled from 'styled-components';
import colors from 'styles/variables/colors';

const ToastCard = styled.div`
	width: 391px;
	min-height: 61px;
	height: fit-content;

	background-color: ${colors.tundora};
	box-shadow: 0px 5px 10px #00000029;
	border-width: 1px;
	border-style: solid;
	border-radius: 2px;
	opacity: 1;
	margin: 5px 0;
	padding: 10px 24px 10px 16px;

	display: flex;
	align-items: center;
	justify-content: space-between;

	&:hover {
		transform: scale(1.02);
	}

	&.success {
		border-color: ${colors.success};
	}

	&.info {
		border-color: ${colors.info};
	}

	&.error {
		border-color: ${colors.error};
	}

	&.warning {
		border-color: ${colors.warning};
	}
`;

const Dot = styled.div`
	width: 10px;
	height: 10px;
	border-radius: 50%;
	margin-right: 11px;

	&.success {
		background-color: ${colors.success};
	}

	&.info {
		background-color: ${colors.info};
	}

	&.error {
		background-color: ${colors.error};
	}

	&.warning {
		background-color: ${colors.warning};
	}
`;

const Content = styled.div`
	width: 90%;
	word-wrap: break-word;
	font-size: 14px;
	color: ${colors.white};
`;

const Close = styled.div`
	width: fit-content;
	height: fit-content;
	cursor: pointer;
	color: ${colors.white};
`;

const Styled = {
	ToastCard,
	Dot,
	Content,
	Close
};

export default Styled;
