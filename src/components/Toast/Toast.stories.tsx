import { ComponentStory, ComponentMeta } from '@storybook/react';
import Toast from './Toast';
import { IToast } from './types';

export default {
	title: 'Toast',
	component: Toast,
	argTypes: {
		message: { type: 'text' },
		mode: {
			options: ['warning', 'success', 'info', 'error'],
			control: { type: 'select' }
		}
	}
} as ComponentMeta<typeof Toast>;

const Template: ComponentStory<typeof Toast> = (args: IToast) => <Toast {...args} onClose={() => {}} />;

export const ToastStory = Template.bind({});

ToastStory.args = {
	message: 'This is example',
	mode: 'success'
};
