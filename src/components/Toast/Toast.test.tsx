import { render } from '@testing-library/react';

import Toast from './Toast';

describe('Toast', () => {
	it('should render Toast', () => {
		const toast = render(<Toast message='This is test' mode='warning' onClose={() => {}} />);

		expect(toast).toMatchSnapshot();
	});
});
