import React from 'react';
import { Alert } from 'antd';
import { CheckedIcon, ClosedIcon, EyeIcon } from 'components/icons';

import './style.scss';

const content = {
	pending: {
		icon: <EyeIcon className='icon' fill='#026bfa' />,
		title: 'Pending review',
		message: 'Campaign is awaiting review from the Collabs campaign team'
	},
	approved: {
		icon: <CheckedIcon className='icon' fill='#71d08e' />,
		title: 'Campaign approved',
		message: 'Your campaign has been approved. All the influencers has been invited. Keep track to see when all spots are filled!'
	},
	declined: {
		icon: <ClosedIcon className='icon' fill='#e46a81' />,
		title: 'Campaign declined'
	}
};

const declinedMsg = (message, reason) => (
	<React.Fragment>
		<div>
			<p className='title'>Reason:&nbsp;</p>
			<p className='multi-line'>{reason}</p>
		</div>
		<div>
			<p className='title'>Message from Collabs:&nbsp;</p>
			<p className='multi-line'>{message}</p>
		</div>
	</React.Fragment>
);

const handleOnClose = (type, itemId) => {
	if (type === 'approved') {
		localStorage.setItem(itemId, true);
	}
};

const NotificationPanel = ({ message, reason, type = 'pending', itemId }) => (
	<Alert
		showIcon
		icon={content[type].icon}
		closeText={<span className='close'>CLOSE</span>}
		className={`notification-panel-components ${type}`}
		message={<span>{content[type].title}</span>}
		description={type !== 'declined' ? <span>{content[type].message}</span> : declinedMsg(message, reason)}
		onClose={handleOnClose.bind(this, type, itemId)}
	/>
);

export default NotificationPanel;
