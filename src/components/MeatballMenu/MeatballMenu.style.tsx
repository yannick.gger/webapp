import styled from 'styled-components';
import colors from 'styles/variables/colors';

const Button = styled.button<{ bgColor?: string }>`
	flex: 1;
	border: 1px solid transparent;
	background-color: ${(props) => props.bgColor || colors.transparent};
	min-height: 35px;
	cursor: pointer;

	&:hover {
		border: 1px solid ${colors.borderGray};
	}

	&.selected {
		border: 1px solid ${colors.borderGray};
	}

	& > i {
		line-height: 0;
		display: flex;
		justify-content: center;
		align-items: center;
		width: auto;
		height: auto;
	}
`;

const Styled = {
	Button
};

export default Styled;
