import { Dropdown } from 'antd';

import Icon from 'components/Icon';

import Styled from './MeatballMenu.style';
import { IMeatballMenuProps } from './types';

const MeatballMenu = (props: IMeatballMenuProps) => {
	if (props.dropdownMenu) {
		return (
			<Dropdown overlay={props.dropdownMenu}>
				<Styled.Button {...props}>
					<Icon icon='options' />
				</Styled.Button>
			</Dropdown>
		);
	} else {
		return (
			<Styled.Button {...props}>
				<Icon icon='options' />
			</Styled.Button>
		);
	}
};

export default MeatballMenu;
