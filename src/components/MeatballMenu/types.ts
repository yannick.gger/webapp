import { DropDownProps } from 'antd/lib/dropdown';

export interface IMeatballMenuProps extends React.HTMLAttributes<HTMLButtonElement> {
	id?: string;
	disabled?: boolean;
	className?: string;
	onClick?: () => void;
	role?: string;
	ariaLabel?: string;
	dropdownMenu?: React.ReactNode;
	bgColor?: string;
	dropdownProps?: DropDownProps;
}
