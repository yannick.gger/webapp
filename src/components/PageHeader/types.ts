export interface IPageHeaderProps {
	homeUrl?: string;
	homeText?: string;
	headline: string;
	children?: React.ReactNode;
	onClickBack?: (e: React.MouseEvent<HTMLAnchorElement>) => void;
	backgroundColor?: string;
	showBreadcrumb?: boolean;
	showCurrentDate?: boolean;
}
