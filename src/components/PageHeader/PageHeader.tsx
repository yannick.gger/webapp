import { useEffect, useState } from 'react';
import Icon from 'components/Icon';
import moment from 'moment';
import Styled from './PageHeader.style';
import { IPageHeaderProps } from './types';
import { Link } from 'react-router-dom';

/**
 * Page header
 * @todo i18next
 * @param {IPageHeaderProps} props
 * @returns {JSX.Element}
 */
const PageHeader = (props: IPageHeaderProps): JSX.Element => {
	const [currentDate, setCurrentDate] = useState<string>(moment().format('HH:mm dddd, DD MMMM'));
	const { onClickBack, homeUrl, homeText, headline } = props;

	const handleBackClick = (e: React.MouseEvent<HTMLAnchorElement>): void => {
		onClickBack && onClickBack(e);
	};

	useEffect(() => {
		props.showCurrentDate && dateInterval();
	}, []);

	const dateInterval = (): void => {
		setInterval(() => {
			setCurrentDate(moment().format('HH:mm dddd, DD MMMM'));
		}, 1000); // update each second
	};

	return (
		<Styled.Wrapper>
			<Styled.HeaderBottom>
				<Styled.PageHeadingWrapper>
					<Styled.PageHeading>{headline}</Styled.PageHeading>
					{props.showBreadcrumb && (
						<Styled.Breadcrumb>
							<Styled.BreadcrumbItem>
								<Link to={homeUrl || '/'} onClick={handleBackClick}>
									{/* eslint-disable-next-line prettier/prettier */}
									<Icon icon='chevron-left' size='24' svgProps={{ viewBox: '0 -5 24 24' }} /> {homeText ?? 'Back to home'}
								</Link>
							</Styled.BreadcrumbItem>
						</Styled.Breadcrumb>
					)}
				</Styled.PageHeadingWrapper>
				<Styled.ComponentContainer>{props.children}</Styled.ComponentContainer>
			</Styled.HeaderBottom>
			{props.showCurrentDate && (
				<Styled.DateWrapper>
					<Styled.DateText>{currentDate}</Styled.DateText>
				</Styled.DateWrapper>
			)}
		</Styled.Wrapper>
	);
};

PageHeader.defaultProps = {
	showBreadcrumb: true,
	showCurrentDate: false
};

export default PageHeader;
