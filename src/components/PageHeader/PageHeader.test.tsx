import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import PageHeader from './PageHeader';

const mockedProps = {
	headline: 'Test headline'
};

describe('<PageHeader /> test', () => {
	it('renders Page header', () => {
		<MemoryRouter>
			const header = render(
			<PageHeader {...mockedProps} />
			); expect(header).toMatchSnapshot();
		</MemoryRouter>;
	});
});
