import styled from 'styled-components';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import { breakpoints } from 'styles/variables/media-queries';

const gutters = guttersWithRem;
const wrapperPadding = `${gutters.m} ${gutters.xxl};`;

const Wrapper = styled.header`
	min-height: 72px;
	background-color: ${({ theme }) => theme.background};
	padding: ${wrapperPadding};
	transition: background-color 0.2s ease-in-out;

	&.fixed-header {
		position: sticky;
		top: 0;
		left: 0;
		width: 100%;
		box-shadow: 0 4px 12px -4px rgb(0, 0, 0, 0.1);
	}
`;

const HeaderBottom = styled.div`
	@media (min-width: ${breakpoints.md}) {
		display: flex;
	}
`;

const PageHeadingWrapper = styled.div`
	padding-right: ${gutters.m};
`;

const PageHeading = styled.h1`
	margin-bottom: 1rem;

	@media (min-width: ${breakpoints.md}) {
		margin-bottom: 0;
	}
`;

const Breadcrumb = styled.ul`
	font-family: ${typography.SecondaryFontFamiliy};
	display: flex;
	align-items: center;
	margin: 0;
	padding: 0;
	list-style: none;
	margin-left: -0.25rem;
	margin-right: -0.25rem;
	margin-bottom: ${gutters.xs};
`;

const BreadcrumbItem = styled.li`
	display: inline-block;
	padding: 0 0.25rem;
	word-spacing: -5px;

	&:first-child {
		padding-left: 0;
	}

	a {
		display: flex;
		align-items: center;
		font-size: 1.125rem;
		color: ${colors.header.color};

		.icon {
			width: 24px;
			height: 32px;
		}
	}
`;

const ComponentContainer = styled.div`
	display: flex;
	flex-grow: 1;
	align-items: center;
	flex-direction: column;

	@media (min-width: ${breakpoints.md}) {
		justify-content: center;
	}
`;

const DateWrapper = styled.div`
	margin-bottom: ${guttersWithRem.s};
`;

const DateText = styled.span`
	font-family: ${typography.SecondaryFontFamiliy};
`;

const Styled = {
	Wrapper,
	HeaderBottom,
	PageHeadingWrapper,
	PageHeading,
	ComponentContainer,
	Breadcrumb,
	BreadcrumbItem,
	DateWrapper,
	DateText
};

export default Styled;
