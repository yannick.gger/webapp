import React from 'react';
import { Row, Col, Button } from 'antd';
import { ReactSVG } from 'react-svg';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import OrganizationLink from '../organization-link';
import withFlag from 'hocs/with-flag';
import NoDash from './no-invited-instagram-owners.svg';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from '../ghost-user-access';

const NoInvitedInstagramOwners: React.FC<any> = (props) => {
	const { recommendationURL } = props;
	const renderLinkTo: any = (link: string) => () =>
		link ? (
			<OrganizationLink to={link}>
				<Button type='primary' className='btn-uppercase' size='large'>
					{i18next.t('organization:campaign.influencers.button.findInfluencers', { defaultValue: 'Find influencers' })}
				</Button>
			</OrganizationLink>
		) : (
			<div>{i18next.t('organization:campaign.influencers.message.noRecommendation', { defaultValue: 'Recommendation is not enabled' })}</div>
		);
	return (
		<div>
			<Row>
				<Col sm={{ span: 10, offset: 7 }} className='text-center'>
					<div className='empty-icon'>
						<ReactSVG src={NoDash} />
					</div>
					<div className='mb-30'>
						<h2>{i18next.t('organization:campaign.influencers.title', { defaultValue: 'Add your first influencers' })}</h2>
						<p>
							{i18next.t('organization:campaign.influencers.subTitle', {
								defaultValue: 'When you have added influencers to this campaign, they will appear here.'
							})}
						</p>
					</div>
					{withFlag('recommendation')(renderLinkTo('/discover'), renderLinkTo(recommendationURL.pathname), {})}
				</Col>
			</Row>
		</div>
	);
};

export default translate('organization')(NoInvitedInstagramOwners);
