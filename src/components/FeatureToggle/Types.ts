export type EnabledFeatures = {
	name: string;
	enabled: string;
};

export interface IFeatureFlags {
	children: any;
	enabledFeatures: EnabledFeatures[];
}
