import { render, screen } from '@testing-library/react';
import { FeatureToggleContext } from 'contexts/FeatureFlagContext';
import { FeatureToggle } from './FeatureToggle';
const enabledFeatures = [{ name: 'MOCKED_FEATURE', enabled: 'true' }, { name: 'MOCKED_FEATURE_FALSE', enabled: 'false' }];

//@todo test against the react hook later
describe('Feature flags', () => {
	const customRender = (ui: any) => {
		return render(<FeatureToggle enabledFeatures={enabledFeatures}>{ui}</FeatureToggle>);
	};

	it('should render the child component', () => {
		customRender(
			<FeatureToggle enabledFeatures={enabledFeatures}>
				<FeatureToggleContext.Consumer>
					{(value: any) => {
						const isEnabled = enabledFeatures.find((x) => x.name === value.enabledFeatures[0].name && x.enabled === 'true');
						return isEnabled ? <h1>My feature</h1> : null;
					}}
				</FeatureToggleContext.Consumer>
			</FeatureToggle>
		);
		expect(screen.getByText('My feature')).toBeInTheDocument;
	});

	it('should not render the child component', () => {
		customRender(
			<FeatureToggle enabledFeatures={enabledFeatures}>
				<FeatureToggleContext.Consumer>
					{(value: any) => {
						const isEnabled = value.enabledFeatures.find((x: any) => x.name === 'MOCKED_FEATURE_DISABLED' && x.enabled === true);
						return isEnabled ? <h1>My feature</h1> : <span>Not enabled</span>;
					}}
				</FeatureToggleContext.Consumer>
			</FeatureToggle>
		);
		const component = screen.getByText('Not enabled');
		expect(component).toBeInTheDocument;
	});

	it('should not render the disabled feature', () => {
		customRender(
			<FeatureToggle enabledFeatures={enabledFeatures}>
				<FeatureToggleContext.Consumer>
					{(value: any) => {
						const isEnabled = value.enabledFeatures.find((x: any) => x.name === 'MOCKED_FEATURE_FALSE' && x.enabled === true);
						return isEnabled ? <h1>My feature</h1> : <span>Not enabled</span>;
					}}
				</FeatureToggleContext.Consumer>
			</FeatureToggle>
		);
		const component = screen.getByText('Not enabled');
		expect(component).toBeInTheDocument;
	});
});
