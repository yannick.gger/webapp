import Dropdown, { DropdownItem, DropdownMenu } from './Dropdown';
export default Object.assign(Dropdown, {
	Menu: DropdownMenu,
	Item: DropdownItem
});
