import { ReactNode, HTMLAttributes } from 'react';

export interface IDropdown {
	children: ReactNode;
	title?: ReactNode;
	icon?: string;
	tooltip?: string;
	open?: boolean;
	position?: 'right' | 'left';
	className?: string;
	size?: '10' | '16' | '24' | '32' | '40';
	disabled?: boolean;
}

export interface IDropdownItem extends HTMLAttributes<HTMLDivElement> {
	children: ReactNode;
}
