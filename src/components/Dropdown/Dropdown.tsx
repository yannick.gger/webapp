import Styled from './Dropdown.style';
import Icon from 'components/Icon';
import { useEffect, useRef, useState } from 'react';
import { IDropdown, IDropdownItem } from './types';
import classNames from 'classnames';

/**
 * Dropdown
 * @todo Test, move child components, add context
 * @param {IDropdown} props
 * @returns {JSX.Element}
 */
const Dropdown = ({ open, children, tooltip, position, title, icon, className, size, disabled, ...props }: IDropdown) => {
	const [isOpen, setIsOpen] = useState<boolean>(open || false);
	const [displayTooltip, setDisplayTooltip] = useState<boolean>(false);
	const wrapperRef = useRef<HTMLDivElement>(null);
	let tooltipTimeout: ReturnType<typeof setTimeout>;

	const TOOLTIP_TIMEOUT: number = 700;

	const handleClickOutside = (e: any): void => {
		if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
			setIsOpen(false);
		}

		if (e.target instanceof HTMLAnchorElement) {
			setIsOpen(false);
		}
	};

	const handleEscapeKey = (e: any): void => {
		if (wrapperRef.current && e.key === 'Escape') {
			setIsOpen(false);
		}
	};

	const clearTooltipTimeout = (): void => clearTimeout(tooltipTimeout);

	const handleMouseEnter = (): void => {
		setDisplayTooltip(false);
		tooltipTimeout = setTimeout(() => {
			setDisplayTooltip(true);
		}, TOOLTIP_TIMEOUT);
	};

	const handleMouseLeave = (): void => {
		clearTooltipTimeout();
		setDisplayTooltip(false);
	};

	const handleButtonClick = (): void => {
		if (disabled) return;
		clearTooltipTimeout();
		setIsOpen(!isOpen);
	};

	useEffect(() => {
		document.addEventListener('click', handleClickOutside, true);
		document.addEventListener('keyup', handleEscapeKey, true);

		return () => {
			document.removeEventListener('click', handleClickOutside, true);
			document.removeEventListener('keyup', handleEscapeKey, true);
		};
	}, []);

	useEffect(() => {
		if (isOpen) setDisplayTooltip(false);
	}, [isOpen]);

	const renderTooltip = () => {
		return (
			<Styled.tooltip className={displayTooltip ? 'visible' : ''}>
				<Styled.tooltipText>{tooltip}</Styled.tooltipText>
			</Styled.tooltip>
		);
	};

	return (
		<Styled.Wrapper className={className}>
			<Styled.DropdownWrapper
				ref={wrapperRef}
				className={classNames({
					show: isOpen,
					right: position === 'right',
					left: position === 'left'
				})}
			>
				<Styled.Button
					onClick={handleButtonClick}
					onMouseEnter={handleMouseEnter}
					onMouseLeave={handleMouseLeave}
					className={isOpen ? 'show' : ''}
					aria-haspopup={true}
					aria-expanded={isOpen}
					disabled={disabled}
					{...props}
				>
					{title && <span>{title}</span>} {icon && <Icon icon={icon || ''} size={size || '24'} />}
				</Styled.Button>
				{children}
				{tooltip && renderTooltip()}
			</Styled.DropdownWrapper>
		</Styled.Wrapper>
	);
};

export const DropdownMenu = (props: IDropdownItem) => {
	return <Styled.DropdownMenu>{props.children}</Styled.DropdownMenu>;
};

export const DropdownItem = ({ children, ...props }: IDropdownItem) => {
	return <Styled.DropdownItem {...props}>{children}</Styled.DropdownItem>;
};

Dropdown.defaultProps = {
	position: 'right'
};

export default Dropdown;
