import { ComponentMeta, ComponentStory } from '@storybook/react';
import Dropdown from './index';

export default {
	title: 'Dropdown',
	component: Dropdown
} as ComponentMeta<typeof Dropdown>;

const Template: ComponentStory<typeof Dropdown> = (args: any) => (
	<div style={{ maxWidth: '700px', margin: '0 auto', display: 'flex', flexDirection: 'row' }}>
		<div style={{ flex: '1 1 auto' }}>
			<h3>Dropdown with text</h3>
			<Dropdown title='Dropdown menu' open={true} position='right'>
				<Dropdown.Menu>
					<Dropdown.Item>
						<a href='#'>Menu item 1</a>
					</Dropdown.Item>
					<Dropdown.Item>
						<a href='#'>Menu item 2</a>
					</Dropdown.Item>
					<Dropdown.Item>
						<a href='#'>Menu item 3</a>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
		</div>
		<div style={{ flex: '1 1 auto' }}>
			<h3>Dropdown with text and icon</h3>
			<Dropdown title='Dropdown menu' icon='calendar' position='left'>
				<Dropdown.Menu>
					<Dropdown.Item>
						<a href='#'>Menu item 1</a>
					</Dropdown.Item>
					<Dropdown.Item>
						<a href='#'>Menu item 2</a>
					</Dropdown.Item>
					<Dropdown.Item>
						<a href='#'>Menu item 3</a>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
		</div>
		<div style={{ flex: '1 1 auto' }}>
			<h3>Dropdown with icon only</h3>
			<Dropdown icon='options'>
				<Dropdown.Menu>
					<Dropdown.Item>
						<a href='#'>Menu item 1</a>
					</Dropdown.Item>
					<Dropdown.Item>
						<a href='#'>Menu item 2</a>
					</Dropdown.Item>
					<Dropdown.Item>
						<a href='#'>Menu item 3</a>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
		</div>
	</div>
);
export const RegularDropdown = Template.bind({});
