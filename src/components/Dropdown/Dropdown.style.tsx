import styled from 'styled-components';
import colors from 'styles/variables/colors';

const DropdownMenu = styled.div`
	position: absolute;
	transform: translate3d(0px, 28px, 0px);
	top: 0px;
	right: 0px;
	will-change: transform;
	z-index: 1000;
	display: none;
	float: left;
	min-width: 260px;
	padding: 0.5rem 0.75rem;
	margin: 2px 0 0;
	font-size: 1rem;
	text-align: left;
	list-style: none;
	background-color: ${colors.dropdown.menu.background};
	background-clip: padding-box;
	border: 1px solid ${colors.dropdown.menu.borderColor};
	border-radius: 0.125rem;
	box-shadow: 0px 5px 10px ${colors.dropdown.menu.boxShadowColor};
`;

const DropdownItem = styled.div`
	display: flex;
	align-items: center;
	width: 100%;
	padding: 0.25rem;
	text-align: inherit;
	white-space: nowrap;
	background-color: ${colors.dropdown.menuItem.background};
	border: 0;
	border-radius: 0.125rem;
	transition: background-color 0.2s ease-in-out;
	cursor: pointer;

	&:hover {
		background-color: ${colors.dropdown.menuItem.backgroundHover};
	}

	a {
		display: block;
		width: 100%;
		font-size: 0.875rem;
		color: ${colors.dropdown.menuItem.color};
		padding: 0.1875rem 0.125rem 0 0.125rem;
	}
`;

const Wrapper = styled.div`
	position: relative;
`;

const DropdownWrapper = styled.div`
	position: relative;
	display: inline-flex;

	&.show {
		${DropdownMenu} {
			display: block;
		}
	}

	&.left {
		${DropdownMenu} {
			left: 0;
			right: auto;
		}
	}

	&.right {
		${DropdownMenu} {
			right: 0;
			left: auto;
		}
	}
`;

const tooltip = styled.div`
	position: absolute;
	background-color: ${colors.dropdown.tooltip.background};
	box-shadow: 0px 3px 6px ${colors.dropdown.tooltip.boxShadowColor};
	border-radius: 0.125rem;
	padding: 0.25rem;
	opacity: 0;
	transition: opacity 0.2s linear;
	margin-left: auto;
	margin-right: auto;
	margin-top: 4px;
	white-space: nowrap;
	top: 100%;
	right: 0;

	&.visible {
		opacity: 1;
	}
`;

const tooltipText = styled.span`
	color: ${colors.dropdown.tooltip.color};
	font-size: 0.75rem;
`;

const Button = styled.button<{ disabled?: boolean }>`
	display: flex;
	align-items: center;
	padding: 0.5rem;
	background-color: transparent;
	border: none;
	border-radius: 0.125rem;
	cursor: ${(p) => (!p.disabled ? 'pointer' : 'unset')};
	transition: background-color 0.2s ease-in-out;

	&:hover,
	&.show {
		background-color: ${(p) => (!p.disabled ? colors.dropdown.button.backgroundHover : 'transparent')} !important;
	}

	& > i {
		line-height: 0;
		display: flex;
		justify-content: center;
		align-items: center;
		width: auto;
		height: auto;
	}

	span + i {
		margin-left: 4px;
	}
`;

const Styled = {
	Wrapper,
	Button,
	DropdownWrapper,
	DropdownMenu,
	DropdownItem,
	tooltip,
	tooltipText
};

export default Styled;
