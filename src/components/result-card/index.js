import React, { Component } from 'react';
import { Row, Col, Divider, Card, Icon, Avatar } from 'antd';

import ResultModal from './result-modal.js';

import './styles.scss';

class AssignmentCard extends Component {
	render() {
		return (
			<Card
				className='campaign-card'
				cover={
					<div className='blue-cover-bg d-flex align-items-center justify-content-center' style={{ height: '100px' }}>
						<img src='https://storage.googleapis.com/avatars.listagram.se/110/11015060.jpg' alt='' className='blurd' />
					</div>
				}
			>
				<div className='d-flex justify-content-center' style={{ marginTop: '-66px' }}>
					<Avatar src='https://storage.googleapis.com/avatars.listagram.se/110/11015060.jpg' icon='user' className='xlarge' />
				</div>
				<Card.Meta title='@username' className='mt-10 mb-15 text-center' />

				<Row>
					<Col xs={{ span: 12 }}>
						<h4 className='mb-0'>Followers</h4>
						<h3 className='mb-0'>124k</h3>
					</Col>
					<Col xs={{ span: 12 }}>
						<h4 className='mb-0'>Engagement</h4>
						<h3 className='mb-0'>2.9%</h3>
					</Col>
				</Row>
				<Divider className='mt-15 mb-15' />
				<h4 className='color-green'>
					<Icon type='check' /> Assignment 1
				</h4>
				<p>Completed and uploaded screenshot 3 days ago</p>
				<Divider className='mt-15 mb-15' />
				<Row>
					<Col xs={{ span: 24 }}>
						<h4 className='color-blue mb-0'>Completed assignments</h4>
						<p className='mb-0'>2 out of 3 total</p>
					</Col>
				</Row>
				<Divider className='mt-15 mb-15' />
				<ResultModal />
				<a href='/' className='fr'>
					<Icon type='instagram' /> View post on insta
				</a>
			</Card>
		);
	}
}

export default AssignmentCard;
