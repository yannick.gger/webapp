import React from 'react';
import { Modal, Button, Icon, Card, Avatar, Divider, Timeline } from 'antd';

class ResultModal extends React.Component {
	state = { visible: false };

	showModal = () => {
		this.setState({
			visible: true
		});
	};

	handleCancel = () => {
		this.setState({
			visible: false
		});
	};

	render() {
		return (
			<div>
				<a className='fl' onClick={this.showModal}>
					<Icon type='profile' /> View history
				</a>
				<Modal
					title='History for user @username'
					visible={this.state.visible}
					onCancel={this.handleCancel}
					footer={[
						<Button key='submit' type='primary' onClick={this.handleCancel}>
							Close
						</Button>
					]}
				>
					<div className='modal-fullscreen-cover blue-cover-bg d-flex align-items-center justify-content-center' style={{ height: '100px' }}>
						<img src='https://storage.googleapis.com/avatars.listagram.se/110/11015060.jpg' alt='' className='blurd' />
					</div>
					<div className='d-flex justify-content-center' style={{ marginTop: '-40px' }}>
						<Avatar src='https://storage.googleapis.com/avatars.listagram.se/110/11015060.jpg' icon='user' className='xlarge' />
					</div>
					<Card.Meta title='@username' className='mt-10 mb-15 text-center' />
					<p className='text-center'>Has posted 2 out of 3 total assignments</p>
					<Divider />
					<Timeline>
						<Timeline.Item>
							Notified influencer to post assignment 3 <span className='date fr color-blue'>1 day ago</span>
						</Timeline.Item>
						<Timeline.Item color='red'>
							Assignment 3 past deadline <span className='date fr color-red'>3 days ago</span>
						</Timeline.Item>
						<Timeline.Item color='green'>
							Assignment 2 marked as completed <span className='date fr'>2015-09-19</span>
						</Timeline.Item>
						<Timeline.Item color='green'>
							Uploaded stats screenshot for assignment 2 <span className='date fr'>2015-09-18</span>
						</Timeline.Item>
						<Timeline.Item color='green'>
							Posted second assignment on time <span className='date fr'>2015-09-15</span>
						</Timeline.Item>
						<Timeline.Item color='green'>
							Assignment 1 marked as completed <span className='date fr'>2015-09-09</span>
						</Timeline.Item>
						<Timeline.Item color='green'>
							Uploaded stats screenshot for assignment 1<span className='date fr'>2015-09-08</span>
						</Timeline.Item>
						<Timeline.Item color='green'>
							Posted first assignment on time <span className='date fr'>2015-09-05</span>
						</Timeline.Item>
						<Timeline.Item color='green'>
							Joined campaign <span className='date fr'>2015-09-01</span>
						</Timeline.Item>
					</Timeline>
				</Modal>
			</div>
		);
	}
}

export default ResultModal;
