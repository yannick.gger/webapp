import { useContext } from 'react';
import ReactDOM from 'react-dom';

import { ToastContext } from 'contexts';
import Toast from 'components/Toast/Toast';
import useToastPortal from 'hooks/ToastPortal/useToastPortal';

const ToastPortal = () => {
	const toastCtx = useContext(ToastContext);
	const { loaded, portalId } = useToastPortal();

	return loaded ? (
		ReactDOM.createPortal(
			<div className='toast-container'>
				{toastCtx.toasts.map((toast) => {
					return <Toast key={toast.id} mode={toast.mode} onClose={() => toastCtx.removeToast(toast.id)} message={toast.message} />;
				})}
			</div>,
			document.getElementById(portalId) as HTMLElement
		)
	) : (
		<></>
	);
};

export default ToastPortal;
