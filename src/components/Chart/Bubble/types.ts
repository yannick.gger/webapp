export interface IBubbleData {
	label?: string;
	data: Array<{ x: number; y: number; r: number }>;
	backgroundColor: string;
	borderColor?: string;
	hoverBackgroundColor?: string;
	hoverBorderColor?: string;
}

export interface IBubble {
	legendPosition?: 'bottom' | 'top' | 'left' | 'right';
	labels: Array<string>;
	datasets: Array<IBubbleData>;
	pointStyle?: 'circle' | 'cross' | 'crossRot' | 'dash' | 'line' | 'rect' | 'rectRounded' | 'rectRot' | 'star' | 'triangle';
}
