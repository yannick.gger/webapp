import { render } from '@testing-library/react';
import 'jest-canvas-mock';

import Bubble from './Bubble';

describe('Bubble Chart', () => {
	it('should render Bubble Chart', () => {
		const bubbleChart = render(<Bubble labels={['test']} datasets={[{ data: [{ x: 15, y: 60, r: 10 }], backgroundColor: '#222' }]} />);

		expect(bubbleChart).toMatchSnapshot();
	});
});
