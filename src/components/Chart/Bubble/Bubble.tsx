import { Bubble as BubbleChart } from 'react-chartjs-2';
import 'chart.js/auto';

import { IBubble } from './types';

/**
 *
 * @param props
 * @returns JSX.Element
 */

const Bubble = (props: IBubble) => {
	const options = {
		elements: {
			point: {
				pointStyle: props.pointStyle
			}
		}
	};

	const data = {
		labels: props.labels,
		datasets: props.datasets
	};

	return <BubbleChart data={data} options={options} />;
};

export default Bubble;
