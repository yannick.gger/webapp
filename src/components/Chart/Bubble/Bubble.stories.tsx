import { ComponentStory, ComponentMeta } from '@storybook/react';
import Bubble from './Bubble';
import { IBubble } from './types';

const labels = ['female', 'male'];

const datasets = [
	{
		label: 'Female',
		data: Array.from({ length: 50 }, () => ({
			x: Math.floor(Math.random() * (60 - 15) + 15),
			y: Math.floor(Math.random() * (60 - 15) + 15),
			r: Math.floor(Math.random() * (20 - 15) + 15)
		})),
		borderColor: 'rgba(255, 99, 132, 0.5)',
		backgroundColor: 'rgba(255, 99, 132, 0.5)'
	},
	{
		label: 'Male',
		data: Array.from({ length: 50 }, () => ({
			x: Math.floor(Math.random() * (60 - 15) + 15),
			y: Math.floor(Math.random() * (60 - 15) + 15),
			r: Math.floor(Math.random() * (20 - 15) + 15)
		})),
		borderColor: 'rgba(53, 162, 235, 0.5)',
		backgroundColor: 'rgba(53, 162, 235, 0.5)'
	}
];

export default {
	title: 'Chart',
	component: Bubble,
	argTypes: {
		legendPosition: {
			options: ['bottom', 'top', 'left', 'right'],
			control: { type: 'select' }
		},
		pointStyle: {
			options: ['circle', 'cross', 'crossRot', 'dash', 'line', 'rect', 'rectRounded', 'rectRot', 'star', 'triangle'],
			control: { type: 'select' }
		},
		labels: { control: { type: 'object' } },
		datasets: { control: { type: 'object' } }
	}
} as ComponentMeta<typeof Bubble>;

const Template: ComponentStory<typeof Bubble> = (args: IBubble) => <Bubble {...args} />;

export const BubbleChart = Template.bind({});

BubbleChart.args = {
	legendPosition: 'bottom',
	pointStyle: 'circle',
	labels: labels,
	datasets: datasets
};
