import { ComponentStory, ComponentMeta } from '@storybook/react';
import Radar from './Radar';
import { IRadar } from './types';

const labels = ['StrengthA', 'StrengthB', 'StrengthC', 'StrengthD', 'StrengthE', 'StrengthF'];

const datasets = [
	{
		data: labels.map(() => Math.floor(Math.random() * 5) + 1),
		backgroundColor: 'rgba(255, 99, 132, 0.5)',
		borderColor: 'rgba(255, 99, 132, 0.5)'
	}
];

export default {
	title: 'Chart',
	component: Radar,
	argTypes: {
		legendPosition: {
			options: ['bottom', 'top', 'left', 'right'],
			control: { type: 'select' }
		},
		labels: { control: { type: 'object' } },
		datasets: { control: { type: 'object' } }
	}
} as ComponentMeta<typeof Radar>;

const Template: ComponentStory<typeof Radar> = (args: IRadar) => <Radar {...args} />;

export const RadarChart = Template.bind({});

RadarChart.args = {
	legendPosition: 'bottom',
	labels: labels,
	datasets: datasets
};
