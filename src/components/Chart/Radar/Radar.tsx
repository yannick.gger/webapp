import { Radar as RadarChart } from 'react-chartjs-2';
import { IRadar } from './types';
import 'chart.js/auto';

const Radar = (props: IRadar) => {
	const options = {
		scales: {
			r: {
				beginAtZero: true,
				min: 0,
				max: 5,
				ticks: {
					stepSize: 1
				}
			}
		},
		plugins: {
			legend: {
				position: props.legendPosition ? props.legendPosition : 'top',
				display: props.legendPosition ? true : false
			}
		}
	};

	const data = {
		labels: props.labels,
		datasets: props.datasets
	};

	return <RadarChart data={data} options={options} />;
};

export default Radar;
