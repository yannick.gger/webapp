export interface IRadarData {
	label?: string;
	data: Array<number>;
	backgroundColor?: string;
	borderColor?: string;
	borderWidth?: number;
}

export interface IRadar {
	legendPosition?: 'top' | 'bottom' | 'left' | 'right';
	labels: Array<string>;
	datasets: Array<IRadarData>;
}
