import { ComponentStory, ComponentMeta } from '@storybook/react';
import Scatter from './Scatter';
import { IScatter } from './types';

const datasets = [
	{
		label: 'Test',
		data: Array.from({ length: 20 }, () => {
			return { x: Math.floor(Math.random() * 1000) + 1, y: Math.floor(Math.random() * 5000) + 1 };
		}),
		backgroundColor: 'rgba(255, 99, 132, 0.5)'
	}
];

export default {
	title: 'Chart',
	component: Scatter,
	argTypes: {
		legendPosition: {
			options: ['bottom', 'top', 'left', 'right'],
			control: { type: 'select' }
		},
		datasets: { control: { type: 'object' } }
	}
} as ComponentMeta<typeof Scatter>;

const Template: ComponentStory<typeof Scatter> = (args: IScatter) => <Scatter {...args} />;

export const ScatterChart = Template.bind({});

ScatterChart.args = {
	legendPosition: 'bottom',
	datasets: datasets
};
