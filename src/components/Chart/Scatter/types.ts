export interface IScatterData {
	label?: string;
	data: Array<{ x: number; y: number }>;
	backgroundColor?: string;
}

export interface IScatter {
	legendPosition: 'top' | 'bottom' | 'left' | 'right';
	datasets: Array<IScatterData>;
}
