import { Scatter as ScatterChart } from 'react-chartjs-2';
import 'chart.js/auto';

import { IScatter } from './types';

const Scatter = (props: IScatter) => {
	const options = {
		scales: {
			x: {
				beginAtZero: true
			},
			y: {
				beginAtZero: true
			}
		},

		plugins: {
			legend: {
				position: props.legendPosition
			}
		}
	};

	const data = {
		datasets: props.datasets
	};

	return <ScatterChart data={data} options={options} />;
};

export default Scatter;
