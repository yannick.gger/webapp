import styled from 'styled-components';
import { INumberContentStyle } from './types';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const typograph = typography.headings;
const large = {
	fontSize: typograph.h1.fontSize,
	fontWeight: typograph.h1.fontWeight
};
const medium = {
	fontSize: typograph.h2.fontSize,
	fontWeight: typograph.h2.fontWeight
};
const small = {
	fontSize: typograph.h3.fontSize,
	fontWeight: typograph.h3.fontWeight
};

const Content = styled.span<INumberContentStyle>`
	width: 100%;
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	text-align: center;
	font-size: ${(props) => (props.size === 'sm' ? small.fontSize : props.size === 'lg' ? large.fontSize : medium.fontSize)};
	font-weight: ${(props) => (props.size === 'sm' ? small.fontWeight : props.size === 'lg' ? large.fontWeight : medium.fontWeight)};
	color: ${(props) => props.color || colors.dataLibrary.value};
	font-family: ${typograph.fontFamily};

	& > span {
		font-size: 1rem;
		font-weight: normal;
	}
`;

export const Styled = {
	Content
};
