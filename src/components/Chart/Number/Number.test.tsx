import { render, screen } from '@testing-library/react';
import Number from './Number';

test('Number component should load', () => {
	const numberChart = render(<Number color='#fff' value={100} />);
	expect(numberChart).toMatchSnapshot();
});
