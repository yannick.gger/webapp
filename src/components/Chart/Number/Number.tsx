import { Styled } from './Number.style';
import { INumber } from './types';

import useNumberGrowEffect from 'hooks/Chart/useNumberGrowEffect';

/**
 *
 * @param props
 * @returns JSX.Element
 */

const Number = (props: INumber) => {
	let displayedValue = props.value;
	if (typeof props.value === 'number') {
		displayedValue = useNumberGrowEffect(props.value);
	}

	return (
		<Styled.Content color={props.color} size={props.size} weight={props.weight}>
			{displayedValue === 'N/A' ? <span>N/A</span> : displayedValue}
		</Styled.Content>
	);
};

export default Number;
