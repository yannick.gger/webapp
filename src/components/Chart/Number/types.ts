export interface INumberContentStyle {
	size?: 'sm' | 'lg';
	weight?: number;
	color?: string;
}

export interface INumber extends INumberContentStyle {
	value: number | string;
}
