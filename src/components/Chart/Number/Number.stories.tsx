import { ComponentStory, ComponentMeta } from '@storybook/react';
import Number from './Number';
import { INumber } from './types';

export default {
	title: 'Chart',
	component: Number,
	argTypes: {
		value: { type: 'number' },
		color: { control: 'color' }
	}
} as ComponentMeta<typeof Number>;

const Template: ComponentStory<typeof Number> = (args: INumber) => <Number {...args} />;

export const NumberChart = Template.bind({});

NumberChart.args = {
	value: 80,
	color: 'white'
};
