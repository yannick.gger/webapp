export interface ILineData {
	label?: string;
	data: Array<number>;
	borderColor: string;
	pointStyle?: string;
}

export interface ILine {
	borderWidth?: number;
	legendPosition?: 'top' | 'bottom' | 'left' | 'right';
	pointRadius?: number;
	lineTension: number;
	labels: Array<string>;
	datasets: Array<ILineData>;
	isHoverable?: boolean;
	chartHeight?: number;
}
