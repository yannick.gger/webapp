import { render } from '@testing-library/react';
import 'jest-canvas-mock';

import Line from './Line';

describe('Line Chart', () => {
	it('should render Line Chart', () => {
		const lineChart = render(
			<Line labels={['Jan', 'Feb', 'Mar', 'Apr']} datasets={[{ data: [10, 20, 50, 30], borderColor: '#ce3030' }]} legendPosition='bottom' lineTension={1} />
		);

		expect(lineChart).toMatchSnapshot();
	});
});
