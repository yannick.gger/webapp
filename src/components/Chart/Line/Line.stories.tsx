import { ComponentStory, ComponentMeta } from '@storybook/react';
import Line from './Line';
import { ILine } from './types';

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

const datasets = [
	{
		label: 'Dataset 1',
		data: labels.map(() => Math.floor(Math.random() * 100)),
		borderColor: 'rgba(255, 99, 132, 0.5)',
		backgroundColor: 'rgba(255, 99, 132, 0.5)'
	},
	{
		label: 'Dataset 2',
		data: labels.map(() => Math.floor(Math.random() * 100)),
		borderColor: 'rgba(53, 162, 235, 0.5)',
		backgroundColor: 'rgba(53, 162, 235, 0.5)'
	}
];

export default {
	title: 'Chart',
	component: Line,
	argTypes: {
		legendPosition: {
			options: ['bottom', 'top', 'left', 'right'],
			control: { type: 'select' }
		},
		pointRadius: { type: 'number' },
		lineTension: { type: 'number' },
		labels: { control: { type: 'object' } },
		datasets: { control: { type: 'object' } }
	}
} as ComponentMeta<typeof Line>;

const Template: ComponentStory<typeof Line> = (args: ILine) => <Line {...args} />;

export const LineChart = Template.bind({});

LineChart.args = {
	legendPosition: 'bottom',
	pointRadius: 2,
	lineTension: 0,
	labels: labels,
	datasets: datasets
};
