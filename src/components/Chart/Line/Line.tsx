import { useRef, useEffect, useState } from 'react';
import { Line as LineChart } from 'react-chartjs-2';
import { ChartData, ChartArea } from 'chart.js';
import 'chart.js/auto';
import typography from 'styles/variables/typography';
import { formatNumber } from 'shared/helpers/Chart/chart-util';

import { ILine } from './types';

/**
 *
 * @param props
 * @returns JSX.Element
 */

const Line = (props: ILine) => {
	const lineRef = useRef();
	const [lineData, setLineDate] = useState<ChartData<'line'>>({
		datasets: []
	});
	const [backgroundColor, setBackgroundColor] = useState<string | CanvasGradient>('transparent');
	const [isMouseHover, setIsMouseHover] = useState(false);

	useEffect(() => {
		const chart: any = lineRef.current;
		if (chart) {
			if (!props.isHoverable) {
				setBackgroundColor(createGradient(chart.ctx, chart.chartArea));
			} else {
				if (isMouseHover) {
					setBackgroundColor(createGradient(chart.ctx, chart.chartArea));
				} else {
					setBackgroundColor('transparent');
				}
			}
		}
	}, [isMouseHover]);

	const data = {
		labels: props.labels,
		datasets: props.datasets
	};

	useEffect(() => {
		const chart: any = lineRef.current;

		if (!chart) {
			return;
		}

		const chartData = {
			...data,
			datasets: data.datasets.map((dataset) => ({
				...dataset,
				backgroundColor: backgroundColor,
				fill: true
			}))
		};

		setLineDate(chartData);
	}, [props.datasets, backgroundColor]);

	const createGradient = (ctx: CanvasRenderingContext2D, area: ChartArea) => {
		const gradient = ctx.createLinearGradient(0, area.bottom, 0, area.top);

		const colorWithOpacity = props.datasets[0].borderColor.split('');
		colorWithOpacity.push('60');

		gradient.addColorStop(0, 'white');
		gradient.addColorStop(1, colorWithOpacity.join(''));

		return gradient;
	};

	const options = {
		elements: {
			line: {
				borderWidth: props.borderWidth,
				lineTension: props.lineTension * 0.1
			},
			point: {
				radius: props.pointRadius
			}
		},
		responsive: true,
		maintainAspectRatio: false,
		scales: {
			y: {
				beginAtZero: true,
				ticks: {
					font: {
						family: typography.BaseFontFamiliy,
						weight: 500
					},
					callback: function(value: number) {
						return formatNumber(value);
					}
				}
			},
			x: {
				ticks: {
					maxTicksLimit: 15,
					font: {
						family: typography.BaseFontFamiliy
					}
				}
			}
		},
		plugins: {
			counter: {
				innerText: ''
			},
			legend: {
				position: props.legendPosition,
				display: props.legendPosition ? true : false
			},
			title: {
				display: false
			}
		}
	};

	return (
		<LineChart
			height={props.chartHeight}
			onMouseEnter={() => setIsMouseHover(true)}
			onMouseLeave={() => setIsMouseHover(false)}
			ref={lineRef}
			data={lineData}
			options={options}
		/>
	);
};

export default Line;
