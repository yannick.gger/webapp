import { render } from '@testing-library/react';
import 'jest-canvas-mock';

import Bar from './Bar';

describe('Bar Chart', () => {
	it('should render Bar Chart', () => {
		const barChart = render(
			<Bar datasets={[{ label: 'Campaign A', data: [50, 60, 30] }, { label: 'Campaign B', data: [20, 80, 100] }]} labels={['Jan', 'Feb', 'Mar']} />
		);

		expect(barChart).toMatchSnapshot();
	});
});
