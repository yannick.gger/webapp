import { ComponentStory, ComponentMeta } from '@storybook/react';
import Bar from './Bar';
import { IBar } from './types';

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

const datasets = [
	{
		label: 'Dataset 1',
		data: labels.map(() => Math.floor(Math.random() * 100)),
		backgroundColor: 'rgba(255, 99, 132, 0.5)'
	},
	{
		label: 'Dataset 2',
		data: labels.map(() => Math.floor(Math.random() * 100)),
		backgroundColor: 'rgba(53, 162, 235, 0.5)'
	}
];

export default {
	title: 'Chart',
	component: Bar,
	argTypes: {
		type: {
			options: ['horizontal', 'vertical'],
			control: { type: 'select' }
		},
		legendPosition: {
			options: ['bottom', 'top', 'left', 'right'],
			control: { type: 'select' }
		},
		labels: {
			control: { type: 'object' }
		},
		datasets: {
			control: { type: 'object' }
		}
	}
} as ComponentMeta<typeof Bar>;

const Template: ComponentStory<typeof Bar> = (args: IBar) => <Bar {...args} />;

export const BarChart = Template.bind({});

BarChart.args = {
	type: 'horizontal',
	legendPosition: 'bottom',
	labels: labels,
	datasets: datasets
};
