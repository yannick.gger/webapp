import { Bar as BarChart } from 'react-chartjs-2';
import 'chart.js/auto';

import { IBar } from './types';

/**
 * @param props
 * @returns JSX.Element
 */

const Bar = (props: IBar) => {
	const indexAxis = props.type === 'horizontal' ? ('y' as const) : undefined;
	const images = props.images || [];

	const imgLabels = {
		id: 'imgLabels',

		afterDraw(chart: any, args: any, options: any) {
			const { ctx } = chart;
			const xAxis = chart.scales['x'];
			const yAxis = chart.scales['y'];
			xAxis.ticks.forEach((value: any, index: any) => {
				const x = xAxis.getPixelForTick(index);
				const image = new Image();
				image.src = options.images[index];
				ctx.drawImage(image, x - 12, yAxis.bottom + 10, 30, 30);
			});
		}
	};

	const options = {
		indexAxis: indexAxis,
		scales: {
			x: {
				ticks: {
					padding: props.xTickPadding || 0
				}
			}
		},
		elements: {
			bar: {
				borderWidth: props.chartBorderWidth
			}
		},
		responsive: true,
		plugins: {
			legend: {
				position: props.legendPosition
			},
			title: {
				display: false
			},
			imgLabels: {
				images: images
			}
		}
	};

	const data = {
		labels: props.labels,
		datasets: props.datasets
	};

	return <BarChart data={data} options={options} plugins={[imgLabels]} />;
};

export default Bar;
