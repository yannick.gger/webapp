interface IDatasets {
	label: string;
	data: Array<number>;
	borderColor?: string;
	backgroundColor?: string;
}

export interface IBar {
	type?: 'vertical' | 'horizontal';
	chartBorderWidth?: number;
	legendPosition?: 'bottom' | 'top' | 'left' | 'right';
	labels: Array<string | number | undefined>;
	datasets: Array<IDatasets>;
	images?: Array<string>;
	xTickPadding?: number;
}
