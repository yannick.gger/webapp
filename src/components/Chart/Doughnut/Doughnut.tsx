import { Doughnut as DoughnutChart } from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import 'chart.js/auto';
import typography from 'styles/variables/typography';

import { IDoughnut } from './types';
import Styled from './Doughnut.style';

/**
 *
 * @param props
 * @returns JSX.Element
 */

const Doughnut = (props: IDoughnut) => {
	const counter = {
		id: 'counter',
		beforeDraw(chart: any, args: any, options: any) {
			const { ctx, chartArea } = chart;

			ctx.save();
			ctx.fillStyle = options.fontColor;
			ctx.font = `700 ${options.fontSize} ${options.fontStyle || typography.BaseFontFamiliy}`;
			ctx.textAlign = 'center';

			const legendPosition = options.legendPosition;

			if (legendPosition) {
				ctx.fillText(
					options.innerText ? options.innerText : '',
					legendPosition === 'left' ? (chartArea[legendPosition] + chartArea.width + 50) / 2 : (chartArea.width + 50) / 2,
					chartArea.top + chartArea.height / 2
				);
			} else {
				ctx.fillText(options.innerText ? options.innerText : '', (chartArea.width + 50) / 2, chartArea.top + chartArea.height / 1.8);
			}
		}
	};

	const options = {
		responsive: true,
		maintainAspectRatio: false,
		layout: {
			padding: 25
		},
		plugins: {
			datalabels: {
				display: props.outsideLabel,
				padding: 10,
				align: 'end',
				anchor: 'end',
				formatter: function(value: any, context: any) {
					return context.chart.data.labels[context.dataIndex];
				},
				font: {
					family: typography.BaseFontFamiliy
				}
			},
			counter: {
				innerText: props.innerText,
				fontColor: props.innerFontColor,
				fontStyle: props.innerFontStyle,
				fontSize: props.innerFontSize,
				legendPosition: props.legendPosition
			},
			legend: {
				display: props.legendDisplay,
				position: props.legendPosition
			},
			title: {
				display: false
			}
		}
	};

	const doughnutData = {
		labels: props.labels,
		datasets: [
			{
				data: props.data,
				backgroundColor: props.backgroundColors,
				borderColor: props.borderColors,
				borderWidth: props.borderWidth,
				spacing: props.spacing,
				hoverBackgroundColor: props.hoverBackgroundColors,
				hoverBorderColor: props.hoverBorderColors,
				hoverBorderWidth: props.hoverBorderWidth,
				cutout: props.cutout
			}
		]
	};

	//@todo options ChartDataLabels type
	return (
		<Styled.Content>
			<DoughnutChart data={doughnutData} options={options} plugins={[counter, ChartDataLabels]} />
		</Styled.Content>
	);
};

export default Doughnut;
