import { render } from '@testing-library/react';
import 'jest-canvas-mock';

import Doughnut from './Doughnut';

describe('Doughnut Chart', () => {
	it('should render Doughnut Chart', () => {
		const component = render(<Doughnut title='Test Doughnut Chart' legendPosition='bottom' data={[90, 10]} />);

		expect(component).toMatchSnapshot();
	});
});
