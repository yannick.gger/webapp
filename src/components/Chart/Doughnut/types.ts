export interface IDoughnutData {
	data: Array<number>;
	backgroundColors?: Array<string>;
	borderColors?: Array<string>;
	borderWidth?: number;
	spacing?: number;
	hoverBackgroundColors?: Array<string>;
	hoverBorderColors?: Array<string>;
	hoverBorderWidth?: number;
}

export interface IDoughnut extends IDoughnutData {
	legendDisplay?: boolean;
	legendPosition?: 'bottom' | 'top' | 'left' | 'right';
	labels?: Array<string>;
	innerText?: string | number;
	innerFontColor?: string;
	innerFontStyle?: string;
	innerFontSize?: string;
	cutout?: number | string;
	outsideLabel?: boolean;
}
