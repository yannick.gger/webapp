import { ComponentStory, ComponentMeta } from '@storybook/react';
import Doughnut from './Doughnut';
import { IDoughnut } from './types';

const labels = ['female', 'male'];

const datasets = {
	data: labels.map(() => Math.floor(Math.random() * 100)),
	backgroundColors: ['rgba(255, 99, 132, 0.5)', 'rgba(53, 162, 235, 0.5)'],
	borderColors: [],
	borderWidth: 0,
	spacing: 1,
	hoverBackgroundColors: [],
	hoverBorderColors: [],
	hoverBorderWidth: 0
};

export default {
	title: 'Chart',
	component: Doughnut,
	argTypes: {
		legendPosition: {
			options: ['bottom', 'top', 'left', 'right'],
			control: { type: 'select' }
		},
		labels: { control: { type: 'object' } },
		data: { control: { type: 'object' } },
		backgroundColors: { control: { type: 'object' } },
		borderColors: { control: { type: 'object' } },
		borderWidth: { type: 'number' },
		spacing: { type: 'number' },
		hoverBackgroundColors: { control: { type: 'object' } },
		hoverBorderColors: { control: { type: 'object' } },
		hoverBorderWidth: { type: 'number' },
		innerText: { control: { type: 'text' } },
		innerFontColor: { control: 'color' },
		innerFontStyle: { control: { type: 'text' } },
		innerFontSize: { control: { type: 'text' } },
		cutout: { control: { type: 'text' } }
	}
} as ComponentMeta<typeof Doughnut>;

const Template: ComponentStory<typeof Doughnut> = (args: IDoughnut) => <Doughnut {...args} />;

export const DoughnutChart = Template.bind({});

DoughnutChart.args = {
	legendPosition: 'bottom',
	labels: labels,
	...datasets,
	innerText: 100,
	innerFontColor: '#000',
	innerFontSize: '2em',
	cutout: '80%'
};
