import { ComponentStory, ComponentMeta } from '@storybook/react';
import Pie from './Pie';
import { IPie } from './types';

const labels = ['Sweden', 'Finland', 'Denmark', 'Norway'];

const datasets = [
	{
		data: labels.map(() => Math.floor(Math.random() * 100)),
		backgroundColor: ['rgba(255, 99, 132, 0.5)', 'rgba(141, 99, 255, 0.5)', 'rgba(99, 245, 255, 0.5)', 'rgba(255, 239, 99, 0.5)'],
		borderColor: ['rgba(255, 99, 132, 0.5)', 'rgba(141, 99, 255, 0.5)', 'rgba(99, 245, 255, 0.5)', 'rgba(255, 239, 99, 0.5)']
	}
];

export default {
	title: 'Chart',
	component: Pie,
	argTypes: {
		legendPosition: {
			options: ['bottom', 'top', 'left', 'right'],
			control: { type: 'select' }
		},
		borderWidth: { type: 'number' },
		spacing: { type: 'number' },
		hoverOffset: { type: 'number' },
		labels: { control: { type: 'object' } },
		datasets: { control: { type: 'object' } }
	}
} as ComponentMeta<typeof Pie>;

const Template: ComponentStory<typeof Pie> = (args: IPie) => <Pie {...args} />;

export const PieChart = Template.bind({});

PieChart.args = {
	legendPosition: 'bottom',
	borderWidth: 1,
	spacing: 5,
	hoverOffset: 5,
	labels: labels,
	datasets: datasets
};
