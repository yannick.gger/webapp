export interface IPieData {
	label?: string;
	data: Array<number>;
	backgroundColor: Array<string>;
	borderColor?: Array<string>;
	hoverBackgroundColor?: Array<string>;
	hoverBorderColor?: Array<string>;
}

export interface IPie {
	legendPosition: 'top' | 'bottom' | 'left' | 'right';
	labels: Array<string>;
	datasets: Array<IPieData>;
	borderWidth?: number;
	hoverOffset?: number;
	spacing?: number;
}
