import { Pie as PieChart } from 'react-chartjs-2';
import 'chart.js/auto';

import { IPie } from './types';

/**
 *
 * @param props
 * @returns JSX.Element
 */

const Pie = (props: IPie) => {
	const options = {
		elements: {
			arc: {
				borderWidth: props.borderWidth
			}
		},
		datasets: {
			pie: {
				hoverOffset: props.hoverOffset,
				spacing: props.spacing
			}
		},
		plugins: {
			legend: {
				position: props.legendPosition
			}
		}
	};

	const data = {
		labels: props.labels,
		datasets: props.datasets
	};

	return <PieChart data={data} options={options} />;
};

export default Pie;
