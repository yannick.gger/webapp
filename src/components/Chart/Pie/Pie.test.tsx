import { render } from '@testing-library/react';
import 'jest-canvas-mock';

import Pie from './Pie';

describe('Pie Chart', () => {
	it('should render Pie Chart', () => {
		const component = render(
			<Pie labels={['10s', '20s', '30s']} datasets={[{ data: [30, 40, 30], backgroundColor: ['red', 'green', 'yellow'] }]} legendPosition='left' />
		);

		expect(component).toMatchSnapshot();
	});
});
