import React from 'react';
import { Modal, message } from 'antd';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import CampaignForm from '../campaign-form/';
import getOrganizationCampaignsQuery from 'graphql/get-organization-campaigns.graphql';
import getDraftCampaign from 'graphql/get-draft-campaigns.graphql';
import './styles.scss';

const CREATE_CAMPAIGN = gql`
	mutation createCampaign(
		$name: String!
		$brief: String
		$brandName: String
		$contactPerson: String
		$notificationEmail: String
		$influencerTargetCount: Int!
		$tags: [String]
		$mentions: [String]
		$organizationSlug: String!
		$campaignLogoId: ID
		$campaignCoverPhotoId: ID
		$currency: String!
		$language: String!
		$myListIds: [ID]
		$campaignId: ID
	) {
		createCampaign(
			input: {
				name: $name
				brief: $brief
				organizationSlug: $organizationSlug
				influencerTargetCount: $influencerTargetCount
				brandName: $brandName
				contactPerson: $contactPerson
				notificationEmail: $notificationEmail
				tags: $tags
				mentions: $mentions
				campaignLogoId: $campaignLogoId
				campaignCoverPhotoId: $campaignCoverPhotoId
				language: $language
				currency: $currency
				myListIds: $myListIds
				campaignId: $campaignId
			}
		) {
			campaign {
				id
				name
				brief
				contactEmail
				notificationEmail
				brandName
				contactPerson
				influencerTargetCount
				language
				currency
			}
		}
	}
`;
class CreateCampaignModal extends React.Component {
	state = {
		photoUploadStatus: false
	};

	handlePhotoUploading = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: true }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handlePhotoUploadDone = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: false }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handleSubmit = (createCampaign) => {
		return (e) => {
			e.preventDefault();

			if (this.state.photoUploadStatus) return message.info('Photo upload is still in progress. Please wait a moment.');

			const form = this.formRef.props.form;
			const {
				match: {
					params: { organizationSlug = null }
				},
				location: { state },
				history,
				closeModal
			} = this.props;

			form.validateFieldsAndScroll(
				async (
					err,
					{
						influencerTargetCount,
						name,
						brief,
						brandName,
						contactPerson,
						notificationEmail,
						campaignLogoId,
						campaignCoverPhotoId,
						mentions,
						tags,
						language,
						currency,
						myListIds
					}
				) => {
					if (err) return null;

					const graphQlValues = {
						influencerTargetCount,
						name,
						brief,
						brandName,
						contactPerson,
						notificationEmail,
						organizationSlug,
						mentions,
						tags,
						campaignLogoId,
						campaignCoverPhotoId,
						language,
						currency
					};
					if (state && state.myListIds) {
						graphQlValues.myListIds = state.myListIds;
					} else {
						graphQlValues.myListIds = myListIds && myListIds.length > 0 ? myListIds.map((id) => Number(id)) : [];
					}
					if (state && state.campaignId) {
						graphQlValues.campaignId = state.campaignId;
					}
					const res = await createCampaign({ variables: graphQlValues });
					if (res.data.createCampaign) {
						message.info('Campaign was created');
						closeModal();
						history.push(`/${organizationSlug}/campaigns/${res.data.createCampaign.campaign.id}`);
					} else {
						message.error('Something went wrong');
					}
				}
			);
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		const { location } = this.props;
		const lists = location && location.state && location.state.lists ? location.state.lists : [];
		return (
			<Mutation mutation={CREATE_CAMPAIGN} refetchQueries={[{ query: getDraftCampaign }]}>
				{(createCampaign, { loading }) => (
					<Modal
						className='create-campaign-modal-container'
						width={750}
						title='Create Campaign'
						visible
						onOk={this.handleOk}
						onCancel={this.handleCancel}
						wrapClassName='custom-modal separate-parts title-center'
						maskClosable={false}
						footer={null}
					>
						<CampaignForm
							wrappedComponentRef={this.saveFormRef}
							organizationSlug={this.props.match.params.organizationSlug}
							onPhotoUploading={this.handlePhotoUploading}
							onPhotoUploadDone={this.handlePhotoUploadDone}
							loading={loading}
							handleSubmit={this.handleSubmit(createCampaign)}
							lists={lists}
						/>
					</Modal>
				)}
			</Mutation>
		);
	}
}

export default CreateCampaignModal;
