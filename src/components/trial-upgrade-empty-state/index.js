import React, { Component } from 'react';
import { Button } from 'antd';
import OrganizationLink from 'components/organization-link';
import { checkGhostUserIsSaler } from 'services/auth-service';
import NoSub from './loading.svg';
import GhostUserAccess from '../ghost-user-access';

import './styles.scss';

export default class TrialUpgradeEmptyState extends Component {
	render() {
		return (
			<div className='no-sub-view no-content mt-50 text-center'>
				<img src={NoSub} className='mb-20' alt='' />
				<h1 className='mb-10'>Loading...</h1>
				<p className='mb-30'>
					{`It’s not. You need a subscription to access this page.`} <br />
					<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
						<OrganizationLink to={`/settings/billing/select-plan`}>
							<Button type='primary' size='large'>
								Upgrade now
							</Button>
						</OrganizationLink>
					</GhostUserAccess>
				</p>
			</div>
		);
	}
}
