import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import MainNavigation from './MainNavigation';

const mockedProps = {
	menuItems: [
		{
			url: '/mocked-url-1',
			title: 'Mocked menu item 1',
			icon: '',
			condition: undefined
		},
		{
			url: '/mocked-url-2',
			title: 'Mocked menu item 2',
			icon: 'calendar',
			condition: undefined
		}
	]
};

describe('<MainNavigation /> test', () => {
	it('renders main navigation', () => {
		const header = render(
			<MemoryRouter>
				<MainNavigation {...mockedProps} />
			</MemoryRouter>
		);
		expect(header).toMatchSnapshot();
	});
});
