import React, { useEffect, useRef, useState } from 'react';
import { IMainNavigationProps, menuItem } from './types';
import classNames from 'classnames';
import Icon from 'components/Icon';
import Styled from './MainNavigation.style';
import MenuLink from './Components/MenuLink';

/**
 * Main navigation
 * Menu that supports only two levels.
 * @param {IMainNavigationProps} props
 * @returns {JSX.Element}
 */
const MainNavigation = (props: IMainNavigationProps) => {
	const [isOpen, setIsOpen] = useState<boolean>(false);
	const [activeChildMenu, setActiveChildMenu] = useState<number>(-1);
	const wrapperRef = useRef<HTMLDivElement>(null);

	const closeMenu = () => {
		setIsOpen(false);
		setActiveChildMenu(-1);
	};

	const handleChildrenClick = (idx: number) => {
		if (idx === activeChildMenu) {
			setActiveChildMenu(-1);
		} else {
			setActiveChildMenu(idx);
		}
	};

	const handleClickOutside = (e: any): void => {
		if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
			closeMenu();
		}
	};

	const handleEscapeKey = (e: any): void => {
		if (wrapperRef.current && e.key === 'Escape') {
			closeMenu();
		}
	};

	const handleHamburgerClick = (): void => {
		setIsOpen(!isOpen);
	};

	useEffect(() => {
		document.addEventListener('click', handleClickOutside, true);
		document.addEventListener('keyup', handleEscapeKey, true);

		return () => {
			document.removeEventListener('click', handleClickOutside, true);
			document.removeEventListener('keyup', handleEscapeKey, true);
		};
	}, []);

	return (
		<div ref={wrapperRef}>
			<Styled.hamburgerMenu onClick={handleHamburgerClick} className={classNames({ open: isOpen })}>
				<span />
				<span />
				<span />
			</Styled.hamburgerMenu>

			<Styled.Wrapper className={isOpen ? 'open' : ''}>
				<Styled.MenuList>
					{props.menuItems &&
						props.menuItems.map((item: menuItem, idx: number) => {
							if (item.condition && !item.condition()) {
								return null;
							}

							return (
								<React.Fragment key={idx}>
									<Styled.MenuItem
										className={classNames({ expanded: activeChildMenu === idx, 'active-parent': location && location.pathname.includes(item.url) })}
										level={0}
										hasChildren={item.children !== undefined && item.children.length > 0}
									>
										<Styled.MenuItemRow onClick={() => handleChildrenClick(idx)}>
											<MenuLink
												isTopParent={item.children && item.children.length > 0}
												url={item.url}
												title={item.title}
												iconName={item.icon}
												tabIndex={0}
												onClick={() => !item.children && closeMenu()}
											/>
											{item.children && item.children.length > 0 && (
												<Styled.ToggleIcon>
													<Icon icon='chevron-right' />
												</Styled.ToggleIcon>
											)}
										</Styled.MenuItemRow>
										{item.children && item.children.length > 0 && (
											<Styled.MenuList className={activeChildMenu === idx ? 'open' : ''}>
												{item.children &&
													item.children.map((childItem: any) => {
														if (childItem.condition && !childItem.condition()) {
															return null;
														}
														return (
															<Styled.MenuItem level={1} hasChildren={false} key={`1_${childItem.title}_${idx}`}>
																<MenuLink url={childItem.url} title={childItem.title} iconName={childItem.icon} tabIndex={0} onClick={() => closeMenu()} />
															</Styled.MenuItem>
														);
													})}
											</Styled.MenuList>
										)}
									</Styled.MenuItem>
								</React.Fragment>
							);
						})}
				</Styled.MenuList>
			</Styled.Wrapper>
		</div>
	);
};

export default MainNavigation;
