export interface IMainNavigationProps {
	menuItems?: Array<menuItem>;
	location?: Location;
}

export type menuItem = {
	url: string;
	title: string;
	icon?: string;
	condition?: any;
	children?: Array<menuItem>;
};

export type StyledMenuItem = {
	level: number;
	hasChildren: boolean;
};
