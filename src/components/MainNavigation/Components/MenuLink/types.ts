export interface IMenuLinkProps extends React.HTMLAttributes<HTMLAnchorElement> {
	title: string;
	url: string;
	isTopParent?: boolean;
	iconName?: string;
}
