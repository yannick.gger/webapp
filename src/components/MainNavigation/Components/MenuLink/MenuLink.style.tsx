import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const MenuItemLink = styled(NavLink)`
	display: flex;
	align-items: center;
	flex-grow: 1;
	font-family: ${typography.BaseFontFamiliy};
	font-size: 1rem;
	font-weight: 500;
	color: ${colors.MainNavigation.color};
	width: 100%;
	line-height: 1;

	&:hover {
		color: ${colors.MainNavigation.colorHover};
	}

	.icon {
		margin-right: 14px;
		line-height: 0;
	}

	&.active-page {
		font-weight: 900;
	}
`;

const Styled = {
	MenuItemLink
};

export default Styled;
