import Icon from 'components/Icon';
import React from 'react';
import Styled from './MenuLink.style';
import { IMenuLinkProps } from './types';

const MenuLink = (props: IMenuLinkProps) => {
	const { title, url, iconName, tabIndex, onClick, isTopParent } = props;

	const handleClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
		if (isTopParent) e.preventDefault();

		onClick && onClick(e);
	};

	return (
		<Styled.MenuItemLink to={url} tabIndex={tabIndex} activeClassName='active-page' onClick={(e) => handleClick(e)}>
			{iconName && <Icon icon={iconName} size='32' />}
			<span>{title}</span>
		</Styled.MenuItemLink>
	);
};

export default MenuLink;
