import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import MenuLink from './MenuLink';

const mockedProps = {
	url: '/mocked-url-1',
	title: 'Mocked menu item 1',
	icon: ''
};

describe('<MenuLink /> test', () => {
	it('renders menu link', () => {
		const header = render(
			<MemoryRouter>
				<MenuLink {...mockedProps} />
			</MemoryRouter>
		);
		expect(header).toMatchSnapshot();
	});
});
