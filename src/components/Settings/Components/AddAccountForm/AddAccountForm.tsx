import Icon from 'components/Icon';
import Styled from '../../Settings.style';
import S from './AddAccountForm.style';
import { useContext, useEffect, useState } from 'react';
import { ToastContext } from 'contexts';
import LoadingSpinner from 'components/LoadingSpinner';
import { Button } from 'components/Button';
import { login } from 'shared/facebook/facebook-sdk';
import { loginResponse } from 'shared/facebook/types';
import FacebookAuthService from 'services/FacebookApi/Facebook-auth.service';
import { ICollabsData, StatusCode } from 'services/Response.types';
import Modal from 'components/Modal';

export interface IAddAccountForm {
	currentUser: any;
}

/**
 * Add Account form
 * @todo i18next
 * @param {IPersonalFormProps} props
 * @returns {JSX.Element}
 */
const AddAccountForm = (props: IAddAccountForm): JSX.Element => {
	const { currentUser } = props;
	const { addToast } = useContext(ToastContext);
	const [instagramToken, setInstagramToken] = useState<string>('');
	const [instagramIsLoading, setInstagramIsLoading] = useState<boolean>(false);
	const [instagramConnected, setInstagramConnected] = useState<boolean>(currentUser.connectedWithFacebook);
	const [instagramModalOpen, setInstagramModalOpen] = useState<boolean>(false);
	const [displayInstagramInfo, setDisplayInstagramInfo] = useState<boolean>(currentUser.connectedWithFacebook);

	const [FbPagesIsLoading, setFbPagesIsLoading] = useState<boolean>(false);
	const [FbPages, setFbPages] = useState<Array<string>>([]);

	const getConnectedFacebookPages = () => {
		let pages: Array<string> = [];
		setFbPagesIsLoading(true);

		FacebookAuthService.getBusinessAccounts()
			.then((result) => {
				if (result) {
					result.map((pageItem: ICollabsData) => {
						pages.push(pageItem.attributes.name);
					});

					setFbPages(pages);
					setFbPagesIsLoading(false);
					setDisplayInstagramInfo(true);
				}
			})
			.catch(() => {
				addToast({ id: 'pages-fb-ig-err', mode: 'error', message: 'Oops! Cannot get your connected pages' });
				setFbPagesIsLoading(false);
			});
	};

	const connectWithInstagram = () => {
		login().then((data: loginResponse) => {
			if (!data.authResponse) {
				setInstagramConnected(false);
				setInstagramIsLoading(false);
			} else {
				setInstagramToken(data.authResponse.accessToken);
				setInstagramConnected(true);
				setDisplayInstagramInfo(true);
			}
		});
	};

	const disconnectFromInstagram = async () => {
		setInstagramConnected(false);
		setInstagramIsLoading(true);
		setDisplayInstagramInfo(false);
		FacebookAuthService.deleteFacebookToken().then((result) => {
			if (result === StatusCode.OK) {
				addToast({ id: 'sign-out-fb-ig', mode: 'success', message: 'You have disconnected from Instagram.' });
				setFbPages([]);
			} else {
				addToast({ id: 'sign-out-fb-ig-err', mode: 'error', message: 'Oops! Something went wrong' });
			}
			setInstagramIsLoading(false);
		});
	};

	const handleConnectWithInstagram = () => {
		!instagramConnected ? connectWithInstagram() : disconnectFromInstagram();
	};

	const instagramButtonText = () => {
		if (instagramIsLoading) {
			return 'Loading...';
		}

		if (!instagramConnected) {
			return 'Connect Instagram';
		}

		if (instagramConnected) {
			return 'Disconnect Instagram';
		}
	};

	useEffect(() => {
		if (instagramToken !== '') {
			setInstagramIsLoading(true);
			FacebookAuthService.storeFacebookAccessToken(instagramToken).then((result) => {
				if (result === StatusCode.OK) {
					setInstagramConnected(true);
					setInstagramIsLoading(false);
					addToast({ id: 'sigin-fb-ig', mode: 'success', message: 'You have now connected with Instagram.' });

					getConnectedFacebookPages();
				}
			});
		}
	}, [instagramToken]);

	useEffect(() => {
		if (currentUser.connectedWithFacebook) {
			getConnectedFacebookPages();
		}
	}, []);

	return (
		<>
			<Styled.FormCard id='add-account'>
				<Styled.IconBar>
					<Icon icon='product' />
				</Styled.IconBar>
				<h2>Add account</h2>
				<p>Connect with social media or add an agent to your Collabs account here.</p>
				<S.AccountTypeContainer>
					<h4>Instagram</h4>
					<S.ButtonGroup>
						<Button size='sm' onClick={() => handleConnectWithInstagram()} disabled={instagramIsLoading}>
							{instagramButtonText()}
							{instagramIsLoading && <LoadingSpinner size='sm' />}
						</Button>
						{displayInstagramInfo && <S.LinkButton onClick={() => setInstagramModalOpen(!instagramModalOpen)}>Info</S.LinkButton>}
					</S.ButtonGroup>
				</S.AccountTypeContainer>
				<S.AccountTypeContainer>
					<h4>Add agent</h4>
					<p>Coming soon!</p>
				</S.AccountTypeContainer>
			</Styled.FormCard>
			<Modal open={instagramModalOpen} handleClose={() => setInstagramModalOpen(!instagramModalOpen)} size='xs'>
				<Modal.Header>
					<S.HeaderInner>
						<h3>Information about your connection</h3>
						<S.CloseButton onClick={() => setInstagramModalOpen(!instagramModalOpen)}>
							<Icon icon='cancel' size='16' />
						</S.CloseButton>
					</S.HeaderInner>
				</Modal.Header>
				<p>You have connected with Instagram with your selected account.</p>
				<S.ConnectedPagesContainer>
					<S.ConnectedPagesHeading>Connected Facebook pages</S.ConnectedPagesHeading>
					{FbPagesIsLoading ? (
						<LoadingSpinner size='sm' />
					) : (
						<S.FacebookPagesList>
							{FbPages.length > 0 && FbPages.map((page: string) => <S.FacebookPagesListItem>{page}</S.FacebookPagesListItem>)}
						</S.FacebookPagesList>
					)}
				</S.ConnectedPagesContainer>
				<S.LinkButton onClick={() => handleConnectWithInstagram()}>{instagramButtonText()}</S.LinkButton>
			</Modal>
		</>
	);
};

export default AddAccountForm;
