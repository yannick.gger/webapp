import styled from 'styled-components';
import { RegularLink } from 'styles/utils/Link';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const AccountTypeContainer = styled.div`
	border-bottom: 1px solid ${colors.addAccountCard.accountTypeBorderColor};
	padding-top: 1.875rem;
	padding-bottom: 1rem;

	&:not(:last-child) {
		margin-bottom: 1.5rem;
	}
`;

const ButtonGroup = styled.div`
	display: flex;
	align-items: center;
	gap: 32px;

	a {
		font-family: ${typography.SecondaryFontFamiliy};
		font-size: 1rem;
		font-weight: 900;
		margin-left: 0;
	}
`;

const CloseButton = styled.button`
	border: none;
	background-color: transparent;
	padding: 0;
	cursor: pointer;
	margin-left: auto;
	line-height: 0;
`;

const HeaderInner = styled.div`
	display: flex;
	align-items: center;
`;

const ConnectedPagesHeading = styled.span`
	display: block;
	font-size: 16px;
	font-weight: 900;
	margin-bottom: 0.5rem;
`;

const FacebookPagesList = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
`;

const FacebookPagesListItem = styled.li`
	margin-bottom: 8px;
	padding-bottom: 8px;
	border-bottom: 1px solid ${colors.addAccountCard.accountTypeBorderColor};
`;

const ConnectedPagesContainer = styled.div`
	margin-top: 1.25rem;
	margin-bottom: 1rem;
`;

const LinkButton = styled.a`
	${RegularLink};
	font-size: 0.875rem;
`;

const S = {
	AccountTypeContainer,
	ButtonGroup,
	CloseButton,
	HeaderInner,
	ConnectedPagesHeading,
	FacebookPagesList,
	FacebookPagesListItem,
	ConnectedPagesContainer,
	LinkButton
};

export default S;
