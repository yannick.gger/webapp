import React, { useContext, useEffect, useState } from 'react';
import Styled from '../../Settings.style';
import Icon from '../../../Icon';
import { Form, Formik } from 'formik';
import InputText from '../../../Form/Elements/Text';
import Select, { SelectOption } from '../../../Form/Elements/Select/Select';
import { Button, LinkButton } from '../../../Button';
import LoadingSpinner from 'components/LoadingSpinner';
import { createClient } from 'shared/ApiClient/ApiClient';
import { ToastContext } from 'contexts';
import handleErrors from 'utils/formik_error_handler';
import { ItemAttributes } from 'types/http';
import { JsonApiDocument, Store } from 'json-api-models';
import { isNil } from 'lodash';
import { AxiosRequestConfig } from 'axios';

type BrandDraft = {
	id?: string;
	name: string;
	client: string;
	logo?: File;
};
const BrandForm = () => {
	const client = createClient();
	const [loading, setLoading] = useState(false);
	const [initialValues, setInitialValues] = useState<BrandDraft | null>(null);
	const [models, setModels] = useState(new Store());
	const { addToast } = useContext(ToastContext);
	const clientOptions: SelectOption[] = models.findAll('client').map(({ id, name, publisher }) => ({
		value: `${publisher.id}_${id}`,
		label: `${name} (${publisher.name})`
	}));

	useEffect(() => {
		setLoading(true);
		Promise.all([client.get<JsonApiDocument>('/clients'), client.get<JsonApiDocument>('/brands', { params: { includes: 'client.publisher' } })])
			.then((responses) => {
				const updated = new Store();
				for (const { data } of responses) {
					updated.sync(data);
				}
				setModels(updated);
			})
			.finally(() => setLoading(false));
	}, []);

	if (loading) {
		return <LoadingSpinner position='center' />;
	}

	return (
		<Styled.FormCard>
			<Styled.IconBar>
				<Icon icon='product' />
			</Styled.IconBar>
			<h2>Manage brands</h2>
			{isNil(initialValues) ? (
				<>
					<p>Select your brand:</p>
					<div className='d-flex'>
						<Button
							onClick={() => {
								setInitialValues({
									name: '',
									client: 1 === clientOptions.length ? clientOptions[0].value : ''
								});
							}}
							size='sm'
						>
							Create a new brand
						</Button>
						{models.findAll('brand').map((brand) => (
							<Button
								key={brand.id}
								onClick={() => {
									setInitialValues({
										id: brand.id,
										name: brand.name,
										client: `${brand.client.publisher.id}_${brand.client.id}`
									});
								}}
								size='sm'
								className='ml-1'
							>
								{brand.name}
							</Button>
						))}
					</div>
				</>
			) : (
				<Styled.FormWrapper>
					<Formik
						initialValues={initialValues}
						onSubmit={async (values, { setErrors }) => {
							const [publisherId, clientId] = values.client.split('_');
							const config: AxiosRequestConfig = {
								url: `/publishers/${publisherId}/clients/${clientId}/brands`,
								method: 'POST',
								data: {
									name: values.name
								}
							};
							if (!isNil(values.id)) {
								config.url += `/${values.id}`;
								config.method = 'PATCH';
							}

							const promises = [client.request<ItemAttributes>(config)];

							if (!isNil(values.logo) && !isNil(values.id)) {
								const formData = new FormData();
								formData.set('file', values.logo);
								promises.push(client.post<ItemAttributes>(`/brands/${values.id}/logo`, formData));
							}

							try {
								await Promise.all(promises);
								addToast({ id: 'brand-created', mode: 'success', message: `Brand ${isNil(values.id) ? 'created' : 'updated'}!` });

								// Refresh models
								const { data } = await client.get<JsonApiDocument>('/brands');
								models.sync(data);
								setInitialValues(null);
								setModels(models);
							} catch (e) {
								handleErrors(e, setErrors);
							}
						}}
					>
						{({ isSubmitting, isValid, resetForm, setFieldValue, values }) => (
							<Form>
								<Styled.InputWrapper>
									<Styled.Label htmlFor='name'>Name</Styled.Label>
									<InputText id='name' name='name' placeholder='My brand' required size='md' />
								</Styled.InputWrapper>
								{isNil(values.id) ? (
									<Styled.InputWrapper>
										<Styled.Label htmlFor='client'>Client</Styled.Label>
										<Select name='client' options={clientOptions} required size='md' />
									</Styled.InputWrapper>
								) : (
									<Styled.InputWrapper>
										<Styled.Label htmlFor='logo'>Logo</Styled.Label>
										<Styled.UploadInput
											id='logo'
											name='logo'
											type='file'
											size='md'
											onChange={({ target }) => {
												if (target.files!.length) {
													setFieldValue('logo', target.files![0]);
												}
											}}
										/>
									</Styled.InputWrapper>
								)}
								<Styled.ButtonGroup>
									<LinkButton
										type='submit'
										onClick={() => {
											resetForm();
											setInitialValues(null);
										}}
										disabled={isSubmitting}
									>
										Cancel
									</LinkButton>
									<Button type='submit' disabled={!isValid || isSubmitting}>
										{isSubmitting ? <LoadingSpinner size='sm' /> : 'Save'}
									</Button>
								</Styled.ButtonGroup>
							</Form>
						)}
					</Formik>
				</Styled.FormWrapper>
			)}
		</Styled.FormCard>
	);
};

export default BrandForm;
