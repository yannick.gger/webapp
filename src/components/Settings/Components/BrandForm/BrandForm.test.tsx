import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';

import BrandForm from './BrandForm';

test('Matches the snapshot', async () => {
	const { asFragment } = render(
		<ThemeProvider theme={themes.default}>
			<BrandForm />
		</ThemeProvider>
	);

	expect(asFragment()).toMatchSnapshot();
	userEvent.click(await screen.findByText('Create a new brand'));
	userEvent.type(await screen.findByLabelText('Name'), 'Google @c');
	userEvent.click(await screen.findByText('Save'));

	await screen.findByText('Select your brand:');
});
