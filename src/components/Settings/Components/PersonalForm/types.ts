import { CurrentUser } from 'components/Settings/types';

export interface IPersonalFormProps {
	currentUser: CurrentUser;
}

export type PersonalFormValues = {
	email: string;
};
