import { Button, LinkButton } from 'components/Button';
import InputText from 'components/Form/Elements/Text';
import Icon from 'components/Icon';
import Styled from '../../Settings.style';
import { Form, Formik, FormikHelpers } from 'formik';
import sendUpdateEmailConfirmation from 'graphql/send-update-email-confirmation.graphql';
import { Mutation } from 'react-apollo';
import * as Yup from 'yup';
import { IPersonalFormProps, PersonalFormValues } from './types';
import { useContext } from 'react';
import { ToastContext } from 'contexts';
import LoadingSpinner from 'components/LoadingSpinner';
import classNames from 'classnames';

/**
 * PersonalForm
 * @todo i18next
 * @param {IPersonalFormProps} props
 * @returns {JSX.Element}
 */
const PersonalForm = (props: IPersonalFormProps): JSX.Element => {
	const { currentUser } = props;
	const { addToast } = useContext(ToastContext);

	const defaultValues: PersonalFormValues = {
		email: currentUser.email
	};

	const validateSchema = Yup.object().shape({
		email: Yup.string()
			.email('Invalid email')
			.required('Required')
	});

	const handleSubmit = async (sendUpdateEmailConfirmation: any, values: PersonalFormValues) => {
		const graphQlValues = values;

		try {
			const res = await sendUpdateEmailConfirmation({
				variables: graphQlValues
			});
			if (res.data.sendUpdateEmailConfirmation) {
				addToast({ id: 'updated-email', mode: 'success', message: 'Your email has been updated successfully.' });
			} else {
				addToast({ id: 'error-update-email', mode: 'error', message: 'There was an error when updating your email.' });
			}
		} catch ({ graphQLErrors }) {
			console.error(graphQLErrors);
		}
	};

	return (
		<Styled.FormCard>
			<Styled.IconBar>
				<Icon icon='product' />
			</Styled.IconBar>
			<h2>Personal info</h2>
			<Styled.FormWrapper>
				<Mutation mutation={sendUpdateEmailConfirmation}>
					{(mutation: any, { loading }: any) => (
						<>
							<Formik
								initialValues={defaultValues}
								onSubmit={(values: any, { setSubmitting }: FormikHelpers<any>) => {
									handleSubmit(mutation, values);
									setSubmitting(false);
								}}
								validationSchema={validateSchema}
							>
								{({ isSubmitting, isValid, resetForm, dirty }) => (
									<Form>
										<Styled.InputWrapper>
											<Styled.Label htmlFor='email'>Email</Styled.Label>
											<InputText type='email' id='email' name='email' placeholder={'you@collabs.app'} required size='md' />
										</Styled.InputWrapper>
										<Styled.ButtonGroup>
											<LinkButton type='submit' onClick={() => resetForm()} disabled={!dirty || isSubmitting}>
												Cancel
											</LinkButton>
											<Button type='submit' disabled={!isValid || isSubmitting}>
												Save
											</Button>
										</Styled.ButtonGroup>
									</Form>
								)}
							</Formik>
							<Styled.LoadingOverlay className={classNames({ visible: loading })}>
								<LoadingSpinner />
							</Styled.LoadingOverlay>
						</>
					)}
				</Mutation>
			</Styled.FormWrapper>
		</Styled.FormCard>
	);
};

export default PersonalForm;
