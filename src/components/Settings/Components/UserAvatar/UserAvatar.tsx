import { getUserFriendlyRoleName } from 'shared/utils/user';
import Styled from './UserAvatar.style';

export interface IUserAvatar {
	fullName: string;
}

const UserAvatar = (props: IUserAvatar) => {
	const { fullName } = props;

	const getInitals = (name: string) => {
		if (!name) return '';

		if (name.length === 2) {
			return name;
		} else if (name.indexOf(' ') >= 0) {
			return name
				.split(' ')
				.map((word: string) => word[0])
				.join('')
				.slice(0, 2);
		} else {
			return name.charAt(0);
		}
	};

	return (
		<Styled.Wrapper>
			<Styled.Avatar>
				<span>{getInitals(fullName)}</span>
			</Styled.Avatar>
			<Styled.fullNameSpan>{fullName}</Styled.fullNameSpan>
			<Styled.RoleSpan>{getUserFriendlyRoleName()}</Styled.RoleSpan>
		</Styled.Wrapper>
	);
};

export default UserAvatar;
