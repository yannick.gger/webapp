import styled from 'styled-components';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const Wrapper = styled.div`
	display: flex;
	align-items: center;
	flex-direction: column;
	margin-bottom: 48px;
`;

const Avatar = styled.div`
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
	border: 4px ${colors.avatar.borderColor} solid;
	background-color: ${colors.avatar.backgroundColor};
	padding: 0.25rem;
	border-radius: 250px;
	width: 204px;
	height: 204px;
	margin-bottom: 24px;

	> span {
		padding-top: 0.5rem;
		font-size: 3.583rem;
		font-weight: 900;
	}
`;

const fullNameSpan = styled.span`
	font-weight: 900;
	font-size: 1.5rem;
	line-height: 1.22;
`;

const RoleSpan = styled.span`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 1.25rem;
	color: #5c5c5c;
`;

const Styled = {
	Wrapper,
	Avatar,
	fullNameSpan,
	RoleSpan
};

export default Styled;
