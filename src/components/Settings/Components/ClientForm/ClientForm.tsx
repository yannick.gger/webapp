import React, { useContext } from 'react';
import Styled from '../../Settings.style';
import Icon from '../../../Icon';
import { Form, Formik } from 'formik';
import InputText from '../../../Form/Elements/Text';
import Select, { SelectOption } from '../../../Form/Elements/Select/Select';
import { Button, LinkButton } from '../../../Button';
import LoadingSpinner from 'components/LoadingSpinner';
import { createClient } from 'shared/ApiClient/ApiClient';
import { ToastContext } from 'contexts';
import handleErrors from 'utils/formik_error_handler';
import { ItemAttributes } from 'types/http';

type Props = {
	onSubmit: (iem: ItemAttributes) => void;
	options: SelectOption[];
};
const ClientForm = ({ options, onSubmit }: Props) => {
	const { addToast } = useContext(ToastContext);
	const initialValues = {
		name: '',
		publisher: ''
	};

	if (options.length === 1) {
		initialValues.publisher = options[0].value;
	}

	return (
		<Styled.FormCard>
			<Styled.IconBar>
				<Icon icon='product' />
			</Styled.IconBar>
			<h2>Create a client</h2>
			<Styled.FormWrapper>
				<Formik
					initialValues={initialValues}
					onSubmit={async ({ publisher, name }, { setErrors }) => {
						try {
							const { data } = await createClient().post<ItemAttributes>(`/publishers/${publisher}/clients`, { name });
							addToast({ id: 'client-created', mode: 'success', message: 'Client created!' });
							onSubmit(data);
						} catch (e) {
							handleErrors(e, setErrors);
						}
					}}
				>
					{({ isSubmitting, isValid, resetForm, dirty }) => (
						<Form>
							<Styled.InputWrapper>
								<Styled.Label htmlFor='name'>Name</Styled.Label>
								<InputText id='name' name='name' placeholder='My client' required size='md' />
							</Styled.InputWrapper>
							<Styled.InputWrapper>
								<Styled.Label htmlFor='publisher'>Publisher</Styled.Label>
								<Select name='publisher' options={options} required size='md' />
							</Styled.InputWrapper>
							<Styled.ButtonGroup>
								<LinkButton type='submit' onClick={() => resetForm()} disabled={!dirty || isSubmitting}>
									Cancel
								</LinkButton>
								<Button type='submit' disabled={!isValid || isSubmitting}>
									{isSubmitting ? <LoadingSpinner size='sm' /> : 'Save'}
								</Button>
							</Styled.ButtonGroup>
						</Form>
					)}
				</Formik>
			</Styled.FormWrapper>
		</Styled.FormCard>
	);
};

export default ClientForm;
