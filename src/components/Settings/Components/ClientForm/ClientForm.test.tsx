import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';

import ClientForm from './ClientForm';

const OPTIONS = [{ label: 'Publisher #1', value: 'ccaa3a64-2d17-409c-a9ae-96eef4ac9dd7' }];

test('displays the correct step', async () => {
	const callback = jest.fn();
	const { asFragment } = render(
		<ThemeProvider theme={themes.default}>
			<ClientForm options={OPTIONS} onSubmit={callback} />
		</ThemeProvider>
	);

	expect(asFragment()).toMatchSnapshot();
	userEvent.type(await screen.findByLabelText('Name'), 'Bond, James Bond');
	userEvent.click(await screen.findByText('Save'));

	await waitFor(async () => {
		expect(callback).toHaveBeenCalled();
	});
});
