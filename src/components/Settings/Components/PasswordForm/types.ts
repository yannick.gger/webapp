import { CurrentUser } from 'components/Settings/types';

export interface IPasswordFormProps {
	currentUser: CurrentUser;
}

export type PasswordFormValues = {
	oldPassword: string;
	newPassword: string;
	confirmPassword: string;
};
