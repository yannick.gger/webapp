import { Button, LinkButton } from 'components/Button';
import InputText from 'components/Form/Elements/Text';
import * as Yup from 'yup';
import Icon from 'components/Icon';
import Styled from '../../Settings.style';
import { Form, Formik, FormikHelpers, FormikState } from 'formik';
import { IPasswordFormProps, PasswordFormValues } from './types';
import { Mutation } from 'react-apollo';
import updateUserPassword from 'graphql/update-user-password.graphql';
import { useContext } from 'react';
import { ToastContext } from 'contexts';
import LoadingSpinner from 'components/LoadingSpinner';
import classNames from 'classnames';
import { GraphQLError } from 'graphql';

/**
 * PasswordForm
 * @todo i18next
 * @param {IPasswordFormProps} props
 * @returns {JSX.Element}
 */
const PasswordForm = (props: IPasswordFormProps): JSX.Element => {
	const { addToast } = useContext(ToastContext);
	const defaultValues: PasswordFormValues = {
		oldPassword: '',
		newPassword: '',
		confirmPassword: ''
	};

	const validateSchema = Yup.object().shape({
		oldPassword: Yup.string().required('This field is required'),
		newPassword: Yup.string()
			.min(8, 'Your password is too short')
			.required('This field is required'),
		confirmPassword: Yup.string()
			.required('This field is required')
			.when('newPassword', {
				is: (val: string) => (val && val.length > 0 ? true : false),
				then: Yup.string().oneOf([Yup.ref('newPassword')], 'Both password need to be the same')
			})
	});

	const handleSubmit = async (
		updateUserPassword: any,
		values: PasswordFormValues,
		resetForm: (nextState?: Partial<FormikState<any>> | undefined) => void,
		setFieldError: (field: string, message: string | undefined) => void
	) => {
		const graphQlValues = {
			oldPassword: values.oldPassword,
			newPassword: values.newPassword
		};

		try {
			const res = await updateUserPassword({
				variables: graphQlValues
			});

			if (res.data.updatePassword.successful) {
				addToast({ id: 'updated-password', mode: 'success', message: 'Your password has been updated successfully.' });
				resetForm();
			} else {
				addToast({ id: 'error-update-password', mode: 'error', message: 'There was an error when updating your password.' });
			}
		} catch ({ graphQLErrors }) {
			if (Array.isArray(graphQLErrors)) {
				graphQLErrors.map((error: GraphQLError) => {
					// Wrong password
					if (error.message === 'Failed to authenticate') {
						setFieldError('oldPassword', 'Wrong password, please try again.');
					}
				});
			}
		}
	};

	return (
		<Styled.FormCard>
			<Styled.IconBar>
				<Icon icon='product' />
			</Styled.IconBar>
			<h2>Change password</h2>
			<p>You can change your current password here.</p>
			<Styled.FormWrapper>
				<Mutation mutation={updateUserPassword}>
					{(mutation: any, { loading }: any) => (
						<>
							<Formik
								initialValues={defaultValues}
								onSubmit={(values: PasswordFormValues, { setSubmitting, resetForm, setFieldError }: FormikHelpers<PasswordFormValues>) => {
									handleSubmit(mutation, values, resetForm, setFieldError);
									setSubmitting(false);
								}}
								validationSchema={validateSchema}
							>
								{({ isSubmitting, isValid, resetForm, dirty }) => (
									<Form>
										<Styled.InputWrapper>
											<Styled.Label htmlFor='old-password'>Old password</Styled.Label>
											<InputText type='password' id='old-password' name='oldPassword' placeholder='Type your old password' required size='md' />
										</Styled.InputWrapper>
										<Styled.InputWrapper>
											<Styled.Label htmlFor='new-password'>New password</Styled.Label>
											<InputText type='password' id='new-password' name='newPassword' placeholder='Type your new password' required size='md' />
										</Styled.InputWrapper>
										<Styled.InputWrapper>
											<Styled.Label htmlFor='confirm-password'>Confirm password</Styled.Label>
											<InputText type='password' id='confirm-password' name='confirmPassword' placeholder='Confirm your new password' required size='md' />
										</Styled.InputWrapper>
										<Styled.ButtonGroup>
											<LinkButton type='submit' onClick={() => resetForm()} disabled={!dirty || isSubmitting}>
												Cancel
											</LinkButton>
											<Button type='submit' disabled={!isValid || isSubmitting}>
												Save
											</Button>
										</Styled.ButtonGroup>
									</Form>
								)}
							</Formik>
							<Styled.LoadingOverlay className={classNames({ visible: loading })}>
								<LoadingSpinner />
							</Styled.LoadingOverlay>
						</>
					)}
				</Mutation>
			</Styled.FormWrapper>
		</Styled.FormCard>
	);
};

export default PasswordForm;
