import PageHeader from 'components/PageHeader';
import { useState } from 'react';
import CollabsAuthService from 'services/Authentication/Collabs-api';
import UserAvatar from './Components/UserAvatar/UserAvatar';
import Styled from './Settings.style';

import PersonalForm from './Components/PersonalForm';
import PasswordForm from './Components/PasswordForm';
import { ISettingsContainer } from './types';
import AddAccountForm from './Components/AddAccountForm/AddAccountForm';
import { GlobalUserObject } from 'services/Authentication/Collabs-api/types';
import ClientForm from './Components/ClientForm/ClientForm';
import { reduce } from 'lodash';
import { SelectOption } from '../Form/Elements/Select/Select';
import BrandForm from './Components/BrandForm/BrandForm';

function isPublisher({ permissions }: GlobalUserObject): boolean {
	for (const id in permissions.entities) {
		const { type, role } = permissions.entities[id];

		if ('publisher' === type && 'administrator' === role) {
			return true;
		}
	}
	return false;
}

function getOptions(type: string, { permissions }: GlobalUserObject): SelectOption[] {
	return reduce(
		permissions.entities,
		(result: SelectOption[], entity, id) => {
			if (entity.type === type) {
				result.push({
					label: entity.name,
					value: id
				});
			}

			return result;
		},
		[]
	);
}

/**
 * SettingsContainer
 * @todo i18next
 * @param {ISettingsContainer} props
 * @returns {JSX.Element}
 */
const SettingsContainer = (props: ISettingsContainer): JSX.Element => {
	const [selectedTab, setSelectedTab] = useState('personal');
	const loggedInUser = CollabsAuthService.getCollabsUserObject();

	return (
		<Styled.Wrapper>
			<PageHeader headline='Settings' showBreadcrumb={true} showCurrentDate={false} />
			<Styled.Content>
				<Styled.TabNav>
					<Styled.Navigation>
						<Styled.NavigationItem>
							<Styled.NavigationItemLink onClick={() => setSelectedTab('personal')}>Personal</Styled.NavigationItemLink>
						</Styled.NavigationItem>
						{isPublisher(loggedInUser) && (
							<Styled.NavigationItem>
								<Styled.NavigationItemLink onClick={() => setSelectedTab('publishers')}>Team</Styled.NavigationItemLink>
							</Styled.NavigationItem>
						)}
					</Styled.Navigation>
				</Styled.TabNav>
				<UserAvatar fullName={loggedInUser.name} />
				<Styled.TabContainer>
					{selectedTab === 'personal' && (
						<Styled.Tab>
							<PersonalForm currentUser={props.currentUser} />
							{loggedInUser.permissions.isInfluencer && <AddAccountForm currentUser={props.currentUser} />}
							<PasswordForm currentUser={props.currentUser} />
						</Styled.Tab>
					)}
					{selectedTab === 'publishers' && (
						<Styled.Tab>
							<ClientForm
								onSubmit={async () => {
									await CollabsAuthService.me();
									setSelectedTab('personal');
								}}
								options={getOptions('publisher', loggedInUser)}
							/>
							<BrandForm />
						</Styled.Tab>
					)}
				</Styled.TabContainer>
			</Styled.Content>
		</Styled.Wrapper>
	);
};

export default SettingsContainer;
