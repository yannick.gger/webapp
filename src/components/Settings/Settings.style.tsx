import styled from 'styled-components';
import { CTALink } from 'styles/utils/CTALink';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import { Flex } from '../../styles/utils/UtilityClasses';
import { InputField } from '../../styles/formElements/input';

const Wrapper = styled.div`
	width: 100%;
	height: 100vh;

	${Flex};

	.ml-1 {
		margin-left: 1rem;
	}
`;

const Content = styled.div`
	max-width: 1920px;
	margin: 0 auto;
	padding: 0 2rem 3rem 2rem;
`;

const TabNav = styled.div``;

const Navigation = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
`;

const NavigationItem = styled.li``;

const NavigationItemLink = styled.a`
	${CTALink};
`;

const TabContainer = styled.div``;

const Tab = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const TabContent = styled.div`
	margin: 0 auto;
`;

const FormCard = styled.div`
	position: relative;
	background-color: ${colors.formCard.background};
	padding: 2.5rem;
	border: 1px solid ${colors.formCard.borderColor};
	box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.16);
	width: 100%;
	max-width: 43.75rem;

	& + & {
		margin-top: 2.5rem;
	}

	p {
		color: ${colors.formCard.color};
	}
`;

const IconBar = styled.div`
	margin-bottom: 2rem;
`;

const InputWrapper = styled.div`
	margin-bottom: ${guttersWithRem.xl};
`;

const UploadInput = styled.input`
	${InputField};
`;

const Label = styled.label`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 0.875rem;
`;

const FormWrapper = styled.div`
	padding-top: 1.5rem;
`;

const ButtonGroup = styled.div`
	display: flex;

	button:last-child {
		margin-left: auto;
	}
`;

const LoadingOverlay = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: rgba(255, 255, 255, 0.75);
	opacity: 0;
	visibility: hidden;
	transition: opacity 0.2s ease-in-out, visibility 0.2s ease-in-out;

	&.visible {
		opacity: 1;
		visibility: visible;
	}
`;

const Styled = {
	Wrapper,
	Content,
	TabNav,
	Navigation,
	NavigationItem,
	NavigationItemLink,
	TabContainer,
	Tab,
	TabContent,
	IconBar,
	InputWrapper,
	UploadInput,
	Label,
	FormWrapper,
	ButtonGroup,
	FormCard,
	LoadingOverlay
};

export default Styled;
