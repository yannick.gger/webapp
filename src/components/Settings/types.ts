export type CurrentUser = {
	id: string;
	name: string;
	email: string;
	connectedWithFacebook: boolean;
};

export interface ISettingsContainer {
	currentUser: CurrentUser;
}
