import React from 'react';
import { Modal } from 'antd';
import { TodoIcon, PigLeetoIcon, NotificationIcon, AltIcon } from 'components/icons';

import MobileApp from 'assets/img/app/mobile-app.png';
import GooglePlayBadge from 'assets/img/logo/google-play-store-logo.svg';
import AppStoreBadge from 'assets/img/logo/download-on-the-app-store-apple.svg';

import './style.scss';

const LinkedBadge = ({ badge, link = '#', alt = '', ...rest }) => (
	<div className='linked-badge' {...rest}>
		<a href={link} target='_blank' rel='noopener noreferrer'>
			<img src={badge} alt={alt} className='illustration' />
		</a>
	</div>
);

const modalContent = (
	<React.Fragment>
		{/* Summary */}
		<h1 className='title'>Don&apos;t forget the app!</h1>
		<p className='summary multi-line'>
			With the app you will get notifications as soon as you have anything todo. Also very simple to handle lorem ipsum dolor sit amet consectuter adipscing.
		</p>
		{/* Feature list */}
		<ul className='app-feature-list'>
			<li className='app-feature-list-item'>
				<NotificationIcon className='app-feature-list-item-icon' /> &nbsp; &nbsp; &nbsp;Notifications
			</li>
			<li className='app-feature-list-item'>
				<TodoIcon className='app-feature-list-item-icon' /> &nbsp; &nbsp; &nbsp;Todos
			</li>
			<li className='app-feature-list-item'>
				<PigLeetoIcon className='app-feature-list-item-icon' /> &nbsp; &nbsp; &nbsp;Payments
			</li>
			<li className='app-feature-list-item'>
				<AltIcon className='app-feature-list-item-icon' /> &nbsp; &nbsp; Everything campaigns
			</li>
		</ul>
		{/* Links logo */}
		<div>
			<LinkedBadge badge={AppStoreBadge} link='https://apps.apple.com/us/app/collabs/id1459481598?ls=1' />
			<LinkedBadge badge={GooglePlayBadge} link='https://play.google.com/store/apps/details?id=com.collabsapp' />
		</div>
		{/* App image */}
		<div className='mobile-app'>
			<img src={MobileApp} alt='Collabs Mobile App' />
		</div>
	</React.Fragment>
);

const DontForgetTheAppModal = (props) => (
	<Modal closable centered footer={null} width='1006px' destroyOnClose className='dont-forget-the-app' {...props}>
		{modalContent}
	</Modal>
);

export default DontForgetTheAppModal;
