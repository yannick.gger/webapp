import React from 'react';
import { Row, Col, Card, Icon, Select, Input, Form, Checkbox } from 'antd';
import { countries } from 'country-data';

export default class DeliveryForm extends React.Component {
	render() {
		const { userAccountSetting } = this.props.currentUser;
		const { getFieldDecorator, getFieldValue, setFieldsValue } = this.props.form;
		if (getFieldValue('userDeliverySetting.useSettingsAddress') === undefined) {
			setFieldsValue({ 'userDeliverySetting.useSettingsAddress': true });
		}

		return (
			<Row gutter={15}>
				<Col xs={{ span: 24 }} sm={{ span: 24 }}>
					<Form.Item label='Use my address'>
						{getFieldDecorator('userDeliverySetting.useSettingsAddress', {
							initialValue: true,
							valuePropName: 'checked'
						})(
							<Checkbox size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />}>
								Use the same address that you have selected under Account settings.
							</Checkbox>
						)}
					</Form.Item>
				</Col>
				{getFieldValue('userDeliverySetting.useSettingsAddress') && userAccountSetting && (
					<Card className='mb-30' style={{ padding: '20px' }}>
						<h3>
							{userAccountSetting.firstName} {userAccountSetting.lastName}
						</h3>
						{userAccountSetting.businessName}
						<br />
						{userAccountSetting.addressLine1}
						<br />
						{userAccountSetting.addressLine2}
						<br />
						{userAccountSetting.addressPostalCode} {userAccountSetting.addressCity}
						<br />
						{userAccountSetting.country}
					</Card>
				)}
				{!getFieldValue('userDeliverySetting.useSettingsAddress') && (
					<Col xs={{ span: 24 }}>
						<Row gutter={15}>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<Form.Item label='Name'>
									{getFieldDecorator('userDeliverySetting.name', {
										rules: [{ required: true, message: 'Name' }],
										initialValue: userAccountSetting ? `${userAccountSetting.firstName} ${userAccountSetting.lastName} ${userAccountSetting.businessName}` : ''
									})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Collabsgatan 35A' />)}
								</Form.Item>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<Form.Item label='Address'>
									{getFieldDecorator('userDeliverySetting.addressLine1', {
										initialValue: userAccountSetting ? userAccountSetting.addressLine1 : '',
										rules: [{ required: true, message: 'Skriv in din adress' }]
									})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Collabsgatan 35A' />)}
								</Form.Item>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<Form.Item label='Address line 2'>
									{getFieldDecorator('userDeliverySetting.addressLine2', {
										initialValue: userAccountSetting ? userAccountSetting.addressLine2 : ''
									})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Collabsgatan 35A' />)}
								</Form.Item>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 8 }}>
								<Form.Item label='Postnummer'>
									{getFieldDecorator('userDeliverySetting.addressPostalCode', {
										initialValue: userAccountSetting ? userAccountSetting.addressPostalCode : '',
										rules: [{ required: true, message: 'Skriv in ditt postnummer' }]
									})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='111 60' />)}
								</Form.Item>
							</Col>
						</Row>
						<Row gutter={15}>
							<Col xs={{ span: 24 }} sm={{ span: 12 }}>
								<Form.Item label='Stad'>
									{getFieldDecorator('userDeliverySetting.addressCity', {
										initialValue: userAccountSetting ? userAccountSetting.addressCity : '',
										rules: [{ required: true, message: 'Skriv in din stad' }]
									})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Stockholm' />)}
								</Form.Item>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 12 }}>
								<Form.Item label='Land'>
									{getFieldDecorator('userDeliverySetting.country', {
										initialValue: userAccountSetting ? userAccountSetting.country : '',
										rules: [{ required: true, message: 'Välj land' }]
									})(
										<Select
											prefix={<Icon type='global' style={{ color: 'rgba(0,0,0,.25)' }} />}
											placeholder='Select country'
											style={{ width: 360 }}
											filterOption={(input, option) => option.props.children[2].toLowerCase().indexOf(input.toLowerCase()) >= 0}
											optionFilterProp='children'
											size='large'
											showSearch
										>
											{countries.all
												.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
												.filter(({ status }) => status === 'assigned')
												.map((country) => (
													<Select.Option value={country.alpha2} key={country.alpha2}>
														{country.emoji} {country.name}
													</Select.Option>
												))}
										</Select>
									)}
								</Form.Item>
							</Col>
						</Row>
					</Col>
				)}
			</Row>
		);
	}
}
