import React, { Component } from 'react';
import { Alert, Form } from 'antd';
import { translate } from 'react-i18next';

class NotApplicableForPayment extends Component {
	render() {
		const { form, t } = this.props;
		return (
			<Form.Item>
				{form.getFieldDecorator('notApplicableForPaymentError', {
					rules: [
						{
							validator: (_, __, callback) => {
								callback('Payment not available unless Swedish and Company');
							},
							message: ''
						}
					]
				})(
					<Alert
						type={form.getFieldError('notApplicableForPaymentError') ? 'error' : 'warning'}
						description={t('influencer:notApplicableForPaymentDescription', {
							defaultValue:
								'This campaign has a compensation through payments which is currently only available for users within Sweden and with a registered company. We are doing our best to open up for new markets and hopefully you can join another campaign soon.'
						})}
					/>
				)}
			</Form.Item>
		);
	}
}

export default translate('influencer')(NotApplicableForPayment);
