import React from 'react';
import { Row, Col, Input, Form, Icon, Select, Checkbox, Tooltip } from 'antd';
import { countries, callingCountries } from 'country-data';

import NotApplicableForPayment from './not-applicable-for-payment';
import BirthdayPicker from './birthday-picker';
import stripeCountries from './stripe-countries.json';
import AdditionalOwners from './additional-owners';

const FormItem = Form.Item;
class StripeForm extends React.Component {
	showField = (fieldName) => {
		const getFieldValue = this.props.form.getFieldValue;
		const country = getFieldValue('userAccountSetting.country');
		if (!country) {
			return false;
		}
		const type = getFieldValue('userAccountSetting.type');
		if (!type) {
			return false;
		}
		const stripeCountry = stripeCountries.find(({ id }) => id === country);
		if (!stripeCountry) {
			return false;
		}
		const verificationFields = stripeCountry.verification_fields[type].minimum;

		return verificationFields.find((field) => field === fieldName);
	};

	render() {
		const { form, campaign } = this.props;
		const { getFieldDecorator, setFieldsValue, getFieldValue } = form;
		const numberPrefixSelector = getFieldDecorator('userAccountSetting.phone.prefix', {
			initialValue: '+46'
		})(
			<Select style={{ width: 120 }} showSearch>
				{callingCountries.all
					.sort((a, b) => (a.countryCallingCodes[0] > b.countryCallingCodes[0] ? 1 : -1))
					.map((country) => (
						<Select.Option value={country.countryCallingCodes[0]} key={country.emoji}>
							{country.emoji}
							{country.countryCallingCodes[0]}
						</Select.Option>
					))}
			</Select>
		);

		const notApplicableForPayment =
			campaign &&
			campaign.paymentCompensationsCount > 0 &&
			getFieldValue('userAccountSetting.country') &&
			getFieldValue('userAccountSetting.type') &&
			(getFieldValue('userAccountSetting.country') !== 'SE' || getFieldValue('userAccountSetting.type') !== 'company');

		return (
			<React.Fragment>
				<Row gutter={15}>
					<Col xs={{ span: 24 }} sm={{ span: 9 }}>
						<FormItem label='Country'>
							{getFieldDecorator('userAccountSetting.country', {
								rules: [{ required: true, message: 'Select your country' }]
							})(
								<Select
									prefix={<Icon type='global' style={{ color: 'rgba(0,0,0,.25)' }} />}
									placeholder='Select country'
									style={{ width: '100%' }}
									filterOption={(input, option) => option.props.children[2].toLowerCase().indexOf(input.toLowerCase()) >= 0}
									optionFilterProp='children'
									size='large'
									showSearch
								>
									{countries.all
										.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
										.filter(({ status }) => status === 'assigned')
										.map((country) => (
											<Select.Option value={country.alpha2} key={country.alpha2}>
												{country.emoji} {country.name}
											</Select.Option>
										))}
								</Select>
							)}
						</FormItem>
					</Col>
					<Col xs={{ span: 24 }} sm={{ span: 9 }}>
						<FormItem label='Type'>
							{getFieldDecorator('userAccountSetting.type', {
								rules: [{ required: true, message: 'Select if your instagram business is ran as a company or you as an individual' }]
							})(
								<Select prefix={<Icon type='global' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Select type' style={{ width: '100%' }} size='large'>
									<Select.Option value={'individual'}>Individual</Select.Option>

									<Select.Option value={'company'}>Company</Select.Option>
								</Select>
							)}
						</FormItem>
					</Col>
				</Row>
				{notApplicableForPayment && <NotApplicableForPayment form={form} />}

				{!notApplicableForPayment && getFieldValue('userAccountSetting.country') && getFieldValue('userAccountSetting.type') && (
					<Row>
						{getFieldValue('userAccountSetting.type') !== 'company' && (
							<Row>
								<Row>
									<Col sm={{ span: 24 }}>
										<h3>{`Your information`}</h3>
									</Col>
								</Row>
								<Row gutter={15}>
									<Col xs={{ span: 24 }} sm={{ span: 9 }}>
										<FormItem label='First name'>
											{getFieldDecorator('userAccountSetting.firstName', {
												rules: [{ required: true, message: 'Enter your first name' }]
											})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)', paddingBottom: '2px' }} />} placeholder='John' />)}
										</FormItem>
									</Col>

									<Col xs={{ span: 24 }} sm={{ span: 9 }}>
										<FormItem label='Last name'>
											{getFieldDecorator('userAccountSetting.lastName', {
												rules: [{ required: true, message: 'Enter your last name' }]
											})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Johnsson' />)}
										</FormItem>
									</Col>
								</Row>

								{(this.showField('legal_entity.personal_id_number') ||
									(getFieldValue('userAccountSetting.type') === 'individual' && getFieldValue('userAccountSetting.country') === 'SE')) && (
									<Row gutter={15}>
										<Col xs={{ span: 24 }} sm={{ span: 18 }}>
											<FormItem label='Personal ID number'>
												{getFieldDecorator('userAccountSetting.personalIdNumber', {
													rules: [
														{ required: true, message: 'Skriv in ditt personnummer' },
														{ transform: (value) => value && value.trim() },
														{ pattern: /^(19|20)?[0-9]{6}[- ]?[0-9]{4}$/, message: 'Follow pattern YYYYMMDD-NNNN for personal id number' }
													]
												})(<Input size='large' placeholder='YYYYMMDD-XXXX' />)}
											</FormItem>
										</Col>
									</Row>
								)}

								<Row gutter={15}>
									<Col xs={{ span: 24 }} sm={{ span: 24 }}>
										<BirthdayPicker showField={this.showField} getFieldDecorator={getFieldDecorator} setFieldsValue={setFieldsValue} />
									</Col>
								</Row>

								{this.showField('legal_entity.ssn_last_4') && (
									<Col xs={{ span: 24 }} sm={{ span: 18 }}>
										<FormItem label='SSN last 4 digits'>
											{getFieldDecorator('userAccountSetting.ssnLast4', {
												rules: [{ required: true, message: 'Enter your last 4 digits on your SSN' }]
											})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='111 60' />)}
										</FormItem>
									</Col>
								)}

								{this.showField('legal_entity.personal_address_line1') && (
									<Col xs={{ span: 24 }} sm={{ span: 18 }}>
										<FormItem label='Address'>
											{getFieldDecorator('userAccountSetting.personalAddressLine1', {
												rules: [{ required: true, message: 'Skriv in din address' }]
											})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Mylivingstreet 10' />)}
										</FormItem>
									</Col>
								)}

								{this.showField('legal_entity.personal_address_line1') && (
									<Col xs={{ span: 24 }} sm={{ span: 18 }}>
										<FormItem label='Address line2'>
											{getFieldDecorator('userAccountSetting.personalAddressLine2', {})(
												<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='C/O John Johnsson' />
											)}
										</FormItem>
									</Col>
								)}

								{this.showField('legal_entity.personal_address_postal_code') && (
									<Col xs={{ span: 24 }} sm={{ span: 7 }} style={{ paddingRight: '13px' }}>
										<FormItem label='Zip code'>
											{getFieldDecorator('userAccountSetting.personalAddressPostalCode', {
												rules: [{ required: true, message: 'Skriv in ditt postnummer' }]
											})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='111 11' />)}
										</FormItem>
									</Col>
								)}
								{this.showField('legal_entity.personal_address_city') && (
									<Col xs={{ span: 24 }} sm={{ span: 11 }} style={{ paddingLeft: '13px' }}>
										<FormItem label='City'>
											{getFieldDecorator('userAccountSetting.personalAddressCity', {
												rules: [{ required: true, message: 'Skriv in din stad' }]
											})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Stockholm' />)}
										</FormItem>
									</Col>
								)}
							</Row>
						)}

						{getFieldValue('userAccountSetting.type') === 'company' && (
							<Row>
								<Col sm={{ span: 24 }}>
									<h3 style={{ padding: '10px 0 0 0' }}>Company information</h3>
								</Col>
							</Row>
						)}
						{this.showField('legal_entity.business_name') && (
							<Col xs={{ span: 24 }} sm={{ span: 18 }}>
								<FormItem label='Business name'>
									{getFieldDecorator('userAccountSetting.businessName', {
										rules: [{ required: true, message: 'Enter the name of your Business' }]
									})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='My Company INC' />)}
								</FormItem>
							</Col>
						)}
						{this.showField('legal_entity.business_tax_id') && (
							<Col xs={{ span: 24 }} sm={{ span: 18 }}>
								<FormItem label='Organization number'>
									{getFieldDecorator('userAccountSetting.businessTaxId', {
										rules: [
											{ required: true, message: 'Fill in your business tax id' },
											{
												transform: (value) => value && value.trim()
											},
											{
												validator: (rule, value, callback) => {
													if (form.getFieldValue('userAccountSetting.country') === 'SE' && value && value.match(/^[0-9]{6}-[0-9]{4}$/) === null) {
														callback('Enter your swedish organization number as XXXXXX-XXXX');
													} else {
														callback();
													}
												},
												message: 'Enter your swedish organization number as XXXXXX-XXXX'
											}
										]
									})(
										<Input
											size='large'
											prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />}
											placeholder='552121-1111'
											onBlur={() => {
												if (
													form.getFieldValue('userAccountSetting.country') !== 'SE' ||
													[undefined, null].includes(form.getFieldValue('userAccountSetting.businessTaxId'))
												) {
													return;
												}
												form.getFieldValue('userAccountSetting.businessTaxId') &&
													form.setFieldsValue({
														'userAccountSetting.businessVat': `SE${form.getFieldValue('userAccountSetting.businessTaxId').replace(/\D/g, '')}01`
													});
											}}
										/>
									)}
								</FormItem>
							</Col>
						)}
						{getFieldValue('userAccountSetting.type') === 'company' && (
							<Col xs={{ span: 24 }} sm={{ span: 18 }}>
								<FormItem
									label={
										<span>
											VAT number for european union{' '}
											<Tooltip title='In Sweden this is SE followed by your organization number (without dash) followed by 01. Only valid if you are registered for VAT.'>
												<Icon type='question-circle-o' />
											</Tooltip>
										</span>
									}
								>
									{getFieldDecorator('userAccountSetting.businessVat', {
										rules: [
											{
												required: [
													'AT',
													'BE',
													'BG',
													'HR',
													'CY',
													'CZ',
													'DK',
													'EE',
													'FI',
													'FR',
													'DE',
													'EL',
													'HU',
													'IE',
													'IT',
													'LV',
													'LI',
													'LU',
													'MT',
													'NL',
													'PL',
													'PT',
													'RO',
													'SK',
													'SI',
													'ES',
													'SE'
												].includes(getFieldValue('userAccountSetting.country')),
												message: 'You need a company with VAT registration and you need to enter a correct VAT number.'
											},
											{
												transform: (value) => value && value.trim()
											},
											{
												pattern: /^(ATU[0-9]{8}|BE[01][0-9]{9}|BG[0-9]{9,10}|HR[0-9]{11}|CY[A-Z0-9]{9}|CZ[0-9]{8,10}|DK[0-9]{8}|EE[0-9]{9}|FI[0-9]{8}|FR[0-9A-Z]{2}[0-9]{9}|DE[0-9]{9}|EL[0-9]{9}|HU[0-9]{8}|IE([0-9]{7}[A-Z]{1,2}|[0-9][A-Z][0-9]{5}[A-Z])|IT[0-9]{11}|LV[0-9]{11}|LT([0-9]{9}|[0-9]{12})|LU[0-9]{8}|MT[0-9]{8}|NL[0-9]{9}B[0-9]{2}|PL[0-9]{10}|PT[0-9]{9}|RO[0-9]{2,10}|SK[0-9]{10}|SI[0-9]{8}|ES[A-Z]([0-9]{8}|[0-9]{7}[A-Z])|SE[0-9]{12}|GB([0-9]{9}|[0-9]{12}|GD[0-4][0-9]{2}|HA[5-9][0-9]{2}))$/,
												message: 'Follow pattern for EU VAT numbers.'
											}
										]
									})(<Input size='large' placeholder='SEXXXXXXXX01' disabled={form.getFieldValue('userAccountSetting.country') === 'SE'} />)}
								</FormItem>
							</Col>
						)}

						<Col xs={{ span: 24 }} sm={{ span: 18 }}>
							<FormItem label='Address line1'>
								{getFieldDecorator('userAccountSetting.addressLine1', {
									rules: [{ required: true, message: 'Skriv in din adress' }]
								})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Collabsgatan 35A' />)}
							</FormItem>
						</Col>

						<Col xs={{ span: 24 }} sm={{ span: 18 }}>
							<FormItem label='Address line2'>
								{getFieldDecorator('userAccountSetting.addressLine2', {})(
									<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='C/O John Johnsson' />
								)}
							</FormItem>
						</Col>

						<Col xs={{ span: 12 }} sm={{ span: 7 }} style={{ paddingRight: '13px' }}>
							<FormItem label='Zip code'>
								{getFieldDecorator('userAccountSetting.addressPostalCode', {
									rules: [{ required: true, message: 'Skriv in ditt postnummer' }]
								})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='111 60' />)}
							</FormItem>
						</Col>

						<Col xs={{ span: 12 }} sm={{ span: 11 }}>
							<FormItem label='City'>
								{getFieldDecorator('userAccountSetting.addressCity', {
									rules: [{ required: true, message: 'Skriv in din stad' }]
								})(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Stockholm' />)}
							</FormItem>
						</Col>

						<Col xs={{ span: 24 }} sm={{ span: 18 }}>
							<Form.Item label='Telefonnummer'>
								{getFieldDecorator('userAccountSetting.phone.number', {
									rules: [
										{ required: true, message: 'Skriv in ditt telefonnummer' },
										{
											pattern: /^(?=.*[0-9])[- ()0-9]+$/,
											message: 'Enter a correct phone number'
										}
									]
								})(
									<Input
										size='large'
										addonBefore={numberPrefixSelector}
										prefix={<Icon type='phone' style={{ color: 'rgba(0,0,0,.25)' }} />}
										placeholder='000 00 00 00'
									/>
								)}
							</Form.Item>
						</Col>

						{this.showField('legal_entity.additional_owners') && (
							<Col sm={{ span: 24 }}>
								<h3 style={{ padding: '15px 0 5px 0' }}>You and people who own more than 25%</h3>
							</Col>
						)}

						{this.showField('legal_entity.additional_owners') && (
							<Col xs={{ span: 24 }} sm={{ span: 24 }}>
								<AdditionalOwners form={form} />
							</Col>
						)}

						<Col xs={{ span: 24 }} sm={{ span: 24 }}>
							<FormItem label='Terms of service'>
								{getFieldDecorator('userAccountSetting.stripeTosShownAndAccepted', {
									rules: [
										{
											validator: (rule, value, callback) => {
												!value ? callback('You have to accept the terms') : callback();
											},
											message: 'You have to accept the terms'
										}
									],
									valuePropName: 'checked'
								})(
									<Checkbox size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='111 60'>
										By clicking, you agree to{' '}
										<a href='https://collabs.app/tos.html#for-influencers' target='_blank' rel='noopener noreferrer'>
											our terms
										</a>
										.
									</Checkbox>
								)}
							</FormItem>
						</Col>
					</Row>
				)}
			</React.Fragment>
		);
	}
}
export default StripeForm;
