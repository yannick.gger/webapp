import { Col, Form, Row, Select } from 'antd';
import moment from 'moment';
import React from 'react';

const Option = Select.Option;

const months = moment.monthsShort();

class BirthdayPicker extends React.Component {
	state = {
		months,
		days: Array.from(Array(31).keys()),
		year: null,
		month: null,
		day: null
	};

	handleYearChange = (value) => {
		const daysInMonth = moment(`${value || 2000}-${this.state.month || '01'}`, 'YYYY-MM').daysInMonth();
		this.setState({
			days: Array.from(Array(daysInMonth).keys()),
			year: value
		});
		this.props.setFieldsValue({ 'userAccountSetting.dob.day': this.state.day > daysInMonth ? null : this.state.day });
	};

	handleDayChange = (value) => {
		this.setState({
			day: value
		});
	};

	handleMonthChange = (value) => {
		const daysInMonth = moment(`${this.state.year || 2000}-${value || '01'}`, 'YYYY-MM').daysInMonth();
		this.setState({
			days: Array.from(Array(daysInMonth).keys()),
			month: value
		});
		this.props.setFieldsValue({ 'userAccountSetting.dob.day': this.state.day > daysInMonth ? null : this.state.day });
	};

	render() {
		const { getFieldDecorator } = this.props;
		const dayOptions = this.state.days.map((day) => <Option key={day + 1}>{day + 1}</Option>);
		const monthOptions = this.state.months.map((month, index) => <Option key={index + 1}>{month}</Option>);
		let yearOptions = [];

		const currentYear = moment().year();
		let startYear = currentYear - 120;

		while (startYear <= currentYear - 13) {
			startYear++;
			yearOptions.push(<Option key={startYear}>{startYear}</Option>);
		}
		yearOptions = yearOptions.reverse();

		return (
			<Form.Item label='Birthday'>
				<Row gutter={5}>
					<Col xs={{ span: 8 }} sm={{ span: 6 }} style={{ paddingRight: '13px' }}>
						{getFieldDecorator('userAccountSetting.dob.day', {
							rules: [{ required: true, message: 'Skriv in dagen i månaden då du föddes' }]
						})(
							<Select size='large' showSearch style={{ width: '100%' }} onChange={this.handleDayChange}>
								{dayOptions}
							</Select>
						)}
					</Col>

					<Col xs={{ span: 8 }} sm={{ span: 6 }} style={{ paddingRight: '13px' }}>
						{getFieldDecorator('userAccountSetting.dob.month', {
							rules: [{ required: true, message: 'Skriv in in månaden på året då du föddes' }]
						})(
							<Select size='large' showSearch style={{ width: '100%' }} onChange={this.handleMonthChange}>
								{monthOptions}
							</Select>
						)}
					</Col>
					<Col xs={{ span: 8 }} sm={{ span: 6 }}>
						{getFieldDecorator('userAccountSetting.dob.year', {
							rules: [{ required: true, message: 'Välj ett år' }]
						})(
							<Select size='large' showSearch style={{ width: '100%' }} onChange={this.handleYearChange}>
								{yearOptions}
							</Select>
						)}
					</Col>
				</Row>
			</Form.Item>
		);
	}
}

export default BirthdayPicker;
