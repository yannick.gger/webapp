import { Row, Col, Checkbox, InputNumber, Button, Form, Icon, Input } from 'antd';
import BirthdayPicker from './birthday-picker2';
import React from 'react';

let uuid = 0;
class AdditionalOwners extends React.Component {
	remove = (k) => {
		const { form } = this.props;
		// can use data-binding to get
		const keys = form.getFieldValue('keys');

		// can use data-binding to set
		form.setFieldsValue({
			keys: keys.filter((key) => key !== k)
		});
	};

	remove = (k) => {
		const { form } = this.props;
		const submitTypeKey = `userAccountSetting.additionalOwners[${k}].submitType`;
		form.setFieldsValue({ [submitTypeKey]: 'delete' });
	};

	add = () => {
		const { form } = this.props;
		// can use data-binding to get
		const keys = form.getFieldValue('keys');
		const nextKeys = keys.concat(++uuid);
		// can use data-binding to set
		// important! notify form to detect changes
		form.setFieldsValue({
			keys: nextKeys
		});
	};

	render() {
		const { getFieldDecorator, getFieldValue, setFieldsValue } = this.props.form;
		const formItemLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 4 }
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 24 }
			}
		};
		const formItemLayoutWithOutLabel = {
			wrapperCol: {
				xs: { span: 24, offset: 0 },
				sm: { span: 24, offset: 0 }
			}
		};
		getFieldDecorator('keys', { initialValue: [] });
		const keys = getFieldValue('keys');
		const formItems = keys.map((k, index) => {
			const submitType = getFieldValue(`userAccountSetting.additionalOwners[${k}].submitType`);

			return (
				<Form.Item
					style={{ display: submitType === 'delete' ? 'none' : 'block' }}
					{...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
					required={false}
					key={k}
				>
					{
						<Row gutter={15}>
							<Col xs={{ span: 24 }} sm={{ span: 9 }}>
								<Form.Item>
									{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].firstName`, {
										validateTrigger: ['onChange', 'onBlur'],
										rules: [
											{
												required: true,
												whitespace: true,
												message: 'Please input owners first name'
											}
										]
									})(<Input placeholder='First name' size='large' style={{ height: '46px', marginBottom: '10px' }} />)}
								</Form.Item>
							</Col>

							<Col xs={{ span: 24 }} sm={{ span: 9 }}>
								<Form.Item>
									{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].lastName`, {
										validateTrigger: ['onChange', 'onBlur'],
										rules: [
											{
												required: true,
												whitespace: true,
												message: 'Please input owners last name'
											}
										]
									})(<Input placeholder='Last name' size='large' style={{ height: '46px' }} />)}
									{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].submitType`)(<Input type='hidden' />)}
								</Form.Item>
							</Col>
						</Row>
					}

					{
						<Row gutter={15}>
							<Col>
								<BirthdayPicker index={k} showField={this.showField} getFieldDecorator={getFieldDecorator} setFieldsValue={setFieldsValue} />
							</Col>
						</Row>
					}

					{
						<Form.Item>
							<Row gutter={5} style={{ paddingTop: '20px' }}>
								<Col xs={{ span: 8 }} sm={{ span: 6 }}>
									Role
								</Col>
								<Col xs={{ span: 8 }} sm={{ span: 12 }}>
									{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].relationship.accountOpener`, {
										initialValue: true,
										valuePropName: 'checked'
									})(
										<Checkbox size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />}>
											Account opener
										</Checkbox>
									)}
								</Col>
							</Row>

							<Row gutter={5} style={{ paddingTop: '5px' }}>
								<Col xs={{ span: 8 }} sm={{ span: 6 }} />
								<Col xs={{ span: 8 }} sm={{ span: 12 }}>
									{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].relationship.owner`, {
										initialValue: true,
										valuePropName: 'checked'
									})(
										<Checkbox size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />}>
											Owner(25% or more)
										</Checkbox>
									)}
								</Col>
							</Row>

							<Form.Item>
								<Row style={{ paddingTop: '20px', display: 'flex', alignItems: 'center' }}>
									<Col xs={{ span: 8 }} sm={{ span: 6 }}>
										Job title
									</Col>
									<Col xs={{ span: 8 }} sm={{ span: 12 }}>
										{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].relationship.title`, {
											validateTrigger: ['onChange', 'onBlur'],
											rules: [
												{
													required: false,
													whitespace: true
												}
											]
										})(<Input placeholder='CEO, Manager, Partner' style={{ width: '100%', height: '46px' }} />)}
									</Col>
								</Row>
							</Form.Item>

							<Form.Item>
								<Row gutter={5} style={{ paddingTop: '20px', display: 'flex', alignItems: 'center' }}>
									<Col xs={{ span: 8 }} sm={{ span: 6 }}>
										Ownership
									</Col>
									<Col xs={{ span: 6 }} sm={{ span: 6 }} style={{ paddingRight: '13px' }}>
										{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].relationship.percentOwnership`, {
											rules: [
												{ required: false, type: 'number', message: 'The percent owned by the person of the account’s legal entity' },
												{
													validator: (rule, value, callback) => {
														callback(value < 0 ? [new Error()] : []);
													},
													message: 'Value can not be negative'
												}
											]
										})(<InputNumber className='ownership' size='large' placeholder='100' style={{ width: '100%', height: '46px' }} min={0} />)}
									</Col>
								</Row>
							</Form.Item>
							{getFieldDecorator(`userAccountSetting.additionalOwners[${k}].id`)(<Input type='hidden' />)}
						</Form.Item>
					}
					<Icon className='dynamic-delete-button' type='minus-circle-o' onClick={() => this.remove(k)} style={{ cursor: 'pointer' }} />
				</Form.Item>
			);
		});
		return (
			<React.Fragment>
				<Form.Item {...formItemLayoutWithOutLabel} label='Owners that own more than 25% of the company (Except you)' required>
					{formItems}
					<Button type='dashed' onClick={this.add}>
						<Icon type='plus' /> Add owner
					</Button>
				</Form.Item>
			</React.Fragment>
		);
	}
}

export default AdditionalOwners;
