import React, { Component } from 'react';
import { Row, Col, Button } from 'antd';
import { ReactSVG } from 'react-svg';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import OrganizationLink from '../organization-link/';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from '../ghost-user-access';

import NoDash from './no-dashboard.svg';

class NoCampaigns extends Component {
	render() {
		const { tab, draftCampaignsCount } = this.props;
		if (tab === 'active') {
			return (
				<div className='no-campaign-container pb-30'>
					<Row>
						<Col sm={{ span: 10, offset: 7 }} className='text-center'>
							<div className='empty-icon' style={{ marginTop: '0 !important' }}>
								<ReactSVG src={NoDash} />
							</div>
							<div className='mb-30'>
								<h2>{i18next.t('campaign:components.campaignList.active.message.title', { defaultValue: 'No active campaigns' })}</h2>
								<p>
									{draftCampaignsCount > 0
										? `${i18next.t('campaign:components.campaignList.active.message.subTitle1', { defaultValue: 'You have' })} ${draftCampaignsCount} ${
												draftCampaignsCount > 1
													? i18next.t('campaign:components.campaignList.active.message.subTitle2', { defaultValue: 'campaigns' })
													: i18next.t('campaign:components.campaignList.active.message.subTitle3', { defaultValue: 'campaign' })
										  } ${i18next.t('campaign:components.campaignList.active.message.subTitle4', { defaultValue: 'created that are not started' })}. `
										: i18next.t('campaign:components.campaignList.active.message.subTitle5', { defaultValue: "You don't have any campaigns created." })}
									{i18next.t('campaign:components.campaignList.active.message.subTitle6', {
										defaultValue: 'Continue working with Drafts or create a new one.'
									})}
								</p>
							</div>

							<OrganizationLink to='/campaigns/create'>
								<Button type='primary' className='btn-uppercase' size='large'>
									{i18next.t('campaign:components.campaignList.active.button.createACampaign', { defaultValue: 'Create a campaign' })}
								</Button>
							</OrganizationLink>
						</Col>
					</Row>
				</div>
			);
		} else {
			return (
				<div className='pb-30'>
					<Row>
						<Col sm={{ span: 10, offset: 7 }} className='text-center'>
							<div className='empty-icon'>
								<ReactSVG src={NoDash} />
							</div>
							<div className='mb-30'>
								<h2> {i18next.t('campaign:components.campaignList.notActive.message.title', { defaultValue: 'Create your first campaign' })}</h2>
								<p>
									{i18next.t('campaign:components.campaignList.notActive.message.subTitle', {
										defaultValue: 'When you have created a campaign, it will appear here where.'
									})}
								</p>
							</div>
							<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
								<OrganizationLink to='/campaigns/create'>
									<Button type='primary' className='btn-uppercase' size='large'>
										{i18next.t('campaign:components.campaignList.active.button.createACampaign', { defaultValue: 'Create a campaign' })}
									</Button>
								</OrganizationLink>
							</GhostUserAccess>
						</Col>
					</Row>
				</div>
			);
		}
	}
}

export default translate('campaign')(NoCampaigns);
