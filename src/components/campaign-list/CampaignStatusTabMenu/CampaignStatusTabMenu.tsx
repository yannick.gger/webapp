import { useState, useEffect } from 'react';

import { Menu } from 'antd';

const CampaignStatusTabMenu = (props: any) => {
	const [counts, setCounts] = useState<{ [key: string]: number }>({
		activeCampaignCount: 0,
		draftCampaignCount: 0,
		doneCampaignCount: 0,
		campaignCount: 0
	});

	useEffect(() => {
		if (props.organizationData) {
			setCounts({
				activeCampaignCount: props.organizationData.activeCampaignCount,
				draftCampaignCount: props.organizationData.draftCampaignCount,
				doneCampaignCount: props.organizationData.doneCampaignCount,
				campaignCount: props.organizationData.campaignCount
			});
		}
	}, [props.organizationData]);

	return (
		<div className='campaign-nav no-slide-animation ant-tabs-spacing-fix full-width nav-pills no-link'>
			<Menu mode='horizontal' selectedKeys={[props.currentMenuKey]} className='mb-20 clearfix' onClick={props.onClick}>
				{props.menuItems.map((menuItem: { key: string; text: string; counter: string }) => {
					return (
						<Menu.Item data-ripple='rgba(46,90,139,0.15)' key={menuItem.key}>
							{menuItem.text} ({counts[menuItem.counter]})
						</Menu.Item>
					);
				})}
			</Menu>
		</div>
	);
};

export default CampaignStatusTabMenu;
