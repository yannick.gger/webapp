import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Row, Col, Spin } from 'antd';
import { Link, withRouter, matchPath } from 'react-router-dom';
import { Query } from 'react-apollo';
import { translate } from 'react-i18next';
import i18n from 'i18next';

import CampaignStatusTabMenu from './CampaignStatusTabMenu';
import CampaignCard from './campaign-card/';
import CreateCard from '../create-card/';
import NoCampaigns from './no-campaigns';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from '../ghost-user-access';

import PaginatedItems from 'components/paginated-items';

const getOrganizationCampaignsQuery = gql`
	query getOrganizationCampaigns(
		$organizationSlug: String!
		$invitesSent: Boolean
		$invitesClosed: Boolean
		$draft: Boolean
		$active: Boolean
		$done: Boolean
		$withCampaignListFields: Boolean = true
		$sharedToTeam: Boolean = false
		$cursor: String
	) {
		organization(slug: $organizationSlug) {
			id
			campaignCount(sharedToTeam: $sharedToTeam) @include(if: $withCampaignListFields)
			draftCampaignCount(sharedToTeam: $sharedToTeam) @include(if: $withCampaignListFields)
			activeCampaignCount(sharedToTeam: $sharedToTeam) @include(if: $withCampaignListFields)
			doneCampaignCount(sharedToTeam: $sharedToTeam) @include(if: $withCampaignListFields)
			campaignsConnection(
				first: 12
				after: $cursor
				invitesSent: $invitesSent
				invitesClosed: $invitesClosed
				draft: $draft
				active: $active
				done: $done
				sharedToTeam: $sharedToTeam
			) {
				edges {
					node {
						id
						instagramOwnersJoinedCount @include(if: $withCampaignListFields)
						instagramOwnersInvitedCount @include(if: $withCampaignListFields)
						influencerTargetCount @include(if: $withCampaignListFields)
						name
						invitesSent

						todoInfluencersUploadContent {
							currentState
							remainingInfluencers
						}
						todoReviewContent {
							currentState
							remainingInfluencers
						}
						todoInfluencersPostInstagram {
							currentState
							remainingInfluencers
						}
						campaignCoverPhoto {
							id
							thumbnail
						}
						campaignLogo {
							id
						}
						campaignReviews {
							edges {
								node {
									id
									status
								}
							}
						}
						campaignDelivery {
							id
							latestDeliveryAt
						}
						assignments {
							edges {
								node {
									id
									startTime

									todoInfluencersDoAssignment {
										currentState
										remainingInfluencers
									}
								}
							}
						}
						instagramOwners(limit: 14) {
							edges {
								node {
									id
									instagramOwnerId
								}
							}
						}
						instagramOwnersJoined: instagramOwners(limit: 14, joined: true) {
							edges {
								node {
									id
									instagramOwnerId
								}
							}
						}
					}
				}

				pageInfo {
					endCursor
					hasNextPage
				}
			}
		}
	}
`;

class CampaignList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			currentMenuKey: this.getMenuItems()[0].key
		};
	}

	getMenuItems = () => {
		return [
			{
				key: 'active',
				text: i18n.t('campaign:components.campaignList.menu.active', { defaultValue: 'Active' }),
				counter: 'activeCampaignCount'
			},
			{
				key: 'draft',
				text: i18n.t('campaign:components.campaignList.menu.draft', { defaultValue: 'Draft' }),
				counter: 'draftCampaignCount'
			},
			{
				key: 'done',
				text: i18n.t('campaign:components.campaignList.menu.done', { defaultValue: 'Done' }),
				counter: 'doneCampaignCount'
			},
			{
				key: 'all',
				text: i18n.t('campaign:components.campaignList.menu.all', { defaultValue: 'All' }),
				counter: 'campaignCount'
			}
		];
	};

	changeCurrentMenu = ({ key }) => {
		this.setState({
			currentMenuKey: key
		});
	};

	render() {
		const { currentMenuKey } = this.state;
		const menuItems = this.getMenuItems();
		const showShared = !!matchPath(this.props.location.pathname, { path: '/:organizationSlug/campaigns/shared', exact: true });
		const variables = { organizationSlug: this.props.match.params.organizationSlug, [currentMenuKey]: true, sharedToTeam: showShared };

		return (
			<Query query={getOrganizationCampaignsQuery} variables={variables} fetchPolicy='cache-and-network' notifyOnNetworkStatusChange>
				{(queryRes) => {
					const { loading, error, data } = queryRes;

					return (
						<React.Fragment>
							<CampaignStatusTabMenu
								menuItems={menuItems}
								currentMenuKey={currentMenuKey}
								onClick={this.changeCurrentMenu}
								organizationData={data.organization}
							/>
							{loading && !data.organization && <Spin className='collabspin' />}
							{!loading && error && <p>{i18n.t('campaign:components.campaignList.error.notLoadCampaigns', { defaultValue: 'Could not load campaigns' })}</p>}
							{!loading && !error && data.organization && data.organization.campaignsConnection.edges.length === 0 && (
								<NoCampaigns tab={currentMenuKey} draftCampaignsCount={data.organization.draftCampaignCount} />
							)}
							{!error && data.organization && data.organization.campaignsConnection.edges.length > 0 && (
								<Row gutter={30} className='item-rows gutter-30'>
									<React.Fragment>
										<Col className='d-flex' xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 8 }}>
											<Link
												to={`/${this.props.match.params.organizationSlug}/campaigns/create`}
												data-ripple='rgba(132,146, 164, 0.2)'
												className='empty-cell create-new'
											>
												<CreateCard>{i18n.t('campaign:components.campaignList.createCampaign', { defaultValue: 'Create campaign' })}</CreateCard>
											</Link>
										</Col>
										<PaginatedItems
											noResults={<span>{i18n.t('campaign:components.campaignList.noResults', { defaultValue: 'No results' })}</span>}
											queryRes={queryRes}
											dataRef='organization.campaignsConnection' // The location in the graphql query for the list
										>
											{({ data: nodeData }) =>
												nodeData.edges.map(({ node }) => (
													<Col
														key={node.id}
														className='d-flex'
														xs={{ span: 24 }}
														md={{ span: nodeData.edges.length > 1 ? 12 : 16 }}
														xl={{ span: nodeData.edges.length > 1 ? 8 : 14 }}
													>
														<CampaignCard campaign={node} />
													</Col>
												))
											}
										</PaginatedItems>
									</React.Fragment>
								</Row>
							)}
						</React.Fragment>
					);
				}}
			</Query>
		);
	}
}

export default withRouter(translate('campaign')(CampaignList));
