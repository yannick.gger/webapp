import React, { Component } from 'react';
import { Menu, Dropdown, Icon, Modal, message } from 'antd';
import { isAdmin, hasGhostToken } from 'services/auth-service';
import removeCampaign from 'graphql/remove-campaign.graphql';
import { ApolloConsumer, Mutation } from 'react-apollo';
import ButtonLink from 'components/button-link';
import copyCampaign from 'graphql/copy-campaign.graphql';
import { getTokenPayload } from 'services/auth-service';

const MenuItemGroup = Menu.ItemGroup;

export default class CampaignDropdown extends Component {
	handleCopyCampaign = async (copyCampaign) => {
		const { campaign, history, organizationSlug } = this.props;
		const { id } = campaign;
		const response = await copyCampaign({ variables: { id } });
		if (response.data.copyCampaign.campaign) {
			message.success('Campaign was coppied!');
			const newCampaignId = response.data.copyCampaign.campaign.id;
			const newCampaignLink = `/${organizationSlug}/campaigns/${newCampaignId}`;
			history.push(newCampaignLink);
		} else {
			message.error('Something went wrong');
		}
	};
	menu = (copyCampaign) => {
		const { campaign } = this.props;
		const {
			user: { id: currentUserId }
		} = getTokenPayload();
		return (
			<Menu>
				<MenuItemGroup title='Quick actions'>
					<Menu.Item key='0' data-ripple='rgba(132,146, 164, 0.2)'>
						<ButtonLink onClick={() => this.handleCopyCampaign(copyCampaign)}>
							<Icon type='copy' className='mr-10' /> Make a copy
						</ButtonLink>
					</Menu.Item>
					{(isAdmin() ||
						hasGhostToken() ||
						Number(currentUserId) === Number(campaign.userId) ||
						((campaign.campaignReviews.edges.length === 0 ||
							(campaign.campaignReviews.edges.length > 0 && campaign.campaignReviews.edges[0].node.status !== 'approved')) &&
							!campaign.reportSent &&
							!campaign.makeAsReport)) && (
						<Menu.Item key='1' data-ripple='rgba(132,146, 164, 0.2)'>
							<ApolloConsumer>
								{(client) => (
									<a
										className='color-red'
										onClick={() => {
											this.showDeleteConfirm({ client: client, campaignId: campaign.id });
										}}
									>
										<Icon type='delete' className='mr-10' /> Delete
									</a>
								)}
							</ApolloConsumer>
						</Menu.Item>
					)}
				</MenuItemGroup>
			</Menu>
		);
	};
	showDeleteConfirm = ({ client, campaignId }) => {
		Modal.confirm({
			title: 'Are you sure you want to remove this campaign?',
			content: 'A removed campaign can not be restored after removal.',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				client
					.mutate({
						mutation: removeCampaign,
						variables: { id: campaignId },
						refetchQueries: ['getOrganizationCampaigns']
					})
					.then(() => {
						message.info('Campaign was removed');
					});
			}
		});
	};

	render() {
		return (
			<Mutation mutation={copyCampaign} refetchQueries={['getOrganizationCampaigns']}>
				{(copyCampaign) => (
					<React.Fragment>
						<Dropdown overlay={this.menu(copyCampaign)} trigger={['click']} placement='bottomRight'>
							<Icon type='ellipsis' />
						</Dropdown>
					</React.Fragment>
				)}
			</Mutation>
		);
	}
}
