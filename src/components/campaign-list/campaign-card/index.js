import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Row, Col, Tag, Card, Button, Avatar } from 'antd';
import { FormattedMessage, injectIntl } from 'react-intl';
import Moment from 'react-moment';
import moment from 'moment';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import withFlag from 'hocs/with-flag';
import { apiToMoment, API_DATE_FORMAT } from 'utils';
import { ClosedIcon, EyeIcon, CheckedIcon } from 'components/icons';
import { getTaskGroups, getRequiresActionCount } from 'services/organization-campaign-tasks';
import OrganizationLink from '../../organization-link/';
import CampaignDropdown from './campaign-dropdown.js';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from '../../ghost-user-access';

import './styles.scss';

class CampaignCard extends Component {
	get statusesLabel() {
		return {
			pending: (
				<div className='status-header-content pending'>
					<EyeIcon fill='#FFFFFF' />
					{i18next.t('campaign:components.campaignList.campaignCard.status.inReview', { defaultValue: 'In review' })}
				</div>
			),
			declined: (
				<div className='status-header-content declined'>
					<ClosedIcon fill='#FFFFFF' />
					{i18next.t('campaign:components.campaignList.campaignCard.status.declined', { defaultValue: 'Declined!' })}
				</div>
			),
			approved: (
				<div className='status-header-content approved'>
					<CheckedIcon fill='#FFFFFF' />
					{i18next.t('campaign:components.campaignList.campaignCard.status.approved', { defaultValue: 'Approved' })}
				</div>
			)
		};
	}

	renderLinkTo = (link, content = 'Find Influencers') => () => {
		if (!link) {
			return <div>{i18next.t('campaign:components.campaignList.campaignCard.noRecommendation', { defaultValue: 'Recommendation is not enabled' })}</div>;
		}

		return (
			<Link to={link}>
				<Button type='primary' size='large' style={{ width: '100%', marginBottom: '5px' }}>
					{content}
				</Button>
			</Link>
		);
	};

	get renderFooter() {
		const { campaign } = this.props;
		if (campaign.instagramOwnersJoinedCount) {
			return (
				<OrganizationLink to={`/campaigns/${campaign.id}`}>
					<div>
						<h4>
							<FormattedMessage
								id='campaign-count'
								defaultMessage={`{count} {count, plural, one {influencer} other {influencers}} has joined`}
								values={{ count: campaign.instagramOwnersJoinedCount }}
							/>
						</h4>
						<div className='avatar-list'>
							{campaign.instagramOwnersJoined.edges.slice(0, campaign.instagramOwnersJoinedCount > 6 ? 7 : 6).map(({ node }) => (
								<Avatar key={node.instagramOwnerId} src='#' icon='user' />
							))}

							{campaign.instagramOwnersJoinedCount > 6 && <Avatar key='more'>+{campaign.instagramOwnersJoinedCount - 7}</Avatar>}
						</div>
					</div>
				</OrganizationLink>
			);
		}

		const { edges } = campaign.campaignReviews;
		const lastStatus = ((edges[edges.length - 1] || {}).node || {}).status;
		if (lastStatus === 'approved') {
			return (
				<OrganizationLink to={`/campaigns/${campaign.id}`}>
					<div>
						<h4>
							<FormattedMessage
								id='campaign-count'
								defaultMessage={`Invitation sent to {count} {count, plural, one {influencer} other {influencers}}`}
								values={{ count: campaign.instagramOwnersInvitedCount }}
							/>
						</h4>
						<div className='avatar-list'>
							{campaign.instagramOwners.edges.slice(0, campaign.instagramOwnersInvitedCount > 6 ? 7 : 6).map(({ node }) => (
								<Avatar key={node.instagramOwnerId} src='#' icon='user' />
							))}

							{campaign.instagramOwnersInvitedCount > 6 && <Avatar key='more'>+{campaign.instagramOwnersInvitedCount - 7}</Avatar>}
						</div>
					</div>
				</OrganizationLink>
			);
		}

		if (campaign.instagramOwnersInvitedCount < campaign.influencerTargetCount) {
			return (
				<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
					<Row>
						<Col xs={{ span: 24 }}>{withFlag('recommendation')(this.renderLinkTo(`/${this.props.match.params.organizationSlug}/discover`), {})}</Col>
					</Row>
				</GhostUserAccess>
			);
		}

		if (campaign.instagramOwnersInvitedCount >= campaign.influencerTargetCount) {
			if (checkGhostUserIsSaler()) {
				return null;
			} else {
				return this.renderLinkTo(`/${this.props.match.params.organizationSlug}/campaigns/${campaign.id}/send-campaign-review`, 'Invite Influencers')();
			}
		}

		return null;
	}

	render() {
		const { campaign, intl, history, match } = this.props;
		const taskGroups = getTaskGroups({ campaign, organizationSlug: this.props.match.params.organizationSlug, intl });
		const todos = getRequiresActionCount({ taskGroups });
		const sortedAssignments = campaign.assignments.edges.slice().sort((a, b) => apiToMoment(a.node.startTime) > apiToMoment(b.node.startTime));
		const firstAssignment = sortedAssignments[0];
		const { edges } = campaign.campaignReviews;
		const lastStatus = ((edges[edges.length - 1] || {}).node || {}).status;

		return (
			<Card
				hoverable
				className='campaign-card'
				data-ripple='rgba(132,146, 164, 0.2)'
				cover={
					<OrganizationLink to={`/campaigns/${campaign.id}`}>
						{this.statusesLabel[lastStatus]}
						<img alt='Cover' src={campaign.campaignCoverPhoto ? campaign.campaignCoverPhoto.thumbnail : `/app/default-cover.jpg`} />
					</OrganizationLink>
				}
				actions={[<CampaignDropdown key='campaign-dropdown' campaign={campaign} organizationSlug={match.params.organizationSlug} history={history} />]}
			>
				<OrganizationLink to={`/campaigns/${campaign.id}`}>
					<Card.Meta title={campaign.name} />
					<hr />
					<Row>
						<Col xs={{ span: 12 }}>
							<h4 style={{ marginBottom: '0' }}>{campaign.invitesSent ? 'Invited' : 'Discovered'}</h4>
							<h2 className='color-blue'>{campaign.instagramOwnersInvitedCount}</h2>
						</Col>
						<Col xs={{ span: 12 }}>
							<h4 style={{ marginBottom: '0' }}>{i18next.t('campaign:components.campaignList.campaignCard.spots', { defaultValue: 'Spots' })}</h4>
							<h2 className='color-blue'>
								{campaign.instagramOwnersJoinedCount}/{campaign.influencerTargetCount}
							</h2>
						</Col>
					</Row>
					<hr />
					{firstAssignment && moment().isBefore(apiToMoment(firstAssignment.node.startTime)) && (
						<Tag className='card-tag'>
							<span>
								{i18next.t('campaign:components.campaignList.campaignCard.firstPost', { defaultValue: 'First post' })}
								<Moment parse={API_DATE_FORMAT} fromNow>
									{firstAssignment.node.startTime}
								</Moment>
							</span>
						</Tag>
					)}
					{firstAssignment && moment().isAfter(apiToMoment(firstAssignment.node.startTime)) && (
						<Tag className='card-tag' color='blue'>
							<span>
								{i18next.t('campaign:components.campaignList.campaignCard.firstPost', { defaultValue: 'First post' })}
								<Moment parse={API_DATE_FORMAT} fromNow>
									{firstAssignment.node.startTime}
								</Moment>
							</span>
						</Tag>
					)}
					{todos > 0 && (
						<Tag color='#3A7DE3' className='card-tag'>
							{`${i18next.t('campaign:components.campaignList.campaignCard.todo', { defaultValue: 'To-do' })}: ${todos}`}
						</Tag>
					)}
				</OrganizationLink>
				<hr />
				{this.renderFooter}
			</Card>
		);
	}
}

export default withRouter(injectIntl(translate('campaign')(CampaignCard)));
