import styled from 'styled-components';

const getSize = (size: string) => {
	switch (size) {
		case 'sm':
			return '24px';
		case 'lg':
			return '56px';
		default:
			return '48px';
	}
};

const getPosition = (position: string) => {
	switch (position) {
		case 'center':
			return 'margin: 2rem auto';
		default:
			return '';
	}
};

const Spinner = styled.div<{ size: 'sm' | 'md' | 'lg'; position: 'default' | 'center' }>`
	width: ${(props) => getSize(props.size)};
	height: ${(props) => getSize(props.size)};
	border: ${(props) => (props.size === 'sm' ? '2.5px' : '5px')} solid #333;
	${(props) => getPosition(props.position)};
	border: 5px solid #333;
	border-radius: 50%;
	display: block;
	box-sizing: border-box;
	animation: rotation 0.7s linear infinite, changeColor 1s ease-in-out infinite;
	border-bottom-color: transparent;
`;

const Styled = {
	Spinner
};

export default Styled;
