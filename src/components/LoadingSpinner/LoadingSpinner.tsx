import Styled from './LoadingSpinner.style';

export interface LoadingSpinnerProps {
	size?: 'sm' | 'md' | 'lg';
	position?: 'default' | 'center';
}

const LoadingSpinner = (props: LoadingSpinnerProps) => {
	return <Styled.Spinner className='cb-spinner' size={props.size || 'md'} position={props.position || 'default'} data-testid='loading-spinner' />;
};

export default LoadingSpinner;
