import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { Comment, Avatar, Tooltip, Button, Input, message } from 'antd';
import moment from 'moment';

import EditDeleteComment from './edit-delete-comment.js';
import updateCommentInProfile from 'graphql/update-comment-in-profile.graphql';

class SingleComment extends Component {
	state = {
		openEditForm: false,
		value: ''
	};

	handleSave = () => {
		const { updateCommentId, client } = this.props;
		const { value } = this.state;

		this.setState({ openEditForm: false });

		client
			.mutate({
				mutation: updateCommentInProfile,
				variables: { id: updateCommentId, comment: value },
				refetchQueries: ['getListComments']
			})
			.then(() => {
				message.success('Comment was saved');
			});
	};

	handleEditForm = () => {
		this.setState({ openEditForm: true });
	};

	handleCloseEditForm = () => {
		this.setState({ openEditForm: false });
	};

	onChange = (e) => {
		this.setState({ value: e.target.value });
	};

	render() {
		const { updateCommentId, node, currentUserId } = this.props;
		const userNameFirstCharacter = node.userName.charAt(0).toUpperCase();
		return (
			<div className='d-flex comment-alone-container'>
				<Comment
					author={
						<Tooltip title={`${node.userEmail} - ${node.organizationName}`}>
							<span>{node.userName}</span>
						</Tooltip>
					}
					avatar={<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>{userNameFirstCharacter}</Avatar>}
					content={
						this.state.openEditForm ? (
							<div className='pt-10 pb-10'>
								<Input.TextArea rows={4} defaultValue={node.comment} onChange={(value) => this.onChange(value)} />
								<div className='d-flex pl-20 pt-20' style={{ justifyContent: 'flex-end' }}>
									<Button type='primary' size='small' className='mr-10' onClick={this.handleSave}>
										Save
									</Button>
									<Button size='small' onClick={this.handleCloseEditForm}>
										Cancel
									</Button>
								</div>
							</div>
						) : (
							<p style={{ color: '#3d3f41' }}>{node.comment}</p>
						)
					}
					datetime={
						<Tooltip title={moment(node.createdAt).format('YYYY-MM-DD HH:mm:ss')}>
							<span>{moment(node.createdAt).fromNow()}</span>
						</Tooltip>
					}
				/>
				{currentUserId === node.userId && <EditDeleteComment handleEditForm={this.handleEditForm} commentId={updateCommentId} />}
			</div>
		);
	}
}

export default withApollo(SingleComment);
