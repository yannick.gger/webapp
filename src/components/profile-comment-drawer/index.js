import React from 'react';
import { Drawer, Button, Input, Spin, message, Comment, Avatar, Form, Icon, Badge } from 'antd';
import { Query, Mutation } from 'react-apollo';
import getListComments from 'graphql/get-list-comments.graphql';
import addListComment from 'graphql/add-list-comment.graphql';
import getCurrentUser from 'graphql/get-current-user.graphql';
import updateInstagramOwnerCommentStatus from 'graphql/update-instagram-owner-comment-status.graphql';
import SingleComment from './single-comment';

const { TextArea } = Input;

const Editor = ({ onChange, onSubmit, submitting, value, profileName }) => (
	<div>
		<Form.Item>
			<TextArea rows={4} onChange={onChange} value={value} placeholder={`Add comment for ${profileName}`} />
		</Form.Item>
		<Form.Item>
			<Button htmlType='submit' loading={submitting} onClick={onSubmit} type='primary' size='small'>
				Add Comment
			</Button>
		</Form.Item>
	</div>
);

class ProfileCommentDrawer extends React.Component {
	state = {
		visible: false,
		comment: ''
	};

	handleChange = (e) => {
		this.setState({ comment: e.target.value });
	};

	handleSubmit = (addListComment, mutationVars) => {
		return (e) => {
			e.preventDefault();

			addListComment({ variables: mutationVars })
				.then(() => {
					this.setState({ comment: '' });
					message.success('Comment was saved');
				})
				.catch((error) => {
					message.error("Comment can't be blank");
				});
		};
	};

	handleSearchInfluencer = (searchInfluencer) => {
		this.setState({ searchInfluencer });
	};

	handleUpdateInstagramOwnerCommentStatus = (updateInstagramOwnerCommentStatus, myListId, instagramOwnerId) => {
		return (e) => {
			e.preventDefault();

			updateInstagramOwnerCommentStatus({
				variables: { myListId: myListId, instagramOwnerId: instagramOwnerId, profileCommentStatus: 1 }
			})
				.then(() => {
					this.setState({ visible: true });
				})
				.catch(({ error }) => {
					message.error('Something went wrong updating comment status');
					throw error;
				});
		};
	};

	render() {
		const { comment } = this.state;
		const { myListId, organization, instagramOwnerUser, commentSeenCount, newCommentCount, oldCommentsCount } = this.props;
		const mutationVars = {
			myListId,
			organizationId: organization.id,
			instagramOwnerIds: [instagramOwnerUser.id],
			comment,
			status: 'new_comment',
			profileCommentStatus: 0
		};
		const newCommentIsPresent = newCommentCount > 0;

		return (
			<React.Fragment>
				{newCommentIsPresent ? (
					<Mutation mutation={updateInstagramOwnerCommentStatus}>
						{(updateInstagramOwnerCommentStatus) => (
							<Badge dot>
								<Button onClick={this.handleUpdateInstagramOwnerCommentStatus(updateInstagramOwnerCommentStatus, myListId, instagramOwnerUser.id)}>
									<Icon type='message' theme='filled' />
								</Button>
							</Badge>
						)}
					</Mutation>
				) : (
					<Badge count={commentSeenCount + oldCommentsCount} overflowCount={99}>
						<Button
							onClick={() => {
								this.setState({ visible: true });
							}}
						>
							<Icon type='message' theme='filled' />
						</Button>
					</Badge>
				)}
				<Mutation
					mutation={addListComment}
					refetchQueries={[
						{
							query: getListComments,
							variables: { myListId, organizationId: organization.id, instagramOwnerId: instagramOwnerUser.id, commentType: 'profile_comment' }
						}
					]}
				>
					{(addListComment, { loading: mutationLoading }) => {
						return (
							<Drawer
								className='profile-comment-drawer-wrapper'
								width={498}
								placement='right'
								closable={false}
								onClose={() => this.setState({ visible: false })}
								visible={this.state.visible}
							>
								<Query query={getCurrentUser} fetchPolicy='network-only'>
									{({ data: currentUserData, loading: queryLoading }) => {
										if (queryLoading) {
											return <Spin className='collabspin' />;
										}
										const currentUser = currentUserData && currentUserData.currentUser;
										const userNameFirstCharacter = currentUser.name.charAt(0).toUpperCase();
										return (
											<section className='profile-comment-section'>
												<Query
													query={getListComments}
													variables={{
														myListId,
														organizationId: organization.id,
														instagramOwnerId: instagramOwnerUser.id,
														commentType: 'profile_comment'
													}}
													fetchPolicy='cache-and-network'
												>
													{({ data: commentData, loading: commentLoading, refetch }) => {
														this.refetch = refetch;

														if (commentLoading) {
															return <Spin className='collabspin' />;
														}

														return commentData.listComments.edges.map((comment) => (
															<SingleComment updateCommentId={comment.node.id} node={comment.node} currentUserId={currentUser.id} />
														));
													}}
												</Query>
												<Comment
													avatar={<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>{userNameFirstCharacter}</Avatar>}
													content={
														<Editor
															onChange={this.handleChange}
															onSubmit={this.handleSubmit(addListComment, mutationVars)}
															submitting={mutationLoading}
															value={comment}
															profileName={instagramOwnerUser.username}
														/>
													}
												/>
											</section>
										);
									}}
								</Query>
							</Drawer>
						);
					}}
				</Mutation>
			</React.Fragment>
		);
	}
}

export default ProfileCommentDrawer;
