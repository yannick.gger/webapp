import { getLatestMessageObject } from 'components/IntegratedInbox/Helpers';
import moment from 'moment';
import CampaignsService from 'services/Campaign/Campaigns.service';
import { ICollabsData, ICollabsResponse } from 'services/Response.types';
import { mapWeekDay } from 'shared/helpers/dates';
import { Campaign, Conversation, Message } from './types';

export const useIntegratedInboxData = () => {
	/**
	 * getCampaigns
	 * @returns {Promise<Campaign[]>}
	 */
	const getCampaigns = async (): Promise<Campaign[]> => {
		return CampaignsService.getCampaigns(['latestMessage', 'unreadMessages']).then((response: ICollabsResponse) => {
			const data: Array<ICollabsData> = response.data;
			let campaigns: Array<Campaign> = [];

			data &&
				data.map((item: ICollabsData) => {
					let latestMessage: any = null;
					let hasUnreadMessages: boolean = false;

					if (item.relationships.latestMessage.data) {
						const latestMessageData = item.relationships.latestMessage.data;
						latestMessage = getLatestMessageObject(latestMessageData, response.included);

						const latestMessageId = latestMessageData.id;
						const unreadMessagesData = item.relationships.unreadMessages.data;
						hasUnreadMessages = unreadMessagesData && unreadMessagesData.find((u: { id: string }) => u.id === latestMessageId);
					}

					campaigns = [
						...campaigns,
						{
							id: Number(item.id),
							name: item.attributes.name,
							latestMessageDate: latestMessage ? new Date(latestMessage.attributes.createdAt).getTime() : new Date('1970-01-01').getTime(),
							date: latestMessage ? mapWeekDay(latestMessage.attributes.createdAt) : '',
							unSeen: hasUnreadMessages,
							coverPhoto: {
								largeCoverPhoto: item.links && item.links.largeCoverPhoto,
								mediumCoverPhoto: item.links && item.links.mediumCoverPhoto,
								smallCoverPhoto: item.links && item.links.smallCoverPhoto
							}
						}
					];

					campaigns = campaigns.sort((a: Campaign, b: Campaign) => b.latestMessageDate - a.latestMessageDate);
				});

			return campaigns;
		});
	};

	/**
	 * getConversations
	 * @param campaignId
	 * @returns
	 */
	const getConversations = async (campaignId: number): Promise<Conversation[]> => {
		return CampaignsService.getCampaignConversations(campaignId).then(async (response: ICollabsResponse) => {
			const data: Array<ICollabsData> = response.data;
			let conversations: Array<Conversation> = [];

			data &&
				data.map((item: ICollabsData) => {
					const influencerId = item.relationships.influencer.data.id;
					const influencer = response.included.find((i: any) => i.type === 'influencer' && i.id === influencerId);

					const latestMessageData = item.relationships.latestMessage.data;
					const latestMessageId = latestMessageData.id;

					const latestMessage = getLatestMessageObject(latestMessageData, response.included);
					const unreadMessagesData = item.relationships.unreadMessages.data;
					const hasUnreadMessages = unreadMessagesData && unreadMessagesData.find((u: { id: string }) => u.id === latestMessageId);

					if (item.id) {
						conversations = [
							...conversations,
							{
								conversationId: item.id,
								influencer: {
									influencerId: influencerId,
									username: influencer && influencer.attributes.username,
									profilePictureUrl: influencer && influencer.links.profilePictureUrl ? influencer.links.profilePictureUrl : undefined
								},
								createdAt: moment(item.attributes.createdAt).format('YYYY-MM-DD, HH:mm'),
								unSeen: hasUnreadMessages !== undefined,
								blast: item.attributes.blast,
								latestMessageDate: latestMessage ? new Date(latestMessage.attributes.createdAt).getTime() : new Date('1970-01-01').getTime(),
								date: latestMessage ? mapWeekDay(latestMessage.attributes.createdAt) : ''
							}
						];
					}
				});
			return conversations;
		});
	};

	const getMessages = async (conversationId: number) => {};

	const sendMessage = async () => {};

	return {
		getCampaigns,
		getConversations
	};
};
