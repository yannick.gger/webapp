import React, { useState, useRef, useContext, useEffect } from 'react';
import Grid from 'styles/grid/grid';
import Styled from './NewMessageModal.style';
import IconArrow from 'assets/icons/icon-chevron-down-small.svg';
import ConversationsService from 'services/Conversations';
import { ToastContext } from 'contexts';
import Avatar from 'components/Avatar';
import MessageField from '../MessageWindow/Components/MessageField';
import MessageList from '../MessageWindow/Components/MessageList';
import Accordion from 'components/Accordion/Accordion';
import { CampaignInstagramOwner } from 'components/IntegratedInbox/types';
import { ICollabsResponse } from 'services/Response.types';
import CampaignsService from 'services/Campaign/Campaigns.service';
import { getLoggedInUser } from 'components/IntegratedInbox/Helpers';
import { mapWeekDay } from 'shared/helpers/dates';
import moment from 'moment';
import StartView from '../MessageWindow/Components/StartView/StartView';
import Dropdown from 'components/Dropdown';
import { DropdownItem, DropdownMenu } from 'components/Dropdown/Dropdown';
import LoadingSpinner from 'components/LoadingSpinner';
import Modal from 'components/Modal';
import { IntegratedInboxContextType } from 'contexts/IntegratedInbox/Context';
import Icon from 'components/Icon';

export interface INewMessageModalProps {
	isModalOpen: boolean;
	onModalClose: (selectedUser: any) => void;
	influencers: any;
	campaignId: number;
	loggedInUser: any;
	context: IntegratedInboxContextType;
	influencersLoading: boolean;
}

/**
 * NewMessageModal
 * @todo i18next
 * @param {INewMessageModalProps} props
 * @returns {JSX.Element}
 */
const NewMessageModal = (props: INewMessageModalProps) => {
	const ANIMATION_DURATION = 200;
	const ANIMATION_DELAY = 100;
	const [messages, setMessages] = useState<Array<any>>([]);
	const [selectedUsers, setSelectedUsers] = useState<Array<any>>([]);
	const [joinedUsers, setJoinedUsers] = useState<Array<CampaignInstagramOwner>>([]);
	const [notJoinedUsers, setNotJoinedUsers] = useState<Array<CampaignInstagramOwner>>([]);
	const [displayAllJoined, setDisplayAllJoined] = useState<boolean>(false);
	const [displayAllNotJoined, setDisplayAllNotJoined] = useState<boolean>(false);
	const [disableRemoval, setDisableRemoval] = useState<boolean>(false);
	const [disabledTextbox, setDisabledTextbox] = useState<boolean>(true);
	const [conversationId, setConversationId] = useState<string>('');
	const [loadingConversation, setLoadingConversation] = useState<boolean>(false);
	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		if (props.influencers) {
			setJoinedUsers(props.influencers.filter((i: any) => i.joined === true));
			setNotJoinedUsers(props.influencers.filter((i: any) => i.joined === false));
		} else {
			setJoinedUsers([]);
			setNotJoinedUsers([]);
		}
	}, [props.influencers]);

	useEffect(() => {
		if (selectedUsers.length === 1) {
			getConversation(props.campaignId, selectedUsers[0].username);
		} else {
			setMessages([]);
		}
	}, [selectedUsers]);

	const closeModal = (selectedUsers: any) => {
		const modal = document.querySelector('.ant-modal');
		if (modal) {
			modal.classList.add('fade-out');
		}

		setTimeout(() => {
			props.onModalClose(selectedUsers);
		}, 100);
	};

	const createConversation = (message: string) => {
		if (selectedUsers && props.campaignId && message) {
			setDisableRemoval(true);

			const conversationData = {
				message: message,
				influencer: selectedUsers[0].id,
				campaign: props.campaignId.toString()
			};

			setMessages([
				...messages,
				{
					createdAt: 'Now',
					id: Math.random(),
					message: conversationData.message,
					readAt: '',
					conversationStarter: true,
					user: {
						id: props.loggedInUser.id,
						avatar: '',
						username: props.loggedInUser.name,
						name: props.loggedInUser.name,
						organization: {
							name: props.loggedInUser.organization.name
						}
					}
				}
			]);

			if (selectedUsers.length === 1) {
				ConversationsService.createConversation(conversationData).then((result: ICollabsResponse) => {
					if (result.data) {
						closeModal(selectedUsers[0]);
					} else {
						addToast({ id: 'IICCM-error', mode: 'error', message: 'Oops! Cannot send your message' });
						setDisableRemoval(false);
					}
				});
			} else {
				createBlastConversation(message, conversationData);
			}
		}
	};

	const createBlastConversation = (message: string, conversationData: any) => {
		if (props.campaignId && message) {
			setDisableRemoval(true);
			console.info('Create blast conversation');
			const userIds: Array<string> = [];
			selectedUsers.map((u: any) => userIds.push(u.id));

			ConversationsService.createBlastConversation(conversationData).then((result: ICollabsResponse) => {
				if (result.data) {
					closeModal('');
				} else {
					addToast({ id: 'IICCM-error', mode: 'error', message: 'Oops! Cannot send your message' });
					console.error(result.errors);
				}
			});
		}
	};

	const getMessages = (conversationId: string) => {
		setLoadingConversation(true);
		ConversationsService.getMessages(conversationId)
			.then((result: ICollabsResponse) => {
				if (result.data) {
					const data = result.data;
					const included = result.included;
					const messageArr: Array<any> = [];

					data.map((messageItem: any) => {
						const attributes = messageItem.attributes;
						const relationships = messageItem.relationships;
						let userObject: any = {};
						let isInfluencer: boolean = false;

						// If influencer
						if (relationships.influencer.data) {
							isInfluencer = true;
							userObject = included.find((x: any) => x.type === relationships.influencer.data.type && x.id === relationships.influencer.data.id);

							// If user
						} else if (relationships.user.data) {
							isInfluencer = false;
							userObject = included.find((x: any) => x.type === relationships.user.data.type && x.id === relationships.user.data.id);
						}

						messageArr.push({
							id: messageItem.id,
							createdAt: `${mapWeekDay(attributes.createdAt)}, ${moment(attributes.createdAt).format('HH:mm')}`,
							message: attributes.message,
							readAt: attributes.readAt,
							conversationStarter: getLoggedInUser().id === userObject.id,
							inConversation: attributes.inConversation,
							user: {
								id: userObject.id,
								initials: userObject.attributes.initials,
								name: userObject.attributes.name,
								avatar: userObject.attributes.profilePictureUrl,
								instagramUsername: userObject.attributes.instagramUsername,
								isInfluencer: userObject.attributes.instagramUsername !== null,
								organization: getLoggedInUser().organization
							}
						});
					});
					setMessages(messageArr);
					setDisabledTextbox(false);
				} else {
					addToast({ id: 'IIGM-error', mode: 'error', message: 'Oops! Cannot get messages for the conversation' });
					console.error(result.errors);
					setDisableRemoval(false);
				}
			})
			.finally(() => {
				setLoadingConversation(false);
			});
	};

	const getConversation = (campaignId: number, username: string) => {
		setDisabledTextbox(true);
		setLoadingConversation(true);
		CampaignsService.getCampaignConversations(campaignId, username).then((result: ICollabsResponse) => {
			if (result.data) {
				if (result.data.length > 0) {
					const id = result.data[0].id;
					getMessages(id);
					setConversationId(id);
				} else {
					setLoadingConversation(false);
				}

				setDisabledTextbox(result.data.length > 0);
			} else {
				addToast({ id: 'IIGC-error', mode: 'error', message: 'Oops! Cannot get conversation' });
				console.error(result.errors);
				setDisabledTextbox(false);
			}
		});
	};

	const sendMessage = (conversationId: string, message: string) => {
		if (message) {
			setMessages([
				...messages,
				{
					createdAt: 'Now',
					id: Math.random(),
					message: message,
					readAt: '',
					conversationStarter: true,
					user: {
						id: props.loggedInUser.id,
						avatar: '',
						username: props.loggedInUser.name,
						name: props.loggedInUser.name,
						organization: {
							name: props.loggedInUser.organization.name
						}
					}
				}
			]);

			ConversationsService.createMessage(conversationId, message).then((result: ICollabsResponse) => {
				if (result.data) {
					closeModal(selectedUsers[0]);
					setDisableRemoval(false);
				} else {
					setDisableRemoval(false);
					addToast({ id: 'IISM-error', mode: 'error', message: 'Oops! Cannot send your message' });
					console.error(result.errors);
				}
			});
		}
	};

	const onClickUser = (e: any, item: any) => {
		if (loadingConversation) return;
		const user = selectedUsers.find((u: any) => u.id === item.id);
		if (user === undefined && !disableRemoval) {
			setSelectedUsers([...selectedUsers, item]);
		}
	};

	const onClickRemoveUser = (e: any, item: any) => {
		if (loadingConversation) return;
		setMessages([]);
		setSelectedUsers(selectedUsers.filter((u) => u.id !== item.id));
	};

	const onClickSend = (message: string) => {
		if (conversationId !== '') {
			// We have a conversation already
			sendMessage(conversationId, message);
		} else {
			createConversation(message);
		}
	};

	const animation = (i: number) => `scaleIn ${ANIMATION_DURATION}ms ease-out ${ANIMATION_DELAY}ms forwards`;

	const onClickSelectAll = (e: React.MouseEvent<HTMLAnchorElement>, joined: boolean) => {
		e.preventDefault();
		setSelectedUsers(joined ? joinedUsers : notJoinedUsers);
		setDisabledTextbox(false);
		return false;
	};

	const renderDropdown = (joined: boolean) => {
		return (
			<Dropdown icon='options'>
				<DropdownMenu>
					<DropdownItem>
						<a href='#' onClick={(e: React.MouseEvent<HTMLAnchorElement>) => onClickSelectAll(e, joined)}>
							Select all
						</a>
					</DropdownItem>
					<DropdownItem>
						<a
							href='#'
							onClick={() => {
								setSelectedUsers([]);
							}}
						>
							Deselect all
						</a>
					</DropdownItem>
				</DropdownMenu>
			</Dropdown>
		);
	};

	return (
		<Modal
			open={props.isModalOpen}
			handleClose={() => {
				props.onModalClose(false);
			}}
		>
			<Modal.Header>
				<Styled.BackButton onClick={() => props.onModalClose(false)}>
					<Icon icon='chevron-left' />
					Back
				</Styled.BackButton>
			</Modal.Header>
			<Modal.Body>
				<Grid.Container gap={8}>
					<Grid.Column lg={12} xl={4}>
						<Styled.UserList>
							<div css={{ position: 'relative' }}>
								<div css={{ position: 'absolute', right: 0, top: '-5px' }}>{renderDropdown(true)}</div>
								<Accordion title={`Joined`} open={joinedUsers.length > 0}>
									<Styled.ListWrapper className={displayAllJoined ? 'scrollbar' : ''}>
										<Styled.MessageList>
											{joinedUsers.map((item: any, idx: number) => {
												return (
													<Styled.MessageListItem key={idx} onClick={(e: any) => onClickUser(e, item)} data-id={item.id}>
														<Styled.MessageListItemInner>
															<Styled.MessageListItemImg>
																<Avatar imageUrl={item.avatar} name={item.username || item.name} size='sm' />
															</Styled.MessageListItemImg>
															<Styled.MessageListItemContent>
																<Styled.MessageListItemName>
																	<span className='campaign-title'>{item.username}</span>
																</Styled.MessageListItemName>
															</Styled.MessageListItemContent>
														</Styled.MessageListItemInner>
													</Styled.MessageListItem>
												);
											})}
										</Styled.MessageList>
									</Styled.ListWrapper>
									{joinedUsers.length === 0 && (
										<Styled.NoUsersLabel>
											<span>{`Oh no! There is no users yet`}</span>
										</Styled.NoUsersLabel>
									)}
									{!displayAllJoined && joinedUsers.length > 6 && <Styled.SeeAllButton onClick={() => setDisplayAllJoined(true)}>+See all</Styled.SeeAllButton>}
								</Accordion>
							</div>
							<div css={{ position: 'relative' }}>
								<div css={{ position: 'absolute', right: 0, top: '-5px' }}>{renderDropdown(false)}</div>
								<Accordion title={`Not joined`} open={notJoinedUsers.length > 0}>
									<Styled.ListWrapper className={displayAllNotJoined ? 'scrollbar' : ''}>
										<Styled.MessageList>
											{notJoinedUsers.map((item: any) => {
												return (
													<Styled.MessageListItem key={item.id} onClick={(e: any) => onClickUser(e, item)} data-id={item.id}>
														<Styled.MessageListItemInner>
															<Styled.MessageListItemImg>
																<Avatar imageUrl={item.avatar} name={item.username || item.name} size='sm' />
															</Styled.MessageListItemImg>
															<Styled.MessageListItemContent>
																<Styled.MessageListItemName>
																	<span className='campaign-title'>{item.username || item.name}</span>
																</Styled.MessageListItemName>
															</Styled.MessageListItemContent>
														</Styled.MessageListItemInner>
													</Styled.MessageListItem>
												);
											})}
										</Styled.MessageList>
										{notJoinedUsers.length === 0 && (
											<Styled.NoUsersLabel>
												<span>{`Oh no! There is no users yet`}</span>
											</Styled.NoUsersLabel>
										)}
									</Styled.ListWrapper>
									{!displayAllNotJoined && notJoinedUsers.length > 6 && (
										<Styled.SeeAllButton onClick={() => setDisplayAllNotJoined(true)}>+See all</Styled.SeeAllButton>
									)}
								</Accordion>
							</div>
						</Styled.UserList>
					</Grid.Column>
					<Grid.Column lg={12} xl={8}>
						<Styled.InnerWrapper>
							{!selectedUsers.length && <StartView disableModal={false} selectedCampaign={null} />}

							{selectedUsers.length > 0 && (
								<>
									<Styled.NewMessageHeading>New message</Styled.NewMessageHeading>
									<Styled.ToWrapper>
										<Styled.ToLabel>To:</Styled.ToLabel>
										<div className='scrollbar'>
											{selectedUsers.map((item, idx) => {
												return (
													<Styled.ToBadge key={idx} style={{ animation: animation(idx) }}>
														<Avatar imageUrl={item.avatar} name={item.name || item.username} size='sm' />
														<span>{item.username || item.name}</span>
														{!disableRemoval && <Styled.badgeClose onClick={(e: React.MouseEvent<HTMLButtonElement>) => onClickRemoveUser(e, item)} />}
													</Styled.ToBadge>
												);
											})}
										</div>
									</Styled.ToWrapper>
									{/* Message window */}
									<Styled.MessageWrapper>
										{!loadingConversation && (
											<>
												<MessageList loggedInUser={props.loggedInUser} selectedUsers={selectedUsers} messages={messages} conversation={null} />
												<MessageField
													onPressEnter={(message) => onClickSend(message)}
													onClickSend={(message) => onClickSend(message)}
													selectedUsers={selectedUsers}
													disabled={disabledTextbox}
												/>
											</>
										)}

										{loadingConversation ? <LoadingSpinner /> : null}
									</Styled.MessageWrapper>
								</>
							)}
						</Styled.InnerWrapper>
					</Grid.Column>
				</Grid.Container>
			</Modal.Body>
		</Modal>
	);
};

export default NewMessageModal;
