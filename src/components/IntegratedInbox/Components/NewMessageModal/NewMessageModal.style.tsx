import { Modal as AntdModal } from 'antd';
import styled from 'styled-components';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';
import MessageField from '../MessageWindow/Components/MessageField/MessageField.styles';
import Accordion from 'components/Accordion/Accordion.style';

const Modal = styled(AntdModal)`
	font-family: ${typography.BaseFontFamiliy};
	border: 1px solid #888888;
	padding: 0;
	border-radius: 5px;
	top: 100px;
`;

const BackButton = styled.span`
	cursor: pointer;
	display: flex;
	align-items: center;
	font-size: 0.7em;

	& > .icon {
		margin-left: -6px;
	}
`;

const InnerWrapper = styled.div`
	height: 573px;
	position: relative;
	border-left: 1px solid #888888;
	padding-left: 0.5rem;
`;

const MessageWrapper = styled.div`
	position: relative;

	height: calc(100% - 122px);

	${MessageField.MessageContainer} {
		bottom: -20px;
		left: 8px;
		width: calc(100% - 8px);
	}

	.cb-spinner {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translateX(-50%, -50%);
	}
`;

const MessageListItemContent = styled.div`
	display: flex;
	width: 100%;
	flex-grow: 1;
	align-items: center;
	height: 100%;
	overflow: hidden;
`;

const MessageList = styled.ul`
	margin: 0;
	padding: 0;
`;

const MessageListItem = styled.li`
	display: flex;
	align-items: center;
	align-items: center;
	position: relative;
	padding: 0.5rem;
	z-index: 1;
	transition: background-color 0.2s ease-in-out;

	&:hover {
		background-color: #e8f2fa;
	}
`;

const MessageListItemInner = styled.div`
	position: relative;
	display: flex;
	align-items: center;
	height: 100%;
	width: 100%;
	cursor: pointer;
	z-index: 1010;
`;

const MessageListItemImg = styled.div`
	display: flex;
	align-items: center;
	position: relative;
	margin-right: 8px;
	text-align: center;

	.avatar {
		margin-left: 0;
	}
`;

const MessageListItemName = styled.div`
	.campaign-title {
		display: block;
		font-size: 0.875rem;
		font-weight: 700;
		white-space: nowrap;
		max-width: 200px;
		overflow: hidden;
		text-overflow: ellipsis;
		padding-top: 2px;
	}
`;

const SelectedUserName = styled.span`
	padding: 0 5px;
	border-radius: 5px;
	background-color: ${colors.gray2};
	cursor: pointer;
`;

const NewMessageHeading = styled.h2`
	font-size: 1.2rem;
	font-weight: 700;
`;

const ToWrapper = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 16px;
	height: 42px;
	transition: transform 0.2s ease-in-out;
	gap: 8px;

	.scrollbar {
		display: flex;
		align-items: center;

		gap: 4px;
		overflow: hidden;
		overflow-x: auto;
		overflow-y: hidden;
		height: 50px;
	}
`;

const ToLabel = styled.span`
	color: #888;
	padding-top: 0.125rem;
	padding-right: 0.125rem;
`;

const ToBadge = styled.div`
	display: flex;
	align-items: center;
	background-color: #f2f2f2;
	color: #888;
	padding: 0.25rem 0.5rem;
	border-radius: 2px;
	border: 1px solid #333;
	max-height: 2rem;
	transform: scale(0);

	.avatar {
		margin-left: 0;
		margin-right: 8px;
	}

	span {
		padding-top: 0.1875rem;
	}
`;

const badgeClose = styled.button`
	background: transparent
		url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23888'%3e%3cpath d='M.293.293a1 1 0 011.414 0L8 6.586 14.293.293a1 1 0 111.414 1.414L9.414 8l6.293 6.293a1 1 0 01-1.414 1.414L8 9.414l-6.293 6.293a1 1 0 01-1.414-1.414L6.586 8 .293 1.707a1 1 0 010-1.414z'/%3e%3c/svg%3e")
		center/0.5rem auto no-repeat;
	height: 0.5rem;
	width: 0.5rem;
	cursor: pointer;
	margin-left: 4px;
	border: none;
	margin-top: 1px;
`;

const UserList = styled.div`
	padding-top: 2.5rem;

	${Accordion.Wrapper} {
		padding-bottom: 0.9375rem;
	}

	${Accordion.Header} {
		font-size: 0.875rem;
		margin-bottom: 0.125rem;
	}

	${Accordion.ToggleIcon} {
		margin: 0;
	}
`;

const SeeAllButton = styled.div`
	font-family: ${typography.button.fontFamiliy};
	background: transparent;
	border: none;
	color: #3a71e3;
	font-size: 0.875rem;
	cursor: pointer;
`;

const ListWrapper = styled.div`
	max-height: 240px;
	overflow: hidden;
`;

const NoUsersLabel = styled.div`
	span {
		font-size: 0.875rem;
	}
`;

const Styled = {
	Modal,
	BackButton,
	InnerWrapper,
	MessageWrapper,
	MessageList,
	MessageListItemContent,
	MessageListItem,
	MessageListItemInner,
	MessageListItemImg,
	MessageListItemName,
	SelectedUserName,
	NewMessageHeading,
	ToWrapper,
	ToLabel,
	ToBadge,
	badgeClose,
	UserList,
	SeeAllButton,
	ListWrapper,
	NoUsersLabel
};

export default Styled;
