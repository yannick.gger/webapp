import styled from 'styled-components';
import colors from 'styles/variables/colors';
import chevronDownSmall from 'assets/icons/icon-chevron-down-small.svg';
import Button from 'components/Button/Button.style';

const theme = colors.button;

const Wrapper = styled.div`
	font-family: 'Epilogue', sans-serif;
	height: 633px;
	color: ${colors.iIListColor};
	position: relative;
	max-width: 500px;
	width: 100%;
	float: right;
	padding-right: 0.5rem;
`;

const ConversationsListItemContent = styled.div`
	display: flex;
	height: 100%;
	flex-grow: 1;
	border-bottom: 1px solid ${colors.iIListItemContentBorderColor};
	min-height: 80px;
	height: 100%;
	padding: 0.9375rem 0;
	opacity: 1;
	overflow: hidden;
	transition: opacity 0.4s ease-in-out;
	align-items: center;
`;

const ConversationsListItem = styled.li`
	position: relative;
	height: 80px;
	z-index: 1;
	transition: background-color ease-in-out 0.2s, border-color ease-in-out 0.2s;

	&:hover:not(.active-item):not(.selected-user) {
		background-color: ${colors.iIConversationsListItemHoverBackground};

		${ConversationsListItemContent} {
			border-color: ${colors.iIConversationsListItemHoverBackground};
		}
	}

	&.selected-user {
		background-color: ${colors.iIConversationsListItemActiveBackground};

		${ConversationsListItemContent} {
			border-color: ${colors.iIConversationsListItemActiveBackground};
		}
	}
`;

const ConversationsListItemInner = styled.div`
	position: relative;
	display: flex;
	height: 100%;
	cursor: pointer;
	padding: 0 0.5rem;
	z-index: 1010;
`;

const ConversationsListItemImg = styled.div`
	display: flex;
	align-items: center;
	position: relative;
	margin-right: 8px;
	padding: 0.9375rem 0;
	text-align: center;

	.avatar {
		height: 48px;
		width: 48px;
		transition: opacity 0.1s ease-in-out, transform 0.1s ease-in-out;

		figcaption {
			padding-top: 3px;
		}
	}
`;

const MessagesList = styled.ul`
	list-style: none;
	padding: 0;
	margin: 0;
	height: 633px;
	overflow-y: auto;
	width: 100%;
	transition: left 0.2s ease-in;
`;

const ConversationsListItemName = styled.div`
	.campaign-title {
		display: block;
		font-size: 1.1875rem;
		font-weight: 700;
		padding-top: 0.25rem;
		white-space: nowrap;
		max-width: 280px;
		overflow: hidden;
		text-overflow: ellipsis;
	}
`;

const ConversationsListItemActiveConversations = styled.div`
	.label {
		font-size: 14px;
	}
`;

const ConversationsListItemDate = styled.div`
	font-family: 'Space Mono', monospace;
	margin-left: auto;
`;

const ToggleBody = styled.div`
	height: 0;
	overflow: hidden;
	transition: height 0.2s ease-in-out;
`;

const TogglePanel = styled.div`
	position: relative;
	z-index: 1;

	&.open {
		.toggle {
			transform: rotate(180deg);
		}

		${ToggleBody} {
			height: 50px;
		}
	}
`;

const TogglePanelTitle = styled.div`
	font-size: 14px;
	display: flex;
	align-items: center;
	z-index: 1;

	.toggle {
		display: inline-block;
		background: url(${chevronDownSmall}) no-repeat top left;
		width: 16px;
		height: 16px;
		margin-left: 4px;
		transition: transform 0.05s ease-in-out;
	}
`;

const ConversationList = styled(MessagesList)`
	opacity: 0;
	transition: opacity 0.2s ease-in-out;

	-webkit-animation: slide-left 0.5s;
	animation: slide-left 0.5s;

	&.selected {
		opacity: 1;
	}

	@-webkit-keyframes slide-left {
		0% {
			-webkit-transform: translateX(100px);
			transform: translateX(100px);
		}
		100% {
			-webkit-transform: translateX(0);
			transform: translateX(0);
		}
	}
	@keyframes slide-left {
		0% {
			-webkit-transform: translateX(100px);
			transform: translateX(100px);
		}
		100% {
			-webkit-transform: translateX(0);
			transform: translateX(0);
		}
	}
`;

const Types = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	font-family: 'Space Mono', monospace;
	margin-bottom: 8px;
	opacity: 0;
	visibility: hidden;
	transition: opacity 0.2s ease-in-out;
	margin-left: auto;
	max-width: 500px;

	&.visible {
		opacity: 1;
		visibility: visible;
	}

	button {
		position: relative;
		background-color: transparent;
		font-size: 1rem;
		border: none;
		color: #333;
		cursor: pointer;
		width: 124px;
		height: 24px;
		z-index: 1;
	}
`;

const TypesInner = styled.div`
	--decoration-left: 0%;

	position: relative;
	z-index: 1;

	&:after {
		position: absolute;
		width: 124px;
		height: 24px;
		border: 2px solid #333;
		content: '';
		display: block;
		top: 2px;
		left: var(--decoration-left);
		transition: left 0.2s ease-out;
		z-index: 1;
	}
`;

const NewMessageCircle = styled.div`
	width: 16px;
	height: 16px;
	border: 1px solid ${colors.white};
	position: absolute;
	top: 9px;
	right: 0px;
	background-color: #3972e4;
	border-radius: 20px;
`;

const EmptyListText = styled.h3`
	display: block;
	width: 100%;
	text-align: center;
	margin-top: 0.5rem;
	font-size: 1rem;
`;

const NewMessageButton = styled.button`
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: transparent;
	border: none;
	display: block;
	margin-left: auto;
	cursor: pointer;
	opacity: 0;
	transition: opacity 0.2s ease-in-out, background-color 0.1s ease-in-out, fill 0.1s ease;
	padding: 0.25rem;

	i {
		display: flex;
		align-items: center;
		justify-content: center;
	}

	&:hover:not(:disabled) {
		background-color: #333;

		path {
			fill: #fff;
		}
	}

	&.visible {
		opacity: 1;
	}

	&:disabled {
		cursor: default;
		path {
			fill: ${theme.primary.disabled.color};
		}
	}
`;

const CampaignListWrapper = styled.div`
	position: relative;
	width: 100%;

	.cb-spinner {
		position: absolute;
		top: 34px;
		left: 50%;
		transform: translateX(-50%);
	}

	&.minimized {
		position: absolute;
		left: -94px;

		${MessagesList} {
			height: 270px;
			width: 85px;
			overflow: hidden;
			padding-top: 8px;

			&.scrollbar {
				height: 633px;
			}

			.avatar {
				width: 48px;
				height: 48px;
			}

			> :not(.active-item):not(:hover) {
				.avatar {
					opacity: 0.7;
				}
			}

			.active-item {
				.avatar {
					transform: scale(1.3616);
				}
			}

			${ConversationsListItem} {
				height: 64px;
				min-height: 64px;
				&:hover {
					background-color: transparent;
				}
			}

			${ConversationsListItemContent} {
				opacity: 0;
				visibility: hidden;
				height: 0;
				min-height: 0;
				padding: 0;
			}

			${ConversationsListItemImg} {
				margin-right: 0;
			}

			${ConversationsListItemInner} {
				display: block;
				justify-content: center;

				&:hover {
					background-color: transparent;
				}
			}
		}
	}

	${Button.SecondaryButton} {
		&,
		&:hover {
			background: transparent;
			border-color: transparent;
		}
	}
`;

const ConversationListWrapper = styled.div`
	position: relative;
	width: 100%;

	.cb-spinner {
		position: absolute;
		top: 34px;
		left: 50%;
		transform: translateX(-50%);
	}
`;

const Styled = {
	Wrapper,
	MessagesList,
	ConversationsListItem,
	ConversationsListItemInner,
	ConversationsListItemImg,
	ConversationsListItemName,
	ConversationsListItemDate,
	ConversationsListItemContent,
	ConversationsListItemActiveConversations,
	ConversationList,
	TogglePanel,
	TogglePanelTitle,
	ToggleBody,
	Types,
	TypesInner,
	NewMessageCircle,
	EmptyListText,
	NewMessageButton,
	CampaignListWrapper,
	ConversationListWrapper
};

export default Styled;
