import { useEffect, useRef, useState } from 'react';
import { IConversationsList } from './types';
import Styled from './ConversationsList.style';
import classNames from 'classnames';
import Avatar from 'components/Avatar';
import Icon from 'components/Icon';
import { SecondaryButton } from 'components/Button';
import { Campaign, Conversation, IInfluencerConversation } from 'components/IntegratedInbox/types';
import LoadingSpinner from 'components/LoadingSpinner';

/**
 * ConversationsList
 * @param {IConversationsList} props
 * @returns JSX.Element
 */
const ConversationsList = (props: IConversationsList) => {
	const [conversationsList, setConversationsList] = useState<Array<Conversation>>();
	const [isActiveCampaign, setActiveCampaign] = useState<number>(-1);
	const [isActiveConversation, setActiveConversation] = useState<Conversation>();
	const [isCampaignSelected, setIsCampaignSelected] = useState<boolean>(false);
	const [listType, setListType] = useState<number>(1);
	const [scrollible, setScrollible] = useState<boolean>(true);
	const conversationListRef = useRef<HTMLUListElement>(null);

	useEffect(() => {
		const filtered = props.conversations.filter((x: Conversation) => x.conversationId !== '');
		setConversationsList(filtered);
	}, [props.conversations]);

	useEffect(() => {
		if (isActiveCampaign && conversationListRef.current !== null) {
			conversationListRef.current.scrollTop = 0;
		}
	}, [isActiveCampaign]);

	useEffect(() => {
		if (props.activeConversation) {
			setActiveConversation(props.activeConversation);
		}
	}, [props.activeConversation]);

	useEffect(() => {
		if (props.context.activeCampaign) {
			const campaign = props.context.activeCampaign;

			if (campaign.id) {
				setIsCampaignSelected(true);
				setActiveCampaign(campaign.id);
				setScrollible(false);
			}
		}
	}, [props.context.activeCampaign]);

	const onClickListItem = (campaign: Campaign): void => {
		if (props.conversationLoading) return;

		if (campaign.id === isActiveCampaign) {
			setActiveCampaign(-1);
			setActiveConversation(undefined);
			setIsCampaignSelected(false);
			setScrollible(true);
		} else {
			setIsCampaignSelected(true);
			setActiveCampaign(campaign.id);
			setScrollible(false);
			props.onClickCampaign && props.onClickCampaign(campaign);
		}
	};

	const onClickConversationItem = (conversation: Conversation): void => {
		if (props.userListLoading) return;
		props.onClickConversation && props.onClickConversation(conversation);
		setActiveConversation(conversation);
	};

	const onClickMessageType = (e: React.MouseEvent<HTMLButtonElement>, type: number): void => {
		const currentItem = e.target as HTMLElement;
		if (currentItem !== null && currentItem.parentElement) {
			currentItem.parentElement.style.setProperty('--decoration-left', `${currentItem.offsetLeft}px`);
			setListType(type);
		}
	};

	const renderCampaigns = (): JSX.Element => {
		return (
			<Styled.CampaignListWrapper className={isActiveCampaign !== -1 ? 'minimized' : ''}>
				{props.campaigns.length > 0 && (
					<Styled.MessagesList className={scrollible ? 'scrollbar' : ''} ref={conversationListRef}>
						{props.campaigns.map((item: Campaign) => {
							return (
								<Styled.ConversationsListItem key={item.id} onClick={() => onClickListItem(item)} className={isActiveCampaign === item.id ? 'active-item' : ''}>
									<Styled.ConversationsListItemInner>
										<Styled.ConversationsListItemImg>
											<Avatar imageUrl={item.coverPhoto.mediumCoverPhoto} name={item.name} size='lg' />
											{item.unSeen && <Styled.NewMessageCircle title={`You have new unread messages`} />}
										</Styled.ConversationsListItemImg>
										<Styled.ConversationsListItemContent>
											<Styled.ConversationsListItemName>
												<span className='campaign-title'>{item.name}</span>
											</Styled.ConversationsListItemName>
											<Styled.ConversationsListItemDate>
												<Styled.ConversationsListItemDate>
													<span>{item.date}</span>
												</Styled.ConversationsListItemDate>
											</Styled.ConversationsListItemDate>
										</Styled.ConversationsListItemContent>
									</Styled.ConversationsListItemInner>
								</Styled.ConversationsListItem>
							);
						})}
					</Styled.MessagesList>
				)}
				{!scrollible && isActiveCampaign !== -1 && props.campaigns.length > 3 && (
					<SecondaryButton
						size='sm'
						onClick={() => {
							setScrollible(true);
						}}
					>
						See all
					</SecondaryButton>
				)}
				{props.loading && isActiveCampaign === -1 && <LoadingSpinner />}
			</Styled.CampaignListWrapper>
		);
	};

	const renderConversationList = (): JSX.Element => {
		if (listType === 2) return renderBlastsList();
		return (
			<Styled.ConversationListWrapper>
				<Styled.ConversationList
					className={classNames('conversations-list', {
						selected: isActiveCampaign !== -1
					})}
				>
					{!props.conversationLoading && (
						<>
							{conversationsList &&
								conversationsList.map((item: Conversation, index: number) => {
									return (
										<Styled.ConversationsListItem
											className={isActiveConversation !== undefined && isActiveConversation.conversationId === item.conversationId ? 'selected-user' : ''}
											key={index}
											onClick={() => onClickConversationItem(item)}
											data-id={item.conversationId}
										>
											<Styled.ConversationsListItemInner>
												<Styled.ConversationsListItemImg>
													<Avatar imageUrl={item.influencer.profilePictureUrl} name={item.influencer.username} size='sm' />
													{item.unSeen ? <Styled.NewMessageCircle title={`You have new unread messages`} /> : null}
												</Styled.ConversationsListItemImg>
												<Styled.ConversationsListItemContent>
													<Styled.ConversationsListItemName>
														<span className='campaign-title'>{item.influencer.username}</span>
													</Styled.ConversationsListItemName>
													<Styled.ConversationsListItemDate>
														<span>{item.date}</span>
													</Styled.ConversationsListItemDate>
												</Styled.ConversationsListItemContent>
											</Styled.ConversationsListItemInner>
										</Styled.ConversationsListItem>
									);
								})}
						</>
					)}
					{!props.conversationLoading && (conversationsList && conversationsList.length < 1) && (
						<Styled.EmptyListText>You have no conversations</Styled.EmptyListText>
					)}
				</Styled.ConversationList>
				{props.conversationLoading && isActiveCampaign > -1 && <LoadingSpinner />}
			</Styled.ConversationListWrapper>
		);
	};

	const renderBlastsList = (): JSX.Element => {
		if (!conversationsList) return <></>;

		const filtered = conversationsList.filter((conversation: Conversation) => conversation.blast === true);
		if (!filtered.length) return <Styled.EmptyListText>There is no blasts in this campaign yet!</Styled.EmptyListText>;

		return (
			<Styled.ConversationListWrapper>
				<Styled.ConversationList
					className={classNames('conversations-list', {
						selected: isActiveCampaign !== -1
					})}
				>
					{filtered.map((item: any) => {
						return (
							<Styled.ConversationsListItem
								className={isActiveConversation !== undefined && isActiveConversation.conversationId === item.conversationId ? 'selected-user' : ''}
								key={item.id}
								onClick={() => onClickListItem(item)}
								data-id={item.id}
							>
								<Styled.ConversationsListItemInner>
									<Styled.ConversationsListItemImg>
										<Avatar imageUrl={item.avatar} name={item.name} />
									</Styled.ConversationsListItemImg>
									<Styled.ConversationsListItemContent>
										<Styled.ConversationsListItemName>
											<span className='campaign-title'>{item.name}</span>
										</Styled.ConversationsListItemName>
										{item.latestMessage && item.latestMessage.date ? (
											<Styled.ConversationsListItemDate>
												<span>{item.latestMessage.date}</span>
											</Styled.ConversationsListItemDate>
										) : null}
									</Styled.ConversationsListItemContent>
								</Styled.ConversationsListItemInner>
							</Styled.ConversationsListItem>
						);
					})}
					{!props.loading && props.conversations.length < 1 && <Styled.EmptyListText>You have no conversations</Styled.EmptyListText>}
					{props.loading && isActiveCampaign > -1 && <LoadingSpinner />}
				</Styled.ConversationList>
			</Styled.ConversationListWrapper>
		);
	};

	return (
		<>
			<Styled.NewMessageButton
				onClick={() => props.onClickOpenModal && props.onClickOpenModal()}
				className={isCampaignSelected ? 'visible' : ''}
				disabled={props.disableModal}
			>
				<Icon icon='new-message' size='32' />
			</Styled.NewMessageButton>
			<div className='clearfix'>
				<Styled.Types className={isActiveCampaign !== -1 ? 'visible' : ''}>
					<Styled.TypesInner style={{ opacity: 0, visibility: 'hidden' }}>
						<button className={listType === 1 ? 'selected' : ''} onClick={(e) => onClickMessageType(e, 1)}>
							Messages
						</button>
						<button className={listType === 2 ? 'selected' : ''} onClick={(e) => onClickMessageType(e, 2)}>
							Blasts
						</button>
					</Styled.TypesInner>
				</Styled.Types>
				<Styled.Wrapper className={isActiveCampaign !== -1 ? 'd-flex' : ''}>
					{renderCampaigns()}
					{isCampaignSelected ? renderConversationList() : null}
				</Styled.Wrapper>
			</div>
		</>
	);
};

export default ConversationsList;
