import { Campaign, Conversation, IInfluencerConversation } from 'components/IntegratedInbox/types';

export interface IConversationsList {
	campaigns: Array<Campaign>;
	conversations: Array<Conversation>;
	activeConversation?: Conversation;
	loading?: boolean;
	conversationLoading?: boolean;
	userListLoading?: boolean;
	onClickCampaign?: (campagin: Campaign) => void;
	onClickConversation?: (conversation: Conversation) => void;
	isModalOpen?: boolean;
	onClickOpenModal?: () => void;
	onClickCloseModal?: () => void;
	disableModal: boolean;
	context: any;
}
