import { Campaign, Conversation } from 'components/IntegratedInbox/types';
import { ICampaign, ICampaignUser } from 'services/Campaign/types';

export interface IMessageWindow {
	selectedConversatíon: Conversation | null;
	selectedCampaign: Campaign | null;
	messages?: any;
	loadingMessages: boolean;
	onClickSendMessage: (convsersationId: string, message: string) => void;
	onClickOpenModal: () => void;
	loggedInUser: any;
	disableModal: boolean;
}
