import classNames from 'classnames';
import Icon from 'components/Icon';
import { useState } from 'react';
import Styled from './MessageField.styles';
import { IMessageField } from './types';

/**
 * MessageField
 * @todo i18next
 * @param {IMessageField} props
 * @returns {JSX.Element}
 */
const MessageField = (props: IMessageField) => {
	const [message, setMessage] = useState('');
	const [focused, setFocused] = useState(false);

	const onChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
		const value = e.currentTarget.value;
		props.onChange && props.onChange(value);
		setMessage(value);
	};

	const onKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
		if (e.key === 'Enter' && !e.shiftKey) {
			e.preventDefault();
			props.onPressEnter && props.onPressEnter(message);
			setMessage('');
			return false;
		}
		return true;
	};

	const onClickSend = () => {
		props.onClickSend && props.onClickSend(message);
		setMessage('');
	};

	const notifyText = () => {
		if (props.selectedUsers.length < 2) {
			return `${props.selectedUsers.length} person will be notified`;
		}

		return `${props.selectedUsers.length} people will be notified`;
	};

	const placeHolder = () => {
		const selectedUsers = props.selectedUsers;
		const firstTwoUsers = selectedUsers.slice(0, 2);

		if (selectedUsers.length > 2) {
			return 'Message all people';
		}
		const text = `Message ${firstTwoUsers.map((a) => a.username).join(' and ')}`;

		return text.slice(0, 40).concat('...');
	};

	const onFocus = () => setFocused(true);
	const onBlur = () => setFocused(false);

	return (
		<Styled.MessageContainer>
			<Styled.MessageTextContainer className={focused ? 'focused' : ''}>
				<Styled.MessageTextarea
					value={message}
					placeholder={placeHolder()}
					onChange={onChange}
					onKeyPress={onKeyPress}
					disabled={props.disabled}
					className='scrollbar'
					onFocus={onFocus}
					onBlur={onBlur}
				/>
			</Styled.MessageTextContainer>
			<Styled.MessageTextBar>
				<Styled.SendMessageButtonContainer>
					<Styled.NotifyText>{notifyText()}</Styled.NotifyText>
					<Styled.SendMessageButton onClick={onClickSend} disabled={props.disabled}>
						<Icon icon='send' />
					</Styled.SendMessageButton>
				</Styled.SendMessageButtonContainer>
			</Styled.MessageTextBar>
		</Styled.MessageContainer>
	);
};

export default MessageField;
