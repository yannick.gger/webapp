import { render } from '@testing-library/react';
import MessageField from './MessageField';

const mockedProps = {
	inputReference: {},
	username: 'test_user'
};

describe('Integrated Inbox - Message Field', () => {
	it('renders the component', () => {
		const topbar = render(<MessageField selectedUsers={[]} {...mockedProps} />);
		expect(topbar).toMatchSnapshot();
	});
});
