export interface IMessageField {
	selectedUsers: Array<any>
	onChange?: (value: string) => void;
	onPressEnter?: (message: string) => void;
	onClickSend?: (message: string) => void;
	disabled?: boolean;
}
