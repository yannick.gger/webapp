import styled from 'styled-components';
import typography from 'styles/variables/typography';

const MessageContainer = styled.div`
	position: absolute;
	bottom: 20px;
	left: 2rem;
	width: calc(100% - 4rem);
`;

const MessageTextContainer = styled.div`
	height: 116px;
	padding-bottom: 2.8125rem;
	border: 1px solid #e2e2e2;
	transition: box-shadow 0.2s ease-in-out;

	&.focused {
		box-shadow: 0px 0px 6px #4766aa;
	}
`;

const MessageTextarea = styled.textarea`
	width: 100%;
	height: calc(104px - 30px);
	padding: 1rem 1.125rem;
	padding-bottom: 0;
	margin: 0;
	resize: none;
	border-radius: 0;
	border: none;

	&:focus-visible {
		outline: none;
	}
`;

const MessageTextBar = styled.div`
	position: absolute;
	display: flex;
	align-items: center;
	bottom: 9px;
	left: 0;
	width: 100%;
	height: 30px;
	padding: 0 1rem;
`;

const SendMessageButtonContainer = styled.div`
	display: flex;
	align-items: center;
	margin-left: auto;
`;

const SendMessageButton = styled.button`
	display: flex;
	background-color: transparent;
	align-items: center;
	border: none;

	&:not(:disabled) {
		cursor: pointer;
	}
`;

const NotifyText = styled.span`
	font-family: ${typography.BaseFontFamiliy};
	font-size: 0.875rem;
	text-align: right;
	color: #888;
`;

const Styled = {
	MessageContainer,
	MessageTextContainer,
	MessageTextarea,
	MessageTextBar,
	SendMessageButtonContainer,
	SendMessageButton,
	NotifyText
};

export default Styled;
