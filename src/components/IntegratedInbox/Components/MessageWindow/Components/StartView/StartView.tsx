import { Button } from 'components/Button';
import Styled from './StartView.style';
import { IStartView } from './types';
import tail from 'assets/img/influencers/tail.png';
import blondie from 'assets/img/influencers/blond.png';
import guy from 'assets/img/influencers/guy.png';
import plus from 'assets/img/figures/plus-circle.png';
import Icon from 'components/Icon';

/**
 * StartView for the message window
 * @todo i18next
 * @param {IStartView} props
 * @returns {JSX.Element}
 */
const StartView = (props: IStartView) => {
	const { selectedCampaign, onClickOpenModal, disableModal } = props;

	return (
		<Styled.NewMessageWrapper>
			<Styled.NewMessageInner>
				<Styled.FigureWrapper>
					<img src={blondie} className='model' />
					<img src={tail} className='front-model' />
					<img src={guy} className='model' />
					<img src={plus} className='plus' />
				</Styled.FigureWrapper>
				<Styled.NewMessageHeading>It’s nice to chat with someone</Styled.NewMessageHeading>
				<Styled.NewMessageText>Get started by selecting a campaign and influencer from the left menu or start a new conversation.</Styled.NewMessageText>
				{selectedCampaign && selectedCampaign.id && (
					<Button onClick={() => onClickOpenModal && onClickOpenModal()} disabled={disableModal}>
						<Icon icon='new-message' />
						<span>New message</span>
					</Button>
				)}
			</Styled.NewMessageInner>
		</Styled.NewMessageWrapper>
	);
};

export default StartView;
