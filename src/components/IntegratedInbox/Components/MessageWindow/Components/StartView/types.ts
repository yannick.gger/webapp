import { Campaign } from 'components/IntegratedInbox/types';
import { ICampaign } from 'services/Campaign/types';

export interface IStartView {
	selectedCampaign: Campaign | null;
	onClickOpenModal?: () => void;
	disableModal: boolean;
}
