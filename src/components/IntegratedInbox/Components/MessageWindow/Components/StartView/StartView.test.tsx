import { render } from '@testing-library/react';
import StartView from './StartView';

const mockedProps = {
	selectedCampaign: {
		id: 1,
		name: 'Mocked campaign',
		coverPhotoUrl: ''
	}
};

describe('Integrated Inbox - Start view', () => {
	it('renders the component', () => {
		const startView = render(<StartView {...mockedProps} onClickOpenModal={() => {}} />);
		expect(startView).toMatchSnapshot();
	});
});
