import styled from 'styled-components';

const NewMessageWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100%;
	width: 100%;
`;

const NewMessageInner = styled.div`
	text-align: center;
	max-width: 380px;

	button {
		margin: auto;
	}
`;

const NewMessageHeading = styled.h3`
	font-size: 1.2rem;
	font-weight: 700;
	margin-bottom: 18px;
`;

const NewMessageText = styled.p`
	font-size: 1rem;
	line-height: 1.5;
	margin-bottom: 20px;
`;

const FigureWrapper = styled.div`
	position: relative;
	margin-bottom: 40px;
	height: 164px;

	.model {
		margin: 0px 46px;
	}

	.front-model {
		position: absolute;
		left: 50%;
		top: 22px;
		width: 8.75rem;
		transform: translateX(-50%);
	}

	.plus {
		position: absolute;
		right: 100px;
		bottom: 8px;
	}
`;

const circleFigure = styled.div`
	width: 2.75rem;
	height: 2.75rem;
`;

const Styled = {
	NewMessageWrapper,
	NewMessageInner,
	NewMessageHeading,
	NewMessageText,
	FigureWrapper,
	circleFigure
};

export default Styled;
