import { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import { IMessageList } from './types';
import Styled from './MessageList.style';
import Avatar from 'components/Avatar';

/**
 * MessageList
 * @todo i18next
 * @param {IMessageList} props
 * @returns {JSX.Element}
 */
const MessageList = (props: IMessageList) => {
	const messagesEndRef = useRef<HTMLDivElement>(null);
	const [loaded, setLoaded] = useState<boolean>(false);
	const [display, setDisplay] = useState<boolean>(false);
	const [scrolled, setScrolled] = useState<boolean>(false);

	const formatNameWithOrg = (user: any): string => {
		if (!user) return '';
		const firstName = user.name ? user.name.split(' ').shift() : '';
		const org = user.organization ? user.organization.name : '';

		return `${firstName}, ${org}`;
	};

	useEffect(() => {
		setLoaded(true);
		const firstItem = document.querySelectorAll('.unread')[0]; // Scroll to first unread message
		if (firstItem) {
			firstItem.scrollIntoView({ behavior: 'auto' });
		} else {
			messagesEndRef.current && messagesEndRef.current.scrollIntoView({ behavior: 'auto' });
		}
		setScrolled(true);
	}, []);

	useEffect(() => {
		if (loaded) {
			messagesEndRef.current && messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
		}
	}, [props.messages]);

	useEffect(() => {
		// Wait until we have scrolled down to the bottom and set Display to true
		if (scrolled) {
			setTimeout(() => {
				setDisplay(true);
			}, 100);
		}
	}, [scrolled]);

	return (
		<Styled.MessageListWrapper className={display ? 'visible' : ''}>
			<Styled.MessageList role='grid'>
				{props.selectedUsers.length > 0 && props.conversation ? (
					<Styled.MessageListInfo role='row'>
						<Styled.StartDate>{props.conversation && props.conversation.createdAt}</Styled.StartDate>
						<Styled.StartConversationText>This is the start of your conversation</Styled.StartConversationText>
					</Styled.MessageListInfo>
				) : null}
				{props.messages.length > 0 &&
					props.messages.map((item: any) => {
						return (
							<Styled.MessageListItem
								key={item.id}
								className={classNames('', {
									reverse: item.conversationStarter,
									unread: item.readAt === null && item.user.id !== props.loggedInUser.id && item.isConversation
								})}
								role='row'
							>
								<Styled.MessageListItemDate>
									<span>
										{item.createdAt} {!item.user.isInfluencer ? `${formatNameWithOrg(item.user)}` : ''}
									</span>
								</Styled.MessageListItemDate>
								<Styled.MessageListItemInner>
									<Avatar imageUrl={item.user.avatar} name={item.user.username || item.user.name} />
									<Styled.MessageListItemContent>
										<Styled.MessageListItemText>
											<span>{item.message}</span>
										</Styled.MessageListItemText>
									</Styled.MessageListItemContent>
								</Styled.MessageListItemInner>
							</Styled.MessageListItem>
						);
					})}
				<div ref={messagesEndRef} />
			</Styled.MessageList>
		</Styled.MessageListWrapper>
	);
};

export default MessageList;
