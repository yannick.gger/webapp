import { Conversation } from 'components/IntegratedInbox/types';

export interface IMessageList {
	selectedUsers: any;
	messages: any;
	loggedInUser: any;
	conversation: Conversation | null;
}
