import styled from 'styled-components';

const MessageListWrapper = styled.div`
	height: calc(100% - 116px);
	overflow-y: auto;
	overflow-x: hidden;
	padding-right: 0.9375rem;
	padding-bottom: 0.9375rem;
	opacity: 0;

	&.visible {
		opacity: 1;
	}

	/* total width */
	&::-webkit-scrollbar {
		background-color: transparent;
		width: 8px;
	}

	/* background of the scrollbar except button or resizer */
	&::-webkit-scrollbar-track {
		background-color: transparent;
	}

	/* scrollbar itself */
	&::-webkit-scrollbar-thumb {
		background-color: #babac0;
		border-radius: 16px;
		border: 4px solid transparent;
	}

	/* set button(top and bottom of the scrollbar) */
	&::-webkit-scrollbar-button {
		display: none;
	}
`;

const MessageList = styled.div``;

const MessageListInfo = styled.div`
	text-align: center;
	margin-bottom: 30px;
`;

const MessageListItemInner = styled.div`
	display: flex;
`;

const MessageListItemDate = styled.div`
	font-family: 'Space Mono', monospace;
	font-size: 0.875rem;
	line-height: 1.93;
	text-align: left;
	color: #888;
	text-align: center;
	margin-bottom: 8px;
`;

const MessageListItemContent = styled.div`
	margin: 3px 0 0 12px;
`;

const MessageListItemText = styled.div`
	padding: 1rem 1rem 0.6875rem;
	border-radius: 2px;
	background-color: #f2f2f2;
`;

const MessageListItem = styled.div`
	&:not(:last-child) {
		margin-bottom: 30px;
	}

	&.reverse {
		${MessageListItemInner} {
			flex-direction: row-reverse;
		}

		${MessageListItemText} {
			background-color: #f6f6ed;
		}
	}
`;

const StartDate = styled.span`
	display: block;
	color: #c0c0c0;
`;

const StartConversationText = styled.span`
	display: block;
	color: #c0c0c0;
`;

const FallbackAvatar = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 48px;
	height: 48px;
	margin: 2px 0 0 11px;
	padding: 12px 9px;
	border: solid 1px #333;
	background-color: #e9fcee;
	border-radius: 50px;

	> span {
		font-size: 1rem;
		font-weight: 700;
		line-height: 1.69;
		height: 100%;
		text-align: center;
		color: #333;
	}
`;

const Styled = {
	MessageListWrapper,
	MessageListInfo,
	MessageListItem,
	MessageListItemInner,
	MessageListItemDate,
	MessageListItemContent,
	MessageListItemText,
	MessageList,
	StartDate,
	StartConversationText,
	FallbackAvatar
};

export default Styled;
