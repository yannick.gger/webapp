import Avatar from 'components/Avatar';
import Styled from './Topbar.styles';
import { ITopbar } from './types';

/**
 * Message window topbar
 * @todo i18next
 * @param {ITopbar} props
 * @returns {JSX.Element}
 */
const Topbar = (props: ITopbar) => {
	const { selectedUser, selectedCampaign } = props;

	return (
		<Styled.TopBar>
			{selectedUser ? (
				<>
					<Styled.TopBarUser>
						<Styled.TopBarImage>
							<Avatar imageUrl={selectedUser.avatar} name={selectedUser.username || selectedUser.name} size='sm' />
						</Styled.TopBarImage>
						<Styled.TopBarReceiver>
							<span>To:</span> <span>{selectedUser.username || selectedUser.name}</span>
						</Styled.TopBarReceiver>
					</Styled.TopBarUser>
					<Styled.TopBarCampaignName>
						<span>{selectedCampaign && selectedCampaign.name}</span>
					</Styled.TopBarCampaignName>
					<Styled.TopBarTogglePanel>
						<span>Details</span>
					</Styled.TopBarTogglePanel>
				</>
			) : null}
		</Styled.TopBar>
	);
};

export default Topbar;
