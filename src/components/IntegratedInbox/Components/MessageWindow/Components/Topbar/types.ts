import { Campaign } from 'components/IntegratedInbox/types';

export interface ITopbar {
	selectedUser: any;
	selectedCampaign: Campaign | null;
}
