import styled from 'styled-components';

const TopBar = styled.div`
	display: flex;
	align-items: center;
	background-color: #f2f2f2;
	border-radius: 2px 2px 0px 0px;
	height: 55px;
	padding: 0 1rem;
	border-bottom: 1px solid #888888;
	color: #333;

	> div {
		width: 33.33%;
	}
`;

const TopBarUser = styled.div`
	display: flex;
	align-items: center;
`;

const TopBarImage = styled.div`
	margin-right: 8px;
	width: 24px;
	height: 24px;

	figure {
		margin: 0;
	}
`;

const TopBarReceiver = styled.div`
	span:first-child {
		font-size: 16px;
		color: #333;
	}
`;

const TopBarCampaignName = styled.div`
	display: flex;
	justify-content: center;
	font-size: 19px;
	font-weight: 500;

	span {
		display: inline-block;
		white-space: nowrap;
		overflow: hidden !important;
		text-overflow: ellipsis;
		max-width: 236px;
	}
`;

const TopBarTogglePanel = styled.div`
	text-align: right;
	display: none;

	> span {
		font-family: 'Space Mono', monospace;
		font-size: 16px;
		color: #888;
	}
`;

const Styled = {
	TopBar,
	TopBarUser,
	TopBarCampaignName,
	TopBarTogglePanel,
	TopBarReceiver,
	TopBarImage
};

export default Styled;
