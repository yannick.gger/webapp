import { render } from '@testing-library/react';
import Topbar from './Topbar';

const mockedProps = {
	selectedUser: {
		id: 1,
		name: 'John doe',
		initials: 'JD',
		avatar: 'fake.png'
	},
	selectedCampaign: {
		id: 1,
		name: 'Campaign 1',
		coverPhoto: {
			largeCoverPhoto: '',
			mediumCoverPhoto: '',
			smallCoverPhoto: ''
		},
		unSeen: false,
		latestMessageDate: 213123213,
		date: 'Today'
	}
};

describe('Integrated Inbox - Message Window Topbar', () => {
	it('renders the component', () => {
		const topbar = render(<Topbar {...mockedProps} />);
		expect(topbar).toMatchSnapshot();
	});
});
