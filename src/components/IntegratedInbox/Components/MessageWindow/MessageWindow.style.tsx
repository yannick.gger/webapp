import styled from 'styled-components';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	font-family: 'Epilogue', sans-serif;
	color: #333;
	max-width: 765px;
`;

const MessageWrapper = styled.div`
	position: relative;
	background-color: ${colors.white};
	height: 650px;
	padding: 2rem;

	.cb-spinner {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translateX(-50%, -50%);
	}
`;

const NewMessageWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100%;
	width: 100%;
`;

const NewMessageInner = styled.div`
	text-align: center;
	max-width: 380px;
`;

const NewMessageHeading = styled.h3`
	font-size: 1.2rem;
	font-weight: 700;
	margin-bottom: 18px;
`;

const NewMessageText = styled.p`
	font-size: 1rem;
	line-height: 1.5;
	margin-bottom: 20px;
`;

const Styled = {
	Wrapper,
	MessageWrapper,
	NewMessageWrapper,
	NewMessageInner,
	NewMessageHeading,
	NewMessageText
};

export default Styled;
