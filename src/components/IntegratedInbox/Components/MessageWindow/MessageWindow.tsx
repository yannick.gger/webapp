import { useEffect, useState } from 'react';
import { IMessageWindow } from './types';
import Styled from './MessageWindow.style';
import Topbar from './Components/Topbar/Topbar';
import MessageField from './Components/MessageField';
import MessageList from './Components/MessageList';
import StartView from './Components/StartView/StartView';
import { getLoggedInUser } from 'components/IntegratedInbox/Helpers';
import LoadingSpinner from 'components/LoadingSpinner';

/**
 * MessageWindow Component
 * @todo Split into smaller components
 * @param {IMessageWindow} props
 * @returns {JSX.Element}
 */
const MessageWindow = (props: IMessageWindow): JSX.Element => {
	const [messages, setMessages] = useState(props.messages);
	const currentUser = props.loggedInUser;
	const selectedUser = props.selectedConversatíon && props.selectedConversatíon.influencer;

	useEffect(() => {
		if (props.selectedConversatíon !== null && props.messages) {
			const conversationId = props.selectedConversatíon.conversationId;
			const conversationMessages = props.messages.find((c: any) => c.id === conversationId);
			if (conversationMessages) {
				setMessages(conversationMessages.messages);
			}
		} else {
			setMessages([]);
		}
	}, [props.messages]);

	const sendMessage = (message: string): void => {
		if (message !== '' && (props.selectedConversatíon && props.selectedConversatíon)) {
			// Create a temporary message object. It will be replaced by the response.
			setMessages((messages: any) => [
				...messages,
				{
					createdAt: 'Now',
					id: Math.random(),
					message: message,
					readAt: '',
					conversationStarter: currentUser.id === getLoggedInUser().id,
					user: {
						id: currentUser.id,
						avatar: '',
						initials: currentUser.initials,
						username: currentUser.name,
						name: currentUser.name,
						organization: {
							name: currentUser.organization.name
						}
					}
				}
			]);

			props.onClickSendMessage && props.onClickSendMessage(props.selectedConversatíon.conversationId, message);
		}
	};

	return (
		<Styled.Wrapper>
			<Topbar selectedUser={selectedUser} selectedCampaign={props.selectedCampaign} />
			<Styled.MessageWrapper>
				{!selectedUser && <StartView disableModal={props.disableModal} selectedCampaign={props.selectedCampaign} onClickOpenModal={props.onClickOpenModal} />}
				{!props.loadingMessages && selectedUser ? (
					<>
						<MessageList loggedInUser={currentUser} selectedUsers={[selectedUser]} messages={messages} conversation={props.selectedConversatíon} />
						<MessageField onPressEnter={(message) => sendMessage(message)} onClickSend={(message) => sendMessage(message)} selectedUsers={[selectedUser]} />
					</>
				) : null}
				{props.loadingMessages ? <LoadingSpinner /> : null}
			</Styled.MessageWrapper>
		</Styled.Wrapper>
	);
};

export default MessageWindow;
