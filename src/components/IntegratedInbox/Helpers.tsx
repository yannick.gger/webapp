import moment from 'moment';
import CollabsAuthService from 'services/Authentication/Collabs-api';
import { Conversation, ICampaign, latestMessage } from 'services/Campaign/types';
import { mapWeekDay } from 'shared/helpers/dates';
import { Campaign, CampaignInstagramOwner, IInfluencerConversation } from './types';

/**
 * Map Instagram owners
 * @param {any} response
 * @returns {Array<CampaignInstagramOwner>}
 */
export const mapInstagramOwners = (response: any): Array<IInfluencerConversation> => {
	let instagramOwners: Array<IInfluencerConversation> = [];
	const data = response.data;
	const includes = response.included;

	// Loop Instagram owners
	data.map((item: any) => {
		const userRelation = item.relationships.influencer;
		if (!userRelation.data) return;

		const influencerObject = includes.find((u: any) => u.type === 'influencer' && u.id === userRelation.data.id);
		if (influencerObject) {
			const instagramOwner: IInfluencerConversation = {
				id: influencerObject.id,
				username: influencerObject.attributes.username,
				avatar: influencerObject.links.profilePictureUrl,
				joined: item.attributes.joined,
				latestMessage: { date: '', rawDate: moment('1970-01-01').toDate(), message: '', readAt: '' },
				conversation: {
					id: '',
					createdAt: '',
					blast: false,
					campaignId: 0,
					createdBy: 0
				},
				unreadMessages: []
			};

			instagramOwners = [...instagramOwners, instagramOwner];
		}
	});

	return instagramOwners;
};

/**
 * getLatestMessageObject
 * @param {any} latestMessageObject
 * @param {any} included
 * @returns latestMessage object
 */
export const getLatestMessageObject = (latestMessageObject: any, included: any) => {
	if (!included || !latestMessageObject) return []; // @todo why array?
	return included.find((x: any) => x.type === latestMessageObject.type && x.id === latestMessageObject.id);
};

/**
 * getConversationObject
 * @param {any} conversationObject
 * @param {string} id
 * @returns conversation object
 */
export const getConversationObject = (conversationObject: any, id: string) => {
	if (!conversationObject) return [];
	return conversationObject.find((conversation: any) => conversation.relationships.influencer.data.id === id);
};

/**
 * mapConversation
 * @param conversation
 * @returns Conversation object
 */
export const mapConversation = (conversation: any): Conversation => {
	const conversationObj: Conversation = {
		id: conversation.id,
		createdAt: moment(conversation.attributes.createdAt).format('YYYY-MM-DD, HH:mm'),
		blast: conversation.attributes.blast,
		campaignId: conversation.relationships.campaign.data.id,
		createdBy: conversation.relationships['createdBy'].data.id
	};

	return conversationObj;
};

/**
 * mapLatestMessage
 * @param latestMessage
 * @returns latestMessageObj
 */
export const mapLatestMessage = (latestMessage: any): latestMessage => {
	const latestMessageObj: latestMessage = {
		date: mapWeekDay(latestMessage.attributes.createdAt),
		rawDate: moment(latestMessage.attributes.createdAt).toDate(),
		message: latestMessage.attributes.message,
		readAt: latestMessage.attributes.readAt
	};

	return latestMessageObj;
};

/**
 * getLoggedInUser
 * @returns CollabsUserObject
 */
export const getLoggedInUser = () => {
	return CollabsAuthService.getCollabsUserObject();
};

/**
 * changeBodyBackground
 */
export const changeBodyBackground = () => {
	const wrapper = document.querySelector('.fade-bg');
	if (wrapper) {
		wrapper.classList.add('bg--spring-wood');
	}
};

/**
 * resetBodyBackground
 */
export const resetBodyBackground = () => {
	const wrapper = document.querySelector('.fade-bg');
	if (wrapper) {
		wrapper.classList.remove('bg--spring-wood');
	}
};

/**
 * sortCampaignsByDate
 * @param campaigns
 * @returns {Array<ICampaign>} sorted array
 */
export const sortCampaignsByDate = (campaigns: Array<Campaign>) => {
	const sorted = campaigns.sort((a: any, b: any) => b.latestMessageDate - a.latestMessageDate);
	return sorted;
};

/**
 * sortConversationsByDate
 * @param campaigns
 * @returns {Array<any>} sorted array
 */
export const sortConversationsByDate = (conversations: Array<any>) => {
	const sorted = conversations.sort((a: any, b: any) => b.latestMessage.rawDate - a.latestMessage.rawDate);
	return sorted;
};

/**
 * sortCampaignsByActive
 * @param {Array<ICampaign>} campaigns
 * @param {number} activeId
 * @returns sorted array
 */
export const sortCampaignsByActive = (campaigns: Array<Campaign>, activeId: number) => {
	const sortByDate = sortCampaignsByDate(campaigns); // Make sure that the list is sorted by date first
	const sorted = sortByDate.sort((x: any, y: any) => {
		return x.id === activeId ? -1 : y.id === activeId ? 1 : 0;
	});

	return sorted;
};
