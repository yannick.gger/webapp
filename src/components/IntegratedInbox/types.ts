import { IntegratedInboxContextType } from 'contexts/IntegratedInbox/Context';
import { latestMessage } from 'services/Campaign/types';

export type CampaignInstagramOwner = {
	id: number;
	username: string;
	avatar: string;
	joined: boolean;
};

export interface IInfluencerConversation extends CampaignInstagramOwner {
	latestMessage: latestMessage;
	conversation: Conversation;
	unreadMessages: Array<any>;
}

export interface IConversation {
	campaignId: number;
	conversations: Array<IInfluencerConversation>;
}

// New models
export type Campaign = {
	id: number;
	name: string;
	coverPhoto: {
		largeCoverPhoto: string;
		mediumCoverPhoto: string;
		smallCoverPhoto: string;
	};
	unSeen: Boolean;
	latestMessageDate: number;
	date: string;
};

export type Conversation = {
	conversationId: string;
	influencer: {
		profilePictureUrl: string | undefined;
		influencerId: string | number;
		username: string;
		avatar?: string;
	};
	createdAt: string;
	unSeen: boolean;
	blast: boolean;
	latestMessageDate: number;
	date: string;
};

export type Message = {
	messageId: string;
	username: string;
	avatar: string;
	message: string;
	date: string;
};

export interface IIntegratedInboxContainerProps {
	context: IntegratedInboxContextType;
}
