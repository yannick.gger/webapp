import styled from 'styled-components';

const Wrapper = styled.div`
	position: relative;
	max-width: 1920px;
	margin-left: auto;
	margin-right: auto;

	.container {
		opacity: 0;
		visibility: hidden;
		transition: opacity 0.2s ease-in-out, visibility 0.2s ease-in-out;

		&.visible {
			opacity: 1;
			visibility: visible;
		}
	}
`;

const LoadingConversation = styled.div`
	position: absolute;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	text-align: center;

	.cb-spinner {
		margin: 0 auto;
	}

	span {
		display: block;
		padding-top: 16px;
	}
`;

const Styled = {
	Wrapper,
	LoadingConversation
};

export default Styled;
