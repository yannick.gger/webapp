import { useContext, useEffect, useRef, useState } from 'react';
import CampaignsService from 'services/Campaign/Campaigns.service';
import { ICampaignUser } from 'services/Campaign/types';
import { ICollabsResponse } from 'services/Response.types';
import Grid from 'styles/grid/grid';
import Styled from './IntegratedInboxContainer.style';
import { ToastContext } from 'contexts';
import ConversationsList from './Components/ConversationsList';
import MessageWindow from './Components/MessageWindow/MessageWindow';
import moment from 'moment';
import ConversationsService from 'services/Conversations';
import { mapWeekDay } from 'shared/helpers/dates';
import { changeBodyBackground, getLoggedInUser, mapInstagramOwners, sortCampaignsByDate, sortCampaignsByActive } from './Helpers';
import { Campaign, CampaignInstagramOwner, Conversation, IInfluencerConversation, IIntegratedInboxContainerProps } from './types';
import NewMessageModal from './Components/NewMessageModal';
import withIntegratedInboxContext from 'contexts/IntegratedInbox/Consumer';
import { useHistory, useParams } from 'react-router-dom';
import { useIntegratedInboxData } from './hooks';
import { getCurrentOrganization } from 'shared/utils';
import LoadingSpinner from 'components/LoadingSpinner';
import classNames from 'classnames';

/**
 * Integrated Inbox Container ( WIP )
 * @todo Clear and refactor this mess!! // KB
 * @todo UI Adjustments, context for messages and instagramOwners, i18next
 * @returns JSX.Element
 */
const IntegratedInboxContainer = (props: IIntegratedInboxContainerProps) => {
	const { context } = props;
	const { getCampaigns, getConversations } = useIntegratedInboxData();

	const [campaigns, setCampaigns] = useState<Array<Campaign>>([]);
	const [conversations, setConversations] = useState<Array<IInfluencerConversation>>([]);
	const [campaignInstagramOwners, setCampaignInstagramOwners] = useState<Array<CampaignInstagramOwner>>([]);
	const [selectedCampaign, setSelectedCampaign] = useState<Campaign | null>(null);
	const [messages, setMessages] = useState<Array<any>>([]);
	const [listLoading, setListLoading] = useState<boolean>(false);
	const [conversationsLoading, setConversationsLoading] = useState<boolean>(false);
	const [influencersLoading, setInfluencersLoading] = useState<boolean>(false);

	const [messagesLoading, setMessagesLoading] = useState<boolean>(false);
	const [isModalOpen, setIsModalOpen] = useState(false);

	const { campaignId, conversationId } = useParams();
	const fetchConversationsByUrl = campaignId !== undefined && conversationId === undefined;

	const { addToast } = useContext(ToastContext);
	const history = useHistory();
	const orgSlug = getCurrentOrganization();

	// Context
	const campaignList = context.campaignList;

	// Logged in user
	const loggedInUser = getLoggedInUser();

	// Get and set campaigns
	const fetchCampaigns = (): void => {
		context.UpdateCampaignsListRequest();

		getCampaigns()
			.then((campaigns: Array<Campaign>) => context.UpdateCampaignsListSuccess(campaigns))
			.catch((error: any) => {
				addToast({ id: 'IIC-error', mode: 'error', message: 'Oops! Cannot get your campaigns' });
				context.UpdateCampaignsListFailure(error);
				console.error(error);
			});
	};

	// Get and set conversations
	const fetchConversations = (campaignId: number) => {
		context.UpdateConversationsRequest();
		getConversations(campaignId)
			.then((conversations: Array<Conversation>) => context.UpdateConversationsSuccess(conversations))
			.catch((error: any) => {
				addToast({ id: 'IIConversations-error', mode: 'error', message: 'Oops! Cannot get your conversations' });
				context.UpdateConversationsFailure(error);
				console.error(error);
			})
			.finally(() => !conversationId && context.FetchByUrl(false));
	};

	const getCampaignInstagramUsers = (campaignId: number): void => {
		setInfluencersLoading(true);
		CampaignsService.getCampaignInstagramOwners(campaignId)
			.then((result: ICollabsResponse) => {
				if (result.data) {
					const instagramOwners = mapInstagramOwners(result);
					let influencerConversations: Array<IInfluencerConversation> = [];

					instagramOwners.map((item: IInfluencerConversation) => {
						influencerConversations = [
							...influencerConversations,
							{
								...item,
								latestMessage: {
									date: '',
									rawDate: moment('1970-01-01').toDate(),
									message: '',
									readAt: ''
								}
							}
						];
					});
					// Set influencers for new conversation modal
					setCampaignInstagramOwners(instagramOwners);
				}
			})
			.catch(() => {
				addToast({ id: 'IIIG-error', mode: 'error', message: 'Oops! Cannot get users' });
			})
			.finally(() => setInfluencersLoading(false));
	};

	// Get messages
	const getMessages = (conversationId: string, shouldLoad: boolean = true, includeBlasts: boolean = false): void => {
		setMessagesLoading(shouldLoad);
		ConversationsService.getMessages(conversationId)
			.then((result: ICollabsResponse) => {
				if (result.data) {
					const data = result.data;
					const included = result.included;
					const messageArr: Array<any> = [];
					const index = messages.findIndex((item: any) => item.id === conversationId);

					data.map((messageItem: any) => {
						const attributes = messageItem.attributes;
						const relationships = messageItem.relationships;
						let userObject: any = null;
						let isInfluencer: boolean = false;

						// If influencer
						if (relationships.influencer.data) {
							isInfluencer = true;
							userObject = included.find((x: any) => x.type === relationships.influencer.data.type && x.id === relationships.influencer.data.id);

							// If user
						} else if (relationships.user.data) {
							isInfluencer = false;
							userObject = included.find((x: any) => x.type === relationships.user.data.type && x.id === relationships.user.data.id);
						}

						messageArr.push({
							id: messageItem.id,
							createdAt: `${mapWeekDay(attributes.createdAt)}, ${moment(attributes.createdAt).format('HH:mm')}`,
							message: attributes.message,
							readAt: attributes.readAt,
							conversationStarter: loggedInUser.id === (userObject && userObject.id),
							inConversation: attributes.inConversation,
							user: {
								id: userObject && userObject.id,
								avatar: userObject && userObject.links ? userObject.links.profilePictureUrl : null,
								username: (userObject && userObject.attributes.username) || (userObject && userObject.attributes.initials),
								name: null,
								isInfluencer: isInfluencer,
								organization: loggedInUser.organization
							}
						});

						if (attributes.readAt === null && (userObject && userObject.id) !== loggedInUser.id && attributes.inConversation) {
							markMessageAsRead(conversationId, messageItem.id);
						}
					});

					if (index === -1) {
						setMessages([
							...messages,
							{
								id: conversationId,
								messages: messageArr
							}
						]);
					} else {
						const item = messages[index];
						const updatedItem = {
							...item,
							messages: messageArr
						};

						const updatedArray = [...messages];
						updatedArray[index] = updatedItem;
						setMessages(updatedArray);
					}
				} else {
					addToast({ id: 'IICM-error', mode: 'error', message: 'Oops! Cannot get messages' });
					console.error(result.errors);
				}
			})
			.finally(() => {
				setMessagesLoading(false);
				conversationId && context.FetchByUrl(false);
			});
	};

	// Mark a message as read
	const markMessageAsRead = (conversationId: string, conversationMessageId: string) => {
		ConversationsService.setReadAtByMessageId(conversationId, conversationMessageId);
	};

	const setCampaignToActive = (campaign: Campaign) => {
		context.SetActiveCampaign(campaign);
		fetchConversations(campaign.id);
		setSelectedCampaign(campaign);
		setCampaigns(sortCampaignsByActive(campaignList.campaigns, campaign.id));
	};

	const resetCampaignState = () => {
		context.RemoveActiveConversation();
		context.RemoveActiveCampaign();
		setSelectedCampaign(null);
		setCampaigns(sortCampaignsByDate(campaigns));
		history.push(`/${orgSlug}/integrated-inbox`);
	};

	const onClickCampaign = (campaign: Campaign): void => {
		if (listLoading) return;

		if (campaign.id) {
			setCampaignToActive(campaign);
			history.push(`/${orgSlug}/integrated-inbox/${campaign.id}`);
		} else {
			resetCampaignState();
		}

		setMessages([]);
		setConversations([]);
		setCampaignInstagramOwners([]);
		context.RemoveActiveConversation();
	};

	const setActiveConversation = (conversation: Conversation): void => {
		if (messagesLoading) return;
		context.SetActiveConversation(conversation);
		changeBodyBackground();

		// Get messages for the selected conversation
		if (selectedCampaign && conversation.conversationId) {
			history.push(`/${orgSlug}/integrated-inbox/${selectedCampaign.id}/${conversation.conversationId}`);
			getMessages(conversation.conversationId);
		}
	};

	const onClickSendMessage = (conversationId: string, message: string): void => {
		sendMessage(conversationId, message);
	};

	const sendMessage = (conversationId: string, message: string): void => {
		if (!message) return;
		ConversationsService.createMessage(conversationId, message).then((result: ICollabsResponse) => {
			if (result.data) {
				setTimeout(() => {
					getMessages(conversationId, false);
				}, 60000); // wait 1 minute before we get the messages from api

				// update conversation list
				let conversationsArr = conversations.map((user: any) => {
					if (user.conversation && user.conversation.id == conversationId) {
						return { ...user, latestMessage: { ...user.latestMessage, date: 'Today' } };
					}
					return user; // else return unmodified item
				});

				setConversations(conversationsArr);
			} else {
				addToast({ id: 'IISM-error', mode: 'error', message: 'Oops! Cannot send your message' });
				console.error(result.errors);
			}
		});
	};

	const onClickOpenModal = () => {
		if (selectedCampaign) {
			getCampaignInstagramUsers(selectedCampaign.id);
		}

		setIsModalOpen(true);
	};

	const onClickCloseModal = (user?: ICampaignUser) => {
		setIsModalOpen(false);
		if (user !== undefined) {
			if (selectedCampaign && user) {
				fetchConversations(selectedCampaign.id);
			}
		}
	};

	useEffect(() => {
		fetchCampaigns();
		context.FetchByUrl(campaignId !== undefined);
	}, []);

	useEffect(() => {
		setListLoading(campaignList.loading);

		if (campaignList.loaded && !campaignList.error) {
			setCampaigns(campaignList.campaigns);
		}
	}, [campaignList]);

	useEffect(() => {
		if (campaignId !== undefined) {
			const campaign = campaigns.find((c: any) => c.id === Number(campaignId));
			if (campaign) {
				context.SetActiveCampaign(campaign);
			}
		}
	}, [campaigns]);

	useEffect(() => {
		if (context.activeCampaign.id) {
			context.fetchByUrl.loading && setCampaignToActive(context.activeCampaign);
			setMessages([]);
			setConversations([]);
			setCampaignInstagramOwners([]);
		}
	}, [context.activeCampaign]);

	useEffect(() => {
		if (conversationId !== undefined) {
			const conversation = context.conversationList.conversations.find((c: Conversation) => c.conversationId === conversationId);
			conversation && setActiveConversation(conversation);
		}
	}, [context.conversationList.conversations]);

	useEffect(() => {
		setListLoading(context.campaignList.loading);
		setConversationsLoading(context.conversationList.loading);
	}, [context.campaignList.loading, context.conversationList.loading]);

	return (
		<Styled.Wrapper>
			<Grid.Container
				className={classNames('container', {
					visible: !context.fetchByUrl.loading && !context.fetchByUrl.loading
				})}
			>
				<Grid.Column xs={12} md={6}>
					<ConversationsList
						campaigns={campaignList.campaigns}
						conversations={context.conversationList.conversations}
						activeConversation={context.activeConversation}
						loading={listLoading}
						conversationLoading={conversationsLoading}
						userListLoading={listLoading}
						onClickConversation={(conversation: Conversation) => setActiveConversation(conversation)}
						onClickCampaign={(campaign: Campaign) => onClickCampaign(campaign)}
						onClickOpenModal={() => onClickOpenModal()}
						onClickCloseModal={() => onClickCloseModal()}
						isModalOpen={isModalOpen}
						disableModal={conversationsLoading}
						context={context}
					/>
				</Grid.Column>
				<Grid.Column xs={12} md={6}>
					<MessageWindow
						loggedInUser={loggedInUser}
						selectedCampaign={context.activeCampaign}
						selectedConversatíon={context.activeConversation}
						messages={messages}
						loadingMessages={messagesLoading}
						onClickSendMessage={(convsersationId: string, message: string) => onClickSendMessage(convsersationId, message)}
						onClickOpenModal={() => onClickOpenModal()}
						disableModal={conversationsLoading}
					/>
				</Grid.Column>
			</Grid.Container>
			{context.fetchByUrl.loading && (
				<Styled.LoadingConversation>
					<LoadingSpinner size='lg' /> <span css={{ display: 'block' }}>{`Loading conversation${fetchConversationsByUrl ? 's' : ''}...`}</span>
				</Styled.LoadingConversation>
			)}
			{context.activeCampaign && isModalOpen && (
				<NewMessageModal
					isModalOpen={isModalOpen}
					onModalClose={(selectedUser: any) => onClickCloseModal(selectedUser)}
					influencers={campaignInstagramOwners}
					campaignId={context.activeCampaign.id}
					loggedInUser={loggedInUser}
					context={context}
					influencersLoading={influencersLoading}
				/>
			)}
		</Styled.Wrapper>
	);
};

export default withIntegratedInboxContext(IntegratedInboxContainer);
