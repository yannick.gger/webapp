import React from 'react';
import { Form, Input, Popover, Select, InputNumber, Alert, Button, Progress, Icon, Row, Col } from 'antd';
import { withApollo } from 'react-apollo';
import { currencies } from 'country-data';
import debounce from 'lodash/debounce';
import ButtonLink from 'components/button-link';
import getOrganizationLists from 'graphql/get-organization-my-lists.graphql';
import { translate } from 'react-i18next';
import { getTokenPayload, getOrganizationBySlug } from 'services/auth-service.js';
import CampaignUploadLogo from './upload-logo.js';
import CampaignUploadCover from './upload-cover.js';
import listCheckIcon from '../../assets/img/logo/list-check.png';
import listSearchIcon from '../../assets/img/logo/list-search.png';
import './styles.scss';

const FormItem = Form.Item;
const Option = Select.Option;

const popoverContent = (
	<div style={{ width: 500 }}>
		<p>
			<strong>Storytelling:</strong> Let the influencer’s know this is something they can’t afford to miss.
			<br />
			<em>Tip:</em> Is your product or service eco-friendly? Is it healthy? What good does it do the consumer? These arguments will position the collaboration
			to be something beneficial for the influencer's engaged audience.
		</p>

		<p>
			<strong>Purpose:</strong> What’s the purpose of your campaign?
		</p>

		<p>
			<strong>Target/KPI:</strong> What do you wish to achieve with your campaign?
		</p>
		<ul>
			<li>Brand awareness</li>
			<li>Subscriptions (newsletter, membership)</li>
			<li>Sales & conversion</li>
			<li>Engagement</li>
		</ul>

		<p>
			<strong>Recommended guidelines:</strong>
		</p>
		<ul>
			<li>Start / end the caption with "in collaboration with" or #advertisement</li>
			<li>The image may not be removed for at least 30 days</li>
			<li>Do not post any other sponsored content the same day</li>
			<li>Delays regarding draft and publication may result in a reduction of payment</li>
			<li>The content should be at the top of the feed for at least 8 hours </li>
			<li>The brand’s entitled to re-publish and/or use content on other platforms royalty-free</li>
		</ul>
		<p>
			<em>*Attn: The more you’re asking, the more you’re expected to give..</em>
		</p>
	</div>
);

class CampaignForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			photoUploadCount: 0,
			percent: 0,
			step: 1,
			maxStep: 8,
			organizationLists: [],
			findLater: false
		};
		this.step1 = React.createRef();
		this.step2 = React.createRef();
		this.step3 = React.createRef();
		this.step4 = React.createRef();
		this.step5 = React.createRef();
		this.step6 = React.createRef();
		this.step7 = React.createRef();
		this.step8 = React.createRef();
		this.step9 = React.createRef();
		this.inputStep1 = React.createRef();
		this.inputStep2 = React.createRef();
		this.inputStep3 = React.createRef();
		this.inputStep4 = React.createRef();
		this.inputStep5 = React.createRef();
		this.inputStep6 = React.createRef();
		this.inputStep7 = React.createRef();
		this.createCampaignFooter = React.createRef();
	}

	componentDidMount() {
		const { client, lists } = this.props;
		client
			.query({
				query: getOrganizationLists,
				variables: { slug: this.props.organizationSlug }
			})
			.then((response) => {
				const organizationLists =
					response.data && response.data.organization && response.data.organization.myLists ? response.data.organization.myLists.edges : [];
				this.setState({ organizationLists });
				if (organizationLists.length > 0) {
					this.setState({ maxStep: 9 });
				}
				if (lists.length > 0) {
					this.setState({ maxStep: 8 });
				}
				this.modalEl = document.querySelector('.custom-modal.separate-parts.title-center');
				console.assert(this.modalEl, 'This modal container is required. If you change the class, please update the selector above! ^^');

				this.scroll = debounce(() => {
					try {
						let winScroll = this.modalEl.scrollTop;
						let height = this.modalEl.scrollHeight - this.modalEl.clientHeight;
						let scrolled = (winScroll / height) * 100;

						if (scrolled > 95) {
							scrolled = 100;
						}

						this.setState(() => ({ percent: scrolled }));
						let breakingPoints = this.state.maxStep === 8 ? [91, 84, 73, 60, 47, 35, 24, 12] : [87, 75, 65, 54, 43, 32, 14, 9];
						const steps = [this.state.maxStep, 8, 7, 6, 5, 4, 3, 2, 1];
						let i = 0;
						let step = 1;
						for (; i < steps.length; i++) {
							if (scrolled > breakingPoints[i]) {
								step = steps[i];
								break;
							}
						}
						this.handleStepChange(step);
					} catch (e) {
						console.log('e', e);
					}
				}, 100);
				if (this.modalEl) {
					this.modalEl.addEventListener('scroll', this.scroll);
				}
			});
	}

	componentWillUnmount() {
		if (this.modalEl) {
			this.modalEl.removeEventListener('scroll', this.scroll);
		}
	}

	handleStepChange = (step) => {
		const prevStep = this.state.step;
		if (step !== prevStep) {
			this.setState({ step });

			if (this[`inputStep${step}`]) {
				let input = this[`inputStep${step}`].rcSelect ? this[`inputStep${step}`].rcSelect : document.querySelector(`#${this[`inputStep${step}`].props.id}`);
				if ([3, 4].includes(step)) input = input.selectionRef;
				else if (step === 7) input = input.inputRef;
				input.focus({ preventScroll: true });
			}
		}
	};

	handleScrollIntoView = (elementFocused) => {
		if (elementFocused.current) {
			elementFocused.current.scrollIntoView({
				behavior: 'smooth',
				block: 'nearest'
			});
		}
	};

	returnStepOne = (e) => {
		e.preventDefault();
		this.modalEl.scrollTop = 0;
	};

	handleOnClick = (el) => () => {
		if ('activeElement' in document && document.activeElement.type === 'button') document.activeElement.blur();
		if (this.state.step < this.state.maxStep) this.handleStepChange(this.state.step + 1);
		this.handleScrollIntoView(el);
	};

	hide = () => {
		this.setState({
			visible: false
		});
	};

	handleVisibleChange = (visible) => {
		this.setState({ visible });
	};

	handlePhotoUploading = () => {
		this.setState({ photoUploadCount: this.state.photoUploadCount + 1 }, () => {
			if (this.state.photoUploadCount === 1) this.props.onPhotoUploading();
		});
	};

	handlePhotoUploadDone = () => {
		this.setState({ photoUploadCount: this.state.photoUploadCount - 1 }, () => {
			if (this.state.photoUploadCount === 0) this.props.onPhotoUploadDone();
		});
	};

	handleShowLists = () => {
		this.setState({
			showSelectLists: true,
			findLater: false
		});
	};
	handleCloseSelectLists = () => {
		this.setState({ showSelect: true, showSelectLists: false });
	};

	handleFindLaterView = () => {
		this.setState({ findLater: true, showSelectLists: false });
	};

	render() {
		const { campaign, form, organizationSlug, handleSubmit, loading, lists, t } = this.props;
		const { step, maxStep, organizationLists, showSelectLists, findLater } = this.state;
		const { getFieldDecorator } = form;
		let selectListNodes = null;
		const formItemLayout = { labelCol: { span: 24 }, wrapperCol: { span: 24 } };
		let currencyAlertMessage = '';
		let handleOkScroll = (el) => {
			return (
				<div className='d-flex align-items-center justify-content-center pt-20 accept-btn-wrapper'>
					<Button type='primary' onClick={this.handleOnClick(el)}>
						OK
						<Icon type='check' />
					</Button>
				</div>
			);
		};
		const listOptions =
			organizationLists &&
			organizationLists.length > 0 &&
			organizationLists.map((list) => (
				<Option key={list.node.id} value={list.node.id}>
					{list.node.name}
				</Option>
			));
		if (lists.length > 0) {
			selectListNodes = null;
		} else {
			if (showSelectLists && !findLater) {
				selectListNodes = (
					<div className='create-campaign-step step-9' ref={this.step9}>
						<div>
							<div className='flex-box'>
								<Button style={{ width: '100%' }} type='primary' size='large' className='mr-20' onClick={this.handleShowLists}>
									Yes, I already have a list!
								</Button>
								<Button style={{ width: '100%' }} size='large' onClick={this.handleFindLaterView}>
									I will find influencers later
								</Button>
							</div>
							<div className='flex-box'>
								<h3 style={{ flex: 1 }} className='mr-20'>
									What list do you want to use?
								</h3>
								<div style={{ flex: 1 }}>
									<FormItem {...formItemLayout} label=''>
										{getFieldDecorator('myListIds', {
											rules: [{ required: false, message: 'Please select which list you want to use in this campaign' }]
										})(
											<Select size='large' placeholder='Select a list' mode='multiple'>
												{listOptions}
											</Select>
										)}
									</FormItem>
								</div>
							</div>
							{handleOkScroll(this.createCampaignFooter)}
						</div>
					</div>
				);
			} else if (findLater) {
				selectListNodes = (
					<div className='create-campaign-step step-9' ref={this.step9}>
						<div>
							<div className='flex-box pb-20'>
								<Button size='large' className='mr-20' onClick={this.handleShowLists}>
									Yes, I already have a list!
								</Button>
								<Button type='primary' size='large' onClick={this.handleCloseSelectLists}>
									I will find influencers later
								</Button>
							</div>
							{handleOkScroll(this.createCampaignFooter)}
						</div>
					</div>
				);
			} else {
				selectListNodes = (
					<div className='create-campaign-step step-9' ref={this.step9}>
						<div>
							<Row className='d-flex align-items-center justify-content-center list-box-wrapper' gutter={20}>
								<Col md={{ span: 12 }} className='select-list-box' type='primary' size='large' onClick={this.handleShowLists}>
									<Button className='lists-button' onClick={this.handle} style={{ padding: '0 !important' }}>
										<img src={listCheckIcon} alt='list-icon-check' />
										<p>Yes, I already have a list!</p>
									</Button>
								</Col>
								<Col md={{ span: 12 }} className='select-list-box'>
									<Button className='lists-button' onClick={this.handleFindLaterView}>
										<img src={listSearchIcon} alt='list-icon-search' />
										<p>I will find influencers later</p>
									</Button>
								</Col>
							</Row>
							{handleOkScroll(this.createCampaignFooter)}
						</div>
					</div>
				);
			}
		}
		if (campaign && campaign.paymentCompensationsCount > 0) {
			currencyAlertMessage = t('currencyAlertMessageRemoveCommission');
		} else if (campaign && campaign.invoiceCompensationsCount > 0) {
			currencyAlertMessage = t('currencyAlertMessageRemoveInvoice');
		} else {
			currencyAlertMessage = t('currencyAlertMessageSupportedCurrencies');
		}

		return (
			<section className='step'>
				<h3 className='create-campaign-heading'>
					STEP {step} / {maxStep}
				</h3>
				<Progress className='create-campaign-progress-bar' percent={this.state.percent} showInfo={false} />
				<div className='create-campaign-headline'>
					<h3 style={{ transform: `translateX(${(1 - step) * 1300}px)` }}>Start off by defining your campaign and brand name</h3>
					<h3 style={{ transform: `translate(${(2 - step) * 1300}px, ${-41}px)` }}>Who is the main contact point for your campaign?</h3>
					<h3 style={{ transform: `translate(${(3 - step) * 1300}px, ${-41 * 2}px)` }}>In which language would you like your campaign to be shown?</h3>
					<h3 style={{ transform: `translate(${(4 - step) * 1300}px, ${-41 * 3}px)` }}>Select the currency used for influencer payments</h3>
					<h3 style={{ transform: `translate(${(5 - step) * 1300}px, ${-41 * 4}px)` }}>Now let the influencers know what your campaign is about!</h3>
					<h3 style={{ transform: `translate(${(6 - step) * 1300}px, ${-41 * 5}px)` }}>How many influencers do you want in your campaign?</h3>
					<h3 style={{ transform: `translate(${(7 - step) * 1300}px, ${-41 * 6}px)` }}>How about the mentions and hashtags for your campaign?</h3>
					<h3 style={{ transform: `translate(${(8 - step) * 1300}px, ${-41 * 7}px)` }}>Upload your logo and a campaign cover photo</h3>
					<h3 style={{ transform: `translate(${(9 - step) * 1300}px, ${-41 * 8}px)` }}>Do you want to use one of your lists in this campaign?</h3>
				</div>
				<main>
					<Form className='create-campaign-form-container'>
						{campaign && campaign.invitesSent && (
							<Alert message='Campaign can not be edited' description="Campaign can't be edited after invites are sent." type='warning' showIcon />
						)}
						<div className='create-campaign-step step-1' ref={this.step1} style={{ paddingTop: 0 }}>
							<div>
								<FormItem {...formItemLayout} label='Campaign name'>
									{getFieldDecorator('name', {
										rules: [
											{ required: true, whitespace: true, message: 'Please choose a name for your campaign' },
											{ max: 25, message: 'Use a shorter name for your campaign so it looks good everywhere. Limit is 25 character.' }
										]
									})(
										<Input
											autoFocus
											ref={(input) => (this.inputStep1 = input)}
											className='input-line-style'
											size='large'
											placeholder='Campaign name'
											disabled={campaign && campaign.invitesSent}
										/>
									)}
								</FormItem>
								<FormItem {...formItemLayout} label='Brand name'>
									{getFieldDecorator('brandName', {
										initialValue: campaign ? campaign.organization && campaign.organization.name : getOrganizationBySlug(organizationSlug).name,
										rules: [
											{ required: true, whitespace: true, message: 'Please choose a brand name for your campaign' },
											{ max: 25, message: 'Use a shorter brand name for your campaign so it looks good everywhere. Limit is 25 character.' }
										]
									})(<Input className='input-line-style' size='large' placeholder='Brand name' disabled={campaign && campaign.invitesSent} />)}
								</FormItem>
								{handleOkScroll(this.step2)}
							</div>
						</div>
						<div className='create-campaign-step step-2' ref={this.step2}>
							<div>
								<FormItem {...formItemLayout} label='Contact person'>
									{getFieldDecorator('contactPerson', {
										initialValue: getTokenPayload() && getTokenPayload().user && getTokenPayload().user.name,
										rules: [
											{ required: true, whitespace: true, message: 'Please write a name for the contact person' },
											{ max: 25, message: 'Use a shorter contact name so it looks good everywhere. Limit is 25 character.' }
										]
									})(
										<Input
											ref={(input) => (this.inputStep2 = input)}
											className='input-line-style'
											size='large'
											placeholder='John Johnsson'
											disabled={campaign && campaign.invitesSent}
										/>
									)}
								</FormItem>
								<FormItem {...formItemLayout} label='Notification Email (Your Own Email)'>
									{getFieldDecorator('notificationEmail', {
										initialValue: getTokenPayload() && getTokenPayload().user && getTokenPayload().user.email,
										rules: [
											{ type: 'email', message: 'The input is not valid E-mail!' },
											{ required: true, whitespace: true, message: 'Please write an email for the campaign notification' }
										]
									})(<Input className='input-line-style' size='large' placeholder='john@email.com' disabled={campaign && campaign.invitesSent} />)}
								</FormItem>
								{handleOkScroll(this.step3)}
							</div>
						</div>
						<div className='create-campaign-step step-3' ref={this.step3}>
							<div>
								<FormItem {...formItemLayout} label='language'>
									{getFieldDecorator('language', {
										rules: [{ required: true, message: 'Please select which language your brief will have' }]
									})(
										<Select
											size='large'
											disabled={campaign && campaign.invitesSent}
											ref={(input) => (this.inputStep3 = input)}
											showSearch
											placeholder='Select a language'
											filterOption={(input, option) => {
												const textIncludes = option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
												const optionalIncludes = option.props.id && option.props.id.indexOf(input.toLowerCase()) >= 0;
												return textIncludes || optionalIncludes;
											}}
										>
											<Option value='en' id='engelska'>
												English
											</Option>
											<Option value='sv' id='svenska'>
												Swedish
											</Option>
										</Select>
									)}
								</FormItem>
								{handleOkScroll(this.step4)}
							</div>
						</div>
						<div className='create-campaign-step step-4' ref={this.step4}>
							<div>
								<FormItem {...formItemLayout} label='currency'>
									{getFieldDecorator('currency', {
										rules: [{ required: true, message: 'Please select which currency your products and/or payments should be presented as' }]
									})(
										<Select
											size='large'
											disabled={campaign && (campaign.invitesSent || (campaign.paymentCompensationsCount > 0 || campaign.invoiceCompensationsCount > 0))}
											ref={(input) => (this.inputStep4 = input)}
											showSearch
											placeholder='Select a currency'
											filterOption={(input, option) => {
												const codeIncludes = option.key.toLowerCase().indexOf(input.toLowerCase()) >= 0;
												const nameIncludes = option.props.children[0].toLowerCase().indexOf(input.toLowerCase()) >= 0;
												return codeIncludes || nameIncludes;
											}}
										>
											{currencies.all
												.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
												.map((currency) => (
													<Option key={currency.code} value={currency.code}>
														{currency.name} ({currency.code})
													</Option>
												))}
										</Select>
									)}
									<Alert showIcon type='info' className='mt-20' description={currencyAlertMessage} />
								</FormItem>
								{handleOkScroll(this.step5)}
							</div>
						</div>
						<div className='create-campaign-step step-5' ref={this.step5}>
							<div>
								<FormItem
									className='full-width-label'
									{...formItemLayout}
									label={
										<React.Fragment>
											<Popover
												content={popoverContent}
												title='Standard Brief'
												trigger='click'
												visible={this.state.visible}
												onVisibleChange={this.handleVisibleChange}
											>
												<a className='fr'>Show me tips!</a>
											</Popover>
											Brief (Visible to influencers)
										</React.Fragment>
									}
								>
									{getFieldDecorator('brief', {
										rules: [
											{ message: 'Please write a brief for your campaign' },
											{ required: true, whitespace: true, message: 'Please write a brief for your campaign' }
										]
									})(
										<Input.TextArea
											size='large'
											rows={4}
											ref={(input) => (this.inputStep5 = input)}
											disabled={campaign && campaign.invitesSent}
											placeholder='A brief for about your campaign and what it includes. Visible to influencers when joining the campaign.'
										/>
									)}
								</FormItem>
								{handleOkScroll(this.step6)}
							</div>
						</div>
						<div className='create-campaign-step step-6' ref={this.step6}>
							<div>
								<FormItem label='I want' className='form-item-inline d-flex justify-content-center align-items-center expect-influencers'>
									{getFieldDecorator('influencerTargetCount', {
										initialValue: 10,
										rules: [{ required: true, message: 'Please enter a number between 1 and 500' }]
									})(<InputNumber ref={(input) => (this.inputStep6 = input)} size='large' disabled={campaign && campaign.invitesSent} min={1} max={500} />)}
									<span> influencers to join my campaign</span>
								</FormItem>
								{false && (
									<FormItem {...formItemLayout} label='Use influencers from list' hasFeedback>
										{getFieldDecorator('select', {
											rules: [{ required: true, message: 'Please select the list to use' }]
										})(
											<Select size='large' disabled={campaign && campaign.invitesSent} placeholder='Please select a list'>
												<Option value='my-list-1'>My influencer list</Option>
												<Option value='my-list-2'>My other list</Option>
												<Option value='create-new'>+ Create new list</Option>
											</Select>
										)}
									</FormItem>
								)}
								{handleOkScroll(this.step7)}
							</div>
						</div>
						<div className='create-campaign-step step-7' ref={this.step7}>
							<div>
								<FormItem label='What tags should be used?'>
									{getFieldDecorator('tags', {
										rules: [
											{
												validator: (rule, value, callback) => {
													let errors = [];

													if (
														value &&
														value.filter((val) => val.match(/^(?:#)([A-Za-z0-9_åäö](?:(?:[A-Za-z0-9_åäö]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_åäö]))?)$/g))
															.length !== value.length
													) {
														errors.push(new Error());
													}
													callback(errors);
												},
												message: 'A tag must start with # and can not be longer than 28 characters and can only contain a-z, 0-9, _ and dots.'
											},
											{
												validator: (rule, value, callback) => {
													let errors = [];

													if (value && value.length >= 5) {
														errors.push(new Error());
													}
													callback(errors);
												},
												message: 'Use 5 or less tags to not appear spammy. '
											}
										]
									})(
										<Select
											mode='tags'
											ref={(input) => (this.inputStep7 = input)}
											disabled={campaign && campaign.invitesSent}
											style={{ width: '100%' }}
											size='large'
											tokenSeparators={[',', ' ']}
											placeholder='Tags #sampletag #myhashtag'
											notFoundContent='Enter a #hashtag'
										/>
									)}
								</FormItem>
								<FormItem label='What mentions should be used?'>
									{getFieldDecorator('mentions', {
										rules: [
											{
												validator: (rule, value, callback) => {
													let errors = [];

													if (
														value &&
														value.filter((val) => val.match(/^(?:@)([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)$/g)).length !==
															value.length
													) {
														errors.push(new Error());
													}
													callback(errors);
												},
												message: 'A mention must start with @ and can not be longer than 28 characters and can only contain a-z, 0-9, _ and dots.'
											},
											{
												validator: (rule, value, callback) => {
													let errors = [];

													if (value && value.length >= 5) {
														errors.push(new Error());
													}
													callback(errors);
												},
												message: 'Use 5 or less mentions to not appear spammy. '
											}
										]
									})(
										<Select
											mode='tags'
											style={{ width: '100%' }}
											size='large'
											tokenSeparators={[',', ' ']}
											disabled={campaign && campaign.invitesSent}
											placeholder='Mentions @companyname @profile'
											notFoundContent='Enter a @mention'
										/>
									)}
								</FormItem>
								{handleOkScroll(this.step8)}
							</div>
						</div>
						<div className='create-campaign-step step-8' ref={this.step8}>
							<div>
								{!this.props.hideUploadFields && (
									<React.Fragment>
										<FormItem {...formItemLayout} label='Upload your company logo'>
											{getFieldDecorator('campaignLogoId')(
												<CampaignUploadLogo
													form={this.props.form}
													disabled={campaign && campaign.invitesSent}
													onPhotoUploading={this.handlePhotoUploading}
													onPhotoUploadDone={this.handlePhotoUploadDone}
												/>
											)}
										</FormItem>
										<FormItem {...formItemLayout} label='Upload cover photo used for campaign page'>
											{getFieldDecorator('campaignCoverPhotoId')(
												<CampaignUploadCover
													form={this.props.form}
													disabled={campaign && campaign.invitesSent}
													onPhotoUploading={this.handlePhotoUploading}
													onPhotoUploadDone={this.handlePhotoUploadDone}
												/>
											)}
										</FormItem>
									</React.Fragment>
								)}
								{handleOkScroll(maxStep === 8 ? this.createCampaignFooter : this.step9)}
							</div>
						</div>
						{this.state.organizationLists.length > 0 ? selectListNodes : null}
						<div className='create-campaign-footer' ref={this.createCampaignFooter}>
							<div>
								<h3>Alright, that's it.</h3>
								<Button size='large' data-ripple='rgba(0, 0, 0, 0.2)' key='submit' type='primary' loading={loading} onClick={handleSubmit}>
									Go to your campaign
									<Icon type='arrow-right' />
								</Button>
							</div>
						</div>
					</Form>
				</main>
				<div className='go-back-btn'>
					<ButtonLink onClick={this.returnStepOne}>go back</ButtonLink>
				</div>
			</section>
		);
	}
}

const WrappedCampaignForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const propFields = props.fields || {};
		let formFields = {};
		Object.keys(propFields).forEach((propFieldKey) => {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		});

		return formFields;
	}
})(CampaignForm);

export default withApollo(translate('campaign')(WrappedCampaignForm));
