import React from 'react';
import { Form, Input, Popover, Select, InputNumber, Alert, Checkbox } from 'antd';
import { currencies } from 'country-data';

import { getTokenPayload, getOrganizationBySlug, isLoginAdminUser, isSaler, ghostUserIsSaler } from 'services/auth-service.js';
import CampaignUploadLogo from './upload-logo.js';
import CampaignUploadCover from './upload-cover.js';
import { translate } from 'react-i18next';

const FormItem = Form.Item;
const Option = Select.Option;

const popoverContent = (
	<div style={{ width: 500 }}>
		<p>
			<strong>Storytelling:</strong> Let the influencer’s know this is something they can’t afford to miss.
			<br />
			<em>Tip:</em> Is your product or service eco-friendly? Is it healthy? What good does it do the consumer? These arguments will position the collaboration
			to be something beneficial for the influencer's engaged audience.
		</p>

		<p>
			<strong>Purpose:</strong> What’s the purpose of your campaign?
		</p>

		<p>
			<strong>Target/KPI:</strong> What do you wish to achieve with your campaign?
		</p>
		<ul>
			<li>Brand awareness</li>
			<li>Subscriptions (newsletter, membership)</li>
			<li>Sales & conversion</li>
			<li>Engagement</li>
		</ul>

		<p>
			<strong>Recommended guidelines:</strong>
		</p>
		<ul>
			<li>Start / end the caption with "in collaboration with" or #advertisement</li>
			<li>The image may not be removed for at least 30 days</li>
			<li>Do not post any other sponsored content the same day</li>
			<li>Delays regarding draft and publication may result in a reduction of payment</li>
			<li>The content should be at the top of the feed for at least 8 hours </li>
			<li>The brand’s entitled to re-publish and/or use content on other platforms royalty-free</li>
		</ul>
		<p>
			<em>*Attn: The more you’re asking, the more you’re expected to give..</em>
		</p>
	</div>
);

/*<FormItem {...formItemLayout} label="Use influencers from list" hasFeedback>
  {getFieldDecorator("select", {
    rules: [{ required: true, message: "Please select the list to use" }]
  })(
    <Select size="large" placeholder="Please select a list">
      <Option value="my-list-1">My influencer list</Option>
      <Option value="my-list-2">My other list</Option>
      <Option value="create-new">+ Create new list</Option>
    </Select>
  )}
</FormItem>*/

class CampaignForm extends React.Component {
	state = {
		visible: false,
		adminOverride: false,
		photoUploadCount: 0
	};

	hide = () => {
		this.setState({
			visible: false
		});
	};

	handleVisibleChange = (visible) => {
		this.setState({ visible });
	};

	handlePhotoUploading = () => {
		this.setState({ photoUploadCount: this.state.photoUploadCount + 1 }, () => {
			if (this.state.photoUploadCount === 1) this.props.onPhotoUploading();
		});
	};

	handlePhotoUploadDone = () => {
		this.setState({ photoUploadCount: this.state.photoUploadCount - 1 }, () => {
			if (this.state.photoUploadCount === 0) this.props.onPhotoUploadDone();
		});
	};

	handleAdminOverride = (e) => {
		const override = e.target.checked;
		this.props.enableSave(override);
		this.setState({ adminOverride: override });
	};

	componentDidUpdate(prevProps) {
		if (prevProps.campaign !== this.props.campaign && this.state.adminOverride === true) this.handleAdminOverride({ target: { checked: false } });
	}

	render() {
		const { campaign, form, organizationSlug, t } = this.props;
		const { adminOverride } = this.state;
		const { getFieldDecorator } = form;
		const formItemLayout = { labelCol: { span: 24 }, wrapperCol: { span: 24 } };
		let currencyAlertMessage = '';
		if (campaign && campaign.paymentCompensationsCount > 0) {
			currencyAlertMessage = t('currencyAlertMessageRemoveCommission');
		} else if (campaign && campaign.invoiceCompensationsCount > 0) {
			currencyAlertMessage = t('currencyAlertMessageRemoveInvoice');
		} else {
			currencyAlertMessage = t('currencyAlertMessageSupportedCurrencies');
		}

		const inputDisabled = campaign && campaign.invitesSent && !adminOverride;

		return (
			<Form className=''>
				{campaign && campaign.invitesSent && (
					<React.Fragment>
						{isLoginAdminUser() || isSaler() || ghostUserIsSaler() ? (
							<Alert
								message='Campaign is already live'
								description={
									<React.Fragment>
										<p>
											Campaign can't be edited after invites are sent. As admin, you can override this.
											<br />
											Make sure that all involved parties have agreed to any new terms.
										</p>
										<Checkbox value={true} checked={adminOverride} onChange={this.handleAdminOverride} style={{ color: 'red' }}>
											{adminOverride ? 'Using admin privilege to override' : 'Use admin privilege to override'}
										</Checkbox>
									</React.Fragment>
								}
								type={adminOverride ? 'error' : 'warning'}
							/>
						) : (
							<Alert message='Campaign can not be edited' description="Campaign can't be edited after invites are sent." type='warning' showIcon />
						)}
					</React.Fragment>
				)}
				<FormItem {...formItemLayout} label='Campaign name'>
					{getFieldDecorator('name', {
						rules: [
							{ required: true, whitespace: true, message: 'Please choose a name for your campaign' },
							{ max: 25, message: 'Use a shorter name for your campaign so it looks good everywhere. Limit is 25 character.' }
						]
					})(<Input size='large' placeholder='Campaign name' disabled={inputDisabled} />)}
				</FormItem>
				<FormItem {...formItemLayout} label='Brand name'>
					{getFieldDecorator('brandName', {
						initialValue: campaign ? campaign.organization && campaign.organization.name : getOrganizationBySlug(organizationSlug).name,
						rules: [
							{ required: true, whitespace: true, message: 'Please choose a brand name for your campaign' },
							{ max: 25, message: 'Use a shorter brand name for your campaign so it looks good everywhere. Limit is 25 character.' }
						]
					})(<Input size='large' placeholder='Brand name' disabled={inputDisabled} />)}
				</FormItem>
				<FormItem {...formItemLayout} label='Contact person'>
					{getFieldDecorator('contactPerson', {
						initialValue: getTokenPayload() && getTokenPayload().user && getTokenPayload().user.name,
						rules: [
							{ required: true, whitespace: true, message: 'Please write a name for the contact person' },
							{ max: 25, message: 'Use a shorter contact name so it looks good everywhere. Limit is 25 character.' }
						]
					})(<Input size='large' placeholder='John Johnsson' disabled={inputDisabled} />)}
				</FormItem>
				<FormItem {...formItemLayout} label='Notification Email (Your Own Email)'>
					{getFieldDecorator('notificationEmail', {
						initialValue: getTokenPayload() && getTokenPayload().user && getTokenPayload().user.email,
						rules: [
							{ type: 'email', message: 'The input is not valid E-mail!' },
							{ required: true, whitespace: true, message: 'Please write an email for the campaign notification' }
						]
					})(<Input size='large' placeholder='john@email.com' disabled={inputDisabled} />)}
				</FormItem>

				<FormItem {...formItemLayout} label='language'>
					{getFieldDecorator('language', {
						rules: [{ required: true, message: 'Please select which language your brief will have' }]
					})(
						<Select size='large' disabled={inputDisabled} placeholder='Select a language'>
							<Option value='en'>English</Option>
							<Option value='sv'>Swedish</Option>
						</Select>
					)}
				</FormItem>
				<FormItem {...formItemLayout} label='currency'>
					{getFieldDecorator('currency', {
						rules: [{ required: true, message: 'Please select which currency your producs should be presented as' }]
					})(
						<Select
							size='large'
							disabled={campaign && (inputDisabled || (campaign.paymentCompensationsCount > 0 || campaign.invoiceCompensationsCount > 0))}
							showSearch
							placeholder='Select a currency'
						>
							{currencies.all
								.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
								.map((currency) => (
									<Option key={currency.code} value={currency.code}>
										{currency.name} ({currency.code})
									</Option>
								))}
						</Select>
					)}
					<Alert showIcon type='info' className='mt-20' description={currencyAlertMessage} />
				</FormItem>
				<FormItem
					className='full-width-label'
					{...formItemLayout}
					label={
						<React.Fragment>
							<Popover content={popoverContent} title='Standard Brief' trigger='click' visible={this.state.visible} onVisibleChange={this.handleVisibleChange}>
								<a className='fr'>Show me tips!</a>
							</Popover>
							Brief (Visible to influencers)
						</React.Fragment>
					}
				>
					{getFieldDecorator('brief', {
						rules: [
							{ message: 'Please enter a brief for your campaign' },
							{ required: true, whitespace: true, message: 'Please choose a brand name for your campaign' }
						]
					})(
						<Input.TextArea
							size='large'
							rows={4}
							disabled={inputDisabled}
							placeholder='A brief for about your campaign and what it includes. Visible to influencers when joining the campaign.'
						/>
					)}
				</FormItem>
				<FormItem label='I want' className='form-item-inline'>
					{getFieldDecorator('influencerTargetCount', {
						initialValue: 10,
						rules: [{ required: true, message: 'Please enter a number between 1 and 500' }]
					})(<InputNumber size='large' disabled={inputDisabled} min={1} max={500} />)}
					<span> influencers to join my campaign</span>
				</FormItem>
				{false && (
					<FormItem {...formItemLayout} label='Use influencers from list' hasFeedback>
						{getFieldDecorator('select', {
							rules: [{ required: true, message: 'Please select the list to use' }]
						})(
							<Select size='large' disabled={inputDisabled} placeholder='Please select a list'>
								<Option value='my-list-1'>My influencer list</Option>
								<Option value='my-list-2'>My other list</Option>
								<Option value='create-new'>+ Create new list</Option>
							</Select>
						)}
					</FormItem>
				)}
				<FormItem label='What tags should be used?'>
					{getFieldDecorator('tags', {
						rules: [
							{
								validator: (rule, value, callback) => {
									let errors = [];

									if (
										value &&
										value.filter((val) => val.match(/^(?:#)([A-Za-z0-9_åäö](?:(?:[A-Za-z0-9_åäö]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_åäö]))?)$/g)).length !==
											value.length
									) {
										errors.push(new Error());
									}
									callback(errors);
								},
								message: 'A tag must start with # and can not be longer than 28 characters and can only contain a-z, 0-9, _ and dots.'
							},
							{
								validator: (rule, value, callback) => {
									let errors = [];

									if (value && value.length >= 5) {
										errors.push(new Error());
									}
									callback(errors);
								},
								message: 'Use 5 or less tags to not appear spammy. '
							}
						]
					})(
						<Select
							mode='tags'
							disabled={inputDisabled}
							style={{ width: '100%' }}
							size='large'
							tokenSeparators={[',', ' ']}
							placeholder='Tags #sampletag #myhashtag'
							notFoundContent='Enter a #hashtag'
						/>
					)}
				</FormItem>
				<FormItem label='What mentions should be used?'>
					{getFieldDecorator('mentions', {
						rules: [
							{
								validator: (rule, value, callback) => {
									let errors = [];

									if (
										value &&
										value.filter((val) => val.match(/^(?:@)([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)$/g)).length !== value.length
									) {
										errors.push(new Error());
									}
									callback(errors);
								},
								message: 'A mention must start with @ and can not be longer than 28 characters and can only contain a-z, 0-9, _ and dots.'
							},
							{
								validator: (rule, value, callback) => {
									let errors = [];

									if (value && value.length >= 5) {
										errors.push(new Error());
									}
									callback(errors);
								},
								message: 'Use 5 or less mentions to not appear spammy. '
							}
						]
					})(
						<Select
							mode='tags'
							style={{ width: '100%' }}
							size='large'
							tokenSeparators={[',', ' ']}
							disabled={inputDisabled}
							placeholder='Mentions @companyname @profile'
							notFoundContent='Enter a @mention'
						/>
					)}
				</FormItem>
				{!this.props.hideUploadFields && (
					<React.Fragment>
						<FormItem {...formItemLayout} label='Upload your company logo'>
							{getFieldDecorator('campaignLogoId')(
								<CampaignUploadLogo
									form={this.props.form}
									disabled={inputDisabled}
									onPhotoUploading={this.handlePhotoUploading}
									onPhotoUploadDone={this.handlePhotoUploadDone}
								/>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label='Upload cover photo used for campaign page'>
							{getFieldDecorator('campaignCoverPhotoId')(
								<CampaignUploadCover
									form={this.props.form}
									disabled={inputDisabled}
									onPhotoUploading={this.handlePhotoUploading}
									onPhotoUploadDone={this.handlePhotoUploadDone}
								/>
							)}
						</FormItem>
					</React.Fragment>
				)}
			</Form>
		);
	}
}

const WrappedCampaignForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const propFields = props.fields || {};
		let formFields = {};
		Object.keys(propFields).forEach((propFieldKey) => {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		});

		return formFields;
	}
})(CampaignForm);

export default translate('campaign')(WrappedCampaignForm);
