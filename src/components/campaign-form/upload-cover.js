import React, { Component } from 'react';
import { Upload, Icon, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';

import { firstOrganizationSlug } from 'services/auth-service';

const Dragger = Upload.Dragger;

const CREATE_CAMPAIGN_COVER_PHOTO = gql`
	mutation createCampaignCoverPhoto($coverPhoto: Upload!, $organizationSlug: String!) {
		createCampaignCoverPhoto(input: { coverPhoto: $coverPhoto, organizationSlug: $organizationSlug }) {
			campaignCoverPhoto {
				id
				url
			}
		}
	}
`;

const propsWithClient = ({ client, form, callbacks: { onPhotoUploading, onPhotoUploadDone } }) => ({
	name: 'file',
	multiple: false,
	accept: 'image/*',
	beforeUpload: (file) => {
		const sizeInMB = file.size / (1024 * 1024);

		if (sizeInMB > 10) {
			file.isExceedSize = true;
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');

			return false;
		}

		return true;
	},
	customRequest: ({ file, onError, onSuccess, onProgress }) => {
		onPhotoUploading && onPhotoUploading();
		client
			.mutate({
				mutation: CREATE_CAMPAIGN_COVER_PHOTO,
				variables: { coverPhoto: file, organizationSlug: firstOrganizationSlug() }
			})
			.then(({ data, loading, error }) => {
				if (error) onError();
				if (data) {
					onSuccess(data, file);

					form.setFields({
						campaignCoverPhotoId: {
							value: data.createCampaignCoverPhoto.campaignCoverPhoto.id
						}
					});
				}

				if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);

				onPhotoUploadDone && onPhotoUploadDone();
			})
			.catch(({ graphQLErrors }) => {
				onPhotoUploadDone && onPhotoUploadDone();
				graphQLErrors.forEach((error) => {
					message.error(error.message);
				});
				onError();
			});

		return {
			abort() {}
		};
	},
	onChange: (info) => {
		const status = info.file.status;

		if (info.file.isExceedSize) {
			info.fileList.splice(info.fileList.indexOf(info.file), 1);
		}
		if (status === 'done') {
			message.success(`${info.file.name} file uploaded successfully.`);
		} else if (status === 'error') {
			message.error(`${info.file.name} file upload failed.`);
		}
	}
});

export default class CampaignUploadCover extends Component {
	render() {
		const { onPhotoUploading, onPhotoUploadDone } = this.props;

		return (
			<ApolloConsumer>
				{(client) => (
					<Dragger {...propsWithClient({ client, form: this.props.form, callbacks: { onPhotoUploading, onPhotoUploadDone } })}>
						<p className='ant-upload-drag-icon'>
							<Icon type='cloud-upload' />
						</p>
						<p className='ant-upload-text'>Click or drag file to this area to upload</p>
						<p className='ant-upload-hint'>Supported file format is JPG and PNG. Cover photo should be 1340px * 560px. This is used for your landingpage.</p>
					</Dragger>
				)}
			</ApolloConsumer>
		);
	}
}
