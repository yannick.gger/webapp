import React from 'react';
import { Tooltip, Avatar } from 'antd';
import { shortAmountFormatter } from 'components/formatters';

export default ({ instagramOwner, avatarClass }) => {
	return (
		<Tooltip
			placement='topLeft'
			key={instagramOwner.id}
			title={`${instagramOwner.instagramOwnerUser.username} / ${shortAmountFormatter({ value: instagramOwner.followedByCount })}`}
		>
			<Avatar size='large' src={instagramOwner.instagramOwnerUser.avatar} icon='user' className={avatarClass} />
		</Tooltip>
	);
};
