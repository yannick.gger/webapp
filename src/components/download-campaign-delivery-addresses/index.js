import { Button, Card, Divider, Spin } from 'antd';
import { CSVLink } from 'components/csv';
import { lookup } from 'country-data';
import getCampaignDeliveryAddressesQuery from 'graphql/get-campaign-delivery-addresses.graphql';
import React from 'react';
import { Query } from 'react-apollo';

export default class DownloadCampaignDeliveryAddresses extends React.Component {
	render() {
		const { campaign } = this.props;
		return (
			<Card className='small-title mb-10 mt-10'>
				<Query query={getCampaignDeliveryAddressesQuery} variables={{ id: campaign.id }}>
					{({ data, error, loading }) => {
						if (loading) {
							return <Spin className='collabspin' />;
						}

						if (error) {
							return <span>Could not load CSV file</span>;
						}

						const csvData = data.campaign.deliveryAddresses.map(
							({ id, shippingAddress, addressLine2, name, email, shippingCity, shippingZip, shippingCountry, phone }) => ({
								id,
								name,
								email,
								shippingAddress,
								addressLine2,
								shippingCity,
								shippingZip,
								shippingCountry: lookup.countries({ alpha2: shippingCountry })[0].name,
								phone
							})
						);
						return (
							<CSVLink data={csvData} filename={`Collabs - ${data.campaign.name} - Addresses`}>
								<Button>Click here to download CSV file</Button>
							</CSVLink>
						);
					}}
				</Query>
				<Divider />
				<p>
					Hint: Here is a guide{' '}
					<a href='https://www.wikihow.com/Open-CSV-Files' target='_blank' rel='noopener noreferrer'>
						how to open CSV files
					</a>
				</p>
			</Card>
		);
	}
}
