import { connect } from 'react-redux';
import { fromJS } from 'immutable';

export const FlaggedFeature = ({ name, children, flags }) => children({ name, flags, enabled: !!flags[name] });

export const maptStateToProps = (state) => ({
	flags: (state.getIn(['currentUser', 'flags']) || fromJS({})).toJS()
});

export default connect(maptStateToProps)(FlaggedFeature);
