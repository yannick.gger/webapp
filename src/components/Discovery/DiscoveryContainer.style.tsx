import styled from 'styled-components';
import { CSSProperties } from 'react';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import fontSize from 'styles/variables/font-size';
import Grid from 'styles/grid/grid';
import typography from 'styles/variables/typography';
import { breakpoints } from 'styles/variables/media-queries';
import Icon from 'components/Icon';
import { Button } from 'components/Button';
import Row from 'styles/grid/row';

const Wrapper = styled(Row)`
	width: 100%;
`;

const Section = styled(Grid.Column)<{ height?: number; maxHeight?: number }>`
	height: ${(props) => props.height && `${props.height}px`};
	max-height: ${(props) => props.maxHeight && `${props.maxHeight}px`};
	border: 1px solid ${colors.borderGray};
`;

const SearchInputSection = styled.div`
	position: relative;
`;

const SearchInputWrapper = styled.div`
	margin-bottom: ${guttersWithRem.xxs};
`;

const Header = styled.div`
	width: 100%;
`;

const HeaderInnerWrapper = styled.div`
	max-width: 100%;
	margin-top: ${guttersWithRem.m};

	@media (min-width: ${breakpoints.lg}) {
		max-width: 1280px;
		width: calc(100vw - 21rem);
		margin: 0;
	}

	& > div {
		padding: 0;

		@media (min-width: ${breakpoints.lg}) {
			padding-left: 100px;
			padding-right: 200px;
		}
	}
`;

const CategoriesWrapper = styled.div`
	min-height: 50px;
	position: relative;

	&:after {
		content: '';
		position: absolute;
		z-index: 1;
		top: 0;
		right: 0;
		bottom: 0;
		pointer-events: none;
		background-image: linear-gradient(to right, rgba(255, 255, 255, 0), #f6efe4 85%);
		width: 2rem;
	}

	& > div {
		display: flex;
		column-gap: 8px;
		white-space: nowrap;
		overflow-x: scroll;
		overflow-y: hidden;
		position: relative;

		div {
			display: flex;
		}
	}
`;

const AutoCorrection = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-size: ${fontSize.s};
	margin-bottom: ${guttersWithRem.s};
`;

const FlexDiv = styled.div<CSSProperties>`
	display: flex;
	flex: ${(props) => props.flex};
	padding: ${(props) => props.padding};
	height: ${(props) => props.height};
	width: ${(props) => props.width};
	position: ${(props) => props.position};
	column-gap: ${(props) => props.columnGap};
	justify-content: ${(props) => props.justifyContent};
	align-items: ${(props) => props.alignItems};
	top: ${(props) => props.top};
	left: ${(props) => props.left};
	right: ${(props) => props.right};
	bottom: ${(props) => props.bottom};
	overflow-x: ${(props) => props.overflowX};
`;

const ButtonsWrapper = styled(FlexDiv)`
	flex-direction: column;
	width: 100%;
	margin-bottom: ${guttersWithRem.m};
	row-gap: ${guttersWithRem.m};

	@media (min-width: ${breakpoints.md}) {
		flex-direction: row;
		justify-content: space-between;
	}
`;

const CustomIcon = styled(Icon)``;

const CustomButton = styled(Button)`
	background-color: ${colors.transparent};

	${CustomIcon} {
		&.icon {
			margin-left: 0;
			line-height: 0;
		}
	}

	&.filter-opened {
		background-color: ${colors.discovery.black};
		color: ${colors.white};
		svg {
			fill: ${colors.white};
		}

		&:hover {
			svg {
				fill: ${colors.discovery.black};
			}
		}
	}
`;

const FilterSummaryWrapper = styled.div`
	display: flex;
	align-items: flex-end;
	column-gap: ${guttersWithRem.xxs};
`;

const Filters = styled.div`
	display: flex;
	max-width: 600px;
	flex-wrap: wrap;
	gap: ${guttersWithRem.xxs};
`;

const ResultContentWrapper = styled.div`
	display: flex;
	overflow: hidden;
`;

const SearchTotalResultCount = styled.span<{ executionTimeMs: number | null }>`
	position: relative;
	display: inline-block;

	& > span {
		&:hover:before {
			content: '(${(props) => props.executionTimeMs && props.executionTimeMs / 1000} seconds)		';
			white-space: nowrap;
			font-size: 10px;
			z-index: 10;
			position: absolute;
			bottom: -10px;
		}
	}
`;

const ClearFilterButton = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	cursor: pointer;
	color: ${colors.button.sencondary.color};
`;

const Styled = {
	Wrapper,
	Section,
	Header,
	HeaderInnerWrapper,
	CategoriesWrapper,
	SearchInputSection,
	SearchInputWrapper,
	AutoCorrection,
	ButtonsWrapper,
	CustomIcon,
	FlexDiv,
	CustomButton,
	ResultContentWrapper,
	SearchTotalResultCount,
	ClearFilterButton,
	FilterSummaryWrapper,
	Filters
};

export default Styled;
