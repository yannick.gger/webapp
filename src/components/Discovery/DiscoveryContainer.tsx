import { useEffect, useState, useRef, useContext } from 'react';
import classnames from 'classnames';

import DiscoveryContext from 'contexts/Discovery';
import DiscoveryService from 'services/DiscoveryApi';
import MainContent from 'styles/layout/main-content';
import Styled from './DiscoveryContainer.style';
import SearchInput from './Components/SearchInput';
import AutoCompleteList from './Components/AutoCompleteList';
import CategoryTag from './Components/CategoryTag';
import ResultContainer from './Components/ResultContainer';
import PageHeader from 'components/PageHeader';
import FilterDrawer from './Components/FilterDrawer';
import LoadingSpinner from 'components/LoadingSpinner';
import { SearchCateogryType, InfluencerListItemType } from 'contexts/Discovery/types';
import LookALikeLoading from './Components/LookALikeLoading';
import FilterTag from './Components/FilterTag';

/**
 * @todo i18next
 */
const DiscoveryContainer = () => {
	const {
		loading,
		lookALikeLoading,
		searchText,
		searchResult,
		categoryTags,
		changeSearchValueHandler,
		addSearchCategoryHandler,
		lookALikeInfluencerName,
		removeLookALikeHandler,
		searchTotalResult,
		filter,
		resetFilter,
		removeOneFilter
	} = useContext(DiscoveryContext);
	const [cursor, setCursor] = useState(-1);
	const [inputValue, setInputValue] = useState<string | null>(null);
	const [autoCompleteOptions, setAutoCompleteOptions] = useState([]);
	const [showFilter, setShowFilter] = useState(false);
	const [isSearching, setIsSearching] = useState(false);
	const [selectedInfluencers, setSelectedInfluencers] = useState<string[]>([]);
	const [timer, setTimer] = useState<any>(0);

	const CancelSignal = new AbortController();
	const cancel = useRef<any>(null);
	const cursoredItem = useRef<{ text: string; categoryId: string }>({ text: '', categoryId: '' });

	useEffect(() => {
		if (searchText) {
			setInputValue(searchText);
		} else {
			setInputValue(null);
		}
	}, [searchText]);

	const getAutoCompleteOptionsHandler = () => {
		if (timer) {
			clearTimeout(timer);
		}

		const newTimer = setTimeout(() => {
			if (inputValue && inputValue.length >= 2 && isSearching) {
				cancel.current = CancelSignal;
				DiscoveryService.autoCompleteSearch(inputValue, cancel.current.signal)
					.then((res) => {
						setAutoCompleteOptions(
							res.data.map((category: { attributes: { name?: string; username?: string }; id: string; type: string }) => {
								let label = category.attributes.name;
								let isInfluencer = false;

								if (category.attributes.username) {
									label = category.attributes.username;
									isInfluencer = true;
								}

								return {
									id: category.id,
									label: label,
									isInfluencer: isInfluencer
								};
							})
						);
						cancel.current = null;
					})
					.catch((err) => {
						console.log(err);
					});
			} else {
				setAutoCompleteOptions([]);
			}
		}, 400);
		setTimer(newTimer);
	};

	const keyboardNavigation = (e: React.KeyboardEvent<HTMLInputElement>) => {
		if (e.key === 'ArrowDown') {
			e.preventDefault();
			if (autoCompleteOptions.length > 0) {
				setCursor((prev) => {
					return prev < autoCompleteOptions.length - 1 ? prev + 1 : prev;
				});
			}
		}
		if (e.key === 'ArrowUp') {
			e.preventDefault();
			if (autoCompleteOptions.length > 0) {
				setCursor((prev) => {
					return prev > 0 ? prev - 1 : 0;
				});
			}
		}
		if (e.key === 'Escape') {
			setCursor(-1);
			setAutoCompleteOptions([]);
		}
		if (e.key === 'Enter') {
			if (cursor !== -1) {
				changeSearchValueHandler({ text: cursoredItem.current.text, categoryId: cursoredItem.current.categoryId });
				setInputValue(cursoredItem.current.text);
			} else {
				changeSearchValueHandler({ text: inputValue, categoryId: '' });
			}
			setAutoCompleteOptions([]);
		}
	};

	const changeHandler = (searchWord: string) => {
		if (inputValue !== searchWord) {
			setInputValue(searchWord);
			setIsSearching(true);
		}
	};

	const selectOptionHandler = (id: string, label: string, isInfluencer: boolean) => {
		let newLabel = label;
		if (isInfluencer) {
			newLabel = `@${label}`;
		}
		setInputValue(newLabel);
		changeSearchValueHandler({ text: newLabel, categoryId: id });
	};

	const cursorOptionHandler = (id: string, label: string, isInfluencer: boolean) => {
		let newLabel = label;
		if (isInfluencer) {
			newLabel = `@${label}`;
		}
		cursoredItem.current = { text: newLabel, categoryId: id };
	};

	const resetSelectedInfluencers = () => {
		setSelectedInfluencers([]);
	};

	const selectAllHandler = () => {
		if (searchResult) {
			if (selectedInfluencers.length === searchResult.length) {
				resetSelectedInfluencers();
			} else {
				setSelectedInfluencers(searchResult.map((influencer: InfluencerListItemType) => influencer.id));
			}
		}
	};

	const selectOneHandler = () => {
		return (id: string) => {
			setSelectedInfluencers((prev) => {
				const isExist = prev.some((prevId) => prevId === id);
				if (isExist) {
					return prev.filter((prevId) => prevId !== id);
				} else {
					return prev.concat(id);
				}
			});
		};
	};

	const clickCategoryHandler = (category: { id: string; name: string; level: number }) => {
		addSearchCategoryHandler({
			text: category.name,
			categoryId: category.id
		});
	};

	useEffect(() => {
		setCursor(-1);
		if (inputValue && inputValue.length === 0) {
			setAutoCompleteOptions([]);
		}
		if (cancel.current !== null) {
			cancel.current.abort();
			cancel.current = null;
		}

		getAutoCompleteOptionsHandler();
	}, [inputValue]);

	useEffect(() => {
		setSelectedInfluencers([]);
	}, [searchResult]);

	useEffect(() => {
		if (lookALikeLoading) {
			window.scrollTo(0, 0);
		}
	}, [lookALikeLoading]);

	return (
		<Styled.Wrapper>
			<Styled.Header>
				<PageHeader headline='Discovery'>
					<Styled.HeaderInnerWrapper>
						<div>
							<Styled.SearchInputSection>
								<Styled.SearchInputWrapper>
									<SearchInput
										value={inputValue ? inputValue : ''}
										onChange={(e) => changeHandler(e.target.value)}
										onKeyDown={(e) => {
											keyboardNavigation(e);
										}}
										onReset={() => {
											changeHandler('');
										}}
										placeholder={lookALikeInfluencerName ? '' : 'Search keyword or @username.'}
										lookALike={lookALikeInfluencerName}
										removeLookALike={removeLookALikeHandler}
									/>
								</Styled.SearchInputWrapper>

								{autoCompleteOptions.length > 0 && (
									<AutoCompleteList
										cursor={cursor}
										options={autoCompleteOptions}
										onSelect={(id, label, isInfluencer) => {
											selectOptionHandler(id, label, isInfluencer);
											setAutoCompleteOptions([]);
											setIsSearching(false);
										}}
										onCursor={(id, label, isInfluencer) => {
											cursorOptionHandler(id, label, isInfluencer);
											setIsSearching(false);
										}}
									/>
								)}
								{/* @todo: autoCorrection */}
								{/* <Styled.AutoCorrection>Did you mean ... ?</Styled.AutoCorrection> */}
							</Styled.SearchInputSection>
							<Styled.CategoriesWrapper>
								{categoryTags && categoryTags.length > 0 ? (
									<>
										<div>
											{categoryTags.map((category: SearchCateogryType) => {
												let textSum = 0;
												for (let i = 0; i < category.name.length; i++) {
													textSum += category.name.charCodeAt(i);
												}

												return (
													<div
														onClick={() => {
															clickCategoryHandler(category);
														}}
														key={category.id}
													>
														<CategoryTag text={category.name} size='m' score={textSum % 100} />
													</div>
												);
											})}
										</div>
									</>
								) : null}
							</Styled.CategoriesWrapper>
						</div>
					</Styled.HeaderInnerWrapper>
				</PageHeader>

				<MainContent>
					<Styled.ButtonsWrapper>
						<Styled.FlexDiv columnGap='1rem' alignItems='flex-end'>
							<Styled.CustomButton onClick={() => {}}>
								<Styled.CustomIcon icon='my-lists' size='24' />
								My Lists
							</Styled.CustomButton>
							<Styled.CustomButton
								onClick={() => {
									setShowFilter((prev) => !prev);
								}}
								className={classnames({ 'filter-opened': showFilter })}
							>
								<Styled.CustomIcon icon='filter' size='24' />
								Filters
							</Styled.CustomButton>
							{filter !== null && Object.values(filter).some((el) => el !== null && el !== '') ? (
								<Styled.FilterSummaryWrapper>
									<Styled.Filters>
										{Object.entries(filter)
											.filter(([key, value]) => value !== null && value !== '')
											.map(([key, value]) => key)
											.map((filterKey) => {
												return (
													<FilterTag
														key={filterKey}
														filterKey={filterKey}
														onRemove={() => {
															removeOneFilter(filterKey);
														}}
													/>
												);
											})}
									</Styled.Filters>
									<Styled.ClearFilterButton onClick={resetFilter}>Clear all filters</Styled.ClearFilterButton>
								</Styled.FilterSummaryWrapper>
							) : null}
						</Styled.FlexDiv>
						<Styled.FlexDiv alignItems='center' columnGap='1rem'>
							{searchTotalResult && searchTotalResult.totalResultCount !== null && (
								<Styled.SearchTotalResultCount executionTimeMs={searchTotalResult.executionTimeMs}>
									<span>
										Result <strong>{searchTotalResult.totalResultCount.toLocaleString()} influencers</strong>
									</span>
								</Styled.SearchTotalResultCount>
							)}
						</Styled.FlexDiv>
					</Styled.ButtonsWrapper>
				</MainContent>
			</Styled.Header>

			<MainContent>
				<Styled.ResultContentWrapper>
					<FilterDrawer
						isVisible={showFilter}
						onHide={() => {
							setShowFilter(false);
						}}
					/>

					{lookALikeLoading ? (
						<LookALikeLoading />
					) : loading ? (
						<LoadingSpinner size={'lg'} position={'center'} />
					) : (
						<ResultContainer
							isInfluencerSearch={inputValue && inputValue.startsWith('@') ? true : false}
							tableList={searchResult || []}
							selectedInfluencers={selectedInfluencers}
							onSelectAll={selectAllHandler}
							onSelectOne={selectOneHandler}
							onResetSelection={resetSelectedInfluencers}
						/>
					)}
				</Styled.ResultContentWrapper>
			</MainContent>
		</Styled.Wrapper>
	);
};

export default DiscoveryContainer;
