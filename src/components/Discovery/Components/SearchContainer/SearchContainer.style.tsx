import styled from 'styled-components';
import typography from 'styles/variables/typography';
import fontSize from 'styles/variables/font-size';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import { breakpoints } from 'styles/variables/media-queries';

const Wrapper = styled.div`
	width: 100%;
`;

const InnerWrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
`;

const CatchPhrase = styled.div`
	width: 100%;
	text-align: center;
	font-family: ${typography.BaseFontFamiliy};
	font-size: ${typography.headings.h4.fontSize};
	font-weight: ${typography.headings.h4.fontWeight};

	margin-bottom: ${guttersWithRem.xxxl};
`;

const SearchInputWrapper = styled.div`
	position: relative;

	margin: 0;

	@media (min-width: ${breakpoints.md}) {
		margin: 0 15%;
	}
`;

const SearchInnerWrapper = styled.div`
	margin-bottom: ${guttersWithRem.xxs};
`;

const AdvancedSearchWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: flex-end;
	font-family: ${typography.BaseFontFamiliy};
	font-size: ${fontSize.l};
	margin-top: ${guttersWithRem.xs};
	margin-bottom: ${guttersWithRem.xxxl};

	& > a {
		color: ${colors.discovery.link};
	}
`;

const DefaultImageWrapper = styled.div`
	max-width: 500px;
	margin: 0 auto;
`;

const Styled = {
	Wrapper,
	InnerWrapper,
	CatchPhrase,
	SearchInputWrapper,
	SearchInnerWrapper,
	AdvancedSearchWrapper,
	DefaultImageWrapper
};

export default Styled;
