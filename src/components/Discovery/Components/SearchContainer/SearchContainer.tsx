import React, { useState, useRef, useEffect } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';

import DiscoveryService from 'services/DiscoveryApi';

import SearchInput from '../SearchInput';
import AutoCompleteList from '../AutoCompleteList';
import MainContent from 'styles/layout/main-content';

import Styled from './SearchContainer.style';
import EmptyResultImg from 'assets/img/app/empty-state-discovery.svg';

/**
 * @todo i18next
 */
const SearchContainer = () => {
	const [inputValue, setInputValue] = useState('');
	const [searchValue, setSearchValue] = useState<{ text: string; categoryId: string }>({ text: '', categoryId: '' });
	const [cursor, setCursor] = useState(-1);
	const [autoCompleteOptions, setAutoCompleteOptions] = useState([]);
	const [timer, setTimer] = useState<any>(0);
	const CancelSignal = new AbortController();
	const cancel = useRef<any>(null);

	const cursoredItem = useRef<{ text: string; categoryId: string }>({ text: '', categoryId: '' });

	const history = useHistory();
	const location = useLocation();

	const keyboardNavigation = (e: React.KeyboardEvent<HTMLInputElement>) => {
		if (e.key === 'ArrowDown') {
			e.preventDefault();
			if (autoCompleteOptions.length > 0) {
				setCursor((prev) => {
					return prev < autoCompleteOptions.length - 1 ? prev + 1 : prev;
				});
			}
		}
		if (e.key === 'ArrowUp') {
			e.preventDefault();
			if (autoCompleteOptions.length > 0) {
				setCursor((prev) => {
					return prev > 0 ? prev - 1 : 0;
				});
			}
		}
		if (e.key === 'Escape') {
			setCursor(-1);
			setAutoCompleteOptions([]);
		}
		if (e.key === 'Enter') {
			if (cursor !== -1) {
				setSearchValue({ text: cursoredItem.current.text, categoryId: cursoredItem.current.categoryId });
			} else {
				setSearchValue({ text: inputValue, categoryId: '' });
			}
		}
	};

	const autoCompleteHandler = () => {
		if (timer) {
			clearTimeout(timer);
		}

		const newTimer = setTimeout(() => {
			if (inputValue.length >= 2) {
				cancel.current = CancelSignal;
				DiscoveryService.autoCompleteSearch(inputValue, cancel.current.signal)
					.then((res) => {
						setAutoCompleteOptions(
							res.data.map((category: { attributes: { name?: string; username?: string }; id: string; type: string }) => {
								let label = category.attributes.name;
								let isInfluencer = false;

								if (category.attributes.username) {
									label = category.attributes.username;
									isInfluencer = true;
								}

								return {
									id: category.id,
									label: label,
									isInfluencer: isInfluencer
								};
							})
						);
						cancel.current = null;
					})
					.catch((err) => {
						console.log(err);
					});
			}
		}, 400);
		setTimer(newTimer);
	};

	const changeHandler = (searchWord: string) => {
		setInputValue(searchWord);
	};

	const selectOptionHandler = (id: string, label: string, isInfluencer: boolean) => {
		let newLabel = label;
		if (isInfluencer) {
			newLabel = `@${label}`;
		}
		setSearchValue({ text: newLabel, categoryId: id });
	};

	const cursorOptionHandler = (id: string, label: string, isInfluencer: boolean) => {
		let newLabel = label;
		if (isInfluencer) {
			newLabel = `@${label}`;
		}
		cursoredItem.current = { text: newLabel, categoryId: id };
	};

	useEffect(() => {
		setCursor(-1);
		if (inputValue.length === 0) {
			setAutoCompleteOptions([]);
		}
		if (cancel.current !== null) {
			cancel.current.abort();
			cancel.current = null;
		}

		autoCompleteHandler();
	}, [inputValue]);

	useEffect(() => {
		if (searchValue.text !== '' && searchValue.categoryId !== '') {
			history.push(`${location.pathname}/search?q=${searchValue.text}&categories=${searchValue.categoryId}`);
		} else if (searchValue.text !== '' && searchValue.categoryId === '') {
			history.push(`${location.pathname}/search?q=${searchValue.text}`);
		}
	}, [searchValue]);

	return (
		<Styled.Wrapper>
			<MainContent>
				<Styled.CatchPhrase>What do you want to promote?</Styled.CatchPhrase>

				<Styled.SearchInputWrapper>
					<Styled.SearchInnerWrapper>
						<SearchInput
							value={inputValue}
							onChange={(e) => {
								changeHandler(e.target.value);
							}}
							onReset={() => {
								changeHandler('');
							}}
							placeholder='Search keyword or @username.'
							onKeyDown={(e) => {
								keyboardNavigation(e);
							}}
						/>
					</Styled.SearchInnerWrapper>
					{autoCompleteOptions.length > 0 && (
						<AutoCompleteList
							cursor={cursor}
							options={autoCompleteOptions}
							onSelect={(id, label, isInfluencer) => {
								selectOptionHandler(id, label, isInfluencer);
							}}
							onCursor={(id, label, isInfluencer) => {
								cursorOptionHandler(id, label, isInfluencer);
							}}
						/>
					)}
					<Styled.AdvancedSearchWrapper>
						<Link to={`${location.pathname}/search`}>Advanced Search</Link>
					</Styled.AdvancedSearchWrapper>
				</Styled.SearchInputWrapper>
				<Styled.DefaultImageWrapper>
					<img src={EmptyResultImg} alt='discovery landing page' />
				</Styled.DefaultImageWrapper>
			</MainContent>
		</Styled.Wrapper>
	);
};

export default SearchContainer;
