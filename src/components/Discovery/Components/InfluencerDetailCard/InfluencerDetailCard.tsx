import { useState, useEffect, useContext } from 'react';
import DiscoveryContext from 'contexts/Discovery';

import { formatNumberWithDecimals } from 'shared/helpers/Chart/chart-util';
import ProfileCard from '../ProfileCard';
import Grid from 'styles/grid/grid';
import Styled from './InfluencerDetailCard.style';
import colors from 'styles/variables/colors';
import Modal from 'components/Modal';

import CountrySummaryList from '../CountrySummaryList';
import GenderData from '../GenderData';
import { InfluencerListItemType } from 'contexts/Discovery/types';
import LoadingSpinner from 'components/LoadingSpinner';
import Icon from '../../../Icon';

const convertDateFormat = (value: string) => {
	const months: { [key: number]: string } = {
		0: 'Jan',
		1: 'Feb',
		2: 'Mar',
		3: 'Apr',
		4: 'May',
		5: 'Jun',
		6: 'Jul',
		7: 'Aug',
		8: 'Sep',
		9: 'Oct',
		10: 'Nov',
		11: 'Dec'
	};
	const targetDate = new Date(value);
	let date = targetDate.getDate();
	let month = months[targetDate.getMonth()];
	let year = targetDate.getFullYear();

	return `${date} ${month}, ${year}`;
};

const InfluencerDetailCard = (props: { influencer: InfluencerListItemType; onSelect: () => void; isSelected: boolean }) => {
	const [datasets, setDatasets] = useState<{ total: number; data: number[]; labels: string[] }>({
		total: 0,
		labels: ['Women', 'Men', 'Unknown'],
		data: [100, 200, 5]
	});
	const [countryData, setCountryData] = useState<{ name: string; followers: number }[]>([]);
	const { lookALikeHandler, getReason } = useContext(DiscoveryContext);
	const [showReasonModal, setShowReasonModal] = useState(false);
	const [loading, setLoading] = useState(false);
	const [instagramPosts, setInstagramPosts] = useState<any[] | null>(null);

	const showReasonHandler = (influencer: any) => {
		setShowReasonModal(true);
		setLoading(true);
		getReason(influencer.instagramPostsLink).then((res: { data: { attributes: { posts: any[] } } }) => {
			if (res) {
				setInstagramPosts(res.data.attributes.posts);
			}
			setLoading(false);
		});
	};

	useEffect(() => {
		if (props.influencer.audienceDetail) {
			if (props.influencer.audienceDetail.genders) {
				const data = Object.values(props.influencer.audienceDetail.genders).map((genderValue: { count: number | null; ratio: number | null }) =>
					genderValue.count !== null ? genderValue.count : 0
				);
				const labels = Object.keys(props.influencer.audienceDetail.genders).map((el) => {
					switch (el) {
						case 'male':
						case 'man':
						case 'men':
							return 'Men';
						case 'female':
						case 'woman':
						case 'women':
							return 'Women';
						case 'unknown':
							return 'Unknown';
						default:
							return '';
					}
				});
				setDatasets({ total: props.influencer.followersCount, labels: labels, data: data });
			}
			if (props.influencer.audienceDetail.countries) {
				const countries = props.influencer.audienceDetail.countries;
				const data = Object.keys(countries).map((country) => {
					return { name: country.toLocaleUpperCase(), followers: countries[country].count || 0 };
				});
				setCountryData(data);
			}
		}
	}, [props.influencer]);

	// @todo: i18next
	return (
		<>
			<Styled.Wrapper>
				<Styled.FlexDiv
					className='header'
					justifyContent='flex-end'
					alignItems='center'
					columnGap='1rem'
					onClick={(e) => {
						e.stopPropagation();
					}}
				>
					<Styled.FlexDiv className='header-buttons'>
						<Styled.CustomButton size='sm' onClick={() => {}}>
							<Styled.CustomIcon icon='comment' size='16' />
							No comments
						</Styled.CustomButton>

						<Styled.CustomButton
							size='sm'
							onClick={() => {
								lookALikeHandler(props.influencer);
							}}
						>
							<Styled.CustomIcon icon='look-a-like' size='16' />
							Lookalike
						</Styled.CustomButton>
					</Styled.FlexDiv>
				</Styled.FlexDiv>

				<Grid.Container
					onClick={(e) => {
						e.stopPropagation();
					}}
				>
					<Grid.Column md={3}>
						<ProfileCard data={props.influencer} />
					</Grid.Column>

					<Grid.Column md={3}>
						<Styled.DataContainer minHeight='230px'>
							<Styled.DataBlockTitle>Age span of followers</Styled.DataBlockTitle>
							{props.influencer.audienceDetail
								? Object.entries(props.influencer.audienceDetail.ages).map(([key, value], index) => {
										return (
											<Styled.AgeSpanDataWrapper key={key} className={index % 2 ? '' : 'odd'}>
												<span>{key}</span>
												<span>{value.ratio !== null ? formatNumberWithDecimals(value.ratio, '%') : '-'}</span>
											</Styled.AgeSpanDataWrapper>
										);
								  })
								: null}
						</Styled.DataContainer>
					</Grid.Column>

					<Grid.Column md={3}>
						<Styled.DataContainer minHeight='230px'>
							<Styled.DataBlockTitle>Top 5 countries</Styled.DataBlockTitle>
							<CountrySummaryList items={countryData} totalFollowers={props.influencer.followersCount} sign='%' isTopThree={false} />
						</Styled.DataContainer>
					</Grid.Column>

					<Grid.Column md={3}>
						<Styled.DataContainer minHeight='230px'>
							<Styled.DataBlockTitle>Gender</Styled.DataBlockTitle>
							<Styled.GenderDataContainer>
								<GenderData data={datasets} colors={[colors.chartPrimary, colors.chartSecondary, colors.chartTertiary]} />
							</Styled.GenderDataContainer>
						</Styled.DataContainer>
					</Grid.Column>

					{props.influencer.brandAffiliations && props.influencer.brandAffiliations.length > 0 ? (
						<Grid.Column>
							<Styled.DataContainer minHeight='85px' className='brand-affiliations'>
								<Styled.DataBlockTitle>Brand affiliations</Styled.DataBlockTitle>

								<Grid.Container>
									{props.influencer.brandAffiliations.map(({ username, url, postedAt }, index) => {
										if (index < 5) {
											return (
												<Grid.Column md={3} key={username}>
													<Styled.FlexDiv className='brand-affiliation-item'>
														<div>{username}</div>
														<Styled.LinkToPost href={url} target='_blank'>
															View post
														</Styled.LinkToPost>
													</Styled.FlexDiv>
													<Styled.DateInfo>{`Last ${convertDateFormat(postedAt)}`}</Styled.DateInfo>
												</Grid.Column>
											);
										}
									})}
								</Grid.Container>
							</Styled.DataContainer>
						</Grid.Column>
					) : null}
					{props.influencer.instagramPostsLink ? (
						<Grid.Column>
							<Styled.FooterWrapper
								onClick={() => {
									showReasonHandler(props.influencer);
								}}
							>
								<Styled.CustomIcon icon='tooltip' size='24' />
								<span>Why do I get this result?</span>
							</Styled.FooterWrapper>
						</Grid.Column>
					) : null}
				</Grid.Container>
			</Styled.Wrapper>
			<Modal
				open={showReasonModal}
				size='xs'
				handleClose={() => {
					setShowReasonModal(false);
				}}
			>
				<Modal.Header>
					<Styled.ModalHeader>
						<h2>Why do I get this result?</h2>
						<button onClick={() => setShowReasonModal(false)}>
							<Icon icon='cross' />
						</button>
					</Styled.ModalHeader>
				</Modal.Header>
				<Modal.Body>
					{loading ? (
						<LoadingSpinner size='lg' />
					) : (
						<div>
							{instagramPosts && instagramPosts.length > 0 ? (
								<>
									<div>
										<p>Many factors are used to create a search result. The following posts have contributed to the score of this result.</p>
									</div>
									<Styled.PostItemWrapper>
										{instagramPosts.map((post) => {
											return (
												<Styled.PostItem key={post.id}>
													<div>
														<Styled.ScoreText>{`Score : ${formatNumberWithDecimals(post.score)}`}</Styled.ScoreText>
														<Styled.DateInfo>{`Last ${convertDateFormat(post.date)}`}</Styled.DateInfo>
													</div>
													<Styled.LinkToPost href={post.url} target='_blank'>
														View post
													</Styled.LinkToPost>
												</Styled.PostItem>
											);
										})}
									</Styled.PostItemWrapper>
								</>
							) : (
								<div>
									Failed to fetch posts.
									<br />
									They might have been a temporary issues or the posts are deleted
								</div>
							)}
						</div>
					)}
				</Modal.Body>
			</Modal>
		</>
	);
};

export default InfluencerDetailCard;
