import { CSSProperties } from 'react';
import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import Icon from 'components/Icon';
import Dropdown from 'components/Dropdown';
import { Button } from 'components/Button';

const dataContainerPadding = `${guttersWithRem.s} ${guttersWithRem.xs}`;

const Wrapper = styled.div``;

const FlexDiv = styled.div<CSSProperties>`
	font-family: ${typography.BaseFontFamiliy};
	display: flex;
	flex: ${(props) => props.flex};
	padding: ${(props) => props.padding};
	height: ${(props) => props.height};
	position: ${(props) => props.position};
	column-gap: ${(props) => props.columnGap};
	justify-content: ${(props) => props.justifyContent};
	align-items: ${(props) => props.alignItems};
	top: ${(props) => props.top};
	left: ${(props) => props.left};
	right: ${(props) => props.right};
	bottom: ${(props) => props.bottom};

	&.header {
		margin-bottom: ${guttersWithRem.xs};
	}
	&.comming-soon-icons {
		padding: 0 1.25rem;
		position: relative;
		cursor: not-allowed;

		&:hover:after {
			content: 'comming soon';
			white-space: nowrap;
			font-size: 8px;
			padding: 0.25rem 0.5rem;
			position: absolute;
			top: -15px;
			left: 40px;
			z-index: 10;
		}
	}

	&.brand-affiliation-item {
		justify-content: space-between;
		background-color: ${colors.discovery.grayBackground};
		line-height: 1;
		padding: 0.75rem ${guttersWithRem.xs};
	}

	&.header-buttons {
		column-gap: ${guttersWithRem.xs};
	}
`;

const DataContainer = styled.div<CSSProperties>`
	min-height: ${(props) => props.minHeight};
	min-width: ${(props) => props.minWidth};
	max-height: ${(props) => props.maxHeight};
	max-width: ${(props) => props.width};
	padding: ${dataContainerPadding};
	overflow-x: ${(props) => props.overflowX};
	overflow-y: ${(props) => props.overflowY};
	padding-top: 2rem;

	&.brand-affiliations {
		padding-top: ${guttersWithRem.m};
	}
`;

const GenderDataContainer = styled.div<CSSProperties>`
	display: flex;
	align-items: center;
	position: relative;
	min-width: 280px;
`;

const DataBlockTitle = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-weight: 600;
	line-height: 1;
	margin-bottom: ${guttersWithRem.s};
`;

const CustomIcon = styled(Icon)`
	&.icon {
		margin-left: 0;
		line-height: 0;
	}
`;

const DropdownWrapper = styled.div`
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.xs};
	border: solid 2px ${colors.button.primary.borderColor};
	padding: 0.25rem 0.5rem;

	&:hover:not(:disabled) {
		background-color: ${colors.button.primary.backgroundHover};
		color: ${colors.button.primary.color};
	}
`;

const CustomDropdown = styled(Dropdown)`
	font-family: ${typography.SecondaryFontFamiliy};

	& button {
		padding: 0px;

		&:hover,
		&.show {
			background-color: unset;
		}
	}
`;

const DropdownMenuWrapper = styled.div`
	position: absolute;
	top: -8px;
	right: -10px;
`;

const AgeSpanDataWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	padding: 0.75rem ${guttersWithRem.xs};
	line-height: 1;

	&.odd {
		background-color: ${colors.discovery.grayBackground};
	}
`;

const LinkToPost = styled.a`
	font-family: ${typography.SecondaryFontFamiliy};
	text-decoration: none;
	color: ${colors.button.sencondary.color};
	border: none;
`;

const DateInfo = styled.div`
	font-size: ${typography.label.small.fontSize};
	color: ${colors.labelColor};
	padding: 0 ${guttersWithRem.xs};
`;

const CustomButton = styled(Button)`
	background-color: ${colors.transparent};
`;

const FooterWrapper = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	display: flex;
	align-items: center;
	justify-content: flex-end;
	column-gap: ${guttersWithRem.m};
	color: ${colors.button.sencondary.color};
	cursor: pointer;
`;

const ModalHeader = styled.div`
	display: flex;

	button {
		background: none;
		border: none;
		padding: 0;
		margin-left: auto;
		cursor: pointer;
	}
`;

const PostItemWrapper = styled.div`
	max-height: 400px;
	overflow-y: auto;
`;

const PostItem = styled.div`
	width: 100%;
	min-height: 60px;
	display: flex;
	align-items: center;
	justify-content: space-between;
`;

const ScoreText = styled.div`
	padding: 0 ${guttersWithRem.xs};
`;

const Styled = {
	Wrapper,
	CustomIcon,
	DataContainer,
	GenderDataContainer,
	DataBlockTitle,
	CustomDropdown,
	DropdownMenuWrapper,
	DropdownWrapper,
	FlexDiv,
	AgeSpanDataWrapper,
	DateInfo,
	LinkToPost,
	CustomButton,
	FooterWrapper,
	ModalHeader,
	PostItem,
	PostItemWrapper,
	ScoreText
};

export default Styled;
