import { useState, useEffect, useContext } from 'react';
import classnames from 'classnames';

import DiscoveryContext from 'contexts/Discovery';
import { formatNumberWithDecimals } from 'shared/helpers/Chart/chart-util';
import { guttersWithRem } from 'styles/variables/gutter';
import Styled from './InfluencerListItem.style';
import { formatNumber } from 'shared/helpers/Chart/chart-util';
import Checkbox from 'components/Checkbox';
import InfluencerDetailCard from '../InfluencerDetailCard';
import { InfluencerListItemType } from 'contexts/Discovery/types';
import DiscoveryDropdown from '../DiscoveryDropdown';

const InfluencerListItem = (props: { onSelect: () => void; data: any; isSelected: boolean }) => {
	const { getOneInfluencerExtraData } = useContext(DiscoveryContext);
	const [influencerData, setInfluencerData] = useState<InfluencerListItemType>({
		id: '',
		age: '',
		country: '',
		followersCount: 0,
		gender: '',
		network: '',
		networkId: '',
		networkLinks: {
			instagram: ''
		},
		profileImageUrl: '',
		username: '',
		audienceBrief: {
			age: { name: null, count: null, ratio: null },
			country: { name: null, count: null, ratio: null },
			gender: { name: null, count: null, ratio: null }
		},
		audienceDetail: {
			ages: {
				'13-17': { count: 0, ratio: 0 },
				'18-24': { count: 0, ratio: 0 },
				'25-34': { count: 0, ratio: 0 },
				'35-44': { count: 0, ratio: 0 },
				'45-54': { count: 0, ratio: 0 },
				'55-64': { count: 0, ratio: 0 },
				'65+': { count: 0, ratio: 0 }
			},
			countries: {},
			genders: {
				female: { count: 0, ratio: 0 },
				male: { count: 0, ratio: 0 },
				unknown: { count: 0, ratio: 0 }
			}
		},
		brandAffiliations: [],
		categories: [],
		averageViews: 0,
		topicScore: 0,
		relevancy: 0,
		engagement: 0,
		engagementOrganic: 0,
		engagementPaid: 0,
		paidPostsRatio: 0
	});
	const [showDetail, setShowDetail] = useState(false);
	const [showInstagramLink, setShowInstagramLink] = useState(false);

	const getInfluencerDetail = () => {
		if (!showDetail) {
			getOneInfluencerExtraData(props.data.id).then((res: {}) => {
				setInfluencerData((prev) => {
					return { ...prev, ...res };
				});
			});
		}
		setShowDetail((prev) => !prev);
	};

	useEffect(() => {
		if (props.data) {
			setInfluencerData((prev) => ({ ...prev, ...props.data }));
		}
	}, [props.data]);

	const getNameAndRatio = (param: { name?: string | null; count: number | null; ratio: number | null }) => {
		if (param.name !== null && param.ratio !== null) {
			return `${param.name!.toLocaleUpperCase()} ${Math.floor(param.ratio * 100)}%`;
		}
	};

	return (
		<>
			<Styled.Wrapper>
				<Styled.Td>
					<Styled.Div display='flex'>
						<Checkbox
							checked={props.isSelected}
							onChange={(e) => {
								e.stopPropagation();
								props.onSelect();
							}}
						/>
					</Styled.Div>
				</Styled.Td>
				<Styled.Td maxWidth='250px'>
					<Styled.Div display='flex' alignItems='center' columnGap={guttersWithRem.m} overflowX='auto'>
						<span>
							<Styled.CustomAvatar imageUrl={influencerData.profileImageUrl || ''} name={influencerData.username} />
						</span>
						<span>
							<Styled.InfluencerName>{influencerData.username}</Styled.InfluencerName>
							<div onMouseEnter={() => setShowInstagramLink(true)} onMouseLeave={() => setShowInstagramLink(false)}>
								{showInstagramLink ? (
									<Styled.InfluencerSubText>
										<Styled.LinkToInstagram href={influencerData.networkLinks.instagram} target='_blank'>
											<Styled.CustomIcon icon='instagram' size='24' className='instagram-icon' />
											Go to instagram
										</Styled.LinkToInstagram>
									</Styled.InfluencerSubText>
								) : (
									<Styled.InfluencerSubText>{influencerData.followersCount && formatNumber(influencerData.followersCount)} Followers</Styled.InfluencerSubText>
								)}
							</div>
						</span>
					</Styled.Div>
				</Styled.Td>
				<Styled.Td>
					<Styled.Data>{influencerData.topicScore !== null ? formatNumber(influencerData.topicScore) : '-'}</Styled.Data>
				</Styled.Td>
				<Styled.Td>
					<Styled.Data>{influencerData.engagement !== null ? formatNumberWithDecimals(influencerData.engagement, '%') : '-'}</Styled.Data>
				</Styled.Td>
				<Styled.Td>{influencerData.audienceBrief.age.name ? <Styled.Data>{getNameAndRatio(influencerData.audienceBrief.age)}</Styled.Data> : `-`}</Styled.Td>
				<Styled.Td maxWidth='100px'>
					{influencerData.audienceBrief.country.name ? <Styled.Data>{getNameAndRatio(influencerData.audienceBrief.country)}</Styled.Data> : `-`}
				</Styled.Td>
				<Styled.Td>
					{influencerData.audienceBrief.gender.name ? <Styled.Data>{getNameAndRatio(influencerData.audienceBrief.gender)}</Styled.Data> : `-`}
				</Styled.Td>
				<Styled.Td>
					<Styled.Div display='flex' columnGap='1rem' alignItems='center'>
						<Styled.IconContainer>
							<Styled.CustomIcon icon='like' size='32' />
						</Styled.IconContainer>
						<Styled.IconContainer>
							<Styled.CustomIcon icon='hide' size='32' />
						</Styled.IconContainer>

						<Styled.IconContainer>
							<DiscoveryDropdown selectedItems={[influencerData.id]} size='32' />
						</Styled.IconContainer>
						<Styled.IconContainer className={classnames('detail-button', { 'show-detail': showDetail })} onClick={getInfluencerDetail}>
							<Styled.CustomIcon className='detail-button' icon='chevron-down' size='24' />
						</Styled.IconContainer>
					</Styled.Div>
				</Styled.Td>
			</Styled.Wrapper>
			{showDetail && (
				<Styled.ExpendWrapper>
					<Styled.Td colSpan={100}>
						<InfluencerDetailCard influencer={influencerData} onSelect={() => props.onSelect()} isSelected={props.isSelected} />
					</Styled.Td>
				</Styled.ExpendWrapper>
			)}
		</>
	);
};

export default InfluencerListItem;
