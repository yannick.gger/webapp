import { CSSProperties } from 'react';
import styled from 'styled-components';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import Avatar from 'components/Avatar';
import Icon from 'components/Icon';

const wrapperPadding = `${guttersWithRem.l} ${guttersWithRem.s}`;

const Wrapper = styled.tr`
	width: 100%;
	overflow-x: auto;
	background-color: ${colors.discovery.tableItemBackground};
	border: 2px solid ${colors.discovery.border};
	border-left: none;
	border-right: none;
	border-bottom: none;
`;

const Td = styled.td<CSSProperties>`
	padding: ${wrapperPadding};
	text-align: ${(props) => props.textAlign || 'left'};
	max-width: ${(props) => props.maxWidth};
	height: ${(props) => props.maxHeight || '50px'};
`;

const ExpendWrapper = styled(Wrapper)`
	border-top: none;

	& > ${Td} {
		padding-top: 0px;
	}
`;

const Div = styled.div<CSSProperties>`
	display: ${(props) => props.display};
	justify-content: ${(props) => props.justifyContent};
	align-items: ${(props) => props.alignItems};
	column-gap: ${(props) => props.columnGap};
	row-gap: ${(props) => props.rowGap};
	font-family: ${typography.list.fontFamily};
	overflow-x: ${(props) => props.overflowX};
`;

const InfluencerName = styled(Div)`
	font-weight: ${typography.list.fontWeight};
	font-size: ${typography.list.fontSize};
`;

const InfluencerSubText = styled(Div)`
	font-family: ${typography.list.accessory.fontFamilies};
	font-size: ${typography.list.small.fontSize};
	white-space: nowrap;
`;

const Data = styled(Div)`
	font-weight: ${typography.list.fontWeight};
	font-size: ${typography.list.medium.fontSize};
	white-space: nowrap;
`;

const IconContainer = styled.div`
	cursor: pointer;
	max-height: 32px;

	&.detail-button {
		transform: rotate(-90deg);
		&:hover {
			background-color: ${colors.discovery.grayBackground};
		}
	}

	&.show-detail {
		transform: rotate(0deg);
		background-color: ${colors.discovery.grayBackground};
	}
`;

const CustomAvatar = styled(Avatar)`
	margin: 0;
`;

const CustomIcon = styled(Icon)`
	&.icon {
		line-height: 48px;
	}

	&.detail-button {
		padding: 0.3125rem;
	}

	&.instagram-icon {
		line-height: unset;
		display: flex;
		fill: ${colors.button.sencondary.color};
		height: 0.875rem;
	}
`;

const LinkToInstagram = styled.a`
	display: flex;
	text-decoration: none;
	color: ${colors.button.sencondary.color};
	border: none;
`;

const Styled = {
	Wrapper,
	ExpendWrapper,
	Td,
	Div,
	InfluencerName,
	InfluencerSubText,
	Data,
	IconContainer,
	CustomAvatar,
	CustomIcon,
	LinkToInstagram
};

export default Styled;
