import { CSSProperties } from 'react';
import styled from 'styled-components';

import colors from 'styles/variables/colors';
import fontSize from 'styles/variables/font-size';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import Avatar from 'components/Avatar';
import Icon from 'components/Icon';

const Wrapper = styled.div`
	padding: ${guttersWithRem.xxs};
	max-height: 350px;
`;

const InnerWrapper = styled.div`
	display: flex;
	flex-direction: column;
`;

const CustomAvatar = styled(Avatar)`
	margin: 0;
`;

const FlexDiv = styled.div<CSSProperties>`
	display: flex;
	flex-direction: ${(props) => props.flexDirection};
	justify-content: ${(props) => props.justifyContent};
	align-items: ${(props) => props.alignItems};
	overflow-x: ${(props) => props.overflowX};
	overflow-y: ${(props) => props.overflowY};
	flex: ${(props) => props.flex};
	flex-wrap: ${(props) => props.flexWrap};
	row-gap: ${(props) => props.rowGap};
	max-height: ${(props) => props.maxHeight};
	text-align: ${(props) => props.textAlign};

	&.divide-line {
		position: relative;
		&:after {
			position: absolute;
			top: 2px;
			bottom: 2px;
			right: 0;
			width: 1px;

			content: ' ';
			border-right: 1px solid ${colors.discovery.border};
		}
	}

	&.influencer-info {
		margin-bottom: ${guttersWithRem.s};
		& > div {
			flex: 1;
			text-align: center;
		}
	}

	&.categories-wrapper {
		max-height: 80px;
		flex-wrap: wrap;
		row-gap: 0.2rem;
	}
`;

const Div = styled.div<CSSProperties>`
	margin-bottom: ${(props) => props.marginBottom};

	&.light-gray-bg {
		background-color: ${colors.discovery.grayBackground};
	}
	&.divide-line {
		position: relative;
		&:after {
			position: absolute;
			top: 2px;
			bottom: 2px;
			right: 0;
			width: 1px;

			content: ' ';
			border-right: 1px solid ${colors.discovery.border};
		}
	}
`;

const DataText = styled.span`
	font-family: ${typography.headings.fontFamily};
	font-weight: ${typography.headings.h5.fontWeight};
	font-size: ${typography.headings.h5.fontSize};
	padding-top: 0.75rem;
	line-height: 1;
`;

const LabelText = styled.span`
	font-family: ${typography.label.fontFamily};
	font-size: ${typography.label.small.fontSize};
	color: ${colors.labelColor};
	padding-bottom: 0.5rem;
	text-align: center;
`;

const CustomIcon = styled(Icon)`
	line-height: 0;
`;

const InfluencerData = styled.div`
	&.value {
		font-family: ${typography.BaseFontFamiliy};
		font-weight: 600;

		&::first-letter {
			text-transform: capitalize;
		}
	}

	&.label {
		font-family: ${typography.label.fontFamily};
		font-size: ${typography.label.small.fontSize};
		color: ${colors.labelColor};
	}
`;

const Styled = {
	Wrapper,
	InnerWrapper,
	CustomAvatar,
	FlexDiv,
	Div,
	InfluencerData,
	DataText,
	LabelText,
	CustomIcon
};

export default Styled;
