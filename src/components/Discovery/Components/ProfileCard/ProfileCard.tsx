import { formatNumber, formatNumberWithDecimals } from 'shared/helpers/Chart/chart-util';
import CategoryTag from '../CategoryTag';
import Styled from './ProfileCard.style';
import { InfluencerListItemType } from 'contexts/Discovery/types';

const ProfileCard = (props: { data: InfluencerListItemType }) => {
	// @todo: i18next
	return (
		<Styled.Wrapper>
			<Styled.InnerWrapper>
				<Styled.FlexDiv marginBottom='0.5rem' className='influencer-info'>
					<Styled.Div className='divide-line'>
						<Styled.InfluencerData className='value'>{props.data.gender ? props.data.gender : '-'}</Styled.InfluencerData>
						<Styled.InfluencerData className='label'>Gender</Styled.InfluencerData>
					</Styled.Div>
					<Styled.Div className='divide-line'>
						<Styled.InfluencerData className='value'>{props.data.age ? props.data.age : '-'}</Styled.InfluencerData>
						<Styled.InfluencerData className='label'>Age Span</Styled.InfluencerData>
					</Styled.Div>
					<Styled.Div>
						<Styled.InfluencerData className='value'>{props.data.country ? props.data.country.toUpperCase() : '-'}</Styled.InfluencerData>
						<Styled.InfluencerData className='label'>Country</Styled.InfluencerData>
					</Styled.Div>
				</Styled.FlexDiv>

				<Styled.Div marginBottom='0.2rem' className='light-gray-bg'>
					<Styled.FlexDiv>
						<Styled.FlexDiv flex='1' flexDirection='column' justifyContent='center' alignItems='center' className='divide-line'>
							<Styled.DataText>{props.data.averageViews ? formatNumber(props.data.averageViews) : '-'}</Styled.DataText>
							<Styled.LabelText>Average view</Styled.LabelText>
						</Styled.FlexDiv>

						<Styled.FlexDiv flex='1' flexDirection='column' justifyContent='center' alignItems='center'>
							<Styled.DataText>
								{props.data.paidPostsRatio && props.data.paidPostsRatio !== null ? formatNumberWithDecimals(props.data.paidPostsRatio, '%') : '-'}
							</Styled.DataText>
							<Styled.LabelText>Paid post ratio</Styled.LabelText>
						</Styled.FlexDiv>
					</Styled.FlexDiv>
				</Styled.Div>
				<Styled.Div marginBottom='1rem' className='light-gray-bg'>
					<Styled.FlexDiv>
						<Styled.FlexDiv flex='1' flexDirection='column' justifyContent='center' alignItems='center' className='divide-line'>
							<Styled.DataText>
								{props.data.engagementOrganic && props.data.engagementOrganic !== null ? formatNumberWithDecimals(props.data.engagementOrganic, '%') : '-'}
							</Styled.DataText>
							<Styled.LabelText>Organic Engagement</Styled.LabelText>
						</Styled.FlexDiv>

						<Styled.FlexDiv flex='1' flexDirection='column' justifyContent='center' alignItems='center'>
							<Styled.DataText>
								{props.data.engagementPaid && props.data.engagementPaid !== null ? formatNumberWithDecimals(props.data.engagementPaid, '%') : '-'}
							</Styled.DataText>
							<Styled.LabelText>Paid Engagement</Styled.LabelText>
						</Styled.FlexDiv>
					</Styled.FlexDiv>
				</Styled.Div>

				<Styled.FlexDiv flexWrap='wrap' rowGap='0.2rem' overflowY='auto'>
					{props.data.categories.map((category, index) => {
						if (index < 5) {
							return <CategoryTag key={category.id} text={category.name} score={category.score} size='s' />;
						}
					})}
				</Styled.FlexDiv>
			</Styled.InnerWrapper>
		</Styled.Wrapper>
	);
};

export default ProfileCard;
