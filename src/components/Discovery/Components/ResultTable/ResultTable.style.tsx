import { CSSProperties } from 'react';
import styled from 'styled-components';
import typography from 'styles/variables/typography';
import Icon from 'components/Icon';
import { guttersWithRem } from 'styles/variables/gutter';

const wrapperPadding = `${guttersWithRem.xs} ${guttersWithRem.s}`;

const Table = styled.table`
	width: 100%;
`;

const TableRow = styled.tr``;

const Th = styled.th<CSSProperties>`
	font-family: ${typography.list.accessory.fontFamilies};
	font-size: ${typography.list.medium.fontSize};
	font-weight: ${typography.list.accessory.fontWeight};
	text-align: ${(props) => props.textAlign || 'left'};
	padding: ${wrapperPadding};
	white-space: nowrap;
`;

const TableHeader = styled.thead`
	width: 100%;
`;

const TableBody = styled.tbody``;

const IconWrapper = styled.span`
	cursor: pointer;
	&:hover {
		border-radius: 2px;
		background-color: #e2e2e2;
	}
`;

const CustomIcon = styled(Icon)`
	line-height: 0;
`;

const CheckboxWrapper = styled.div`
	position: relative;
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
`;

const FlexDiv = styled.div<CSSProperties>`
	display: flex;
	height: 24px;
	position: ${(props) => props.position};
	column-gap: ${(props) => props.columnGap};
	align-items: ${(props) => props.alignItems};
	top: ${(props) => props.top};
	left: ${(props) => props.left};
	right: ${(props) => props.right};
	bottom: ${(props) => props.bottom};
`;

const CheckboxInnerWapper = styled(FlexDiv)`
	padding: 0 5px;
	&:hover {
		border-radius: 2px;
		background-color: #e2e2e2;
	}
`;

const Styled = {
	TableRow,
	TableHeader,
	TableBody,
	Table,
	Th,
	IconWrapper,
	CustomIcon,
	CheckboxWrapper,
	FlexDiv,
	CheckboxInnerWapper
};

export default Styled;
