import { useState, useEffect } from 'react';
import Styled from './ResultTable.style';
import Checkbox from 'components/Checkbox';
import InfluencerListItem from '../InfluencerListItem';
import DiscoveryDropdown from '../DiscoveryDropdown';

const ResultTable = (props: { list: any[]; selectedItems: any[]; onSelectAll: () => void; onSelectOne: () => (id: string) => void; onReset: () => void }) => {
	const [isSelected, setIsSelected] = useState(false);

	useEffect(() => {
		if (props.selectedItems.length > 0) {
			setIsSelected(true);
		} else {
			setIsSelected(false);
		}
	}, [props.selectedItems]);

	// @todo i18next
	return (
		<Styled.Table cellSpacing={0}>
			<Styled.TableHeader>
				<tr>
					<Styled.Th>
						<Styled.CheckboxWrapper>
							<Styled.FlexDiv columnGap='1rem' position='absolute' left='-5px'>
								<Styled.CheckboxInnerWapper columnGap='0.25rem' alignItems='center'>
									<Checkbox onChange={props.onSelectAll} checked={isSelected} />
									<Styled.CustomIcon icon='chevron-down' size='16' />
								</Styled.CheckboxInnerWapper>

								{props.selectedItems && props.selectedItems.length > 0 ? (
									<Styled.FlexDiv>
										<DiscoveryDropdown selectedItems={props.selectedItems} onResetSelection={props.onReset} />
										<Styled.CustomIcon icon='look-a-like' />
									</Styled.FlexDiv>
								) : null}
							</Styled.FlexDiv>
						</Styled.CheckboxWrapper>
					</Styled.Th>
					<Styled.Th />
					<Styled.Th>Topic score</Styled.Th>
					<Styled.Th>Engagement</Styled.Th>
					<Styled.Th>Age span</Styled.Th>
					<Styled.Th>Location</Styled.Th>
					<Styled.Th>Gender</Styled.Th>
					<Styled.Th />
				</tr>
			</Styled.TableHeader>
			<Styled.TableBody>
				{props.list.length > 0 &&
					props.list.map((influencer) => {
						const isSelected = props.selectedItems.some((item) => item === influencer.id);
						return <InfluencerListItem key={influencer.id} data={influencer} onSelect={() => props.onSelectOne()(influencer.id)} isSelected={isSelected} />;
					})}
			</Styled.TableBody>
		</Styled.Table>
	);
};

export default ResultTable;
