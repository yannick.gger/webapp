import Dropdown from 'components/Dropdown';
import Styled from './DiscoveryDropdown.style';

const AddInfluencerItem = (props: {
	type: string;
	items: Array<{ id: string; itemName: string }>;
	onCancel?: () => void;
	onClick: (campaignId: string) => void;
	isCancellable?: boolean;
}) => {
	const onClickHandler = (e: React.MouseEvent<HTMLAnchorElement>, campaignId: string) => {
		e.preventDefault();
		props.onClick(campaignId);
	};

	return (
		<Dropdown.Menu>
			<div>
				<Styled.MenuHeader>
					<span>Add to {props.type}</span>
					{props.isCancellable && (
						<span onClick={props.onCancel}>
							<Styled.CustomIcon icon='cancel' size='16' />
						</span>
					)}
				</Styled.MenuHeader>
				<Styled.MenuItemsWrapper>
					{props.items.map((item) => (
						<Dropdown.Item key={item.id}>
							<a href='#' onClick={(e: React.MouseEvent<HTMLAnchorElement>) => onClickHandler(e, item.id)}>
								{item.itemName}
							</a>
						</Dropdown.Item>
					))}
				</Styled.MenuItemsWrapper>
			</div>
		</Dropdown.Menu>
	);
};

export default AddInfluencerItem;
