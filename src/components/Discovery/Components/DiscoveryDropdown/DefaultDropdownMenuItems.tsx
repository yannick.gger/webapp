import LoadingSpinner from 'components/LoadingSpinner';
import Dropdown from 'components/Dropdown';
import Styled from './DiscoveryDropdown.style';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { FEATURE_FLAG_DISCOVERY_LISTS_VIEW } from 'constants/feature-flag-keys';

const DefaultDropdownMenuItems = (props: { onAddCampaign: () => void; onAddList?: () => void; loading: boolean }) => {
	const [isEnabled] = useFeatureToggle();
	return (
		<Dropdown.Menu>
			<Dropdown.Item>
				<Styled.FlexDiv alignItems='center' columnGap='0.5rem' onClick={props.onAddCampaign}>
					<Styled.CustomIcon icon='add-new-folder' />
					Add to campaign
					{props.loading ? <LoadingSpinner size='sm' /> : null}
				</Styled.FlexDiv>
			</Dropdown.Item>
			{isEnabled(FEATURE_FLAG_DISCOVERY_LISTS_VIEW) && (
				<Dropdown.Item>
					<Styled.FlexDiv alignItems='center' columnGap='0.5rem' onClick={props.onAddList}>
						<Styled.CustomIcon icon='add-new-folder' />
						Add to list
					</Styled.FlexDiv>
				</Dropdown.Item>
			)}

			{/* 			<Dropdown.Item>
				<Styled.FlexDiv alignItems='center' columnGap='0.5rem'>
					<Styled.CustomIcon icon='add-new-folder' />
					Create new list
				</Styled.FlexDiv>
			</Dropdown.Item> */}
		</Dropdown.Menu>
	);
};

export default DefaultDropdownMenuItems;
