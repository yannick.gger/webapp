import { useState, useContext, useEffect } from 'react';
import Styled from './DiscoveryDropdown.style';
import useCampaignData from 'hooks/Chart/useCampaignData';
import useListData from 'hooks/Chart/useListData';
import DefaultDropdownMenuItems from './DefaultDropdownMenuItems';
import CampaignListMenuItems from './AddInfluencerItem';
import { StatusCode } from 'services/Response.types';
import { ToastContext } from 'contexts';

const DROPDOWN_OPTION = {
	NULL: '',
	CREATE_NEW_LIST: 'new list',
	ADD_TO_LIST: 'list',
	ADD_TO_CAMPAIGN: 'campaign'
};

const DiscoveryDropdown = (props: { selectedItems: string[]; onResetSelection?: () => void; size?: '32' }) => {
	const [loading, setLoading] = useState(false);
	const [selectedOption, setSelectedOption] = useState(DROPDOWN_OPTION.NULL);
	const [items, setItems] = useState<Array<{ id: string; itemName: string }>>([]);
	const { getCampaigns, setInfluencersToCampaign } = useCampaignData();
	const { getLists, addInfluencersToList } = useListData();
	const { addToast } = useContext(ToastContext);

	const getAllCampaignList = () => {
		setLoading(true);
		getCampaigns()
			.then((res) => {
				setItems(res);
				setSelectedOption(DROPDOWN_OPTION.ADD_TO_CAMPAIGN);
			})
			.catch((err) => {
				console.log(err);
			})
			.finally(() => {
				setLoading(false);
			});
	};
	const saveInfluencersHandler = (influencers: string[]) => {
		return (itemId: string) => {
			selectedOption === DROPDOWN_OPTION.ADD_TO_CAMPAIGN
				? setInfluencersToCampaign(itemId, influencers)
						.then((res) => {
							if (res === StatusCode.CREATED) {
								addToast({ id: `success-set-influencers-${itemId}`, mode: 'success', message: 'Successfully saved' });
								if (props.onResetSelection) {
									props.onResetSelection();
								}
							} else {
								addToast({ id: `fail-set-influencers-${itemId}`, mode: 'error', message: 'Something went wrong!' });
							}
						})
						.finally(() => {
							setSelectedOption(DROPDOWN_OPTION.NULL);
						})
				: addInfluencersToList(itemId, influencers)
						.then((res) => {
							if (res === StatusCode.CREATED) {
								addToast({ id: `success-set-influencers-${itemId}`, mode: 'success', message: 'Successfully saved' });
								if (props.onResetSelection) {
									props.onResetSelection();
								}
							} else {
								addToast({ id: `fail-set-influencers-${itemId}`, mode: 'error', message: 'Something went wrong!' });
							}
						})
						.finally(() => {
							setSelectedOption(DROPDOWN_OPTION.NULL);
						});
		};
	};

	const getAllInfluencerList = () => {
		setLoading(true);
		getLists()
			.then((res) => {
				setItems(res);
				setSelectedOption(DROPDOWN_OPTION.ADD_TO_LIST);
			})
			.catch((err) => {
				console.log(err);
			})
			.finally(() => {
				setLoading(false);
			});
	};

	if (props.size === '32') {
		return (
			<Styled.CustomDropdown icon='add-new-folder' position='right' size={props.size} className='lg'>
				{selectedOption === DROPDOWN_OPTION.NULL ? (
					<DefaultDropdownMenuItems onAddCampaign={getAllCampaignList} onAddList={getAllInfluencerList} loading={loading} />
				) : (
					<CampaignListMenuItems
						type={selectedOption}
						items={items}
						isCancellable={true}
						onCancel={() => {
							setSelectedOption(DROPDOWN_OPTION.NULL);
						}}
						onClick={(campaignId: string) => saveInfluencersHandler(props.selectedItems)(campaignId)}
					/>
				)}
			</Styled.CustomDropdown>
		);
	}

	return (
		<Styled.CustomDropdown icon='add-new-folder' position='left'>
			{selectedOption === DROPDOWN_OPTION.NULL ? (
				<DefaultDropdownMenuItems onAddCampaign={getAllCampaignList} onAddList={getAllInfluencerList} loading={loading} />
			) : (
				<CampaignListMenuItems
					type={selectedOption}
					items={items}
					isCancellable={true}
					onCancel={() => {
						setSelectedOption(DROPDOWN_OPTION.NULL);
					}}
					onClick={(campaignId: string) => saveInfluencersHandler(props.selectedItems)(campaignId)}
				/>
			)}
		</Styled.CustomDropdown>
	);
};

export default DiscoveryDropdown;
