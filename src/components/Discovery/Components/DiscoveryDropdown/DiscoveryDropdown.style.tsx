import { CSSProperties } from 'react';
import styled from 'styled-components';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import Dropdown from 'components/Dropdown';
import Icon from 'components/Icon';

const FlexDiv = styled.div<CSSProperties>`
	display: flex;
	height: 24px;
	position: ${(props) => props.position};
	column-gap: ${(props) => props.columnGap};
	align-items: ${(props) => props.alignItems};
	top: ${(props) => props.top};
	left: ${(props) => props.left};
	right: ${(props) => props.right};
	bottom: ${(props) => props.bottom};
`;

const CustomIcon = styled(Icon)`
	line-height: 0;
	cursor: pointer;
`;

const CustomDropdown = styled(Dropdown)`
	font-family: ${typography.BaseFontFamiliy};

	& button {
		padding: 0px;

		&:hover,
		&.show {
			border-radius: 2px;
			background-color: ${colors.discovery.iconHover};
		}
	}

	&.lg {
		display: flex;

		& button {
			padding: 0px;

			&:hover,
			&.show {
				border-radius: 2px;
				background-color: ${colors.discovery.grayBackground};
			}
		}
	}
`;

const MenuHeader = styled.div`
	padding-bottom: ${guttersWithRem.xs};
	display: flex;
	justify-content: space-between;
	align-items: center;
	font-family: ${typography.SecondaryFontFamiliy};
	border-bottom: 1px solid ${colors.discovery.border};
`;

const MenuItemsWrapper = styled.div`
	max-height: 300px;
	overflow-y: auto;
`;

const Styled = {
	FlexDiv,
	CustomIcon,
	CustomDropdown,
	MenuHeader,
	MenuItemsWrapper
};

export default Styled;
