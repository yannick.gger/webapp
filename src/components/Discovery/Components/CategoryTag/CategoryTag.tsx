import Styled from './CategoryTag.style';

const CategoryTag = (props: { text: string; score?: number; size: 's' | 'm' }) => {
	return (
		<Styled.TagContainer size={props.size} score={props.score}>
			{props.text.toUpperCase()}
		</Styled.TagContainer>
	);
};

export default CategoryTag;
