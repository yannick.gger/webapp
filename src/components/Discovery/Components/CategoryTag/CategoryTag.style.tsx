import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const tagContainerPaddingSmall = `${guttersWithRem.xs} ${guttersWithRem.m}`;
const tagContainerPaddingMedium = `${guttersWithRem.m} ${guttersWithRem.m}`;

const getColor = (score?: number) => {
	// If the category does not have a score, then show a random color
	if (typeof score === 'undefined') {
		score = Math.ceil(Math.random() * 100);
	}

	if (score > 80) {
		return '#e1ecee';
	}
	if (score > 60) {
		return '#fee0e1';
	}
	if (score > 40) {
		return '#f6eced';
	}
	if (score > 20) {
		return '#feebd9';
	}

	return '#f6f6ed';
};

const TagContainer = styled.div<{ score?: number; size: 's' | 'm' }>`
	display: inline;
	padding: ${(props) => (props.size === 's' ? tagContainerPaddingSmall : props.size === 'm' ? tagContainerPaddingMedium : '')};
	border: ${(props) => (props.size === 's' ? `1px solid ${colors.discovery.tagBorder}` : props.size === 'm' ? `2px solid ${colors.discovery.tagBorder}` : '')};
	border-radius: 2px;
	font-family: ${typography.BaseFontFamiliy};
	font-size: ${typography.label.small.fontSize};
	margin-right: ${guttersWithRem.xxs};
	white-space: nowrap;
	line-height: 1;
	cursor: pointer;
	background-color: ${(props) => getColor(props.score)};
`;

const Styled = {
	TagContainer
};

export default Styled;
