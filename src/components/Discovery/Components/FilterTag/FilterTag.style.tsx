import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	padding: ${guttersWithRem.xxs};
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.xxs};

	border: 1px solid ${colors.black};
	border-radius: 2px;
	background-color: ${colors.black};
	color: white;

	line-height: 0;
`;

const Styled = {
	Wrapper
};

export default Styled;
