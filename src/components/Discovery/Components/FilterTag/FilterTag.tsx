import Styled from './FilterTag.style';
import Icon from 'components/Icon';
/**
 * @todo i18next
 */

const FilterTag = (props: { filterKey: string; onRemove: () => void }) => {
	const showFilterName = (target: string) => {
		switch (target) {
			case 'audienceAgeRanges':
				return 'Audience Age';
			case 'audienceCountries':
				return 'Audience Country';
			case 'audienceGenders':
				return 'Audience Gender';
			case 'brandMentions':
				return 'Brand Mention';
			case 'excludeBrandMentions':
				return 'Exclude Brand Mention';
			case 'countries':
				return 'Country';
			case 'genders':
				return 'Gender';
			case 'maxAge':
				return 'Max Age';
			case 'minAge':
				return 'Min Age';
			case 'minFollowers':
				return 'Min Followers';
			case 'maxFollowers':
				return 'Max Followers';
			default:
				return '';
		}
	};

	return (
		<Styled.Wrapper>
			<span>{showFilterName(props.filterKey)}</span>
			<div onClick={props.onRemove}>
				<Icon icon='cross' size='10' />
			</div>
		</Styled.Wrapper>
	);
};

export default FilterTag;
