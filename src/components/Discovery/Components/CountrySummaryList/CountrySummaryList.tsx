import Styled from './CountrySummaryList.style';
import { BarItem, Bar } from '../BarItem';
import { ICountrySummaryList, CountryData } from './types';
import colors from 'styles/variables/colors';

const CountrySummaryList = (props: ICountrySummaryList) => {
	let filteredCountries: any[] = [];

	if (props.items.length) {
		filteredCountries = props.items.sort((a: any, b: any) => b.followers - a.followers);
	}

	const getColors = (index: number) => {
		let colorSet = { containerColor: colors.borderGray, fillerColor: colors.black };
		switch (index) {
			case 0:
				colorSet.containerColor = colors.chart.country.first.light;
				colorSet.fillerColor = colors.chart.country.first.color;
				break;
			case 1:
				colorSet.containerColor = colors.chart.country.second.light;
				colorSet.fillerColor = colors.chart.country.second.color;
				break;
			case 2:
				colorSet.containerColor = colors.chart.country.third.light;
				colorSet.fillerColor = colors.chart.country.third.color;
				break;
			case 3:
				colorSet.containerColor = colors.chart.country.fourth.light;
				colorSet.fillerColor = colors.chart.country.fourth.color;
				break;
			case 4:
				colorSet.containerColor = colors.chart.country.fifth.light;
				colorSet.fillerColor = colors.chart.country.fifth.color;
				break;
			default:
				colorSet.containerColor = colors.buttonGray;
				colorSet.fillerColor = colors.gray5;
		}

		return colorSet;
	};

	if (props.isTopThree) {
		filteredCountries = filteredCountries.filter((item: any, index: number) => {
			if (index < 3) {
				return item;
			}
		});
	}

	filteredCountries = filteredCountries.map((item, index) => {
		return { ...item, ...getColors(index) };
	});

	return (
		<Styled.Wrapper>
			{filteredCountries.length > 0 ? (
				filteredCountries.map((item: CountryData) => {
					// Double the percentage so it looks good. (Most users has less than 30%)
					const percent = `${Math.min(100, Math.floor((item.followers / props.totalFollowers) * 200))}%`;
					let value = 0;
					if (props.sign === 'K') {
						value = Math.floor(item.followers / 1000);
					} else if (props.sign === '%') {
						value = Math.floor((item.followers / props.totalFollowers) * 100);
					} else {
						value = item.followers;
					}
					return (
						<Styled.FlexItem key={item.name}>
							<BarItem label={item.alpha3code || item.name} value={value} sign={props.sign}>
								<Bar total={props.totalFollowers} percent={percent} containerBg={item.containerColor} fillerBg={item.fillerColor} />
							</BarItem>
						</Styled.FlexItem>
					);
				})
			) : (
				<Styled.CenteredWrapper>
					<span>N/A</span>
				</Styled.CenteredWrapper>
			)}
		</Styled.Wrapper>
	);
};

export default CountrySummaryList;
