import styled from 'styled-components';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

interface Wrapper {
	width?: string;
	height?: string;
}

const Wrapper = styled.div<Wrapper>`
	width: ${(props) => props.width || '100%'};
	height: ${(props) => props.height || 'auto'};

	display: flex;
	flex-direction: column;
	border-top: 1px solid black;
	padding: 0.625rem 0;
`;

const FlexItem = styled.span`
	&:not(:last-child):after {
		display: inline-block;
		width: 100%;
		content: '';
		border: 0.5px solid ${colors.discovery.border};
		transform: translateY(-0.5rem);
	}
`;

const CenteredWrapper = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	flex: 1;
	display: flex;
	justify-content: center;
`;

const Styled = {
	Wrapper,
	CenteredWrapper,
	FlexItem
};

export default Styled;
