import styled from 'styled-components';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
`;

const Label = styled.label`
	font-family: ${typography.label.fontFamily};
	font-size: ${typography.label.small.fontSize};
`;

const Input = styled.input`
	width: 120px;
	height: 38px;
	border-radius: 0;
	border: 1px solid ${colors.discovery.tagBorder};
	text-align: center;
	background-color: ${colors.white};

	font-family: ${typography.input.fontFamilies};
	font-size: ${typography.input.fontSize};

	::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	&:focus {
		outline: none;
	}
`;

const Styled = {
	Wrapper,
	Label,
	Input
};

export default Styled;
