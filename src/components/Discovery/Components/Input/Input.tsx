import { InputHTMLAttributes } from 'react';
import Styled from './Input.style';

interface IInput extends InputHTMLAttributes<HTMLInputElement> {
	label?: string;
}

const Input = (props: IInput) => {
	return (
		<Styled.Wrapper>
			{props.label && <Styled.Label>{props.label}</Styled.Label>}
			<Styled.Input {...props} />
		</Styled.Wrapper>
	);
};

export default Input;
