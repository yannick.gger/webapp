import styled from 'styled-components';
import typography from 'styles/variables/typography';

const Wrapper = styled.div`
	width: 100%;
	overflow-x: auto;
`;

const EmptyResultImgWrapper = styled.div`
	max-width: 500px;
	margin: 0 auto;
`;

const FetchLaterPhrase = styled.div`
	text-align: center;
	font-size: ${typography.headings.h5.fontSize};
	font-weight: ${typography.headings.h5.fontWeight};
`;

const Styled = {
	Wrapper,
	EmptyResultImgWrapper,
	FetchLaterPhrase
};

export default Styled;
