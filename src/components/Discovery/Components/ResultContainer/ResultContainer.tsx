import ResultTable from '../ResultTable';
import Styled from './ResultContainer.style';
import EmptyResultAnimation from 'assets/animations/no-search-results.json';
import { Player } from '@lottiefiles/react-lottie-player';
/**
 * @todo i18next
 */
const ResultContainer = (props: {
	isInfluencerSearch: boolean;
	tableList: any[];
	selectedInfluencers: any[];
	onSelectAll: () => void;
	onSelectOne: () => (id: string) => void;
	onResetSelection: () => void;
}) => {
	return (
		<Styled.Wrapper>
			{props.tableList.length > 0 ? (
				<ResultTable
					list={props.tableList}
					selectedItems={props.selectedInfluencers}
					onSelectAll={props.onSelectAll}
					onSelectOne={props.onSelectOne}
					onReset={props.onResetSelection}
				/>
			) : (
				<Styled.EmptyResultImgWrapper>
					{props.isInfluencerSearch ? (
						<Styled.FetchLaterPhrase>
							We will fetch it soon. <br />
							Try to search later!
						</Styled.FetchLaterPhrase>
					) : null}
					<Player src={EmptyResultAnimation} autoplay={true} keepLastFrame={true} />
				</Styled.EmptyResultImgWrapper>
			)}
		</Styled.Wrapper>
	);
};

export default ResultContainer;
