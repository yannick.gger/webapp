import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import { breakpoints } from 'styles/variables/media-queries';
import typography from 'styles/variables/typography';

const InnerWrapper = styled.div``;

const Wrapper = styled.div`
	min-width: 0px;
	position: relative;

	${InnerWrapper} {
		width: 0px;
		overflow-y: auto;
		border-top: 1px solid ${colors.discovery.tagBorder};
	}

	&.isVisible {
		min-width: 350px;
		margin-right: ${guttersWithRem.m};
		animation: moveToRight 0.1s ease-in-out;
		@keyframes moveToRight {
			0% {
				transform: translateX(-350px);
			}
			100% {
				transform: translateX(0px);
			}
		}

		${InnerWrapper} {
			width: 350px;
			@keyframes moveToRight {
				0% {
					transform: translateX(-350px);
				}
				100% {
					transform: translateX(0px);
				}
			}

			@media (min-width: ${breakpoints.lg}) {
				top: 345px;
			}
		}
	}
`;

const FilteringSummaryWrapper = styled(InnerWrapper)`
	padding: ${guttersWithRem.s} 0;
`;

const filterItemPadding = `${guttersWithRem.l} ${guttersWithRem.xs} ${guttersWithRem.s}`;
const FilterItemWapper = styled.div`
	padding: ${filterItemPadding};
	border-bottom: 1px solid ${colors.discovery.tagBorder};
`;

const AccordionInnerWrapper = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};

	&.selected-countries {
		max-height: 100px;
		overflow-y: auto;
		margin-top: ${guttersWithRem.m};
		margin-bottom: ${guttersWithRem.m};
	}

	&.countries-list {
		max-height: 200px;
		overflow-y: auto;
	}
`;

const FlexDiv = styled.div`
	display: flex;
`;

const FollowersInputsWrapper = styled(FlexDiv)`
	align-items: flex-end;
	justify-content: center;
	column-gap: ${guttersWithRem.s};
`;

const BetweenMinMaxDiv = styled(FlexDiv)`
	height: 38px;
	align-items: center;
`;

const Styled = {
	InnerWrapper,
	Wrapper,
	FilteringSummaryWrapper,
	FilterItemWapper,
	AccordionInnerWrapper,
	FlexDiv,
	FollowersInputsWrapper,
	BetweenMinMaxDiv
};

export default Styled;
