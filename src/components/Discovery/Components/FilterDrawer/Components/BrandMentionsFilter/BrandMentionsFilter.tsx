import React, { useState } from 'react';
import classnames from 'classnames';
import Styled from './BrandMentionsFilter.style';
import SearchInput from '../../../SearchInput';
import Icon from 'components/Icon';

const BrandMentionsFilter = (props: {
	brandMentions: { [key: string]: string[] };
	onSetFilter: (value: string, option: string) => void;
	onRemoveBrand: (value: string, option: string) => void;
}) => {
	const [enteredText, setEnteredText] = useState('');
	const [option, setOption] = useState<string>('include');

	const setBrandName = (e: React.KeyboardEvent) => {
		if (e.code === 'Enter') {
			props.onSetFilter(enteredText, option);
			setEnteredText('');
		}
	};

	const changeOptionHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		setOption(e.target.value);
	};

	const removeBrandName = (value: string, option: string) => {
		props.onRemoveBrand(value, option);
	};

	return (
		<Styled.Wrapper>
			<SearchInput
				searchIcon={false}
				iconSize='24'
				onReset={() => {
					setEnteredText('');
				}}
				value={enteredText}
				onChange={(e) => {
					setEnteredText(e.target.value);
				}}
				onKeyDown={setBrandName}
			/>
			<Styled.OptionWrapper>
				<Styled.RadioButtonWrapper className={classnames({ selected: option === 'include' })}>
					<input type='radio' id='include' name='barnd-mentions' value='include' checked={option === 'include'} onChange={changeOptionHandler} />
					<label htmlFor='include'>Include</label>
				</Styled.RadioButtonWrapper>
				<Styled.RadioButtonWrapper className={classnames({ selected: option === 'exclude' })}>
					<input type='radio' id='exclude' name='barnd-mentions' value='exclude' checked={option === 'exclude'} onChange={changeOptionHandler} />
					<label htmlFor='exclude'>Exclude</label>
				</Styled.RadioButtonWrapper>
			</Styled.OptionWrapper>

			<Styled.BrandsWrapper>
				{props.brandMentions[option].map((brand, index) => {
					return (
						<Styled.BrandItem
							key={index}
							onClick={() => {
								removeBrandName(brand, option);
							}}
						>
							{brand}
							<Icon icon='cancel' size='10' />
						</Styled.BrandItem>
					);
				})}
			</Styled.BrandsWrapper>
		</Styled.Wrapper>
	);
};

export default BrandMentionsFilter;
