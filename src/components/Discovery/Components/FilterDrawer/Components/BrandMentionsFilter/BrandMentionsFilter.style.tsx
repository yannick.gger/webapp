import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const Wrapper = styled.div``;

const OptionWrapper = styled.div`
	display: flex;
	margin-bottom: ${guttersWithRem.xxs};
`;

const radioPadding = `${guttersWithRem.s} ${guttersWithRem.l}`;
const RadioButtonWrapper = styled.div`
	cursor: pointer;
	margin-top: ${guttersWithRem.xxs};
	display: flex;
	flex: 1;
	justify-content: center;
	align-items: center;
	column-gap: ${guttersWithRem.xs};
	padding: ${radioPadding};
	font-family: ${typography.SecondaryFontFamiliy};

	&.selected {
		color: ${colors.white};
		background-color: ${colors.black};
	}
`;

const BrandsWrapper = styled.div`
	display: flex;
	flex-wrap: wrap;
	column-gap: ${guttersWithRem.xxs};
	row-gap: ${guttersWithRem.xxs};
`;

const brandItemPadding = `${guttersWithRem.xxs} ${guttersWithRem.xs}`;
const BrandItem = styled.div`
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.xxs};
	font-family: ${typography.SecondaryFontFamiliy};
	padding: ${brandItemPadding};
	border: 1px solid ${colors.black};
	border-radius: 2px;
	background-color: ${colors.black};
	color: ${colors.white};
	cursor: pointer;

	& svg {
		fill: ${colors.white};
	}
`;

const Styled = {
	Wrapper,
	OptionWrapper,
	RadioButtonWrapper,
	BrandsWrapper,
	BrandItem
};

export default Styled;
