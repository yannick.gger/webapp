import { useState, useEffect, useContext } from 'react';
import classNames from 'classnames';

import DiscoveryContext from 'contexts/Discovery';
import Styled from './FilterDrawer.style';
import Accordion from 'components/Accordion';
import Checkbox from 'components/Checkbox';
import { countries } from 'country-data';
import SearchInput from '../SearchInput';
import Input from '../Input';
import BrandMentionsFilter from './Components/BrandMentionsFilter';

const rawCountriesData = countries.all;

const getTheCountryData = () => {
	const countryData: { [key: string]: { name: string; selected: boolean; show: boolean } } = {};
	rawCountriesData.forEach((country) => {
		if (country.alpha3 !== '') {
			const name = country.name;
			const code = country.alpha3;
			countryData[code] = { name: name, selected: false, show: true };
		}
	});
	return countryData;
};

/**
 * @todo i18next
 */
const FilterDrawer = (props: { isVisible: boolean; onHide: () => void }) => {
	const { filter, setFilter, changeBrandMentions, removeBrandMentions, updateGenderHandler, updateAudienceAgeRange } = useContext(DiscoveryContext);
	const [influencerMinAge, setInfluencerMinAge] = useState<number | null>(null);
	const [influencerMaxAge, setInfluencerMaxAge] = useState<number | null>(null);
	const [influencerGenderFilter, setInfluencerGenderFilter] = useState<{ [key: string]: boolean }>({
		female: false,
		male: false,
		unknown: false
	});
	const [influencerCountryFilter, setInfluencerCountryFilter] = useState<{ [key: string]: { name: string; selected: boolean; show: boolean } }>(
		getTheCountryData()
	);
	const [influencerCountrySearchInput, setInfluencerCountrySearchInput] = useState<string | null>(null);
	const [minFollower, setMinFollower] = useState<number | null>(null);
	const [maxFollower, setMaxFollower] = useState<number | null>(null);
	const [brandMentionFilter, setBrandMentionFilter] = useState<{ [key: string]: string[] }>({
		include: [],
		exclude: []
	});

	const [audienceAgeRanges, setAudienceAgeRanges] = useState<{
		[key: string]: boolean;
	}>({
		'13-17': false,
		'18-24': false,
		'25-34': false,
		'35-44': false,
		'45-54': false,
		'55-64': false,
		'65+': false
	});
	const [audienceGenderFilter, setAudienceGenderFilter] = useState<{ [key: string]: boolean }>({
		female: false,
		male: false,
		unknown: false
	});
	const [audienceCountryFilter, setAudienceCountryFilter] = useState<{ [key: string]: { name: string; selected: boolean; show: boolean } }>(
		getTheCountryData()
	);
	const [audienceCountrySearchInput, setAudienceCountrySearchInput] = useState<string | null>(null);

	const isIncludedInSearchText = (searchInput: string | undefined, text: string) => {
		if (searchInput && searchInput.length > 0) {
			return text.toLowerCase().includes(searchInput.toLowerCase());
		} else {
			return true;
		}
	};

	const searchCountryHandler = (
		updateCountryFilter: (newValue: { [key: string]: { name: string; selected: boolean; show: boolean } }) => void,
		input: string | '',
		countryFilter: { [key: string]: { name: string; selected: boolean; show: boolean } }
	) => {
		const newCountryFilter = { ...countryFilter };
		Object.keys(newCountryFilter).forEach((countryCode) => {
			newCountryFilter[countryCode].show = isIncludedInSearchText(input, newCountryFilter[countryCode].name);
		});
		updateCountryFilter(newCountryFilter);
	};

	const selectOneCountry = async (
		updateCountryFilter: (newValue: { [key: string]: { name: string; selected: boolean; show: boolean } }) => void,
		countryCode: string,
		countryFilter: { [key: string]: { name: string; selected: boolean; show: boolean } }
	) => {
		const newCountryFilter = { ...countryFilter };
		newCountryFilter[countryCode].selected = !newCountryFilter[countryCode].selected;
		updateCountryFilter(newCountryFilter);
		return updateFilter;
	};

	const updateFilter = () => {
		setFilter({
			minAge: influencerMinAge,
			maxAge: influencerMaxAge,
			genders: getGenders(influencerGenderFilter),
			countries: Object.keys(influencerCountryFilter)
				.filter((countryCode) => influencerCountryFilter[countryCode].selected)
				.join(','),
			minFollowers: minFollower,
			maxFollowers: maxFollower,
			brandMentions: brandMentionFilter.include ? brandMentionFilter.include.join(',') : null,
			excludeBrandMentions: brandMentionFilter.exclude ? brandMentionFilter.exclude.join(',') : null,
			audienceAgeRanges: getObjectKeys(audienceAgeRanges),
			audienceGenders: getGenders(audienceGenderFilter),
			audienceCountries: Object.keys(audienceCountryFilter)
				.filter((countryCode) => audienceCountryFilter[countryCode].selected)
				.join(',')
		});
	};

	const getGenders = (value: { [key: string]: boolean } | null) => {
		if (value) {
			return Object.keys(value)
				.filter((genderKey) => value[genderKey])
				.map((gender) => {
					return getGender(gender);
				})
				.join(',');
		} else {
			return null;
		}
	};

	const getGender = (gender: string) => {
		switch (gender) {
			case 'female':
				return 'f';
			case 'male':
				return 'm';
			case 'unknown':
				return 'x';
			default:
				return '';
		}
	};

	const getGenderLabel = (gender: string) => {
		switch (gender) {
			case 'female':
				return 'Women';
			case 'male':
				return 'Men';
			case 'unknown':
				return 'Unknown';
			default:
				return '';
		}
	};

	const getObjectKeys = (value: { [key: string]: boolean }) => {
		return Object.keys(value)
			.filter((key) => value[key])
			.join(',');
	};

	const setDefaultFilter = (param: { [key: string]: any }) => {
		const { key, value } = param;
		if (key === 'minAge') {
			setInfluencerMinAge(value ? Number(value) : null);
		}
		if (key === 'maxAge') {
			setInfluencerMaxAge(value ? Number(value) : null);
		}
		if (key === 'genders') {
			const genders = value ? value.split(',') : [];
			const newGenderFilter = {
				female: genders.some((gender: string) => gender === 'f'),
				male: genders.some((gender: string) => gender === 'm'),
				unknown: genders.some((gender: string) => gender === 'x')
			};
			setInfluencerGenderFilter(newGenderFilter);
		}
		if (key === 'countries' && influencerCountryFilter) {
			const newCountryData = getTheCountryData();
			const activeCountries: { [key: string]: { name: string; selected: boolean; show: boolean } } = {};
			const queryCountries = value ? value.split(',') : [];

			queryCountries.forEach((key: string) => {
				const countryCode = key.toLocaleUpperCase();
				activeCountries[countryCode] = { name: influencerCountryFilter[countryCode].name, selected: true, show: false };
			});

			setInfluencerCountryFilter({ ...newCountryData, ...activeCountries });
		}
		if (key === 'minFollowers') {
			setMinFollower(value ? Number(value) : null);
		}
		if (key === 'maxFollowers') {
			setMaxFollower(value ? Number(value) : null);
		}
		if (key === 'brandMentions') {
			setBrandMentionFilter((prev) => {
				return { ...prev, include: value ? value.split(',') : [] };
			});
		}
		if (key === 'excludeBrandMentions') {
			setBrandMentionFilter((prev) => {
				return { ...prev, exclude: value ? value.split(',') : [] };
			});
		}
		if (key === 'audienceAgeRanges') {
			const ages = value ? value.split(',') : [];
			setAudienceAgeRanges((prev) => {
				const updateAges: { [key: string]: boolean } = {
					'13-17': false,
					'18-24': false,
					'25-34': false,
					'35-44': false,
					'45-54': false,
					'55-64': false,
					'65+': false
				};
				ages.forEach((age: string) => {
					updateAges[age] = true;
				});
				return { ...prev, ...updateAges };
			});
		}
		if (key === 'audienceGenders') {
			const genders = value ? value.split(',') : [];
			setAudienceGenderFilter({
				female: genders.some((gender: string) => gender === 'f'),
				male: genders.some((gender: string) => gender === 'm'),
				unknown: genders.some((gender: string) => gender === 'x')
			});
		}
		if (key === 'audienceCountries' && audienceCountryFilter) {
			const newCountryData = getTheCountryData();
			const activeCountries: { [key: string]: { name: string; selected: boolean; show: boolean } } = {};
			const queryCountries = value ? value.split(',') : [];
			queryCountries.forEach((key: string) => {
				const countryCode = key.toLocaleUpperCase();
				activeCountries[countryCode] = { name: audienceCountryFilter[countryCode].name, selected: true, show: false };
			});
			setAudienceCountryFilter({ ...newCountryData, ...activeCountries });
		}
	};

	useEffect(() => {
		if (filter) {
			Object.entries(filter).forEach(([key, value]) => {
				setDefaultFilter({ key: key, value: value });
			});
		}
	}, [filter]);

	return (
		<Styled.Wrapper className={classNames({ isVisible: props.isVisible })}>
			<Styled.InnerWrapper>
				<Styled.FilterItemWapper>
					<Accordion title='Age'>
						<Styled.FollowersInputsWrapper>
							<Input
								label='min'
								type='text'
								pattern='[0-9]*'
								value={isNaN(Number(influencerMinAge)) ? '' : Number(influencerMinAge)}
								onChange={(e) => {
									setInfluencerMinAge(+e.target.value);
								}}
								onBlur={updateFilter}
							/>
							<Styled.BetweenMinMaxDiv>-</Styled.BetweenMinMaxDiv>
							<Input
								label='max'
								type='text'
								pattern='[0-9]*'
								value={isNaN(Number(influencerMaxAge)) ? '' : Number(influencerMaxAge)}
								onChange={(e) => {
									setInfluencerMaxAge(+e.target.value);
								}}
								onBlur={updateFilter}
							/>
						</Styled.FollowersInputsWrapper>
					</Accordion>
				</Styled.FilterItemWapper>

				<Styled.FilterItemWapper>
					<Accordion title='Genders'>
						{Object.keys(influencerGenderFilter).map((gender) => {
							return (
								<Checkbox
									key={gender}
									label={getGenderLabel(gender)}
									onChange={() => {
										updateGenderHandler(gender, 'influencer');
									}}
									checked={influencerGenderFilter[gender]}
								/>
							);
						})}
					</Accordion>
				</Styled.FilterItemWapper>

				<Styled.FilterItemWapper>
					<Accordion title='Countries'>
						<SearchInput
							type='text'
							value={influencerCountrySearchInput || ''}
							onChange={(e) => {
								setInfluencerCountrySearchInput(e.target.value);
								searchCountryHandler(setInfluencerCountryFilter, e.target.value, influencerCountryFilter);
							}}
							onReset={() => {
								setInfluencerCountrySearchInput('');
								searchCountryHandler(setInfluencerCountryFilter, '', influencerCountryFilter);
							}}
							searchIcon={false}
							iconSize='24'
						/>
						<Styled.AccordionInnerWrapper className='selected-countries'>
							<div>
								Selected countries (
								{influencerCountryFilter &&
									Object.keys(influencerCountryFilter).filter((countryCode) => {
										return influencerCountryFilter[countryCode].selected;
									}).length}
								)
							</div>
							{influencerCountryFilter &&
								Object.keys(influencerCountryFilter)
									.filter((countryCode) => {
										return influencerCountryFilter[countryCode].selected;
									})
									.map((item) => {
										return (
											<Checkbox
												key={item}
												label={influencerCountryFilter[item].name}
												checked={influencerCountryFilter[item].selected}
												onChange={() =>
													selectOneCountry(setInfluencerCountryFilter, item, influencerCountryFilter).then((updateFilterFunction) => {
														updateFilterFunction();
													})
												}
											/>
										);
									})}
						</Styled.AccordionInnerWrapper>
						<Styled.AccordionInnerWrapper className='countries-list'>
							{influencerCountryFilter &&
								Object.keys(influencerCountryFilter)
									.filter((countryCode) => {
										return influencerCountryFilter[countryCode].show;
									})
									.map((item) => {
										return (
											<Checkbox
												key={item}
												label={influencerCountryFilter[item].name}
												checked={influencerCountryFilter[item].selected}
												onChange={() =>
													selectOneCountry(setInfluencerCountryFilter, item, influencerCountryFilter).then((updateFilterFunction) => {
														updateFilterFunction();
													})
												}
											/>
										);
									})}
						</Styled.AccordionInnerWrapper>
					</Accordion>
				</Styled.FilterItemWapper>

				<Styled.FilterItemWapper>
					<Accordion title='Followers'>
						<Styled.FollowersInputsWrapper>
							<Input
								label='min'
								type='text'
								pattern='[0-9]*'
								value={isNaN(Number(minFollower)) ? '' : Number(minFollower)}
								onChange={(e) => {
									setMinFollower(+e.target.value);
								}}
								onBlur={updateFilter}
							/>
							<Styled.BetweenMinMaxDiv>-</Styled.BetweenMinMaxDiv>
							<Input
								label='max'
								type='text'
								pattern='[0-9]*'
								value={isNaN(Number(maxFollower)) ? '' : Number(maxFollower)}
								onChange={(e) => {
									setMaxFollower(+e.target.value);
								}}
								onBlur={updateFilter}
							/>
						</Styled.FollowersInputsWrapper>
					</Accordion>
				</Styled.FilterItemWapper>
				<Styled.FilterItemWapper>
					<Accordion title='Brand Mentions'>
						<BrandMentionsFilter brandMentions={brandMentionFilter} onSetFilter={changeBrandMentions} onRemoveBrand={removeBrandMentions} />
					</Accordion>
				</Styled.FilterItemWapper>

				<Styled.FilterItemWapper>
					<Accordion title={`Audience's Ages`}>
						{Object.keys(audienceAgeRanges).map((ageRange) => {
							return (
								<Checkbox
									key={ageRange}
									label={ageRange}
									onChange={() => {
										updateAudienceAgeRange(ageRange);
									}}
									checked={audienceAgeRanges[ageRange]}
								/>
							);
						})}
					</Accordion>
				</Styled.FilterItemWapper>
				<Styled.FilterItemWapper>
					<Accordion title={`Audience's Genders`}>
						{Object.keys(audienceGenderFilter).map((gender) => {
							return (
								<Checkbox
									key={gender}
									label={getGenderLabel(gender)}
									onChange={() => {
										updateGenderHandler(gender, 'audience');
									}}
									checked={audienceGenderFilter[gender]}
								/>
							);
						})}
					</Accordion>
				</Styled.FilterItemWapper>
				<Styled.FilterItemWapper>
					<Accordion title={`Audience's Countries`}>
						<SearchInput
							type='text'
							value={audienceCountrySearchInput || ''}
							onChange={(e) => {
								setAudienceCountrySearchInput(e.target.value);
								searchCountryHandler(setAudienceCountryFilter, e.target.value, audienceCountryFilter);
							}}
							onReset={() => {
								setAudienceCountrySearchInput('');
								searchCountryHandler(setAudienceCountryFilter, '', audienceCountryFilter);
							}}
							searchIcon={false}
							iconSize='24'
						/>
						<Styled.AccordionInnerWrapper className='selected-countries'>
							<div>
								Selected countries (
								{audienceCountryFilter &&
									Object.keys(audienceCountryFilter).filter((countryCode) => {
										return audienceCountryFilter[countryCode].selected;
									}).length}
								)
							</div>
							{audienceCountryFilter &&
								Object.keys(audienceCountryFilter)
									.filter((countryCode) => {
										return audienceCountryFilter[countryCode].selected;
									})
									.map((item) => {
										return (
											<Checkbox
												key={item}
												label={audienceCountryFilter[item].name}
												checked={audienceCountryFilter[item].selected}
												onChange={() =>
													selectOneCountry(setAudienceCountryFilter, item, audienceCountryFilter).then((updateFilterFunction) => {
														updateFilterFunction();
													})
												}
											/>
										);
									})}
						</Styled.AccordionInnerWrapper>
						<Styled.AccordionInnerWrapper className='countries-list'>
							{audienceCountryFilter &&
								Object.keys(audienceCountryFilter)
									.filter((countryCode) => {
										return audienceCountryFilter[countryCode].show;
									})
									.map((item) => {
										return (
											<Checkbox
												key={item}
												label={audienceCountryFilter[item].name}
												checked={audienceCountryFilter[item].selected}
												onChange={() =>
													selectOneCountry(setAudienceCountryFilter, item, audienceCountryFilter).then((updateFilterFunction) => {
														updateFilterFunction();
													})
												}
											/>
										);
									})}
						</Styled.AccordionInnerWrapper>
					</Accordion>
				</Styled.FilterItemWapper>
			</Styled.InnerWrapper>
		</Styled.Wrapper>
	);
};

export default FilterDrawer;
