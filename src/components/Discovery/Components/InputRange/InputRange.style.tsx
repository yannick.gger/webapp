import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	width: 100%;
	padding: ${guttersWithRem.xl} 1rem;
	display: flex;
	justify-content: center;
	flex-wrap: wrap;
`;

const RangeWrapper = styled.div`
	height: 36px;
	display: flex;
	width: 100%;
`;

type ITrackStyle = {
	backgroundColor: any;
};

const Track = styled.div<ITrackStyle>`
	height: 5px;
	width: 100%;
	border-radius: 4px;
	background: ${(props) => props.backgroundColor};
	align-self: center;
`;

const Thumb = styled.div`
	height: 16px;
	width: 16px;
	border-radius: 50%;
	background-color: ${colors.white};
	display: flex;
	justify-content: center;
	align-items: center;
	box-shadow: 0px 2px 6px ${colors.borderColor};
`;

const FlexDiv = styled.div`
	display: flex;
	column-gap: ${guttersWithRem.s};
	align-items: flex-end;
`;

const BetweenMinMaxInputDiv = styled(FlexDiv)`
	height: 38px;
	align-items: center;
`;

const Styled = {
	Wrapper,
	RangeWrapper,
	Track,
	Thumb,
	FlexDiv,
	BetweenMinMaxInputDiv
};

export default Styled;
