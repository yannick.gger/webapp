import { useState, useEffect } from 'react';
import { Range, getTrackBackground } from 'react-range';
import colors from 'styles/variables/colors';
import Styled from './InputRange.style';
import Input from '../Input';

const InputRange = (props: { min?: number; max?: number; values: number[]; onChange: (values: number[]) => void; step?: number }) => {
	const [min, setMin] = useState(0);
	const [max, setMax] = useState(100);
	const [enteredMin, setEnteredMin] = useState(0);
	const [enteredMax, setEnteredMax] = useState(100);
	const [values, setValues] = useState([13, 65]);

	useEffect(() => {
		if (props.min !== undefined && props.max !== undefined) {
			setMin(props.min);
			setMax(props.max);
			setEnteredMin(props.min);
			setEnteredMax(props.max);
		}
	}, []);

	useEffect(() => {
		setValues([enteredMin, enteredMax]);
	}, [enteredMin, enteredMax]);

	useEffect(() => {
		props.onChange(values);
	}, [values]);

	const setNewNumber = (value: number, type: 'min' | 'max') => {
		if (type === 'min') {
			if (value < min || value > max) {
				setEnteredMin(min);
			} else {
				if (enteredMax < value) {
					setEnteredMax(value);
				} else {
					setEnteredMin(value);
				}
			}
		}
		if (type === 'max') {
			if (value > max || value < min) {
				setEnteredMax(max);
			} else {
				if (enteredMin > value) {
					setEnteredMin(value);
				} else {
					setEnteredMax(value);
				}
			}
		}
	};

	return (
		<Styled.Wrapper>
			<Range
				values={values}
				step={props.step || 1}
				min={min}
				max={max}
				onChange={(values) => {
					setEnteredMin(values[0]);
					setEnteredMax(values[1]);
				}}
				renderTrack={({ props, children }) => (
					<Styled.RangeWrapper
						onMouseDown={props.onMouseDown}
						onTouchStart={props.onTouchStart}
						style={{
							...props.style
						}}
					>
						<Styled.Track
							ref={props.ref}
							backgroundColor={getTrackBackground({
								values,
								colors: [colors.borderColor, colors.black, colors.borderColor],
								min: min,
								max: max
							})}
						>
							{children}
						</Styled.Track>
					</Styled.RangeWrapper>
				)}
				renderThumb={({ props, isDragged }) => (
					<Styled.Thumb
						{...props}
						style={{
							...props.style
						}}
					/>
				)}
			/>
			<Styled.FlexDiv>
				<Input
					label='min'
					type='number'
					value={Number(enteredMin).toString()}
					onChange={(e) => {
						setEnteredMin(+e.target.value);
					}}
					onBlur={(e) => {
						setNewNumber(enteredMin, 'min');
					}}
				/>
				<Styled.BetweenMinMaxInputDiv>-</Styled.BetweenMinMaxInputDiv>
				<Input
					label='max'
					type='number'
					value={Number(enteredMax).toString()}
					onChange={(e) => {
						setEnteredMax(+e.target.value);
					}}
					onBlur={(e) => {
						setNewNumber(+e.target.value, 'max');
					}}
				/>
			</Styled.FlexDiv>
		</Styled.Wrapper>
	);
};

export default InputRange;
