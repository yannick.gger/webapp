import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';

const Wrapper = styled.div`
	position: absolute;
	z-index: 3;
	width: 100%;
	max-height: 350px;
	overflow-y: auto;
	background-color: ${colors.white};
	border: 2px solid;
`;

const AutoSearchWrap = styled.ul`
	list-style-type: none;
	padding: ${guttersWithRem.xs} 0;
	margin: 0;
`;

const AutoSearchData = styled.li`
	width: 100%;
	padding: ${guttersWithRem.xs};
	font-family: ${typography.BaseFontFamiliy};

	&:hover {
		background-color: ${colors.discovery.autoCompleteHover};
		cursor: pointer;
	}
	position: relative;

	&.selected {
		background-color: ${colors.discovery.autoCompleteHover};
		cursor: pointer;
	}
`;

const Styled = {
	Wrapper,
	AutoSearchWrap,
	AutoSearchData
};

export default Styled;
