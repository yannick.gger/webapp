import Styled from './AutoCompleteList.style';

const AutoCompleteList = (props: {
	options: { id: string; label: string; isInfluencer: boolean }[];
	onSelect: (id: string, label: string, isInfluencer: boolean) => void;
	cursor: number;
	onCursor: (id: string, label: string, isInfluencer: boolean) => void;
}) => {
	return (
		<Styled.Wrapper>
			<Styled.AutoSearchWrap>
				{props.options &&
					props.options.map((option, index) => {
						if (index === props.cursor) {
							props.onCursor(option.id, option.label, option.isInfluencer);
						}
						return (
							<Styled.AutoSearchData
								key={option.id}
								onClick={() => {
									props.onSelect(option.id, option.label, option.isInfluencer);
								}}
								className={index === props.cursor ? 'selected' : ''}
							>
								{option.isInfluencer ? `@${option.label}` : option.label}
							</Styled.AutoSearchData>
						);
					})}
			</Styled.AutoSearchWrap>
		</Styled.Wrapper>
	);
};

export default AutoCompleteList;
