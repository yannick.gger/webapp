import { useContext } from 'react';
import DiscoveryContext from 'contexts/Discovery';
import Styled from './LookALikeLoading.style';
import Checkbox from 'components/Checkbox';
import FixedInfluencerListItem from './Components/FixedInfluencerListItem';

const LookALikeLoading = () => {
	const { lookALikeInfluencerName } = useContext(DiscoveryContext);

	return (
		<Styled.PageWrapper>
			<Styled.FixedShadowCover>{lookALikeInfluencerName && <div>{`Analysing ${lookALikeInfluencerName} `}</div>}</Styled.FixedShadowCover>
			<Styled.Table cellSpacing={0}>
				<Styled.TableHeader>
					<Styled.TableRow>
						<Styled.Th>
							<Styled.CheckboxWrapper>
								<Styled.FlexDiv columnGap='1rem' position='absolute' left='-5px'>
									<Styled.CheckboxInnerWapper columnGap='0.25rem' alignItems='center'>
										<Checkbox onChange={() => {}} checked={false} />
										<Styled.CustomIcon icon='chevron-down' size='16' />
									</Styled.CheckboxInnerWapper>
								</Styled.FlexDiv>
							</Styled.CheckboxWrapper>
						</Styled.Th>
						<Styled.Th />
						<Styled.Th>Topic score</Styled.Th>
						<Styled.Th>Engagement</Styled.Th>
						<Styled.Th>Age span</Styled.Th>
						<Styled.Th>Location</Styled.Th>
						<Styled.Th>Gender</Styled.Th>
						<Styled.Th>Categories</Styled.Th>
					</Styled.TableRow>
				</Styled.TableHeader>
				<Styled.TableBody>
					<FixedInfluencerListItem />
					<FixedInfluencerListItem />
					<FixedInfluencerListItem />
					<FixedInfluencerListItem />
					<FixedInfluencerListItem />
					<FixedInfluencerListItem />
					<FixedInfluencerListItem />
				</Styled.TableBody>
			</Styled.Table>
		</Styled.PageWrapper>
	);
};

export default LookALikeLoading;
