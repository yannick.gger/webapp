import { CSSProperties } from 'react';
import styled from 'styled-components';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import Avatar from 'components/Avatar';

const wrapperPadding = `${guttersWithRem.l} ${guttersWithRem.s}`;

const Wrapper = styled.tr`
	width: 100%;
	overflow-x: auto;
	background-color: ${colors.white};
	border: 2px solid ${colors.discovery.border};
	border-left: none;
	border-right: none;
`;

const Td = styled.td<CSSProperties>`
	padding: ${wrapperPadding};
	text-align: ${(props) => props.textAlign || 'left'};
	max-width: ${(props) => props.maxWidth};
	height: ${(props) => props.maxHeight || '50px'};
`;

const Div = styled.div<CSSProperties>`
	display: ${(props) => props.display};
	justify-content: ${(props) => props.justifyContent};
	align-items: ${(props) => props.alignItems};
	column-gap: ${(props) => props.columnGap};
	row-gap: ${(props) => props.rowGap};
	font-family: ${typography.list.fontFamily};
	overflow-x: ${(props) => props.overflowX};
`;

const InfluencerName = styled(Div)`
	font-weight: ${typography.list.fontWeight};
	font-size: ${typography.list.fontSize};
`;

const FollowersNumber = styled(Div)`
	font-family: ${typography.list.accessory.fontFamilies};
	font-size: ${typography.list.small.fontSize};
	white-space: nowrap;
`;

const Data = styled(Div)`
	font-weight: ${typography.list.fontWeight};
	font-size: ${typography.list.medium.fontSize};
	white-space: nowrap;
`;

const IconContainer = styled.div`
	cursor: pointer;
	max-height: 32px;
`;

const CustomAvatar = styled(Avatar)`
	margin: 0;
`;

const Styled = {
	Wrapper,
	Td,
	Div,
	InfluencerName,
	FollowersNumber,
	Data,
	IconContainer,
	CustomAvatar
};

export default Styled;
