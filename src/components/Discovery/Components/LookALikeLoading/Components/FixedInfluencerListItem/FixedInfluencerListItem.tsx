import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import { guttersWithRem } from 'styles/variables/gutter';
import Styled from './FixedInfluencerListItem.style';
import Checkbox from 'components/Checkbox';

const FixedInfluencerListItem = () => {
	return (
		<Styled.Wrapper>
			<Styled.Td>
				<Styled.Div display='flex'>
					<Checkbox checked={false} onChange={() => {}} />
				</Styled.Div>
			</Styled.Td>
			<Styled.Td maxWidth='250px'>
				<Styled.Div display='flex' alignItems='center' columnGap={guttersWithRem.m} overflowX='auto'>
					<Skeleton style={{ width: '40px', height: '40px' }} circle />
					<Skeleton style={{ width: '100px' }} count={2} />
				</Styled.Div>
			</Styled.Td>
			<Styled.Td>
				<Skeleton />
			</Styled.Td>
			<Styled.Td>
				<Skeleton />
			</Styled.Td>
			<Styled.Td>
				<Skeleton />
			</Styled.Td>
			<Styled.Td>
				<Skeleton />
			</Styled.Td>
			<Styled.Td maxWidth='100px'>
				<Skeleton />
			</Styled.Td>
			<Styled.Td>
				<Skeleton />
			</Styled.Td>
			<Styled.Td maxWidth='200px'>
				<Skeleton />
			</Styled.Td>
		</Styled.Wrapper>
	);
};

export default FixedInfluencerListItem;
