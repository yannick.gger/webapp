import { CSSProperties } from 'react';
import styled from 'styled-components';
import typography from 'styles/variables/typography';
import Icon from 'components/Icon';
import { guttersWithRem } from 'styles/variables/gutter';

const PageWrapper = styled.div`
	width: 100%;
	overflow-x: auto;
`;

const thPadding = `${guttersWithRem.xs} ${guttersWithRem.s}`;

const Table = styled.table`
	width: 100%;
`;

const TableRow = styled.tr``;

const Th = styled.th<CSSProperties>`
	font-family: ${typography.list.accessory.fontFamilies};
	font-size: ${typography.list.medium.fontSize};
	font-weight: ${typography.list.accessory.fontWeight};
	text-align: ${(props) => props.textAlign || 'left'};
	padding: ${thPadding};
	white-space: nowrap;
`;

const TableHeader = styled.thead`
	width: 100%;
`;

const TableBody = styled.tbody``;

const CustomIcon = styled(Icon)`
	line-height: 0;
`;

const CheckboxWrapper = styled.div`
	position: relative;
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
`;

const FlexDiv = styled.div<CSSProperties>`
	display: flex;
	height: 24px;
	position: ${(props) => props.position};
	column-gap: ${(props) => props.columnGap};
	align-items: ${(props) => props.alignItems};
	top: ${(props) => props.top};
	left: ${(props) => props.left};
	right: ${(props) => props.right};
	bottom: ${(props) => props.bottom};
`;

const CheckboxInnerWapper = styled(FlexDiv)`
	padding: 0 5px;
`;

const FixedShadowCover = styled.div`
	position: absolute;
	top: 150px;
	bottom: 0;
	right: 0;
	left: 0;
	display: flex;
	justify-content: center;
	z-index: 100;

	& > div {
		border-radius: 10px;
		padding: 10px 20px;
		height: fit-content;
		background-color: rgba(255, 255, 255, 0.5);
		font-size: ${typography.headings.h4.fontSize};
	}
`;

const Styled = {
	PageWrapper,
	FixedShadowCover,
	Table,
	TableHeader,
	TableRow,
	Th,
	CheckboxWrapper,
	FlexDiv,
	CheckboxInnerWapper,
	CustomIcon,
	TableBody
};

export default Styled;
