import React from 'react';
import Styled from './SearchInput.style';
interface ISearchInput extends React.InputHTMLAttributes<HTMLInputElement> {
	onReset: () => void;
	searchIcon?: boolean;
	iconSize?: '10' | '16' | '24' | '32' | '40';
	lookALike?: string | null;
	removeLookALike?: () => void;
}

const SearchInput = (props: ISearchInput & {}) => {
	const { searchIcon = true, iconSize = '40' } = props;

	return (
		<Styled.SearchInputWrapper>
			{searchIcon ? <Styled.CustomIcon icon='search' size={iconSize} /> : null}
			{props.lookALike && (
				<Styled.LookALikePillWrapper>
					<Styled.IconContainer>
						<Styled.CustomIcon icon='look-a-like' />
					</Styled.IconContainer>
					<Styled.LookALikeText>{props.lookALike}</Styled.LookALikeText>
					<Styled.IconContainer onClick={props.removeLookALike}>
						<Styled.CustomIcon icon='cancel' size='16' />
					</Styled.IconContainer>
				</Styled.LookALikePillWrapper>
			)}
			<Styled.Input {...props} />
			{props.value && (
				<Styled.IconContainer onClick={props.onReset}>
					<Styled.CustomIcon icon='cancel-circle' size={iconSize} />
				</Styled.IconContainer>
			)}
		</Styled.SearchInputWrapper>
	);
};

export default SearchInput;
