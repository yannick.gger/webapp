import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import Icon from 'components/Icon';

const inputPadding = `${guttersWithRem.xxs} ${guttersWithRem.m}`;

const Input = styled.input`
	width: 100%;
	min-height: 56px;
	border-radius: 0;
	border: none;
	&:focus {
		outline: none;
	}
`;

const SearchInputWrapper = styled.div`
	width: 100%;
	max-height: 64px;
	font-family: ${typography.input.fontFamilies};
	font-size: ${typography.input.fontSize};
	line-height: ${typography.input.lineHeight};
	border: 2px solid ${colors.discovery.tagBorder};
	border-radius: 2px;
	background-color: ${colors.white};
	overflow: hidden;
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.xxs};
	padding: ${inputPadding};

	&:focus-within {
		box-shadow: 0px 0px 4px 0px ${colors.discovery.inputFocus};
	}
`;

const IconContainer = styled.div`
	display: flex;
	cursor: pointer;
`;

const CustomIcon = styled(Icon)`
	line-height: 0;
`;

const LookALikePillWrapper = styled.div`
	min-height: 50px;
	max-width: 200px;
	border-radius: 50px;
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.xxs};
	padding: ${guttersWithRem.xs};
	border: 1px solid ${colors.black};
`;

const LookALikeText = styled.div`
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
`;

const Styled = {
	SearchInputWrapper,
	IconContainer,
	CustomIcon,
	Input,
	LookALikePillWrapper,
	LookALikeText
};

export default Styled;
