import React, { Component } from 'react';
import { Carousel, Icon } from 'antd';
import { withRouter } from 'react-router-dom';
import TodoCard from 'components/todo-card';
import todoDatas from './handle';
import './styles.scss';
import { ApolloConsumer } from 'react-apollo';

const SampleNextArrow = (props) => {
	const { className, style, disabled, onClick } = props;
	return <Icon type='right-circle' theme='filled' className={`${className} ${disabled ? 'arrow-disabled' : ''}`} style={{ ...style }} onClick={onClick} />;
};

const SamplePrevArrow = (props) => {
	const { className, style, disabled, onClick } = props;
	return <Icon type='left-circle' theme='filled' className={`${className} ${disabled ? 'arrow-disabled' : ''}`} style={{ ...style }} onClick={onClick} />;
};

class TodoCarousel extends Component {
	state = {
		focusedTodoIndex: 0
	};

	getStartingIndex = () => {
		const { todos } = this.props;

		let i = todos.map((todo) => todo.status).indexOf('in_progress');
		if (i < 0) i = todos.map((todo) => todo.status).indexOf('fail');
		if (i < 0) i = todos.map((todo) => todo.status).indexOf('future');
		if (i < 0) i = todos.length - 1;

		return i;
	};

	componentDidMount() {
		this.setState({ focusedTodoIndex: this.getStartingIndex() });
	}

	render() {
		const { todos, campaign } = this.props;
		const { focusedTodoIndex } = this.state;

		let currentTodoCount = todos.filter((todo) => todo.status === 'in_progress').length;

		return (
			<ApolloConsumer>
				{(client) => {
					return (
						<div className='todo-carousel-components'>
							<div className='carousel-components'>
								<div className='carousel-header'>
									<div className='header-left'>
										<span className='caption'>{currentTodoCount ? 'Todos to complete' : 'No todos to complete'}</span>
										{currentTodoCount > 0 && (
											<span className='badge'>
												<span>{currentTodoCount}</span>
											</span>
										)}
									</div>
								</div>
								<Carousel
									dots={false}
									arrows={true}
									className='carousel-content center'
									slidesToShow={1}
									slidesToScroll={1}
									initialSlide={this.getStartingIndex()}
									infinite={false}
									centerMode={true}
									centerPadding='60px'
									speed={500}
									prevArrow={<SamplePrevArrow disabled={focusedTodoIndex === 0} />}
									nextArrow={<SampleNextArrow disabled={focusedTodoIndex === todos.length - 1} />}
									beforeChange={(_i, j) => this.setState({ focusedTodoIndex: j })}
								>
									{todos.map((todo, key) => {
										if (!todo) return false;
										const item = todoDatas[todo.type];

										return !item ? null : (
											<TodoCard
												key={key}
												focused={key === focusedTodoIndex}
												todoType={todo.type}
												todoStatus={todo.status}
												icon={item.icon}
												info={item.info(todo)}
												extra={item.extra(todo)}
												className={item.className}
												extraColor={item.extraColor(todo)}
												actions={item.actions(todo, this.props.parent, client, campaign)}
												connectedWithFacebook={this.props.connectedWithFacebook}
												modalInfoUrl={todo.link}
											>
												{todo.assignment ? item.title(todo.assignment.instagramType) : item.title('post')}
											</TodoCard>
										);
									})}
								</Carousel>
							</div>
						</div>
					);
				}}
			</ApolloConsumer>
		);
	}
}

export default withRouter(TodoCarousel);
