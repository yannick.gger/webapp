import React from 'react';
import moment from 'moment';
import { Icon, Popover } from 'antd';
import CountDown from 'components/countdown';
import { apiToMoment } from 'utils';
import { ReviewIcon, UploadIcon, StatsIcon, InvoiceIcon } from 'components/icons';
import AcceptInvite from 'views/influencer/tasks/join-or-reject.svg';
import PostOnInsta from 'views/influencer/tasks/post-on-insta.svg';
import gql from 'graphql-tag';
import InfluencerService from 'services/Influencer';

const DECLINE_CAMPAIGN = gql`
	mutation declineCampaign($campaignId: ID!) {
		declineCampaign(input: { campaignId: $campaignId }) {
			campaign {
				id
			}
		}
	}
`;

export const extraDay = (item) => {
	if (item.status === 'in_progress' && moment(item.deadline).isValid()) {
		return moment(item.deadline)
			.fromNow()
			.replace(/^in/, '');
	}
};
export const extraColor = (item) => item.status === 'in_progress' && (moment.duration(moment(item.deadline).diff(moment())).asHours() < 48 ? 'red' : 'blue');

const statusOverAction = (item) => {
	const iconParams = { theme: 'filled', style: { fontSize: '40px' } };

	switch (item.status) {
		case 'success':
			return [
				<div domType='link' key={1} targetUrl={`#`} color='green'>
					<Popover title='Completed' placement='topRight' content='This step is done. Good job!'>
						<Icon type='check-circle' {...iconParams} />
					</Popover>
				</div>
			];
		case 'pending':
			return [
				<div domType='link' key={1} targetUrl={`/influencer/campaigns/${item.campaignId}/assignments/${item.assignment.id}/edit-post-modal`} color='blue'>
					<Popover title='Edit content' placement='topRight' content='Awaiting action from the campaign manager'>
						<Icon type='edit' theme='filled' style={{ fontSize: '30px' }} />
					</Popover>
				</div>
			];
		case 'fail':
			return [
				<div domType='link' key={1} targetUrl={`#`} color='red'>
					<Popover title='Unsuccessful' placement='topRight' content='You did not complete this task succesfully'>
						<Icon type='close-circle' {...iconParams} />
					</Popover>
				</div>
			];
		case 'future':
			return [
				<div domType='link' key={1} targetUrl={`#`} color='gray'>
					<Popover title='Future' placement='topRight' content='Complete previous steps and await the start time for this task'>
						<Icon type='clock-circle' {...iconParams} />
					</Popover>
				</div>
			];
		default:
			return;
	}
};

export default {
	TodoAcceptInvite: {
		extraColor,
		title: () => 'Invite to join',
		className: 'todo-item-carousel todo-accept-invite',
		icon: AcceptInvite,
		status: 'future',
		extra: extraDay,
		actions: (item, parent = 'campaigns', client, campaign) => {
			if (item.status !== 'in_progress') return statusOverAction(item);

			return [
				<div
					domType='button'
					key={1}
					onClick={() => {
						InfluencerService.joinCampaign(campaign.campaign.id, campaign.influencer.id);
					}}
					color='green'
				>
					Join
				</div>,
				<div
					domType='button'
					key={2}
					onClick={() => {
						client.mutate({
							mutation: DECLINE_CAMPAIGN,
							variables: item.campaignId
						});
					}}
					color='red'
				>
					Decline
				</div>
			];
		},
		info: (item) => {
			if (item.failed) {
				return [
					{
						text: (
							<span className='show-ellipsis-line-2'>
								{item.spotsLeft === 0 ? 'The campaign is full' : apiToMoment(item.deadline) < moment() ? 'Invites closed' : "You've declined the campaign"}
							</span>
						)
					}
				];
			} else if (item.completed) {
				return [
					{
						text: <span className='show-ellipsis-line-2'>You've joined the campaign</span>
					}
				];
			}

			return [
				{
					text: (
						<span className='show-ellipsis-line-2'>
							Invite closes in <CountDown expiryDate={apiToMoment(item.deadline)} />
						</span>
					)
				}
			];
		}
	},
	TodoUploadPost: {
		extraColor,
		title: (type) => `Upload ${type} for review`,
		className: 'todo-item-carousel todo-upload-post',
		icon: <ReviewIcon />,
		status: 'future',
		info: (item) => {
			if (item.failed) return [{ text: <span className='show-ellipsis-line-2'>Your content was rejected</span> }];
			else if (item.completed) return [{ text: <span className='show-ellipsis-line-2'>Your content was approved</span> }];

			return [
				{
					text: <span className='show-ellipsis-line-2'>Upload your content for review</span>
				}
			];
		},
		extra: extraDay,
		actions: (item, parent = 'campaigns') => {
			if (item.status !== 'in_progress') return statusOverAction(item);

			if (!item || !item.campaignId || !item.assignment) return [];
			return [
				<div
					domType='link'
					key={1}
					targetUrl={`/influencer/${parent}/${item.campaignId}/assignments/${item.assignment.id}/review/modal`}
					icon={<UploadIcon />}
					color='blue'
				>
					Upload
					<br />
					here
				</div>
			];
		}
	},
	TodoPostInstagram: {
		extraColor,
		title: (type) => `Confirm publishing of ${type}`,
		className: 'todo-item-carousel todo-post-instagram',
		icon: PostOnInsta,
		status: 'future',
		info: (item) => {
			if (item.completed) return [{ text: <span className='show-ellipsis-line-2'>Upload confirmed</span> }];

			return [
				{
					text: <span className='show-ellipsis-line-2'>Confirm that you've uploaded</span>
				}
			];
		},
		extra: extraDay,
		actions: (item, parent = 'campaigns') => {
			if (item.status !== 'in_progress') return statusOverAction(item);

			if (!item || !item.campaignId || !item.assignment) {
				return [];
			}

			const todoElementProps = {
				domType: 'link',
				targetUrl: `/influencer/${parent}/${item.campaignId}/assignments/${item.assignment.id}/info/modal`
			};

			return [
				<div key={1} {...todoElementProps} color='blue'>
					Confirm
					<br />
					here
				</div>
			];
		}
	},
	TodoUploadStat: {
		extraColor,
		title: (type) => `Upload ${type} stats`,
		className: 'todo-item-carousel todo-upload-stats',
		icon: <StatsIcon />,
		status: 'future',
		info: (item) => {
			if (!item.campaignId || !item.assignment) return [];
			if (item.completed)
				return [
					{
						text: <span className='show-ellipsis-line-2'>Stats uploaded for "{item.assignment.name}"</span>
					}
				];

			return [
				{
					text: <span className='show-ellipsis-line-2'>For assignment "{item.assignment.name}"</span>
				}
			];
		},
		extra: extraDay,
		actions: (item, parent = 'campaigns') => {
			if (item.status !== 'in_progress') return statusOverAction(item);

			if (!item.campaignId || !item.assignment) return [];
			return [
				<div
					domType='link'
					key={1}
					targetUrl={`/influencer/${parent}/${item.campaignId}/assignments/${item.assignment.id}/report/modal`}
					icon={<UploadIcon />}
					color='blue'
				>
					Upload
					<br />
					here
				</div>
			];
		}
	},
	TodoSendInvoice: {
		extraColor,
		title: (type) => `Time to invoice`,
		className: 'todo-item-carousel todo-upload-stats',
		icon: <InvoiceIcon />,
		status: 'future',
		info: (item) => {
			return [
				{
					text: <span className='show-ellipsis-line-2'>Payday!!</span>
				}
			];
		},
		extra: extraDay,
		actions: (item, parent = 'campaigns') => {
			if (item.status !== 'in_progress') return statusOverAction(item);

			return [
				<div domType='link' key={1} targetUrl={`/influencer/${parent}/${item.campaignId}/campaign-invoice-info/modal`} color='blue'>
					View info
				</div>
			];
		}
	}
};
