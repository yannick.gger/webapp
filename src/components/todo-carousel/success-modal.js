import React, { Component } from 'react';
import { Card, Modal, Alert } from 'antd';
import { Link } from 'react-router-dom';
import SuccessImg from 'views/influencer/assignment-report/success.png';
class Success extends Component {
	render() {
		const { campaign, currentUser, onCancel } = this.props;
		const { paymentCompensationsCount, productCompensationsCount, invoiceCompensationsCount } = campaign;
		let messageSuccessNode = null;
		if (paymentCompensationsCount > 0 || invoiceCompensationsCount > 0) {
			messageSuccessNode = (
				<p className='success-modal-description'>
					Collabs will fetch the stats for your post in 2 days - after this is done you will get your payment. If this is not successful (auto getting stats
					from your further further instructions to upload a screenshot of the stats.
				</p>
			);
		} else if (productCompensationsCount > 0) {
			messageSuccessNode = (
				<p className='success-modal-description'>
					Collabs will fetch the stats for your post in 2 days. If this is not successful (auto getting stats from your post), you will get an email with
					further to a a screenshot of the stats.
				</p>
			);
		}
		return (
			<Modal
				width={921}
				title='Confirm publishing your content and mark as completed'
				visible
				onCancel={onCancel}
				wrapClassName='custom-modal title-center post-instagram-success-container'
				footer={[]}
			>
				<Card className='todo-card-group'>
					<div className='text-center mt-20 mb-30'>
						<div className='success-paragraph-wrapper'>
							<img src={SuccessImg} alt='' />
							<h2 className='color-blue mt-20 mb-0'>Great work!</h2>
							<p className='pb-10'>You are done for the moment!</p>
							<strong className='text-bold'>What happens now?</strong>
							{messageSuccessNode}
						</div>
					</div>
				</Card>
			</Modal>
		);
	}
}
export default Success;
