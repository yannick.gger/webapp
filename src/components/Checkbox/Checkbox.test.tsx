import { render } from '@testing-library/react';
import Checkbox from './Checkbox';

describe('<Checkbox /> test', () => {
	it('renders Checkbox', () => {
		const checkbox = render(<Checkbox label='Checkbox' />);
		expect(checkbox).toMatchSnapshot();
	});
});
