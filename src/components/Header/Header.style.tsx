import colors from 'styles/variables/colors';
import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import { breakpoints } from 'styles/variables/media-queries';
import { Link } from 'react-router-dom';
import typography from 'styles/variables/typography';
/**
 * @todo remove !important after we have change everything regarding the typography
 */

const gutters = guttersWithRem;
const wrapperPadding = `${gutters.m} ${gutters.xxl};`;

const Wrapper = styled.header`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	min-height: 72px;
	background-color: ${({ theme }) => theme.background};
	padding: ${wrapperPadding};
	z-index: 2099;
	transition: box-shadow 0.2s ease-in-out, background-color 0.2s ease-in-out;

	&.fixed-header {
		box-shadow: 0 4px 12px -4px rgb(0, 0, 0, 0.1);
	}
`;

const HeaderTop = styled.div`
	display: flex;
	align-items: center;
`;

const Logotype = styled.a`
	display: none;
	width: 100%;
	max-width: 92px;
	margin-left: 1rem;

	@media (min-width: ${breakpoints.md}) {
		display: block;
	}
`;

const Navigation = styled.nav`
	display: flex;
	align-items: center;
	position: relative;
	width: 160px;
`;

const UserList = styled.ul`
	display: flex;
	align-items: center;
	padding: 0;
	margin: 0 -8px;
	list-style: none;
	margin-left: auto;
	flex-shrink: 0;

	a {
		border-bottom: none;
	}
`;
const UserListItem = styled.li`
	display: inline-block;
	margin: 0 8px;
`;

const ComponentContainer = styled.div`
	display: flex;
	flex-grow: 1;
	align-items: center;

	@media (min-width: ${breakpoints.md}) {
		justify-content: center;
	}
`;

const ActionButton = styled.button`
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
	border: 2px #333 solid;
	background-color: transparent;
	padding: 0.25rem;
	border-radius: 60px;
	width: 40px;
	height: 40px;
	cursor: pointer;
	transition: background-color 0.2s ease-in-out;
	font-weight: 900;

	&:hover {
		background-color: #e9fcee;
	}

	.icon {
		line-height: 0;
	}

	span {
		padding-top: 0.1875rem;
		font-size: 0.875rem;
		line-height: 0;
	}
`;

const notificationCircle = styled.div`
	position: absolute;
	top: 0;
	right: -4px;
	width: 12px;
	height: 12px;
	background-color: #ff474e;
	border: 1px solid #fff;
	border-radius: 20px;
	transition: opacity 0.2s ease-in-out;
	opacity: 0;

	&.visible {
		opacity: 1;
	}
`;

const Styled = {
	Wrapper,
	HeaderTop,
	Navigation,
	UserList,
	UserListItem,
	ComponentContainer,
	Logotype,
	ActionButton,
	notificationCircle
};

export default Styled;
