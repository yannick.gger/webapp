import ConversationsService from 'services/Conversations';
import { ICollabsResponse } from 'services/Response.types';

export const useHeaderData = () => {
	const getUnreadConversations = () => {
		return ConversationsService.getUnreadCount().then((response: ICollabsResponse) => {
			if (!response.errors) {
				return response.data.attributes.count;
			}

			return 0;
		});
	};

	return {
		getUnreadConversations
	};
};
