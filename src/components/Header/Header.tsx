import Styled from './Header.style';
import { IHeaderProps } from './types';
import fallbackLogotype from 'assets/img/logo/collabs-logo-dark.svg';
import classNames from 'classnames';
import MainNavigation from 'components/MainNavigation/MainNavigation';
import { useEffect, useState } from 'react';
import Icon from 'components/Icon';
import { useHistory } from 'react-router-dom';
import { getCurrentOrganization } from 'shared/utils';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { FEATURE_FLAG_INTEGRATED_INBOX } from 'constants/feature-flag-keys';
import { useHeaderData } from './hooks';
import { AxiosError } from 'axios';
import { ApolloConsumer } from 'react-apollo';
import UserMenu from './Components/UserMenu/UserMenu';

/**
 * Header
 * @todo i18next
 * @param {IHeaderProps} props
 * @returns {JSX.Element}
 */
const Header = (props: IHeaderProps): JSX.Element => {
	let integratedInboxInterval: NodeJS.Timeout;

	const [scroll, setScroll] = useState<boolean>(false);
	const [newMessages, setNewMessages] = useState<number | undefined>(undefined);

	const history = useHistory();
	const orgSlug = getCurrentOrganization();

	const [isEnabled] = useFeatureToggle();
	const { getUnreadConversations } = useHeaderData();

	const POLL_INTERVAL = 30000;

	const handleScroll = () => {
		setScroll(window.pageYOffset > 0);
	};

	const messagePolling = () => {
		// @todo CONTEXT!!
		let hasError = false;

		getUnreadConversations()
			.then((data: number) => {
				setNewMessages(data);
			})
			.catch((error: AxiosError) => {
				// silent error output during polling, do not spam the user with roasted toasts :>
				console.error(`Integrated inbox poll: `, error.message);
				hasError = true;
			})
			.finally(() => {
				if (!hasError && newMessages !== undefined) {
					integratedInboxInterval = setInterval(() => {
						messagePolling();
					}, POLL_INTERVAL);
				}
			});
	};

	useEffect(() => {
		messagePolling();
		window.addEventListener('scroll', handleScroll, true);

		return () => {
			window.removeEventListener('scroll', handleScroll, true);
			clearInterval(integratedInboxInterval);
		};
	}, []);

	return (
		<ApolloConsumer>
			{(client) => (
				<Styled.Wrapper className={classNames('header-bar', { 'fixed-header': scroll })}>
					<Styled.HeaderTop>
						<Styled.Navigation>
							<MainNavigation menuItems={props.menuItems} location={props.location} />
							<Styled.Logotype href='/'>
								<img src={fallbackLogotype} alt='Collabs' />
							</Styled.Logotype>
						</Styled.Navigation>
						<Styled.UserList>
							{isEnabled(FEATURE_FLAG_INTEGRATED_INBOX) && (
								<Styled.UserListItem>
									<Styled.ActionButton onClick={() => history.push(`/${orgSlug}/integrated-inbox`)}>
										<Icon icon='mail' size='24' />
										<Styled.notificationCircle className={classNames({ visible: newMessages !== undefined && newMessages > 0 })} />
									</Styled.ActionButton>
								</Styled.UserListItem>
							)}
							<Styled.UserListItem>
								<UserMenu client={client} history={history} orgSlug={orgSlug} />
							</Styled.UserListItem>
						</Styled.UserList>
					</Styled.HeaderTop>
				</Styled.Wrapper>
			)}
		</ApolloConsumer>
	);
};

export default Header;
