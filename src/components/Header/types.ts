import { IMainNavigationProps } from 'components/MainNavigation/types';

export interface IHeaderProps extends IMainNavigationProps {
	fixed?: boolean;
	location?: Location;
}
