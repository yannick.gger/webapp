import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { RegularLink } from 'styles/utils/Link';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';
import { HashLink } from 'react-router-hash-link';

/**
 * References: Dropdown
 */

const ActionDropdownMenu = styled.div`
	position: absolute;
	transform: translate3d(0px, 37px, 0px);
	top: 4px;
	right: 0px;
	will-change: transform;
	z-index: 1000;
	display: none;
	float: left;
	min-width: 312px;
	padding: 2rem 1rem;
	margin: 2px 0 0;
	text-align: left;
	list-style: none;
	background-color: ${colors.dropdown.menu.background};
	background-clip: padding-box;
	border: 1px solid ${colors.dropdown.menu.borderColor};
	border-radius: 0.125rem;
	box-shadow: 0px 5px 10px ${colors.dropdown.menu.boxShadowColor};
`;

const ActionDropdownWrapper = styled.div`
	position: relative;

	&.show {
		${ActionDropdownMenu} {
			display: block;
		}
	}
`;

const ActionDropdownMenuItem = styled.div`
	position: relative;
	font-weight: 500;
	line-height: 1.5;
	padding: 0.5rem 1rem;
	overflow: hidden;
`;

const ActionDropdownUser = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 16px;
	padding: 0 0.25rem 1.5rem 0.25rem;
	border-bottom: 1px solid ${colors.userDropdown.menuItemBorderColor};
`;

const Username = styled.div`
	span {
		display: block;
		line-height: 1.6;
	}
`;

const Name = styled.span`
	display: block;
	font-weight: 900;
`;

const Role = styled.span`
	display: block;
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 0.875rem;
	font-weight: 400;
	color: ${colors.userDropdown.roleColor};
`;

const UserAvatar = styled.div`
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
	border: 1px ${colors.avatar.borderColor} solid;
	background-color: ${colors.avatar.backgroundColor};
	padding: 0.25rem;
	border-radius: 60px;
	width: 48px;
	height: 48px;
	margin-right: 16px;

	> span {
		padding-top: 2px;
		font-weight: 900;
	}
`;

const MenuLink = styled(Link)`
	${RegularLink};
`;

const HashMenuLink = styled(HashLink)`
	${RegularLink};

	&:hover:not(:disabled) {
		border-color: transparent;

		&:after {
			opacity: 0;
		}
	}
`;

const AccountMenu = styled.div`
	margin-bottom: 16px;
`;

const AccountMenuList = styled.ul`
	margin: 0 0 16px 0;
	padding: 0 0 16px 0;
	list-style: none;
	border-bottom: 1px solid #e2e2e2;
`;

const AccountMenuListItem = styled.li`
	> a {
		font-size: 1rem;
		display: block;
		padding: 0.5rem;

		&:hover {
			background-color: #f2f2f2;

			&:after {
				border-color: transparent;
			}
		}
	}
`;

const ActionDropdownMenuTopContainer = styled.div`
	position: relative;
`;

const Styled = {
	ActionDropdownWrapper,
	ActionDropdownMenu,
	ActionDropdownMenuItem,
	ActionDropdownUser,
	UserAvatar,
	MenuLink,
	Username,
	Name,
	Role,
	AccountMenu,
	AccountMenuList,
	AccountMenuListItem,
	ActionDropdownMenuTopContainer,
	HashMenuLink
};

export default Styled;
