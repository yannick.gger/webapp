import classNames from 'classnames';
import { useEffect, useRef, useState } from 'react';
import CollabsAuthService from 'services/Authentication/Collabs-api';
import Styled from './UserMenu.style';
import HeaderStyles from '../../Header.style';
import { signOut } from 'services/auth-service';
import { getUserFriendlyRoleName } from 'shared/utils/user';
import { IUserMenuProps } from './types';

const UserMenu = (props: IUserMenuProps) => {
	const { history, client, orgSlug } = props;
	const [userDropdownOpen, setUserDropdownOpen] = useState<boolean>(false);
	const loggedInUser = CollabsAuthService.getCollabsUserObject();
	const ActionDropdownRef = useRef<HTMLDivElement>(null);

	const handleClickOutside = (e: any): void => {
		if (ActionDropdownRef.current && !ActionDropdownRef.current.contains(e.target)) {
			setUserDropdownOpen(false);
		}

		if (e.target instanceof HTMLAnchorElement) {
			setUserDropdownOpen(false);
		}
	};

	const handleEscapeKey = (e: any): void => {
		if (ActionDropdownRef.current && e.key === 'Escape') {
			setUserDropdownOpen(false);
		}
	};

	const getInitals = (name: string) => {
		if (!name) return '';

		if (name.length === 2) {
			return name;
		} else if (name.indexOf(' ') >= 0) {
			return name
				.split(' ')
				.map((word: string) => word[0])
				.join('')
				.slice(0, 2);
		} else {
			return name.charAt(0);
		}
	};

	useEffect(() => {
		document.addEventListener('click', handleClickOutside, true);
		document.addEventListener('keyup', handleEscapeKey, true);

		return () => {
			document.removeEventListener('click', handleClickOutside, true);
			document.removeEventListener('keyup', handleEscapeKey, true);
		};
	}, []);

	const logOut = () => {
		signOut(client, history);
		CollabsAuthService.clearAll();
	};

	return (
		<Styled.ActionDropdownWrapper className={classNames({ show: userDropdownOpen })} ref={ActionDropdownRef}>
			<HeaderStyles.ActionButton onClick={() => setUserDropdownOpen(!userDropdownOpen)} aria-haspopup={true}>
				<span>{getInitals(loggedInUser.name)}</span>
			</HeaderStyles.ActionButton>
			<Styled.ActionDropdownMenu>
				<Styled.ActionDropdownMenuItem>
					<Styled.ActionDropdownUser>
						<Styled.UserAvatar>
							<span>{getInitals(loggedInUser.name)}</span>
						</Styled.UserAvatar>
						<Styled.Username>
							<Styled.Name>{loggedInUser.name}</Styled.Name>
							<Styled.Role>{getUserFriendlyRoleName()}</Styled.Role>
						</Styled.Username>
					</Styled.ActionDropdownUser>
					{loggedInUser.permissions.isInfluencer && (
						<Styled.AccountMenu>
							<Styled.AccountMenuList>
								<Styled.AccountMenuListItem>
									<Styled.HashMenuLink
										scroll={(el: HTMLDivElement) => {
											el.scrollIntoView(true);
											window.scrollBy({
												top: -80,
												left: 0
											});
										}}
										to={`/${orgSlug}/settings/#add-account`}
									>
										Add account
									</Styled.HashMenuLink>
								</Styled.AccountMenuListItem>
							</Styled.AccountMenuList>
						</Styled.AccountMenu>
					)}
				</Styled.ActionDropdownMenuItem>
				<Styled.ActionDropdownMenuItem>
					<Styled.MenuLink to={`/${orgSlug}/settings`}>Account settings</Styled.MenuLink>
				</Styled.ActionDropdownMenuItem>

				<Styled.ActionDropdownMenuItem>
					<Styled.MenuLink to='' onClick={() => logOut()}>
						Sign out
					</Styled.MenuLink>
				</Styled.ActionDropdownMenuItem>
			</Styled.ActionDropdownMenu>
		</Styled.ActionDropdownWrapper>
	);
};

export default UserMenu;
