import { RouteComponentProps } from 'react-router-dom';

export interface IUserMenuProps {
	history: RouteComponentProps['history'];
	client: any;
	orgSlug: string;
}
