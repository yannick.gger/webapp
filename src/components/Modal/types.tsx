import { ReactNode } from 'react';

export interface IModalProps {
	open?: boolean;
	className?: string;
	role?: string;
	size?: 'xs' | 'md' | 'lg' | 'xl';
	handleClose?: any;
	onAfterClose?: any;
	children: ReactNode;
	domNode?: HTMLElement;
}
