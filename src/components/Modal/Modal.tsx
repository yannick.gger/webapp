import { IModalProps } from './types';
import ModalPortal from './ModalPortal';
import { ModalHeader, ModalBody, ModalFooter } from './Modal.style';

/**
 * Modal
 * @param {IModalProps} props
 * @returns {JSX.Element}
 */
const Modal = (props: IModalProps) => {
	return (
		<ModalPortal
			open={props.open}
			handleClose={props.handleClose}
			size={props.size}
			className={props.className}
			role={props.role}
			onAfterClose={props.onAfterClose}
			domNode={props.domNode}
		>
			{props.children}
		</ModalPortal>
	);
};

Modal.Header = ModalHeader;
Modal.Body = ModalBody;
Modal.Footer = ModalFooter;

export default Modal;
