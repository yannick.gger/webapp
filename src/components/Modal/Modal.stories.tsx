import { storiesOf } from '@storybook/react';
import Modal from './Modal';

storiesOf('Modal', module)
	.add('Default', () => {
		return (
			<>
				<Modal open={true} size={'md'}>
					<Modal.Header>
						<h3>Heading</h3>
					</Modal.Header>
					<Modal.Body>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed mi luctus, mollis metus tincidunt, mollis tellus. Nulla vitae purus quis lectus
							faucibus pretium. Nullam bibendum nisi erat, eu pharetra tellus consequat vitae. Praesent vestibulum fermentum eros eget pretium. Praesent erat
							diam, pellentesque eget vehicula in, pretium ac nibh. Integer volutpat felis et sagittis rhoncus. Integer a justo id nisl tristique pretium non
							sed turpis.
						</p>
					</Modal.Body>
				</Modal>
			</>
		);
	})
	.add('No heading', () => {
		return (
			<>
				<Modal open={true} size={'md'}>
					<Modal.Body>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed mi luctus, mollis metus tincidunt, mollis tellus. Nulla vitae purus quis lectus
							faucibus pretium. Nullam bibendum nisi erat, eu pharetra tellus consequat vitae. Praesent vestibulum fermentum eros eget pretium. Praesent erat
							diam, pellentesque eget vehicula in, pretium ac nibh. Integer volutpat felis et sagittis rhoncus. Integer a justo id nisl tristique pretium non
							sed turpis.
						</p>
					</Modal.Body>
				</Modal>
			</>
		);
	});
