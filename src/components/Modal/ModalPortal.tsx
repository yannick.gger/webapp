import { useEffect, useRef } from 'react';
import { Styled } from './Modal.style';
import ReactDOM from 'react-dom';
import { IModalProps } from './types';
import { targetIsScrollbar } from './utils';

const ModalPortal = (props: IModalProps) => {
	const modalDialogRef = useRef<HTMLDivElement>(null);
	const OPEN_CSS_CLASS = 'show';

	const HandleClose = () => {
		if (props.handleClose) {
			props.handleClose();
		}

		if (props.onAfterClose) {
			props.onAfterClose();
		}
	};

	const HandleKeyUp = (event: any) => {
		switch (event.key) {
			case 'Escape':
				HandleClose();
				break;
			default:
				break;
		}
	};

	const isMultipleModalsOpen = () => {
		if (document) {
			const allModals = document.querySelectorAll('.modal.show');
			const nrOfOpenModals = allModals && allModals.length;
			return !!(nrOfOpenModals > 1);
		}
		return false;
	};

	const HandleClickOutside = (event: any) => {
		if (targetIsScrollbar(event) || isMultipleModalsOpen()) {
			return;
		}
		if (modalDialogRef.current && !modalDialogRef.current.contains(event.target)) {
			HandleClose();
		}
	};

	useEffect(() => {
		if (!props.open) return;

		document.addEventListener('keyup', HandleKeyUp);
		document.addEventListener('mousedown', HandleClickOutside);

		return () => {
			if (!props.open) return;
			document.removeEventListener('keyup', HandleKeyUp);
			document.removeEventListener('mousedown', HandleClickOutside);
		};
	}, [props, props.open]);

	useEffect(() => {
		if (props.open) {
			document.body.classList.add('disable-scroll');
			modalDialogRef.current && modalDialogRef.current.focus();
		} else {
			document.body.classList.remove('disable-scroll');
		}
	}, [props.open]);

	function RenderModal() {
		return (
			<Styled.Modal
				tabIndex={-1}
				role={props.role}
				className={`modal ${props.className || ''} ${props.open ? OPEN_CSS_CLASS : ''}`}
				aria-hidden={!props.open}
				size={props.size || 'md'}
			>
				<Styled.ModalDialog role='document' ref={modalDialogRef} size={props.size || 'md'} tabIndex={props.open ? 0 : -1}>
					<Styled.ModalContent>{props.children}</Styled.ModalContent>
				</Styled.ModalDialog>
			</Styled.Modal>
		);
	}

	if (typeof document === 'undefined') {
		return <></>;
	}

	return ReactDOM.createPortal(RenderModal(), props.domNode || document.body);
};

ModalPortal.defaultProps = {
	shouldCloseOnEsc: true,
	shouldCloseOnOverlayClick: true,
	role: 'dialog'
};

export default ModalPortal;
