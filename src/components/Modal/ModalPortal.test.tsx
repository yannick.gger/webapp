import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import { collabs } from 'styles/themes';
import ModalPortal from './ModalPortal';

describe('<ModalPortal /> test', () => {
	it('renders the component', () => {
		const modalportal = render(
			<ThemeProvider theme={collabs}>
				<ModalPortal open={true}>
					<span>Hello</span>
				</ModalPortal>
			</ThemeProvider>
		);
		expect(modalportal).toMatchSnapshot();
	});
});
