import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import { collabs } from 'styles/themes';
import Modal from './Modal';

describe('<Modal /> test', () => {
	it('renders the component', () => {
		const modal = render(
			<ThemeProvider theme={collabs}>
				<Modal open={true}>
					<span>Hello</span>
				</Modal>
			</ThemeProvider>
		);
		expect(modal).toMatchSnapshot();
	});
});
