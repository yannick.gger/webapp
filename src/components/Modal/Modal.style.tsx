import styled from 'styled-components';
import { breakpoints } from 'styles/variables/media-queries';
import { Flex, Spacing } from 'styles/utils/UtilityClasses';

interface IModalDialog {
	size: string;
}

const GetModalDialogWidth = (size: string) => {
	switch (size) {
		case 'xs':
			return '36rem';
		case 'md':
			return '59.0625rem';
		case 'lg':
			return '59.0625rem';
		case 'xl':
			return '59.0625rem';
		default:
			return '59.0625rem';
	}
};

const ModalDialog = styled.div<IModalDialog>`
	position: relative;
	width: auto;
	max-width: 100%;
	margin: 85px 16px;
	z-index: 3099;
	transition: transform 0.3s ease-out;
	transform: translate(0, -30%);
	outline: none;

	@media (min-width: ${breakpoints.lg}) {
		margin: 4.5rem auto;

		max-width: ${({ size }) => {
			return GetModalDialogWidth(size);
		}};
	}
`;

const Modal = styled.div<IModalDialog>`
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	overflow-x: hidden;
	overflow-y: auto;
	z-index: 2098;
	display: block;
	opacity: 0;
	visibility: hidden;
	pointer-events: none;
	transition: opacity 0.15s linear;

	@media (min-width: ${breakpoints.lg}) {
		padding-right: 18.9884px;
	}

	&.show {
		opacity: 1;
		visibility: visible;
		pointer-events: auto;
		overflow: hidden;

		${ModalDialog} {
			transform: translate(0, 0);
		}
	}
`;

export const ModalHeader = styled.div`
	padding: 1rem 2.5rem 0;
	margin-bottom: 1.25rem;

	h1,
	h2,
	h3,
	h4,
	h5 {
		margin: 0;
	}
`;

export const ModalBody = styled.div`
	padding: 0 2.5rem;
	margin-bottom: 1.25rem;
	transform: translateZ(-0.125rem);
	overflow-y: scroll;
`;

export const ModalFooter = styled.div`
	${Flex};
	${Spacing};
	margin-bottom: 0.625rem;
	padding-bottom: 1rem;
`;

const ModalContent = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	width: 100%;
	pointer-events: auto;
	background-color: ${({ theme }) => theme.modal.content.background};
	background-clip: padding-box;
	border-radius: ${({ theme }) => theme.modal.borderRadius};
	border: ${({ theme }) => theme.modal.border};
	box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.16);
	outline: 0;
	z-index: 15;
`;

export const Styled = {
	Modal,
	ModalDialog,
	ModalContent
};
