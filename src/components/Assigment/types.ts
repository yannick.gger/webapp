export enum AssignmentType {
	ALL = 'all',
	INSTAGRAM_POST = 'instagram_post',
	INSTAGRAM_STORY = 'instagram_story',
	INSTAGRAM_REEL = 'instagram_reel',
	TIKTOK_VIDEO = 'tiktok_video'
}
