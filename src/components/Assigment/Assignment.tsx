import { AssignmentType } from './types';
import Icons from '../../views/Campaign/Dashboard/icons';

export function assignmentTypeToIcon(type: AssignmentType): JSX.Element {
	switch (type) {
		case AssignmentType.INSTAGRAM_POST:
			return <img src={Icons.AssignmentTypeInstagramPost} alt='Instagram post' />;
		case AssignmentType.INSTAGRAM_STORY:
			return <img src={Icons.AssignmentTypeInstagramStory} alt='Instagram Story' />;
		case AssignmentType.INSTAGRAM_REEL:
			return <img src={Icons.AssignmentTypeInstagramReel} alt='Instagram Reel' />;
		case AssignmentType.TIKTOK_VIDEO:
			return <img src={Icons.AssignmentTypeTiktok} alt='Tiktok video' />;
		default:
			return <img src={Icons.List} alt='Task' />;
	}
}

export function assignmentTypeToText(type: AssignmentType): string {
	switch (type) {
		case AssignmentType.INSTAGRAM_POST:
			return 'Instagram post';
		case AssignmentType.INSTAGRAM_STORY:
			return 'Instagram Story';
		case AssignmentType.INSTAGRAM_REEL:
			return 'Instagram Reel';
		case AssignmentType.TIKTOK_VIDEO:
			return 'Tiktok video';
		default:
			return '';
	}
}
