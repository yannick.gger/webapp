import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Field, Form, Formik } from 'formik';
import { useState } from 'react';
import Checkbox from './Elements/Checkbox/Checkbox';
import Select from './Elements/Select/Select';
import InputText from './Elements/Text';
import { IFormInputTextProps } from './Elements/Text/Text';
import Textarea from './Elements/Textarea';

export default {
	title: 'Form Elements',
	component: InputText
} as ComponentMeta<typeof InputText>;

const InputTemplate: ComponentStory<typeof InputText> = (args: IFormInputTextProps) => (
	<>
		<div style={{ maxWidth: 1200, margin: 'auto', padding: '1rem 2rem' }}>
			<h4>Input fields</h4>
			<Formik
				initialValues={{ fullName: '' }}
				onSubmit={(values, actions) => {
					actions.setSubmitting(false);
				}}
			>
				<Form name='mock' method='post' onSubmit={() => {}}>
					<div style={{ marginBottom: '1rem' }}>
						<h4>Default input field</h4>
						<InputText type='text' name='field1' placeholder='Placeholder text' size='md' />
					</div>
					<div style={{ marginBottom: '1rem' }}>
						<h4>Default input field with icon</h4>
						<InputText type='text' name='field2' placeholder='Placeholder text' size='md' icon='calendar' />
						<div style={{ marginBottom: '0.5rem' }} />
						<InputText type='text' name='field3' placeholder='Placeholder text' size='md' icon='calendar' iconPosition='right' />
					</div>
					<div style={{ marginBottom: '1rem' }}>
						<h4>Disabled input field</h4>
						<InputText type='text' name='field4' placeholder='Placeholder text' size='md' disabled />
					</div>
					<div style={{ marginBottom: '1rem' }}>
						<h4>Large input field</h4>
						<InputText type='text' name='field5' placeholder='Placeholder text' size='lg' />
					</div>
					<div style={{ marginBottom: '1rem' }}>
						<h4>Large input field with tooltip</h4>
						<InputText type='text' name='field6' placeholder='Placeholder text' size='lg' toolTip={true} onClickTooltip={() => alert('clicked')} />
					</div>
					<div style={{ marginBottom: '1rem' }}>
						<h4>Large input field with icon</h4>
						<InputText type='text' name='field6' placeholder='Placeholder text' size='lg' icon='calendar' />
					</div>
				</Form>
			</Formik>
		</div>
	</>
);

const TextareaTemplate: ComponentStory<typeof Textarea> = (args: IFormInputTextProps) => (
	<>
		<div style={{ maxWidth: 1200, margin: 'auto', padding: '1rem 2rem' }}>
			<h4>Input fields</h4>
			<Formik initialValues={{ fullName: '' }} onSubmit={() => {}}>
				<Form name='mock' method='post'>
					<Textarea type='text' name='field1' placeholder='Placeholder text' heading='Default textarea width value' value='Lorem ipsum' />
					<Textarea type='text' name='field2' placeholder='Placeholder text' heading='Default textarea without value' />
					<Textarea type='text' name='field3' placeholder='Placeholder text' heading='Default textarea disabled' disabled />
					<Textarea
						type='text'
						name='field3'
						placeholder='Placeholder text'
						heading='Default textarea with tooltip'
						toolTip={true}
						onClickTooltip={() => {
							alert('Tooltip clicked!');
						}}
					/>
				</Form>
			</Formik>
		</div>
	</>
);

const SelectTemplate: ComponentStory<typeof Select> = (args: any) => (
	<>
		<div style={{ maxWidth: 1200, margin: 'auto', padding: '1rem 2rem' }}>
			<h4>Selects</h4>
			<Formik initialValues={{ fullName: '' }} onSubmit={() => {}}>
				<Form name='mock' method='post'>
					<div style={{ marginBottom: '1rem' }}>
						<h5>Default</h5>
						<Field
							name={'example'}
							component={Select}
							options={[
								{
									label: 'Option 1',
									value: 'Option 1'
								},
								{
									label: 'Option 2',
									value: 'Option 2'
								},
								{
									label: 'Option 3',
									value: 'Option 3'
								},
								{
									label: 'Option 4',
									value: 'Option 4'
								}
							]}
						/>
					</div>
					<div style={{ marginBottom: '1rem' }}>
						<h5>With groups</h5>
						<Field
							name={'example'}
							component={Select}
							isSearchable={true}
							options={[
								{
									label: 'Group 1',
									options: [
										{
											label: 'Option 1',
											value: 'Option 1'
										},
										{
											label: 'Option 2',
											value: 'Option 2'
										},
										{
											label: 'Option 3',
											value: 'Option 3'
										},
										{
											label: 'Option 4',
											value: 'Option 4'
										}
									]
								},
								{
									label: 'Group 2',
									options: [
										{
											label: 'Option 1',
											value: 'Option 1'
										},
										{
											label: 'Option 2',
											value: 'Option 2'
										},
										{
											label: 'Option 3',
											value: 'Option 3'
										},
										{
											label: 'Option 4',
											value: 'Option 4'
										}
									]
								}
							]}
						/>
					</div>
				</Form>
			</Formik>
		</div>
	</>
);

const CheckboxTemplate: ComponentStory<typeof Checkbox> = () => {
	const [checked, isChecked] = useState(false);

	return (
		<div style={{ maxWidth: 1200, margin: 'auto', padding: '1rem 2rem' }}>
			<h4>Input fields</h4>
			<Formik initialValues={{ fullName: '' }} onSubmit={() => {}}>
				<Form name='mock' method='post'>
					<Field type='checkbox' name='check' id='check' label='Checkbox' component={Checkbox} checked={checked} onChange={() => isChecked(!checked)} />
					<Field type='checkbox' name='check' id='check' label='Checked Checkbox' checked={true} component={Checkbox} />
					<Field type='checkbox' name='check' id='check' label='Disabled Checkbox' disabled={true} component={Checkbox} />
				</Form>
			</Formik>
		</div>
	);
};

export const InputField = InputTemplate.bind({});
export const TextareaField = TextareaTemplate.bind({});
export const SelectField = SelectTemplate.bind({});
export const CheckboxElement = CheckboxTemplate.bind({});
