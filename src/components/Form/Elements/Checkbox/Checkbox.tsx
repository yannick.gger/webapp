import Checkbox from 'components/Checkbox';

export interface ICheckboxProps {
	label?: string;
	id?: string;
	name?: string;
	disabled?: boolean;
	onChange?: any;
	className?: string;
	checked?: boolean;
}

const CheckboxComponent = (props: ICheckboxProps) => {
	return <Checkbox {...props} />;
};

export default CheckboxComponent;
