import React, { useState } from 'react';
import TogglePanel from 'components/TogglePanel';

import Styled from './BulletList.style';

type Props = {
	label: React.ReactNode;
	name: string;
	value: string;
	onChange: (newValue: string) => void;
};
/**
 * @todo i18next
 */
const BulletList = ({ label, name, value, onChange }: Props) => {
	const [opened, setOpened] = useState(value.length > 0);
	const items = value.split(/\n/);
	const itemsCount = items.length;

	return (
		<TogglePanel heading={label} isOpen={opened} onClick={() => setOpened((prev) => !prev)}>
			<Styled.List>
				{items.map((item, i) => (
					<Styled.Item size='md' key={i}>
						<input
							autoComplete='off'
							type='text'
							value={item}
							onChange={({ target }) => {
								const updated = [...items];
								updated[i] = target.value.trimStart();
								onChange(updated.join(`\n`));
							}}
							onKeyDown={(e) => {
								if (e.code === 'Enter') {
									e.preventDefault();
									onChange(`${value}\n `);
								}
							}}
							autoFocus={'' === item.trim()}
							name={`${name}[${i}]`}
							data-testid={`form-control-${name}[${i}]`}
						/>
						{i === itemsCount - 1 && <code title='Press "enter" to submit'>[enter]</code>}
						{itemsCount > 1 && (
							<Styled.DeleteButton
								onClick={() => {
									const updated = [...items];
									delete updated[i];
									onChange(updated.filter((val) => '' !== val.trim()).join(`\n`));
								}}
							>
								&times;
							</Styled.DeleteButton>
						)}
					</Styled.Item>
				))}
			</Styled.List>
		</TogglePanel>
	);
};
export default BulletList;
