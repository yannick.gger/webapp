import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';

import { InputField } from 'styles/formElements/input';

const Item = styled.li`
	display: flex;
	justify-content: space-between;
	align-items: center;

	& > input {
		${InputField};
		border: none;
		background-color: ${colors.transparent};

		&:hover:not(:disabled),
		&:focus:not(:disabled) {
			border: none;
			box-shadow: none;
		}
	}

	&::before {
		content: '•';
	}
`;

const List = styled.ul`
	padding: ${guttersWithRem.xxs};
`;

const DeleteButton = styled.button`
	border: none;
	background-color: ${colors.transparent};
`;

export default {
	List,
	Item,
	DeleteButton
};
