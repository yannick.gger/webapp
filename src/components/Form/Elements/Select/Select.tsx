import SelectMenu from 'components/SelectMenu';
import { FieldAttributes, useField } from 'formik';

export type SelectOption = {
	label: string;
	value: string;
};

const Select = (
	props: FieldAttributes<any> & { label: string; options: SelectOption[]; isSearchable: boolean; onChangeCallable?: () => void; size: 'md' | 'lg' }
) => {
	const { options, isSearchable, id, name } = props;
	const [field, meta, { setValue }] = useField({ name, id });

	const getValue = (options: Array<{ value: string; label: string; options?: SelectOption[] }>) => {
		if (options) {
			// Ignore group top level.
			return options.find((option) => option.options === undefined && option.value === field.value);
		}
	};

	return (
		<SelectMenu
			{...props}
			isSearchable={isSearchable !== undefined ? isSearchable : false}
			options={options}
			value={getValue(options) as any}
			onChange={({ value, label }: SelectOption) => {
				setValue(value);
				if (props.onChangeCallable) {
					props.onChangeCallable(value, label);
				}
			}}
		/>
	);
};

export default Select;
