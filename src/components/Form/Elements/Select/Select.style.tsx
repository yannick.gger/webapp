import styled from 'styled-components';
import { InputField } from 'styles/formElements/input';

const Wrappper = styled.div`
	${InputField};
`;

const Styled = {
	Wrappper,
};

export default Styled;
