import { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import Styled from './Textarea.style';
import { IFormTextareaProps } from './types';
import TogglePanel from 'components/TogglePanel';
import { useField } from 'formik';

/**
 * Textarea for Formik
 * @param {IFormTextareaProps} props
 * @todo type event params
 * @returns {JSX.Element}
 */
const Textarea = (props: IFormTextareaProps): JSX.Element => {
	const textareaRef = useRef<HTMLTextAreaElement>(null);
	const { className, toolTip, onClickTooltip, heading, disabled, id, name } = props;
	const [field, meta] = useField({ name, id });
	const [active, setActive] = useState<boolean>(false);

	const calcHeight = (value: string): number => {
		let numberOfLineBreaks = (value.match(/\n/g) || []).length;
		// min-height + lines x line-height + padding
		let newHeight = 48 + numberOfLineBreaks * 24 + 8;
		return newHeight;
	};

	const setHeight = (): void => {
		if (textareaRef && textareaRef.current) {
			const textarea = textareaRef.current;
			textarea.style.height = `${calcHeight(textarea.value)}px`;
			textarea.focus();
		}
	};

	const removeHeight = (): void => {
		if (textareaRef && textareaRef.current) {
			const textarea = textareaRef.current;
			textarea.style.height = '24px';
			textarea.blur();
		}
	};

	useEffect(() => {
		active ? setHeight() : removeHeight();
	}, [active]);

	useEffect(() => {
		if (textareaRef && textareaRef.current) {
			const textarea = textareaRef.current;
			setActive(textarea.value !== '');
		}
	}, []);

	return (
		<TogglePanel heading={heading} toolTip={toolTip} onClickTooltip={onClickTooltip} isOpen={active || !!field.value} onClick={() => setActive(!active)} disabled={disabled}>
			<Styled.TextareaContainer>
				<Styled.Textarea
					{...field}
					{...props}
					className={classNames('textarea__field', className)}
					as='textarea'
					ref={textareaRef}
					onKeyDown={setHeight}
					disabled={disabled}
					// eslint-disable-next-line prettier/prettier
					data-testid={`form-control-${id ?? name}`}
					onFocus={() => setActive(true)}
				/>
				{meta.error && <Styled.ErrorMessage>{meta.error}</Styled.ErrorMessage>}
			</Styled.TextareaContainer>
		</TogglePanel>
	);
};

export default Textarea;
