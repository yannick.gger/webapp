import { FieldAttributes } from 'formik';
import { ReactNode } from 'react';

export interface IFormTextareaProps extends FieldAttributes<any> {
	heading: ReactNode;
	toolTip?: boolean;
	onClickTooltip?: () => void;
}
