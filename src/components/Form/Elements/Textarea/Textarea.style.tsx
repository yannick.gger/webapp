import styled from 'styled-components';
import { TextareaField, TextareaFieldContainer, InputFieldContainer } from 'styles/formElements/textarea';
import colors from 'styles/variables/colors';
import { Field } from 'formik';

export type InputType = {
	disabled?: boolean;
};

const Textarea = styled(Field)<InputType>`
	${TextareaField};
`;

const TextareaContainer = styled.div`
	${TextareaFieldContainer};
`;

const InputContainer = styled.div<InputType>`
	${InputFieldContainer};
`;

const ErrorMessage = styled.p`
	font-size: 0.75rem;
	color: ${colors.error};
`;

const Styled = {
	Textarea,
	InputContainer,
	TextareaContainer,
	ErrorMessage
};

export default Styled;
