import styled from 'styled-components';
import { InputField, InputFieldContainer, InputFieldTooltip, InputFieldIcon } from 'styles/formElements/input';
import colors from 'styles/variables/colors';
import { Field } from 'formik';
import { InputType } from 'styles/formElements/types';

const Input = styled(Field)<InputType>`
	${InputField};
`;

const InputContainer = styled.div<InputType>`
	${InputFieldContainer};
`;

const Tooltip = styled.div`
	${InputFieldTooltip};
`;

const Icon = styled.div<InputType>`
	${InputFieldIcon};
`;

const ErrorMessage = styled.p`
	font-size: 0.75rem;
	color: ${colors.error} !important;
	padding-top: 0.25rem;
`;

const Styled = {
	Input,
	InputContainer,
	Tooltip,
	Icon,
	ErrorMessage
};

export default Styled;
