import Icon from 'components/Icon';
import { FieldAttributes, useField } from 'formik';
import Styled from './Input.style';

export interface IFormInputTextProps extends FieldAttributes<any> {
	size: 'md' | 'lg';
	toolTip?: boolean;
	icon?: string;
	iconPosition?: 'left' | 'right';
	onClickTooltip?: () => void;
	autocomplete?: 'on' | 'off';
}

/**
 * Input field for Formik
 * @param {IFormInputTextProps} props
 * @returns {JSX.Element}
 */
const InputText = (props: IFormInputTextProps): JSX.Element => {
	const { className, size, toolTip, icon, iconPosition, onClickTooltip, name, id, autocomplete } = props;
	const [field, meta] = useField({ name, id });
	return (
		<Styled.InputContainer group={icon !== undefined} iconPosition={iconPosition || 'left'} size={size}>
			{icon && (
				<Styled.Icon size={size} iconPosition={iconPosition || 'left'}>
					<Icon icon={icon} size={size === 'lg' ? '32' : '24'} />
				</Styled.Icon>
			)}
			<Styled.Input className={className} {...field} {...props} size={size} />
			{toolTip && (
				<Styled.Tooltip onClick={() => onClickTooltip && onClickTooltip()}>
					<Icon icon='tooltip' className='field-tooltip' />
				</Styled.Tooltip>
			)}
			{meta.error && <Styled.ErrorMessage>{meta.error}</Styled.ErrorMessage>}
		</Styled.InputContainer>
	);
};

InputText.Defaultprops = {
	size: 'md'
};

export default InputText;
