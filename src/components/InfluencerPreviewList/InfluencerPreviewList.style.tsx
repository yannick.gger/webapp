import styled from 'styled-components';

const List = styled.ul`
	list-style: none;
`;

const Item = styled.li`
	display: inline-block;
	margin-left: -0.5rem;
	& img {
	}

	.remaining {
		margin-left: 0.5rem;
	}
`;

const RoundedImage = styled.img`
	border-radius: 100%;
	border: 1px solid #fff;
`;

export default {
	List,
	Item,
	RoundedImage
};
