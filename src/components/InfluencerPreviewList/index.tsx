import { Model } from 'json-api-models';
import { uniqBy } from 'lodash';
import Styled from './InfluencerPreviewList.style';

type Props = {
	groups: Model[];
};

const InfluencerPreviewList = ({ groups }: Props) => {
	let influencers: Model[] = [];
	for (const group of groups) {
		if (group.type === 'influencer') {
			influencers.push(group);
			continue;
		}

		if (group.type !== 'assignmentGroups') {
			throw new Error(`I only support "assignmentGroup" type, not "${group.type}", sorry mate.`);
		}

		for (const { influencer } of group.campaignInstagramOwnerAssignments || []) {
			if (influencer && influencer.username) {
				influencers.push(influencer);
			}
		}
	}
	influencers = uniqBy(influencers, 'id');
	const remaining = influencers.length - 5;

	if (-5 === remaining) {
		return null;
	}

	return (
		<Styled.List>
			{influencers.slice(0, 5).map(({ id, links, username }: Model) => (
				<Styled.Item key={id}>
					<Styled.RoundedImage src={links.profilePictureUrl} alt={username} height={30} width={30} />
				</Styled.Item>
			))}
			{remaining > 0 && (
				<Styled.Item>
					<span className='remaining'>+{remaining}</span>
				</Styled.Item>
			)}
		</Styled.List>
	);
};

export default InfluencerPreviewList;
