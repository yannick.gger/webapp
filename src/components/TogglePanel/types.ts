export interface ITogglePanel {
	children?: React.ReactNode;
	heading?: React.ReactNode;
	isOpen: boolean;
	onClick?: () => void;
	disabled?: boolean;
	toolTip?: boolean;
	onClickTooltip?: () => void;
}
