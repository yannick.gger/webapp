import { useState, useEffect } from 'react';
import classNames from 'classnames';
import Styled from './TogglePanel.style';
import { ITogglePanel } from './types';
import Icon from 'components/Icon';

/**
 * TogglePanel
 * @param {ITogglePanel} props
 * @returns {JSX.Element}
 */
const TogglePanel = (props: ITogglePanel): JSX.Element => {
	const [active, setActive] = useState<boolean>(false);

	useEffect(() => {
		setActive(props.isOpen);
	}, []);

	useEffect(() => {
		!props.disabled && setActive(props.isOpen);
	}, [props.isOpen]);

	const handleOnClick = (): Boolean | void => {
		if (props.disabled) return false;

		props.onClick && props.onClick();
		setActive(!active);
	};

	const handleTooltipClick = () => {
		props.onClickTooltip && props.onClickTooltip();
	};

	return (
		<Styled.Wrapper className={classNames({ selected: active })} aria-expanded={active}>
			<Styled.HeadingContainer onClick={() => handleOnClick()}>
				<Styled.Heading isActive={active}>{props.heading}</Styled.Heading>
				{props.toolTip && (
					<Styled.Tooltip onClick={() => handleTooltipClick()}>
						<Icon icon='tooltip' className='field-tooltip' />
					</Styled.Tooltip>
				)}
			</Styled.HeadingContainer>
			<Styled.ComponentContainer>{props.children}</Styled.ComponentContainer>
		</Styled.Wrapper>
	);
};

export default TogglePanel;
