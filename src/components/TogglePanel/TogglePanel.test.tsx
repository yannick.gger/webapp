import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import TogglePanel from './TogglePanel';
import themes from 'styles/themes'

describe('<TogglePanel /> test', () => {
	it('renders togglePanel', () => {
		const togglePanel = render(
			<ThemeProvider theme={themes['default']}>
				<TogglePanel heading='Mocked heading' isOpen={false} />
			</ThemeProvider>
		);
		expect(togglePanel).toMatchSnapshot();
	});
});
