import React, { Component } from 'react';
import dotProp from 'dot-prop-immutable';
import { Table, Button, Empty } from 'antd';
import TableErrorImg from 'assets/img/app/table-error.svg';
/* Example usage

import PaginatedTable from "components/paginated-table"

<Query query={getAllOrganizationsQuery} notifyOnNetworkStatusChange>
  {queryRes => <PaginatedTable
    queryRes={queryRes}
    columns={[
      {
        title: "Name",
        dataIndex: "node.name"
      }]}
    dataRef="organization.campaigns" // The location in the graphql query for the list
  />}
</Query>

*/

class PaginatedTable extends Component {
	state = {
		initialLoading: false,
		moreLoading: false
	};
	loadMore = (data, fetchMore) => {
		this.setState({ moreLoading: true });
		fetchMore({
			variables: {
				cursor: dotProp.get(data, `${this.props.dataRef}.pageInfo.endCursor`)
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				this.setState({ moreLoading: false });

				const newEdges = dotProp.get(fetchMoreResult, `${this.props.dataRef}.edges`);
				const pageInfo = dotProp.get(fetchMoreResult, `${this.props.dataRef}.pageInfo`);

				return newEdges.length
					? dotProp.set(previousResult, this.props.dataRef, {
							__typename: dotProp.get(previousResult, `${this.props.dataRef}.__typename`),
							edges: [...dotProp.get(previousResult, `${this.props.dataRef}.edges`), ...newEdges],
							pageInfo
					  })
					: previousResult;
			}
		});
	};
	componentDidUpdate = (prevProps) => {
		if (this.props.queryRes && prevProps.queryRes && prevProps.queryRes.loading !== this.props.queryRes.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.queryRes.loading });
		}
	};
	componentDidMount = () => {
		if (this.props.queryRes && this.props.queryRes.loading) {
			this.setState({ initialLoading: true });
		}
	};
	render() {
		const { queryRes, columns, emptyState } = this.props;
		const { error, data, fetchMore } = queryRes;
		const { initialLoading, moreLoading } = this.state;
		const listData = dotProp.get(data, `${this.props.dataRef}`);

		return (
			<div>
				<Table
					className='pb-30'
					columns={columns}
					dataSource={data && listData ? listData.edges : []}
					rowKey={(record) => record.node.id}
					pagination={false}
					loading={initialLoading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load data' />
						) : (
							<Empty description={emptyState || 'Nothing matching the search criterias found'} />
						)
					}}
				/>

				{!error && !initialLoading && data && listData && listData.pageInfo.hasNextPage && (
					<div className='pb-30 text-center'>
						<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(data, fetchMore)}>
							Load more
						</Button>
					</div>
				)}
			</div>
		);
	}
}

export default PaginatedTable;
