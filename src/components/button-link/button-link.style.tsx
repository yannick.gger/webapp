import styled from 'styled-components';

/**
 * Styles for link buttons
 */
const LinkButton = styled.button`
	cursor: pointer;
	display: inline;
	font: inherit;
	perspective-origin: 0 0;
	text-align: start;
	transform-origin: 0 0;
	width: auto;
	border: none;
	background: inherit;

	@supports (-moz-appearance: none) {
		/* Mozilla-only */
		&::-moz-focus-inner {
			/* reset any predefined properties */
			border: none;
			padding: 0;
		}
		&:focus {
			/* add outline to focus pseudo-class */
			outline-style: dotted;
			outline-width: 1px;
		}
	}

	&.link-styling {
		align-items: normal;
		background-color: none;
		border-color: none;
		border-style: none;
		box-sizing: content-box;
		background-color: rgba(0, 0, 0, 0);
		border-color: rgb(0, 0, 238);
		color: rgb(0, 0, 238);
		-moz-appearance: none;
		-webkit-logical-height: 1em; /* Chrome ignores auto, so we have to use this hack to set the correct height  */
		-webkit-logical-width: auto; /* Chrome ignores auto, but here for completeness */
	}
`;

export const Styled = {
	LinkButton
};
