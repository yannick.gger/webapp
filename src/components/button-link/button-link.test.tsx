import { render } from '@testing-library/react';
import ButtonLink from './index';

test('Button component should load', () => {
	const button = render(<ButtonLink>Test</ButtonLink>);
	expect(button).toMatchSnapshot();
});
