import React from 'react';
import { Styled } from './button-link.style';

/**
 * ButtonLink
 *
 * Component to render a styled link button
 *
 * @param props
 * @returns JSX.Element
 */
const ButtonLink = (props: any) => {
	return <Styled.LinkButton {...props} className={['link-button', props.className].join(' ')} />;
};

export default ButtonLink;
