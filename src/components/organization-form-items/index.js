import React from 'react';
import { Form, Input, Select, Icon, Col, Row, Tooltip } from 'antd';
import { countries } from 'country-data';
import { checkGhostUserIsSaler } from 'services/auth-service';

const FormItem = Form.Item;

class OrganizationFormItems extends React.Component {
	render() {
		const { form, readOnly, initialValues = {} } = this.props;
		const { getFieldDecorator } = form;

		return (
			<React.Fragment>
				<FormItem label='Organization Name'>
					{getFieldDecorator('organization.name', {
						initialValue: initialValues.organizationName,
						rules: [
							{
								required: true,
								message: 'Please enter an organization name'
							}
						]
					})(
						<Input
							size='large'
							prefix={<Icon type='solution' style={{ color: 'rgba(0,0,0,.25)' }} />}
							placeholder='Organization name'
							disabled={checkGhostUserIsSaler() || readOnly}
						/>
					)}
				</FormItem>

				<FormItem label='Org. number'>
					{getFieldDecorator('organization.organizationNumber', {
						placeholder: '552121-1111',
						rules: [
							{
								required: true,
								message: 'Please enter an organization number'
							},
							{
								transform: (value) => value && value.trim()
							},
							{
								validator: (rule, value, callback) => {
									if (form.getFieldValue('organization.country') === 'SE' && value && value.match(/^[0-9]{6}-[0-9]{4}$/) === null) {
										callback('Enter your swedish organization number as XXXXXX-XXXX');
									} else {
										callback();
									}
								},
								message: 'Enter your swedish organization number as XXXXXX-XXXX'
							}
						]
					})(
						<Input
							size='large'
							prefix={<Icon type='idcard' style={{ color: 'rgba(0,0,0,.25)' }} />}
							placeholder='Org. number'
							onBlur={() => {
								if (form.getFieldValue('organization.country') !== 'SE' || form.getFieldValue('organization.organizationNumber') === undefined) {
									return;
								}
								form.setFieldsValue({
									'organization.vatNumber': `SE${form.getFieldValue('organization.organizationNumber').replace(/\D/g, '')}01`
								});
							}}
							disabled={checkGhostUserIsSaler() || readOnly}
						/>
					)}
				</FormItem>

				<FormItem
					label={
						<span>
							VAT number for european union{' '}
							<Tooltip title='In Sweden this is SE followed by your organization number (without dash) followed by 01. Only valid if you are registered for VAT.'>
								<Icon type='question-circle-o' />
							</Tooltip>
						</span>
					}
				>
					{getFieldDecorator('organization.vatNumber', {
						rules: [
							{
								required: [
									'AT',
									'BE',
									'BG',
									'HR',
									'CY',
									'CZ',
									'DK',
									'EE',
									'FI',
									'FR',
									'DE',
									'EL',
									'HU',
									'IE',
									'IT',
									'LV',
									'LI',
									'LU',
									'MT',
									'NL',
									'PL',
									'PT',
									'RO',
									'SK',
									'SI',
									'ES',
									'SE'
								].includes(form.getFieldValue('organization.country')),
								message: 'You need a company with VAT registration and you need to enter a correct VAT number.'
							},
							{
								transform: (value) => value && value.trim()
							},
							{
								pattern: /^(ATU[0-9]{8}|BE[01][0-9]{9}|BG[0-9]{9,10}|HR[0-9]{11}|CY[A-Z0-9]{9}|CZ[0-9]{8,10}|DK[0-9]{8}|EE[0-9]{9}|FI[0-9]{8}|FR[0-9A-Z]{2}[0-9]{9}|DE[0-9]{9}|EL[0-9]{9}|HU[0-9]{8}|IE([0-9]{7}[A-Z]{1,2}|[0-9][A-Z][0-9]{5}[A-Z])|IT[0-9]{11}|LV[0-9]{11}|LT([0-9]{9}|[0-9]{12})|LU[0-9]{8}|MT[0-9]{8}|NL[0-9]{9}B[0-9]{2}|PL[0-9]{10}|PT[0-9]{9}|RO[0-9]{2,10}|SK[0-9]{10}|SI[0-9]{8}|ES[A-Z]([0-9]{8}|[0-9]{7}[A-Z])|SE[0-9]{12}|GB([0-9]{9}|[0-9]{12}|GD[0-4][0-9]{2}|HA[5-9][0-9]{2}))$/,
								message: 'Follow pattern for EU VAT numbers.'
							}
						]
					})(
						<Input
							size='large'
							placeholder='SEXXXXXXXXXXX1'
							disabled={(form.getFieldValue('organization.country') === 'SE' && checkGhostUserIsSaler()) || readOnly}
						/>
					)}
				</FormItem>
				<Row gutter={15}>
					<Col xs={{ span: 24 }} sm={{ span: 16 }}>
						<FormItem label='Address'>
							{getFieldDecorator('organization.address1', {
								rules: [{ required: true, message: 'Address is required' }]
							})(
								<Input
									size='large'
									prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />}
									placeholder='Collabs street 35A'
									disabled={checkGhostUserIsSaler() || readOnly}
								/>
							)}
						</FormItem>
					</Col>
					<Col xs={{ span: 24 }} sm={{ span: 8 }}>
						<FormItem label='Zip Code'>
							{getFieldDecorator('organization.zipCode', {
								rules: [{ required: true, message: 'Zip code is required' }]
							})(
								<Input
									size='large'
									prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />}
									placeholder='111 60'
									disabled={checkGhostUserIsSaler() || readOnly}
								/>
							)}
						</FormItem>
					</Col>
					<Col xs={{ span: 24 }} sm={{ span: 12 }}>
						<FormItem label='City'>
							{getFieldDecorator('organization.city', {
								rules: [{ required: true, message: 'City is required' }]
							})(
								<Input
									size='large'
									prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />}
									placeholder='Stockholm'
									disabled={checkGhostUserIsSaler() || readOnly}
								/>
							)}
						</FormItem>
					</Col>
					<Col xs={{ span: 24 }} sm={{ span: 12 }}>
						<FormItem label='Country'>
							{getFieldDecorator('organization.country', {
								rules: [
									{
										required: true,
										message: 'Please select a country'
									}
								],
								initialValue: 'SE'
							})(
								<Select
									prefix={<Icon type='global' style={{ color: 'rgba(0,0,0,.25)' }} />}
									placeholder='Select country'
									size='large'
									filterOption={(input, option) => option.props.children[2].toLowerCase().indexOf(input.toLowerCase()) >= 0}
									optionFilterProp='children'
									showSearch
									disabled={checkGhostUserIsSaler() || readOnly}
								>
									{countries.all
										.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
										.filter(({ status }) => status === 'assigned')
										.map((country) => (
											<Select.Option value={country.alpha2} key={country.alpha2}>
												{country.emoji} {country.name}
											</Select.Option>
										))}
								</Select>
							)}
						</FormItem>
					</Col>
				</Row>
			</React.Fragment>
		);
	}
}

export default OrganizationFormItems;
