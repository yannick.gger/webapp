import React, { Component } from 'react';
import { Menu, Dropdown, Icon, Modal, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';

import OrganizationLink from '../../organization-link';

const MenuItemGroup = Menu.ItemGroup;

const REMOVE_CAMPAIGN = gql`
	mutation removeCampaign($id: ID!) {
		removeCampaign(input: { id: $id }) {
			campaign {
				id
			}
		}
	}
`;

export default class CampaignDropdown extends Component {
	menu = (
		<Menu>
			<MenuItemGroup title='Quick actions'>
				{false && (
					<Menu.Item key='0' data-ripple='rgba(132,146, 164, 0.2)'>
						<OrganizationLink to={`/campaigns/create`}>
							<Icon type='copy' className='mr-10' /> Duplicate
						</OrganizationLink>
					</Menu.Item>
				)}
				<Menu.Item key='1' data-ripple='rgba(132,146, 164, 0.2)'>
					<ApolloConsumer>
						{(client) => (
							<a
								href='#'
								className='color-red'
								onClick={() => {
									this.showDeleteConfirm({ client: client, campaignId: this.props.campaign.id });
								}}
							>
								<Icon type='delete' className='mr-10' /> Delete
							</a>
						)}
					</ApolloConsumer>
				</Menu.Item>
			</MenuItemGroup>
		</Menu>
	);

	showDeleteConfirm = ({ client, campaignId }) => {
		Modal.confirm({
			title: 'Are you sure you want to remove this assignment?',
			content: 'A removed assignment can not be restored after removal.',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				client
					.mutate({
						mutation: REMOVE_CAMPAIGN,
						variables: { id: campaignId },
						refetchQueries: ['getOrganizationCampaigns']
					})
					.then(() => {
						message.info('Assignment was removed');
					});
			}
		});
	};

	render() {
		return (
			<React.Fragment>
				<Dropdown overlay={this.menu} trigger={['click']} placement='bottomRight'>
					<Icon type='ellipsis' />
				</Dropdown>
			</React.Fragment>
		);
	}
}
