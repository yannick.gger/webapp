import React, { Component } from 'react';
import { Row, Col, Tag, Divider, Tooltip, Card, Icon, Avatar } from 'antd';
import Moment from 'react-moment';

class AssignmentCard extends Component {
	getFirstNumberInfuencerAssignments = (number, assignments) => {
		if (!assignments) return;

		return assignments.slice(0, number);
	};

	getUploadedStatsInfluencers = (assignments) => {
		if (!assignments) return;

		return assignments.filter(({ node }) => node.statsAdded === true);
	};

	renderInfluencerTooltip = (instagramOwner) => {
		const {
			followedByCount,
			instagramOwnerUser: { username, avatar }
		} = instagramOwner;

		return (
			<Tooltip placement='topLeft' title={`${username} / ${followedByCount.toShort()}`}>
				<Avatar src={avatar} icon='user' />
			</Tooltip>
		);
	};

	render() {
		const { assignmentNode, campaignInstagramOwnerAssignments } = this.props;

		const uploadedStatsInfluencers = this.getUploadedStatsInfluencers(campaignInstagramOwnerAssignments);

		const slicedUploadedStatsInfluencerAssignments = this.getFirstNumberInfuencerAssignments(9, campaignInstagramOwnerAssignments);

		const influencerAssignmentsLeftCount = campaignInstagramOwnerAssignments.length - slicedUploadedStatsInfluencerAssignments.length;

		return (
			<Card
				hoverable
				className='campaign-card'
				data-ripple='rgba(132,146, 164, 0.2)'
				cover={
					<div className='blue-cover-bg d-flex align-items-center justify-content-center' style={{ height: '150px' }}>
						<Icon className=' ion ion-camera' />
					</div>
				}
			>
				<Card.Meta title={assignmentNode.name} />
				<Divider className='mt-20 mb-20' />
				<Row>
					<Col xs={{ span: 12 }}>
						<h4>Post date</h4>
						<h3 className='color-red mb-0'>
							<Moment fromNow>{assignmentNode.startTime}</Moment>
						</h3>
					</Col>
					<Col xs={{ span: 12 }}>
						<h4>Have yet to post</h4>
						<h3 className='color-blue mb-0'>{assignmentNode.statistics.yetToPostedInfluencersCount} influencers</h3>
					</Col>
				</Row>
				<Divider className='mt-20 mb-20' />
				<h4>
					Has uploaded stats {uploadedStatsInfluencers.length}/{campaignInstagramOwnerAssignments.length}
				</h4>

				{slicedUploadedStatsInfluencerAssignments.length > 0 && (
					<div className='avatar-list'>
						{slicedUploadedStatsInfluencerAssignments.map(({ node }) => this.renderInfluencerTooltip(node.instagramOwner))}

						{influencerAssignmentsLeftCount > 0 && <Avatar key='more'>+ {influencerAssignmentsLeftCount}</Avatar>}
					</div>
				)}

				<Divider className='mt-20 mb-20' />
				<Tag className='card-tag'>Type: {assignmentNode.postType}</Tag>
			</Card>
		);
	}
}

export default AssignmentCard;
