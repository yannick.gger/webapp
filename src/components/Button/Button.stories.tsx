import { ComponentMeta, ComponentStory } from '@storybook/react';
import Icon from 'components/Icon/Icon';
import { Button, SecondaryButton, DarkButton, GhostButton } from './Button';
import { IButtonProps } from './types';

export default {
	title: 'Buttons',
	component: Button,
	argTypes: {
		text: {
			control: { type: 'text' }
		},
		type: {
			options: ['sm', 'md', 'lg'],
			control: { type: 'select' }
		}
	}
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args: IButtonProps) => (
	<>
		<div style={{ maxWidth: 1200, margin: 'auto', padding: '1rem 2rem' }}>
			<h4>Primary button</h4>
			<div style={{ display: 'flex', gap: 15, marginBottom: 30 }}>
				<div>
					<Button {...args}>Search</Button>
				</div>
				<div>
					<Button {...args} disabled={true}>
						Search
					</Button>
				</div>
				<div>
					<Button {...args}>
						<span>Button with icon</span> <Icon icon='calendar' />
					</Button>
				</div>
			</div>
			<h4>Secondary button</h4>
			<div style={{ display: 'flex', gap: 15, marginBottom: 30 }}>
				<SecondaryButton {...args}>Search</SecondaryButton>
				<SecondaryButton {...args} disabled={true}>
					Search
				</SecondaryButton>
			</div>
			<h4>Dark button</h4>
			<div style={{ display: 'flex', gap: 15, marginBottom: 30 }}>
				<DarkButton {...args}>Search</DarkButton>
				<DarkButton {...args} disabled={true}>
					Search
				</DarkButton>
			</div>
			<h4>Ghost button</h4>
			<div style={{ display: 'flex', gap: 15, marginBottom: 30 }}>
				<GhostButton {...args}>Search</GhostButton>
				<GhostButton {...args} disabled={true}>
					Search
				</GhostButton>
			</div>
			<h4>Sizes</h4>
			<div style={{ display: 'flex', gap: 15, marginBottom: 30 }}>
				<div>
					<Button {...args} size='sm'>
						Small
					</Button>
				</div>
				<div>
					<Button {...args}>Medium</Button>
				</div>
				<div>
					<Button {...args} size='lg'>
						Large
					</Button>
				</div>
			</div>
		</div>
	</>
);
export const RegularButton = Template.bind({});
