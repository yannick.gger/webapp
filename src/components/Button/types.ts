export type ButtonSize = 'sm' | 'lg';

export interface IButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
	id?: string;
	size?: ButtonSize;
	disabled?: boolean;
	className?: string;
	loading?: boolean;
	onClick?: () => void;
	role?: string;
	ariaLabel?: string;
	children?: React.ReactNode;
	type?: 'button' | 'submit' | 'reset';
}
