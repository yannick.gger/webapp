import styled from 'styled-components';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';
import { IButtonProps } from './types';

const theme = colors.button;
const typograph = typography.button;

const Button = styled.button<IButtonProps>`
	font-family: ${typograph.fontFamiliy};
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	gap: 8px;
	padding: ${(props) => (props.size === 'sm' ? '0.25rem 0.5rem' : props.size === 'lg' ? '0.75rem 1.5rem' : '0.5625rem 1rem')};
	border: solid 2px ${theme.primary.borderColor};
	background-color: ${theme.primary.background};
	cursor: pointer;
	font-size: ${(props) => (props.size === 'sm' ? typograph.small.fontSize : props.size === 'lg' ? typograph.large.fontSize : typograph.medium.fontSize)};
	color: ${theme.primary.color};
	transition: background-color 150ms ease-in-out, color 150ms ease-in-out, border-color 150ms ease-in-out;

	&:hover:not(:disabled) {
		background-color: ${theme.primary.backgroundHover};
		color: ${theme.primary.color};
	}

	&:disabled {
		border-color: ${theme.primary.disabled.borderColor};
		color: ${theme.primary.disabled.color};
		cursor: default;
		path {
			fill: ${theme.primary.disabled.color};
		}
	}

	.icon {
		display: inherit;
		width: auto;
		height: auto;
	}
`;

const SecondaryButton = styled(Button)`
	border-color: ${theme.sencondary.borderColor};
	color: ${theme.sencondary.color};

	&:hover:not(:disabled) {
		background-color: ${theme.sencondary.backgroundHover};
		color: ${theme.sencondary.colorHover};
	}

	&:disabled {
		color: ${theme.sencondary.disabled.color};
	}
`;

const DarkButton = styled(Button)`
	background-color: ${theme.dark.background};
	color: ${theme.dark.color};

	&:disabled {
		background-color: ${theme.dark.disabled.background};
		color: ${theme.dark.disabled.color};
		border-color: ${theme.dark.disabled.borderColor};

		svg > * {
			fill: ${theme.primary.disabled.color};
		}
	}
`;

const GhostButton = styled(Button)`
	background-color: ${theme.ghost.background};
`;

const ButtonContent = styled.span`
	display: flex;
	align-items: center;
`;

const LinkButton = styled(Button)`
	background-color: transparent;
	border-color: transparent;

	&:hover:not(:disabled) {
		border-color: #333;
		background-color: transparent;
	}

	&:disabled {
		border-color: transparent;
	}
`;

const Styled = {
	Button,
	SecondaryButton,
	DarkButton,
	GhostButton,
	ButtonContent,
	LinkButton
};

export default Styled;
