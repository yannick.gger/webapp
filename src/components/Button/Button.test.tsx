import { render } from '@testing-library/react';
import { Button, SecondaryButton, DarkButton, GhostButton } from './Button';

const mockedOnClick = jest.fn();

const mockedProps = {
	text: 'Button text',
	onClick: mockedOnClick
};

describe('<Button /> test', () => {
	it('renders Button', () => {
		const button = render(<Button {...mockedProps} />);
		expect(button).toMatchSnapshot();
	});

	it('renders Secondary Button', () => {
		const secondaryButton = render(<SecondaryButton {...mockedProps} />);
		expect(secondaryButton).toMatchSnapshot();
	});

	it('renders DarkButton', () => {
		const darkButton = render(<DarkButton {...mockedProps} />);
		expect(darkButton).toMatchSnapshot();
	});

	it('renders GhostButton', () => {
		const ghostButton = render(<GhostButton {...mockedProps} />);
		expect(ghostButton).toMatchSnapshot();
	});
});
