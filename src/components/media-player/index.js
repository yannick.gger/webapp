import React, { Component } from 'react';
import { Carousel, Icon } from 'antd';

import './styles.scss';

export default class MediaPlayer extends Component {
	state = {
		i: 0,
		playing: false,
		showModal: false
	};

	seamlessVideo = (i) => {
		if (this.carouselRef) {
			const carousel = this.carouselRef.current;

			if (i + 1 < this.mediaRefs.length) {
				carousel.goTo(i + 1, true);
			} else {
				this.setState({ playing: false });
			}
		}
	};

	handleCarouselChange = (fromIndex, toIndex) => {
		if (this.mediaRefs[fromIndex].current.nodeName === 'VIDEO') {
			if (this.state.playing) this.mediaRefs[fromIndex].current.pause();
			this.mediaRefs[fromIndex].current.currentTime = 0;
		}

		if (this.mediaRefs[toIndex].current.nodeName === 'VIDEO') {
			this.mediaRefs[toIndex].current.currentTime = 0;
			if (this.state.playing) this.mediaRefs[toIndex].current.play();
		}

		this.mediaRefs[toIndex].current.parentElement.focus();
	};

	handlePlayPause = (i) => {
		const video = this.mediaRefs[i].current;
		if (video.nodeName === 'VIDEO') {
			if (!this.state.playing) {
				this.setState({ playing: true });
				video.play();
			} else {
				this.setState({ playing: false });
				video.pause();
			}
		}
	};

	handleKeyDown = (e, i) => {
		e.preventDefault();

		if (e.key === 'Escape' && this.state.showModal) this.handleFullscreen();
		else if (e.key === ' ') this.handlePlayPause(i);
	};

	handleFullscreen = () => {
		let timestamp;
		if (this.isSafari() && this.mediaRefs[this.state.i].current.nodeName === 'VIDEO') {
			timestamp = this.mediaRefs[this.state.i].current.currentTime;
		}

		this.setState(
			(pS) => ({ showModal: !pS.showModal }),
			() => {
				if (this.isSafari()) {
					const media = this.mediaRefs[this.state.i].current;
					const carousel = this.carouselRef.current;
					if (carousel) carousel.goTo(this.state.i, true);

					if (media.nodeName === 'VIDEO') {
						media.onloadeddata = () => {
							media.currentTime = timestamp;
							if (this.state.playing) media.play();
						};
					}

					this.mediaRefs[this.state.i].current.parentElement.focus();
				}
			}
		);
	};

	componentDidMount() {
		if (navigator.mediaSession) {
			navigator.mediaSession.setActionHandler('play', () => null);
			navigator.mediaSession.setActionHandler('pause', () => null);
		}
	}

	// This is a workaround as Safari doesn't re-evaluate on re-render without forcing it
	// TODO: Hopefully this can be removed in the future
	isSafari = () => /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

	render() {
		const { media } = this.props;
		if (media.length === 0) return null;

		this.carouselRef = React.createRef();
		this.mediaRefs = media.map(() => React.createRef());

		return (
			<div key={this.isSafari() ? this.state.showModal : 0} className={this.state.showModal ? 'media-fullscreen' : ''}>
				<div className='custom-media-player'>
					{media.length === 1 ? (
						this.renderMedia(media[0])
					) : (
						<Carousel
							className='omit-effect media-carousel'
							accessibility={false}
							ref={this.carouselRef}
							effect='fade'
							beforeChange={this.handleCarouselChange}
							afterChange={(i) => (this.isSafari() ? this.setState({ i }) : console.log(i))}
						>
							{media.map((media, i) => this.renderMedia(media, i))}
						</Carousel>
					)}
				</div>
			</div>
		);
	}

	renderMedia = (media, i = 0) => {
		const { playing } = this.state;
		if (!media || !['photo', 'video'].includes(media.type)) return null;

		return (
			<div className='carouselContent' key={i} tabIndex='-1' onKeyDown={(e) => this.handleKeyDown(e, i)}>
				{media.type === 'photo' ? (
					<img ref={this.mediaRefs ? this.mediaRefs[i] : null} src={media.photo} alt='' />
				) : (
					<React.Fragment>
						<video ref={this.mediaRefs ? this.mediaRefs[i] : null} onEnded={() => this.seamlessVideo(i)} onClick={() => this.handlePlayPause(i)}>
							<source src={media.video} type='video/mp4' />
							Your browser does not support the video tag.
						</video>
						<Icon className='play-pause media-tools' type={playing ? 'pause-circle' : 'play-circle'} onClick={() => this.handlePlayPause(i)} />
					</React.Fragment>
				)}
				<Icon className='fs-toggle media-tools' type={this.state.showModal ? 'fullscreen-exit' : 'fullscreen'} onClick={this.handleFullscreen} />
			</div>
		);
	};
}
