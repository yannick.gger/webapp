import styled from 'styled-components';

const ConnectedPagesHeading = styled.span`
	font-size: 14px;
	font-weight: 700;
	display: block;
	padding-top: 15px;
	color: #888a8d;
	line-height: 1.2;
`;

const ConnectedPagesList = styled.ul`
	list-style: none;
	padding: 0;
	margin: 0;
`;

const ConnectedPagesListItem = styled.li`
	margin-bottom: 5px;
`;

const ConnectedPagesText = styled.span`
	font-size: 13px;
`;

export const Styled = {
	ConnectedPagesHeading,
	ConnectedPagesList,
	ConnectedPagesListItem,
	ConnectedPagesText
};
