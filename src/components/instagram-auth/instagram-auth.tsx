import { Button, Spin, Switch } from 'antd';
import i18next from 'i18next';
import React, { useEffect, useState, useContext } from 'react';
import { InstagramIcon } from '../icons';
import { IInstagramAuth } from './types';
import FacebookAuthService from 'services/FacebookApi/Facebook-auth.service';
import { ToastContext } from 'contexts';
import { StatusCode } from 'services/Response.types';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { KEY_FACEBOOK_ACCESS_TOKEN } from 'constants/localStorage-keys';
import { login, loginStatus } from 'shared/facebook/facebook-sdk';
import { loginResponse } from 'shared/facebook/types';
import { Styled } from './instagram-auth.style';

const InstagramAuth = (props: IInstagramAuth) => {
	const [token, setToken] = useState('');
	const [loading, setLoading] = useState(false);
	const [loadingPages, setLoadingPages] = useState(false);
	const [selected, setSelected] = useState(props.isConnected);
	const { addToast } = useContext(ToastContext);
	const storage = new BrowserStorage(StorageType.SESSION);
	const [connectedPages, setConnectedPages] = useState<string>('');

	const onClick = async () => {
		signIn(false);
	};

	const onChange = async (selected: boolean) => {
		selected ? signIn(true) : signOut();
	};

	const getConnectedPages = () => {
		let arr: any[] = [];
		setLoadingPages(true);
		FacebookAuthService.getBusinessAccounts()
			.then((result) => {
				if (result) {
					result.map((page: any) => {
						arr.push(page.attributes.name);
					});

					setConnectedPages(arr.join(', '));
					setLoadingPages(false);
				}
			})
			.catch(() => {
				addToast({ id: 'pages-fb-ig-err', mode: 'error', message: 'Oops! Cannot get your connected pages' });
				setLoadingPages(false);
			});
	};

	useEffect(() => {
		if (!selected) {
			loginStatus().then((status: string) => {
				setSelected(status === 'connected');
			});
		}
	}, []);

	useEffect(() => {
		if (selected) {
			getConnectedPages();
		}
	}, [selected]);

	const signIn = async (sendTokenToApi: boolean) => {
		// login with facebook then authenticate with the API to get a JWT auth token
		login().then((data: loginResponse) => {
			if (!data.authResponse) {
				setSelected(false);
				setLoading(false);
			} else {
				if (sendTokenToApi) {
					setToken(data.authResponse.accessToken);
				} else {
					storage.setItem(KEY_FACEBOOK_ACCESS_TOKEN, data.authResponse.accessToken);
				}
				props.successCallback && props.successCallback();
			}
		});
	};

	const signOut = async () => {
		setLoading(true);
		setSelected(false);
		FacebookAuthService.deleteFacebookToken().then((result) => {
			if (result === StatusCode.OK) {
				addToast({ id: 'sign-out-fb-ig', mode: 'success', message: 'You have disconnected from Instagram.' });
				setConnectedPages('');
			} else {
				addToast({ id: 'sign-out-fb-ig-err', mode: 'error', message: 'Oops! Something went wrong' });
			}
			setLoading(false);
		});
	};

	useEffect(() => {
		if (token !== '') {
			setLoading(true);
			FacebookAuthService.storeFacebookAccessToken(token).then((result) => {
				if (result === StatusCode.OK) {
					setSelected(true);
					addToast({ id: 'sigin-fb-ig', mode: 'success', message: 'You have now connected with Instagram.' });
					setLoading(false);
					getConnectedPages();
				}
			});
		}
	}, [token]);

	return (
		<React.Fragment>
			{props.type === 'button' ? (
				<Button type='primary' onClick={onClick}>
					<span className='anticon'>
						<InstagramIcon className='btn-icon' />
					</span>
					{i18next.t('instagram.connectInstagramProfile', { defaultValue: 'Connect Instagram profile' })}
				</Button>
			) : (
				<>{loading ? <Spin className='collabspin' /> : <Switch onClick={onChange} checked={selected} />}</>
			)}
			<div>
				{loadingPages ? (
					<Spin className='collabspin' />
				) : (
					<>
						{connectedPages !== '' ? (
							<>
								<Styled.ConnectedPagesHeading>Connected Pages:</Styled.ConnectedPagesHeading>
								<Styled.ConnectedPagesText>{connectedPages}</Styled.ConnectedPagesText>
							</>
						) : null}
					</>
				)}
			</div>
		</React.Fragment>
	);
};

export default InstagramAuth;
