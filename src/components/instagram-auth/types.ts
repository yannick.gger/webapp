export interface IInstagramAuth {
	type: 'button' | 'switch';
	selected?: boolean;
	isConnected: boolean;
	successCallback?: () => void;
}

export type ErrorResponse = {
	error: string;
	error_reason: string;
	error_description: string;
};
