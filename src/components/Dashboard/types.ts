import { CampaignStatus } from 'constants/campaignStatus';

export interface CampaignStatistics {
	totalAssignments: number;
	completedAssignments: number;
	influencerTargetCount: number;
	influencersJoined: number;
	nextDeadline: string;
	contentWaitingForReview: number;
}
