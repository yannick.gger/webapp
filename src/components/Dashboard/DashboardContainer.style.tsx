import styled from 'styled-components';

const Wrapper = styled.div`
	width: 100%;
	height: 100vh;
`;

const Content = styled.div`
	max-width: 1920px;
	margin: 0 auto;
	padding-bottom: 3rem;
`;

const Widgets = styled.div`
	padding: 0 2rem;
`;

const GridContainer = styled.div`
	display: grid;
	grid-gap: 1rem;
	grid-template-columns: repeat(12, 1fr);
	align-items: start;
	grid-auto-flow: column;

	> div {
		height: 100%;
	}
`;

const Styled = {
	Wrapper,
	Content,
	Widgets,
	GridContainer
};

export default Styled;
