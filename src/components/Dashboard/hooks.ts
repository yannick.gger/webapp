import { Store } from 'json-api-models';
import CampaignsService from 'services/Campaign/Campaigns.service';
import { ICollabsResponse } from 'services/Response.types';
import { ItemAttributes } from 'types/http';
import { CampaignStatistics } from './types';

export const useCampaignsData = () => {
	const models = new Store();

	const getCampaigns = async () => {
		return CampaignsService.getCampaigns().then((response: ICollabsResponse) => {
			const campaigns = response.data;
			return campaigns;
		});
	};

	const getCampaignStatistics = async (id: number, cancelSignal: any): Promise<CampaignStatistics | null> => {
		return CampaignsService.getCampaign(id.toString(), 'campaignStatistics', cancelSignal).then((response: ICollabsResponse) => {
			const campaignStatistics: ItemAttributes<CampaignStatistics> = response.included && response.included.find((x: any) => x.type === 'campaignStatistics');

			if (campaignStatistics) {
				const {
					totalAssignments,
					completedAssignments,
					influencerTargetCount,
					influencersJoined,
					nextDeadline,
					contentWaitingForReview
				} = campaignStatistics.attributes;

				return {
					totalAssignments,
					completedAssignments,
					influencerTargetCount,
					influencersJoined,
					nextDeadline,
					contentWaitingForReview
				};
			}

			return null;
		});
	};

	return {
		getCampaigns,
		getCampaignStatistics
	};
};
