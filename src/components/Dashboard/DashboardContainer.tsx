import PageHeader from 'components/PageHeader';
import { ToastContext, UserContext } from 'contexts';
import { useContext, useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { getCurrentOrganization } from 'shared/utils';
import RecentCampaigns from './Components/RecentCampaigns';
import ActionBar from './Components/ActionBar';
import Styled from './DashboardContainer.style';
import { useCampaignsData } from './hooks';
import { CampaignCardType } from './Components/RecentCampaigns/types';
import Grid from 'styles/grid/grid';
import CampaignsCard from './Components/CampaignsCard';
import CampaignsModal from './Components/CampaignsModal/CampaignsModal';
import IntegratedInboxCard from './Components/IntegratedInboxCard';
import { CampaignStatus } from 'constants/campaignStatus';

/**
 * DashboardContainer
 * @todo i18next
 * @returns {JSX.Element}
 */
const DashboardContainer = (): JSX.Element => {
	const userContext = useContext(UserContext);
	const { addToast } = useContext(ToastContext);

	const orgSlug = getCurrentOrganization();
	const history = useHistory();
	const { getCampaigns, getCampaignStatistics } = useCampaignsData();
	const [campaigns, setCampaigs] = useState<Array<CampaignCardType>>([]);
	const [modalOpen, setModalOpen] = useState<boolean>(false);
	const [loadingCampaigns, setLoadingCampaigns] = useState<boolean>(false);

	const abortController = new AbortController();
	const cancel = useRef<any>(null);

	const textByDate = (): string => {
		var today = new Date();
		var curHr = today.getHours();

		if (curHr < 12) {
			return 'Good morning';
		} else if (curHr < 18) {
			return 'Good afternoon';
		} else {
			return 'Good evening';
		}
	};

	const pageHeadline = () => {
		if (userContext.user) {
			return `${textByDate()}, ${userContext.user.Username ? userContext.user.Username.split(' ')[0] : ''}`;
		}
		return textByDate();
	};

	const mapCampaignItem = (campaignItem: any): CampaignCardType => {
		return {
			id: campaignItem.id,
			name: campaignItem.attributes.name,
			coverImage: {
				sm: campaignItem.links.smallCoverPhoto,
				md: campaignItem.links.mediumCoverPhoto,
				lg: campaignItem.links.largeCoverPhoto
			},
			campaignUrl: `/${orgSlug}/campaigns/${campaignItem.id}`,
			status: campaignItem.attributes.status
		};
	};

	const requestCampaigns = () => {
		setLoadingCampaigns(true);
		let items: Array<any> = [];
		getCampaigns()
			.then((data: any) => {
				const campaignItems = data.filter((c: CampaignCardType) => c.status !== CampaignStatus.Done);
				campaignItems.map((item: CampaignCardType) => {
					items = [...items, mapCampaignItem(item)];
				});

				items = items.sort((a, b) => b.id - a.id);
				setCampaigs(items);
			})
			.catch(() => {
				addToast({ id: `error-getCampaigns`, mode: 'error', message: 'There was problem to get your recent campaigns.' });
			})
			.finally(() => {
				setLoadingCampaigns(false);
			});
	};

	const getCampaignStats = (id: number) => {
		cancel.current = abortController;
		return getCampaignStatistics(id, cancel.current.signal);
	};

	const abort = () => {
		cancel.current && cancel.current.abort();
	};

	useEffect(() => {
		requestCampaigns();
	}, []);

	return (
		<Styled.Wrapper>
			<PageHeader headline={pageHeadline()} showBreadcrumb={false} showCurrentDate={true} />
			<Styled.Content>
				<ActionBar organizationSlug={orgSlug} history={history} />
				<RecentCampaigns
					isLoading={loadingCampaigns}
					campaigns={campaigns}
					organizationSlug={orgSlug}
					history={history}
					count={3}
					getCampaignStatistics={getCampaignStats}
					cancelSignal={() => abort()}
					onClickSeeAll={() => setModalOpen(!modalOpen)}
				/>
				<Styled.Widgets>
					<Styled.GridContainer>
						<Grid.Column md={6}>
							<CampaignsCard
								isLoading={loadingCampaigns}
								campaigns={campaigns}
								organizationSlug={orgSlug}
								history={history}
								count={4}
								onClickSeeAll={() => setModalOpen(!modalOpen)}
								refetchCampaigns={requestCampaigns}
							/>
						</Grid.Column>
						<Grid.Column md={6}>
							<IntegratedInboxCard />
						</Grid.Column>
					</Styled.GridContainer>
				</Styled.Widgets>
			</Styled.Content>
			<CampaignsModal modalOpen={modalOpen} handleClose={() => setModalOpen(!modalOpen)} campaigns={campaigns} refetchCampaigns={requestCampaigns} />
		</Styled.Wrapper>
	);
};

export default DashboardContainer;
