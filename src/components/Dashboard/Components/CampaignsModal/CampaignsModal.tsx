import Accordion from 'components/Accordion';
import Styled from './CampaignsModal.style';
import Modal from 'components/Modal';
import { CampaignCardType } from '../RecentCampaigns/types';
import Icon from 'components/Icon';
import { ICampaignsModalProps } from './types';
import DeleteCampaginButton from '../DeleteCampaignButton';

/**
 * CampaignsModal
 * @todo i18next
 * @param {ICampaignsModalProps} props
 * @returns {JSX.Element}
 */
const CampaignsModal = (props: ICampaignsModalProps): JSX.Element => {
	const { modalOpen, handleClose, campaigns } = props;

	const activeCampaigns = campaigns && campaigns.filter((x: CampaignCardType) => x.status === 'active'); // @todo ENUM for DRY
	const draftCampaigns = campaigns && campaigns.filter((x: CampaignCardType) => x.status === 'draft');

	const deleteDraftCampaigns = () => {
		props.refetchCampaigns();
	};

	return (
		<Modal open={modalOpen} size='xs' handleClose={() => handleClose()}>
			<Modal.Header>
				<Styled.HeaderInner>
					<h3>All campaigns</h3>
					<Styled.CloseButton onClick={() => handleClose()}>
						<Icon icon='cancel' size='16' />
					</Styled.CloseButton>
				</Styled.HeaderInner>
			</Modal.Header>
			<Modal.Body>
				<Styled.BodyWrapper>
					<Styled.AccordionWrapper>
						<Accordion title='Active campaigns' open={activeCampaigns.length > 0} toggleIconPosition='left'>
							{activeCampaigns.length > 0 && (
								<Styled.BodyInner>
									<Styled.CampaignList>
										{activeCampaigns.map((campaign: CampaignCardType, index: number) => {
											return (
												<Styled.CampaignListItem key={index}>
													<Styled.CampaignListItemLink to={campaign.campaignUrl}>
														<Styled.CampaignListItemHeading>{campaign.name}</Styled.CampaignListItemHeading>
													</Styled.CampaignListItemLink>
												</Styled.CampaignListItem>
											);
										})}
									</Styled.CampaignList>
								</Styled.BodyInner>
							)}
							{activeCampaigns.length === 0 && <Styled.NoResultText>There no active campaings yet!</Styled.NoResultText>}
						</Accordion>
					</Styled.AccordionWrapper>
					<Styled.AccordionWrapper>
						<Accordion title='Draft campaigns' open={draftCampaigns.length > 0} toggleIconPosition='left'>
							{draftCampaigns && (
								<Styled.BodyInner>
									<Styled.CampaignList>
										{draftCampaigns.map((campaign: CampaignCardType, index: number) => {
											return (
												<Styled.CampaignListItem key={index}>
													<Styled.CampaignListItemLink to={campaign.campaignUrl}>
														<Styled.CampaignListItemHeading>{campaign.name}</Styled.CampaignListItemHeading>
														<DeleteCampaginButton campaignId={campaign.id} type='icon' onDelete={deleteDraftCampaigns} />
													</Styled.CampaignListItemLink>
												</Styled.CampaignListItem>
											);
										})}
									</Styled.CampaignList>
								</Styled.BodyInner>
							)}
							{draftCampaigns.length === 0 && <Styled.NoResultText>There no draft campaings yet!</Styled.NoResultText>}
						</Accordion>
					</Styled.AccordionWrapper>
				</Styled.BodyWrapper>
			</Modal.Body>
		</Modal>
	);
};

export default CampaignsModal;
