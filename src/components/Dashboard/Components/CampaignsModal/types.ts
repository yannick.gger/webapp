import { CampaignCardType } from '../RecentCampaigns/types';

export interface ICampaignsModalProps {
	modalOpen: boolean;
	handleClose: () => void;
	campaigns: Array<CampaignCardType>;
	refetchCampaigns: () => void;
}
