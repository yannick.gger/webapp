import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { scrollbarY } from 'styles/utils/scrollbar';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';
import AccordionStyle from 'components/Accordion/Accordion.style';

const BodyWrapper = styled.div`
	max-height: 400px;
	overflow-y: auto;
	overflow-x: hidden;
	${scrollbarY};
`;

const CampaignList = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
	width: 100%;
`;

const CampaignListItem = styled.li`
	position: relative;
	display: block;
	padding-bottom: 0.5rem;
	margin-bottom: 8px;
	border-bottom: 1px solid ${colors.campaignsModal.listItemBorderColor};
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;

	a {
		color: ${colors.campaignsModal.linkColor};
		font-weight: 900;
	}
`;

const { Body } = AccordionStyle;

const AccordionWrapper = styled.div`
	margin-bottom: 32px;

	&:last-child {
		margin-bottom: 0;
	}

	${Body} {
		max-height: unset;
	}
`;

const BodyInner = styled.div`
	padding: 1.3125rem 0 0 0;
`;

const CloseButton = styled.button`
	border: none;
	background-color: transparent;
	padding: 0;
	cursor: pointer;
	margin-left: auto;
	line-height: 0;
`;

const HeaderInner = styled.div`
	display: flex;
	align-items: center;
`;

const CampaignListItemHeading = styled.span`
	display: block;
	font-weight: 900;
	color: ${colors.campaignsCard.headingColor};
`;

const CampaignListItemLink = styled(Link)`
	display: flex;
	justify-content: space-between;
	align-items: center;
	background-color: transparent;
	transition: background-color 0.2s ease-in-out;
	padding: 1rem 0.5rem;

	&:hover {
		background-color: ${colors.campaignsCard.listItemBackgroundHover};
	}
`;

const NoResultText = styled.h4`
	font-size: ${typography.paragraphs.fontSize};
	font-weight: 900;
`;

const Styled = {
	CampaignList,
	CampaignListItem,
	CampaignListItemHeading,
	CampaignListItemLink,
	AccordionWrapper,
	BodyWrapper,
	BodyInner,
	CloseButton,
	HeaderInner,
	NoResultText
};

export default Styled;
