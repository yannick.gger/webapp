import styled from 'styled-components';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;

	.icon {
		& > i {
			&:hover {
				fill: ${colors.error};
			}
		}
	}
`;

const Styled = {
	Wrapper
};

export default Styled;
