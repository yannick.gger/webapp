import React, { useState, useContext } from 'react';

import CampaignsService from 'services/Campaign/Campaigns.service';
import LoadingSpinner from 'components/LoadingSpinner';
import { ToastContext } from 'contexts';
import Icon from 'components/Icon';
import Styled from './DeleteCampaignButton.style';
import { StatusCode } from 'services/Response.types';

const DeleteCampaginButton = (props: { campaignId: string | number; type?: 'icon'; onDelete?: () => void }) => {
	const [loading, setLoading] = useState(false);
	const { addToast } = useContext(ToastContext);

	const deleteCampaignHandler = (e: React.MouseEvent<HTMLElement>) => {
		e.preventDefault();
		setLoading(true);
		CampaignsService.deleteCampaign(props.campaignId)
			.then((res) => {
				if (res === StatusCode.DELETE_SUCCESS) {
					if (props.onDelete) {
						props.onDelete();
					}
					addToast({ id: 'delete-campaign-success', message: 'Deleted!', mode: 'success' });
				}
			})
			.catch((err) => {
				addToast({ id: 'delete-campaign-fail', message: 'Something went wrong!', mode: 'error' });
			})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<Styled.Wrapper onClick={deleteCampaignHandler}>
			{loading ? (
				<LoadingSpinner size='sm' />
			) : props.type && props.type === 'icon' ? (
				<div className='icon'>
					<Icon icon='trash-bin' size='24' />
				</div>
			) : (
				<div>Delete Campaign</div>
			)}
		</Styled.Wrapper>
	);
};

export default DeleteCampaginButton;
