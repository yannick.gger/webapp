import { CampaignStatistics } from 'components/Dashboard/types';

export interface ICampaignCardProps {
	id: number;
	index: number;
	name: string;
	coverImage: any;
	campaignUrl: string;
	toDoCount?: number;
	className?: string;
	onMouseEnter?: (id: number) => void;
	onMouseLeave?: () => void;
	history: any;
	stats?: CampaignStatistics;
	loadingStats?: boolean;
}
