import { GhostButton, LinkButton } from 'components/Button';
import styled from 'styled-components';
import colors from 'styles/variables/colors';
import { gutters, guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';

const getNameTagBackground = (index: number) => {
	switch (index) {
		case 0:
			return colors.campaignCard.themeBackground.earlyDawn;
		case 1:
			return colors.campaignCard.themeBackground.azureishWhite;
		default:
			return colors.campaignCard.themeBackground.isabelline;
	}
};

const wrapperPadding = `${guttersWithRem.xl} ${guttersWithRem.xxl}`;

const NameTag = styled.div<{ index: number }>`
	position: relative;
	display: inline-block;
	padding: ${guttersWithRem.m};
	z-index: 2;
	background-color: ${(p) => getNameTagBackground(p.index)};
	transition: opacity 0.2s ease-in-out;

	span {
		font-size: 1rem;
		line-height: 0;
		font-weight: 900;
	}
`;

const Content = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	z-index: 3;
	height: 100%;
	padding: ${guttersWithRem.xxl};
	opacity: 0;
	transition: opacity 0.2s ease-in-out;
	max-width: 35rem;
	margin: 0 auto;
`;

const ContentInner = styled.div<{ index: number }>`
	display: flex;
	flex-flow: column;
	background-color: ${(p) => getNameTagBackground(p.index)};
	width: 100%;
	height: 100%;
	padding: ${guttersWithRem.xxl};
`;

const Wrapper = styled.div<{ sm: string; md: string; lg: string }>`
	position: relative;
	background-color: ${colors.campaignCard.backgroundColor};
	min-height: 516px;
	height: 100%;
	padding: ${wrapperPadding};
	border: solid 1px ${colors.campaignCard.borderColor};
	overflow: hidden;
	background-image: image-set(url(${(p) => p.md}) 1x, url(${(p) => p.lg}) 2x);
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center;
	&:hover {
		${Content} {
			opacity: 1;
		}

		${NameTag} {
			opacity: 0;
		}
	}
`;

const Type = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 1rem;
`;

const Topbar = styled.div`
	display: flex;
	align-items: center;
	flex: 0 1 auto;
	margin-bottom: ${gutters.l};
`;

const Heading = styled.h3`
	font-size: 2.5rem;
	font-weight: 500;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
`;

const TopArea = styled.div<{ isLoading?: boolean }>`
	flex: 1 1 auto;

	.cb-spinner {
		margin: 0 auto;
	}
`;

const BottomArea = styled.div`
	flex: 0 1 40px;
`;

const ButtonGroup = styled.div`
	display: flex;
	align-items: center;
	margin-left: -0.5rem;
	margin-right: -0.5rem;
	margin-bottom: ${gutters.m};
`;

const Stats = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	border-top: 1px solid ${colors.campaignCard.borderColor};
	padding-top: ${guttersWithRem.m};
`;

const StatsItem = styled.div``;

const StatsLabel = styled.span`
	display: block;
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 0.75rem;
`;

const StatsValue = styled.span`
	display: block;
	font-weight: 900;
	font-size: 0.75rem;
	text-align: center;
`;

const ViewCampaign = styled.div`
	display: flex;
	align-items: center;
	padding-top: 3rem;
`;

const ViewCampaignLink = styled(LinkButton)`
	position: relative;
	margin-left: auto;
	padding: 0;

	&:after {
		position: absolute;
		left: 0;
		bottom: -2px;
		width: 100%;
		height: 2px;
		content: '';
		background-color: ${colors.campaignCard.borderColor};
		opacity: 0;
		transition: opacity 0.2s ease-in-out;
	}

	&:hover:not(:disabled) {
		border-color: transparent;

		&:after {
			opacity: 1;
		}
	}
`;

const StatsBadge = styled.div`
	border: 1px solid ${colors.campaignCard.borderColor};
	border-radius: 3px;
	padding: ${guttersWithRem.s};

	span {
		color: ${colors.campaignCard.statsBadgeSpanColor};
	}
`;

const Styled = {
	Wrapper,
	NameTag,
	Content,
	ContentInner,
	Topbar,
	Type,
	Heading,
	ButtonGroup,
	BottomArea,
	Stats,
	StatsItem,
	StatsLabel,
	StatsValue,
	ViewCampaign,
	ViewCampaignLink,
	StatsBadge,
	TopArea
};

export default Styled;
