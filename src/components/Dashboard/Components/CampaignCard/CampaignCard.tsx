import Styled from './CampaginCard.style';
import image from 'assets/img/demo.jpg';
import Icon from 'components/Icon';
import { ICampaignCardProps } from './types';
import { useState, useEffect } from 'react';
import moment from 'moment';
import { CampaignStatistics } from 'components/Dashboard/types';
import LoadingSpinner from 'components/LoadingSpinner';

/**
 * CampaignCard
 * @todo i18next
 * @param {ICampaignCardProps} props
 * @returns  {JSX.Element}
 */
const CampaignCard = (props: ICampaignCardProps): JSX.Element => {
	const [stats, setStats] = useState<CampaignStatistics | null>(null);

	useEffect(() => {
		props.stats && setStats(props.stats);
	}, [props.stats]);

	return (
		<Styled.Wrapper
			sm={props.coverImage.sm}
			md={props.coverImage.md}
			lg={props.coverImage.lg}
			className={props.className}
			onMouseEnter={() => props.onMouseEnter && props.onMouseEnter(props.id)}
			onMouseLeave={() => props.onMouseLeave && props.onMouseLeave()}
		>
			<Styled.NameTag index={props.index}>
				<span>{props.name}</span>
			</Styled.NameTag>
			<Styled.Content>
				<Styled.ContentInner index={props.index}>
					<Styled.Topbar>
						<Styled.Type>Campaign</Styled.Type>
					</Styled.Topbar>
					<Styled.TopArea isLoading={props.loadingStats}>
						<Styled.Heading>{props.name}</Styled.Heading>
						{props.loadingStats && <LoadingSpinner size='md' />}
					</Styled.TopArea>
					<Styled.BottomArea>
						{!props.loadingStats && stats && (
							<>
								<Styled.ButtonGroup>
									<Styled.StatsBadge>
										{stats.influencersJoined} influencers joined <span>(of {stats.influencerTargetCount})</span>
									</Styled.StatsBadge>
								</Styled.ButtonGroup>
								<Styled.Stats>
									<Styled.StatsItem>
										<Styled.StatsLabel>Spots taken</Styled.StatsLabel>
										<Styled.StatsValue>
											{stats.influencersJoined} / {stats.influencerTargetCount}
										</Styled.StatsValue>
									</Styled.StatsItem>
									<Styled.StatsItem>
										<Styled.StatsLabel>Assingments</Styled.StatsLabel>
										<Styled.StatsValue>
											{stats.completedAssignments} / {stats.totalAssignments}
										</Styled.StatsValue>
									</Styled.StatsItem>
									<Styled.StatsItem>
										<Styled.StatsLabel>Next deadline</Styled.StatsLabel>
										<Styled.StatsValue>
											{stats.nextDeadline && stats.nextDeadline !== '-' ? moment(stats.nextDeadline).format(' D MMM') : '-'}
										</Styled.StatsValue>
									</Styled.StatsItem>
									<Styled.StatsItem>
										<Styled.StatsLabel>Content to review</Styled.StatsLabel>
										<Styled.StatsValue>{stats.contentWaitingForReview}</Styled.StatsValue>
									</Styled.StatsItem>
								</Styled.Stats>
							</>
						)}
						<Styled.ViewCampaign>
							<Styled.ViewCampaignLink onClick={() => props.history.push(props.campaignUrl)}>
								View Campaign <Icon icon='circle-arrow-up-right' />
							</Styled.ViewCampaignLink>
						</Styled.ViewCampaign>
					</Styled.BottomArea>
				</Styled.ContentInner>
			</Styled.Content>
		</Styled.Wrapper>
	);
};

export default CampaignCard;
