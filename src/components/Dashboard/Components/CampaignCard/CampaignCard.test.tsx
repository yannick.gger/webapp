import { render } from '@testing-library/react';
import CampaignCard from './CampaignCard';

describe('<CampaignCard /> test', () => {
	it('renders CampaignCard', () => {
		const campaignBar = render(
			<CampaignCard
				id={1}
				index={0}
				name={'Mocked name'}
				coverImage={{ sm: '//sm.jpg', md: '//md.jog', xl: '//xl.jpg' }}
				campaignUrl={'https://mocked.com'}
				history={undefined}
			/>
		);
		expect(campaignBar).toMatchSnapshot();
	});
});
