import styled from 'styled-components';
import Grid from 'styles/grid/grid';
import { CTALink } from 'styles/utils/CTALink';
import { mediaQueries, mediaMaxQueries } from 'styles/variables/media-queries';

const Wrapper = styled.div`
	min-height: 516px;
	margin-bottom: 5.5rem;
`;

const HeadingWrapper = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 1.2rem;
	margin-bottom: 1.2rem;
`;

const Heading = styled.span`
	font-weight: 900;
	margin-right: 1rem;
`;

const SeeAllLink = styled.a`
	${CTALink};
`;

const BodyWrapper = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
`;

const Column = styled(Grid.Column)``;

const CampaginCardContainer = styled.div`
	flex: 1;
	opacity: 0;
	transition: opacity 0.2s ease-in-out;

	&.loaded {
		opacity: 1;
	}

	&.two-cols {
		${Column} {
			&:first-child .campaign-item {
				${mediaMaxQueries.xl`
					border-bottom: 0;
				 `};

				${mediaQueries.xl`
					border-right: 0;
				`};
			}
		}
	}

	&.three-cols {
		${Column} {
			&:not(:last-child) .campaign-item {
				${mediaMaxQueries.xl`
					border-bottom: 0;
				 `};
			}

			&:nth-child(2) .campaign-item {
				${mediaQueries.xl`
					border-right: 0;
					border-left: 0;
				`};
			}
		}
	}
`;

const NoResult = styled.div`
	flex: 1;
	text-align: center;

	p {
		margin-bottom: 2rem;
	}

	button {
		margin: 0 auto;
	}
`;

const Styled = {
	Wrapper,
	HeadingWrapper,
	Heading,
	SeeAllLink,
	BodyWrapper,
	CampaginCardContainer,
	Column,
	NoResult
};

export default Styled;
