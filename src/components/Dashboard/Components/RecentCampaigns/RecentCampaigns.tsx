import classNames from 'classnames';
import CampaignCard from 'components/Dashboard/Components/CampaignCard';
import { ThemeContext, ToastContext } from 'contexts';
import { useContext, useEffect, useState } from 'react';
import Grid from 'styles/grid/grid';
import { GhostButton } from 'components/Button';
import Styled from './RecentCampaigns.style';
import Icon from 'components/Icon';
import { IRecentCampaignsProps, CampaignCardType } from './types';
import { AxiosError } from 'axios';
import { CampaignStatistics } from 'components/Dashboard/types';
import LoadingSpinner from 'components/LoadingSpinner/LoadingSpinner';

/**
 * RecentCampaigns
 * @todo i18next
 * @param {IRecentCampaignsProps} props
 * @returns {JSX.Element}
 */
const RecentCampaigns = (props: IRecentCampaignsProps): JSX.Element => {
	const themeContext = useContext(ThemeContext);
	const { addToast } = useContext(ToastContext);

	const [display, setDisplay] = useState<boolean>(false);
	const [currentCampaignStats, setCurrentCampaignStats] = useState<CampaignStatistics>();
	const [loadingStats, setLoadingStats] = useState<boolean>(true);
	const [campaigns, setCampaigns] = useState<Array<CampaignCardType>>([]);

	useEffect(() => {
		if (props.campaigns.length > 0) {
			setCampaigns(props.campaigns.slice(0, props.count));

			setTimeout(() => {
				setDisplay(true);
			}, 200);
		}
	}, [props.campaigns]);

	const setTheme = (index: number): void => {
		switch (index) {
			case 0:
				themeContext.setTheme('earlyDawn');
				break;
			case 1:
				themeContext.setTheme('azureishWhite');
				break;
			case 2:
				themeContext.setTheme('isabelline');
				break;
			default:
				themeContext.setTheme('homeDashboard');
		}
	};

	const resetTheme = (): void => {
		themeContext.setTheme('homeDashboard');
	};

	const handleMouseEnter = (index: number, id: number): void => {
		setTheme(index);
		setLoadingStats(true);
		if (props.getCampaignStatistics) {
			props
				.getCampaignStatistics(id)
				.then((data: CampaignStatistics) => {
					setCurrentCampaignStats(data);
					setLoadingStats(data === null);
				})
				.catch((error: AxiosError) => {
					addToast({ id: `error-getCampaignStats`, mode: 'error', message: 'There was problem to get your campaign stats.' });
					console.error(error.message);
					setLoadingStats(false);
				});
		}
	};

	const handleMouseLeave = () => {
		resetTheme();
		setCurrentCampaignStats(undefined);
		props.cancelSignal && props.cancelSignal();
	};

	return (
		<Styled.Wrapper>
			<Styled.HeadingWrapper>
				<Styled.Heading>Recent campaigns</Styled.Heading>
				{campaigns.length > 0 && (
					<Styled.SeeAllLink role='button' onClick={() => props.onClickSeeAll && props.onClickSeeAll()}>
						See all
					</Styled.SeeAllLink>
				)}
			</Styled.HeadingWrapper>
			<Styled.BodyWrapper>
				{campaigns.length > 0 && (
					<Styled.CampaginCardContainer
						className={classNames({
							loaded: display,
							'two-cols': campaigns.length === 2,
							'three-cols': campaigns.length === 3
						})}
					>
						<Grid.Container gap='0'>
							{campaigns.map((item: CampaignCardType, index: number) => {
								return (
									<Styled.Column key={index} md={12} xl={index !== 2 ? 6 : 12} xxl={12 / campaigns.length}>
										<CampaignCard
											history={props.history}
											onMouseEnter={(id: number) => handleMouseEnter(index, id)}
											onMouseLeave={() => handleMouseLeave()}
											className='campaign-item'
											index={index}
											stats={currentCampaignStats}
											loadingStats={loadingStats}
											{...item}
										/>
									</Styled.Column>
								);
							})}
						</Grid.Container>
					</Styled.CampaginCardContainer>
				)}
				{!props.isLoading && campaigns.length === 0 && (
					<Styled.NoResult className={classNames({ loaded: display })}>
						<h3>You have no campaigns yet!</h3>
						<p>Create a new campaign to get started.</p>
						<GhostButton onClick={() => props.history.push(`/${props.organizationSlug}/campaigns/create`)}>
							New campaign <Icon icon='circle-plus' />
						</GhostButton>
					</Styled.NoResult>
				)}
				{props.isLoading && <LoadingSpinner size='lg' />}
			</Styled.BodyWrapper>
		</Styled.Wrapper>
	);
};

export default RecentCampaigns;
