export interface IRecentCampaignsProps {
	organizationSlug: string;
	campaigns: Array<CampaignCardType>;
	isLoading: boolean;
	history: any;
	count: number;
	getCampaignStatistics?: (id: number) => Promise<any>;
	cancelSignal?: any;
	onClickSeeAll?: () => void;
}

export type CampaignCardType = {
	id: number;
	name: string;
	coverImage: ConverImage;
	campaignUrl: string;
	status: string;
};

type ConverImage = {
	sm: string;
	md: string;
	lg: string;
};
