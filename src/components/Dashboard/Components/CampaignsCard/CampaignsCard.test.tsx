import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import CampaignsCard from './CampaignsCard';

describe('<CampaignCard /> test', () => {
	it('renders CampaignsCard', () => {
		const campaignsCard = render(
			<MemoryRouter>
				<CampaignsCard
					organizationSlug={''}
					campaigns={[]}
					isLoading={false}
					history={undefined}
					count={0}
					onClickSeeAll={function(): void {
						throw new Error('Function not implemented.');
					}}
				/>
			</MemoryRouter>
		);
		expect(campaignsCard).toMatchSnapshot();
	});
});
