export interface ICampaignsCardProps {
	organizationSlug: string;
	campaigns: Array<CampaignItemType>;
	isLoading: boolean;
	history: any;
	count: number;
	onClickSeeAll: () => void;
	refetchCampaigns: () => void;
}

export type CampaignItemType = {
	id: number;
	name: string;
	campaignUrl: string;
	status: string;
};
