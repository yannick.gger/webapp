import Card from 'components/Card';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { CTALink } from 'styles/utils/CTALink';
import colors from 'styles/variables/colors';

const Heading = styled.h4`
	font-size: 1.5rem;
	margin-bottom: 44px;
`;

const CampaignList = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
	width: 100%;
	margin-bottom: 48px;
`;

const CampaignListItem = styled.li`
	padding: 0.125rem 0px;
	border-bottom: 1px ${colors.campaignsCard.listItemBorderColor} solid;
`;

const CampaignListItemHeading = styled.span`
	display: block;
	font-weight: 900;
	color: ${colors.campaignsCard.headingColor};
`;

const CampaignListItemLink = styled(Link)`
	display: flex;
	justify-content: space-between;
	align-items: center;
	background-color: transparent;
	transition: background-color 0.2s ease-in-out;
	padding: 1rem 0.5rem;

	&:hover {
		background-color: ${colors.campaignsCard.listItemBackgroundHover};
	}
`;

const FooterLinks = styled.div`
	display: flex;

	a {
		${CTALink};

		&:nth-child(2) {
			margin-left: auto;
		}
	}
`;

const CardWrapper = styled(Card)`
	padding: 2rem;
	height: 100%;
	min-height: 513px;

	.card__inner {
		display: flex;
		flex-direction: column;
	}

	.card__body {
		flex: 1 0 auto;
	}

	.card__footer {
		flex: 1 40px auto;
	}
`;

const CardContent = styled.div`
	display: flex;
	min-height: 204px;

	h5 {
		display: block;
		width: 100%;
		text-align: center;
	}
`;

const noResultHeading = styled.h3`
	margin-bottom: 0.25rem;
`;

const SeeAllLink = styled.a`
	${CTALink};
`;

const Styled = {
	Heading,
	CampaignList,
	CampaignListItem,
	CampaignListItemHeading,
	FooterLinks,
	CardWrapper,
	CardContent,
	noResultHeading,
	SeeAllLink,
	CampaignListItemLink
};

export default Styled;
