import Card from 'components/Card';
import { Link } from 'react-router-dom';
import Styled from './CampaignsCard.style';
import { ICampaignsCardProps, CampaignItemType } from './types';
import DeleteCampaginButton from '../DeleteCampaignButton';

/**
 * CampaignsCard
 * @todo i18next
 * @param {ICampaignsCardProps} props
 * @returns {JSX.Element}
 */
const CampaignsCard = (props: ICampaignsCardProps): JSX.Element => {
	return (
		<Styled.CardWrapper>
			<Card.Header>
				<Styled.Heading>Campaigns</Styled.Heading>
			</Card.Header>
			<Card.Body>
				<Styled.CardContent>
					{!props.isLoading && props.campaigns.length > 0 && (
						<Styled.CampaignList>
							{props.campaigns.slice(0, props.count).map((item: CampaignItemType, index: number) => {
								return (
									<Styled.CampaignListItem key={index}>
										<Styled.CampaignListItemLink to={`/${props.organizationSlug}/campaigns/${item.id}`}>
											<Styled.CampaignListItemHeading>{item.name}</Styled.CampaignListItemHeading>
											<DeleteCampaginButton campaignId={item.id} type='icon' onDelete={props.refetchCampaigns} />
										</Styled.CampaignListItemLink>
									</Styled.CampaignListItem>
								);
							})}
						</Styled.CampaignList>
					)}
					{!props.isLoading && props.campaigns.length === 0 && (
						<div>
							<Styled.noResultHeading>You have no campaigns yet!</Styled.noResultHeading>
							<p>Create a new campaign to get started.</p>
						</div>
					)}
					{props.isLoading && <h5>Loading...</h5>}
				</Styled.CardContent>
			</Card.Body>
			<Card.Footer>
				<Styled.FooterLinks>
					<Link to={`/${props.organizationSlug}/campaigns/create`}>New Campaign</Link>
					<Styled.SeeAllLink role='button' onClick={() => props.onClickSeeAll && props.onClickSeeAll()}>
						See all
					</Styled.SeeAllLink>
				</Styled.FooterLinks>
			</Card.Footer>
		</Styled.CardWrapper>
	);
};

export default CampaignsCard;
