import { render } from '@testing-library/react';
import ActionBar from './ActionBar';

describe('<ActionBar /> test', () => {
	it('renders ActionBar', () => {
		const actionBar = render(<ActionBar organizationSlug={'mocked'} history={undefined} />);
		expect(actionBar).toMatchSnapshot();
	});
});
