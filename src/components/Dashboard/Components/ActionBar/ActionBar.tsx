import { GhostButton } from 'components/Button';
import Styled from './ActionBar.style';
import Icon from 'components/Icon';
import { IActionBarProps } from './types';

/**
 * ActionBar
 * @todo i18next
 * @param props
 * @returns {JSX.Element}
 */
const ActionBar = (props: IActionBarProps): JSX.Element => {
	const { history, organizationSlug } = props;
	return (
		<Styled.Wrapper>
			<Styled.ButtonList>
				<Styled.ButtonListItem>
					<Styled.Link
						onClick={() => {
							history.push(`/${organizationSlug}/discover/new-discovery`);
						}}
					>
						Discovery <Icon icon='circle-arrow-up-right' />
					</Styled.Link>
				</Styled.ButtonListItem>
				<Styled.ButtonListItem>
					<Styled.Link
						onClick={() => {
							history.push(`/data-library`);
						}}
					>
						Data library <Icon icon='circle-arrow-up-right' />
					</Styled.Link>
				</Styled.ButtonListItem>
				<Styled.ButtonListItem>
					<GhostButton
						onClick={() => {
							history.push(`/${organizationSlug}/campaigns/create`);
						}}
					>
						New campaign <Icon icon='circle-plus' />
					</GhostButton>
				</Styled.ButtonListItem>
			</Styled.ButtonList>
		</Styled.Wrapper>
	);
};

export default ActionBar;
