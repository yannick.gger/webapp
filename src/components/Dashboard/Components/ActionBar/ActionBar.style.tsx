import { LinkButton } from 'components/Button';
import styled from 'styled-components';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	display: flex;
	padding: 0 2em;
	margin-bottom: 1.5rem;
`;

const ButtonList = styled.ul`
	padding: 0;
	list-style: none;
	margin: 0 -16px;
	margin-left: auto;
`;

const ButtonListItem = styled.li`
	display: inline-block;
	margin: 0 16px;
`;

const Link = styled(LinkButton)`
	&:hover:not(:disabled) {
		border-color: ${colors.dashboard.actionBar.linkBorderColor};
	}
`;

const Styled = {
	Wrapper,
	ButtonList,
	ButtonListItem,
	Link
};

export default Styled;
