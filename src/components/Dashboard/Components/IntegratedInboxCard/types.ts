export interface IIntegratedInboxCardProps {
	campaignId?: number;
}

export type Conversation = {
	conversationId: string;
	campaignName: string;
	influencer: string;
	createdAt: string;
	unSeen: Boolean;
};

export type Campaign = {
	campaignId: number;
	name: string;
	unSeen: Boolean;
	latestMessageDate: number;
	date: string;
};
