import Card from 'components/Card';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getCurrentOrganization } from 'shared/utils';
import Styled from './IntegratedInboxCard.style';
import { useIntegratedInboxData } from './hooks';
import { Campaign, Conversation, IIntegratedInboxCardProps } from './types';

/**
 * IntegratedInboxCard
 * @todo i18next, split up into multiple components
 * @param {IIntegratedInboxCardProps} props
 * @returns {JSX.Element}
 */
const IntegratedInboxCard = (props: IIntegratedInboxCardProps) => {
	const orgSlug: string = getCurrentOrganization();
	const { getCampaigns, getConversations } = useIntegratedInboxData();
	const [campaigns, setCampaigns] = useState<Array<Campaign>>([]);
	const [conversations, setConversations] = useState<Array<Conversation>>([]);

	const [loading, setLoading] = useState<boolean>(false);

	useEffect(() => {
		setLoading(true);
		props.campaignId ? _getMessagesByCampaign(props.campaignId) : _getCampaigns();
	}, []);

	const _getMessagesByCampaign = (campaignId: number) => {
		setConversations([]);
		getConversations(campaignId)
			.then((convArray: Array<Conversation>) => {
				setConversations(convArray.slice(0, 4));
			})
			.finally(() => {
				setLoading(false);
			});
	};

	const _getCampaigns = () => {
		setCampaigns([]);
		getCampaigns()
			.then((campaignArray: Array<Campaign>) => {
				setCampaigns(campaignArray.slice(0, 4));
			})
			.finally(() => {
				setLoading(false);
			});
	};

	const renderCampaigns = () => {
		return (
			<>
				{campaigns.length > 0 && (
					<Styled.MessageList>
						{campaigns.map((item: Campaign, index: number) => {
							return (
								<Styled.MessageListItem key={index}>
									<Styled.MessageListItemLink to={`/${orgSlug}/integrated-inbox/${item.campaignId}`}>
										<Styled.MessageListItemTitle>
											{item.name}
											{item.unSeen ? <Styled.NewMessageCircle /> : null}
										</Styled.MessageListItemTitle>
										<Styled.MessageListItemDate>{item.date}</Styled.MessageListItemDate>
									</Styled.MessageListItemLink>
								</Styled.MessageListItem>
							);
						})}
					</Styled.MessageList>
				)}

				{!loading && campaigns.length === 0 && (
					<div>
						<Styled.noResultHeading>You have no messages yet!</Styled.noResultHeading>
					</div>
				)}
			</>
		);
	};

	const renderConversations = () => {
		return (
			<>
				{conversations.length > 0 && (
					<Styled.MessageList>
						{conversations.map((conversation: Conversation, index: number) => {
							return (
								<Styled.MessageListItem key={index}>
									<Styled.MessageListItemLink to={`/${orgSlug}/integrated-inbox/${props.campaignId}/${conversation.conversationId}`}>
										<Styled.MessageListItemTitle>
											{conversation.campaignName} {conversation.unSeen ? <Styled.NewMessageCircle /> : null}
										</Styled.MessageListItemTitle>
										<Styled.MessageListItemDate>
											{conversation.influencer}, {conversation.createdAt}
										</Styled.MessageListItemDate>
									</Styled.MessageListItemLink>
								</Styled.MessageListItem>
							);
						})}
					</Styled.MessageList>
				)}
				{!loading && conversations.length === 0 && (
					<div>
						<Styled.noResultHeading>You have no messages yet!</Styled.noResultHeading>
					</div>
				)}
			</>
		);
	};

	return (
		<Styled.CardWrapper>
			<Card.Header>
				<Styled.Heading>Messages</Styled.Heading>
			</Card.Header>
			<Card.Body>
				<Styled.CardContent>
					{props.campaignId ? renderConversations() : renderCampaigns()}
					{loading && <h5>Loading...</h5>}
				</Styled.CardContent>
			</Card.Body>
			<Card.Footer>
				<Styled.FooterLinks>
					<Link to={`/${orgSlug}/integrated-inbox`}>New message</Link>
					<Link to={`/${orgSlug}/integrated-inbox`}>See all</Link>
				</Styled.FooterLinks>
			</Card.Footer>
		</Styled.CardWrapper>
	);
};

export default IntegratedInboxCard;
