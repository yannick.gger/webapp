import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import IntegratedInboxCard from './IntegratedInboxCard';

describe('<IntegratedInboxCard /> test', () => {
	it('renders IntegratedInboxCard', () => {
		const integratedInboxCard = render(
			<MemoryRouter>
				<IntegratedInboxCard />
			</MemoryRouter>
		);
		expect(integratedInboxCard).toMatchSnapshot();
	});
});
