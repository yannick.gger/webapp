import { getLatestMessageObject } from 'components/IntegratedInbox/Helpers';
import moment from 'moment';
import CampaignsService from 'services/Campaign/Campaigns.service';
import { ICollabsData, ICollabsResponse } from 'services/Response.types';
import { mapWeekDay } from 'shared/helpers/dates';
import { Campaign, Conversation } from './types';

export const useIntegratedInboxData = () => {
	const getCampaigns = async () => {
		return CampaignsService.getCampaigns(['latestMessage', 'unreadMessages']).then((response: ICollabsResponse) => {
			const data: Array<ICollabsData> = response.data;
			let campaigns: Array<Campaign> = [];

			data &&
				data.map((item: ICollabsData) => {
					const latestMessageData = item.relationships.latestMessage.data;
					const latestMessage = getLatestMessageObject(latestMessageData, response.included);

					if (latestMessage.attributes && latestMessage.attributes.createdAt) {
						const latestMessageId = latestMessageData.id;
						const unreadMessagesData = item.relationships.unreadMessages.data;
						const hasUnreadMessages = unreadMessagesData && unreadMessagesData.find((u: { id: string }) => u.id === latestMessageId);

						campaigns = [
							...campaigns,
							{
								campaignId: Number(item.id),
								name: item.attributes.name,
								latestMessageDate: new Date(latestMessage.attributes.createdAt).getTime(),
								date: moment(latestMessage.attributes.createdAt).format('ddd. DD MMMM. HH:MM'),
								unSeen: hasUnreadMessages
							}
						];

						campaigns = campaigns.sort((a: Campaign, b: Campaign) => b.latestMessageDate - a.latestMessageDate);
					}
				});

			return campaigns;
		});
	};

	const getConversations = async (campaignId: number) => {
		return CampaignsService.getCampaignConversations(campaignId).then(async (response: ICollabsResponse) => {
			const data: Array<ICollabsData> = response.data;
			let conversations: Array<Conversation> = [];

			if (data.length < 1) return conversations;

			// Get campaign object
			const currentCampaign = response.included.find((c: any) => c.type === 'campaign');

			data.map((item: ICollabsData) => {
				const influencerId = item.relationships.influencer.data.id;
				const influencer = response.included.find((i: any) => i.type === 'influencer' && i.id === influencerId);

				const latestMessageId = item.relationships.latestMessage.data.id;
				const unreadMessagesData = item.relationships.unreadMessages.data;
				const hasUnreadMessages = unreadMessagesData && unreadMessagesData.find((u: { id: string }) => u.id === latestMessageId);

				if (item.id) {
					conversations = [
						...conversations,
						{
							conversationId: item.id,
							campaignName: currentCampaign.attributes.name,
							influencer: influencer && influencer.attributes.username,
							createdAt: moment(item.attributes.createdAt).format('ddd. DD MMMM. HH:MM'),
							unSeen: hasUnreadMessages !== undefined
						}
					];
				}
			});
			return conversations;
		});
	};

	return {
		getCampaigns,
		getConversations
	};
};
