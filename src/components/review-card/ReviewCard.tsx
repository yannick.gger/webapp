import { Row, Col, Tag, Divider, Tooltip, Button, Card, Avatar } from 'antd';

import classes from './ReviewCard.module.scss';

const ReviewCard = () => {
	return (
		<Card
			className='campaign-card'
			cover={
				<>
					<img
						src='https://scontent-arn2-1.cdninstagram.com/vp/090fea5c9d6510c78ab810dcfda40bc7/5BE3F912/t51.2885-15/e35/35998774_825853284281202_6628093354817093632_n.jpg?efg=eyJ1cmxnZW4iOiJ1cmxnZW5fZnJvbV9pZyJ9'
						alt=''
					/>
					<Tag className={`card-tag mt-0 mb-0 pa-bl ${classes.tag}`}>For: Assignment name long stuff</Tag>
				</>
			}
		>
			<Card.Meta description='In collab. w @company - This is the caption of the image omgomg everything is epic wow such amaze' />

			<Row className='mt-20'>
				<Col>
					<Tooltip placement='topLeft' title='User name / followers'>
						<Avatar src='https://storage.googleapis.com/avatars.listagram.se/170/1706996866.jpg' icon='user' className='fl mr-10' />
					</Tooltip>
				</Col>
				<Col>
					<h3 className='fl mb-0'>@username</h3>
				</Col>
			</Row>
			<Divider />
			<Button className={`ant-btn-success ${classes.button}`}>Approve</Button>
			<Button className={classes.button} type='danger'>
				Decline
			</Button>
		</Card>
	);
};

export default ReviewCard;
