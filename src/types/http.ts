export interface ItemAttributes<T = any> {
	id: string;
	type: string;
	attributes: T;
	relationships?: Record<string, RelationShip>;
	links?: Record<string, string>;
}

export interface RelationShip {
	data: { id: string; type: string };
	self: string;
	related: string;
}

export interface CollectionResponse<T, R = any> {
	data: ItemAttributes<T>[];
	included?: ItemAttributes<R>[];
}

export interface ItemResponse<T extends ItemAttributes, R = any> {
	data: T;
	included?: ItemAttributes<R>[];
}

interface ViolationSource {
	parameter: string;
	message: string;
}

export interface Violation {
	status: string;
	title: string;
	source: ViolationSource;
}

export interface ErrorResponse {
	errors: Violation[];
}
