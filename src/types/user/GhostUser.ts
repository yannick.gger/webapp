type GhostUser = {
	token: string;
	user: {
		id: string;
	};
};

export default GhostUser;
