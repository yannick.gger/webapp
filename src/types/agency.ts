export interface Agency {
	name: string;
	description: string | null;
	createdAt: string;
}
