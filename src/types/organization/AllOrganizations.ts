import OrganizationEdge from './OrganizationEdge';

interface AllOrganizations extends OrganizationEdge {
	organizations: {
		pageInfo: {
			endCursor: string;
			hasNextPage: boolean;
		};
		edges: OrganizationEdge['node'][];
	};
}

export default AllOrganizations;
