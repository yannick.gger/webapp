type OrganizationEdge = {
	node: {
		id: string;
		name: string;
		slug: string;
		organizationNumber: string;
		vatNumber: string;
		taxPercent: number;
		address1: string;
		zipCode: string;
		city: string;
		country: string;
		isTrial: boolean;
		testAccount: boolean;
		isSubscribed: boolean;
		trialEndsAt: string;
		campaignCount?: number;
		draftCampaignCount?: number;
		activeCampaignCount?: number;
		doneCampaignCount?: number;
		currentUserRole?: string;
		hasTeamMember?: boolean;
		hasListSharedBySaler?: boolean;
		publisher: {
			logo: string;
		};
	};
};

export default OrganizationEdge;
