import Enzyme from 'enzyme';

jest.mock('react-chartjs-2', () => ({
	Bar: () => null,
	Line: () => null,
	Doughnut: () => null,
	Pie: () => null,
	Bubble: () => null
}));

const localStorageMock = {
	getItem: jest.fn(),
	setItem: jest.fn(),
	clear: jest.fn()
};
global.localStorage = localStorageMock;
