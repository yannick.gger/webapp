import { css } from 'styled-components';
import colors from 'styles/variables/colors';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';

const wrapperPadding = `${guttersWithRem.xs} ${guttersWithRem.s}`;

export const Table = css`
	width: 100%;
`;

export const TableHeader = css`
	width: 100%;
`;

export const TableBody = css``;

export const Th = css`
	font-family: ${typography.list.accessory.fontFamilies};
	font-size: ${typography.list.medium.fontSize};
	font-weight: ${typography.list.accessory.fontWeight};
	text-align: left;
	padding: ${wrapperPadding};
	white-space: nowrap;
`;

export const Tr = css`
	width: 100%;
	overflow-x: auto;
	background-color: ${colors.white};
	border: 2px solid ${colors.discovery.border};
	border-left: none;
	border-right: none;
	border-bottom: none;
`;

export const Td = css`
	padding: ${wrapperPadding};
	text-align: left;
`;
