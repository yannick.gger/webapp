export type InputType = {
	size: 'md' | 'lg';
	iconPosition?: 'left' | 'right';
	group?: boolean;
};

export type TextareaType = {
	disabled?: boolean;
};
