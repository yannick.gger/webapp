import { css } from 'styled-components';
import { InputType } from './types';

const getFieldPadding = (size: string): string => {
	return size === 'md' ? '2.875rem' : '3.5rem';
};

export const InputField = css<InputType>`
	background-color: ${({ theme }) => theme.InputField.background};
	border: ${({ theme }) => theme.InputField.border};
	padding: ${(props) => (props.size === 'md' ? '0.5rem 1rem' : '1rem 1rem')};
	width: 100%;
	font-size: 1rem;
	transition: box-shadow 0.1s ease-in-out;
	border-radius: ${({ theme }) => theme.InputField.borderRadius};
	outline: 0;

	&:hover:not(:disabled),
	&:focus:not(:disabled) {
		border: ${({ theme }) => theme.InputField.hover.border};
		box-shadow: 0 0 0 1px #333;
	}

	&::placeholder {
		color: ${({ theme }) => theme.InputField.placeholderColor};
	}

	&[type='password']:not(:placeholder-shown) {
		font-family: Verdana;
		letter-spacing: 0.125em;
	}

	&[disabled] {
		border-color: ${({ theme }) => theme.InputField.disabled.borderColor};

		&,
		&::placeholder {
			color: ${({ theme }) => theme.InputField.disabled.placeholderColor};
		}
	}
`;

export const InputFieldContainer = css<InputType>`
	position: relative;
	display: ${(props) => (props.group ? 'flex' : 'block')};
	align-items: ${(props) => (props.group ? 'center' : '')};

	> input {
		padding-left: ${(props) => (props.iconPosition === 'left' ? (props.group ? getFieldPadding(props.size) : '') : '')};
		padding-right: ${(props) => (props.iconPosition === 'right' ? (props.group ? getFieldPadding(props.size) : '') : '')};
	}
`;

export const InputFieldTooltip = css`
	position: absolute;
	top: -28px;
	right: 0;
	cursor: pointer;
`;

export const InputFieldIcon = css<InputType>`
	position: absolute;
	left: ${(props) => (props.iconPosition === 'left' ? '0' : 'auto')};
	right: ${(props) => (props.iconPosition === 'right' ? '0' : 'auto')};
	display: flex;
	padding: ${(props) => (props.size === 'md' ? '0.5rem 1rem' : '1rem 1rem')};

	i {
		display: flex;
		align-items: center;
	}
`;
