import colors from 'styles/variables/colors';
import { Themes, IThemeProps } from './types';
import { base } from './base';
import { PALETTE } from 'styles/variables/original-colors';

export type ThemeType = typeof collabs;

/**
 * Collabs base theme
 */
export const collabs: IThemeProps = {
	...base,
	background: colors.body.background,

	//Typography
	font: base.fontFamilies[0],
	fontSize: '1rem',
	color: colors.body.color,
	lineHeight: '1.5',
	heading: {
		font: base.fontFamilies[0],
		weight: base.weight.bold,
		color: colors.black,
		h1: {
			fontSize: base.fontSizes.mm,
			lineHeight: '1.370'
		},
		h2: {
			fontSize: base.fontSizes.ms,
			lineHeight: '1.370'
		},
		h3: {
			fontSize: base.fontSizes.xxl,
			lineHeight: '1.370'
		},
		h4: {
			fontSize: base.fontSizes.xl,
			lineHeight: '1.370'
		},
		h5: {
			fontSize: base.fontSizes.lg,
			lineHeight: '1.370'
		}
	},
	href: {
		color: colors.link.color,
		border: `1px solid ${colors.link.borderColor}`,
		hover: {
			color: colors.link.color,
			border: `1px solid ${colors.link.borderColorHover}`
		}
	},

	// Modal
	modal: {
		border: `1px solid ${colors.modal.borderColor}`,
		borderRadius: '2px',
		content: {
			background: colors.modal.background
		}
	},

	// Toggle Panel
	TogglePanel: {
		HeadingColor: colors.togglePanel.HeadingColor,
		borderBottom: `1px solid ${colors.togglePanel.borderBottomColor}`,
		active: {
			HeadingColor: colors.togglePanel.active.HeadingColor
		},
		hover: {
			HeadingColor: colors.togglePanel.hover.HeadingColor
		}
	},

	// Form Elements
	InputField: {
		background: colors.input.background,
		border: `1px solid ${colors.input.borderColor}`,
		borderRadius: '2px',
		placeholderColor: colors.input.placeholder,
		hover: {
			border: `1px solid ${colors.input.borderColorHover}`
		},
		disabled: {
			borderColor: colors.input.borderColorDisabled,
			placeholderColor: colors.input.placeholderDisabled
		}
	},
	textarea: {
		background: colors.textarea.background,
		borderBottom: `1px solid ${colors.textarea.borderBottomColor}`,
		headingColor: colors.textarea.headingColor,
		selected: {
			headingColor: colors.textarea.headingColorSelected
		}
	},
	select: {
		fontFamily: base.fontFamilies[0],
		fontSize: '1rem',
		placeholderColor: colors.select.placeholder,
		indicatorColor: colors.select.indicator,
		controlBorder: `1px solid ${colors.select.controlBorderColor}`,
		optionBackground: colors.select.optionBackground,
		optionSelectedColor: colors.select.optionSelected,
		controlBoxshadowColor: colors.select.controlBorderColor,
		menuBoxshadowColor: colors.select.menuBoxshadow
	}
};

/**
 * Integrated Inbox
 */
export const integratedInbox: ThemeType = {
	...collabs,
	background: colors.integratedInbox.background
};

export const discovery: ThemeType = {
	...collabs,
	background: colors.discovery.background
};

export const dataLibrary: ThemeType = {
	...collabs,
	background: colors.dataLibrary.background
};

export const homeDashboard: ThemeType = {
	...collabs,
	background: '#e1ecee'
};

export const earlyDawn: ThemeType = {
	...collabs,
	background: PALETTE.earlyDawn
};

export const azureishWhite: ThemeType = {
	...collabs,
	background: PALETTE.azureishWhite
};

export const isabelline: ThemeType = {
	...collabs,
	background: PALETTE.isabelline
};

export const createCampaign: ThemeType = {
	...collabs,
	background: colors.createCampaign.background
};

export const settings: ThemeType = {
	...collabs,
	background: colors.settings.background
};

export const contentReview: ThemeType = {
	...collabs,
	background: colors.white
};

const themes: Themes = {
	default: collabs,
	integratedInbox: integratedInbox,
	discovery: discovery,
	dataLibrary: dataLibrary,
	homeDashboard: homeDashboard,
	earlyDawn: earlyDawn,
	azureishWhite: azureishWhite,
	isabelline: isabelline,
	createCampaign: createCampaign,
	settings: settings,
	contentReview
};

export default themes;
