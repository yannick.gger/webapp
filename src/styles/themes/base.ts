import typography from 'styles/variables/typography';
import { BaseThemeProps } from './types';

export const base: BaseThemeProps = {
	fontFamilies: [typography.BaseFontFamiliy, typography.SecondaryFontFamiliy],
	fontSizes: {
		xs: '0.75rem',
		sm: '0.875rem',
		md: '1rem',
		lg: '19.2px',
		xl: '20px',
		xxl: '22px',
		ms: '23px', // mega small
		mm: '33.18px' // mega medium
	},
	weight: {
		light: 200,
		normal: 400,
		bold: 700
	},
	space: ['0rem', '0.25rem', '0.5rem', '0.75rem', '1rem', '1.25rem', '1.5rem', '2rem', '2.5rem', '3rem', '3.5rem', '4rem'],
	transition: '0.2s ease-in-out'
};
