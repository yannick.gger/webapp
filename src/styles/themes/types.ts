export interface BaseThemeProps {
	fontFamilies: Array<string>;
	fontSizes: any;
	weight: Weight;
	space: Array<string>;
	transition: string;
}

export interface IThemeProps extends BaseThemeProps {
	background: string;
	font: string;
	fontSizes: any;
	weight: Weight;
	color: string;
	heading: Heading;
	fontSize: string;
	lineHeight: string;
	href: Href;
	modal: Modal;
	TogglePanel: TogglePanel;
	InputField: InputField;
	textarea: Textarea;
	select: Select;
}

type Href = {
	color: string;
	border: string;
	hover: {
		color: string;
		border: string;
	};
};

type Heading = {
	font: string;
	weight: number;
	color: string;
	h1: HeadingElement;
	h2: HeadingElement;
	h3: HeadingElement;
	h4: HeadingElement;
	h5: HeadingElement;
};

type HeadingElement = {
	fontSize: string;
	lineHeight: string;
};

type Weight = {
	light: number;
	normal: number;
	bold: number;
};

type Modal = {
	border: string;
	borderRadius: string;
	content: {
		background: string;
	};
};

type TogglePanel = {
	HeadingColor: string;
	borderBottom: string;
	active: {
		HeadingColor: string;
	};
	hover: {
		HeadingColor: string;
	};
};

type InputField = {
	background: string;
	border: string;
	borderRadius: string;
	placeholderColor: string;
	hover: {
		border: string;
	};
	disabled: {
		borderColor: string;
		placeholderColor: string;
	};
};

type Textarea = {
	background: string;
	borderBottom: string;
	headingColor: string;
	selected: {
		headingColor: string;
	};
};

type Select = {
	fontFamily: string;
	fontSize: string;
	placeholderColor: string;
	indicatorColor: string;
	controlBorder: string;
	optionBackground: string;
	optionSelectedColor: string;
	controlBoxshadowColor: string;
	menuBoxshadowColor: string;
};

export type Themes = {
	[key: string]: any;
};
