import styled from 'styled-components';

/**
 * ============ FLEX ============
 * Use case:
 * - Implementation of small layouts with a few rows or a few columns.
 * - Alignments
 * - Content-first designs
 */

const Container = styled.div`
	display: flex;
`;

const Flex = {
	Container
};

export default Flex;
