import originalColors, { PALETTE } from './original-colors';

const colors = {
	// Main
	brandPrimary: '#157ff1',
	brandAccent: '#6c63ff',
	brandAccentAlt: '#00c661',
	// Grayscale
	white: '#ffffff',
	black: '#3d3f41',
	gray1: '#f6f9fa',
	gray2: '#e5eaf1',
	gray3: '#e7eaef',
	gray4: '#98a0a9',
	gray5: '#888a8d',
	gray6: '#595c5f',

	// Borders
	borderColor: '#e5eaf1', //gray2
	borderColorOnBg: '#d9e0e9',
	borderGray: '#333',
	// Brand
	brandColor1: '#54bca6', //Teal
	brandColor2: '#45cc6f', //Green
	brandColor3: '#54adcb', //Light blue
	brandColor4: '#626cdd', //Purple
	brandColor5: '#d5539a', //Pink
	brandColor6: '#d2ad4d', //yellow
	borderColorBg: '#d9e0e9',
	purpleLink: '#b0b0ff',
	mainPurple: '#5d70f8',
	mainPink: '#f126bc',
	mainGreen: '#6dc57b',
	mainYellow: '#eecf59',
	mainBlue: '#5085e5',
	mainTeal: '#49c8ae', //34CEAF
	lightTeal: '#e7fff2',
	lightPurple: '#ebedff',
	lightGreen: '#f1fef3',
	lightYellow: '#fcf6d5',
	lightBlue: '#f0fbfe',
	lightPink: '#f5eefd',

	pattensBlue: '#DEEBFF',
	transparent: 'transparent',

	// Toast
	tundora: '#414141',
	success: '#218f3f',
	info: '#3a71e3',
	error: '#da202a',
	warning: '#f5bd4f',

	buttonGray: '#F2F2F2',

	//
	// Body
	//
	body: {
		background: PALETTE.antiqueWhite,
		color: PALETTE.black
	},

	chart: {
		country: {
			first: {
				color: originalColors.PRICKLY_PURPLE,
				light: originalColors.GraySuit
			},
			second: {
				color: originalColors.CaribbeanGreen,
				light: originalColors.SwansDown
			},
			third: {
				color: originalColors.TurquoiseBlue,
				light: originalColors.Botticelli
			},
			fourth: {
				color: originalColors.Jade,
				light: originalColors.GLACIAL_WATER_GREEN
			},
			fifth: {
				color: originalColors.SanMarino,
				light: originalColors.INKED_SILK
			}
		}
	},
	chartPrimary: originalColors.Affair,
	chartSecondary: originalColors.CaribbeanGreen,
	chartTertiary: originalColors.TurquoiseBlue,

	chartPrimaryLight: originalColors.GraySuit,
	chartSecondaryLight: originalColors.SwansDown,
	chartTeriaryLight: originalColors.Botticelli,

	//
	// LineChart colors
	//
	chartLineReach: originalColors.GoldenTainoi,
	chartLineImpressions: originalColors.TurquoiseBlue,
	chartLineTraffic: originalColors.Affair,
	chartLineAudience: originalColors.DeepCerulean,
	chartLineBudget: originalColors.Jade,
	chartLineEngagement: originalColors.SanMarino,

	//
	// DataLibrary
	//
	dataLibrary: {
		backgroundDashboard: PALETTE.alabaster,
		background: PALETTE.antiFlashWhite,
		value: PALETTE.black,
		noData: PALETTE.argent,
		divider: PALETTE.argent
	},

	discovery: {
		background: PALETTE.alabaster,
		grayBackground: PALETTE.reachGreen,
		autoCompleteHover: PALETTE.antiFlashWhite,
		border: PALETTE.mercury,
		tagBorder: PALETTE.black,
		inputFocus: PALETTE.royalBlue,
		link: PALETTE.royalBlue,
		iconHover: PALETTE.mercury,
		black: PALETTE.black,
		tableItemBackground: PALETTE.eggWhite
	},

	//
	// Label
	//
	labelColor: PALETTE.taupeGrey,

	//
	// Links
	//
	link: {
		color: PALETTE.black,
		borderColor: PALETTE.black,
		borderColorHover: PALETTE.mistyRose
	},

	//
	// Integrated inbox
	//
	iIListColor: originalColors.MineShaft,
	iIListItemContentBorderColor: originalColors.Mercury,
	iIConversationsListItemHoverBackground: originalColors.LinkWater,
	iIConversationsListItemActiveBackground: originalColors.ChileanHeath,
	integratedInbox: {
		background: PALETTE.antiFlashWhite
	},

	//
	// Buttons
	//
	button: {
		primary: {
			background: PALETTE.white,
			backgroundHover: PALETTE.antiFlashWhite,
			borderColor: PALETTE.black,
			color: PALETTE.black,
			disabled: {
				color: PALETTE.argent,
				borderColor: PALETTE.argent
			}
		},
		sencondary: {
			borderColor: PALETTE.white,
			color: PALETTE.royalBlue,
			colorHover: PALETTE.royalBlue,
			backgroundHover: PALETTE.white,
			disabled: {
				color: PALETTE.argent
			}
		},
		dark: {
			background: PALETTE.black,
			color: PALETTE.white,
			disabled: {
				background: PALETTE.black,
				color: PALETTE.taupeGrey,
				borderColor: PALETTE.black
			}
		},
		ghost: {
			background: PALETTE.transparent
		}
	},

	//
	// Dropdowns
	//
	dropdown: {
		button: {
			backgroundHover: PALETTE.antiFlashWhite
		},
		menu: {
			background: PALETTE.white,
			borderColor: PALETTE.black,
			boxShadowColor: '#00000029'
		},
		menuItem: {
			color: PALETTE.black,
			background: PALETTE.transparent,
			backgroundHover: PALETTE.antiFlashWhite
		},
		tooltip: {
			background: PALETTE.black,
			boxShadowColor: '#00000029',
			color: PALETTE.antiFlashWhite
		}
	},

	//
	// User dropdown
	//
	userDropdown: {
		menuItemBorderColor: PALETTE.mercury,
		roleColor: PALETTE.davysGrey
	},

	//
	// Main navigation
	//
	MainNavigation: {
		background: PALETTE.alabaster,
		backgroundOpen: PALETTE.antiFlashWhite,
		color: PALETTE.black,
		colorHover: PALETTE.black,
		borderColor: PALETTE.black,
		menuLinkHoverBorderColor: PALETTE.black,
		hamburgerMenuColor: PALETTE.black
	},

	//
	// Header
	//
	header: {
		background: PALETTE.alabaster,
		color: PALETTE.black
	},

	//
	// Checkbox
	//
	checkbox: {
		border: PALETTE.black
	},

	//
	// Modal
	//
	modal: {
		background: PALETTE.white,
		borderColor: PALETTE.black
	},

	//
	// Input
	//
	input: {
		background: PALETTE.white,
		borderColor: PALETTE.taupeGrey,
		borderColorHover: PALETTE.black,
		borderColorDisabled: PALETTE.mercury,
		placeholder: PALETTE.taupeGrey,
		placeholderDisabled: PALETTE.argent
	},

	//
	// Textarea
	//
	textarea: {
		background: 'transparent',
		headingColor: PALETTE.taupeGrey,
		borderBottomColor: PALETTE.argent,
		headingColorSelected: PALETTE.black
	},

	//
	// Toggle Panel
	//
	togglePanel: {
		HeadingColor: PALETTE.taupeGrey,
		borderBottomColor: PALETTE.argent,
		active: {
			HeadingColor: PALETTE.black
		},
		hover: {
			HeadingColor: PALETTE.black
		}
	},

	//
	// Select
	//
	select: {
		placeholder: PALETTE.argent,
		indicator: PALETTE.black,
		controlBorderColor: PALETTE.black,
		optionBackground: PALETTE.antiFlashWhite,
		optionSelected: PALETTE.black,
		controlBoxshadow: PALETTE.black,
		menuBoxshadow: `${PALETTE.black}29`
	},

	CTALinks: {
		color: PALETTE.blueRibbon
	},

	dashboard: {
		actionBar: {
			linkBorderColor: PALETTE.black
		}
	},

	campaignCard: {
		themeBackground: {
			earlyDawn: PALETTE.earlyDawn,
			azureishWhite: PALETTE.azureishWhite,
			isabelline: PALETTE.isabelline,
			dedication: PALETTE.dedication
		},
		backgroundColor: PALETTE.alabaster,
		borderColor: PALETTE.black,
		statsBadgeSpanColor: PALETTE.davysGrey
	},

	campaignsCard: {
		headingColor: PALETTE.black,
		listItemBorderColor: originalColors.Mercury,
		listItemBackgroundHover: PALETTE.antiFlashWhite
	},

	campaignsModal: {
		listItemBorderColor: originalColors.Mercury,
		listItemBackgroundHover: PALETTE.antiFlashWhite,
		linkColor: PALETTE.black
	},

	integratedInboxCard: {
		listItemBorderColor: originalColors.Mercury,
		listItemBackgroundHover: PALETTE.antiFlashWhite,
		titleColor: PALETTE.black,
		subTitleColor: PALETTE.davysGrey,
		newMessageCircleBackground: '#3972e4', // outside of palette
		newMessageCircleBorderColor: PALETTE.white
	},

	//
	// Create campaign
	//
	createCampaign: {
		background: PALETTE.antiqueWhite,
		widgetHoverBackground: PALETTE.dedication,
		subTextColor: PALETTE.taupeGrey
	},

	//
	// Avatars
	//
	avatar: {
		borderColor: PALETTE.black,
		backgroundColor: PALETTE.feta
	},

	//
	// Settings view
	//
	settings: {
		background: PALETTE.azureishWhite
	},

	//
	// Form card
	//
	formCard: {
		background: PALETTE.white,
		borderColor: PALETTE.black,
		color: PALETTE.taupeGrey
	},

	//
	// Add account card
	//
	addAccountCard: {
		accountTypeBorderColor: PALETTE.mercury
	},

	//
	// Influencer tables
	//
	influencerList: {
		borderColor: PALETTE.mercury
	},

	//
	// Content review
	//
	contentReview: {
		usernameColor: PALETTE.taupeGrey,
		dateColor: PALETTE.davysGrey,
		imageBackgroundColor: PALETTE.alabaster,
		textareaBorderColor: PALETTE.black,
		modalBorderTopColor: PALETTE.mercury,
		cancelButtonColor: PALETTE.taupeGrey,
		accordionTitleColor: PALETTE.black,
		commentListBorderColor: PALETTE.mercury,
		commentBackgroundColor: PALETTE.antiFlashWhite,
		commentDateColor: PALETTE.taupeGrey,
		statusBackgroundColor: PALETTE.black,
		statusColor: PALETTE.antiFlashWhite,
		sidebarItemActiveBackgroundColor: PALETTE.alabaster,
		sidebarItemColor: PALETTE.davysGrey
	},

	// Brief page
	//
	briefPage: {
		campaignInfo: {
			background: PALETTE.reachGreen
		},
		campaignAssignments: {
			background: PALETTE.bleachWhite,
			dateInfo: PALETTE.reachGreen,
			divideLine: '#cccccc',
			detail: {
				title: PALETTE.taupeGrey
			}
		},
		campaignCommissions: {
			background: PALETTE.bleachWhite,
			label: PALETTE.reachGreen,
			divideLine: '#cccccc'
		}
	}
};

export default colors;
