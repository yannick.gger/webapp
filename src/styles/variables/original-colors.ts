//
// Collabs palette
//
export const PALETTE = {
	// Whites
	transparent: 'transparent',
	white: '#ffffff',
	eggWhite: '#FFFAF1',

	// Greys
	antiFlashWhite: '#f2f2f2',
	argent: '#c1c1c1',
	taupeGrey: '#888888',
	davysGrey: '#5c5c5c',
	black: '#333333',
	mercury: '#E2E2E2',

	// Yellows
	earlyDawn: '#fcf8ea',
	butteryWhite: '#fcfce9',
	maize: '#fdc653',
	yellowRed: '#f5bd4',
	tigersEye: '#e3813e',

	// Reds
	sunsetOrange: '#fd5258',
	amaranthRed: '#da202a',

	// Greens
	middleBlueGreen: '#7de8cd',
	gOGreen: '#00ab60',
	seaGreen: '#26a248',
	feta: '#e9fcee',
	reachGreen: '#f6f6ed',

	// Blues
	azureishWhite: '#e1ecee',
	cultured: '#f1f9f7',
	skyBlue: '#5dd2e6',
	celadonBlue: '#0086a7',
	royalBlue: '#3a71e3',
	blueYonder: '#4766aa',
	blueRibbon: '#0071ee',

	// Purples
	darkLavender: '#744c7e',

	// Pinks
	merino: '#fbedeb',
	softPeach: '#feeae6',
	isabelline: '#f6eced',
	mistyRose: '#fee0e1',

	// Peaches
	bleachWhite: '#feebd9',
	antiqueWhite: '#f7ebdf',
	alabaster: '#f6efe4',
	dedication: '#ffe3c9'
};

const originalColors = {
	MineShaft: PALETTE.black,
	Scorpion: PALETTE.davysGrey,
	Concrete: PALETTE.antiFlashWhite,
	Gray: PALETTE.taupeGrey,
	GoldenTainoi: PALETTE.maize,
	TurquoiseBlue: PALETTE.skyBlue,
	Affair: PALETTE.darkLavender,
	DeepCerulean: PALETTE.celadonBlue,
	Jade: PALETTE.gOGreen,
	SanMarino: PALETTE.blueYonder,
	CaribbeanGreen: PALETTE.middleBlueGreen,
	DoveGray: '#707070',
	Mercury: '#E2E2E2',
	BlueRibbon: '#0071EE',
	Amethyst: '#AD50CF',
	GraySuit: '#CCC4CF',
	SwansDown: '#DDF2ED',
	Botticelli: '#D6E7E9',
	LinkWater: '#e8f2fa',
	ChileanHeath: '#ffffe5',
	PRICKLY_PURPLE: '#9d5fb4',
	GLACIAL_WATER_GREEN: '#ccead4',
	INKED_SILK: '#dadce3'
};

export default originalColors;
