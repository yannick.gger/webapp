const fontSize = {
	sm: '10px',
	md: '12px',
	lg: '16px',
	s: '0.875rem',
	m: '1rem',
	l: '1.25rem',
	xl: '1.5rem'
};

export default fontSize;
