/**
 * @deprecated The object should not be used
 */
const breakPoints = {
	small: '768px',
	medium: '992px',
	large: '1320px',
	xLarge: '1600px'
};

export default breakPoints;
