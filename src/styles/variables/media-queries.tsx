import { css, CSSObject } from 'styled-components';

type Breakpoints = {
	xs: string;
	sm: string;
	md: string;
	lg: string;
	xl: string;
	xxl: string;
};

export const breakpoints: Breakpoints = {
	xs: '0',
	sm: '576px',
	md: '768px',
	lg: '992px',
	xl: '1200px',
	xxl: '1400px'
};

/**
 * @deprecated This function will be removed, use mediaQueries instead of this one.
 */
export const media = Object.keys(breakpoints).reduce((acc: any, label: string) => {
	acc[label] = (...args: any) => css`
		@media (min-width: ${breakpoints[label as keyof typeof breakpoints]}) {
			${args}
		}
	`;
	return acc;
}, {});

const breakpointToMediaQuery = (size: keyof typeof breakpoints) => (templateStrings: TemplateStringsArray) => css`
	@media (min-width: ${breakpoints[size]}) {
		${templateStrings}
	}
`;

const breakpointToMaxMediaQuery = (size: keyof typeof breakpoints) => (templateStrings: TemplateStringsArray) => css`
	@media (max-width: ${breakpoints[size]}) {
		${templateStrings}
	}
`;

export const mediaQueries = {
	small: breakpointToMediaQuery('sm'),
	medium: breakpointToMediaQuery('md'),
	large: breakpointToMediaQuery('lg'),
	xl: breakpointToMediaQuery('xl'),
	xxl: breakpointToMediaQuery('xxl')
};

export const mediaMaxQueries = {
	small: breakpointToMaxMediaQuery('sm'),
	medium: breakpointToMaxMediaQuery('md'),
	large: breakpointToMaxMediaQuery('lg'),
	xl: breakpointToMaxMediaQuery('xl'),
	xxl: breakpointToMaxMediaQuery('xxl')
};
