/**
 * Collabs Global styles
 */
import { createGlobalStyle } from 'styled-components';
import CollabsLogo from '../assets/img/logo/collabs-logo.svg';
import { IThemeProps } from './themes/types';
import { scrollbarY } from './utils/scrollbar';

const GlobalStyle = createGlobalStyle<{ theme: IThemeProps }>`
	* {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
	}

	body {
    margin: 0;
    padding: 0;
    font-family: ${({ theme }) => theme.font};
    font-size: ${({ theme }) => theme.fontSize};
		background-color: ${({ theme }) => theme.background};
		transition: background-color ${({ theme }) => theme.transition};
		color: ${({ theme }) => theme.color};
	}

	// Headings
	h1,
	h2,
	h3,
	h4,
	h5 {
		font-family: ${({ theme }) => theme.heading.font};
		font-weight: ${({ theme }) => theme.heading.weight};
		color: ${({ theme }) => theme.heading.color};
	}

	h1 {
		font-size: ${({ theme }) => theme.heading.h1.fontSize};
		line-height: ${({ theme }) => theme.heading.h1.lineHeight};
		margin-bottom: 1.5rem;
	}

	h2 {
		font-size: ${({ theme }) => theme.heading.h2.fontSize};
		line-height: ${({ theme }) => theme.heading.h2.lineHeight};
		margin-bottom: 1.05rem;
	}

	h3 {
		font-size: ${({ theme }) => theme.heading.h3.fontSize};
		line-height: ${({ theme }) => theme.heading.h3.lineHeight};
		margin-bottom: 1rem;
	}

	h4 {
		font-size: ${({ theme }) => theme.heading.h4.fontSize};
		line-height: ${({ theme }) => theme.heading.h4.lineHeight};
		margin-bottom: 0.9125rem;
	}

	h5 {
		font-size: ${({ theme }) => theme.heading.h5.fontSize};
		line-height: ${({ theme }) => theme.heading.h5.lineHeight};
		margin-bottom: 0.875rem;
	}

	// Paragraphs
	p {
    font-size: ${({ theme }) => theme.fontSize};
		line-height: ${({ theme }) => theme.lineHeight};
		color: ${({ theme }) => theme.color};
		margin-bottom: 0.5rem;
	}

	// Hrefs
	a[href^="http"],
	a[href^="mailto:"],
	a[href^="tel:"] {
		color: ${({ theme }) => theme.href.color};
		border-bottom: ${({ theme }) => theme.href.border};
		transition: border ${({ theme }) => theme.transition};

		&:hover,
		&:focus {
			color: ${({ theme }) => theme.href.hover.color};
			border-bottom: ${({ theme }) => theme.href.hover.border};
		}
	}

  select,
	input {
	  -webkit-appearance:none;
	}

	input[type=date] {
	  line-height: 1;
	}

	// Lists
	ul,
	ol {
		padding-left: 0;
	}

	.disable-scroll {
		overflow-y: hidden;
		padding-right: 18.9888px;

		.header-bar {
			padding-right: calc(2rem + 1.0625rem) !important;
		}
	}

	@keyframes App-logo-spin {
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
	}

	@keyframes scaleIn {
  0% {
    transform: scale(0);
		opacity: 0;
  }

  100% {
    transform: scale(1);
		opacity: 1;
  }
	}

	@keyframes scaleOut {
		0% {
			transform: scale(1);
			opacity: 1;
		}

		100% {
			transform: scale(0);
			opacity: 0;
		}
	}

	.item-rows {
    display: flex;
    flex-flow: row wrap;

    .d-flex {
        margin-bottom: 30px;
    }
	}

	.ant-layout {
		background-color: transparent;
	}

	.main-content {
		padding-top: 4.5rem;
	}

	.standalone {
    &.logo {
        width: 175px;
        height: 41px;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
        margin: 50px auto 0;

        &.dark {
            background-image: url(${CollabsLogo});
        }
    }
	}

	.ant-spin-container {
    background: #fff;
    box-shadow: 0 1px 3px 0 rgba(112, 157, 199, 0.15), 0 2px 3px 0 rgba(81, 104, 125, 0.05);
    border-radius: 4px;
	}

	.multi-line {
    white-space: pre-line;
	}

	.white-space-pre-line {
    white-space: pre-line;
	}

	.cover-center {
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
	}

	textarea.chatlio-new-message {
    border: 1px solid gray !important;
    border-radius: 10px !important;
    margin-bottom: 20px;
    min-width: 98%;
    margin: 0 auto 20px auto;
    &:focus {
        border-radius: 10px !important;
    }
    &:hover {
        border-radius: 10px !important;
    }
	}

	.chatlio-new-message-container {
    padding: 0 10px;
	}

	img {
    max-width: 100%;
	}

	td.fit-content {
    width: 1% !important;
	}

	.scrollbar {
		${scrollbarY}
	}


	.fade-out {
    -webkit-animation: fadeOut 1s;
    animation: fadeOut 1s;
    animation-fill-mode: forwards;
	}

	@-webkit-keyframes fadeOut {
			0% { opacity: 1;}
			100% { opacity: 0;}
	}
	@keyframes fadeOut {
			0% { opacity: 1;}
			100% { opacity: 0;}
	}

	.clearfix::after {
		content: "";
		clear: both;
		display: table;
	}

	@keyframes rotation {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
	}

	@keyframes changeColor {
  0% {
    border-color: #C5DCE0;
		border-bottom-color: transparent;
  }

  50% {
    border-color: #FFD7CC;
		border-bottom-color: transparent;
  }

  100% {
    border-color: #C5DCE0;
		border-bottom-color: transparent;
  }
}

  .w-100 {
    width: 100%
  }

  table.table th {
    text-align: left;
  }
`;

export default GlobalStyle;
