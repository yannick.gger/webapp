import styled from 'styled-components';

const MainContent = styled.div`
	padding: 0 2rem;
`;

export default MainContent;
