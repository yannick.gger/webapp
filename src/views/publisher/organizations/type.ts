import OrganizationEdge from 'types/organization/OrganizationEdge';
import GhostUser from 'types/user/GhostUser';

export interface IOrganizations {
	match: any;
}

export type PublisherData = {
	publisher: {
		id: number;
		organizations: {
			pageInfo: {
				endCursor: string;
				hasNextPage: boolean;
			};
			edges: OrganizationEdge[];
		};
	};
};

export type GhostUserData = {
	data: { signInAsGhost: GhostUser };
};
