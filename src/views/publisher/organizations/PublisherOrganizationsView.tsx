import { Menu, Dropdown, Button, Layout, Tooltip, Tag } from 'antd';
import { lookup } from 'country-data';
import { Query, QueryResult } from 'react-apollo';
import { Link } from 'react-router-dom';

import { apiToMoment } from 'utils';
import PaginatedTable from 'components/paginated-table';
import { LoginAsButton } from 'components';
import getPublisherOrganizationsQuery from 'graphql/get-publisher-organizations.graphql';
import { IOrganizations, PublisherData } from './type';
import OrganizationEdge from 'types/organization/OrganizationEdge';
import classes from './PublisherOrganizationsView.module.scss';

const MenuItems = (props: IOrganizations) => {
	const { match } = props;
	return (
		<Menu>
			<Menu.ItemGroup title='Invite'>
				<Menu.Item key='0'>
					<Link to={`/p/${match.params.publisherSlug}/organizations/invite`}>Invite new organization</Link>
				</Menu.Item>
			</Menu.ItemGroup>
		</Menu>
	);
};

const PublisherOrganizationsView = (props: IOrganizations) => {
	const { match } = props;

	document.title = `Organizations | Publisher | Collabs`;

	return (
		<Layout>
			<Layout.Header>
				<h1>Organizations</h1>
				<Dropdown overlay={<MenuItems match={match} />} trigger={['click']} placement='bottomRight'>
					<Button type='primary' icon=' ion ion-plus' size='large' />
				</Dropdown>
			</Layout.Header>
			<Layout.Content>
				<Query query={getPublisherOrganizationsQuery} variables={{ slug: match.params.publisherSlug }} notifyOnNetworkStatusChange>
					{(queryRes: QueryResult<PublisherData>) => {
						return (
							<PaginatedTable
								queryRes={queryRes}
								columns={[
									{
										title: 'Name',
										dataIndex: 'node.name',
										render: (name: string, organization: OrganizationEdge) => {
											return (
												<div>
													<span className={classes.organizationName}>{name}</span>
													{organization.node.isTrial && (
														<Tooltip title={`Trial ends at ${apiToMoment(organization.node.trialEndsAt).format('YYYY-MM-DD')}`}>
															<Tag color='blue'>Trial</Tag>
														</Tooltip>
													)}
													{organization.node.testAccount && (
														<Tooltip title="Account is a test account used and can't create campaigns">
															<Tag color='orange'>Test</Tag>
														</Tooltip>
													)}
													{organization.node.isSubscribed && (
														<Tooltip title='Account is on basic subscription'>
															<Tag color='green'>Basic</Tag>
														</Tooltip>
													)}
													{!organization.node.isSubscribed && !organization.node.testAccount && !organization.node.isTrial && (
														<Tooltip title='Account is on basic subscription'>
															<Tag color='brown'>No subscription</Tag>
														</Tooltip>
													)}
												</div>
											);
										}
									},
									{
										title: 'Org#',
										dataIndex: 'node.organizationNumber'
									},
									{
										title: 'Country',
										dataIndex: 'node.country',
										render: (country: string) => lookup.countries({ alpha2: country })[0].name
									},
									{
										title: 'No. of campaigns',
										dataIndex: 'node.campaignCount'
									},
									{
										title: 'Action',
										key: 'action',
										width: 360,
										render: (organization: OrganizationEdge) => {
											const loginAsButtonProps = {
												...props,
												mutationVariables: { organizationId: organization.node.id }
											};
											return (
												<span className='d-flex align-items-center'>
													<LoginAsButton {...loginAsButtonProps} />
												</span>
											);
										}
									}
								]}
								dataRef='publisher.organizations'
							/>
						);
					}}
				</Query>
			</Layout.Content>
		</Layout>
	);
};

export default PublisherOrganizationsView;
