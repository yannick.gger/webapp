import React, { Component } from 'react';
import { Row, Col, Icon, Card, Button, Layout, Spin } from 'antd';
import { ApolloConsumer, Query } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { signOut } from 'services/auth-service';
import AccountSettings from './components/account-settings';
import getPublisherBySlug from 'graphql/get-publisher-by-slug.graphql';

class Header extends Component {
	render() {
		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<h1>Settings</h1>
						<Button
							type='danger'
							className='btn-uppercase'
							onClick={() => {
								signOut(client, this.props.history);
							}}
						>
							<Icon type='logout' />
							Logout
						</Button>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}

Header.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

const HeaderWithRouter = withRouter(Header);

const tabListNoTitle = [
	{
		key: 'account',
		tab: 'Account'
	}
];

const contentListNoTitle = (key, publisher) => {
	switch (key) {
		case 'account':
			return <AccountSettings publisher={publisher} />;

		default:
			return <p>Available soon.</p>;
	}
};

class PublisherSettings extends Component {
	state = {
		noTitleKey: 'account'
	};
	onTabChange = (key, type) => {
		this.setState({ [type]: key });
	};
	render() {
		document.title = `Settings | Publisher | Collabs`;
		const publisherSlug = this.props.match.params.publisherSlug;
		return (
			<Layout>
				<Layout.Header>
					<HeaderWithRouter />
				</Layout.Header>
				<Layout.Content>
					<Row>
						<Col sm={{ span: 14, offset: 5 }}>
							<Query query={getPublisherBySlug} variables={{ slug: publisherSlug }}>
								{({ loading, data }) => {
									if (loading) return <Spin className='collabspin' />;
									return (
										<Card
											style={{ width: '100%' }}
											tabList={tabListNoTitle}
											activeTabKey={this.state.noTitleKey}
											onTabChange={(key) => {
												this.onTabChange(key, 'noTitleKey');
											}}
										>
											{contentListNoTitle(this.state.noTitleKey, data.publisher)}
										</Card>
									);
								}}
							</Query>
						</Col>
					</Row>
				</Layout.Content>
			</Layout>
		);
	}
}

export default PublisherSettings;
