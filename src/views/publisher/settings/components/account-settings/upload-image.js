import React, { Component } from 'react';
import { Upload, Icon, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import UpdatePublisherSettings from 'graphql/update-publisher-settings.graphql';

// import "./styles.scss"

export default class UploadPublisherLogo extends Component {
	state = {
		previewVisible: false,
		previewImage: '/image-sample.png',
		fileList: [],
		fileListUntouched: true
	};

	handleCancel = () => this.setState({ previewVisible: false });

	handlePreview = (file) => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleCustomRequest = ({ client, publisherId }) => {
		return ({ file, onError, onSuccess, onProgress }) => {
			client
				.mutate({
					mutation: UpdatePublisherSettings,
					variables: { id: publisherId, logo: file },
					refetchQueries: ['getPublisherBySlug']
				})
				.then(({ data, loading, error }) => {
					if (error) onError();
					if (data) {
						this.setState({ previewImage: data.updatePublisher.publisher.logo });

						onSuccess(data, file);
					}

					if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);
				});

			return {
				abort() {
					console.log('upload progress is aborted.');
				}
			};
		};
	};

	handleChange = (info) => {
		const status = info.file.status;
		this.setState({ fileList: info.fileList });

		if (status === 'done') {
			message.success(`${info.file.name} file uploaded successfully.`);
		} else if (status === 'error') {
			message.error(`${info.file.name} file upload failed.`);
		}
	};

	render() {
		const { fileList } = this.state;
		const { publisherId } = this.props;
		const UploadButton = () => (
			<div>
				<p className='ant-upload-drag-icon'>
					<Icon type='inbox' />
				</p>
				<p className='ant-upload-text px-20'>Click or drag an image here to upload</p>
			</div>
		);

		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<Upload.Dragger
							listType='picture'
							accept='image/*'
							fileList={fileList}
							className={fileList.length >= 1 ? 'hide-upload-area' : null}
							onPreview={this.handlePreview}
							onChange={this.handleChange}
							customRequest={this.handleCustomRequest({ client, publisherId })}
							multiple={false}
						>
							<UploadButton />
						</Upload.Dragger>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}
