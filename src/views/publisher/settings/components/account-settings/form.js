import React from 'react';
import { Form, Input, Icon, Select } from 'antd';

import UploadImage from './upload-image';
import { countries } from 'country-data';

const FormItem = Form.Item;

class AccountSettingsForm extends React.Component {
	render() {
		const { publisher, form } = this.props;
		const { getFieldDecorator } = form;

		return (
			<React.Fragment>
				<Form className='ant-form-lg'>
					<div className='border-bottom mb-30 pb-20'>
						<h4 className='color-blue'>Change logo</h4>
						<FormItem>{getFieldDecorator('logo')(<UploadImage publisherId={publisher.id} newImageIdHandler={this.handleNewImageId} />)}</FormItem>
						{/* <FormItem label="Email">{getFieldDecorator("Email")(<Input size="large" placeholder="Your mail" />)}</FormItem> */}
					</div>
					<div className='pb-20'>
						<h4 className='color-blue'>Change settings</h4>
						<FormItem label='Company Name'>
							{getFieldDecorator('name', { rules: [{ required: true, message: 'Please enter a company name' }] })(
								<Input prefix={<Icon type='solution' style={{ color: 'rgba(0,0,0,.25)' }} />} size='large' />
							)}
						</FormItem>
						<FormItem label='Organization Number'>
							{getFieldDecorator('organizationNumber', { rules: [{ required: true, message: 'Please enter an organization number' }] })(
								<Input prefix={<Icon type='idcard' style={{ color: 'rgba(0,0,0,.25)' }} />} size='large' />
							)}
						</FormItem>
						<FormItem label='VAT Number'>
							{getFieldDecorator('vatNumber', { rules: [{ required: true, message: 'Please enter a VAT number' }] })(
								<Input size='large' prefix={<Icon type='wallet' style={{ color: 'rgba(0,0,0,.25)' }} />} />
							)}
						</FormItem>
						<FormItem label='Address'>
							{getFieldDecorator('address1', { rules: [{ required: true, message: 'Please enter an address' }] })(
								<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} />
							)}
						</FormItem>
						<FormItem label='Address 2'>
							{getFieldDecorator('address2')(<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} />)}
						</FormItem>
						<FormItem label='Zip Code'>
							{getFieldDecorator('zipCode', { rules: [{ required: true, message: 'Please enter a zip code' }] })(
								<Input size='large' prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} />
							)}
						</FormItem>
						<FormItem label='City'>
							{getFieldDecorator('city', { rules: [{ required: true, message: 'Please enter a city' }] })(
								<Input prefix={<Icon type='home' style={{ color: 'rgba(0,0,0,.25)' }} />} size='large' />
							)}
						</FormItem>
						<FormItem label='Country'>
							{getFieldDecorator('country', { rules: [{ required: true, message: 'Please enter a country' }] })(
								<Select
									prefix={<Icon type='global' style={{ color: 'rgba(0,0,0,.25)' }} />}
									placeholder='Select country'
									size='large'
									filterOption={(input, option) => option.props.children[2].toLowerCase().indexOf(input.toLowerCase()) >= 0}
									optionFilterProp='children'
									showSearch
								>
									{countries.all
										.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
										.filter(({ status }) => status === 'assigned')
										.map((country) => (
											<Select.Option value={country.alpha2} key={country.alpha2}>
												{country.emoji} {country.name}
											</Select.Option>
										))}
								</Select>
							)}
						</FormItem>
					</div>
				</Form>
			</React.Fragment>
		);
	}
}

const WrappedAccountSettingsForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const propFields = props.fields || {};
		let formFields = {};
		Object.keys(propFields).forEach((propFieldKey) => {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		});

		return formFields;
	}
})(AccountSettingsForm);

export default WrappedAccountSettingsForm;
