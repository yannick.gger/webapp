import React from 'react';
import { Button, message } from 'antd';
import AccountSettingsForm from './form';
import { Mutation } from 'react-apollo';
import updatePublisher from 'graphql/update-publisher-settings.graphql';

class AccountSettings extends React.Component {
	state = {
		fields: {}
	};
	static getDerivedStateFromProps = (props, state) => {
		let propsFields = {};

		Object.keys(props.publisher)
			.filter((key) => ['name', 'organizationNumber', 'vatNumber', 'address1', 'address2', 'zipCode', 'country', 'city'].includes(key))
			.forEach((key) => {
				propsFields[key] = {
					value: props.publisher[key]
				};
			});

		return { fields: { ...propsFields, ...state.fields } };
	};

	handleSubmit = (updatePublisher, publisherId) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, { name, organizationNumber, vatNumber, address1, address2, zipCode, country, city }) => {
				if (err) return null;

				const graphQlValues = {
					name,
					organizationNumber,
					vatNumber,
					address1,
					address2,
					zipCode,
					country,
					city,
					id: publisherId
				};

				const res = await updatePublisher({ variables: graphQlValues });
				if (res.data.updatePublisher.errors.length > 0) {
					res.data.updatePublisher.errors.forEach((error) => message.error(error.message));
				} else {
					message.info('Settings updated');
				}
			});
		};
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	handleFormChange = (changedFields) => {
		this.setState(({ fields }) => ({
			fields: { ...fields, ...changedFields }
		}));
	};
	render() {
		const { publisher } = this.props;
		const { fields } = this.state;

		return (
			<Mutation mutation={updatePublisher}>
				{(updatePublisher, { loading }) => (
					<React.Fragment>
						<AccountSettingsForm publisher={publisher} fields={fields} wrappedComponentRef={this.saveFormRef} onChange={this.handleFormChange} />
						<div>
							<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(updatePublisher, publisher.id)}>
								Save
							</Button>
						</div>
					</React.Fragment>
				)}
			</Mutation>
		);
	}
}

export default AccountSettings;
