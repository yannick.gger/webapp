import styled from 'styled-components';

const Wrapper = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;

	.deauthorize-card {
		max-width: 900px;
		text-align: center;
	}
`;

const CardText = styled.div`
	max-width: 500px;
	margin: 0 auto;
`;

const StatusText = styled.p`
	font-weight: 700;
`;

export const Styled = {
	Wrapper,
	CardText,
	StatusText
};
