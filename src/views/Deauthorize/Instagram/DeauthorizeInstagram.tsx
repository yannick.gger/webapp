import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import FacebookAuthService from 'services/FacebookApi/Facebook-auth.service';
import { StatusCode } from 'services/Response.types';
import Grid from 'styles/grid/grid';
import MainContent from 'styles/layout/main-content';
import Row from 'styles/grid/row';
import Card from 'components/Card';
import { Styled } from './DeauthorizeInstagram.style';
import LoadingSpinner from 'components/LoadingSpinner';

/**
 * @todo i18next
 */
const DeauthorizeInstagram = () => {
	const location = useLocation();
	const [loading, setLoading] = useState(false);
	const [statusText, setStatusText] = useState('');

	const deauthorizeInstagram = () => {
		setLoading(true);
		if (location.pathname.includes('deauthorize')) {
			FacebookAuthService.deleteFacebookToken()
				.then((result) => {
					if (result === StatusCode.OK) {
						setStatusText('You have been deauthorized from Instagram. We will redirect you shortly...');
						setTimeout(() => {
							window.location.href = '/';
						}, 8000);
					} else {
						setStatusText('Oops! Something went wrong');
					}
					setLoading(false);
				})
				.catch(() => {
					setLoading(false);
				});
		}
	};

	useEffect(() => {
		deauthorizeInstagram();
	}, []);

	return (
		<MainContent>
			<Row>
				<Grid.Container>
					<Grid.Column lg={6} xxl={12}>
						<Styled.Wrapper>
							<Card className='deauthorize-card'>
								<Card.Body>
									<h1>Deauthorize you from Instagram</h1>
									<Styled.CardText>
										<p>Sorry to hear that you want to deauthorize your account from our platform. You can reauthenticate whenever you want in your settings.</p>
									</Styled.CardText>
									{loading && <LoadingSpinner size='sm' />}
									{!loading && (
										<div>
											<Styled.StatusText>{statusText}</Styled.StatusText>
										</div>
									)}
								</Card.Body>
							</Card>
						</Styled.Wrapper>
					</Grid.Column>
				</Grid.Container>
			</Row>
		</MainContent>
	);
};

export default DeauthorizeInstagram;
