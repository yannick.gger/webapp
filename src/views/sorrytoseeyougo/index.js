import React, { Component } from 'react';

import './styles.scss';

import illustration from './unsub.svg';
import logo from './collabs-logo-color.svg';

export default class SorryToSeeYouGoView extends Component {
	render() {
		document.title = `Sorry to see you go | Collabs`;
		return (
			<div className='sorry-to-see-you-go'>
				<img src={logo} alt='' />
				<div className='blue-box'>
					<h1>Sorry to see you go</h1>
					<p>No worries, you have now been unsubscribed from further emails.</p>
					<a href='/' className='btn'>
						Go back home
					</a>
					<img src={illustration} className='illustration' alt='' />
				</div>
			</div>
		);
	}
}
