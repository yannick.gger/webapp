import styled from 'styled-components';

import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';
import { Flex, Borders, Typo, Background, Spacing } from 'styles/utils/UtilityClasses';
import { guttersWithRem } from 'styles/variables/gutter';

const Wrapper = styled.section`
	${Flex};
	${Borders};
	${Typo};
	${Background};
	${Spacing};

	.back-link {
		color: ${colors.black};
	}

	h4 {
		margin: 3rem 0 0;
	}

	table {
		width: 100%;

		tbody > tr {
			background-color: ${colors.white};
			border-bottom: 2px solid ${colors.influencerList.borderColor};
		}

		th {
			font-family: ${typography.SecondaryFontFamiliy};
			font-weight: normal;
		}

		td,
		th {
			padding: 0.75rem 0.5rem;
		}
	}

	.status-joined {
		color: ${colors.success};
	}

	.status-declined {
		color: ${colors.warning};
	}

	.display-toggle {
		label {
			display: inline-box;
			border: 2px solid ${colors.borderGray};
			padding: 0.5rem 1rem;

			&.current-filter {
				background-color: ${colors.borderGray};
				color: ${colors.white};
			}

			input {
				appearance: none;
			}
		}
	}
`;

const Content = styled.div`
	padding: 0 2em;
	margin-left: auto;
	margin-right: auto;
	padding-bottom: 1.875rem;
`;

const ActionIcons = styled.div<{ disabled?: boolean }>`
	display: flex;
	column-gap: ${guttersWithRem.m};
`;

const ActionIconWrapper = styled.div<{ disabled?: boolean }>`
	cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};

	&[title] {
		position: relative;
	}

	&[title]:after {
		content: attr(title);
		position: absolute;
		bottom: -10px;
		width: max-content;
		opacity: 0;
		transition: opacity 0.1s;
		font-size: 0.5rem;
	}

	&[title]:hover:after {
		opacity: 1;
	}

	&.title-invite {
		&[title]:after {
			left: -25%;
		}
	}
	&.title-preview {
		&[title]:after {
			left: -50%;
		}
	}
	&.title-delete {
		&[title]:after {
			left: 0;
		}
	}
`;

export default {
	Wrapper,
	Content,
	ActionIcons,
	ActionIconWrapper
};
