import { MemoryRouter, Route } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';
import themes from '../../../styles/themes';
import InfluencerList from './InfluencerList';
import 'i18n';

test('it matches the snapshot', async () => {
	const { asFragment } = render(
		<MemoryRouter initialEntries={['/foo-bar/campaigns/1351']}>
			<ThemeProvider theme={themes.default}>
				<Route path='/:organizationSlug/campaigns/:campaignId'>
					<InfluencerList match={{ params: { campaignId: '1351', organizationSlug: 'syncro-group' } }} />
				</Route>
			</ThemeProvider>
		</MemoryRouter>
	);

	await screen.findByText('christ_jones');
	expect(asFragment()).toMatchSnapshot('Loading complete');
	userEvent.click(await screen.findByTestId('row-31818-action'));
	await screen.findAllByTestId('loading-spinner');
	await screen.findAllByTestId('mail-icon');
});
