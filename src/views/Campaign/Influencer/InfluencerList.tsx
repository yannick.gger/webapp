import React, { useContext, useState } from 'react';
import { isNil, map, capitalize } from 'lodash';

import CampaignsService from 'services/Campaign/Campaigns.service';
import Styled from './InfluencerList.style';
import withCampaign, { WithCampaignProps } from 'hocs/with-campaign';
import { Model } from 'json-api-models';
import { useTranslation } from 'react-i18next/hooks';
import PageHeader from 'components/PageHeader';
import InfluencerDetails from '../Dashboard/InfluencerDetails';
import LoadingSpinner from 'components/LoadingSpinner';
import CopyToClipboard from 'components/CopyToClipboard';
import Icon from 'components/Icon';
import { createClient } from 'shared/ApiClient/ApiClient';
import { ToastContext } from 'contexts';
import { AxiosError } from 'axios';
import { ErrorResponse } from 'types/http';
import { StatusCode } from 'services/Response.types';
/**
 * @todo i18next
 */
enum DISPLAY_TYPE {
	JOINED,
	GROUPS
}

function getStatusText(joined: boolean, declined: boolean): string {
	switch (true) {
		case joined:
			return 'joined';
		case declined:
			return 'declined';
		default:
			return 'not_joined';
	}
}

const InfluencerList = ({ models, campaign, campaignUrl, refresh }: WithCampaignProps) => {
	// Hooks
	const [displayType, setDisplayType] = useState(DISPLAY_TYPE.JOINED);
	const [isSendingInvitation, setSendingInvitation] = useState(false);
	const [isDeletingInfluencer, setIsDeletingInfluencer] = useState(false);
	const [t] = useTranslation('campaign');
	const { addToast } = useContext(ToastContext);
	const client = createClient();

	// Event Handlers
	const onDisplayToggle: React.ChangeEventHandler<HTMLInputElement> = ({ target }) => {
		setDisplayType(parseInt(target.value, 10) as DISPLAY_TYPE);
	};
	const sendInvite = (influencers: string[]) => {
		if (isSendingInvitation) {
			return;
		}

		setSendingInvitation(true);
		client
			.post(`/campaigns/${campaign.id}/invites`, { influencers })
			.then(() => {
				setSendingInvitation(false);
				addToast({ id: 'invite-sent', mode: 'success', message: 'Invitation(s) sent.' });
			})
			.catch(({ response }: AxiosError<ErrorResponse>) => {
				setSendingInvitation(false);

				if (response) {
					response.data.errors
						.slice(0, 3)
						.map(({ source }, index) => addToast({ id: `violation-${index}`, mode: 'error', message: `${source.parameter}: ${source.message}` }));
				}
			});
	};

	const removeInfluencerHandler = (deleteLink: string) => {
		if (isDeletingInfluencer) {
			return;
		}
		setIsDeletingInfluencer(true);
		CampaignsService.deleteCampaignInfluencer(deleteLink)
			.then((res) => {
				if (res === StatusCode.DELETE_SUCCESS) {
					refresh();
					addToast({ id: 'remove-influencer-success', message: 'Removed!', mode: 'success' });
				}
			})
			.catch((err) => {
				addToast({ id: 'remove-influencer-fail', message: 'Something went wrong!', mode: 'error' });
			})
			.finally(() => {
				setIsDeletingInfluencer(false);
			});
	};

	const influencers: Model[] = models.findAll('campaignInstagramOwner').filter(({ influencer }: Model) => null !== influencer && influencer.username);
	const items: Record<string, Model[]> = {};
	const joinedTitle = t('influencer.category.joined');

	switch (displayType) {
		case DISPLAY_TYPE.JOINED:
			items[joinedTitle] = influencers.filter(({ joined }) => joined);
			items[t('influencer.category.not_joined')] = influencers.filter(({ joined }) => !joined);
			break;

		case DISPLAY_TYPE.GROUPS:
			// TODO
			break;
		default:
			console.error("You think you're funny, huh?!?");
	}

	return (
		<Styled.Wrapper>
			<PageHeader headline='Influencers' homeUrl={campaignUrl} homeText='Back to campaign' />
			<Styled.Content>
				<div className='display-toggle d-flex justify-center'>
					<label htmlFor='display-status' className={displayType === DISPLAY_TYPE.JOINED ? 'current-filter' : 'cursor-pointer'}>
						Influencers
						<input type='radio' id='display-status' name='display-type' value={DISPLAY_TYPE.JOINED} onChange={onDisplayToggle} />
					</label>
					<label htmlFor='display-groups' className={displayType === DISPLAY_TYPE.GROUPS ? 'current-filter' : 'cursor-pointer'}>
						Groups
						<input type='radio' id='display-groups' name='display-type' value={DISPLAY_TYPE.GROUPS} onChange={onDisplayToggle} />
					</label>
				</div>
				{map(items, (list, name) =>
					list.length > 0 ? (
						<React.Fragment key={name}>
							<h4>{name}</h4>
							<table className='table'>
								<thead>
									<tr>
										<th colSpan={2}>
											<input type='checkbox' />
										</th>
										<th>Commission</th>
										<th>Status</th>
										{joinedTitle !== name && (
											<>
												<th>Invitation URL</th>
												<th>Email status</th>
											</>
										)}
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									{map(list, ({ id, influencer, joined, declined, campaignInstagramOwnerCommissions, links, invite }) => (
										<tr key={id}>
											<td>
												<input type='checkbox' />
											</td>
											<td>
												<InfluencerDetails influencer={influencer} />
											</td>
											<td>
												{campaignInstagramOwnerCommissions.map(({ commission }: Model) =>
													(commission.fixedAmount / 100).toLocaleString('en', { style: 'currency', currency: commission.fixedAmountCurrency })
												)}
											</td>
											<td className={`status-${getStatusText(joined, declined)}`}>{t(getStatusText(joined, declined))}</td>
											{joinedTitle !== name && (
												<>
													<td>{invite && invite.sent && !invite.used && <CopyToClipboard description='Invitation link' value={links.frontendJoin} />}</td>
													<td>{!isNil(invite) && capitalize(invite.emailStatus)}</td>
												</>
											)}
											<td>
												<Styled.ActionIcons>
													<Styled.ActionIconWrapper
														data-testid={`row-${id}-action`}
														onClick={() => sendInvite([influencer.id])}
														disabled={joined || declined}
														title='Invite now'
														className='title-invite'
													>
														{isSendingInvitation ? <LoadingSpinner size='sm' /> : <Icon testId={`mail-icon`} icon='mail' size='24' />}
													</Styled.ActionIconWrapper>
													<Styled.ActionIconWrapper
														onClick={() => {
															window.open(`preview/${influencer.id}`, '_blank');
														}}
														title='Show preview'
														className='title-preview'
													>
														<Icon icon='unhide' size='24' />
													</Styled.ActionIconWrapper>
													<Styled.ActionIconWrapper
														onClick={() => {
															removeInfluencerHandler(links.delete);
														}}
														disabled={joined}
														title='Delete'
														className='title-delete'
													>
														{isDeletingInfluencer ? <LoadingSpinner size='sm' /> : <Icon icon='trash-bin' size='24' />}
													</Styled.ActionIconWrapper>
												</Styled.ActionIcons>
											</td>
										</tr>
									))}
								</tbody>
							</table>
						</React.Fragment>
					) : null
				)}
			</Styled.Content>
		</Styled.Wrapper>
	);
};

export default withCampaign(InfluencerList);
