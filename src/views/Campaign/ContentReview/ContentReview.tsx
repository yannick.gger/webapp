import ContentReviewContainer from 'components/ContentReview/ContentReviewContainer';
import PageHeader from 'components/PageHeader';
import withTheme from 'hocs/with-theme';
import { useParams } from 'react-router-dom';
import { getCurrentOrganization } from 'shared/utils';
import Styled from './ContentReview.style';

const ContentReview = () => {
	const orgSlug = getCurrentOrganization();
	const { campaignId, reviewId } = useParams();
	const homeUrl = `/${orgSlug}/campaigns/${campaignId}`;

	return (
		<Styled.Wrapper>
			<PageHeader headline='Content review' homeUrl={homeUrl} homeText='Back to campaign' />
			<Styled.Content>
				<ContentReviewContainer campaignId={campaignId || ''} reviewId={reviewId || ''} />
			</Styled.Content>
		</Styled.Wrapper>
	);
};

export default withTheme(ContentReview, 'contentReview');
