import React from 'react';
import Styled from './InfluencerDetails.style';
import { Influencer } from './types';

type Props = {
	influencer: Influencer;
};

const InfluencerDetails = ({ influencer }: Props) => {
	return (
		<Styled.Wrapper>
			<img src={influencer.links.profilePictureUrl} alt={influencer.username} className='rounded' />
			<p>
				<strong>{influencer.username}</strong>
				<br />
				<small className='font-secondary'>{influencer.followersCount.toLocaleString('en', { notation: 'compact' })} Followers</small>
			</p>
		</Styled.Wrapper>
	);
};

export default InfluencerDetails;
