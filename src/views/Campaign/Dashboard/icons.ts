import List from 'assets/icons/icon-filter-1.svg';
import Product from 'assets/icons/icon-product.svg';
import Money from 'assets/icons/icon-money.svg';
import Folder from 'assets/icons/icon-folder.svg';
import Search from 'assets/icons/icon-search.svg';
import AssignmentTypeTiktok from 'assets/icons/icon-tiktok-discovery-card.svg';
import AssignmentTypeInstagramStory from 'assets/icons/icon-story.svg';
import AssignmentTypeInstagramReel from 'assets/icons/icon-reels-discovery-card.svg';
import AssignmentTypeInstagramPost from 'assets/icons/icon-post.svg';

export default {
	List,
	Product,
	Money,
	Folder,
	Search,
	AssignmentTypeTiktok,
	AssignmentTypeInstagramStory,
	AssignmentTypeInstagramReel,
	AssignmentTypeInstagramPost
};
