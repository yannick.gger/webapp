import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { JsonApiDocument, Model, Store } from 'json-api-models';

import LoadingSpinner from 'components/LoadingSpinner';
import { createClient } from 'shared/ApiClient/ApiClient';
import Icons from '../icons';

type Props = {
	onClose: () => void;
};

type TreeProps = {
	items: Model[];
};

type Folder = {
	name: string;
	children: Folder[];
};

const FolderTree = (props: TreeProps) => {
	const [showChildren, setShowChildren] = useState(false);
	const toggleDisplay = () => setShowChildren(!showChildren);

	return (
		<ul>
			{props.items.map(({ id, name, children }) => (
				<li key={id}>
					<span className='cursor-pointer' onClick={toggleDisplay}>
						<img src={children.length > 0 ? Icons.Folder : Icons.List} alt='Folder' /> {name}
					</span>

					{showChildren && children.length > 0 && <FolderTree items={children} />}
				</li>
			))}
		</ul>
	);
};

const AddInfluencer = ({ onClose }: Props) => {
	const [loading, setLoading] = useState(false);
	const [models, setModels] = useState(new Store());
	const [search, setSearch] = useState('');
	const client = createClient();

	const containsSearch = ({ name, children }: Model) => {
		if (name.toLowerCase().includes(search.toLowerCase())) {
			return true;
		}

		for (const child of children) {
			if (containsSearch(child)) {
				return true;
			}
		}

		return false;
	};

	useEffect(() => {
		const CancelToken = axios.CancelToken;
		const source = CancelToken.source();
		const promises = [
			client.get<JsonApiDocument>('/folders?includes=children', { cancelToken: source.token }),
			client.get<JsonApiDocument>('/lists?includes=folder', { cancelToken: source.token })
		];

		setLoading(true);
		Promise.all(promises)
			.then((responses) => {
				for (const { data } of responses) {
					models.sync(data);
				}

				setModels(models);
				setLoading(false);
			})
			.catch((e) => {
				if (!axios.isCancel(e)) {
					console.error('Unable to fetch folders: %O', e);
				}
				setLoading(false);
			});

		return () => source.cancel();
	}, []);

	const rootFolders = models
		.findAll('folder')
		.filter(({ parent }) => !parent)
		.filter(containsSearch);

	return (
		<div className='context-menu bg-white border-1'>
			<header>
				<button type='button' className='close cursor-pointer' aria-label='Close' onClick={() => onClose()}>
					<span aria-hidden='true'>&times;</span>
				</button>
				<h5>All list</h5>
			</header>
			<div className='cm-content'>
				<div className='search-bar d-flex'>
					<img src={Icons.Search} alt='Search' />
					<input type='search' placeholder='Find your list' value={search} onChange={({ target }) => setSearch(target.value)} />
				</div>
				<FolderTree items={rootFolders} />
				{loading && (
					<ul>
						<li>
							<LoadingSpinner size='sm' />
						</li>
					</ul>
				)}
			</div>
		</div>
	);
};

export default AddInfluencer;
