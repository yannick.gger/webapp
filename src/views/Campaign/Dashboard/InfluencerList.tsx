import { Model } from 'json-api-models';

import Styled from './InfluencerList.style';
import InfluencerDetails from './InfluencerDetails';

type Props = {
	items: Model[];
};
const InfluencerList = ({ items }: Props) => {
	return (
		<Styled.Wrapper>
			{items.map(({ id, influencer }) => (
				<Styled.Item key={id}>
					<InfluencerDetails influencer={influencer} />
				</Styled.Item>
			))}
		</Styled.Wrapper>
	);
};

export default InfluencerList;
