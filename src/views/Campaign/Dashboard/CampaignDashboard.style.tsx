import styled from 'styled-components';

import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import { Flex, Borders, Typo, Background, Spacing } from 'styles/utils/UtilityClasses';

const Wrapper = styled.section`
	width: 100%;
	max-width: 1920px;

	margin: 0 auto;
	padding-bottom: 30px;

	.campaign-actions {
		align-self: end;

		a {
			color: inherit;
		}
	}
`;

const Content = styled.div`
	padding: 0 ${guttersWithRem.xxl};
	margin-left: auto;
	margin-right: auto;

	.campaign-metrics {
		display: flex;
		justify-content: center;

		.label {
			font-weight: normal;
			font-family: ${typography.label.fontFamily};
			font-size: 1.25rem;
		}

		div {
			background-color: ${colors.campaignCard.themeBackground.dedication};
			font-weight: bold;
			text-align: center;
			padding: 1.5rem 3rem;
			margin: 0px 0.5rem;
		}

		margin-bottom: ${guttersWithRem.xxxl};
	}
	.campaign-next-steps > div {
		position: relative;
	}

	.btn-link {
		background: none;
		border: 0 none;
		color: ${colors.mainBlue};
		display: block;
		text-align: center;
		width: 100%;
		cursor: pointer;
	}

	${Flex};
	${Borders};
	${Typo};
	${Background};
	${Spacing};

	.context-menu {
		position: absolute;
		top: 3rem;
		left: 0;
		width: 100%;
		box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.25);

		ul {
			list-style: none;
			padding: 0.5rem;
		}

		li {
			font-weight: 500;
		}

		.cm-content {
			padding: 0.75rem;
		}

		.close {
			float: right;
			background: transparent;
			border: 0 none;
		}

		header {
			border-bottom: 1px solid ${colors.gray3};
			padding: 0.5rem;

			h5 {
				font-size: 0.9rem;
				margin: 0;
				font-weight: 500;
			}
		}

		.search-bar {
			border: 1px solid ${colors.gray4};
			padding: 0.25rem 0.5rem;
			img {
				margin-right: 0.25rem;
			}

			input {
				display: block;
				width: 100%;
				border: 0 none;

				&:focus,
				&:active {
					border: 0 none;
				}
			}
		}
	}
`;

export default {
	Content,
	Wrapper
};
