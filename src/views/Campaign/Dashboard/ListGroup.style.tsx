import styled from 'styled-components';

import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';

const Wrapper = styled.ul`
	padding-left: 0;
	margin-bottom: 0;
	list-style: none;
`;

const Item = styled.li`
	padding: ${guttersWithRem.s};
	margin-bottom: ${guttersWithRem.s};
	border: 1px solid ${colors.black};
	background: #fff;
	display: flex;
	justify-content: space-between;
	cursor: pointer;

	p {
		margin: 0;
		flex: 8;
	}

	.suffix {
		margin-left: auto;
	}

	.icon {
		margin-right: 0.5rem;
		flex: 1;
	}

	&:hover {
		box-shadow: 0 2px 8px rgba(0, 0, 0, 0.2);
	}
`;

export default {
	Wrapper,
	Item
};
