import React from 'react';

import Styled from './ListGroup.style';

type Props = {
	items: Item[];
};

type Item = {
	icon: React.ReactNode;
	title: React.ReactNode;
	subtitle?: React.ReactNode;
	suffix?: React.ReactNode;
	actions?: React.ReactNode;
	onClick?: (event: React.MouseEvent<HTMLLIElement>) => void;
};

const ListGroup = ({ items }: Props) => {
	return (
		<Styled.Wrapper>
			{items.map(({ icon, title, subtitle, suffix, actions, ...itemProps }, index) => (
				<Styled.Item key={index} {...itemProps}>
					<div className='icon'>{icon}</div>
					<p className='content'>
						<strong>{title}</strong>
						<br />
						{subtitle && <small className='font-secondary'>{subtitle}</small>}
					</p>
					{suffix && <div className='suffix'>{suffix}</div>}
					{actions}
				</Styled.Item>
			))}
		</Styled.Wrapper>
	);
};

export default ListGroup;
