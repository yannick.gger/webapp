import { Model } from 'json-api-models';

export interface CampaignStatistics {
	totalAssignments: number;
	completedAssignments: number;
	influencerTargetCount: number;
	influencersJoined: number;
	nextDeadline: string;
	contentWaitingForReview: number;
}

export enum AssignmentReviewStatus {
	REVIEW,
	APPROVED,
	REJECTED
}

export const CURRENCIES = ['SEK', 'EUR', 'NOK'] as const;
export type Currency = typeof CURRENCIES[number]; // Eq: 'SEK' | 'EUR' | 'NOK'

export enum ASSIGNMENT_TYPE {
	INSTAGRAM_POST = 'instagram_post',
	INSTAGRAM_STORY = 'instagram_story',
	INSTAGRAM_REEL = 'instagram_reel',
	TIKTOK_VIDEO = 'tiktok_video'
}

export enum NotificationState {
	UNKNOWN = 'unknown',
	CREATED = 'created',
	SENDING = 'sending',
	FAILED = 'failed',
	DELIVERED = 'delivered',
	READ = 'read',
	CLICKED = 'clicked'
}
export interface IAssignmentReview {
	text: string;
	createdAt: Date;
	updatedAt: Date;
	reviewedAt: Date;
	status: AssignmentReviewStatus;
	approvalComment: string;
	waivedDeadline: boolean;
	rejectComment: string;
}

export interface Influencer {
	id: string;
	links: {
		profilePictureUrl: string;
	};
	username: string;
	followersCount: number;
}

export class AssignmentReview extends Model {
	protected casts = {
		createdAt: Date,
		updatedAt: Date,
		reviewedAt: Date,
		waivedDeadline: Boolean
	};
}

export type ContentReview = IAssignmentReview & AssignmentReview;

export interface Commission {
	name: string; // Ex: 'Commission 1';
	maxNumberOfInfluencers: number; // Ex: 10;
	fixedAmount: number | string; // Ex: 1000000;
	fixedAmountCurrency: string; // Ex: 'SEK';
	revenueBasedAmount: number; // Ex: 2500;
	revenueBasedAmountCurrency: Currency; // Ex: 'SEK';
	revenueBasedPercentage: number; // Ex: 1.5;
	agencyFee: number; // Ex: 1000;
	agencyFeeCurrency: Currency; // Ex: 'SEK';
	agencyFeePercentage: number; // Ex: 0.75;
	influencers: string[]; // Ex: ['3c30f4bc-eade-493e-902a-222b62e8a497', '34d6a9b6-2288-4045-9bd9-e4e955971ff2']
}

export interface AssignmentDeadline {
	name: string;
	date: string;
}

export interface AssignmentGroup {
	name: string; // Ex: 'Date 1'
	start: string; // Ex: '2022-07-01'
	end: string; // Ex: '2022-07-31'
	influencerIds: string[]; // Ex: ['2136a9d9-7ed2-46fb-b893-419e2f7edf73', '742e23c7-3db0-4738-8044-b1ea80ac65b5']
	deadlines: AssignmentDeadline[];
}

export interface Assignment {
	name: string;
	type: ASSIGNMENT_TYPE;
	description: string;
	dos: string;
	donts: string;
	cta: string;
	groups: AssignmentGroup[];
}

export interface Product {
	name: string;
	link?: string;
	description?: string;
	value: number;
	currency: Currency;
	influencers: string[];
}
