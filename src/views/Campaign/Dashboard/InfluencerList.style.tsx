import styled from 'styled-components';

import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const Wrapper = styled.ul`
	padding-left: 0;
	margin-bottom: 0;
	list-style: none;
	font-family: ${typography.label};
`;

const Item = styled.li`
	border-bottom: 1px solid ${colors.gray2};
	margin-bottom: 0.5rem;
	padding-bottom: 0.5rem;
`;

export default {
	Wrapper,
	Item
};
