import styled from 'styled-components';
import typography from 'styles/variables/typography';

const Wrapper = styled.div`
	display: flex;
	align-items: center;

	img {
		height: 50px;
		width: 50px;
		border-radius: 100%;
		margin-right: 2rem;
	}

	p {
		margin: 0;
	}

	small {
		font-family: ${typography.SecondaryFontFamiliy};
	}
`;

export default { Wrapper };
