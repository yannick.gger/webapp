import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { useTranslation } from 'react-i18next/hooks';

import Styled from './CampaignDashboard.style';
import withCampaign, { WithCampaignProps } from 'hocs/with-campaign';
import { createClient } from 'shared/ApiClient/ApiClient';
import Grid from 'styles/grid/grid';
import ListGroup from './ListGroup';
import { Model } from 'json-api-models';
import Accordion from 'components/Accordion';
import InfluencerList from './InfluencerList';
import Icons from './icons';
import AddInfluencer from './ContextMenu/AddInfluencer';
import { JsonApiIdentifier } from 'json-api-models/src/types';
import IntegratedInboxCard from 'components/Dashboard/Components/IntegratedInboxCard';
import ContentReviewCard from 'components/ContentReview/Components/ContentReviewCard';
import ContentReviewCardStyle from 'components/ContentReview/Components/ContentReviewCard/ContentReviewCard.style';
import Card from 'components/Card';
import { apiToMoment } from 'utils';
import { assignmentTypeToIcon } from 'components/Assigment/Assignment';
import PageHeader from 'components/PageHeader';
import InfluencerPreviewList from 'components/InfluencerPreviewList';
import { Influencer } from './types';
import { isNil } from 'lodash';
import { AssignmentType } from 'components/Assigment/types';

// Modals
import CommissionForm from './Modal/CommissionForm';
import TaskForm from './Modal/TaskForm';
import ProductForm from './Modal/ProductForm';
import ModelViewModal, { Property } from './Modal/ModelViewModal';

function ensureInfluencerExists({ influencer }: Model): boolean {
	return null !== influencer && influencer.username;
}

function getViewModalProperties(type: string): Property[] {
	switch (type) {
		case 'campaignProduct':
			return [{ path: 'name' }, { path: 'description' }, { path: 'link', formatter: 'link' }, { path: 'value', formatter: 'price' }];
		case 'assignment':
			return [{ path: 'name' }, { path: 'description' }, { path: 'dos' }, { path: 'donts' }, { path: 'cta' }];
		case 'commission':
			return [
				{ path: 'revenueBasedAmount', formatter: 'amount' },
				{ path: 'revenueBasedAmountCurrency' },
				{ path: 'fixedAmount', formatter: 'amount' },
				{ path: 'fixedAmountCurrency' },
				{ path: 'revenueBasedPercentage', formatter: 'percentage' },
				{ path: 'maxNumberOfInfluencers' },
				{ path: 'agencyFee' },
				{ path: 'agencyFeeCurrency' },
				{ path: 'agencyFeePercentage', formatter: 'percentage' }
			];
	}
	return [];
}

function getDeleteUrl(campaignId: string, entityId: string, type: string): string {
	let subPath = type;

	switch (type) {
		case 'campaignProduct':
			subPath = 'products';
			break;
		case 'assignment':
			subPath = 'assignments';
			break;
		case 'commission':
			subPath = 'commissions';
			break;
	}
	return `/campaigns/${campaignId}/${subPath}/${entityId}`;
}
/**
 * @todo i18next
 */
const CampaignDashboard = ({ campaignUrl, campaign, models, refresh }: WithCampaignProps) => {
	const { name, campaignStatistics } = campaign;
	const { totalAssignments, completedAssignments, influencerTargetCount, influencersJoined, nextDeadline, contentWaitingForReview } = campaignStatistics;
	const influencers: Model[] = models.findAll('campaignInstagramOwner').filter(ensureInfluencerExists);
	const joinedInfluencers = influencers.filter(({ joined }) => joined).slice(0, 3);
	const pendingInfluencers = influencers.filter(({ joined }) => !joined).slice(0, 3);
	const [showFolders, setShowFolders] = useState(false);
	const [modalEditIdentifier, setModalEditIdentifier] = useState<JsonApiIdentifier | null>(null);
	const [viewModalIdentifier, setViewModalIdentifier] = useState<JsonApiIdentifier | null>(null);
	const [t] = useTranslation('campaign');
	const clearModalIdentifier = () => setModalEditIdentifier(null);
	const client = createClient();

	let productInitialValues, assignmentsInitialValues, commissionInitialValues;

	if (!isNil(modalEditIdentifier)) {
		switch (modalEditIdentifier.type) {
			case 'campaignProduct':
				productInitialValues = models.find(modalEditIdentifier);
				break;
			case 'assignment':
				assignmentsInitialValues = models.find(modalEditIdentifier);
				break;
			case 'commission':
				commissionInitialValues = models.find(modalEditIdentifier);
				break;
		}
	}

	return (
		<Styled.Wrapper>
			<PageHeader headline={name} homeUrl='/'>
				<div className='campaign-actions'>
					<Link to={`${campaign.id}/edit`}>Edit campaign</Link>
				</div>
			</PageHeader>
			<Styled.Content>
				<div className='campaign-metrics'>
					<div>
						<p className='label'>Spots taken</p>
						{influencersJoined} / {influencerTargetCount}
					</div>
					<div>
						<p className='label'>Assignments</p>
						{completedAssignments} / {totalAssignments}
					</div>
					<div>
						<p className='label'>Next deadline</p>
						{nextDeadline ? moment(nextDeadline).format('D MMM') : '-'}
					</div>
					<div>
						<p className='label'>Content to review</p>
						{contentWaitingForReview}
					</div>
				</div>
				{!isNil(viewModalIdentifier) && (
					<ModelViewModal
						model={models.find(viewModalIdentifier)}
						properties={getViewModalProperties(viewModalIdentifier.type)}
						actions={[
							{ label: 'Cancel', onClick: () => setViewModalIdentifier(null) },
							{
								label: 'Delete',
								onClick: () => {
									// @todo i18next
									confirm("You're sure about that?") &&
										client.delete(getDeleteUrl(campaign.id, viewModalIdentifier.id, viewModalIdentifier.type)).then(() => refresh());
								}
							},
							{
								label: 'Edit',
								onClick: () => {
									setModalEditIdentifier(viewModalIdentifier);
									setViewModalIdentifier(null);
								}
							}
						]}
					/>
				)}
				<Grid.Container className='campaign-next-steps'>
					<Grid.Column lg={3}>
						<button className='btn-link' onClick={() => setShowFolders(!showFolders)}>
							+ Add Influencers
						</button>
						<hr />
						{showFolders && <AddInfluencer onClose={() => setShowFolders(false)} />}
						<ListGroup
							items={models
								.findAll('assignmentGroups')
								.slice(0, 3)
								.map((group) => ({
									// @todo i18next
									icon: <img src={Icons.List} alt='Influencers group' />,
									title: group.name,
									suffix: <InfluencerPreviewList groups={[group]} />
								}))}
						/>
					</Grid.Column>
					<Grid.Column lg={3}>
						<TaskForm
							campaignId={campaign.id}
							influencers={influencers.map(({ influencer }) => influencer as Influencer)}
							initialValues={assignmentsInitialValues}
							onAdd={() => {
								clearModalIdentifier();
								refresh();
							}}
							onClose={clearModalIdentifier}
						/>
						<hr />
						<ListGroup
							items={models.findAll('assignment').map(({ id, name, createdAt, endAt, type, groups }) => ({
								icon: assignmentTypeToIcon(type as AssignmentType),
								title: name,
								subtitle: apiToMoment(createdAt).format('YYYY-MM-DD') + (endAt ? ' - ' + apiToMoment(endAt).format('YYYY-MM-DD') : ''),
								suffix: <InfluencerPreviewList groups={groups || []} />,
								onClick: () => setViewModalIdentifier({ id, type: 'assignment' })
							}))}
						/>
					</Grid.Column>
					<Grid.Column lg={3}>
						<ProductForm
							campaignId={campaign.id}
							onAdd={() => {
								clearModalIdentifier();
								refresh();
							}}
							onClose={clearModalIdentifier}
							influencers={influencers.map(({ influencer }) => influencer as Influencer)}
							initialValues={productInitialValues}
						/>
						<hr />
						<ListGroup
							items={models.findAll('campaignProduct').map(({ id, name, influencers, value }) => ({
								icon: <img src={Icons.Product} alt='Product' />,
								title: name,
								subtitle: <>Value: {(value.amount / 100).toLocaleString('en', { currency: value.currency, style: 'currency' })}</>,
								suffix: <InfluencerPreviewList groups={influencers || []} />,
								onClick: () => setViewModalIdentifier({ id, type: 'campaignProduct' })
							}))}
						/>
					</Grid.Column>
					<Grid.Column lg={3}>
						<CommissionForm
							campaignId={campaign.id}
							influencers={influencers.map(({ influencer }) => influencer as Influencer)}
							initialValues={commissionInitialValues}
							onAdd={() => {
								clearModalIdentifier();
								refresh();
							}}
							onClose={clearModalIdentifier}
						/>
						<hr />
						<ListGroup
							items={models.findAll('commission').map(({ id, name, fixedAmount, fixedAmountCurrency, campaignInstagramOwnerCommissions: owners }) => ({
								icon: <img src={Icons.Money} alt='Commission' />,
								title: name,
								subtitle: fixedAmount > 0 ? <>Value: {(fixedAmount / 100).toLocaleString('en', { currency: fixedAmountCurrency, style: 'currency' })}</> : null,
								suffix: <InfluencerPreviewList groups={owners.map(({ influencer }: Model) => influencer)} />,
								onClick: () => setViewModalIdentifier({ id, type: 'commission' })
							}))}
						/>
					</Grid.Column>
				</Grid.Container>
				<hr />
				<Grid.Container>
					<Grid.Column lg={4}>
						<IntegratedInboxCard campaignId={Number(campaign.id)} />
					</Grid.Column>
					<Grid.Column lg={4}>
						<ContentReviewCard campaignId={campaign.id} />
					</Grid.Column>
					<Grid.Column lg={4}>
						<ContentReviewCardStyle.CardWrapper>
							<Card.Header>
								<ContentReviewCardStyle.Heading>Influencers</ContentReviewCardStyle.Heading>
							</Card.Header>
							<Card.Body>
								<ContentReviewCardStyle.CardContent>
									<div className='w-100'>
										<Accordion title={t('influencer.category.joined')} open={joinedInfluencers.length > 0} className='w-100'>
											<InfluencerList items={joinedInfluencers} />
										</Accordion>
										<Accordion title={t('influencer.category.not_joined')} open={pendingInfluencers.length > 0} className='w-100'>
											<InfluencerList items={pendingInfluencers} />
										</Accordion>
									</div>
								</ContentReviewCardStyle.CardContent>
							</Card.Body>
							<Card.Footer>
								<ContentReviewCardStyle.FooterLinks>
									<Link to={`${campaignUrl}/influencers`}>See all</Link>
								</ContentReviewCardStyle.FooterLinks>
							</Card.Footer>
						</ContentReviewCardStyle.CardWrapper>
					</Grid.Column>
				</Grid.Container>
			</Styled.Content>
		</Styled.Wrapper>
	);
};

export default withCampaign(CampaignDashboard);
