import React, { useEffect, useState } from 'react';
import { ErrorMessage, Field, FieldArray, Formik } from 'formik';
import { AxiosRequestConfig } from 'axios';
import { isNil } from 'lodash';
import * as Yup from 'yup';

import Modal from 'components/Modal';
import { Button } from 'components/Button';
import Textarea from 'components/Form/Elements/Textarea';
import BulletList from 'components/Form/Elements/BulletList';
import { createClient } from 'shared/ApiClient/ApiClient';
import { Assignment, ASSIGNMENT_TYPE, AssignmentDeadline, AssignmentGroup } from '../types';
import Styled from './ModalForm.style';
import FormGroup from './FormGroup';
import { ModalProps } from './types';
import { Model } from 'json-api-models';
import moment from 'moment';
import Icon from 'components/Icon';
import handleErrors from 'utils/formik_error_handler';

function createAssignmentDeadline(): AssignmentDeadline {
	return {
		name: '',
		date: ''
	};
}

function createAssignmentGroup(): AssignmentGroup {
	return {
		name: '',
		start: '',
		end: '',
		influencerIds: [],
		deadlines: []
	};
}

const INITIAL_VALUES: Assignment = {
	name: '',
	type: ASSIGNMENT_TYPE.INSTAGRAM_POST,
	description: '',
	dos: '',
	donts: '',
	cta: '',
	groups: [createAssignmentGroup()]
};

const VALIDATION_SCHEMA = Yup.object({
	name: Yup.string().required(),
	description: Yup.string().required(),
	dos: Yup.string().required(),
	donts: Yup.string().required(),
	cta: Yup.string(),
	groups: Yup.array()
		.of(
			Yup.object({
				name: Yup.string().required(),
				start: Yup.date(),
				end: Yup.date(),
				influencerIds: Yup.array()
					.of(Yup.string())
					.max(100),
				deadlines: Yup.array()
					.of(
						Yup.object({
							name: Yup.string().required(),
							date: Yup.date()
						})
					)
					.max(100)
			})
		)
		.min(1)
		.max(100)
});

function modelToAssignment(model: Model): Assignment {
	const { name, type, description, groups, dos, donts, cta } = model;

	return {
		name,
		type: type as ASSIGNMENT_TYPE,
		description,
		dos,
		donts,
		cta,
		groups: (groups || []).map(({ id, name, start, end, deadlines, campaignInstagramOwnerAssignments }: Model) => ({
			id,
			name,
			start: moment(start).format('YYYY-MM-DD'),
			end: moment(end).format('YYYY-MM-DD'),
			influencerIds: campaignInstagramOwnerAssignments.filter((model: Model) => !isNil(model.influencer)).map(({ influencer }: Model) => influencer.id),
			deadlines: deadlines.map(({ id, name, date }: Model) => ({
				id,
				name,
				date: moment(date).format('YYYY-MM-DD')
			}))
		}))
	};
}

/**
 * @todo i18next
 */
const TaskForm = ({ campaignId, influencers, onAdd, initialValues, onClose }: ModalProps) => {
	const [opened, setOpened] = useState(false);
	const client = createClient();

	useEffect(() => {
		if (!isNil(initialValues)) {
			setOpened(true);
		}
	}, [initialValues]);

	return (
		<>
			<button className='btn-link' onClick={() => setOpened(true)} data-testid='add-assignment-modal'>
				+ Add Task
			</button>
			<Modal
				open={opened}
				handleClose={() => {
					setOpened(false);
					onClose && onClose();
				}}
				size='md'
			>
				<Modal.Header>
					<h4>{initialValues ? `Edit ${initialValues.name}` : 'New task'}</h4>
				</Modal.Header>
				<Modal.Body>
					<Formik
						initialValues={initialValues ? modelToAssignment(initialValues) : INITIAL_VALUES}
						enableReinitialize
						validationSchema={VALIDATION_SCHEMA}
						onSubmit={async (values, { setErrors }) => {
							const config: AxiosRequestConfig = {
								url: `/campaigns/${campaignId}/assignments`,
								method: 'POST',
								data: values
							};

							if (!isNil(initialValues)) {
								config.url += `/${initialValues.id}`;
								config.method = 'PATCH';
							}

							try {
								await client.request(config);
								setOpened(false);
								onAdd();
							} catch (e) {
								handleErrors(e, setErrors);
							}
						}}
					>
						{({ isSubmitting, isValid, setFieldValue, values }) => (
							<Styled.Form noValidate>
								<FormGroup label='Title' name='name' required autocomplete='off' />
								<FormGroup label='Format' name='type' required as='select'>
									<option value={ASSIGNMENT_TYPE.INSTAGRAM_STORY}>Instagram Story</option>
									<option value={ASSIGNMENT_TYPE.INSTAGRAM_POST}>Instagram Post</option>
									<option value={ASSIGNMENT_TYPE.INSTAGRAM_REEL}>Instagram Reel</option>
									<option value={ASSIGNMENT_TYPE.TIKTOK_VIDEO}>TikTok video</option>
								</FormGroup>
								<div className='form-group'>
									<Textarea name='description' heading='Description' />
								</div>
								<div className='form-group'>
									<BulletList name='dos' label="Do's" value={values.dos} onChange={(val) => setFieldValue('dos', val)} />
									<ErrorMessage name='dos' className='validation-error' component='p' />
								</div>
								<div className='form-group'>
									<BulletList name='donts' label="Dont's" value={values.donts} onChange={(val) => setFieldValue('donts', val)} />
									<ErrorMessage name='donts' className='validation-error' component='p' />
								</div>
								<div className='form-group'>
									<Textarea
										name='cta'
										heading={
											<>
												CTA <small>(Optional)</small>
											</>
										}
									/>
								</div>
								<FieldArray name='groups'>
									{({ remove, push }) => (
										<>
											{values.groups.map((group, groupIndex) => (
												<React.Fragment key={groupIndex}>
													<div className='d-flex justify-between'>
														<h3>Date</h3>
														{groupIndex > 0 && (
															<div className='d-flex justify-between form-group'>
																<button type='button' className='btn-link' onClick={() => remove(groupIndex)}>
																	- Remove Date
																</button>
															</div>
														)}
													</div>

													<FormGroup label='Title' name={`groups.${groupIndex}.name`} placeholder={`Date ${groupIndex + 1}`} required />

													<div className='d-flex justify-between'>
														<FormGroup label='Date start' name={`groups.${groupIndex}.start`} type='date' required />
														<FormGroup label='Date end' name={`groups.${groupIndex}.end`} type='date' required />
													</div>
													<div className='form-group'>
														<label htmlFor={`groups.${groupIndex}.influencerIds`}>Assign Influencer(s)</label>
														<div className='input-group d-flex'>
															<button type='button' onClick={() => setFieldValue(`groups.${groupIndex}.influencerIds`, influencers.map(({ id }) => id))}>
																Select all
															</button>
															<Field
																id={`groups.${groupIndex}.influencerIds`}
																data-testid={`input-groups.${groupIndex}.influencerIds`}
																name={`groups.${groupIndex}.influencerIds`}
																as='select'
																multiple
																className='d-block'
																required
															>
																{influencers.map((influencer) => (
																	<option value={influencer.id} key={influencer.id}>
																		@{influencer.username}
																	</option>
																))}
															</Field>
														</div>
														<ErrorMessage name={`groups.${groupIndex}.influencerIds`} className='validation-error' component='p' />
													</div>
													<h5>Deadlines</h5>
													<FieldArray name={`groups.${groupIndex}.deadlines`}>
														{({ remove: removeDeadline, push: addDeadline }) => (
															<>
																{values.groups[groupIndex].deadlines.map((deadline, deadlineIndex) => (
																	<div className='d-flex justify-between' key={deadlineIndex}>
																		<FormGroup
																			label='Title'
																			name={`groups.${groupIndex}.deadlines.${deadlineIndex}.name`}
																			placeholder={`Deadline ${deadlineIndex + 1}`}
																		/>
																		<FormGroup label='Date' name={`groups.${groupIndex}.deadlines.${deadlineIndex}.date`} type='date' />
																		<button type='button' className='btn-link' onClick={() => removeDeadline(deadlineIndex)}>
																			<Icon icon='cancel' size='16' />
																		</button>
																	</div>
																))}
																<div className='d-flex justify-between form-group'>
																	<button type='button' className='btn-link' onClick={() => addDeadline(createAssignmentDeadline())}>
																		+ Add Deadline
																	</button>
																</div>
																<hr />
															</>
														)}
													</FieldArray>
												</React.Fragment>
											))}
											<button type='button' className='btn-link' onClick={() => push(createAssignmentGroup())}>
												Add Date
											</button>
										</>
									)}
								</FieldArray>
								<hr />
								<div className='d-flex justify-between'>
									<Button type='button' onClick={() => setOpened(false)}>
										Cancel
									</Button>
									<Button disabled={isSubmitting || !isValid} type='submit'>
										Save
									</Button>
								</div>
							</Styled.Form>
						)}
					</Formik>
				</Modal.Body>
			</Modal>
		</>
	);
};

export default TaskForm;
