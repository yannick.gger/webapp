import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';
import TaskForm from './TaskForm';

const INFLUENCERS = [
	{
		id: '1234-5678',
		links: {
			profilePictureUrl: 'http://some.cdn.com/bwayne.png'
		},
		username: 'bruce_wayne',
		followersCount: 69420
	},
	{
		id: '91011-1213',
		links: {
			profilePictureUrl: 'http://some.cdn.com/bbanner.png'
		},
		username: 'bruce_banner',
		followersCount: 42000
	}
];

test('it matches the snapshot', async () => {
	const callback = jest.fn();
	const { asFragment } = render(
		<ThemeProvider theme={themes.default}>
			<TaskForm campaignId={1351} influencers={INFLUENCERS} onAdd={callback} />
		</ThemeProvider>
	);

	const inputValues: Record<string, string> = {
		name: 'My assignment',
		description: 'My description',
		'dos[0]': 'Do this',
		'donts[0]': 'Dont do this',
		'groups.0.name': 'Date 1',
		'groups.0.start': '2022-07-01',
		'groups.0.end': '2022-07-31'
	};

	expect(asFragment()).toMatchSnapshot('Initial state');

	for (const name in inputValues) {
		userEvent.type(await screen.findByTestId(`form-control-${name}`), inputValues[name]);
	}
	userEvent.click(await screen.findByText('Select all'));

	userEvent.click(await screen.findByText('Save'));
	await waitFor(() => expect(callback).toHaveBeenCalled());
});
