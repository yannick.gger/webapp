import { Influencer } from '../types';
import { Model } from 'json-api-models';

export interface ModalProps {
	campaignId: string | number;
	influencers: Influencer[];
	initialValues?: Model;
	onClose?: () => void;
	onAdd: () => void;
}
