import styled from 'styled-components';
import { Form } from 'formik';

import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import { Flex, Borders } from 'styles/utils/UtilityClasses';

const StyledForm = styled(Form)`
	${Flex};
	${Borders};

	max-height: 50vh;

	label {
		font-family: ${typography.SecondaryFontFamiliy};
		font-size: 0.8rem;
	}

	input,
	select {
		padding: 0.5rem 1rem;
		border: 0 none;
		background: transparent;
	}

	.input-group {
		border: 1px solid ${colors.borderColor};
	}

	.d-block {
		display: block;
		width: 100%;
	}

	.validation-error {
		color: ${colors.warning};
	}

	.form-group {
		margin-bottom: ${guttersWithRem.l};
	}

	.btn-link {
		background: none;
		border: 0 none;
		color: ${colors.mainBlue};
		cursor: pointer;
	}
`;

export default {
	Form: StyledForm
};
