import React, {useEffect, useState} from 'react';
import { Formik, Field, ErrorMessage } from 'formik';
import { Method} from 'axios';
import { isNil } from 'lodash';
import * as Yup from 'yup';

import Modal from 'components/Modal';
import { Button } from 'components/Button';
import Textarea from 'components/Form/Elements/Textarea';
import { createClient } from 'shared/ApiClient/ApiClient';
import { CURRENCIES, Product } from '../types';
import Styled from './ModalForm.style';
import handleErrors from 'utils/formik_error_handler';
import FormGroup from './FormGroup';
import icons from '../icons';
import { ModalProps } from './types';
import { Model } from 'json-api-models';

const INITIAL_VALUES: Product = {
	name: '',
	description: '',
	link: '',
	value: 0,
	currency: 'SEK',
	influencers: []
};

const VALIDATION_SCHEMA = Yup.object({
	name: Yup.string().required(),
	description: Yup.string(),
	link: Yup.string().url(),
	value: Yup.number(),
	currency: Yup.string().oneOf(Array.from(CURRENCIES))
});

function modelToDraft(model: Model): Product {
	const { name, description, link, value, influencers } = model;
	const { amount, currency } = value;

	return {
		name: name || '',
		description: description || '',
		link: link || '',
		currency: currency || '',
		value: amount / 100,
		// eslint-disable-next-line prettier/prettier
		influencers: influencers?.map(({ id }: Model) => id) ?? []
	};
}

/**
 * @todo i18next
 */
const ProductForm = ({ campaignId, onAdd, influencers, initialValues, onClose }: ModalProps) => {
	const [opened, setOpened] = useState(false);
	const values = isNil(initialValues) ? INITIAL_VALUES : modelToDraft(initialValues);
	const client = createClient();

	useEffect(() => {
		if (!isNil(initialValues)) {
			setOpened(true);
		}
	}, [initialValues])

	return (
		<>
			<button className='btn-link' onClick={() => setOpened(true)} data-testid='add-product-modal'>
				+ Add Product
			</button>
			<Modal open={opened} handleClose={() => setOpened(false)} size='xs'>
				<Modal.Header>
					<h4>
						<img src={icons.Product} alt='Product' />
						&nbsp; {isNil(initialValues) ? 'New product'  : `Edit ${values.name}`}
					</h4>
				</Modal.Header>
				<Modal.Body>
					<Formik
						initialValues={values}
						validationSchema={VALIDATION_SCHEMA}
						enableReinitialize
						onSubmit={async ({ value, ...data }, { setErrors }) => {
							let path = `/campaigns/${campaignId}/products`;
							let method : Method = 'POST';

							if (!isNil(initialValues)) {
								path += `/${initialValues.id}`;
								method = 'PATCH';
							}

							try {
								await client.request({
									method,
									url: path,
									data: { value: parseInt((value * 100).toFixed(0)), ...data }
								});
								setOpened(false);
								onAdd();
							} catch (e) {
								handleErrors(e, setErrors);
							}
						}}
					>
						{({ isSubmitting, isValid, setFieldValue }) => (
							<Styled.Form noValidate>
								<FormGroup label='Product name' name='name' required autocomplete="off"/>
								<FormGroup label='Product url' name='link' type='url' autocomplete="off" />
								<div className='form-group'>
									<Textarea name='description' heading='Description' />
								</div>
								<div className='form-group'>
									<label htmlFor='fc-value'>Product value</label>
									<div className='form-control-group d-flex justify-between'>
										<Field id='fc-currency' data-testid='form-control-currency' name='currency' as='select'>
											{CURRENCIES.map((currency) => (
												<option value={currency} key={currency}>
													{currency}
												</option>
											))}
										</Field>
										<Field id='fc-value' data-testid='form-control-value' name='value' type='number' placeholder='Set amount' className='d-block' />
										<ErrorMessage name='value' className='validation-error' component='p' />
										<ErrorMessage name='currency' className='validation-error' component='p' />
									</div>
									<div className='help-text'>All price is Incl. VAT (25%).</div>
								</div>
								<div className='form-group'>
									<label htmlFor='fc-influencers'>Assign influencer(s)</label>
									<div className='input-group d-flex'>
										<button type='button' onClick={() => setFieldValue('influencers', influencers.map(({ id }) => id))}>
											Select all
										</button>
										<Field id='fc-influencers' data-testid='input-influencers' name='influencers' as='select' multiple className='d-block'>
											{influencers.map((influencer) => (
												<option value={influencer.id} key={influencer.id}>
													@{influencer.username}
												</option>
											))}
										</Field>
									</div>
									<ErrorMessage name='influencers' className='validation-error' component='p' />
								</div>
								<hr />
								<div className='d-flex justify-between'>
									<Button type='button' onClick={() => {
										setOpened(false);
										onClose && onClose();
									}}>
										Cancel
									</Button>
									<Button disabled={isSubmitting || !isValid} type='submit'>
										Save
									</Button>
								</div>
							</Styled.Form>
						)}
					</Formik>
				</Modal.Body>
			</Modal>
		</>
	);
};

export default ProductForm;
