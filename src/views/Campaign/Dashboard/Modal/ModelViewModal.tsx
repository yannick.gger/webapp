import React from 'react';
import { Model } from 'json-api-models';
import { get, isNil } from 'lodash';
import Modal from 'components/Modal';
import { IButtonProps } from 'components/Button/types';
import { useTranslation } from 'react-i18next/hooks';
import Styled from './ModelViewModal.style';

type Action = { label: string } & IButtonProps;
type Formatter = 'price' | 'link' | 'amount' | 'percentage';

export type Property = {
	path: string;
	formatter?: Formatter;
};

type Props = {
	model: Model;
	properties: Property[];
	actions: Action[];
};

function formatValue(value: any, formatter?: Formatter): React.ReactNode {
	switch (formatter) {
		case 'price':
			return (value.amount / 100).toLocaleString('en', { currency: value.currency, style: 'currency' });

		case 'link':
			return <a href={value}>{value}</a>;

		case 'amount':
			return (value / 100).toLocaleString();

		case 'percentage':
			return (value / 100).toLocaleString('en', { style: 'percent', minimumFractionDigits: 2 });

		default:
			return value;
	}
}

const ModelViewModal = ({ model, properties, actions }: Props) => {
	const [t] = useTranslation('campaign');

	return (
		<Modal open size='xs'>
			<Modal.Header>
				<h4>{model.name}</h4>
			</Modal.Header>
			<Modal.Body>
				<dl>
					{properties.map(({ path, formatter }) => {
						const value = get(model, path);

						return isNil(value) ? null : (
							<React.Fragment key={path}>
								<dt>{t(`modal.view.${path}`)}</dt>
								<dd>{formatValue(value, formatter)}</dd>
							</React.Fragment>
						);
					})}
				</dl>
			</Modal.Body>
			<Modal.Footer>
				<Styled.ButtonsWrapper>
					{actions.map(({ label, ...btnProps }, index) => {
						if (label === 'Cancel') {
							return (
								<Styled.LeftButton key={index} {...btnProps} size='sm'>
									{label}
								</Styled.LeftButton>
							);
						} else {
							return (
								<Styled.RightButton key={index} {...btnProps} size='sm' className={label === 'Delete' ? 'delete' : 'edit'}>
									{label}
								</Styled.RightButton>
							);
						}
					})}
				</Styled.ButtonsWrapper>
			</Modal.Footer>
		</Modal>
	);
};

export default ModelViewModal;
