import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';
import CommissionForm from './CommissionForm';

const INFLUENCERS = [
	{
		id: '1234-5678',
		links: {
			profilePictureUrl: 'http://some.cdn.com/bwayne.png'
		},
		username: 'bruce_wayne',
		followersCount: 69420
	},
	{
		id: '91011-1213',
		links: {
			profilePictureUrl: 'http://some.cdn.com/bbanner.png'
		},
		username: 'bruce_banner',
		followersCount: 42000
	}
];

test('it matches the snapshot', async () => {
	const callback = jest.fn();
	const { asFragment } = render(
		<ThemeProvider theme={themes.default}>
			<CommissionForm campaignId={1351} influencers={INFLUENCERS} onAdd={callback} />
		</ThemeProvider>
	);

	const inputValues: Record<string, string> = {
		name: 'Custom name',
		fixedAmount: '1234'
	};

	expect(asFragment()).toMatchSnapshot('Initial state');
	userEvent.selectOptions(await screen.findByTestId('input-fixedAmountCurrency'), 'EUR');

	for (const name in inputValues) {
		userEvent.type(await screen.findByTestId(`input-${name}`), inputValues[name]);
	}
	userEvent.click(await screen.findByText('Select all'));

	const influencersSelect = await screen.findByTestId<HTMLSelectElement>('input-influencers');
	for (const option of Array.from(influencersSelect.options)) {
		expect(option.selected).toBeTruthy();
	}

	userEvent.click(await screen.findByText('Save'));
	await waitFor(() => expect(callback).toHaveBeenCalled());
});
