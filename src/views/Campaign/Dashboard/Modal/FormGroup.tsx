import { Field, ErrorMessage, FieldAttributes } from 'formik';

type Props = {
	label: React.ReactNode;
};

const FormGroup = ({ label, children, ...props }: Props & FieldAttributes<any>) => {
	const identifier = props.name || props.id;

	return (
		<div className='form-group'>
			<label htmlFor={`fc-${identifier}`}>{label}</label>
			<Field id={`fc-${identifier}`} data-testid={`form-control-${identifier}`} name={`${identifier}`} className='border-1 d-block' {...props}>
				{children}
			</Field>
			<ErrorMessage name={identifier} className='validation-error' component='p' />
		</div>
	);
};

export default FormGroup;
