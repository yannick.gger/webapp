import React, { useEffect, useState } from 'react';
import { Formik, ErrorMessage, Field } from 'formik';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { isNil } from 'lodash';

import Modal from 'components/Modal';
import { Button } from 'components/Button';
import { createClient } from 'shared/ApiClient/ApiClient';
import { Commission, CURRENCIES } from '../types';
import icons from '../icons';
import Styled from './ModalForm.style';
import { ErrorResponse } from 'types/http';
import { ModalProps } from './types';
import { Model } from 'json-api-models';

type CommissionDraft = Partial<Commission>;

const INITIAL_VALUES: CommissionDraft = {
	name: 'Commission 1',
	fixedAmount: '',
	fixedAmountCurrency: CURRENCIES[0],
	influencers: []
};

function modelToCommission(model: Model): CommissionDraft {
	const { name, fixedAmount, fixedAmountCurrency, campaignInstagramOwnerCommissions } = model;

	return {
		name,
		fixedAmount: fixedAmount / 100,
		fixedAmountCurrency,
		influencers: campaignInstagramOwnerCommissions.map(({ influencer }: Model) => influencer.id)
	};
}

/**
 * @todo i18next
 */
const CommissionForm = ({ campaignId, influencers, onAdd, onClose, initialValues }: ModalProps) => {
	const [opened, setOpened] = useState(false);
	const client = createClient();

	useEffect(() => {
		if (!isNil(initialValues)) {
			setOpened(true);
		}
	}, [initialValues]);

	return (
		<>
			<button className='btn-link' onClick={() => setOpened(true)} data-testid='add-commission-modal'>
				+ Add new Commission
			</button>
			<Modal
				open={opened}
				handleClose={() => {
					setOpened(false);
					onClose && onClose();
				}}
				size='xs'
			>
				<Modal.Header>
					<h4>
						<img src={icons.Money} alt='Commission' />
						&nbsp; {initialValues ? `Edit ${initialValues.name}` : 'Commission'}
					</h4>
				</Modal.Header>
				<Modal.Body>
					<Formik
						initialValues={isNil(initialValues) ? INITIAL_VALUES : modelToCommission(initialValues)}
						enableReinitialize
						onSubmit={async ({ fixedAmount, ...values }, { setErrors }) => {
							values.maxNumberOfInfluencers = values.influencers!.length;
							const config: AxiosRequestConfig = {
								url: `/campaigns/${campaignId}/commissions`,
								method: 'POST',
								data: { fixedAmount: parseInt((fixedAmount * 100).toFixed(0)), ...values }
							};

							if (!isNil(initialValues)) {
								config.url += `/${initialValues.id}`;
								config.method = 'PATCH';
							}

							try {
								await client.request(config);
								setOpened(false);
								onAdd();
							} catch (e) {
								if (!axios.isAxiosError(e)) {
									console.error('Unexpected error %O', e);
									return;
								}

								const { data } = e.response! as AxiosResponse<ErrorResponse>;
								const errors: Record<string, string> = {};

								for (const { source } of data.errors) {
									errors[source.parameter.replace('commissions[0].', '')] = source.message;
								}

								setErrors(errors);
							}
						}}
					>
						{({ isSubmitting, isValid, setFieldValue }) => (
							<Styled.Form noValidate={'development' === process.env.NODE_ENV}>
								<div className='form-group'>
									<label htmlFor='fc-name'>Title</label>
									<Field id='fc-name' data-testid='input-name' name='name' className='border-1 d-block' autocomplete='off' />
									<ErrorMessage name='name' className='validation-error' component='p' />
								</div>
								<div className='form-group'>
									<label htmlFor='fc-fixedAmount'>Fixed cost commission</label>
									<div className='input-group d-flex justify-between'>
										<Field id='fc-fixedAmountCurrency' data-testid='input-fixedAmountCurrency' name='fixedAmountCurrency' as='select'>
											{CURRENCIES.map((currency) => (
												<option value={currency} key={currency}>
													{currency}
												</option>
											))}
										</Field>
										<Field
											id='fc-fixedAmount'
											data-testid='input-fixedAmount'
											name='fixedAmount'
											type='number'
											min={100}
											placeholder='Set amount'
											className='d-block'
										/>
									</div>
									<ErrorMessage name='fixedAmountCurrency' className='validation-error' component='p' />
									<ErrorMessage name='fixedAmount' className='validation-error' component='p' />
								</div>
								<div className='form-group'>
									<label htmlFor='fc-influencers'>Assign influencer(s)</label>
									<div className='input-group d-flex'>
										<button type='button' onClick={() => setFieldValue('influencers', influencers.map(({ id }) => id))}>
											Select all
										</button>
										<Field id='fc-influencers' data-testid='input-influencers' name='influencers' as='select' multiple className='d-block'>
											{influencers.map((influencer) => (
												<option value={influencer.id} key={influencer.id}>
													@{influencer.username}
												</option>
											))}
										</Field>
									</div>
									<ErrorMessage name='influencers' className='validation-error' component='p' />
								</div>
								<hr />
								<div className='d-flex justify-between'>
									<Button type='button' onClick={() => setOpened(false)}>
										Cancel
									</Button>
									<Button disabled={isSubmitting || !isValid} type='submit'>
										Save
									</Button>
								</div>
							</Styled.Form>
						)}
					</Formik>
				</Modal.Body>
			</Modal>
		</>
	);
};

export default CommissionForm;
