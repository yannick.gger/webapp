import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import { Button } from 'components/Button';
import colors from 'styles/variables/colors';

const ButtonsWrapper = styled.div`
	padding: 0 40px;
`;

const LeftButton = styled(Button)`
	float: left;
`;

const RightButton = styled(Button)`
	float: right;
	margin-left: ${guttersWithRem.xs};

	&.delete {
		color: ${colors.error};
	}

	&.edit {
		color: ${colors.button.sencondary};
	}
`;

const Styled = {
	ButtonsWrapper,
	RightButton,
	LeftButton
};

export default Styled;
