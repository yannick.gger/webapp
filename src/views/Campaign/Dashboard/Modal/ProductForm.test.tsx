import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';
import ProductForm from './ProductForm';

test('it matches the snapshot', async () => {
	const callback = jest.fn();
	const { asFragment } = render(
		<ThemeProvider theme={themes.default}>
			<ProductForm
				campaignId={1351}
				onAdd={callback}
				influencers={[{ id: '123123', links: { profilePictureUrl: 'link' }, username: 'abc', followersCount: 100 }]}
			/>
		</ThemeProvider>
	);

	const inputValues: Record<string, string> = {
		name: 'Something you wish you had',
		link: 'https://store.foo.fr',
		description: 'My description',
		value: '1000000'
	};

	expect(asFragment()).toMatchSnapshot('Initial state');

	for (const name in inputValues) {
		userEvent.type(await screen.findByTestId(`form-control-${name}`), inputValues[name]);
	}
	userEvent.click(await screen.findByText('Select all'));

	userEvent.click(await screen.findByText('Save'));
	await waitFor(() => expect(callback).toHaveBeenCalled());
});
