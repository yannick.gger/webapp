import { useState, useEffect, useContext } from 'react';
import { useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { Button } from 'components/Button';
import MainContent from 'styles/layout/main-content';
import CampaignsService from 'services/Campaign/Campaigns.service';
import { ToastContext } from 'contexts';
import LoadingSpinner from 'components/LoadingSpinner';
import PageHeader from 'components/PageHeader';
import Grid from 'styles/grid/grid';
import Checkbox from 'components/Checkbox';
import { guttersWithRem } from 'styles/variables/gutter';

const Wrapper = styled.div``;

const Section = styled.div`
	margin-bottom: ${guttersWithRem.xxl};
`;

const ImageUploadWrapper = styled.div`
	margin: 0 auto;
	display: flex;
`;

const Styled = {
	Wrapper,
	Section,
	ImageUploadWrapper
};

const CampaignEditPage = () => {
	const [loading, setLoading] = useState(false);
	const [campaignImages, setCampaignImages] = useState<any[] | null>(null);
	const [coverImageFiles, setCoverImageFiles] = useState<any[] | null>(null);
	const [selectedCampaignImageId, setSelectedCampaignImageId] = useState(null);
	const toast = useContext(ToastContext);
	const { params } = useRouteMatch();

	const updateFile = (event: any) => {
		setCoverImageFiles(event.target.files);
	};

	const saveCoverImages = () => {
		if (coverImageFiles) {
			setLoading(true);
			[...coverImageFiles].map((image) => {
				CampaignsService.uploadCampaignCoverImage(image)
					.then((res) => {
						if (res) {
							toast.addToast({ id: 'success-add-cover-image', message: 'Success!', mode: 'success' });
							setCampaignImages(null);
							getCampaignImages();
							setLoading(false);
						}
					})
					.catch((err) => {
						toast.addToast({ id: 'error-add-cover-image', message: 'Something went wrong!', mode: 'error' });
						console.log(err);
					})
					.finally(() => {
						setLoading(false);
					});
			});
		}
	};

	const getCampaignImages = () => {
		CampaignsService.getCampaignCoverImages()
			.then((res) => {
				if (res) {
					setCampaignImages(res.data.map((img) => ({ ...img, selected: false })));
				}
			})
			.catch((err) => {
				console.log(err);
				setCampaignImages(null);
			});
	};

	useEffect(() => {
		getCampaignImages();
	}, []);

	const selectOneImageHandler = (imgObj: any) => {
		if (campaignImages) {
			const newImages = campaignImages.map((img) => {
				if (imgObj.id === img.id) {
					return { ...img, selected: true };
				} else {
					return { ...img, selected: false };
				}
			});
			setCampaignImages(newImages);
			setSelectedCampaignImageId(imgObj.id);
		}
	};

	const updateCampaignImage = () => {
		if (selectedCampaignImageId) {
			setLoading(true);
			CampaignsService.setCampaignImage(params.campaignId, selectedCampaignImageId)
				.then((res) => {
					if (res) {
						toast.addToast({ id: 'success-update-campaign', message: 'Success', mode: 'success' });
						setLoading(false);
					}
				})
				.catch((err) => {
					toast.addToast({ id: 'err-update-campaign', message: 'Something went wrong', mode: 'err' });
					console.log(err);
				});
		}
	};

	return (
		<Styled.Wrapper>
			<PageHeader showBreadcrumb={true} headline='Edit Campaign' />
			<MainContent>
				<Styled.Section>
					<h4>Upload new cover image</h4>
					<Styled.ImageUploadWrapper>
						<div>
							<input type='file' onChange={updateFile} />
						</div>
						<Button size='sm' onClick={saveCoverImages} disabled={loading || !coverImageFiles || !coverImageFiles.length}>
							{loading ? <LoadingSpinner size='sm' /> : 'Upload'}
						</Button>
					</Styled.ImageUploadWrapper>
				</Styled.Section>

				<Styled.Section>
					<h4>Campaign Images</h4>
					<ul>
						<Grid.Container>
							{campaignImages
								? campaignImages.map((imageObj) => {
										return (
											<Grid.Column md={3} key={imageObj.attributes.uuid}>
												<li>
													<Checkbox
														checked={imageObj.selected}
														onChange={() => {
															selectOneImageHandler(imageObj);
														}}
													/>
													<img src={imageObj.links.horizontalCoverPhoto} alt={`img-${imageObj.attributes.uuid}`} />
												</li>
											</Grid.Column>
										);
								  })
								: null}
						</Grid.Container>
					</ul>
					<Button onClick={updateCampaignImage} disabled={loading || !selectedCampaignImageId}>
						Save as campaign image
					</Button>
				</Styled.Section>
			</MainContent>
		</Styled.Wrapper>
	);
};

export default CampaignEditPage;
