import React from 'react';
import { Form, Icon, Input, Button, message, Spin } from 'antd';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Mutation, Query } from 'react-apollo';

import { setToken } from 'services/auth-service';
import { withBugsnag } from 'hocs';
import { isLoggedIn, redirectToHome } from 'services/auth-service';
import CollabsAuthService from 'services/Authentication/Collabs-api';
import signUpUserWithTeamMemberTokenQuery from 'graphql/sign-up-user-with-team-member-token.graphql';
import getMemberInviteQuery from 'graphql/get-member-invite.graphql';
import { StatusCode } from 'services/Response.types';

const FormItem = Form.Item;

class SignUpTeamMemberWithTokenForm extends React.Component {
	validatePasswordReq = (_, value, callback) => {
		if (value && value.length < 8) callback('Your password must be at least 8 characters long.');
		callback();
	};

	compareToFirstPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && value !== form.getFieldValue('password')) callback('Two passwords that you enter is inconsistent!');
		else callback();
	};

	validateToNextPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value) form.validateFields(['confirm'], { force: true });
		if (value && form.getFieldValue('confirm')) form.validateFields(['confirm'], { force: true });
		callback();
	};

	componentDidMount = () => {
		if (isLoggedIn()) {
			message.info('An invite can not be accepted while logged in');
			redirectToHome({ history: this.props.history });
		}
	};

	handleSubmit = (signUpUserWithTeamMemberToken) => {
		return (e) => {
			e.preventDefault();
			this.props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphqlValues = {
					...values,
					teamMemberInviteToken: this.props.match.params.inviteToken
				};

				const signUpUserWithTeamMemberTokenRes = await signUpUserWithTeamMemberToken({ variables: graphqlValues });
				const resData = signUpUserWithTeamMemberTokenRes.data.signUpWithTeamInviteToken;
				if (resData && resData.successful && resData.errors.length === 0) {
					setToken(resData.token, { bugsnagClient: this.props.bugsnagClient });

					// Get Collabs API Token
					CollabsAuthService.requestCollabsToken({ username: values.userEmail, password: values.password }).then((result) => {
						if (result === StatusCode.OK) {
							CollabsAuthService.me().then((result) => {
								redirectToHome({ history: this.props.history });
								message.success('Welcome to Collabs!');
								localStorage && localStorage.setItem('signedUp', true);
							});
						}
					});
				} else {
					if (resData.errors) resData.errors.forEach((error) => message.error(error.message));
					else message.error('Something went wrong'); //e.g. inviteToken already used
				}
			});
		};
	};
	render() {
		document.title = `Sign up | Collabs`;
		const { form } = this.props;
		const { getFieldDecorator } = form;
		if (window.FS) {
			localStorage.setItem('fullstory-recording', true);
			window.FS.restart();
		}

		return (
			<Query query={getMemberInviteQuery} variables={{ token: this.props.match.params.inviteToken }}>
				{({ loading, error, data }) => {
					if (loading) {
						return (
							<div className='text-center mt-30'>
								<Spin className='collabspin' />
							</div>
						);
					}

					if (error) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Something went wrong
								</h1>
							</div>
						);
					}

					if (!data.memberInvite) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									No invite was found
								</h1>
								<p>Either you have received an incorrect link or the invitation has already been used.</p>
							</div>
						);
					}

					if (data.memberInvite.used) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Invite token used
								</h1>
								<p>
									This invite has already been used. Please login <a href='/login'>here</a>.
								</p>
							</div>
						);
					}

					if (new Date(data.memberInvite.inviteClosesAt) < new Date()) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Invite expired
								</h1>
								<p>The invite has expired, please ask your co-worker to invite you again.</p>
							</div>
						);
					}

					return (
						<React.Fragment>
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Hello, co-worker
								</h1>
								<p>You have been invited to join {data.memberInvite.organizationName} on Collabs. Welcome!</p>
							</div>
							<section className='block login-register-form'>
								<Mutation mutation={signUpUserWithTeamMemberTokenQuery}>
									{(mutation, { loading }) => (
										<Form onSubmit={this.handleSubmit(mutation)} className='ant-form-lg'>
											<div className='ant-form-group'>
												<FormItem label='Your full name'>
													{getFieldDecorator('userName', {
														placeholder: 'John Doe',
														rules: [
															{
																required: true,
																message: 'Please enter your name'
															}
														]
													})(<Input prefix={<Icon type='user-add' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='John Doe' />)}
												</FormItem>

												<FormItem label='E-mail'>
													{getFieldDecorator('userEmail', {
														initialValue: data.memberInvite.memberEmail,
														rules: [
															{
																required: true,
																message: 'Please enter an email'
															}
														]
													})(<Input prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />} disabled placeholder='E-mail' />)}
												</FormItem>

												<FormItem label='Password'>
													{getFieldDecorator('password', {
														rules: [
															{
																required: true,
																message: 'Please input your password!'
															},
															{
																validator: this.validatePasswordReq
															},
															{
																validator: this.validateToNextPassword
															}
														]
													})(<Input.Password prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} />)}
												</FormItem>
												<FormItem label='Confirm Password'>
													{getFieldDecorator('confirm', {
														rules: [
															{
																required: true,
																message: 'Please confirm your password!'
															},
															{
																validator: this.compareToFirstPassword
															}
														]
													})(<Input.Password prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} onBlur={this.handleConfirmBlur} />)}
												</FormItem>

												<FormItem>
													<Button type='primary' htmlType='submit' loading={loading} className='signup-form-button ant-btn-lg'>
														Register
													</Button>
												</FormItem>
											</div>
										</Form>
									)}
								</Mutation>
							</section>
							<div className='standalone logo dark' />
						</React.Fragment>
					);
				}}
			</Query>
		);
	}
}

SignUpTeamMemberWithTokenForm.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

export default withBugsnag(withRouter(Form.create()(SignUpTeamMemberWithTokenForm)));
