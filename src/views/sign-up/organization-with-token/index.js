import React from 'react';
import { Form, Icon, Input, Button, message, Spin } from 'antd';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Mutation, Query } from 'react-apollo';

import OrganizationFormItems from 'components/organization-form-items';
import { setToken } from 'services/auth-service';
import { withBugsnag } from 'hocs';
import { isLoggedIn, redirectToHome } from 'services/auth-service';
import signUpWithOrganizationTokenQuery from 'graphql/sign-up-with-organization-token.graphql';
import getOrganizationInviteQuery from 'graphql/get-organization-invite.graphql';

const FormItem = Form.Item;

class SignUpOrganizationWithTokenForm extends React.Component {
	validatePasswordReq = (_, value, callback) => {
		if (value && value.length < 8) callback('Your password must be at least 8 characters long.');
		callback();
	};

	compareToFirstPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && value !== form.getFieldValue('user.password')) callback('Two passwords that you enter is inconsistent!');
		else callback();
	};

	validateToNextPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && form.getFieldValue('confirm')) form.validateFields(['confirm'], { force: true });
		callback();
	};

	componentDidMount = () => {
		if (isLoggedIn()) {
			message.info('An invite can not be accepted while logged in');
			redirectToHome({ history: this.props.history });
		}
	};
	handleSubmit = (signUpWithOrganizationToken) => {
		return (e) => {
			e.preventDefault();
			this.props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const signUpWithOrganizationTokenRes = await signUpWithOrganizationToken({
					variables: { ...values, inviteToken: this.props.match.params.inviteToken }
				});
				if (
					signUpWithOrganizationTokenRes.data.signUpWithOrganizationToken &&
					signUpWithOrganizationTokenRes.data.signUpWithOrganizationToken.errors.length === 0
				) {
					setToken(signUpWithOrganizationTokenRes.data.signUpWithOrganizationToken.token, { bugsnagClient: this.props.bugsnagClient });
					message.success('Welcome to Collabs!');
					redirectToHome({ history: this.props.history });
				} else {
					signUpWithOrganizationTokenRes.data.signUpWithOrganizationToken.errors.forEach((error) => message.error(error.message));
				}
			});
		};
	};
	render() {
		document.title = `Sign up | Collabs`;
		const { form } = this.props;
		const { getFieldDecorator } = form;
		if (window.FS) {
			localStorage.setItem('fullstory-recording', true);
			window.FS.restart();
		}

		return (
			<Query query={getOrganizationInviteQuery} variables={{ inviteToken: this.props.match.params.inviteToken }}>
				{({ loading, error, data }) => {
					if (loading) {
						return (
							<div className='text-center mt-30'>
								<Spin className='collabspin' />
							</div>
						);
					}

					if (error) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Something went wrong
								</h1>
							</div>
						);
					}

					if (!data.organizationInvite) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									No invite was found
								</h1>
								<p>Either you have received an incorrect link or the invitation has already been used.</p>
							</div>
						);
					}

					return (
						<React.Fragment>
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Hello, stranger
								</h1>
								<p>You have been invited to join the Collabs family. Welcome!</p>
							</div>
							<section className='block login-register-form'>
								<Mutation mutation={signUpWithOrganizationTokenQuery}>
									{(mutation, { loading }) => (
										<Form onSubmit={this.handleSubmit(mutation)} className='ant-form-lg'>
											<div className='ant-form-group'>
												<OrganizationFormItems form={form} initialValues={{ organizationName: data.organizationInvite.organizationName }} />
												<hr />
												<FormItem label='Your full name'>
													{getFieldDecorator('user.name', {
														placeholder: 'John Doe',
														rules: [
															{
																required: true,
																message: 'Please enter your name'
															}
														]
													})(<Input prefix={<Icon type='user-add' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='John Doe' />)}
												</FormItem>

												<FormItem label='E-mail'>
													{getFieldDecorator('email', {
														initialValue: data.organizationInvite.email,
														rules: [
															{
																required: true,
																message: 'Please enter an email'
															}
														]
													})(<Input prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />} disabled placeholder='E-mail' />)}
												</FormItem>

												<FormItem label='Password'>
													{getFieldDecorator('user.password', {
														rules: [
															{
																required: true,
																message: 'Please input your password!'
															},
															{
																validator: this.validatePasswordReq
															},
															{
																validator: this.validateToNextPassword
															}
														]
													})(<Input.Password prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} />)}
												</FormItem>
												<FormItem label='Confirm Password'>
													{getFieldDecorator('confirm', {
														rules: [
															{
																required: true,
																message: 'Please confirm your password!'
															},
															{
																validator: this.compareToFirstPassword
															}
														]
													})(<Input.Password prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} onBlur={this.handleConfirmBlur} />)}
												</FormItem>

												<FormItem>
													<Button type='primary' htmlType='submit' loading={loading} className='signup-form-button ant-btn-lg'>
														Register
													</Button>
												</FormItem>
											</div>
										</Form>
									)}
								</Mutation>
							</section>
							<div className='standalone logo dark' />
						</React.Fragment>
					);
				}}
			</Query>
		);
	}
}

SignUpOrganizationWithTokenForm.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

export default withBugsnag(withRouter(Form.create()(SignUpOrganizationWithTokenForm)));
