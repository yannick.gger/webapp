import React from 'react';
import { Form, Select, Icon, Input, Button, message, Spin } from 'antd';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Mutation, Query } from 'react-apollo';
import { countries } from 'country-data';

import { setToken } from 'services/auth-service';
import { isLoggedIn, redirectToHome } from 'services/auth-service';
import { withBugsnag } from 'hocs';
import signUpWithPublisherTokenQuery from 'graphql/sign-up-with-publisher-token.graphql';
import getPublisherInviteQuery from 'graphql/get-publisher-invite.graphql';

const FormItem = Form.Item;

class SignUpPublisherWithTokenForm extends React.Component {
	validatePasswordReq = (_, value, callback) => {
		if (value && value.length < 8) callback('Your password must be at least 8 characters long.');
		callback();
	};

	compareToFirstPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && value !== form.getFieldValue('password')) callback('Two passwords that you enter is inconsistent!');
		else callback();
	};

	validateToNextPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && form.getFieldValue('confirm')) form.validateFields(['confirm'], { force: true });
		callback();
	};

	componentDidMount = () => {
		if (isLoggedIn()) {
			message.info('An invite can not be accepted while logged in');
			redirectToHome({ history: this.props.history });
		}
	};
	handleSubmit = (signUpWithPublisherToken) => {
		return (e) => {
			e.preventDefault();
			this.props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const signUpWithPublisherTokenRes = await signUpWithPublisherToken({ variables: { ...values, inviteToken: this.props.match.params.inviteToken } });
				if (signUpWithPublisherTokenRes.data.signUpWithPublisherToken) {
					setToken(signUpWithPublisherTokenRes.data.signUpWithPublisherToken.token, { bugsnagClient: this.props.bugsnagClient });
					message.success('Welcome to Collabs!');
					redirectToHome({ history: this.props.history });
				} else {
					this.props.form.setFields({
						email: {
							errors: [new Error('Could not create user')]
						}
					});
				}
			});
		};
	};
	render() {
		document.title = `Sign up | Collabs`;
		const { getFieldDecorator } = this.props.form;
		if (window.FS) {
			localStorage.setItem('fullstory-recording', true);
			window.FS.restart();
		}
		return (
			<Query query={getPublisherInviteQuery} variables={{ inviteToken: this.props.match.params.inviteToken }}>
				{({ loading, error, data }) => {
					if (loading) {
						return (
							<div className='text-center mt-30'>
								<Spin className='collabspin' />
							</div>
						);
					}

					if (error) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Something went wrong
								</h1>
							</div>
						);
					}

					if (!data.publisherInvite) {
						return (
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									No invite was found
								</h1>
								<p>Either you have received an incorrect link or the invitation has already been used.</p>
							</div>
						);
					}

					return (
						<React.Fragment>
							<div className='text-center mt-30'>
								<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
									Hello, stranger
								</h1>
								<p>You have been invited to join the Collabs family. Welcome!</p>
							</div>
							<section className='block login-register-form'>
								<Mutation mutation={signUpWithPublisherTokenQuery}>
									{(mutation, { loading }) => (
										<Form onSubmit={this.handleSubmit(mutation)} className='ant-form-lg'>
											<div className='ant-form-group'>
												<FormItem label='Publisher Name'>
													{getFieldDecorator('publisherName', {
														initialValue: data.publisherInvite.publisherName,
														rules: [
															{
																required: true,
																message: 'Please enter an publisher name'
															}
														]
													})(<Input prefix={<Icon type='solution' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Publisher name' />)}
												</FormItem>

												<FormItem label='Org. number'>
													{getFieldDecorator('organizationNumber', {
														placeholder: '5948-243',
														rules: [
															{
																required: true,
																message: 'Please enter an organization number'
															}
														]
													})(<Input prefix={<Icon type='idcard' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Org. number' />)}
												</FormItem>
												<FormItem label='Country'>
													{getFieldDecorator('country', {
														rules: [
															{
																required: true,
																message: 'Please select a country'
															}
														],
														initialValue: 'SE'
													})(
														<Select
															prefix={<Icon type='global' style={{ color: 'rgba(0,0,0,.25)' }} />}
															placeholder='Select country'
															size='large'
															filterOption={(input, option) => option.props.children[2].toLowerCase().indexOf(input.toLowerCase()) >= 0}
															optionFilterProp='children'
															showSearch
														>
															{countries.all
																.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
																.filter(({ status }) => status === 'assigned')
																.map((country) => (
																	<Select.Option value={country.alpha2} key={country.alpha2}>
																		{country.emoji} {country.name}
																	</Select.Option>
																))}
														</Select>
													)}
												</FormItem>
												<hr />
												<FormItem label='Your full name'>
													{getFieldDecorator('userName', {
														placeholder: 'John Doe',
														rules: [
															{
																required: true,
																message: 'Please enter your name'
															}
														]
													})(<Input prefix={<Icon type='user-add' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='John Doe' />)}
												</FormItem>

												<FormItem label='E-mail'>
													{getFieldDecorator('email', {
														initialValue: data.publisherInvite.email,
														rules: [
															{
																required: true,
																message: 'Please enter an email'
															}
														]
													})(<Input prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />} disabled placeholder='E-mail' />)}
												</FormItem>

												<FormItem label='Password'>
													{getFieldDecorator('password', {
														rules: [
															{
																required: true,
																message: 'Please input your password!'
															},
															{
																validator: this.validatePasswordReq
															},
															{
																validator: this.validateToNextPassword
															}
														]
													})(<Input.Password prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} />)}
												</FormItem>
												<FormItem label='Confirm Password'>
													{getFieldDecorator('confirm', {
														rules: [
															{
																required: true,
																message: 'Please confirm your password!'
															},
															{
																validator: this.compareToFirstPassword
															}
														]
													})(<Input.Password prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} onBlur={this.handleConfirmBlur} />)}
												</FormItem>

												<FormItem>
													<Button type='primary' htmlType='submit' loading={loading} className='signup-form-button ant-btn-lg'>
														Register
													</Button>
												</FormItem>
											</div>
										</Form>
									)}
								</Mutation>
							</section>
							<div className='standalone logo dark' />
						</React.Fragment>
					);
				}}
			</Query>
		);
	}
}

SignUpPublisherWithTokenForm.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

export default withBugsnag(withRouter(Form.create()(SignUpPublisherWithTokenForm)));
