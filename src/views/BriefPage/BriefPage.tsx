import NavBar from './Components/NavBar';
import Intro from './Components/Intro';
import CampaignInfo from './Components/CampaignInfo';
import CampaignAssignments from './Components/CampaignAssignments';
import CampaignCompensation from './Components/CampaignCompensation';

import Styled from './BriefPage.style';

const BriefPage = (props: { campaign: any; campaignInstagramOwner: any }) => {
	const navigateHandler = (value: string) => {
		const el = document.getElementById(value);
		if (el) {
			el.scrollIntoView({ behavior: 'smooth' });
		}
	};

	return (
		<Styled.Wrapper>
			<NavBar campaign={props.campaign} onClick={navigateHandler} />
			<div>
				<Intro id='intro' campaign={props.campaign} campaignInstagramOwner={props.campaignInstagramOwner} />
				<CampaignInfo id='brief' campaign={props.campaign} />
				<CampaignAssignments id='assignment' campaignInstagramOwner={props.campaignInstagramOwner} />
				<CampaignCompensation id='compensation' campaignInstagramOwner={props.campaignInstagramOwner} />
			</div>
		</Styled.Wrapper>
	);
};

export default BriefPage;
