import styled from 'styled-components';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';
import { breakpoints } from 'styles/variables/media-queries';

const Wrapper = styled.div`
	padding: 0 ${guttersWithRem.xxl};
	background-color: ${colors.white};
	width: 100%;
	height: 3.5rem;
	display: flex;
	justify-content: space-between;
	align-items: center;
	position: sticky;
	top: 0rem;
`;

const NavItems = styled.div`
	display: flex;
	column-gap: ${guttersWithRem.m};
`;

const NavItem = styled.div`
	color: ${colors.button.sencondary.color};
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 1.2rem;
	cursor: pointer;
`;

const CampaignName = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-weight: 600;

	@media (max-width: ${breakpoints.sm}) {
		display: none;
	}
`;

const LinksWrapper = styled.div`
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.m};
`;

const LoginLink = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	text-align: right;
	font-size: 0.8rem;
	color: ${colors.button.sencondary.color};
	cursor: pointer;

	&:hover {
		text-decoration: underline;
	}
`;

const Styled = {
	Wrapper,
	NavItems,
	NavItem,
	CampaignName,
	LoginLink,
	LinksWrapper
};

export default Styled;
