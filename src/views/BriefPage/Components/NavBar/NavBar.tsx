import { isLoggedIn } from 'services/auth-service';

import Styled from './NavBar.style';
import JoinButton from '../JoinButton';

const NavBar = (props: { campaign: any; onClick: (value: string) => void }) => {
	return (
		<Styled.Wrapper>
			<Styled.NavItems>
				<Styled.NavItem
					onClick={() => {
						props.onClick('brief');
					}}
				>
					Brief
				</Styled.NavItem>
				<Styled.NavItem
					onClick={() => {
						props.onClick('assignment');
					}}
				>
					Assignment
				</Styled.NavItem>
				<Styled.NavItem
					onClick={(e) => {
						props.onClick('compensation');
					}}
				>
					Compensation
				</Styled.NavItem>
			</Styled.NavItems>

			<Styled.LinksWrapper>
				{isLoggedIn() ? null : (
					<Styled.LoginLink>
						<div>
							Want to know more?
							<br />
							Login
						</div>
					</Styled.LoginLink>
				)}
				<JoinButton />
			</Styled.LinksWrapper>
		</Styled.Wrapper>
	);
};

export default NavBar;
