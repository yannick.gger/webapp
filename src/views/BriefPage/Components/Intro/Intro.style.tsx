import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';
import Icon from 'components/Icon';

const Wrapper = styled.div`
	padding: 0 ${guttersWithRem.xxl};
	height: 100vh;
	overflow: hidden;
`;

const IntroTextWrapper = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	padding-top: ${guttersWithRem.xxxl};
	text-align: center;
`;

const CampaignName = styled.div`
	font-size: 2.58rem;
	margin-bottom: ${guttersWithRem.xxxl};
`;

const WelcomeText = styled.div`
	margin: 0 auto;
	max-width: 38.5rem;
	font-size: 1.493rem;
	margin-bottom: ${guttersWithRem.xxxl};
	text-align: left;
`;

const SpotsLeft = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 1.037rem;
	color: ${colors.button.sencondary.color};
	margin-bottom: ${guttersWithRem.s};
`;

const CustomIcon = styled(Icon)`
	fill: ${colors.button.sencondary.color};
`;

const ImageWrapper = styled.div`
	margin-top: ${guttersWithRem.m};
	width: 100%;
	max-height: 350px;
	& > img {
		width: 100%;
		object-fit: cover;
	}
`;

const Styled = {
	Wrapper,
	IntroTextWrapper,
	CampaignName,
	WelcomeText,
	SpotsLeft,
	CustomIcon,
	ImageWrapper
};

export default Styled;
