import Styled from './Intro.style';
import FallbackImage from 'assets/img/app/blue-bg-instagram-login.jpg';

const Intro = (props: { id: string; campaign: any; campaignInstagramOwner: any }) => {
	return (
		<Styled.Wrapper id={props.id}>
			<Styled.IntroTextWrapper>
				<Styled.CampaignName>{props.campaign.name}</Styled.CampaignName>
				<Styled.WelcomeText>
					Hi, <strong> {props.campaignInstagramOwner.influencer.username}</strong>! We’re very excited you came here. You’ve been invited to our campaign as
					we’d like to collaborate with you. <strong>Join now</strong> before all spots are taken. It’s first come first served.
				</Styled.WelcomeText>
				<Styled.SpotsLeft> {`Number of spots left: ${props.campaign.full ? 0 : 1}`}</Styled.SpotsLeft>
				<Styled.CustomIcon icon='chevron-down' />
			</Styled.IntroTextWrapper>
			<Styled.ImageWrapper>
				<img
					src={
						props.campaignInstagramOwner.campaign.campaignImages.coverImage ? props.campaignInstagramOwner.campaign.campaignImages.coverImage : FallbackImage
					}
					alt='cover-image'
				/>
			</Styled.ImageWrapper>
		</Styled.Wrapper>
	);
};

export default Intro;
