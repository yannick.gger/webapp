import styled from 'styled-components';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import Icon from 'components/Icon';
import Accordion from 'components/Accordion';

const Wrapper = styled.div`
	padding: 4rem ${guttersWithRem.xxl} 2rem;
	background-color: ${colors.briefPage.campaignAssignments.background};
	text-align: center;
`;

const HeaderSubText = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	margin-bottom: ${guttersWithRem.m};
`;

const HeaderText = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-size: 2rem;
	font-weight: 500;
	margin-bottom: 4rem;
`;

const ContentWrapper = styled.div`
	width: 100%;
	max-width: 40rem;
	margin: 0 auto;
`;

const Summary = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin-bottom: ${guttersWithRem.xl};

	& > span {
		font-weight: 600;
	}
`;

const AssignemntCards = styled.div`
	display: flex;
	flex-direction: column;
	row-gap: ${guttersWithRem.xs};
`;

const AssignmentCard = styled.div`
	width: 100%;
	border: 1px solid ${colors.black};
	background-color: ${colors.white};
	padding: ${guttersWithRem.xl};
	text-align: left;

	& > div {
		&.header {
			display: flex;
			justify-content: space-between;
			margin-bottom: ${guttersWithRem.s};

			& > span {
				display: flex;
				align-items: center;
				column-gap: ${guttersWithRem.xs};
			}
		}

		&.title {
			font-size: 1.5rem;
			font-weight: 500;
			margin-bottom: ${guttersWithRem.s};
		}

		&.description {
			padding-bottom: ${guttersWithRem.s};
			border-bottom: 1px solid ${colors.black};
		}

		&.details {
			padding: 0.625rem 0 0;
		}
	}
`;

const Dates = styled.div`
	padding: 0.25rem ${guttersWithRem.m};
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.xs};
	background-color: ${colors.briefPage.campaignAssignments.dateInfo};
`;

const CustomIcon = styled(Icon)`
	& i {
		line-height: unset;
	}
`;

const CustomAccordion = styled(Accordion)``;

const AccordionContent = styled.div`
	& > div {
		&.detail {
			border-bottom: 1px solid ${colors.briefPage.campaignAssignments.divideLine};
			margin-bottom: ${guttersWithRem.s};

			& > div {
				margin-bottom: ${guttersWithRem.xs};
				&.title {
					font-size: 0.8rem;
					font-weight: 600;
					color: ${colors.briefPage.campaignAssignments.detail.title};
				}

				&.content {
					font-weight: 600;
				}
			}
		}

		&.deadline {
			display: flex;
			column-gap: ${guttersWithRem.xs};
			row-gap: ${guttersWithRem.xs};
			flex-wrap: wrap;
		}
	}
`;

const DeadlineItem = styled.div`
	border: 1px solid ${colors.black};
	border-radius: 2px;
	width: 100%;
	max-width: 18rem;
	padding: ${guttersWithRem.xs};

	& > div {
		display: flex;
		justify-content: space-between;

		& > div {
			&.title {
				font-weight: 600;
			}

			&.date {
				display: flex;
				align-items: center;
				column-gap: ${guttersWithRem.xxs};
				border-radius: 2px;

				border: 1px solid ${colors.black};
				padding: 0 ${guttersWithRem.xxs};
			}
		}
	}
`;

const Styled = {
	Wrapper,
	HeaderSubText,
	HeaderText,
	ContentWrapper,
	Summary,
	AssignmentCard,
	AssignemntCards,
	CustomIcon,
	Dates,
	CustomAccordion,
	AccordionContent,
	DeadlineItem
};

export default Styled;
