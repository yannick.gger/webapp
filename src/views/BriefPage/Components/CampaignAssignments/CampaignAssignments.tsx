import Styled from './CampaignAssignments.style';
import JoinButton from '../JoinButton';
import { apiToMoment } from '../../../../utils';
import { assignmentTypeToIcon, assignmentTypeToText } from '../../../../components/Assigment/Assignment';

const CampaignAssignments = (props: { id: string; campaignInstagramOwner: any }) => {
	const formatDate = (dateValue: string) => {
		const targetDate = apiToMoment(dateValue);
		return targetDate.format('D MMM');
	};

	return (
		<Styled.Wrapper id={props.id}>
			<div>
				<Styled.HeaderSubText>What you will do</Styled.HeaderSubText>
				<Styled.HeaderText>Your assignment</Styled.HeaderText>
			</div>
			<Styled.ContentWrapper>
				<Styled.Summary>
					<span>{`To do's: ${props.campaignInstagramOwner.campaign.assignments.length}`}</span>
					<JoinButton className='join-now' />
				</Styled.Summary>
				<Styled.AssignemntCards>
					{props.campaignInstagramOwner.campaign.assignments.map((assignment: any) => {
						return (
							<Styled.AssignmentCard key={assignment.id}>
								<div className='header'>
									<span>
										{assignmentTypeToIcon(assignment.type)}
										{assignmentTypeToText(assignment.type)}
									</span>
									<Styled.Dates>
										<Styled.CustomIcon icon='calendar' />
										{`${formatDate(assignment.startTime)} - ${formatDate(assignment.endTime)}`}
									</Styled.Dates>
								</div>
								<div className='title'>{assignment.name}</div>
								<div className='description'>{assignment.description}</div>
								<div className='details'>
									<Styled.CustomAccordion className='brief-assignment' toggleIconPosition='left' title='Details'>
										<Styled.AccordionContent>
											<div className='detail'>
												<div className='title'>{`Do's`}</div>
												<div className='content'>{assignment.dos}</div>
											</div>
											<div className='detail'>
												<div className='title'>{`Don'ts`}</div>
												<div className='content'>{assignment.donts}</div>
											</div>
											<div className='detail'>
												<div className='title'>{`CTA`}</div>
												<div className='content'>{assignment.cta}</div>
											</div>
										</Styled.AccordionContent>
									</Styled.CustomAccordion>
									<Styled.CustomAccordion className='brief-assignment' toggleIconPosition='left' title='Deadlines'>
										<Styled.AccordionContent>
											<div className='deadline'>
												{assignment.deadlines.map((deadline: { id: string; date: string; name: string }, index: number) => {
													return (
														<Styled.DeadlineItem key={deadline.id}>
															<div>
																<div className='title'>{`Deadline ${index + 1}`}</div>
																<div className='date'>
																	<Styled.CustomIcon icon='calendar' size='16' />
																	<span>{formatDate(deadline.date)}</span>
																</div>
															</div>
															<div>{deadline.name}</div>
														</Styled.DeadlineItem>
													);
												})}
											</div>
										</Styled.AccordionContent>
									</Styled.CustomAccordion>
								</div>
							</Styled.AssignmentCard>
						);
					})}
				</Styled.AssignemntCards>
			</Styled.ContentWrapper>
		</Styled.Wrapper>
	);
};

export default CampaignAssignments;
