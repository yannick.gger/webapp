import styled from 'styled-components';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import Accordion from 'components/Accordion';
import Icon from 'components/Icon';

const Wrapper = styled.div`
	padding: 4rem ${guttersWithRem.xxl};
	background-color: ${colors.briefPage.campaignInfo.background};
	text-align: center;
`;

const HeaderSubText = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	margin-bottom: ${guttersWithRem.m};
`;

const HeaderText = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-size: 2rem;
	font-weight: 500;
	margin-bottom: 4rem;
`;

const CampaignInfoWrapper = styled.div`
	display: flex;
	flex-direction: column;
	row-gap: ${guttersWithRem.m};
	max-width: 40rem;
	margin: 0 auto;
	margin-bottom: 2.5rem;
`;

const CustomAccordion = styled(Accordion)`
	& > div {
		text-align: left;
		padding: 0 2rem;

		& > div {
			margin-bottom: 1.5rem;
		}
	}
`;

const SpotsLeftCard = styled.div`
	width: 100%;
	max-width: 40rem;

	margin: 0 auto;
	padding: 1.25rem;
	border: 1px solid ${colors.black};

	background-color: ${colors.white};
	text-align: left;

	display: flex;
	flex-direction: column;
	row-gap: ${guttersWithRem.m};

	& > div {
		&.title {
			font-size: 1.5rem;
			font-weight: 500;
		}

		&.spots-left {
			font-weight: 600;

			display: flex;
			justify-content: space-between;
		}
	}
`;

const CustomIcon = styled(Icon)``;

const JoinNowLink = styled.div`
	display: flex;
	align-items: center;
	column-gap: ${guttersWithRem.s};

	cursor: pointer;
	color: ${colors.button.sencondary.color};
	font-weight: 400;
	font-family: ${typography.SecondaryFontFamiliy};

	${CustomIcon} {
		fill: ${colors.button.sencondary.color};
	}
`;

const Styled = {
	Wrapper,
	HeaderSubText,
	HeaderText,
	CampaignInfoWrapper,
	CustomAccordion,
	SpotsLeftCard,
	JoinNowLink,
	CustomIcon
};

export default Styled;
