import Styled from './CampaignInfo.style';
import JoinButton from '../JoinButton';

const CampaignInfo = (props: { id: string; campaign: any }) => {
	return (
		<Styled.Wrapper id={props.id}>
			<div>
				<Styled.HeaderSubText>What we want to achieve</Styled.HeaderSubText>
				<Styled.HeaderText>Let's create together</Styled.HeaderText>
			</div>
			<div>
				<Styled.CampaignInfoWrapper>
					<Styled.CustomAccordion className='brief-page' title='Background' open={true}>
						<div>{props.campaign.background}</div>
					</Styled.CustomAccordion>
					<Styled.CustomAccordion className='brief-page' title='Purpose'>
						<div>{props.campaign.purpose}</div>
					</Styled.CustomAccordion>
					<Styled.CustomAccordion className='brief-page' title='Guidelines'>
						<div>
							<ul>
								{props.campaign.guidelines.map((guideline: string, index: number) => {
									return <li key={index}>{guideline}</li>;
								})}
							</ul>
						</div>
					</Styled.CustomAccordion>
				</Styled.CampaignInfoWrapper>

				<Styled.SpotsLeftCard>
					<div className='title'>Spots left</div>
					<div>{`${Math.floor(Math.random() * 5) + 1} influencers is looking at the brief right now.`}</div>
					<div className='spots-left'>
						<div>{`Number of spots left: ${props.campaign.full ? 0 : 1}/${props.campaign.influencerTargetCount}`}</div>
						<JoinButton className='join-now-link' />
					</div>
				</Styled.SpotsLeftCard>
			</div>
		</Styled.Wrapper>
	);
};

export default CampaignInfo;
