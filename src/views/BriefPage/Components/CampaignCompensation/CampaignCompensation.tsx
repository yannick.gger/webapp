import Styled from './CampaignCompensation.style';
import JoinButton from '../JoinButton';
import FallbackImage from 'assets/img/app/blue-bg-instagram-login.jpg';

const CampaignCompensation = (props: { id: string; campaignInstagramOwner: any }) => {
	return (
		<Styled.Wrapper id={props.id}>
			<div>
				<Styled.HeaderSubText>What you get</Styled.HeaderSubText>
				<Styled.HeaderText>{`Commission & products`}</Styled.HeaderText>
			</div>
			<Styled.ContentWrapper>
				<Styled.Summary>
					<span>{`Total compensation value: ${props.campaignInstagramOwner.campaign.compensationsTotalValue.toLocaleString()} ${
						props.campaignInstagramOwner.campaign.currency
					}`}</span>
					<JoinButton className='join-now' />
				</Styled.Summary>
				<div>
					<Styled.CommissionCards>
						{props.campaignInstagramOwner.campaign.invoices.length > 0
							? props.campaignInstagramOwner.campaign.invoices.map((invoice: { id: string; invoice: { value: number } }) => {
									return (
										<Styled.CommissionCard key={invoice.id}>
											<div className='label'>
												<Styled.CustomIcon icon='money' />
												Commission
											</div>
											<div className='value'>
												<div>{`${invoice.invoice.value.toLocaleString()} ${props.campaignInstagramOwner.campaign.currency}`}</div>
												<div className='tax-info'>Exclusive of VAT (25%)</div>
											</div>
											<div className='invoice-info'>
												<div className='title'>WHAT IS REQUIRED FOR PAYMENT WITH INVOICE</div>
												<ul>
													<li>
														You must be able to send an invoice to get paid. If you do not have your own company, you can use our partner{' '}
														<a href='https://www.gigger.se/collabs' target='_blank'>
															Gigger.
														</a>
													</li>
													<li>Invoice details will be sent to you by email when the campaign is complete.</li>
												</ul>
												<div className='extra-info'>Payment is made via invoice 30 days after you complete the campaign.</div>
											</div>
										</Styled.CommissionCard>
									);
							  })
							: null}
					</Styled.CommissionCards>
					<Styled.ProductCards>
						{props.campaignInstagramOwner.campaign.products.length > 0
							? props.campaignInstagramOwner.campaign.products.map(
									(
										product: {
											id: string;
											quantity: number;
											product: {
												productImage: string | null;
												name: string;
												link: string | null;
												description: string;
												value: number;
											};
										},
										index: number
									) => {
										return (
											<Styled.ProductCard key={product.id}>
												<div className='label'>
													<Styled.CustomIcon icon='product' />
													{`Product ${index + 1}`}
												</div>
												<div className='product-image'>
													<img src={product.product.productImage ? product.product.productImage : FallbackImage} alt={product.product.name} />
												</div>
												<div className='product-info'>
													<div className='name'>{product.product.name}</div>
													<div className='description'>{product.product.description}</div>
													<Styled.CustomAccordion className='brief-product' toggleIconPosition='left' title='Details'>
														<ul>
															<li>{`Value : ${product.product.value.toLocaleString()} ${props.campaignInstagramOwner.campaign.currency}`}</li>
															<li>
																<a href={product.product.link ? product.product.link : '#'} target='_blank'>
																	Product link
																</a>
															</li>
														</ul>
													</Styled.CustomAccordion>
												</div>
											</Styled.ProductCard>
										);
									}
							  )
							: null}
					</Styled.ProductCards>
				</div>
			</Styled.ContentWrapper>
		</Styled.Wrapper>
	);
};

export default CampaignCompensation;
