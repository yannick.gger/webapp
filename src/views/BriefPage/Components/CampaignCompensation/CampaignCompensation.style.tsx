import styled from 'styled-components';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';
import { guttersWithRem } from 'styles/variables/gutter';
import Icon from 'components/Icon';
import Accordion from 'components/Accordion';

const Wrapper = styled.div`
	padding: 2rem ${guttersWithRem.xxl} 4rem;
	background-color: ${colors.briefPage.campaignCommissions.background};
	text-align: center;
`;

const HeaderSubText = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	margin-bottom: ${guttersWithRem.m};
`;

const HeaderText = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	font-size: 2rem;
	font-weight: 500;
	margin-bottom: 4rem;
`;

const ContentWrapper = styled.div`
	width: 100%;
	max-width: 40rem;
	margin: 0 auto;
`;

const Summary = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin-bottom: ${guttersWithRem.xl};

	& > span {
		font-weight: 600;
	}
`;

const CommissionCards = styled.div`
	display: flex;
	flex-direction: column;
	row-gap: ${guttersWithRem.xs};
	margin-bottom: ${guttersWithRem.m};
`;

const CommissionCard = styled.div`
	padding: ${guttersWithRem.xl};
	border: 1px solid ${colors.black};
	background-color: ${colors.white};
	text-align: left;

	& > div {
		&.label {
			width: fit-content;
			display: flex;
			column-gap: ${guttersWithRem.xs};

			padding: ${guttersWithRem.xs};
			background-color: ${colors.briefPage.campaignCommissions.label};

			margin-bottom: ${guttersWithRem.m};
		}

		&.value {
			margin-bottom: ${guttersWithRem.m};

			& > div {
				font-size: 1.5rem;
				font-weight: 600;
				&.tax-info {
					font-size: 0.7rem;
				}
			}
		}

		&.invoice-info {
			padding-top: ${guttersWithRem.m};
			border-top: 1px solid ${colors.briefPage.campaignCommissions.divideLine};
			& > div {
				&.title {
					font-weight: 600;
				}

				&.extra-info {
					border-radius: 2px;
					padding: ${guttersWithRem.xs};
					border: 1px solid ${colors.black};
					font-size: 0.8rem;
				}
			}

			& > ul {
				padding-left: ${guttersWithRem.m};
				font-size: 0.8rem;
			}
		}
	}
`;

const ProductCards = styled.div`
	display: flex;
	flex-wrap: wrap;
	gap: ${guttersWithRem.xs};
`;

const ProductCard = styled.div`
	padding: ${guttersWithRem.xl};
	flex: 1;
	border: 1px solid ${colors.black};
	background-color: ${colors.white};

	& > div {
		&.label {
			margin: 0 auto;
			width: fit-content;
			display: flex;
			column-gap: ${guttersWithRem.xs};

			padding: ${guttersWithRem.xs};
			background-color: ${colors.briefPage.campaignCommissions.label};

			margin-bottom: ${guttersWithRem.l};
		}

		&.product-image {
			margin: 0 auto;
			margin-bottom: ${guttersWithRem.l};
			img {
				max-width: 10rem;
				max-height: 10rem;
			}
		}

		&.product-info {
			& > div {
				&.name {
					font-weight: 600;
					margin-bottom: ${guttersWithRem.l};
				}
				&.description {
					margin-bottom: ${guttersWithRem.l};
				}
				&.detail {
				}
			}
		}
	}
`;

const CustomAccordion = styled(Accordion)``;

const CustomIcon = styled(Icon)`
	line-height: 0;
`;

const Styled = {
	Wrapper,
	HeaderSubText,
	HeaderText,
	ContentWrapper,
	Summary,
	CommissionCards,
	CommissionCard,
	CustomIcon,
	ProductCards,
	ProductCard,
	CustomAccordion
};

export default Styled;
