import styled from 'styled-components';
import { Button } from 'components/Button';
import Icon from 'components/Icon';
import colors from 'styles/variables/colors';

const CustomIcon = styled(Icon)``;

const CustomButton = styled(Button)`
	background-color: ${colors.button.sencondary.color};
	color: ${colors.white};

	${CustomIcon} {
		fill: ${colors.white};
	}

	&:hover {
		${CustomIcon} {
			fill: ${colors.black};
		}
	}
`;

const Wrapper = styled.div`
	&.join-now {
		${CustomButton} {
			border: none;

			&:hover {
				background-color: ${colors.transparent};
			}
		}
	}

	&.join-now-link {
		${CustomButton} {
			border: none;
			color: ${colors.button.sencondary.color};
			background-color: ${colors.transparent};

			&:hover {
				background-color: ${colors.transparent};
			}
		}

		${CustomIcon} {
			fill: ${colors.button.sencondary.color};
		}
	}
`;

const Styled = {
	Wrapper,
	CustomButton,
	CustomIcon
};

export default Styled;
