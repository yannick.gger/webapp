import { useState } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { isLoggedIn, isInfluencer } from 'services/auth-service';
import LoadingSpinner from 'components/LoadingSpinner';

import Styled from './JoinButton.style';
import InfluencerService from 'services/Influencer';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { KEY_COLLABS_API_USER_OBJECT } from 'constants/localStorage-keys';

const JoinButton = (props: { className?: string }) => {
	const [loading, setLoading] = useState(false);
	const { className } = props;
	const { params } = useRouteMatch();
	const history = useHistory();

	const storage = new BrowserStorage(StorageType.LOCAL);

	const routeToSignupPage = () => {
		history.push(`/invites/influencer/sign-up/${params.inviteToken}`);
	};

	const joinCampaignHandler = () => {
		setLoading(true);
		if (isInfluencer()) {
			const user = storage.getItem(KEY_COLLABS_API_USER_OBJECT) || '';
			const influencer = JSON.parse(user).influencer;

			if (influencer) {
				InfluencerService.joinCampaign(params.campaignId, influencer.id).then((res) => {
					history.push('/influencer/campaigns');
				});
			}
		} else {
			console.log('you are not a influencer');
		}
	};

	return (
		<Styled.Wrapper className={className}>
			<Styled.CustomButton size='sm' onClick={isLoggedIn() ? joinCampaignHandler : routeToSignupPage}>
				{loading ? (
					<LoadingSpinner size='sm' />
				) : className ? (
					<>
						Join now
						<Styled.CustomIcon icon='circle-arrow-up-right' size='16' />
					</>
				) : (
					<>
						<Styled.CustomIcon icon='sort' size='24' />
						Join
					</>
				)}
			</Styled.CustomButton>
		</Styled.Wrapper>
	);
};

export default JoinButton;
