import styled from 'styled-components';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	padding: 1rem 0 0;
	background-color: ${colors.white};
	position: relative;
`;

const Styled = {
	Wrapper
};

export default Styled;
