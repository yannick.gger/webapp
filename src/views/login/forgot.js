import React from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { withRouter, Link } from 'react-router-dom';

import './styles.scss';

const FormItem = Form.Item;

class ForgotPasswordForm extends React.Component {
	handleSubmit = (sendForgotPassword) => {
		return (e) => {
			e.preventDefault();
			this.props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const sendForgotPasswordRes = await sendForgotPassword({ variables: values });
				if (sendForgotPasswordRes.data.sendForgotPassword) {
					message.success('An email has been sent to you with a link to reset your password');
					this.props.history.push('/login');
				} else {
					message.error('Could not send a reset email at this time');
				}
			});
		};
	};
	render() {
		const { form } = this.props;
		const { getFieldDecorator } = form;

		const defaultValues = { ...this.props.defaultValues, ...(this.props.location && this.props.location.state ? this.props.location.state.defaultValues : {}) };

		const SEND_FORGOT_PASSWORD = gql`
			mutation sendForgotPassword($email: String!) {
				sendForgotPassword(input: { email: $email }) {
					success
				}
			}
		`;

		return (
			<Mutation mutation={SEND_FORGOT_PASSWORD}>
				{(sendForgotPassword, { loading }) => (
					<Form onSubmit={this.handleSubmit(sendForgotPassword)} className='login-form ant-form-lg'>
						<div className='ant-form-group'>
							Enter your email and we will send you a mail with a link to a page where you can change your password.
							<FormItem>
								{getFieldDecorator('email', {
									initialValue: (defaultValues || {}).email,
									rules: [
										{
											type: 'email',
											message: 'The input is not valid E-mail!'
										},
										{ required: true, message: 'Please input your email!' }
									]
								})(<Input prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='E-mail' />)}
							</FormItem>
							<FormItem>
								<Button type='primary' loading={loading} htmlType='submit' className='login-form-button ant-btn-lg'>
									Send reset email
								</Button>
							</FormItem>
							<Link to={'/login'}>Go back</Link>
						</div>
					</Form>
				)}
			</Mutation>
		);
	}
}

const WrappedForgotPasswordForm = withRouter(Form.create()(ForgotPasswordForm));

export default class LoginForgotView extends React.Component {
	render() {
		document.title = `Forgot password | Collabs`;

		return (
			<div>
				<div className='standalone logo dark' />
				<section className='block login-register-form'>
					<WrappedForgotPasswordForm />
				</section>
			</div>
		);
	}
}
