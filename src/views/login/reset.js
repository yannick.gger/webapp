import React from 'react';
import { Form, Icon, Input, Button, message, Spin } from 'antd';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';
import { withRouter, Link } from 'react-router-dom';
import { redirectToHome, setToken } from 'services/auth-service';

import { withBugsnag } from 'hocs';
import './styles.scss';

const FormItem = Form.Item;

class ResetPasswordForm extends React.Component {
	validatePasswordReq = (_, value, callback) => {
		if (value && value.length < 8) callback('Your password must be at least 8 characters long.');
		callback();
	};

	compareToFirstPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && value !== form.getFieldValue('password')) callback('Two passwords that you enter is inconsistent!');
		else callback();
	};

	validateToNextPassword = (_, value, callback) => {
		const form = this.props.form;
		if (value && form.getFieldValue('confirm')) form.validateFields(['confirm'], { force: true });
		callback();
	};

	handleSubmit = (resetPassword) => {
		return (e) => {
			e.preventDefault();
			this.props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const resetPasswordRes = await resetPassword({ variables: { ...values, token: this.props.token } });
				if (resetPasswordRes.data.resetPassword) {
					setToken(resetPasswordRes.data.resetPassword.token, { bugsnagClient: this.props.bugsnagClient });
					message.success('Your password has been successfully changed');

					redirectToHome({ history: this.props.history });
				} else {
					message.error('Could not change password at this time');
				}
			});
		};
	};
	render() {
		const { form } = this.props;
		const { getFieldDecorator } = form;

		const RESET_PASSWORD = gql`
			mutation resetPassword($token: String!, $password: String!) {
				resetPassword(input: { token: $token, password: $password }) {
					token
				}
			}
		`;

		return (
			<Mutation mutation={RESET_PASSWORD}>
				{(resetPassword, { loading }) => (
					<Form onSubmit={this.handleSubmit(resetPassword)} className='login-form ant-form-lg'>
						<div className='ant-form-group'>
							<FormItem label='Password'>
								{getFieldDecorator('password', {
									rules: [
										{
											required: true,
											message: 'Please input your password!'
										},
										{
											validator: this.validatePasswordReq
										},
										{
											validator: this.validateToNextPassword
										}
									]
								})(<Input.Password className='rippledown' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Password' />)}
							</FormItem>
							<FormItem label='Confirm Password'>
								{getFieldDecorator('confirm', {
									rules: [
										{
											required: true,
											message: 'Please confirm your password!'
										},
										{
											validator: this.compareToFirstPassword
										}
									]
								})(
									<Input.Password className='rippledown' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} onBlur={this.handleConfirmBlur} />
								)}
							</FormItem>
							<FormItem>
								<Button type='primary' loading={loading} htmlType='submit' className='login-form-button ant-btn-lg'>
									Change password
								</Button>
							</FormItem>
							<Link to={'/login'}>Go to login</Link>
						</div>
					</Form>
				)}
			</Mutation>
		);
	}
}

const WrappedResetPasswordForm = withBugsnag(withRouter(Form.create()(ResetPasswordForm)));

const NotValid = () => (
	<span>
		Password reset token is no longer valid. Create a new at <Link to={'/login/forgot'}>Forgot password</Link>
	</span>
);

export default class LoginResetView extends React.Component {
	render() {
		const GET_RESET_PASSWORD_TOKEN = gql`
			query resetPasswordToken($token: String!) {
				resetPasswordToken(token: $token) {
					id
				}
			}
		`;
		document.title = `Reset password | Collabs`;

		return (
			<div>
				<div className='standalone logo dark' />
				<section className='block login-register-form'>
					<Query query={GET_RESET_PASSWORD_TOKEN} variables={{ token: this.props.match.params.token }}>
						{({ data, loading }) => {
							if (loading) {
								return <Spin />;
							}

							if (data.resetPasswordToken) {
								return <WrappedResetPasswordForm token={this.props.match.params.token} />;
							} else {
								return <NotValid />;
							}
						}}
					</Query>
				</section>
			</div>
		);
	}
}
