import React from 'react';
import PropTypes from 'prop-types';

import { isLoggedIn, redirectToHome } from 'services/auth-service';
import LoginForm from './components/login-form';

import './styles.scss';

export default class LoginView extends React.Component {
	render() {
		const { history } = this.props;
		if (isLoggedIn()) {
			redirectToHome({ history });
			return null;
		}
		document.title = `Login | Collabs`;
		return (
			<div>
				<div className='standalone logo dark' />
				<section className='block login-register-form'>
					<LoginForm />
				</section>
			</div>
		);
	}
}

LoginView.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};
