import React from 'react';
import { Spin, Modal, message } from 'antd';
import { withRouter } from 'react-router-dom';
import { setToken, redirectToHome } from 'services/auth-service';

class SendSignInWithInstagramRequestModal extends React.Component {
	componentDidMount() {
		const { location, history, bugsnagClient } = this.props;
		const error = location && location.search.includes('error');
		if (error) {
			history.push('/login');
		} else {
			this.props
				.signInWithInstagram()
				.then((result) => {
					if (result.data && result.data.signInWithInstagram && result.data.signInWithInstagram.token) {
						setToken(result.data.signInWithInstagram.token, { bugsnagClient: bugsnagClient });
						message.success('Logged in with Instagram successfully', 5);
						redirectToHome({ history: history });
					} else if (result.data.signInWithInstagram === null) {
						message.error('Something went wrong');
						setTimeout(() => {
							history.push('/login');
						}, 3000);
					}
				})
				.catch(({ graphQLErrors }) => {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
						setTimeout(() => {
							history.push('/login');
						}, 3000);
					});
				});
		}
	}
	render() {
		return (
			<Modal
				visible
				onOk={this.handleOk}
				onCancel={this.props.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
			>
				<div style={{ textAlign: 'center' }}>
					<Spin className='collabspin' />
					<p className='mt-40'>We are authorizing your Instagram account</p>
				</div>
			</Modal>
		);
	}
}

export default withRouter(SendSignInWithInstagramRequestModal);
