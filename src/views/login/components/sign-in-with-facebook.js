import React, { Component } from 'react';
import { message, Icon } from 'antd';

import { withApollo } from 'react-apollo';
import { setToken } from 'services/auth-service';
import signInWithFacebook from 'graphql/sign-in-with-facebook.graphql';
import '../styles.css';
class SignInWithFacebook extends Component {
	// componentWillUnmount() {
	//   window.FB.Event.unsubscribe("auth.statusChange", this.facebookLogin)
	// }
	// componentDidMount() {
	//   window.FB.Event.subscribe("auth.statusChange", this.facebookLogin)
	// }

	facebookLogin = () => {
		const { FB } = window;
		const { bugsnagClient, client, history } = this.props;
		if (!FB) return;
		FB.getLoginStatus((response) => {
			if (response.authResponse === null) {
				FB.login(this.facebookLoginHandler, { scope: 'instagram_basic,instagram_manage_insights,pages_show_list', return_scopes: true });
			}
			if (response.authResponse) {
				const { accessToken } = response.authResponse;
				const { status } = response;
				if (status === 'connected') {
					client
						.mutate({
							mutation: signInWithFacebook,
							variables: { accessToken }
						})
						.then((result) => {
							const { token } = result.data.signInWithFacebook;
							if (token) {
								setToken(token, { bugsnagClient: bugsnagClient });
								message.success('Login with Facebook successfully');
								setTimeout(() => {
									history.push('/');
								}, 1000);
								FB.logout();
							}
						})
						.catch(({ graphQLErrors }) => {
							if (graphQLErrors) {
								graphQLErrors.forEach((error) => {
									message.error(error.message);
								});
								FB.logout();
							} else {
								message.error('Something went wrong');
							}
						});
				}
			}
		});
	};
	facebookLoginHandler = (response) => {
		if (response.status === 'connected') {
			const accessToken = response.authResponse.accessToken;
			// window.FB.api("/me?fields=id,name,picture", { access_token: accessToken })
		}
	};

	render() {
		return (
			<div className='login-fb-wrapper fb-btn'>
				<a onClick={this.facebookLogin}>
					<Icon type='facebook' theme='filled' className='mr-5' />
					<span>Login with Facebook</span>
				</a>
			</div>
		);
	}
}
export default withApollo(SignInWithFacebook);
