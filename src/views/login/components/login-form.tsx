import React, { useState } from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { redirectToHome, setToken } from 'services/auth-service';
import { UserContext } from '../../../contexts';
import { translate } from 'react-i18next';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { withBugsnag } from 'hocs';
import CollabsAuthService from 'services/Authentication/Collabs-api';

import '../styles.scss';
import { StatusCode } from 'services/Response.types';
import { FEATURE_COLLABS_AUTH } from 'constants/feature-flag-keys';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import ReferralUrlService from 'services/ReferralUrlService';

const FormItem = Form.Item;

const LoginForm = (props: any) => {
	const contextType = UserContext;
	const context = React.useContext(contextType);
	const storage = new BrowserStorage(StorageType.LOCAL);
	const [isEnabled] = useFeatureToggle();

	const { form, t } = props;
	const { getFieldDecorator } = form;
	const defaultValues = { ...props.defaultValues, ...(props.location && props.location.state ? props.location.state.defaultValues : {}) };
	const [btnIsLoading, isBtnIsLoading] = useState(false);

	const SIGN_IN = gql`
		mutation signIn($email: String!, $password: String!) {
			signIn(input: { email: $email, password: $password }) {
				token
				user {
					id
				}
			}
		}
	`;

	const handleSubmit = (signIn: any) => {
		const { t } = props;

		return (e: any) => {
			e.preventDefault();
			isBtnIsLoading(true);

			props.form.validateFieldsAndScroll(async (err: any, values: any) => {
				if (err) {
					isBtnIsLoading(false);
					return null;
				}
				const signInRes = await signIn({ variables: values });
				if (signInRes.data.signIn) {
					setToken(signInRes.data.signIn.token, { bugsnagClient: props.bugsnagClient });
					document.dispatchEvent(new Event('userLoggedInAction'));
					context.updateLoginState();

					if (isEnabled(FEATURE_COLLABS_AUTH)) {
						const creds = { username: values.email, password: values.password };
						CollabsAuthService.requestCollabsToken(creds).then((result) => {
							if (result === StatusCode.OK) {
								CollabsAuthService.me().then((result) => {
									if (result === StatusCode.OK) {
										if (props.location.state && props.location.state.redirectTo) {
											location.href = props.location.state.redirectTo;
										} else {
											ReferralUrlService.redirect(props.history);
										}
									}
								});
							}
						});
					} else {
						ReferralUrlService.redirect(props.history);
					}
				} else {
					props.form.setFields({
						email: {
							value: values.email,
							errors: [new Error(t('default:views.login.loginForm.invalidAccess', { defaultValue: 'No user with that email or password exists' }))]
						}
					});
					isBtnIsLoading(false);
				}
			});
		};
	};

	return (
		<div>
			<Mutation mutation={SIGN_IN}>
				{(signIn: any, { loading }: any) => (
					<Form onSubmit={handleSubmit(signIn)} className='login-form ant-form-lg'>
						<div className='ant-form-group'>
							<FormItem>
								{getFieldDecorator('email', {
									initialValue: (defaultValues || {}).email,
									rules: [
										{
											type: 'email',
											message: t('default:views.login.loginForm.invalidEmail', { defaultValue: 'The input is not valid E-mail!' })
										},
										{ required: true, message: 'Please input your email!' }
									]
								})(<Input prefix={<Icon type='mail' className='login-icon' />} placeholder='E-mail' />)}
							</FormItem>
							<FormItem>
								{getFieldDecorator('password', {
									rules: [
										{
											required: true,
											message: t('default:views.login.loginForm.invalidPassword', { defaultValue: 'Please input your Password!' })
										}
									]
								})(<Input.Password prefix={<Icon type='lock' className='login-icon' />} placeholder='Password' />)}
							</FormItem>

							<FormItem>
								{false &&
									getFieldDecorator('remember', {
										valuePropName: 'checked',
										initialValue: true
									})(<Checkbox>{t('default:views.login.loginForm.rememberMe', { defaultValue: 'Remember me' })}</Checkbox>)}
								<Link to={'/login/forgot'} className='login-form-forgot'>
									{t('default:views.login.loginForm.forgetPassword', { defaultValue: 'Forgot password' })}
								</Link>
							</FormItem>

							<FormItem>
								<Button type='primary' loading={btnIsLoading} htmlType='submit' className='login-form-button ant-btn-lg'>
									{t('default:login', { defaultValue: 'Log in' })}
								</Button>
							</FormItem>
						</div>
					</Form>
				)}
			</Mutation>
		</div>
	);
};

LoginForm.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

export default withBugsnag(withRouter(Form.create()(translate('default')(LoginForm))));
