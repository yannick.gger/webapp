import React from 'react';
import { Mutation } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import signInWithInstagram from 'graphql/sign-in-with-instagram.graphql';
import SendSignInWithInstagramRequestModal from './send-sign-in-with-instagram-request-modal.js';

class SendSignInWithInstagramRequest extends React.Component {
	handleCancel = () => {
		this.props.closeModal();
	};
	render() {
		const { host } = window.location;
		const protocol = 'https';
		const redirectUri = `${protocol}://${host}/instagram/authorization/login/`;
		const code = this.props.location && this.props.location.search.slice(6);
		return (
			<Mutation mutation={signInWithInstagram} variables={{ redirectUri: redirectUri, code: code }}>
				{(signInWithInstagram) => {
					return <SendSignInWithInstagramRequestModal signInWithInstagram={signInWithInstagram} handleCancel={this.handleCancel} />;
				}}
			</Mutation>
		);
	}
}

export default withRouter(SendSignInWithInstagramRequest);
