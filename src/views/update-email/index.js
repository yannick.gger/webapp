import React from 'react';
import { compose, graphql } from 'react-apollo';
import verifyToken from 'graphql/verify-token.graphql';
import { Spin, message } from 'antd';
import { Redirect } from 'react-router-dom';

class UpdateEmail extends React.Component {
	state = { loading: true, data: {} };

	componentDidMount() {
		this.props.match.params.token &&
			this.props
				.verifyToken({ variables: { token: this.props.match.params.token } })
				.then(({ data }) => {
					if (data) {
						this.setState({ loading: false, data });
					}
				})
				.catch((error) => {
					this.setState({ loading: true });
					return message.error(error.message);
				});
	}

	renderResult() {
		const { data } = this.state;
		if (this.state.loading) {
			return <Spin />;
		} else {
			if (data.verifyToken) {
				message.success(`Your email has successfuly changed to: ${data.verifyToken.user.email}`);
				return <Redirect to={'/'} />;
			} else {
				return <span>The change email token is no longer valid.</span>;
			}
		}
	}

	render() {
		return (
			<div>
				<div className='standalone logo dark' />
				<section className='block'>{this.renderResult()}</section>
			</div>
		);
	}
}

export default compose(graphql(verifyToken, { name: 'verifyToken' }))(UpdateEmail);
