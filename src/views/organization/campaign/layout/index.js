import React, { Component } from 'react';
import { Menu, Button, Spin, Icon, Layout, Badge, message, Dropdown } from 'antd';
import { withRouter, Link, Redirect } from 'react-router-dom';
import { Query, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import { isAdmin, checkGhostUserIsSaler, isSaler, isAdminOrGhostAdmin, ghostUserIsSaler } from 'services/auth-service';
import getAdminCampaignReviewQuery from 'graphql/get-admin-campaign-review.graphql';
import GenericNotFoundView from 'views/generic-not-found';
import OrganizationLink from 'components/organization-link/';

import './styles.scss';

import sendCampaignForReviewMutation from 'graphql/send-campaign-review.graphql';
import shareCampaignToTeam from 'graphql/share-campaign-to-team.graphql';
import AssignmentFragment from 'graphql/fragments/assignmentFragment.graphql';
import CampaignReviewFragment from 'graphql/fragments/campaignReviewFragment.graphql';
import CampaignReviewReasonFragment from 'graphql/fragments/campaignReviewReasonFragment.graphql';
import OrganizationFragment from 'graphql/fragments/organization-fragment.graphql';
import CampaignLogoFragment from 'graphql/fragments/campaignLogoFragment.graphql';
import CampaignBrandAssetFragment from 'graphql/fragments/campaignBrandAssetFragment.graphql';
import CampaignInspirationFragment from 'graphql/fragments/campaignInspirationFragment.graphql';
import CampaignFragment from 'graphql/fragments/campaignFragment.graphql';
import InvoiceCompensationFragment from 'graphql/fragments/invoiceCompensationFragment.graphql';
import PaymentCompensationFragment from 'graphql/fragments/paymentCompensationFragment.graphql';
import ProductFragment from 'graphql/fragments/productFragment.graphql';
import ProductImageFragment from 'graphql/fragments/productImageFragment.graphql';
import OrganizationCampaignFragment from 'graphql/organization-campaign-fragment.graphql';
import CampaignCoverPhotoFragment from 'graphql/fragments/campaignCoverPhotoFragment.graphql';
import InstagramPostFragment from 'graphql/fragments/InstagramPostFragment.graphql';
import InstagramStoryFragment from 'graphql/fragments/instagramStoryFragment.graphql';
import TiktokFragment from 'graphql/fragments/tiktokFragment.graphql';

const getOrganizationCampaignQuery = gql`
	query getOrganizationCampaign(
		$id: ID!
		$after: String
		$skipCampaignListIrreleventFields: Boolean = false
		$shortOrgInfo: Boolean = true
		$withCampaignListFields: Boolean = true
		$sharedToTeam: Boolean = false
	) {
		campaign(id: $id) {
			...OrganizationCampaignFragment

			campaignInstagramOwnerAssignments {
				edges {
					node {
						id
						instagramUrl
						posted
						statsAdded
						assignment {
							id
						}
						campaignInstagramOwnerAssignmentScreenshots {
							id
							photo
							photoThumbnail
						}
						instagramOwnerId
					}
				}
			}
		}
	}

	${OrganizationCampaignFragment}
	${CampaignFragment}
	${AssignmentFragment}
	${CampaignReviewReasonFragment}
	${CampaignReviewFragment}
	${OrganizationFragment}
	${CampaignLogoFragment}
	${CampaignBrandAssetFragment}
	${CampaignInspirationFragment}
	${InvoiceCompensationFragment}
	${PaymentCompensationFragment}
	${ProductFragment}
	${ProductImageFragment}
	${InstagramPostFragment}
	${CampaignCoverPhotoFragment}
	${InstagramStoryFragment}
	${TiktokFragment}
`;

const addNewInfluencersMenu = ({ organizationSlug, campaignId, isSubscribed }) => {
	return (
		<Menu>
			<Menu.Item>
				<a href={`/${organizationSlug}/discover`} target='_blank' rel='noopener noreferrer'>
					{i18next.t('organization:campaign.layout.influencer.discover', { defaultValue: 'Discover' })}
				</a>
			</Menu.Item>

			<Menu.Item>
				<a href={`/${organizationSlug}/discover/lists`} target='_blank' rel='noopener noreferrer'>
					{i18next.t('organization:campaign.layout.influencer.list', { defaultValue: 'List' })}
				</a>
			</Menu.Item>
		</Menu>
	);
};

class CampaignLayout extends Component {
	organizationSlug = this.props.location.pathname.split('/')[1];

	getActiveRoute = (menuItems) => {
		const primaryActiveRoute = menuItems.find(({ to }) => to === this.props.location.pathname);

		const secondaryActiveRoute = menuItems.find(({ to }) => this.props.location.pathname.includes(to));

		return primaryActiveRoute ? primaryActiveRoute : secondaryActiveRoute;
	};

	sendNewInfluencersForReview = (campaignId) => {
		const { client } = this.props;
		const refetchQueries = [
			{
				query: getOrganizationCampaignQuery,
				variables: { id: campaignId }
			}
		];

		if (isAdmin() || isSaler()) {
			refetchQueries.push({
				query: getAdminCampaignReviewQuery
			});
		}

		client
			.mutate({
				mutation: sendCampaignForReviewMutation,
				variables: { campaignId: Number(campaignId) },
				refetchQueries: refetchQueries
			})
			.then((response) => {
				if (response.data.createCampaignReview.campaignReview) {
					message.success(i18next.t('organization:campaign.layout.message.reviewSentSuccess', { defaultValue: 'New influencers has been sent for review!' }));
				} else {
					message.error(i18next.t('organization:campaign.layout.message.reviewSentError', { defaultValue: 'Something went wrong' }));
				}
			})
			.catch(({ graphQLErrors }) => {
				if (graphQLErrors) {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
				}
			});
	};

	renderAddNewInfluencerDropdown = ({ id: campaignId, organization: { isSubscribed, slug: organizationSlug } }) => {
		return (
			<Dropdown overlay={addNewInfluencersMenu({ organizationSlug, campaignId, isSubscribed })} trigger={['click']}>
				<Button type='success' data-ripple='rgba(0, 0, 0, 0.3)' className='mr-10'>
					{i18next.t('organization:campaign.layout.button.addNewInfluencers', { defaultValue: 'Add New Influencers' })}
				</Button>
			</Dropdown>
		);
	};

	shareCampaignToTeam = (campaignId) => {
		const { client } = this.props;

		this.setState({ shareLoading: true });
		client
			.mutate({
				mutation: shareCampaignToTeam,
				variables: { campaignId: Number(campaignId) }
			})
			.then((res) => {
				if (res.data.shareCampaignToTeam.errors.length === 0) {
					message.success(i18next.t('organization:campaign.layout.message.sharedWithTeamSuccess', { defaultValue: 'Shared campaign with your team' }));
					this.setState({ shareLoading: false, sharedToTeam: true });
				} else {
					this.setState({ shareLoading: false });
				}
			})
			.catch(({ graphQLErrors }) => {
				this.setState({ shareLoading: false });
				if (graphQLErrors) {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
				}
			});
	};

	render() {
		return (
			<Query query={getOrganizationCampaignQuery} variables={{ id: this.props.match.params.campaignId }}>
				{({ loading, error, data, fetchMore }) => {
					if (loading) return <Spin className='collabspin' />;

					if (this.props.match.url.endsWith('/report') && !isAdmin() && !isSaler() && !ghostUserIsSaler() && !data.campaign.reportSent) {
						message.info('Unless the report is public, only the Collabs-team can see it');
						return <Redirect to={`/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}`} />;
					}

					if (error || data.campaign == null) return <GenericNotFoundView />;

					document.title = `${data.campaign.name} | Campaigns | Collabs`;

					if (this.props.match.url.endsWith('/empty')) {
						return this.props.children({ campaign: data.campaign });
					}

					let isCommissionFixable = true;
					const lastReview = [...data.campaign.campaignReviews.edges].pop();

					if (lastReview) {
						isCommissionFixable = lastReview.node.status === 'declined';
					}

					const menuItems = [
						{
							key: 'overview',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}`,
							content: i18next.t('organization:campaign.layout.menu.overview', { defaultValue: 'Overview' })
						},
						{
							key: 'material',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/material`,
							content: i18next.t('organization:campaign.layout.menu.material', { defaultValue: 'Material' })
						},
						{
							key: 'influencers',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/influencers`,
							content: i18next.t('organization:campaign.layout.menu.influencers', { defaultValue: 'Influencers' })
						},
						{
							key: 'result',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/result`,
							content: i18next.t('organization:campaign.layout.menu.result', { defaultValue: 'Result' }),
							condition: () => process.env.NODE_ENV !== 'production'
						},
						{
							key: 'report',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/report`,
							content: i18next.t('organization:campaign.layout.menu.report', { defaultValue: 'Report' }),
							condition: () => isAdmin() || isSaler() || ghostUserIsSaler() || data.campaign.reportSent
						},
						{
							key: 'assignments',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/assignments`,
							content: i18next.t('organization:campaign.layout.menu.assignments', { defaultValue: 'Assignments' })
						},
						{
							key: 'review',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/review`,
							content: (
								<Badge count={data.campaign.assignmentReviewToReviewCount}>
									{i18next.t('organization:campaign.layout.menu.review', { defaultValue: 'Review' })}
								</Badge>
							),
							condition: () => {
								return data.campaign.assignments.edges.length === 0 || data.campaign.assignments.edges.some(({ node }) => node.hasContentReview);
							}
						},
						{
							key: 'commission',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/products`,
							content: i18next.t('organization:campaign.layout.menu.commission', { defaultValue: 'Commission' }),
							condition: () => {
								return isAdminOrGhostAdmin() || isSaler() || ghostUserIsSaler() || (!isAdminOrGhostAdmin() && isCommissionFixable);
							}
						},
						{
							key: 'settings',
							to: `/${this.organizationSlug}/campaigns/${this.props.match.params.campaignId}/settings`,
							content: (
								<React.Fragment>
									<Icon type=' ion ion-gear-a' style={{ float: 'left', marginTop: '8px' }} />
									{i18next.t('organization:campaign.layout.menu.edit', { defaultValue: 'Edit' })}
								</React.Fragment>
							)
						}
					];

					const campaign = data.campaign || {};
					const { canAddNewInfluencers, newInfluencerExists } = campaign;
					const { edges: campaignReviewEdges } = campaign.campaignReviews;
					const campaignReviewLastNode = (((campaignReviewEdges || []).length && campaignReviewEdges[campaignReviewEdges.length - 1]) || {}).node;
					const { status } = campaignReviewLastNode || {};
					const shareLoading = this.state ? this.state.shareLoading : false;
					const sharedToTeam = this.state ? this.state.sharedToTeam : false;

					return (
						<Layout>
							<Layout.Header>
								<h1>{data.campaign.name}</h1>
								<div>
									{canAddNewInfluencers && this.renderAddNewInfluencerDropdown(campaign)}

									<a href={`/${this.organizationSlug}/campaigns/${data.campaign.id}/preview`} target='_blank' rel='noopener noreferrer'>
										<Button data-ripple='rgba(0, 0, 0, 0.3)' type='primary' className='mr-10'>
											{i18next.t('organization:campaign.layout.button.viewCampaignSite', { defaultValue: 'View campaign site' })}
										</Button>
									</a>

									{campaign.organization.hasTeamMember && (
										<Button
											data-ripple='rgba(0, 0, 0, 0.3)'
											className='mr-10'
											loading={shareLoading}
											disabled={campaign.sharedToTeam || sharedToTeam}
											onClick={() => this.shareCampaignToTeam(campaign.id)}
										>
											{campaign.sharedToTeam || sharedToTeam ? 'Shared' : 'Share with team'}
										</Button>
									)}

									{!newInfluencerExists && status === 'approved' ? (
										<Button disabled type='success'>
											<Icon type='check' />
											{i18next.t('organization:campaign.layout.button.invitesSent', { defaultValue: 'Invites sent' })}
										</Button>
									) : !data.campaign.canBeSentForReview ? (
										newInfluencerExists && status !== 'pending' ? (
											<Button
												type='success'
												disabled={!newInfluencerExists}
												onClick={() => {
													this.sendNewInfluencersForReview(campaign.id);
												}}
											>
												{i18next.t('organization:campaign.layout.button.sendNewInfluencersToReview', { defaultValue: 'Send New Influencers To Review' })}
											</Button>
										) : (
											<Button disabled type='success'>
												<Icon type='check' />
												{i18next.t('organization:campaign.layout.button.campaignWaitingForReview', { defaultValue: 'Campaign waiting for review' })}
											</Button>
										)
									) : !isSaler() ? (
										<OrganizationLink to={`/campaigns/${data.campaign.id}/send-campaign-review`}>
											<Button type='success' data-ripple='rgba(0, 0, 0, 0.3)'>
												{i18next.t('organization:campaign.layout.button.sendCampaignForReview', { defaultValue: 'Send campaign for review' })}
											</Button>
										</OrganizationLink>
									) : null}
								</div>
							</Layout.Header>
							<Layout.Content>
								<div className='campaign-nav no-slide-animation ant-tabs-spacing-fix full-width nav-pills'>
									<Menu mode='horizontal' selectedKeys={[this.getActiveRoute(menuItems) ? this.getActiveRoute(menuItems).key : 'none']}>
										{menuItems.map(({ key, to, content, condition }, index) => {
											if (condition && !condition()) {
												return null;
											}

											return (
												<Menu.Item key={key} data-ripple='rgba(46,90,139,0.15)' style={index === menuItems.length - 1 ? { float: 'right' } : {}}>
													<Link to={{ pathname: to, state: { isCommissionFixable: isCommissionFixable } }}>{content}</Link>
												</Menu.Item>
											);
										})}
									</Menu>
								</div>
								{this.props.children({ campaign: data.campaign, fetchMore })}
							</Layout.Content>
						</Layout>
					);
				}}
			</Query>
		);
	}
}

export default withRouter(withApollo(translate('organization')(CampaignLayout)));
