import React from 'react';
import { Modal, Button, Spin, Icon, message } from 'antd';
import { Query, ApolloConsumer } from 'react-apollo';
import { injectIntl } from 'react-intl';
import gql from 'graphql-tag';

import Content from './content';
import createCampaignDeliveryQuery from 'graphql/create-campaign-delivery.graphql';

const getOrganizationCampaignQuery = gql`
	query getOrganizationCampaign($id: ID!) {
		campaign(id: $id) {
			id
			campaignDelivery {
				id
				latestDeliveryAt
			}
		}
	}
`;

class SendProducts extends React.Component {
	handleCancel = () => {
		this.props.closeModal();
	};
	state = {
		loadingSendProducts: false
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	sendProducts = (client, campaign) => {
		this.setState({ loadingSendProducts: true });

		const form = this.formRef.props.form;
		form.validateFieldsAndScroll(async (err, fields) => {
			if (err) {
				this.setState({ loadingSendProducts: false });
				return null;
			}

			client
				.mutate({
					mutation: createCampaignDeliveryQuery,
					variables: { campaignId: campaign.id, ...fields },
					refetchQueries: [{ query: getOrganizationCampaignQuery, variables: { id: campaign.id } }]
				})
				.then(({ data, loading, error }) => {
					if (error) {
						message.error('Something happened. Could not send invites');
						this.setState({ loadingSendProducts: false });
					}
					if (!loading && data) {
						this.setState({ loadingSendProducts: false });
						message.success('Products for campaign marked as sent');
						this.props.closeModal();
					}
				})
				.catch(() => {
					this.setState({ loadingSendProducts: false });
				});
		});
	};
	render() {
		return (
			<ApolloConsumer>
				{(client) => (
					<Query query={getOrganizationCampaignQuery} variables={{ id: this.props.match.params.campaignId }}>
						{({ loading, error, data }) => {
							if (loading) return <Spin className='collabspin' />;
							if (error) return <p>Could not load campaign</p>;

							let buttons = [];
							if (data.campaign.campaignDelivery !== null) {
								buttons.push(
									<Button size='large' key='ok' type='primary' className='send-cta' loading={this.state.loadingSendInvites} data-ripple disabled>
										Products have already been sent <Icon type='rocket' />
									</Button>
								);
							} else {
								buttons.push(
									<Button
										size='large'
										key='ok'
										type='primary'
										className='send-cta'
										loading={this.state.loadingSendProducts}
										data-ripple
										onClick={() => {
											this.sendProducts(client, data.campaign);
										}}
									>
										I have sent all packages, notify influencers
									</Button>
								);
							}

							return (
								<Modal
									title={'Time to deliver products'}
									visible
									onCancel={this.handleCancel}
									wrapClassName='custom-modal box-parts send-products-modal title-center'
									maskClosable={false}
									footer={buttons}
								>
									<Content campaign={data.campaign} wrappedComponentRef={this.saveFormRef} />
								</Modal>
							);
						}}
					</Query>
				)}
			</ApolloConsumer>
		);
	}
}

export default injectIntl(SendProducts);
