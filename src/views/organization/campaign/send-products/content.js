import { Card, Form, Input, InputNumber, Tag, Timeline } from 'antd';
import DownloadCampaignDeliveryAddresses from 'components/download-campaign-delivery-addresses';
import React from 'react';

const { TextArea } = Input;
const FormItem = Form.Item;

class Content extends React.Component {
	render() {
		const { getFieldDecorator } = this.props.form;
		const formItemLayout = { labelCol: { span: 24 }, wrapperCol: { span: 24 } };
		const { campaign } = this.props;
		return (
			<React.Fragment>
				<div className='description'>
					<p>{`Well done, it's time to deliver products. This is the last step before the influencers will start posting on instagram.`}</p>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>What you need to do</h3>
					<Card className='small-title mb-10 mt-10'>
						<Timeline className='mt-10'>
							<Timeline.Item>Estimate delivery times</Timeline.Item>
							<Timeline.Item>Download the CSV file below</Timeline.Item>
							<Timeline.Item>Deliver all products to influencers</Timeline.Item>
							<Timeline.Item>Press the button below to notify everyone the packages are underway</Timeline.Item>
							<Timeline.Item>Done!</Timeline.Item>
						</Timeline>
					</Card>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>Download CSV file with addresses and all the info</h3>
					<DownloadCampaignDeliveryAddresses campaign={campaign} />
				</div>
				<div className='part'>
					<Form className=''>
						<h3 className='color-blue mb-0'>Delivery information</h3>
						<Card className='small-title mb-30 mt-10'>
							<FormItem {...formItemLayout} label='Estimate lower delivery time'>
								{getFieldDecorator('deliveryDaysLow', {
									rules: [{ required: true, message: 'Please choose an estimated lower delivery time' }]
								})(<InputNumber placeholder='2' min={0} />)}
							</FormItem>
							<FormItem {...formItemLayout} label='Estimate higher delivery time'>
								{getFieldDecorator('deliveryDaysHigh', {
									rules: [{ required: true, message: 'Please choose an estimated higher delivery time' }]
								})(<InputNumber placeholder='7' min={0} />)}
							</FormItem>
							<FormItem {...formItemLayout} label='Carrier'>
								{getFieldDecorator('carrier', {
									rules: [{ required: true, whitespace: true, message: 'Please enter which carrier will deliver the product' }]
								})(<Input placeholder='DHL' />)}
							</FormItem>
						</Card>
						<h3 className='color-blue mb-0'>
							<Tag color='#FF4E64' style={{ float: 'right', marginRight: '0' }}>
								Only do this when all products are sent!
							</Tag>
							Notify influencers packages are sent
						</h3>
						<Card className='small-title mb-10 mt-10'>
							<FormItem {...formItemLayout} label='Message to influencer'>
								{getFieldDecorator('message', {})(
									<TextArea
										placeholder='You will receive a mail from DHL with the location of where you should pick up the package.'
										autosize={{ minRows: 4, maxRows: 6 }}
									/>
								)}
							</FormItem>
						</Card>
					</Form>
				</div>
			</React.Fragment>
		);
	}
}

const WrappedContent = Form.create({})(Content);

export default WrappedContent;
