import { Form } from 'antd';
import DownloadCampaignDeliveryAddresses from 'components/download-campaign-delivery-addresses';
import React from 'react';

class Content extends React.Component {
	render() {
		const { campaign } = this.props;
		return (
			<React.Fragment>
				<h3 className='color-blue mb-0'>Download CSV file with addresses and all the info</h3>
				<DownloadCampaignDeliveryAddresses campaign={campaign} />
			</React.Fragment>
		);
	}
}

const WrappedContent = Form.create({})(Content);

export default WrappedContent;
