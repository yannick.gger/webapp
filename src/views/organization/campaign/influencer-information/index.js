import { Button, Modal, Spin } from 'antd';
import React from 'react';
import { Query } from 'react-apollo';
import { injectIntl } from 'react-intl';
import gql from 'graphql-tag';
import Content from './content';

const getOrganizationCampaignQuery = gql`
	query getOrganizationCampaign($id: ID!) {
		campaign(id: $id) {
			id
		}
	}
`;

class SendProducts extends React.Component {
	handleCancel = () => {
		this.props.closeModal();
	};
	render() {
		return (
			<Query query={getOrganizationCampaignQuery} variables={{ id: this.props.match.params.campaignId }}>
				{({ loading, error, data }) => {
					if (loading) return <Spin className='collabspin' />;
					if (error) return <p>Could not load campaign</p>;

					return (
						<Modal
							title={'Download influencer inforrmation'}
							visible
							onCancel={this.handleCancel}
							wrapClassName='custom-modal title-center'
							maskClosable={false}
							footer={
								<Button size='large' key='submit' type='primary' block loading={loading} onClick={this.props.closeModal}>
									Done
								</Button>
							}
						>
							<Content campaign={data.campaign} />
						</Modal>
					);
				}}
			</Query>
		);
	}
}

export default injectIntl(SendProducts);
