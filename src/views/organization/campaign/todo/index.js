import React from 'react';
import { Row, Col } from 'antd';

import CampaignLayout from '../layout/';
import NoTodo from './no-todos2.svg';

const Todo = () => {
	return (
		<CampaignLayout>
			{({ campaign }) => (
				<Row>
					<Col sm={{ span: 8, offset: 8 }} className='text-center'>
						<div className='empty-icon archive-empty'>
							<img src={NoTodo} alt='' />
						</div>
						<div className='mb-30'>
							<h2>Tasks</h2>
							<p>This feature is available soon! Automatic to-do lists will show you what needs to be done. Pretty neat.</p>
						</div>
					</Col>
				</Row>
			)}
		</CampaignLayout>
	);
};

export default Todo;
