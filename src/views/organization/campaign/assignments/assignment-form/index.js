import React, { Component } from 'react';
import { Tabs, Icon, Form, Radio, InputNumber, Alert, Checkbox } from 'antd';
import withCollabsForm from 'services/collabs-form-decorator';
import { apiToMoment } from 'utils';
import { ghostUserIsSaler, isLoginAdminUser, isSaler } from 'services/auth-service.js';
import { StoryIconFilled, ReelIconFilled, TiktokIconFilled } from 'components/icons';
import SharedFields from './shared-fields';
import './styles.scss';

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class AssignmentForm extends Component {
	state = {
		adminOverride: false
	};

	handleAdminOverride = (e) => {
		const override = e.target.checked;
		this.props.enableSave(override);
		this.setState({ adminOverride: override });
	};

	render() {
		const { form, campaign } = this.props;
		const { adminOverride } = this.state;
		const { getFieldDecorator, getFieldValue, setFieldsValue } = form;
		const inputDisabled = campaign && campaign.invitesSent && !adminOverride;

		return (
			<React.Fragment>
				{campaign && campaign.invitesSent && (
					<React.Fragment>
						{isLoginAdminUser() || isSaler() || ghostUserIsSaler() ? (
							<Alert
								message='Assignment can not be edited'
								description={
									<React.Fragment>
										<p>
											Assignments can't be edited after invites are sent. As admin, you can override this. Make sure that all involved parties have agreed to
											any new terms.
										</p>
										<Checkbox className='mt-10' value={true} checked={adminOverride} onChange={this.handleAdminOverride} style={{ color: 'red' }}>
											{adminOverride ? 'Using admin privilege to override' : 'Use admin privilege to override'}
										</Checkbox>
									</React.Fragment>
								}
								type={adminOverride ? 'error' : 'warning'}
							/>
						) : (
							<Alert message='Assignment can not be edited' description="Assignments can't be edited after invites are sent." type='warning' showIcon />
						)}
					</React.Fragment>
				)}
				<Form layout='vertical'>
					{getFieldDecorator('instagramType', { initialValue: 'photo' })(
						<RadioGroup style={{ display: 'none' }} disabled={inputDisabled}>
							<Radio value={'photo'}>Photo</Radio>
							<Radio value={'video'}>Video</Radio>
							<Radio value={'story'}>Story</Radio>
							<Radio value={'reel'}>Reel</Radio>
							<Radio value={'tiktok'}>Tiktok</Radio>
						</RadioGroup>
					)}
					<Tabs
						defaultActiveKey={getFieldValue('instagramType')}
						className='full-width all-space assignmentTabs'
						onChange={(activeKey) => setFieldsValue({ instagramType: activeKey })}
					>
						<TabPane
							disabled={inputDisabled}
							tab={
								<span>
									<Icon className='ion ion-android-camera' /> Photo
								</span>
							}
							key='photo'
						>
							<SharedFields form={form} campaign={campaign} disabled={inputDisabled}>
								<FormItem label='Should the posted photo be a slideshow?' className={getFieldValue('slideshow') ? 'has-child' : ''}>
									{getFieldDecorator('slideshow', {
										rules: [{ required: true, message: 'Please select if you want a slideshow' }],
										initialValue: false
									})(
										<RadioGroup size='large' className='radio-pills f-w' disabled={inputDisabled}>
											<RadioButton value={false}>No slideshow</RadioButton>
											<RadioButton value={true}>Yes!</RadioButton>
										</RadioGroup>
									)}
								</FormItem>
								{getFieldValue('slideshow') ? (
									<FormItem>
										{getFieldDecorator('slideshowCount', {
											rules: [
												{ required: true, message: 'Enter number of slides in the slideshow' },
												{
													validator: (rule, value, callback) => {
														callback(value < 2 ? [new Error()] : []);
													},
													message: 'A slideshow must have at least 2 slides'
												},
												{
													validator: (rule, value, callback) => {
														callback(value > 10 ? [new Error()] : []);
													},
													message: 'A slideshow can not consist of more than 10 slides'
												}
											]
										})(<InputNumber placeholder='How many slides?' size='large' style={{ width: '300px' }} disabled={inputDisabled} />)}
									</FormItem>
								) : null}
							</SharedFields>
						</TabPane>
						<TabPane
							disabled={inputDisabled}
							tab={
								<span>
									<Icon className='ion ion-ios-videocam' />
									Video
								</span>
							}
							key='video'
						>
							<SharedFields form={form} campaign={campaign} disabled={inputDisabled} />
						</TabPane>
						<TabPane
							disabled={inputDisabled}
							tab={
								<span>
									<Icon className='ion' component={StoryIconFilled} />
									Story
								</span>
							}
							key='story'
						>
							<SharedFields form={form} campaign={campaign} disabled={inputDisabled} />
						</TabPane>
						<TabPane
							disabled={inputDisabled}
							tab={
								<span>
									<Icon className='ion' component={ReelIconFilled} disabled={inputDisabled} />
									Reel
								</span>
							}
							key='reel'
						>
							<SharedFields form={form} campaign={campaign} disabled={inputDisabled} />
						</TabPane>
						<TabPane
							disabled={inputDisabled}
							tab={
								<span>
									<Icon className='ion' component={TiktokIconFilled} disabled={inputDisabled} />
									Tiktok
								</span>
							}
							key='tiktok'
						>
							<SharedFields form={form} campaign={campaign} disabled={inputDisabled} />
						</TabPane>
					</Tabs>
				</Form>
			</React.Fragment>
		);
	}
}

export default withCollabsForm(AssignmentForm, {
	customFieldMap: {
		dateRange: (fields) => (fields && fields.startTime ? [apiToMoment(fields.startTime), apiToMoment(fields.endTime)] : [])
	}
});
