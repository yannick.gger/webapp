import React from 'react';
import { Form, DatePicker, Input } from 'antd';
import moment from 'moment';

const { TextArea } = Input;
const FormItem = Form.Item;

export default ({ form, children, campaign, disabled }) => {
	const { getFieldDecorator, getFieldValue } = form;

	return (
		<React.Fragment>
			<FormItem label='Give your assignment a name'>
				{getFieldDecorator('name', {
					rules: [{ required: true, message: 'You forgot to name your assignment!' }]
				})(<Input size='large' placeholder='Creative photography' disabled={disabled} />)}
			</FormItem>
			<FormItem label='Description'>
				{getFieldDecorator('description', {
					rules: [{ required: true, message: 'You need a description!' }]
				})(
					<TextArea data-ripple placeholder='Describe in a few words what the assignment is about' disabled={disabled} autosize={{ minRows: 2, maxRows: 6 }} />
				)}
			</FormItem>
			{getFieldDecorator('frequency', {
				rules: [{ required: true, message: 'Please select frequency' }],
				initialValue: 'one_off'
			})(<span />)}
			{getFieldValue('frequency') === 'one_off' ? (
				<FormItem label='When do you want the influencer to post?'>
					{getFieldDecorator('dateRange', {
						rules: [{ type: 'array', required: true, message: 'Please select a date range for the user to post' }]
					})(
						<DatePicker.RangePicker
							disabled={disabled}
							disabledDate={(current) => current && current < moment().subtract(1, 'days')}
							size='large'
							format='YYYY-MM-DD'
							style={{ width: '100%' }}
						/>
					)}
				</FormItem>
			) : null}

			{children}
		</React.Fragment>
	);
};
