import React from 'react';
import { Modal, Button, message } from 'antd';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';

import AssignmentForm from './assignment-form/';

// Later load first / last assignment here to make sure you don't pick a date too late
const GET_CAMPAIGN_DATES = gql`
	query getCampaignDates($id: ID!) {
		campaign(id: $id) {
			id
		}
	}
`;

const CREATE_ASSIGNMENT = gql`
	mutation createAssignment(
		$campaignId: ID!
		$name: String!
		$description: String!
		$frequency: AssignmentFrequency
		$startTime: String
		$endTime: String
		$instagramType: InstagramType
		$slideshow: Boolean
		$slideshowCount: Int
	) {
		createAssignment(
			input: {
				campaignId: $campaignId
				name: $name
				description: $description
				frequency: $frequency
				startTime: $startTime
				endTime: $endTime
				instagramType: $instagramType
				slideshow: $slideshow
				slideshowCount: $slideshowCount
			}
		) {
			assignment {
				id
			}
		}
	}
`;

class CreateAssignmentModal extends React.Component {
	handleSubmit = (createAssignment) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					startTime: values.dateRange[0],
					endTime: values.dateRange[1],
					campaignId: this.props.location.pathname.split('/')[3]
				};

				const res = await createAssignment({ variables: graphQlValues });
				if (res.data.createAssignment) {
					message.info('Assignment was created');
					this.props.closeModal();
					//this.props.history.push("/dashboard")
				} else {
					message.error('Something went wrong');
				}
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		return (
			<Mutation mutation={CREATE_ASSIGNMENT} refetchQueries={['getAssignments', 'getOrganizationCampaign']}>
				{(createAssignment, { loading }) => (
					<Modal
						title='What should the influencers post?'
						visible
						onOk={this.handleOk}
						onCancel={this.handleCancel}
						wrapClassName='custom-modal separate-parts title-center'
						maskClosable={false}
						footer={[
							<Button size='large' key='back' onClick={this.handleCancel}>
								Return
							</Button>,
							<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(createAssignment)}>
								Create assignment
							</Button>
						]}
					>
						<AssignmentForm campaign={this.props.campaign} wrappedComponentRef={this.saveFormRef} />
					</Modal>
				)}
			</Mutation>
		);
	}
}

class CreateAssignmentModalWithCampaign extends React.Component {
	render() {
		return (
			<Query query={GET_CAMPAIGN_DATES} variables={{ id: this.props.match.params.campaignId }}>
				{({ data, loading, error }) => {
					if (loading && !data.campaign) {
						return "<Spin className='collabspin' />";
					}

					if (error) {
						return '<span>Failed to load campaign</span>';
					}

					return <CreateAssignmentModal campaign={data.campaign} {...this.props} />;
				}}
			</Query>
		);
	}
}

export default CreateAssignmentModalWithCampaign;
