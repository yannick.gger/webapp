import React, { Component } from 'react';
import { Menu, Dropdown, Icon, Modal, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';

import ButtonLink from 'components/button-link';
import OrganizationLink from 'components/organization-link/';

const MenuItemGroup = Menu.ItemGroup;

const REMOVE_ASSIGNMENT = gql`
	mutation removeAssignment($id: ID!) {
		removeAssignment(input: { id: $id }) {
			assignment {
				id
			}
		}
	}
`;

export default class AssignmentDropdown extends Component {
	menu = (
		<Menu>
			<MenuItemGroup title='Quick actions'>
				<Menu.Item key='1' data-ripple='rgba(0, 0, 0, 0.3)'>
					<OrganizationLink to={`/campaigns/${this.props.campaign.id}/assignments/${this.props.assignment.id}/edit`}>
						<Icon type='edit' className='mr-10' /> Edit
					</OrganizationLink>
				</Menu.Item>
				<Menu.Item key='2' data-ripple='rgba(0, 0, 0, 0.3)'>
					<ApolloConsumer>
						{(client) => (
							<ButtonLink
								className='color-red'
								onClick={() => {
									this.props.campaign.invitesSent
										? message.error("Assignment can't be deleted after invite has been sent.")
										: this.showDeleteConfirm({ client: client, assignmentId: this.props.assignment.id });
								}}
							>
								<Icon type='delete' className='mr-10' /> Delete
							</ButtonLink>
						)}
					</ApolloConsumer>
				</Menu.Item>
			</MenuItemGroup>
		</Menu>
	);

	showDeleteConfirm = ({ client, assignmentId }) => {
		Modal.confirm({
			title: 'Are you sure you want to remove this assignment?',
			content: 'A removed assignment can not be restored after removal.',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				client
					.mutate({
						mutation: REMOVE_ASSIGNMENT,
						variables: { id: assignmentId },
						refetchQueries: ['getAssignments']
					})
					.then(() => {
						message.info('Assignment was removed');
					});
			}
		});
	};

	render() {
		return (
			<React.Fragment>
				<Dropdown overlay={this.menu} trigger={['click']} placement='bottomRight'>
					<Icon type='ellipsis' />
				</Dropdown>
			</React.Fragment>
		);
	}
}
