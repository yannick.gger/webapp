import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import CampaignLayout from '../layout/';
import ListWithQuery from './list-with-query';

class Assignments extends Component {
	render() {
		return (
			<CampaignLayout>
				{({ campaign }) => {
					return <ListWithQuery campaign={campaign} showCreate={true} />;
				}}
			</CampaignLayout>
		);
	}
}

export default withRouter(Assignments);
