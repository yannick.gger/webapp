import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Row, Col, Card, Tag, Tooltip, Icon } from 'antd';
import Moment from 'react-moment';
import { apiToMoment, API_DATE_FORMAT } from 'utils';

import ButtonLink from 'components/button-link';
import AssignmentDropdown from './assignment-dropdown';
import CreateCard from 'components/create-card/';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';
import { ReelIconFilled, StoryIconFilled, TiktokIconFilled } from 'components/icons';

import './styles.scss';

class Assignments extends Component {
	state = {
		initialLoading: false,
		moreLoading: false
	};
	loadMore = (data, fetchMore) => {
		this.setState({ moreLoading: true });
		fetchMore({
			variables: {
				cursor: data.campaign.assignments.pageInfo.endCursor
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				this.setState({ moreLoading: false });

				const newEdges = fetchMoreResult.campaign.assignments.edges;
				const pageInfo = fetchMoreResult.campaign.assignments.pageInfo;

				return newEdges.length
					? {
							campaign: {
								...previousResult.campaign,
								assignments: {
									__typename: previousResult.campaign.assignments.__typename,
									edges: [...previousResult.campaign.assignments.edges, ...newEdges],
									pageInfo
								}
							}
					  }
					: previousResult;
			}
		});
	};
	componentDidUpdate = (prevProps) => {
		if (prevProps.loading !== this.props.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.loading });
		}
	};
	componentDidMount = () => {
		if (this.props.loading) {
			this.setState({ initialLoading: true });
		}
	};
	render() {
		const { error, data, fetchMore, campaign } = this.props;
		const mdSpan = this.props.mdSpan || 12;
		const showCreate = this.props.showCreate && campaign.invitesSent === false;
		const { initialLoading, moreLoading } = this.state;

		return (
			<React.Fragment>
				<Row gutter={30} className='item-rows'>
					{data &&
						data.campaign &&
						data.campaign.assignments.edges.map(({ node }) => (
							<Col className='d-flex' xs={{ span: 24 }} md={{ span: mdSpan }} key={node.id}>
								<Card
									hoverable
									className='assignment-card'
									actions={[<AssignmentDropdown key='assignment-dropdown' assignment={node} campaign={this.props.campaign} />]}
								>
									<div className='card-headline'>
										<span className='icon circle blue fl'>
											{['story', 'reel', 'tiktok'].includes(node.instagramType) ? (
												node.instagramType === 'story' ? (
													<Icon component={StoryIconFilled} />
												) : node.instagramType === 'reel' ? (
													<Icon component={ReelIconFilled} />
												) : (
													<Icon component={TiktokIconFilled} />
												)
											) : (
												<i className={`ion ${{ video: 'ion-ios-videocam', photo: 'ion-camera' }[node.instagramType]}`} />
											)}
										</span>
										<Card.Meta title={node.name} description={node.description} className='multi-line' />
										{false && <ButtonLink>Edit</ButtonLink>}
									</div>
									<hr />
									<Row style={{ display: 'flex' }}>
										<Col>
											<h4 style={{ marginBottom: '0' }}>Posting date</h4>
											<h2 className='color-blue'>
												<Tooltip
													title={
														apiToMoment(node.startTime).format('YYYY-MM-DD') === apiToMoment(node.endTime).format('YYYY-MM-DD')
															? apiToMoment(node.startTime).format('YYYY-MM-DD')
															: `${apiToMoment(node.startTime).format('YYYY-MM-DD')} - ${apiToMoment(node.endTime).format('YYYY-MM-DD')}`
													}
												>
													<Moment parse={API_DATE_FORMAT} fromNow>
														{node.startTime}
													</Moment>
												</Tooltip>
											</h2>
										</Col>
									</Row>
									<hr />
									<Tag className='card-tag'>Assigned to: All users</Tag>
									<Tag className='card-tag'>Type: {node.frequency === 'one_off' ? 'One-Off' : 'Recurring'}</Tag>
								</Card>
							</Col>
						))}
					{showCreate && (
						<Col className='d-flex' xs={{ span: 24 }} md={{ span: 12 }}>
							<Link
								to={`/${this.props.match.params.organizationSlug}/campaigns/${this.props.match.params.campaignId}/assignments/create`}
								data-ripple='rgba(132,146, 164, 0.2)'
								className='empty-cell create-new'
							>
								<CreateCard>Create assignment</CreateCard>
							</Link>
						</Col>
					)}
				</Row>
				{!error &&
					!initialLoading &&
					data &&
					data.campaign &&
					data.campaign.assignments.edges.length !== 0 &&
					data.campaign.assignments.edges.length % 20 === 0 && (
						<div className='pt-30 pb-30 text-center'>
							<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(data, fetchMore)}>
								Load more
							</Button>
						</div>
					)}
			</React.Fragment>
		);
	}
}

export default withRouter(Assignments);
