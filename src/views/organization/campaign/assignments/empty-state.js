import React from 'react';
import { Row, Col, Button } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from '../../../../components/ghost-user-access';
import NoAssign from './no-assignments.svg';

const EmptyState = ({ match }) => (
	<Row>
		<Col sm={{ span: 8, offset: 8 }} className='text-center'>
			<div className='empty-icon'>
				<img src={NoAssign} alt='' className='svg' />
			</div>
			<div className='mb-30'>
				<h2>Create assignment</h2>
				<p>Assignments is tasks created for influencers to complete</p>
			</div>
			<div className='d-flex justify-content-center'>
				<Link to={`${match.url}/create`}>
					<Button type='primary' className='btn-uppercase' size='large'>
						Create assignment
					</Button>
				</Link>
			</div>
		</Col>
	</Row>
);

export default withRouter(EmptyState);
