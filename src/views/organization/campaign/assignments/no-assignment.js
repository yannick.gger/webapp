import React, { Component } from 'react';
import { Row, Col } from 'antd';

import CreateAssignmentModal from './create-assignment-modal.js';
import NoAssign from './no-assignments.svg';

class Assignments extends Component {
	render() {
		return (
			<React.Fragment>
				<Row>
					<Col sm={{ span: 8, offset: 8 }} className='text-center'>
						<div className='empty-icon'>
							<img src={NoAssign} alt='' className='svg' />
						</div>
						<div className='mb-30'>
							<h2>Create assignment</h2>
							<p>Assignments is tasks created for influencers to complete</p>
						</div>
						<div className='d-flex justify-content-center'>
							<CreateAssignmentModal />
						</div>
					</Col>
				</Row>
			</React.Fragment>
		);
	}
}

export default Assignments;
