import React from 'react';
import { Modal, Button, message, Spin } from 'antd';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';

import AssignmentForm from './assignment-form/';

const GET_ASSIGNMENT = gql`
	query getAssignment($id: ID!) {
		assignment(id: $id) {
			id
			name
			description
			instagramType
			startTime
			endTime
			frequency
			slideshow
			slideshowCount
			campaign {
				id
				invitesSent
			}
		}
	}
`;

const UPDATE_ASSIGNMENT = gql`
	mutation updateAssignment(
		$id: ID!
		$name: String!
		$description: String!
		$frequency: AssignmentFrequency
		$startTime: String
		$endTime: String
		$instagramType: InstagramType
		$slideshow: Boolean
		$slideshowCount: Int
	) {
		updateAssignment(
			input: {
				id: $id
				name: $name
				description: $description
				frequency: $frequency
				startTime: $startTime
				endTime: $endTime
				instagramType: $instagramType
				slideshow: $slideshow
				slideshowCount: $slideshowCount
			}
		) {
			assignment {
				id
			}
		}
	}
`;
class EditAssignmentModal extends React.Component {
	state = {
		adminOverride: false
	};

	handleSubmit = ({ updateAssignment, assignmentId }) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					id: assignmentId,
					startTime: values.dateRange[0],
					endTime: values.dateRange[1]
				};

				const res = await updateAssignment({ variables: graphQlValues });
				if (res.data.updateAssignment) {
					message.info('Assignment was updated');
					this.props.closeModal();
				} else {
					message.error('Something went wrong');
				}
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	handleAdminOverride = (adminOverride) => {
		this.setState({ adminOverride });
	};

	render() {
		const { assignment } = this.props;
		const inputDisabled = assignment.campaign.invitesSent && !this.state.adminOverride;

		return (
			<Mutation mutation={UPDATE_ASSIGNMENT} refetchQueries={['getAssignments']}>
				{(updateAssignment, { loading }) => (
					<Modal
						title='What should the influencers post?'
						visible
						onOk={this.handleOk}
						onCancel={this.handleCancel}
						wrapClassName='custom-modal separate-parts title-center'
						maskClosable={false}
						footer={[
							<Button size='large' key='back' onClick={this.handleCancel}>
								Return
							</Button>,
							<Button
								size='large'
								key='submit'
								type='primary'
								disabled={inputDisabled}
								loading={loading}
								onClick={this.handleSubmit({ updateAssignment, assignmentId: assignment.id })}
							>
								Save
							</Button>
						]}
					>
						<AssignmentForm fields={assignment} campaign={assignment.campaign} enableSave={this.handleAdminOverride} wrappedComponentRef={this.saveFormRef} />
					</Modal>
				)}
			</Mutation>
		);
	}
}

class EditAssignmentModalWithAssignment extends React.Component {
	render() {
		return (
			<Query query={GET_ASSIGNMENT} variables={{ id: this.props.match.params.assignmentId }}>
				{({ data, loading, error }) => {
					if (loading) {
						return <Spin className='collabspin' />;
					}

					if (!data.assignment) {
						message.error('Could not found assignment.');
						return '';
					}

					if (error) {
						return '<span>Failed to load assignment</span>';
					}

					const { assignment } = data;
					return <EditAssignmentModal assignment={assignment} {...this.props} />;
				}}
			</Query>
		);
	}
}

export default EditAssignmentModalWithAssignment;
