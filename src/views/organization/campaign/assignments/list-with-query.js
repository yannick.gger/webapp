import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { Spin } from 'antd';

import EmptyState from './empty-state';
import List from './list';

const GET_ASSIGNMENTS = gql`
	query getAssignments($campaignId: ID!, $cursor: String) {
		campaign(id: $campaignId) {
			id
			assignments(first: 20, after: $cursor) @connection(key: "assignments", filter: []) {
				pageInfo {
					endCursor
					hasNextPage
				}

				edges {
					node {
						id
						name
						description
						instagramType
						startTime
						endTime
						slideshowCount
						slideshow
						frequency
					}
				}
			}
		}
	}
`;

class ListWithQuery extends Component {
	render() {
		const { campaign, showCreate, mdSpan } = this.props;
		if (campaign.assignmentsCount === 0) return <EmptyState />;
		return (
			<Query query={GET_ASSIGNMENTS} variables={{ campaignId: campaign.id }} notifyOnNetworkStatusChange>
				{({ loading, error, data, fetchMore }) => {
					if (loading) return <Spin className='collabspin' />;
					if (error) return <p>Something went wrong</p>;
					return <List data={data} fetchMore={fetchMore} error={error} loading={loading} campaign={campaign} showCreate={showCreate} mdSpan={mdSpan} />;
				}}
			</Query>
		);
	}
}

export default withRouter(ListWithQuery);
