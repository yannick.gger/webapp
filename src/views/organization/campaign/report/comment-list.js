import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { Spin, Button } from 'antd';

import getComment from 'graphql/get-comment.graphql';
import SingleComment from './comment.js';

class CommentList extends Component {
	loadMore = (comment, fetchMore) => {
		return fetchMore({
			variables: {
				after: comment.replies.pageInfo.endCursor || null
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					comment: {
						...comment,
						replies: {
							...fetchMoreResult.comment.replies,
							edges: [...prev.comment.replies.edges, ...fetchMoreResult.comment.replies.edges]
						}
					}
				};
				return result;
			}
		});
	};
	render() {
		const { client, currentUserId, comments } = this.props;
		return (
			<React.Fragment>
				{comments &&
					comments.edges.map(({ node }) => {
						const mainCommentId = Number(node.id);
						return (
							<div key={mainCommentId} className='comments-wrapper'>
								<div className='d-flex'>
									<SingleComment
										handleSaveNewComment={this.handleSaveNewComment}
										commentId={mainCommentId}
										currentUserId={currentUserId}
										commentActionId={mainCommentId}
										client={client}
										updateCommentId={mainCommentId}
										node={node}
									/>
								</div>
								<Query query={getComment} variables={{ commentId: mainCommentId }} fetchPolicy='network-only'>
									{({ data, loading: queryLoading, fetchMore }) => {
										if (queryLoading) {
											return <Spin className='collabspin' />;
										}
										const comment = data && data.comment;
										return (
											<React.Fragment>
												<div className='main-comment-reply-container'>
													<section className='pl-50' style={{ paddingBottom: 0 }}>
														{comment &&
															comment.replies &&
															comment.replies.edges.map(({ node }) => {
																const subCommentId = Number(node.id);
																return (
																	<div key={subCommentId}>
																		<SingleComment
																			handleSaveNewComment={this.handleSaveNewComment}
																			commentId={mainCommentId}
																			currentUserId={currentUserId}
																			commentActionId={subCommentId}
																			client={client}
																			updateCommentId={subCommentId}
																			node={node}
																		/>
																	</div>
																);
															})}
														{comment && comment.replies && comment.replies.pageInfo && comment.replies.pageInfo.hasNextPage && (
															<div className='pb-20'>
																<Button type='primary' className='load-more-text' onClick={() => this.loadMore(comment, fetchMore)}>
																	<span style={{ fontSize: '18px', fontWeight: '600' }}>Load more...</span>
																</Button>
															</div>
														)}
													</section>
												</div>
											</React.Fragment>
										);
									}}
								</Query>
							</div>
						);
					})}
			</React.Fragment>
		);
	}
}

export default CommentList;
