import React, { Component } from 'react';
import { MiniArea } from 'ant-design-pro/lib/Charts';
import { Card } from 'antd';
class CampaignReportFunnel extends Component {
	render() {
		const { campaign } = this.props;
		const {
			instagramOwnersInvitedCount,
			instagramOwnersOpenedInviteCount,
			instagramOwnersJoinedCount,
			campaignStaticPostedCount,
			instagramOwnersClickedInviteCount
		} = campaign;
		const spotToFill = campaign.instagramOwnersInvitedCount - campaign.instagramOwnersJoinedCount;
		const visitData = [
			{
				x: 'Influencers invited',
				y: instagramOwnersInvitedCount
			},
			{
				x: 'Opened invitation ',
				y: instagramOwnersOpenedInviteCount
			},
			{
				x: 'Clicked invitation ',
				y: instagramOwnersClickedInviteCount
			},
			{
				x: 'Spots to fill ',
				y: spotToFill
			},
			{
				x: 'Spots filled ',
				y: instagramOwnersJoinedCount
			},
			{
				x: 'Completed campaign ',
				y: campaignStaticPostedCount
			}
		];

		return (
			<section className='campaign-report-funnel-chart pl-10'>
				<h3 className='section-title'>From invitations to spots being filled</h3>
				<Card>
					<div>
						<div className='funnel-chart'>
							<MiniArea line color='#4455E2' height={100} data={visitData} />
						</div>
						<div className='funnel-stats report-funnel-stats'>
							<div>
								<h4>Influencers invited</h4>
								<h2>{instagramOwnersInvitedCount}</h2>
							</div>
							<div>
								<h4>Opened invitation</h4>
								<h2>{instagramOwnersOpenedInviteCount}</h2>
							</div>
							<div>
								<h4>Clicked invitation</h4>
								<h2>{instagramOwnersClickedInviteCount}</h2>
							</div>
							<div>
								<h4>Spots to fill</h4>
								<h2>{spotToFill}</h2>
							</div>
							<div>
								<h4>Spots filled</h4>
								<h2>{instagramOwnersJoinedCount}</h2>
							</div>
							<div>
								<h4>Completed campaign</h4>
								<h2>{campaignStaticPostedCount}</h2>
							</div>
						</div>
					</div>
				</Card>
			</section>
		);
	}
}

export default CampaignReportFunnel;
