import React from 'react';
import { Form, Button, Input } from 'antd';
const { TextArea } = Input;
const ReplyCommentEditor = ({ onChange, onSubmit, onReset, submitting, value, onCancel }) => (
	<div>
		<Form.Item>
			<TextArea rows={4} onChange={onChange} value={value} placeholder='Reply Comment' />
		</Form.Item>
		<Form.Item>
			<div className='d-flex justify-content-end'>
				<Button onClick={onCancel} type='primary'>
					Cancel
				</Button>
				<Button className='ml-10' htmlType='reset' onClick={onReset} type='primary'>
					Reset
				</Button>
				<Button className='ml-10' htmlType='submit' loading={submitting} onClick={onSubmit} type='primary'>
					Add
				</Button>
			</div>
		</Form.Item>
	</div>
);
export default ReplyCommentEditor;
