import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { Menu, Dropdown, Icon, Modal, message } from 'antd';
import removeComment from 'graphql/remove-comment.graphql';
import './report.scss';

class EditDeleteComment extends Component {
	showDeleteConfirm = () => {
		const { commentId, client } = this.props;
		Modal.confirm({
			title: 'Are you sure you want to remove this comment?',
			content: 'A removed comment can not be restored after removal.',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk: () => {
				client
					.mutate({
						mutation: removeComment,
						variables: { id: commentId },
						refetchQueries: ['getComments', 'getComment']
					})
					.then((response) => {
						if (response && response.data && response.data.removeComment && response.data.removeComment.comment) {
							message.success('Comment was removed');
						}
					});
			}
		});
	};

	render() {
		const { handleEditForm } = this.props;
		const menu = (
			<Menu>
				<Menu.Item key='1'>
					<div onClick={() => handleEditForm()}>
						<Icon type='edit' className='mr-10' />
						Edit
					</div>
				</Menu.Item>
				<Menu.Item key='2'>
					<div onClick={this.showDeleteConfirm}>
						<Icon type='delete' className='mr-10 color-red' />
						Delete
					</div>
				</Menu.Item>
			</Menu>
		);
		return (
			<React.Fragment>
				<Dropdown overlay={menu} trigger={['click']} placement='bottomRight'>
					<Icon type='ellipsis' />
				</Dropdown>
			</React.Fragment>
		);
	}
}

export default withApollo(EditDeleteComment);
