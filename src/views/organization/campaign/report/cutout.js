import React, { Component } from 'react';
import { Card, Col, Row } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';
import './styles.scss';

class CampaignReportCutout extends Component {
	render() {
		const { campaign } = this.props;
		const {
			totalPosts,
			totalStories,
			totalTiktoks,
			totalLikeCount,
			totalCommentCount,
			totalImpressionCount,
			totalImpressionStoryCount,
			instagramOwnersJoinedCount,
			totalImpressionTiktokCount
		} = campaign;
		const totalPostsAndStoriesAndTiktoks = totalPosts + totalStories + totalTiktoks;
		const totalLikesAndComments = totalLikeCount + totalCommentCount;
		return (
			<section className='campaign-report-cutout'>
				<h3 className='section-title'>Cutout</h3>
				<Row>
					<Col xs={{ span: 24 }} md={{ span: 12 }}>
						<Card className='gutter-between-box'>
							<div className='violet-box cutout-box pt-20 pb-20'>
								<div className='violet-number sans-serif-number'>
									<ShortAmountFormatter value={instagramOwnersJoinedCount} />
								</div>
								<p className='violet-text'>Influencers</p>
							</div>
						</Card>
					</Col>
					<Col xs={{ span: 24 }} md={{ span: 12 }}>
						<Card className='gutter-between-box'>
							<div className='violet-box cutout-box pt-20 pb-20'>
								<div className='violet-number sans-serif-number'>
									<ShortAmountFormatter value={totalPostsAndStoriesAndTiktoks} />
								</div>
								<p className='violet-text'>Posts & Stories & TikToks</p>
							</div>
						</Card>
					</Col>
				</Row>
				<Row>
					<Col xs={{ span: 24 }} md={{ span: 8 }}>
						<Card className='gutter-between-box'>
							<div className='navy-box cutout-box pt-20 pb-20'>
								<div className='navy-number sans-serif-number'>
									<ShortAmountFormatter value={totalLikesAndComments} />
								</div>
								<p className='navy-text'>Likes & Comments</p>
							</div>
						</Card>
					</Col>
					<Col xs={{ span: 24 }} md={{ span: 8 }}>
						<Card className='gutter-between-box'>
							<div className='navy-box cutout-box pt-20 pb-20'>
								<div className='navy-number sans-serif-number'>
									<ShortAmountFormatter value={totalImpressionStoryCount + totalImpressionTiktokCount} />
								</div>
								<p className='navy-text'>Story views & TikTok plays</p>
							</div>
						</Card>
					</Col>
					<Col xs={{ span: 24 }} md={{ span: 8 }}>
						<Card className='gutter-between-box'>
							<div className='navy-box cutout-box pt-20 pb-20'>
								<div className='navy-number sans-serif-number'>
									<ShortAmountFormatter value={totalImpressionCount} />
								</div>
								<p className='navy-text'>Impressions</p>
							</div>
						</Card>
					</Col>
				</Row>
			</section>
		);
	}
}

export default CampaignReportCutout;
