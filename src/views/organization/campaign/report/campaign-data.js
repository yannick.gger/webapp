import React, { Component } from 'react';
import { Avatar, Card, Col, Row, Tooltip, Button, Progress, Tabs } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';

class CampaignData extends Component {
	render() {
		const { TabPane } = Tabs;
		const { campaign } = this.props;
		const dashedBoxNumber = [0, 1, 2, 3, 4, 5, 6];
		const { totalLikeCount, totalCommentCount, totalImpressionCount } = campaign;
		const engagementPercent = Number(campaign.engagementRate.slice(0, -1));

		const engagementBarRatio = (100 / 12).toFixed(2); // 100 width = 12%
		return (
			<section className='campaign-data'>
				<h3 className='section-title'>Campaign Data</h3>
				<Row>
					<Col xs={{ span: 24 }} xl={{ span: 12 }} className='influ-performance-box card-gutter-10'>
						<Card style={{ backgroundColor: '#1d2029' }}>
							<h3 className='performance-title'>Performance</h3>
							<div>
								<div className='pt-20 d-flex justify-content-center'>
									<div className='circle-progress'>
										<h2 style={{ fontSize: '40px' }}>
											<ShortAmountFormatter value={campaign.totalImpressionCount} />
										</h2>
										<p>Impressions</p>
									</div>
								</div>
								<div className='d-flex pt-30 p-container'>
									<div className='per-progress' style={{ width: '100%' }}>
										<h2 className='percent-score-header'>
											{campaign.engagementRate}
											<span className='performance-percent-text ml-10'>Engagement</span>
										</h2>
										<Progress percent={engagementPercent * engagementBarRatio} strokeColor='#2a7ff5' />
										<p className='per-mini-text'>Total engagement combined</p>
									</div>
								</div>
							</div>
						</Card>
					</Col>
					<Col xs={{ span: 24 }} xl={{ span: 12 }} className='influ-result-box card-gutter-10'>
						<Card style={{ backgroundColor: '#fff' }} className='overflow-fix ant-card-bordered'>
							<h3 className='result-title'>Results</h3>
							<div className='d-flex'>
								<div className='card-container'>
									<Tabs type='card' defaultActiveKey='1' onChange={this.callback}>
										<TabPane tab='Impression' key='1'>
											<div className='line-container mt-30'>
												{dashedBoxNumber.map((dashedBox) => (
													<div key={dashedBox} className='dashedBox'>
														<p className='line' />
													</div>
												))}
											</div>
										</TabPane>
										<TabPane tab='Engagement' key='2'>
											<div className='line-container mt-30'>
												{dashedBoxNumber.map((dashedBox) => (
													<div key={dashedBox} className='dashedBox'>
														<p className='line' />
													</div>
												))}
											</div>
										</TabPane>
									</Tabs>
								</div>
							</div>
						</Card>
					</Col>
				</Row>
			</section>
		);
	}
}

export default CampaignData;
