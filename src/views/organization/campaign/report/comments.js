import React, { Component } from 'react';
import { withApollo, Query } from 'react-apollo';
import { Comment, Avatar, message, Card, Spin, Button } from 'antd';

import addCommentToReport from 'graphql/add-report-comment.graphql';
import getCurrentUser from 'graphql/get-current-user.graphql';
import getComments from 'graphql/get-comments.graphql';
import Editor from './comment-editor.js';
import CommentList from './comment-list.js';
class Comments extends Component {
	state = {
		comments: [],
		submitting: false,
		value: ''
	};

	handleSubmit = () => {
		const { client, campaign } = this.props;
		const { value } = this.state;
		if (!value) {
			return;
		}

		this.setState({
			submitting: true
		});

		client
			.mutate({
				mutation: addCommentToReport,
				variables: {
					campaignId: campaign.id,
					content: value
				},
				refetchQueries: ['getComments']
			})
			.then((response) => {
				if (response && response.data && response.data.commentOnCampaignReport) {
					message.success('Add comment to report successfully');
				} else {
					message.error('Something went wrong');
				}
			})
			.catch(({ graphQLErrors }) => {
				if (graphQLErrors) {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
				}
			});
		this.setState({
			submitting: false,
			value: ''
		});
	};

	handleReset = () => {
		this.setState({
			value: ''
		});
	};

	handleChange = (e) => {
		this.setState({
			value: e.target.value
		});
	};

	loadMore = (campaign, fetchMore) => {
		return fetchMore({
			variables: {
				after: (campaign && campaign.comments && campaign.comments.pageInfo.endCursor) || null
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					campaign: {
						...campaign,
						comments: {
							...fetchMoreResult.campaign.comments,
							edges: [...prev.campaign.comments.edges, ...fetchMoreResult.campaign.comments.edges]
						}
					}
				};
				return result;
			}
		});
	};

	render() {
		const { submitting, value } = this.state;
		const { client } = this.props;
		return (
			<section className='internal-notes'>
				<Query query={getComments} variables={{ campaignId: this.props.campaign.id }} fetchPolicy='network-only'>
					{({ data: commentData, loading: commentsQueryLoading, fetchMore }) => {
						if (commentsQueryLoading && !commentData.campaign) {
							return <Spin className='collabspin' />;
						}
						const campaign = commentData && commentData.campaign;
						const comments = campaign && commentData.campaign.comments;
						const hasNextPage = comments && comments.pageInfo && comments.pageInfo.hasNextPage;
						return (
							<Query query={getCurrentUser} fetchPolicy='network-only'>
								{({ data, loading: queryLoading }) => {
									if (queryLoading) {
										return <Spin className='collabspin' />;
									}
									const user = data && data.currentUser;
									const userNameFirstCharacter = user.name.charAt(0).toUpperCase();
									return (
										<React.Fragment>
											<h3 className='section-title pb-10 pt-20'>Internal notes</h3>
											<Card>
												<div className='comments-container'>
													<CommentList campaign={campaign} client={client} fetchMore={fetchMore} currentUserId={user.id} comments={comments} />
													{hasNextPage && (
														<div className='d-flex justify-content-center pb-10 pt-20'>
															<Button type='primary' onClick={() => this.loadMore(campaign, fetchMore)}>
																<span style={{ fontSize: '18px', fontWeight: '600' }}>Load more comments...</span>
															</Button>
														</div>
													)}
													<Comment
														avatar={<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>{userNameFirstCharacter}</Avatar>}
														content={
															<Editor
																onChange={this.handleChange}
																onSubmit={this.handleSubmit}
																onReset={this.handleReset}
																submitting={submitting}
																value={value}
															/>
														}
													/>
												</div>
											</Card>
										</React.Fragment>
									);
								}}
							</Query>
						);
					}}
				</Query>
			</section>
		);
	}
}

export default withApollo(Comments);
