import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { Comment, Avatar, Tooltip, Button, Input, message } from 'antd';
import moment from 'moment';

import EditDeleteComment from './edit-delete-comment.js';
import updateComment from 'graphql/update-comment.graphql';
import CommentActions from './comment-actions.js';

class SingleComment extends Component {
	state = {
		openEditForm: false,
		value: ''
	};

	handleSave = () => {
		const { updateCommentId, client } = this.props;
		const { value } = this.state;
		this.setState({ openEditForm: false });
		client
			.mutate({
				mutation: updateComment,
				variables: { id: updateCommentId, content: value },
				refetchQueries: ['getComments']
			})
			.then((response) => {
				if (response && response.data && response.data.updateComment && response.data.updateComment.comment) {
					message.success('Comment was saved');
				}
			});
	};

	handleEditForm = () => {
		this.setState({ openEditForm: true });
	};

	handleCloseEditForm = () => {
		this.setState({ openEditForm: false });
	};

	onChange = (e) => {
		this.setState({ value: e.target.value });
	};

	render() {
		const { client, commentId, currentUserId, campaign, commentActionId, updateCommentId, node } = this.props;
		const commentTitleFormat = moment().format('YYYY-MM-DD HH:mm:ss');
		const { content, createdAt, user, likeCount, dislikeCount, currentUserReaction } = node;
		const userNameFirstCharacter = user.name.charAt(0).toUpperCase();
		return (
			<div style={{ width: '100%' }}>
				<div className='d-flex comment-alone-container'>
					<Comment
						author={<p>{user.name}</p>}
						avatar={<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>{userNameFirstCharacter}</Avatar>}
						content={
							this.state.openEditForm ? (
								<div className='pt-10 pb-10'>
									<Input.TextArea rows={4} defaultValue={content} onChange={this.onChange} />
									<div className='d-flex pl-20 pt-20' style={{ justifyContent: 'flex-end' }}>
										<Button type='primary' size='small' className='mr-10' onClick={this.handleSave}>
											Save
										</Button>
										<Button size='small' onClick={this.handleCloseEditForm}>
											Cancel
										</Button>
									</div>
								</div>
							) : (
								<p style={{ color: '#3d3f41' }}>{content}</p>
							)
						}
						datetime={
							<Tooltip title={commentTitleFormat}>
								<span>{moment().from(createdAt)}</span>
							</Tooltip>
						}
					/>
					{user.id === currentUserId && <EditDeleteComment handleEditForm={this.handleEditForm} commentId={updateCommentId} />}
				</div>
				<CommentActions
					campaign={campaign}
					client={client}
					commentId={commentId}
					commentActionId={commentActionId}
					likes={likeCount}
					dislikes={dislikeCount}
					firstCharacter={userNameFirstCharacter}
					reaction={currentUserReaction}
				/>
			</div>
		);
	}
}

export default withApollo(SingleComment);
