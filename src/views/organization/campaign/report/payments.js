import React, { Component } from 'react';
import { Card, Col, Row, Table, Tabs } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';
import { isAdminOrGhostAdmin } from 'services/auth-service';

class CampaignReportPayments extends Component {
	render() {
		const { TabPane } = Tabs;
		const { campaign } = this.props;
		const {
			compensationsTotalCost,
			currency,
			totalImpressionCount,
			totalLikeCount,
			totalCommentCount,
			totalActionCount,
			instagramOwnersPostedCount,
			campaignCompensations,
			productCompensationsCount,
			paymentCompensationsCount,
			invoiceCompensationsCount,
			paymentCompensationsTotalValue,
			invoiceCompensationsTotalValue,
			productCompensationsTotalCost,
			overriddenCostPaid
		} = campaign;
		const cpmValues = `${totalImpressionCount > 0 && compensationsTotalCost > 0 ? (compensationsTotalCost / totalImpressionCount).toFixed(2) : 0} ${currency}`;
		const cpeValues = `${
			totalLikeCount > 0 && totalCommentCount > 0 ? (compensationsTotalCost / (totalLikeCount + totalCommentCount)).toFixed(2) : 0
		} ${currency}`;
		const cpaValues = `${totalActionCount > 0 && compensationsTotalCost > 0 ? (compensationsTotalCost / totalActionCount).toFixed(2) : 0} ${currency}`;
		const totalImpressionValues = `${totalImpressionCount > 0 ? totalImpressionCount * 0.25 : 0} SEK`;
		const productData = campaignCompensations.nodes.filter((node) => node.product !== null);
		const paymentData = campaignCompensations.nodes.filter((node) => node.invoice !== null || node.payment !== null);
		const collabsPaymentDescription = overriddenCostPaid ? 'Fixed price budget for campaign' : '0.25 SEK per 1 Impression';
		const paymentInfluencerDescription = `Paid to influencers by ${paymentCompensationsCount > 0 ? 'card' : 'invoice'}`;
		const paymentToCollabsTotalValues = overriddenCostPaid !== null ? `${overriddenCostPaid.toFixed(2)} SEK` : totalImpressionValues;
		const productShortDescription = `Paid to influencers with ${productCompensationsCount} products`;
		const paymentInfluencerTotalValue = `${(paymentCompensationsTotalValue + invoiceCompensationsTotalValue) * instagramOwnersPostedCount} ${currency}`;
		const paymentToInfluencerColumns = [
			{
				title: 'Product',
				dataIndex: 'product',
				key: 'product',
				render: (record) => {
					return (
						<div className='product-name-wrapper'>
							{record && record.productImages.length > 0 && <img src={record.productImages[0].url} alt={record.name} />}
							<span className='ml-10 product-text'>{record.name}</span>
						</div>
					);
				}
			},
			{
				title: 'Your cost',
				dataIndex: 'product.cost',
				key: 'cost',
				render: (record) => (
					<div className='product-cost-wrapper'>
						<ShortAmountFormatter value={record} /> {currency}
					</div>
				)
			},
			{
				title: 'value',
				dataIndex: 'product.value',
				key: 'value',
				render: (record) => (
					<div className='product-value-wrapper product-text'>
						<ShortAmountFormatter value={record} /> {currency}
					</div>
				)
			}
		];
		const paymentInvoiceOrCardColumns = [
			{
				title: 'Type',
				render: () => {
					return (
						<div className='product-name-wrapper'>
							<span className='ml-10 product-text'>{paymentCompensationsCount > 0 ? 'Payment' : 'Invoice'}</span>
						</div>
					);
				}
			},
			{
				title: paymentCompensationsCount > 0 ? 'Your cost' : 'VAT',
				dataIndex: paymentCompensationsCount > 0 ? 'payment.cost' : 'invoice.vat',
				key: 'cost',
				render: (record) => {
					if (record) {
						return (
							<div className='product-cost-wrapper'>
								<ShortAmountFormatter value={record} /> {paymentCompensationsCount > 0 ? currency : '%'}
							</div>
						);
					} else {
						return <span>__</span>;
					}
				}
			},
			{
				title: 'Value',
				dataIndex: paymentCompensationsCount > 0 ? 'payment.value' : 'invoice.value',
				key: 'value',
				render: (record) => (
					<div className='product-value-wrapper product-text'>
						<span>{`${record} ${currency}`}</span>
					</div>
				)
			}
		];
		const productContent = (
			<div>
				<div className='payment-box product-influencers-wrapper'>
					<h2 className='blue-title'>{`${productCompensationsTotalCost} ${currency}`}</h2>
					<p className='payment-desc'>{productShortDescription}</p>
					<Table dataSource={productData || []} columns={paymentToInfluencerColumns} pagination={false} scroll={{ x: 'max-content' }} />
				</div>
			</div>
		);
		const paymentContent = (
			<div>
				<div className='payment-box payment-influencers-wrapper'>
					<h2 className='blue-title'>{paymentInfluencerTotalValue}</h2>
					<p className='payment-desc'>{paymentInfluencerDescription}</p>
					<Table dataSource={paymentData || []} columns={paymentInvoiceOrCardColumns} pagination={false} scroll={{ x: 500 }} />
				</div>
			</div>
		);
		let paymentToInfluencersNodes = null;
		if (productCompensationsCount > 0 && (paymentCompensationsCount || invoiceCompensationsCount)) {
			paymentToInfluencersNodes = (
				<Card className='payment-influencer-card-wrapper'>
					<Tabs defaultActiveKey='1' onChange={this.callback}>
						<TabPane tab='Product' key='1'>
							{productContent}
						</TabPane>
						<TabPane tab='Payment' key='2'>
							{paymentContent}
						</TabPane>
					</Tabs>
				</Card>
			);
		} else if (productCompensationsCount > 0) {
			paymentToInfluencersNodes = <Card className='payment-influencer-card-wrapper'>{productContent}</Card>;
		} else if (paymentCompensationsCount > 0 || invoiceCompensationsCount > 0) {
			paymentToInfluencersNodes = <Card className='payment-influencer-card-wrapper'>{paymentContent}</Card>;
		} else {
			paymentToInfluencersNodes = <Card className='payment-influencer-card-wrapper'>{productContent}</Card>;
		}
		return isAdminOrGhostAdmin() ? (
			<section className='campaign-report-payments'>
				<Row>
					<Col xs={{ span: 24 }} xl={{ span: 12 }} className='card-gutter-10'>
						<h3 className='section-title'>Payment to influencers</h3>
						{paymentToInfluencersNodes}
					</Col>
					<Col xs={{ span: 24 }} xl={{ span: 12 }} className='card-gutter-10'>
						<h3 className='section-title'>Payment to Collabs</h3>
						<Card style={{ height: '350px', overflow: 'scroll' }}>
							<div className='payment-box payment-collabs-wrapper'>
								<h2 className='blue-title'>{paymentToCollabsTotalValues}</h2>
								<p className='payment-desc'>{collabsPaymentDescription}</p>
								<Card className='payment-collabs-card-inside'>
									<div className='d-flex justify-content-space-between'>
										<div className='d-flex justify-content-space-between flex-column'>
											<div>
												<h3>CPM</h3>
												<p className='value-desc'>Cost per 1000 views</p>
											</div>
											<span>{cpmValues}</span>
										</div>
										<div className='d-flex justify-content-space-between flex-column'>
											<div>
												<h3>CPE</h3>
												<p className='value-desc'>Cost per engagement</p>
											</div>
											<span>{cpeValues}</span>
										</div>
										<div className='d-flex justify-content-space-between flex-column'>
											<div>
												<h3>CPA</h3>
												<p className='value-desc'>Cost per action</p>
											</div>
											<span>{cpaValues}</span>
										</div>
									</div>
								</Card>
							</div>
						</Card>
					</Col>
				</Row>
			</section>
		) : null;
	}
}

export default CampaignReportPayments;
