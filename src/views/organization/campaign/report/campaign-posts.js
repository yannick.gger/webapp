import React, { Component } from 'react';
import { Avatar, Col, Row, Table, Card } from 'antd';
import { Player, BigPlayButton } from 'video-react';
import { getInstagramUrl } from 'utils';
import { ShortAmountFormatter } from 'components/formatters';
import 'video-react/dist/video-react.css';
import './styles.scss';

class CampaignReportPosts extends Component {
	mediaElement = (post) => {
		if (post.mediaType === 'video' || post.mediaType === 'story') {
			return (
				<Col xs={{ span: 24 }} md={{ span: 12 }}>
					<Player src={post.mediaUrl}>
						<BigPlayButton position='center' />
					</Player>
				</Col>
			);
		} else {
			return <Col className='post-image' md={{ span: 12 }} style={{ backgroundImage: `url(${post.thumbnail150x150})` }} />;
		}
	};
	render() {
		const { campaign } = this.props;
		const postColumns = [
			{
				title: 'likes',
				key: 'name',
				dataIndex: 'likeCount',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'comments',
				key: 'comments',
				dataIndex: 'commentsCount',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'reach',
				key: 'reach',
				dataIndex: 'reach',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'impressions',
				key: 'impressions',
				dataIndex: 'impressions',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			}
		];
		const storyColumns = [
			{
				title: 'interactions',
				key: 'interactions',
				dataIndex: 'interactions',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'followers',
				key: 'followers',
				dataIndex: 'followers',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'reach',
				key: 'reach',
				dataIndex: 'reach',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'impressions',
				key: 'impressions',
				dataIndex: 'impressions',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			}
		];
		const tiktokColumns = [
			{
				title: 'likes',
				key: 'name',
				dataIndex: 'likesCount',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'comments',
				key: 'comments',
				dataIndex: 'commentsCount',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'reach',
				key: 'reach',
				dataIndex: 'reach',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			},
			{
				title: 'plays',
				key: 'plays',
				dataIndex: 'playsCount',
				render: (record) => (
					<p>
						<ShortAmountFormatter value={record} />
					</p>
				)
			}
		];
		return (
			<section className='campaign-posts-wrapper'>
				{campaign.assignmentPosts.length > 0 && (
					<Row className='section post-grid mt-50'>
						<Col xs={{ span: 24 }}>
							<div className='section-header'>
								<h2>Posts</h2>
								<p>A handful of posts by the influencers</p>
							</div>
							{campaign.assignmentPosts.map((post) => {
								const postData = campaign.assignmentPosts.filter((instagramPost) => instagramPost.id === post.id);
								return (
									!post.thumbnail150x150.includes('missing.png') && (
										<Row gutter={0} className={`d-flex post-container ${post.alignedStyle ? post.alignedStyle : ''}`} key={post.id}>
											<Col className='post-meta-container' xs={{ span: 24 }} md={{ span: 12 }}>
												<Card>
													<div className='d-flex pb-20'>
														<a href={getInstagramUrl(post.username)} target='_blank' rel='noopener noreferrer'>
															<Avatar size='large' src={post.avatar} />
														</a>
														<div className='pl-10'>
															<a href={getInstagramUrl(post.username)} target='_blank' rel='noopener noreferrer' style={{ color: '#333' }}>
																{post.username}
															</a>
															<div className='post-copy'>
																<p>{post.caption}</p>
															</div>
														</div>
													</div>
													<Table columns={postColumns} dataSource={postData || []} pagination={false} scroll={{ x: 200 }} />
												</Card>
											</Col>
											{this.mediaElement(post)}
										</Row>
									)
								);
							})}
						</Col>
					</Row>
				)}
				{campaign.assignmentStories.length > 0 && (
					<Row className='section post-grid mt-50'>
						<Col xs={{ span: 24 }}>
							<div className='section-header'>
								<h2>Stories</h2>
								<p>A handful of stories by the influencers</p>
							</div>
							{campaign.assignmentStories.map((story) => {
								const storyData = campaign.assignmentStories.filter((instagramStory) => instagramStory.id === story.id);
								return (
									<Row gutter={0} className={`d-flex post-container ${story.alignedStyle ? story.alignedStyle : ''}`} key={story.id}>
										<Col className='post-meta-container' xs={{ span: 24 }} md={{ span: 12 }}>
											<Card>
												<div className='d-flex pb-20'>
													<a href={getInstagramUrl(story.username)} target='_blank' rel='noopener noreferrer'>
														<Avatar size='large' src={story.avatar} />
													</a>
													<div className='pl-10'>
														<a href={getInstagramUrl(story.username)} target='_blank' rel='noopener noreferrer' style={{ color: '#333' }}>
															{story.username}
														</a>
													</div>
												</div>
												<Table columns={storyColumns} dataSource={storyData || []} pagination={false} scroll={{ x: 200 }} />
											</Card>
										</Col>
										{this.mediaElement(story)}
									</Row>
								);
							})}
						</Col>
					</Row>
				)}
				{campaign.assignmentTiktoks.length > 0 && (
					<Row className='section post-grid mt-50'>
						<Col xs={{ span: 24 }}>
							<div className='section-header'>
								<h2>TikTok</h2>
								<p>A handful of TikTok posts by the influencers</p>
							</div>
							{campaign.assignmentTiktoks.map((tiktok) => {
								const tiktokData = campaign.assignmentTiktoks.filter((tiktokPost) => tiktokPost.id === tiktok.id);
								return (
									<Row gutter={0} className={`d-flex post-container ${tiktok.alignedStyle ? tiktok.alignedStyle : ''}`} key={tiktok.id}>
										<Col className='post-meta-container' xs={{ span: 24 }} md={{ span: 12 }}>
											<Card>
												<div className='d-flex pb-20'>
													<a href={getInstagramUrl(tiktok.username)} target='_blank' rel='noopener noreferrer'>
														<Avatar size='large' src={tiktok.avatar} />
													</a>
													<div className='pl-10'>
														<a href={getInstagramUrl(tiktok.username)} target='_blank' rel='noopener noreferrer' style={{ color: '#333' }}>
															{tiktok.username}
														</a>
														<div className='post-copy'>
															<p>{tiktok.caption}</p>
														</div>
													</div>
												</div>
												<Table columns={tiktokColumns} dataSource={tiktokData || []} pagination={false} scroll={{ x: 200 }} />
											</Card>
										</Col>
										{this.mediaElement(tiktok)}
									</Row>
								);
							})}
						</Col>
					</Row>
				)}
				<div className='campaign-report-ending'>
					<p>View the rest of the posts on instagram! We hope you enjoyed the campaign. Thanks for watching.</p>
				</div>
			</section>
		);
	}
}

export default CampaignReportPosts;
