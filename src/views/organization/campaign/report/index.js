import React, { Component } from 'react';
import CampaignReportShortcut from './shortcut.js';
import CampaignReportCutout from './cutout.js';
import CampaignPayments from './payments.js';
import CampaignData from './campaign-data.js';
import CampaignReportFunnel from './report-funnel.js';
import CampaignReportPosts from './campaign-posts.js';
import CampaignReportAssignmentsResult from './assignments-result.js';
import CampaignLayout from '../layout/';
import Comments from './comments.js';

import './report.scss';
class CampaignReport extends Component {
	render() {
		const { campaign, fetchMore } = this.props;
		return (
			<div className='mb-50 report-container'>
				<CampaignReportShortcut campaign={campaign} />
				<CampaignReportCutout campaign={campaign} />
				<CampaignPayments campaign={campaign} />
				<CampaignData campaign={campaign} />
				<CampaignReportFunnel campaign={campaign} />
				<CampaignReportAssignmentsResult campaign={campaign} fetchMore={fetchMore} />
				<CampaignReportPosts campaign={campaign} />
				<Comments campaign={campaign} />
			</div>
		);
	}
}

class CampaignReportWithLayout extends React.Component {
	render() {
		return <CampaignLayout>{({ campaign, fetchMore }) => <CampaignReport campaign={campaign} fetchMore={fetchMore} />}</CampaignLayout>;
	}
}

export default CampaignReportWithLayout;
