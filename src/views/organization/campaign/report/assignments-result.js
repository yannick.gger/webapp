import React from 'react';
import { Table, Tag, Button, Icon } from 'antd';

import { ShortAmountFormatter } from 'components/formatters';

import './styles.scss';

class CampaignReportAssignmentsResult extends React.Component {
	loadMore = (campaign, fetchMore) => {
		return fetchMore({
			variables: {
				after: campaign.assignments.pageInfo.endCursor || null
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					campaign: {
						...campaign,
						assignments: {
							...fetchMoreResult.campaign.assignments,
							edges: [...prev.campaign.assignments.edges, ...fetchMoreResult.campaign.assignments.edges]
						}
					}
				};
				return result;
			}
		});
	};
	render() {
		const { campaign, fetchMore } = this.props;
		const assignmentResultColumns = [
			{
				title: 'assignment',
				key: 'name',
				dataIndex: 'node.name',
				render: (record) => <p>{record}</p>
			},
			{
				title: 'to post',
				key: 'toPost',
				dataIndex: 'node.toPost',
				render: (record) => {
					return (
						<Tag className='assignment-to-post' color={record === 'Story' ? '#267ff8' : '#26bbf8'}>
							{record}
						</Tag>
					);
				}
			},
			{
				title: 'type',
				key: 'postType',
				dataIndex: 'node',
				render: (record) => {
					if (record.instagramType === 'story') {
						return <Icon type='line' />;
					} else {
						return <p className='product-post-type'>{record.postType}</p>;
					}
				}
			},
			{
				title: 'likes',
				key: 'likes',
				dataIndex: 'node',
				render: (record) => {
					if (record.instagramType === 'story') {
						return <Icon type='line' />;
					} else if (record.instagramType === 'tiktok') {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.tiktokStatistics.likesCount} />
							</p>
						);
					} else {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.postStatistics.likeCount} />
							</p>
						);
					}
				}
			},
			{
				title: 'comments',
				key: 'comments',
				dataIndex: 'node',
				render: (record) => {
					if (record.instagramType === 'story') {
						return <Icon type='line' />;
					} else if (record.instagramType === 'tiktok') {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.tiktokStatistics.commentsCount} />
							</p>
						);
					} else {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.postStatistics.commentCount} />
							</p>
						);
					}
				}
			},
			{
				title: 'reach',
				key: 'reach',
				dataIndex: 'node',
				render: (record) => {
					if (record.instagramType === 'story') {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.storyStatistics.reachCount} />
							</p>
						);
					} else if (record.instagramType === 'tiktok') {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.tiktokStatistics.reachCount} />
							</p>
						);
					} else {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.postStatistics.reachCount} />
							</p>
						);
					}
				}
			},
			{
				title: 'impressions',
				key: 'impressions',
				dataIndex: 'node',
				render: (record) => {
					if (record.instagramType === 'tiktok') {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.tiktokStatistics.playsCount} />
							</p>
						);
					} else if (record.instagramType === 'story') {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.storyStatistics.impressionCount} />
							</p>
						);
					} else {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.postStatistics.impressionCount} />
							</p>
						);
					}
				}
			},
			{
				title: 'actions',
				key: 'actions',
				dataIndex: 'node',
				render: (record) => {
					if (record.instagramType === 'story') {
						return (
							<p>
								<ShortAmountFormatter value={record.statistics.storyStatistics.interactionCount} />
							</p>
						);
					} else {
						return <Icon type='line' />;
					}
				}
			}
		];
		return (
			<section className='assignment-result-container pl-10 pr-10'>
				<h3 className='pt-30 section-title'>Assignment Results</h3>
				<Table columns={assignmentResultColumns} dataSource={campaign.assignments.edges || []} pagination={false} scroll={{ x: 1000 }} />
				{campaign.assignments.pageInfo.hasNextPage && (
					<div className='pt-30 text-center'>
						<Button type='primary' onClick={() => this.loadMore(campaign, fetchMore)}>
							Load more
						</Button>
					</div>
				)}
			</section>
		);
	}
}

export default CampaignReportAssignmentsResult;
