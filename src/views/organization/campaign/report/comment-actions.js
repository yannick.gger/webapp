import React, { Component } from 'react';
import { Comment, Avatar, Icon, message } from 'antd';
import Editor from './reply-comment-editor.js';
import replyComment from 'graphql/reply-comment.graphql';
import likeReportComment from 'graphql/like-report-comment.graphql';
import dislikeReportComment from 'graphql/dislike-report-comment.graphql';

class CommentActions extends Component {
	state = {
		openReplyForm: false,
		content: '',
		submitting: false
	};

	handleChange = (e) => {
		this.setState({ content: e.target.value });
	};

	handleReplyComment = () => {
		const { client, commentId } = this.props;
		const { content } = this.state;
		this.setState({ submitting: true });
		client
			.mutate({
				mutation: replyComment,
				variables: {
					commentId,
					content
				},
				refetchQueries: ['getComments']
			})
			.then((response) => {
				if (response && response.data && response.data.replyComment) {
					message.success('Reply comment successfully');
				} else {
					message.error('Something went wrong');
				}
				this.setState({ submitting: false, content: '', openReplyForm: false });
			})
			.catch(({ graphQLErrors }) => {
				if (graphQLErrors) {
					graphQLErrors.forEach((error) => message.error(error.message));
				}
				this.setState({ submitting: false, content: '', openReplyForm: false });
			});
	};

	handleReset = () => {
		this.setState({ content: '' });
	};

	openReplyForm = () => {
		this.setState({ openReplyForm: true });
	};

	hanldeCloseReplyForm = () => {
		this.setState({ openReplyForm: false });
	};

	handleLikeComment = () => {
		const { client, commentActionId } = this.props;
		client.mutate({
			mutation: likeReportComment,
			variables: {
				commentId: commentActionId
			},
			fetchPolicy: 'no-cache',
			refetchQueries: ['getComments']
		});
	};
	handleDislikeComment = () => {
		const { client, commentActionId } = this.props;
		client.mutate({
			mutation: dislikeReportComment,
			variables: {
				commentId: commentActionId
			},
			fetchPolicy: 'no-cache',
			refetchQueries: ['getComments']
		});
	};
	render() {
		const { content, openReplyForm, submitting } = this.state;
		const { likes, dislikes, firstCharacter, reaction } = this.props;
		return (
			<div className='pb-20' style={{ paddingLeft: '45px' }}>
				<div className='d-flex'>
					<div onClick={this.handleLikeComment}>
						{reaction && reaction.reactionType === 'like' ? (
							<div>
								<Icon type='like' theme='filled' />
								<span style={{ paddingLeft: '5px' }}>{likes}</span>
							</div>
						) : (
							<div>
								<Icon type='like' />
								<span style={{ paddingLeft: '5px' }}>{likes}</span>
							</div>
						)}
					</div>
					<div className='pl-10 pr-10' onClick={this.handleDislikeComment}>
						{reaction && reaction.reactionType === 'dislike' ? (
							<div>
								<Icon type='dislike' theme='filled' />
								<span style={{ paddingLeft: '5px' }}>{dislikes}</span>
							</div>
						) : (
							<div>
								<Icon type='dislike' />
								<span style={{ paddingLeft: '5px' }}>{dislikes}</span>
							</div>
						)}
					</div>
					<div onClick={this.openReplyForm} style={{ fontSize: '14px' }}>
						Reply to
					</div>
				</div>
				{openReplyForm && (
					<div className='pt-10'>
						<Comment
							avatar={<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>{firstCharacter}</Avatar>}
							content={
								<Editor
									onChange={this.handleChange}
									onSubmit={this.handleReplyComment}
									onReset={this.handleReset}
									submitting={submitting}
									value={content}
									onCancel={this.hanldeCloseReplyForm}
								/>
							}
						/>
					</div>
				)}
			</div>
		);
	}
}

export default CommentActions;
