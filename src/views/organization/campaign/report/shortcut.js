import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { Link } from 'react-router-dom';
import { Avatar, Card, Col, Row, Tooltip, Icon, message, Spin } from 'antd';

import copyCampaign from 'graphql/copy-campaign.graphql';

class CampaignReportCutout extends Component {
	state = {
		copyCampaignMutationloading: false
	};
	handleCopyCampaign = () => {
		const { client, campaign } = this.props;
		const { id } = campaign;
		this.setState({ copyCampaignMutationloading: true });
		client
			.mutate({
				mutation: copyCampaign,
				variables: { id }
			})
			.then((response) => {
				if (response && response.data && response.data.copyCampaign && response.data.copyCampaign.campaign) {
					message.success('Campaign was copied');
					this.setState({ copyCampaignMutationloading: false });
				} else {
					message.error('Something went wrong');
					this.setState({ copyCampaignMutationloading: false });
				}
			});
	};
	render() {
		const { campaign } = this.props;
		const { copyCampaignMutationloading } = this.state;
		const { id, organization, instagramOwnersJoinedCount } = campaign;
		const amountInfluencers = Math.ceil(campaign.instagramOwnersJoinedCount * 0.4);
		const disabledCard = amountInfluencers > 0 ? '' : 'short-cut-disabled';
		return (
			<section className='shortcuts'>
				<h3 className='section-title'>Shortcuts</h3>
				<Row>
					<Col xs={{ span: 24 }} xl={{ span: 12 }}>
						<Card className='ant-card-bordered overflow-fix gutter-between-box' hoverable={true}>
							<div className={`${disabledCard} d-flex align-items-center justify-content-center`} style={{ height: '33px' }}>
								<Link
									to={{
										pathname: `/${organization.slug}/campaigns/${id}/report/add-top-performers`,
										state: { campaignId: id }
									}}
								>
									<div className='d-flex align-items-center justify-content-center'>
										<div className='avatar-list avatar-size-30'>
											{amountInfluencers > 3 && (
												<Avatar size='medium' key='more' style={{ color: '#fff', backgroundColor: '#c1cad2', lineHeight: '44px' }}>
													+{amountInfluencers - 3}
												</Avatar>
											)}
										</div>
										<div className='ml-15 shortcut-add-top-link'>
											<span className='shortcut-duplicate-text text'>Add top performers to new campaign</span>
										</div>
									</div>
								</Link>
							</div>
						</Card>
					</Col>
					<Col xs={{ span: 24 }} xl={{ span: 12 }}>
						<Card className='ant-card-bordered overflow-fix gutter-between-box' hoverable={true}>
							<div className='d-flex align-items-center justify-content-center' style={{ height: '33px' }} onClick={this.handleCopyCampaign}>
								{copyCampaignMutationloading && <Spin className='collabspin' />}
								<Icon type='copy' className='mr-10' theme='filled' />
								<span className='shortcut-duplicate-text text'>Duplicate this campaign</span>
							</div>
						</Card>
					</Col>
				</Row>
			</section>
		);
	}
}

export default withApollo(CampaignReportCutout);
