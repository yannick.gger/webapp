import React, { Component } from 'react';
import { MiniArea } from 'ant-design-pro/lib/Charts';
import { Card } from 'antd';
import moment from 'moment';

import NumberInfo from 'ant-design-pro/lib/NumberInfo';
import numeral from 'numeral';

const visitData = [];
const beginDay = new Date().getTime();
for (let i = 0; i < 20; i += 1) {
	visitData.push({
		x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
		y: Math.floor(Math.random() * 100) + 10
	});
}

export default class Chart extends Component {
	render() {
		return (
			<React.Fragment>
				<Card className='no-padding'>
					<Card.Meta className='mt-15 ml-15 mb-15' title='Accumulated product worth' />
					<NumberInfo className='px-15' total={numeral(12321).format('0,0')} status='up' subTotal={17.1} />
					<MiniArea line color='#cceafe' height={170} data={visitData} />
				</Card>
			</React.Fragment>
		);
	}
}
