import React from 'react';
import { Row, Col, Card } from 'antd';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';

import { NoInvitedInstagramOwners } from 'components';
import CampaignLayout from '../layout/';
import MyList from './my-list.js';

const GET_INSTAGRAM_OWNERS = gql`
	query getInstagramOwners($campaignId: ID!, $cursor: String, $joined: Boolean) {
		campaign(id: $campaignId) {
			id
			instagramOwners(first: 20, after: $cursor, joined: $joined) @connection(key: "instagramOwners", filter: ["joined"]) {
				pageInfo {
					endCursor
					hasNextPage
				}

				edges {
					node {
						id
						instagramOwnerId
					}
				}
			}
		}
	}
`;

class Influencers extends React.Component {
	state = {
		query: {
			joined: null,
			campaignId: null
		}
	};

	static getDerivedStateFromProps = (props, state) => {
		return { ...state, query: { ...state.query, campaignId: props.match.params.campaignId } };
	};

	handleUpdateQuery = (queryUpdate) => {
		return this.setState((prevState) => ({ query: { ...prevState.query, ...queryUpdate } }));
	};

	render() {
		return (
			<CampaignLayout>
				{({ campaign }) => {
					return (
						<React.Fragment>
							<Row gutter={30}>
								<Col xs={{ span: 24 }} md={{ span: 8 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta title='Influencers in campaign' />
										<h2 className='color-blue mb-0'>30</h2>
									</Card>
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 8 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta title='Posted' />
										<h2 className='color-blue mb-0'>23</h2>
									</Card>
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 8 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta title='Have not posted' />
										<h2 className='color-blue mb-0'>7</h2>
									</Card>
								</Col>
							</Row>
							{campaign.instagramOwnersInvitedCount === 0 && <NoInvitedInstagramOwners />}
							{campaign.instagramOwnersInvitedCount !== 0 && (
								<Query query={GET_INSTAGRAM_OWNERS} variables={this.state.query} notifyOnNetworkStatusChange>
									{({ loading, error, data, fetchMore }) => (
										<MyList
											data={data}
											fetchMore={fetchMore}
											error={error}
											loading={loading}
											campaign={campaign}
											handleUpdateQuery={this.handleUpdateQuery}
											updateHandler={(cache, { data: { removeInstagramOwnersFromCampaign } }) => {
												const { campaign: oldCampaign } = cache.readQuery({ query: GET_INSTAGRAM_OWNERS, variables: this.state.query });
												cache.writeQuery({
													query: GET_INSTAGRAM_OWNERS,
													variables: this.state.query,
													data: {
														campaign: {
															...oldCampaign,
															instagramOwners: {
																...oldCampaign.instagramOwners,
																edges: oldCampaign.instagramOwners.edges.filter(
																	({ node }) => !removeInstagramOwnersFromCampaign.campaignInstagramOwners.map(({ id }) => id).includes(node.id)
																)
															}
														}
													}
												});
											}}
										/>
									)}
								</Query>
							)}
						</React.Fragment>
					);
				}}
			</CampaignLayout>
		);
	}
}

export default withRouter(Influencers);
