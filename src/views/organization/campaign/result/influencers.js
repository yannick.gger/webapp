import React, { Component } from 'react';
import { Row, Col, Menu, Dropdown, Button, Layout } from 'antd';

import MyList from './my-list.js';

const MenuItemGroup = Menu.ItemGroup;
const menu = (
	<Menu>
		<MenuItemGroup title='Add to...'>
			<Menu.Item key='Fitness influencers'>
				<a href='#'>Campaign</a>
			</Menu.Item>
		</MenuItemGroup>
		<Menu.Divider />
		<MenuItemGroup title='Create'>
			<Menu.Item key='Coca cola list'>
				<a href='#'>Duplicate list</a>
			</Menu.Item>
		</MenuItemGroup>
		<Menu.Divider />
		<MenuItemGroup title='Add more'>
			<Menu.Item key='Coca Cola Campaign'>
				<a href='#'>Add more influencers to list</a>
			</Menu.Item>
		</MenuItemGroup>
	</Menu>
);

const DropdownMenu = (
	<React.Fragment>
		<h1>Fitness influencers</h1>
		<Dropdown overlay={menu} trigger={['click']} placement='bottomRight'>
			<Button type='primary' icon=' ion ion-plus' size='large' />
		</Dropdown>
	</React.Fragment>
);

export default class ListsView extends Component {
	state = { visibleModal: false };
	showModal = () => {
		this.setState({
			visibleModal: true
		});
	};

	render() {
		return (
			<Layout>
				<Layout.Header>
					<DropdownMenu />
				</Layout.Header>
				<Layout.Content>
					<Row className=''>
						<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 6 }}>
							<h4>Influencers in list</h4>
							<h2 className='color-blue'>213 Influencers</h2>
						</Col>
						<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 6 }}>
							<h4>Est. Reach</h4>
							<h2 className='color-blue'>1 231 142</h2>
						</Col>
						<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 6 }}>
							<h4>Est. Engagement</h4>
							<h2 className='color-blue'>4%</h2>
						</Col>
						<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 6 }}>
							<h4>Est. CPE based on $150</h4>
							<h2 className='color-blue'>$0,04</h2>
						</Col>
					</Row>
					<MyList />
				</Layout.Content>
			</Layout>
		);
	}
}
