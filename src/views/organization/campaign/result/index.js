import React from 'react';
import { Row, Col, Card } from 'antd';

import { Link } from 'react-router-dom';

import CampaignLayout from '../layout/';
import AssignmentCard from 'components/assignment-card/';

export default class Results extends React.Component {
	filterInfluencerAssignmentByAssignmentId = (assignmentId, influencerAssignments) => {
		if (influencerAssignments.length === 0) return [];

		return influencerAssignments.filter(({ node }) => node.assignment.id === assignmentId);
	};

	render() {
		return (
			<CampaignLayout>
				{({ campaign }) => {
					const campaignAssignments = campaign.assignments.edges;

					return (
						<React.Fragment>
							<Row gutter={30} className='mb-20'>
								<Col xs={{ span: 24 }} md={{ span: 8 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta title='Assignments total' />
										<h2 className='color-blue mb-0'>{campaign.influencersCount} assignments</h2>
									</Card>
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 8 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta title='Completed all assignments' />
										<h2 className='color-blue mb-0'>{campaign.completedAllAssignmentsInfluencersNumber} influencers</h2>
									</Card>
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 8 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta title='Has not completed all' />
										<h2 className='color-blue mb-0'>{campaign.notCompletedAllAssignmentsInfluencersNumber} influencers</h2>
									</Card>
								</Col>
							</Row>

							{campaignAssignments.length > 0 && (
								<Row gutter={30}>
									{campaignAssignments.map(({ node }) => (
										<Col xs={{ span: 24 }} md={{ span: 8 }} className='mb-30' key={node.id}>
											<Link to={`/${this.props.match.params.organizationSlug}/campaigns/${campaign.id}/result/assignment/${node.id}`}>
												<AssignmentCard
													assignmentNode={node}
													campaignInstagramOwnerAssignments={this.filterInfluencerAssignmentByAssignmentId(
														node.id,
														campaign.campaignInstagramOwnerAssignments.edges
													)}
												/>
											</Link>
										</Col>
									))}
								</Row>
							)}
						</React.Fragment>
					);
				}}
			</CampaignLayout>
		);
	}
}
