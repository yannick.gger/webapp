import React, { Component } from 'react';
import { Menu, Dropdown, Icon, message, Button } from 'antd';
import { Mutation } from 'react-apollo';
import { injectIntl, intlShape } from 'react-intl';
import { withRouter } from 'react-router-dom';

import gql from 'graphql-tag';

const REMOVE_INSTAGRAM_OWNERS = gql`
	mutation removeInstagramOwnersFromCampaign($campaignId: ID!, $instagramOwnerIds: [ID]!) {
		removeInstagramOwnersFromCampaign(input: { campaignId: $campaignId, instagramOwnerIds: $instagramOwnerIds }) {
			campaign {
				id
				name
			}
			campaignInstagramOwners {
				id
			}
		}
	}
`;

const handleMenu = ({ campaign, removeInstagramOwnersFromCampaign, instagramOwners }) => (
	<Menu>
		<Menu.ItemGroup title='Action'>
			<Menu.Item key='Remove' data-ripple='rgba(0, 0, 0, 0.3)'>
				<a
					href='#'
					onClick={() =>
						campaign.invitesSent
							? message.error("Influencers can't be removed after campaign invite is sent.")
							: removeInstagramOwnersFromCampaign({
									variables: { campaignId: campaign.id, instagramOwnerIds: instagramOwners }
							  })
					}
				>
					Remove from campaign
				</a>
			</Menu.Item>
		</Menu.ItemGroup>
	</Menu>
);

class HandleSelected extends Component {
	render() {
		const { campaign, updateHandler } = this.props;
		const instagramOwners = this.props.selected;

		return (
			<Mutation
				mutation={REMOVE_INSTAGRAM_OWNERS}
				update={updateHandler}
				onCompleted={({ removeInstagramOwnersFromCampaign }) => {
					if (this.props.handleSuccess) {
						this.props.handleSuccess();
					}
					message.success(
						this.props.intl.formatMessage(
							{
								id: 'add-instagram-owners-to-campaign-success',
								defaultMessage: `Removed {count} {count, plural, one {influencer} other {influencers}} from your campaign "{campaignName}"`
							},
							{ count: instagramOwners.length, campaignName: removeInstagramOwnersFromCampaign.campaign.name }
						)
					);
				}}
			>
				{(removeInstagramOwnersFromCampaign, { loading }) => (
					<React.Fragment>
						<Dropdown
							disabled={!instagramOwners || instagramOwners.length === 0}
							overlay={handleMenu({ campaign, removeInstagramOwnersFromCampaign, instagramOwners })}
							trigger={['click']}
							loading={loading}
						>
							<Button type='primary' data-ripple='rgba(0, 0, 0, 0.3)' size='small' loading={loading}>
								Handle all selected... <Icon type='down' />
							</Button>
						</Dropdown>
						<span style={{ marginLeft: 8 }}>{instagramOwners && instagramOwners.length !== 0 ? `Selected ${instagramOwners.length} items` : ''}</span>
					</React.Fragment>
				)}
			</Mutation>
		);
	}
}

HandleSelected.propTypes = {
	intl: intlShape.isRequired
};

export default withRouter(injectIntl(HandleSelected));
