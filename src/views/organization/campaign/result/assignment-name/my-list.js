import React, { Component } from 'react';
import { Table, Button, Tag, Icon, Avatar, Radio, Row, Col, Empty } from 'antd';
import HandleSelected from './handle-selected';
import { ShortAmountFormatter } from 'components/formatters';
import TableErrorImg from 'assets/img/app/table-error.svg';

const InstagramUrl = 'https://instagram.com/';

const columns = [
	{
		title: '',
		dataIndex: 'node.instagramOwner.avatar',
		width: '60px',
		render: (text, record) => <Avatar size='large' src='#' icon='user' />
	},
	{
		title: 'Name',
		dataIndex: 'node.instagramOwner.instagramOwnerUser.username',
		width: '180px',
		render: (text, record) => (
			<a href='#' target='_blank' rel='noopener noreferrer'>
				{record.name}
			</a>
		)
	},
	{
		title: 'Followers',
		dataIndex: 'node.instagramOwner.followedByCount',
		render: (text, record) => <ShortAmountFormatter value={record.node.instagramOwner.followedByCount} />
	},
	{
		title: 'Engagement',
		dataIndex: 'node.instagramOwner.interactionRate'
	},
	{
		title: 'Status',
		key: 'node.instagramOwner.categories',
		render: (text, record) => (
			<span className='color-green' style={{ fontSize: '14px' }}>
				POSTED 1/3 <Icon className='color-green' type='check' />
			</span>
		)
	}
];

class ListsView extends Component {
	state = {
		selectedRowKeys: [], // Check here to configure the default column
		initialLoading: false,
		moreLoading: false
	};
	onSelectChange = (selectedRowKeys) => {
		this.setState({ selectedRowKeys });
	};
	loadMore = (data, fetchMore) => {
		this.setState({ moreLoading: true });
		fetchMore({
			variables: {
				cursor: data.campaign.instagramOwners.pageInfo.endCursor
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				this.setState({ moreLoading: false });

				const newEdges = fetchMoreResult.campaign.instagramOwners.edges;
				const pageInfo = fetchMoreResult.campaign.instagramOwners.pageInfo;

				return newEdges.length
					? {
							campaign: {
								...previousResult.campaign,
								instagramOwners: {
									__typename: previousResult.campaign.instagramOwners.__typename,
									edges: [...previousResult.campaign.instagramOwners.edges, ...newEdges],
									pageInfo
								}
							}
					  }
					: previousResult;
			}
		});
	};
	componentDidUpdate = (prevProps) => {
		if (prevProps.loading !== this.props.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.loading });
		}
	};
	componentDidMount = () => {
		if (this.props.loading) {
			this.setState({ initialLoading: true });
		}
	};
	render() {
		const { error, data, fetchMore, handleUpdateQuery, campaign, updateHandler } = this.props;
		const { selectedRowKeys, initialLoading, moreLoading } = this.state;
		const rowSelection = {
			selectedRowKeys,
			onChange: this.onSelectChange
		};

		return (
			<div>
				<div className='mb-30 mt-30 border-top pt-30'>
					<Row type='flex' justify='space-between' align='middle'>
						<Col md={{ span: 8 }}>
							<HandleSelected
								selected={selectedRowKeys}
								campaign={campaign}
								updateHandler={updateHandler}
								handleSuccess={() => {
									this.setState({ selectedRowKeys: [] });
								}}
							/>
						</Col>
						<Col md={{ span: 8 }} />
						<Col>
							<span className='mr-20'>Filter by:</span>
							<Radio.Group
								defaultValue='all'
								className='radio-pills'
								onChange={(e) => {
									if (e.target.value === 'joined') handleUpdateQuery({ joined: true });
									if (e.target.value === 'all') handleUpdateQuery({ joined: null });
									if (e.target.value === 'invited') handleUpdateQuery({ joined: false });
								}}
							>
								<Radio.Button className='rippledown' value='all'>
									All
								</Radio.Button>
								<Radio.Button className='rippledown' value='joined'>
									Posted
								</Radio.Button>
								<Radio.Button className='rippledown' value='invited'>
									Not posted
								</Radio.Button>
							</Radio.Group>
						</Col>
					</Row>
				</div>

				<Table
					rowSelection={rowSelection}
					columns={columns}
					dataSource={data && data.campaign ? data.campaign.instagramOwners.edges : []}
					rowKey={(record) => record.node.instagramOwner.id}
					pagination={false}
					loading={initialLoading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load users' />
						) : (
							<Empty description='Nothing matching the search criterias found' />
						)
					}}
				/>

				{!error &&
					!initialLoading &&
					data &&
					data.campaign &&
					data.campaign.instagramOwners.edges.length !== 0 &&
					data.campaign.instagramOwners.edges.length % 20 === 0 && (
						<div className='pt-30 pb-30 text-center'>
							<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(data, fetchMore)}>
								Load more
							</Button>
						</div>
					)}
			</div>
		);
	}
}

export default ListsView;
