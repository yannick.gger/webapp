import React from 'react';
import { Row, Col, Radio, Divider } from 'antd';

import CampaignLayout from '../../layout/';
import ResultCard from 'components/result-card/';

export default class Results extends React.Component {
	render() {
		return (
			<CampaignLayout>
				{() => {
					return (
						<React.Fragment>
							<Row>
								<Col xs={{ span: 24 }} md={{ span: 16 }}>
									<Radio.Group defaultValue='1' className='radio-pills fl'>
										<Radio.Button className='rippledown' value='1'>
											Assignment 1
										</Radio.Button>
										<Radio.Button className='rippledown' value='2'>
											Assignment 2
										</Radio.Button>
										<Radio.Button className='rippledown' value='3'>
											Assignment 3
										</Radio.Button>
									</Radio.Group>
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 8 }}>
									<Radio.Group defaultValue='joined' className='radio-pills fr'>
										<Radio.Button className='rippledown' value='joined'>
											Posted
										</Radio.Button>
										<Radio.Button className='rippledown' value='not-joined'>
											Not posted
										</Radio.Button>
									</Radio.Group>
								</Col>
							</Row>
							<Divider className='mt-20' />
							<Row gutter={30}>
								<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 8 }} xxl={{ span: 6 }} className='mb-30'>
									<ResultCard />
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 8 }} xxl={{ span: 6 }} className='mb-30'>
									<ResultCard />
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 8 }} xxl={{ span: 6 }} className='mb-30'>
									<ResultCard />
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 12 }} xl={{ span: 8 }} xxl={{ span: 6 }} className='mb-30'>
									<ResultCard />
								</Col>
							</Row>
						</React.Fragment>
					);
				}}
			</CampaignLayout>
		);
	}
}
