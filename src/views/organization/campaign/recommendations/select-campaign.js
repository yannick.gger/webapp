import { Layout, Select, Button, Tag } from 'antd';
import { Link } from 'react-router-dom';

import { Component, default as React } from 'react';
import { withRouter } from 'react-router-dom';
import './styles.scss';

class SelectCampaign extends Component {
	render() {
		const { onCampaignChange, campaigns, loading, campaign } = this.props;
		const campaignId = this.props.match.url.split('/')[3];
		const limit = 10;
		return (
			<Layout.Header style={{ background: '#fff' }}>
				<h4>Finding influencers for</h4>
				<div className='filter selected-value-dark'>
					<Select
						showSearch
						className='custom-select rippledown campaigns-list'
						style={{ width: '100%' }}
						placeholder='Select campaign'
						loading={loading}
						value={campaign ? campaignId : undefined}
						optionFilterProp='children'
						filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						onChange={(value) => onCampaignChange(campaigns.find(({ id }) => id === value))}
						dropdownRender={(menu) => (
							<React.Fragment>
								{menu}
							</React.Fragment>
						)}
					>
						{campaigns &&
							campaigns.map((campaign, index) => {
								if (index < limit) {
									return (
										<Select.Option key={campaign.id} value={campaign.id} style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
											<span>{campaign.name}</span>

											{campaign.active ? (
												<Tag color='green' style={{ marginLeft: '1.5em' }}>
													Active
												</Tag>
											) : campaign.draft ? (
												<Tag color='orange' style={{ marginLeft: '1.5em' }}>
													Draft
												</Tag>
											) : (
												''
											)}
										</Select.Option>
									);
								}
							})}
					</Select>
				</div>
			</Layout.Header>
		);
	}
}

export default withRouter(SelectCampaign);
