import React, { Component } from 'react';
import { Drawer, notification, Modal, Button } from 'antd';
import { compose, graphql } from 'react-apollo';
import { fromJS } from 'immutable';
import InfluencerFilterForm, { defaultMinFollowers, defaultMaxFollowers } from './influencer-filter-form';
import getCategoryQuery from 'graphql/get-category.graphql';
import updateCampaignInfluencerFilterMutation from 'graphql/update-campaign-influence-filter.graphql';

class InfluencerFilter extends Component {
	state = {
		submitting: false
	};

	onClose = (ignore = false) => {
		if (ignore) {
			this.props.onClose();
		} else if (this.influencerFilterForm) {
			const filters = { ...this.props.filters };
			filters.minFollowers = parseInt(filters.minFollowers) || defaultMinFollowers;
			filters.maxFollowers = parseInt(filters.maxFollowers) || defaultMaxFollowers;
			filters.countries = (filters.countries || []).map(({ id }) => id);
			filters.audienceGender = filters.audienceGender || 'all_genders';
			filters.categories = (filters.categories || []).map(({ index }) => index);
			filters.audienceAgeGroups = (filters.audienceAgeGroups || []).map(({ id }) => id);
			filters.audienceCountries = (filters.audienceCountries || []).map(({ id }) => id);

			const formData = { ...this.influencerFilterForm.props.form.getFieldsValue() };
			formData.minFollowers = formData.followers[0];
			formData.maxFollowers = formData.followers[1];

			const fields = ['countries', 'categories', 'minFollowers', 'maxFollowers', 'audienceGender', 'audienceCountries', 'audienceAgeGroups'];

			if (fields.filter((key) => !fromJS({ key: formData[key] }).equals(fromJS({ key: filters[key] })))[0]) {
				Modal.confirm({
					okText: 'Yes',
					cancelText: 'No',
					okButtonProps: { type: 'danger' },
					title: 'Do you want to close without saving?',
					content: 'You have unsaved changes that will be resetted if you continue',
					onCancel: () => {},
					onOk: () => this.props.onClose()
				});
			} else {
				this.props.onClose();
			}
		} else {
			this.props.onClose();
		}
	};

	handleSubmit = (e) => {
		e.preventDefault();
		this.influencerFilterForm.props.form.validateFieldsAndScroll(async (err, formData) => {
			if (!err) {
				const graphQlValues = {
					campaignId: this.props.campaignId,
					campaignInfluencerFilter: {
						countries: formData.countries,
						categories: formData.categories,
						minFollowers: formData.followers[0],
						maxFollowers: formData.followers[1],
						audienceGender: formData.audienceGender,
						audienceCountries: formData.audienceCountries,
						audienceAgeGroups: formData.audienceAgeGroups
					}
				};

				try {
					this.onClose(true);
					this.props.onRefetchFilter(true);
					const { data } = await this.props.updateCampaignInfluencerFilter({ variables: graphQlValues, awaitRefetchQueries: true });

					if (data && data.updateCampaignInfluencerFilter.errors.length) {
						notification.error({
							message: 'Filter saving',
							description: (
								<div>
									{data.updateCampaignInfluencerFilter.errors.map((item, index) => (
										<p key={index}>{item.message}</p>
									))}
								</div>
							)
						});
					} else {
						this.props.onRefetchFilter(false);
						document.dispatchEvent(new Event('recommendationRefetch'));
					}
				} catch (err) {
					this.props.onRefetchFilter(false);
					console.log('graphQlValues ERROR:', err);
				}
			} else {
				console.log('ERROR:', err);
			}
		});
	};

	handleReset = (e) => {
		e.preventDefault();
		this.influencerFilterForm.props.form.resetFields();
	};

	get categories() {
		const { loading, error, categories } = this.props.getCategory || {};
		return loading || !!error ? {} : categories.reduce((obj, category) => ({ ...obj, [`${category.index}`]: category }), {});
	}

	render() {
		return (
			<React.Fragment>
				{this.props.displayButton && (
					<Button className='btn-rounded btn-xs' onClick={this.props.showDrawer}>
						Change filters
					</Button>
				)}
				<Drawer
					width={960}
					title='Filters'
					placement='left'
					maskClosable={false}
					visible={this.props.visible}
					onClose={() => this.onClose()}
					className='influencer-filters-recommendation-campaign-organization-views filter-drawer'
				>
					{this.props.visible && (
						<InfluencerFilterForm
							categories={this.categories}
							filters={this.props.filters}
							submitting={this.state.submitting}
							activeKey={[this.props.activeKey]}
							onSubmit={(e) => this.handleSubmit(e)}
							wrappedComponentRef={(ref) => (this.influencerFilterForm = ref)}
						/>
					)}
				</Drawer>
			</React.Fragment>
		);
	}
}

export default compose(
	graphql(getCategoryQuery, { name: 'getCategory' }),
	graphql(updateCampaignInfluencerFilterMutation, { name: 'updateCampaignInfluencerFilter', options: ({ refetchQueries }) => ({ refetchQueries }) })
)(InfluencerFilter);
