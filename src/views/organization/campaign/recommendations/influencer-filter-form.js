import React, { Component } from 'react';
import { Form, Slider, Radio, Select, Row, Col, Spin, Icon, Collapse, Button } from 'antd';
import { countries } from 'country-data';
import { fromJS } from 'immutable';

const FormItem = Form.Item;
const Panel = Collapse.Panel;
const Option = Select.Option;

export const defaultCountries = [{ id: 'SE', name: 'Sweden' }];
// When updating default min/max values, make sure to update slider steps
export const defaultMinFollowers = 1800;
export const defaultMaxFollowers = 1000 * 1000;
// Slider stepsize must be a number that the range is divisable with
// Must fulfill: (max-min) % step = 0
export const sliderStepFollowers = 700;

class InfluencerFilterForm extends Component {
	state = {
		activeKey: [],
		followedByCountMin: defaultMinFollowers,
		followedByCountMax: defaultMaxFollowers
	};

	static getDerivedStateFromProps(nextProps, preState) {
		if (fromJS({ key: nextProps.activeKey }).equals(fromJS({ key: preState.reActiveKey }))) {
			return preState;
		}

		return { ...preState, activeKey: nextProps.activeKey, reActiveKey: nextProps.activeKey };
	}

	get followersDefault() {
		const minFollowers = parseInt(this.props.filters.minFollowers, 10) || defaultMinFollowers;
		const maxFollowers = parseInt(this.props.filters.maxFollowers, 10) || defaultMaxFollowers;

		return [minFollowers, maxFollowers];
	}

	get renderCountry() {
		const countryData = [...countries.all.filter((country) => country.status === 'assigned').toSort('name')];

		return countryData.map((country) => (
			<Option key={country.alpha2 || 'all-opyion'} value={country.alpha2} name={country.name}>
				{country.name}
			</Option>
		));
	}

	handleReset = (e) => {
		e.preventDefault();
		this.props.form.resetFields();
	};

	onCollapse = (activeKey) =>
		this.setState({
			activeKey
		});

	callback = (key) => {
		console.log(key);
	};
	render() {
		const renderCountryOption = this.renderCountry;
		const { submitting, categories, filters, loading } = this.props;
		const { getFieldDecorator, getFieldValue } = this.props.form;
		const { followedByCountMin, followedByCountMax } = this.state;
		const followers = getFieldValue('followers') || this.followersDefault;
		const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };
		const renderCategoryText = (getFieldValue('categories') || []).map((key) => (categories[key] || {}).name).join(' + ');

		return (
			<Form className='influencer-filter-form horizontal-text-form filter'>
				<Collapse onChange={this.callback} defaultActiveKey={['1', '2']} className='ant-collapse-naked'>
					<Panel header='Influencers' key='1'>
						<FormItem {...formItemLayout} label='I want influencers in category'>
							{getFieldDecorator('categories', {
								initialValue: (filters.categories || []).map((item) => item.index)
							})(
								<Select
									mode='multiple'
									disabled={submitting}
									optionFilterProp='name'
									placeholder='All categories'
									className='custom-select bordered rippledown'
									style={{ width: '100%', maxWidth: '800px' }}
									notFoundContent={loading ? <Spin size='small' /> : 'Could not load categories'}
								>
									{Object.values(categories).map((category) => (
										<Option key={category.index || category.id || ['all-opyion']} value={category.index} name={category.name}>
											{category.name}
										</Option>
									))}
								</Select>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label='Who live in'>
							{getFieldDecorator('countries', {
								initialValue: (filters.countries || []).map((item) => item.id)
							})(
								<Select
									mode='multiple'
									disabled={submitting}
									optionFilterProp='name'
									placeholder='All countries'
									className='custom-select bordered rippledown'
									style={{ width: '100%', maxWidth: '800px' }}
								>
									{renderCountryOption}
								</Select>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label='With followers between'>
							{getFieldDecorator('followers', {
								initialValue: this.followersDefault
							})(
								<Slider
									range
									min={followedByCountMin}
									step={sliderStepFollowers}
									max={followedByCountMax}
									className='fl mt-5 mr-30'
									style={{ width: '300px' }}
									disabled={submitting}
								/>
							)}
							{<div>{[followers[0].toShort(), followers[1].toShort()].join(' - ')}</div>}
						</FormItem>
					</Panel>
					<Panel header='Engaged Audience' key='2'>
						<FormItem {...formItemLayout} label='I want to target'>
							{getFieldDecorator('audienceGender', {
								initialValue: filters.audienceGender || 'all_genders'
							})(
								<Radio.Group buttonStyle='solid' size='large' disabled={submitting}>
									<Radio.Button value='male'>
										<Icon type='man' /> Males
									</Radio.Button>
									<Radio.Button value='female'>
										<Icon type='woman' /> Females
									</Radio.Button>
									<Radio.Button value='all_genders'>All</Radio.Button>
								</Radio.Group>
							)}
						</FormItem>
						<Row style={{ display: 'flex', alignItems: 'center' }}>
							<Col span={8}>
								<p className='caption'>That like</p>
							</Col>
							{renderCategoryText && (
								<Col span={16}>
									<h3 className='mb-0'>{renderCategoryText}</h3> <span className='color-gray'>(Same as influencer category)</span>
								</Col>
							)}
						</Row>
						<FormItem {...formItemLayout} label='Who live in'>
							{getFieldDecorator('audienceCountries', {
								initialValue: (filters.audienceCountries || []).map((item) => item.id)
							})(
								<Select
									mode='multiple'
									disabled={submitting}
									id='regionFilter'
									optionFilterProp='name'
									placeholder='All countries'
									className='custom-select bordered rippledown'
									style={{ width: '100%', maxWidth: '800px' }}
								>
									{renderCountryOption}
								</Select>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label='In age span'>
							{getFieldDecorator('audienceAgeGroups', {
								initialValue: (filters.audienceAgeGroups || []).map((item) => item.id)
							})(
								<Select
									mode='multiple'
									disabled={submitting}
									placeholder='All ages'
									optionFilterProp='name'
									className='custom-select bordered rippledown'
									style={{ width: '100%', maxWidth: '800px' }}
									filterOption={(input, option) => parseInt(input, 10).between(option.props.min, option.props.max + 1)}
								>
									<Option min={13} max={17} value='13-17'>
										13 - 17
									</Option>
									<Option min={17} max={24} value='18-24'>
										18 - 24
									</Option>
									<Option min={25} max={34} value='25-34'>
										25 - 34
									</Option>
									<Option min={35} max={44} value='35-44'>
										35 - 44
									</Option>
									<Option min={45} max={54} value='45-54'>
										45 - 54
									</Option>
									<Option min={56} max={64} value='55-64'>
										55 - 64
									</Option>
									<Option min={65} max={10000} value='65-+'>
										65-+
									</Option>
								</Select>
							)}
						</FormItem>
					</Panel>
				</Collapse>
				<div
					style={{
						left: 0,
						bottom: 0,
						zIndex: 10,
						width: '100%',
						textAlign: 'right',
						position: 'absolute',
						padding: '18px 20px',
						background: '#FFFFFF'
					}}
				>
					<Button disabled={submitting} type='gray' className='mr-20' onClick={this.handleReset}>
						Reset
					</Button>
					<Button disabled={submitting} type='primary' loading={submitting} onClick={this.props.onSubmit}>
						Save
					</Button>
				</div>
			</Form>
		);
	}
}

export default Form.create()(InfluencerFilterForm);
