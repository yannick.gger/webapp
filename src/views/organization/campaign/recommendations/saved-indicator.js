import React, { Component } from 'react';

export default class SavedIndicator extends Component {
	state = {
		saved: false
	};

	componentDidMount() {
		document.addEventListener('recommendationAction', this.onSubscribe);
	}

	componentWillUnmount() {
		document.removeEventListener('recommendationAction', this.onSubscribe);
	}

	onSubscribe = () => {
		this.setTimeoutDisplay && clearTimeout(this.setTimeoutDisplay);
		this.setTimeoutDisplay = setTimeout(() => {
			this.isMounted && this.setState({ saved: true });

			this.setTimeoutHide && clearTimeout(this.setTimeoutHide);
			this.setTimeoutHide = setTimeout(() => {
				this.isMounted && this.setState({ saved: false });
			}, 3000);
		}, 5000);
	};

	render() {
		return this.state.saved ? <p className='color-gray mb-0'>Saved!</p> : null;
	}
}
