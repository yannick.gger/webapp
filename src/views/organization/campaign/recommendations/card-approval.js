import React, { Component } from 'react';
import './card-approval.scss';

export default class CardApproval extends Component {
	add = () => {};
	skip = () => {};

	render() {
		const { children, currentIndex } = this.props;

		return (
			<div className='card-approval'>
				{children.map((child, index) =>
					index > currentIndex - 2 ? (
						<div
							className={`card-item ${index === currentIndex ? 'card-approval-active' : ''} ${index < currentIndex ? 'card-approval-processed' : ''} ${
								currentIndex < index ? `next-${index - currentIndex}` : ''
							} ${child.props.result || ''} ${currentIndex + 3 < index ? `hidden` : ''} ${child.props.result === 'skip' ? 'card-reject-class' : ''} ${
								child.props.result === 'add' ? 'card-accept-class' : ''
							}`}
							key={child.key}
						>
							{index < currentIndex ? <div className={`card-approval-overlay`} /> : child}
						</div>
					) : (
						<div key={child.key} />
					)
				)}
			</div>
		);
	}
}
