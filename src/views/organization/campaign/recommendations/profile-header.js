import React, { Component } from 'react';
import { Avatar, Button, Row, Col, Tooltip, Icon } from 'antd';
import { lookup } from 'country-data';
import { VerifiedIcon } from 'components/icons';
import { getEngagementRate, getInfluencerScore, getAudienceMatch, getEstimatedImpressions, renderScoreText } from 'utils';
import InstagramProfileDrawer from 'components/instagram-profile-drawer';

const DottedLineSvg = () => (
	<svg width='1px' height='38px' viewBox='0 0 1 38'>
		<path d='M0.5,0.31 L0.5,37' stroke='#74797F' opacity='0.5' strokeDasharray='0,6' strokeLinecap='square' />
	</svg>
);

const DottedLine = (props) => <Icon component={DottedLineSvg} {...props} />;

const inflScoreDescription = (
	<React.Fragment>
		<p style={{ color: '#fff', marginBottom: '0' }}>
			This is Collabs influencer score. It is a combination of credibility score (amount of real followers), campaign success rate, is open for collaboration
			and other proprietary data.
		</p>
	</React.Fragment>
);

export default class ProfileHeader extends Component {
	get audience() {
		const data = {
			ages: [],
			countries: [],
			gender: 'all_genders'
		};
		const { countries, gender, ages } = (document.querySelector('.recommendation-sidebar-audience-countries') || {}).dataset || {};

		try {
			data.ages = JSON.parse(ages || '[]');
			data.gender = gender || 'all_genders';
			data.countries = JSON.parse(countries || '[]');
		} catch (e) {
			console.log('parse data erorr:', e);
		}

		return data;
	}

	render() {
		const { countries, ages, gender } = this.audience;
		const { instagramOwner, instagramProfile } = this.props;
		const headerOptions = {
			influencerScoreValue: 5,
			audienceMatchValue: null,
			interactionRate: instagramOwner.interactionRate
		};

		const { interactionRate, audienceMatchValue, influencerScoreValue } = headerOptions;
		const engagementRate = getEngagementRate(interactionRate);
		const audienceMatch = getAudienceMatch(audienceMatchValue);
		const influencerScore = getInfluencerScore(influencerScoreValue);

		return (
			<div className='mt-20 mb-5'>
				<div className='influencer-profile'>
					<Avatar size={64} src={instagramOwner.instagramOwnerUser.avatar} style={{ marginRight: '10px' }} />
					<div className='profile-data'>
						<div style={{ display: 'flex' }}>
							<h3>
								<InstagramProfileDrawer instagramOwner={instagramOwner} content={`@${instagramProfile.username}`} />
								{instagramProfile.is_verified && <Icon component={VerifiedIcon} style={{ color: '#3C96ED', marginLeft: '5px' }} />}
							</h3>
							<a href={`https://www.instagram.com/${instagramProfile.username}/`} target='_blank' rel='noopener noreferrer'>
								<Button
									type='primary'
									className='btn-rounded btn-xs'
									style={{ height: '24px', lineHeight: '24px', fontSize: '14px', marginTop: '2px', marginLeft: '15px' }}
								>
									View on Instagram
								</Button>
							</a>
						</div>
						<p style={{ margin: '-10px 0 0' }}>
							{Math.round(instagramProfile.edge_followed_by.count / 1000)}k followers <span className='circle-separator'>•</span>{' '}
							{lookup.countries({ alpha2: instagramOwner.countryCode })[0]
								? lookup.countries({ alpha2: instagramOwner.countryCode })[0].name
								: instagramOwner.countryCode}{' '}
							<span className='circle-separator'>•</span> {instagramOwner.category1 && instagramOwner.category1.name}
							{instagramOwner.category2 && `, ${instagramOwner.category2.name}`}
						</p>
					</div>
				</div>
				<div className='influencer-data'>
					<Row>
						<Col className='engagement-rate' md={{ span: 6 }}>
							<p>
								<strong>{instagramOwner.interactionRate}%</strong> Engagement rate
								<Tooltip title='Average percentage of likes + comments based on amount of followers.'>
									<Icon type='question-circle-o' className='ml-5' />
								</Tooltip>
							</p>
							<h5 className={`recommendation-color-${engagementRate}`}>{renderScoreText(engagementRate)}</h5>
						</Col>
						<Col className='score' md={{ span: 6 }}>
							<DottedLine style={{ float: 'left', margin: '2px 20px 0 0' }} />
							<p>
								<strong>{influencerScoreValue}/10</strong> Score
								<Tooltip title={inflScoreDescription}>
									<Icon type='question-circle-o' className='ml-5' />
								</Tooltip>
							</p>
							<h5 className={`recommendation-color-${influencerScore}`}>{renderScoreText(influencerScore)}</h5>
						</Col>
						<Col className='impression' md={{ span: 6 }}>
							<DottedLine style={{ float: 'left', margin: '2px 20px 0 0' }} />
							<p>
								<strong>{getEstimatedImpressions(instagramOwner, instagramProfile.edge_followed_by.count)} Impressions</strong>
							</p>
							<h5>Estimated</h5>
						</Col>
						<Col className='audience-match' md={{ span: 6 }}>
							<DottedLine style={{ float: 'left', margin: '2px 20px 0 0' }} />
							<p>
								{audienceMatchValue ? (
									<span>
										<strong>{audienceMatchValue}%</strong> Engaged audience match
									</span>
								) : (
									'No information'
								)}
							</p>
							{audienceMatchValue ? <h5 className={`recommendation-color-${audienceMatch}`}>{renderScoreText(audienceMatch)}</h5> : <h5>--</h5>}
						</Col>
					</Row>
				</div>
			</div>
		);
	}
}
