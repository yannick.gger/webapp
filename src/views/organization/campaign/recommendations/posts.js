import React, { Component } from 'react';
import { Row, Col, Icon } from 'antd';

const LikesSvg = () => (
	<svg width='18px' height='16px' viewBox='0 0 18 16'>
		<path
			d='M9.00077085,1.84196607 C5.63887348,-2.10819524 -0.807322639,0.650891845 0.0836414033,6.73509372 C0.697064808,10.9272417 4.91838462,14.432712 9.00077085,16 C13.083349,14.4327501 17.3022308,10.9272417 17.9163645,6.73509372 C18.8072519,0.650910914 12.3617083,-2.10819524 9.00077085,1.84196607 Z'
			fill='#FFF'
		/>
	</svg>
);

const CommentsSvg = () => (
	<svg width='17px' height='16px' viewBox='0 0 17 16'>
		<path
			d='M8.32002163,0 C3.70956484,0 -6.39488462e-14,3.2206775 -6.39488462e-14,7.1842352 C-6.39488462e-14,9.45533082 1.21355836,11.4780894 3.08556322,12.799767 L3.21491293,15.4852492 C3.23896279,15.9859721 3.58931391,16.1492516 3.99556392,15.8508702 L6.23997296,14.2033222 C6.89841947,14.3685196 7.59197648,14.4101386 8.31997837,14.4101386 C12.9304352,14.4101386 16.64,11.1894611 16.64,7.22590337 C16.64,3.26168999 12.9304352,3.27837693e-05 8.31997837,3.27837693e-05 L8.32002163,0 Z'
			fill='#FFF'
		/>
	</svg>
);

const LikesIcon = (props) => <Icon component={LikesSvg} {...props} />;

const CommentsIcon = (props) => <Icon component={CommentsSvg} {...props} />;

export default class Post extends Component {
	render() {
		const { instagramProfile } = this.props;
		const chunk = (arr, size) => arr.reduce((chunks, el, i) => (i % size ? chunks[chunks.length - 1].push(el) : chunks.push([el])) && chunks, []);
		return chunk(instagramProfile.edge_owner_to_timeline_media.edges, 12).map((chunk, index) => (
			<Row gutter={30} key={index} className='larger-breakpoints'>
				{chunk.map(({ node }) => {
					let url = `${node.thumbnail_resources[4].src}`;
					return (
						<Col xs={{ span: 24 }} sm={{ span: 12 }} lg={{ span: 8 }} xl={{ span: 6 }} className='post-item' key={node.id}>
							<div className='post-img-container'>
								<div className='comment-likes-overlay'>
									<LikesIcon className='mr-5' />
									{node.edge_media_preview_like.count}
									<CommentsIcon className='ml-20 mr-5' />
									{node.edge_media_to_comment.count}
								</div>
							</div>
						</Col>
					);
				})}
			</Row>
		));
	}
}
