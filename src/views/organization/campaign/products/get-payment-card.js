import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Query } from 'react-apollo';
import { Spin } from 'antd';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import CreateCard from 'components/create-card/';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import PaymentByInvoiceCard from './payment-by-invoice-card';
import GhostUserAccess from 'components/ghost-user-access';

function sort_obj(obj) {
	var sort_arr = obj.sort(function(a, b) {
		return a.node.id - b.node.id;
	});
	return sort_arr;
}

class PaymentInvoiceOrCard extends Component {
	render() {
		const { campaign, match, history } = this.props;

		return (
			<React.Fragment>
				<Query query={getCampaignCompensationsQuery} variables={{ campaignId: campaign.id }} notifyOnNetworkStatusChange>
					{({ data, loading }) => {
						if (loading) {
							return <Spin className='collabspin' />;
						}
						const campaignData = data && data.campaign;
						const invoices = data && data.campaign && data.campaign.campaignCompensationPaymentsInvoices.edges;
						var sort_invoices = sort_obj(invoices);
						return (
							<section>
								{campaign.invoiceCompensationsCount > 0 &&
									sort_invoices.map(({ node }) => {
										if (node.invoice) {
											return <PaymentByInvoiceCard node={node} campaign={data.campaign} key={node.id} />;
										}
									})}
							</section>
						);
					}}
				</Query>
				<Link
					to={{
						pathname: `/${match.params.organizationSlug}/campaigns/${match.params.campaignId}/products/create-payment`,
						state: { isCommissionFixable: campaign.canBeSentForReview, prevPath: history.location.pathname }
					}}
					data-ripple='rgba(132,146, 164, 0.2)'
					className='empty-cell create-new'
				>
					<CreateCard>{i18next.t('organization:campaign.products.addPayment', { defaultValue: 'Add payment' })}</CreateCard>
				</Link>
			</React.Fragment>
		);
	}
}

export default withRouter(translate('organization')(PaymentInvoiceOrCard));
