import React, { Component } from 'react';
import { Upload, Icon, Modal, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';

const CREATE_PRODUCT_IMAGE = gql`
	mutation createProductImage($image: Upload!, $organizationSlug: String!) {
		createProductImage(input: { image: $image, organizationSlug: $organizationSlug }) {
			productImage {
				id
				thumbnailSquare
			}
		}
	}
`;

const REMOVE_PRODUCT_IMAGE = gql`
	mutation removeProductImage($id: ID!) {
		removeProductImage(input: { id: $id }) {
			productImage {
				id
			}
		}
	}
`;

export default class UploadProductImage extends Component {
	state = {
		previewVisible: false,
		previewImage: '/image-sample.png',
		fileList: [],
		fileListUntouched: true
	};

	handleCancel = () => this.setState({ previewVisible: false });

	handlePreview = (file) => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleRemove = ({ client }) => {
		return () => {
			client
				.mutate({
					mutation: REMOVE_PRODUCT_IMAGE,
					variables: { id: this.state.fileList[0].uid }
				})
				.then(() => {
					this.props.newImageIdHandler(null);

					return true;
				});
		};
	};

	handleCustomRequest = ({ client, callbacks: { onPhotoUploading, onPhotoUploadDone } }) => {
		return ({ file, onError, onSuccess, onProgress }) => {
			onPhotoUploading && onPhotoUploading();
			client
				.mutate({ mutation: CREATE_PRODUCT_IMAGE, variables: { image: file, organizationSlug: this.props.organizationSlug } })
				.then(({ data, loading, error }) => {
					if (error) onError();
					if (data) {
						this.setState({ previewImage: data.createProductImage.productImage.thumbnailSquare });
						this.props.newImageIdHandler(data.createProductImage.productImage.id);

						onSuccess(data, file);
					}

					if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);

					onPhotoUploadDone && onPhotoUploadDone();
				})
				.catch(({ graphQLErrors }) => {
					onPhotoUploadDone && onPhotoUploadDone();
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
					onError();
				});

			return {
				abort() {
					console.log('upload progress is aborted.');
				}
			};
		};
	};

	handleChange = (info) => {
		const status = info.file.status;
		if (!info.file.isExceedSize) {
			this.setState({ fileList: info.fileList });
		}

		if (status === 'done') {
			message.success(`${info.file.name} file uploaded successfully.`);
		} else if (status === 'error') {
			message.error(`${info.file.name} file upload failed.`);
		}
	};

	static getDerivedStateFromProps(props, state) {
		if (props.product && props.product.productImages && props.product.productImages.length > 0 && state.fileListUntouched) {
			return {
				...state,
				fileList: [{ uid: props.product.productImages[0].id, name: 'logo', status: 'done', url: props.product.productImages[0].thumbnailSquare }],
				fileListUntouched: false
			};
		} else {
			return state;
		}
	}

	beforeUpload = (file) => {
		const sizeInMB = file.size / (1024 * 1024);
		if (sizeInMB > 10) {
			file.isExceedSize = true;
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');
			return false;
		}
		return true;
	};

	render() {
		const { previewVisible, fileList } = this.state;
		const { product, onPhotoUploading, onPhotoUploadDone } = this.props;
		const uploadButton = (
			<div>
				<Icon type='plus' />
				<div className='ant-upload-text'>Upload</div>
			</div>
		);

		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<Upload
							listType='picture-card'
							accept='image/*'
							fileList={fileList}
							onRemove={this.handleRemove({ client })}
							onPreview={this.handlePreview}
							onChange={this.handleChange}
							customRequest={this.handleCustomRequest({ client, product, callbacks: { onPhotoUploading, onPhotoUploadDone } })}
							beforeUpload={this.beforeUpload}
						>
							{fileList.length >= 1 ? null : uploadButton}
						</Upload>
						<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
							<img
								alt='example'
								style={{ width: '100%' }}
								src={product && product.productImages && product.productImages.length > 0 ? product.productImages[0].thumbnailSquare : this.state.previewImage}
							/>
						</Modal>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}
