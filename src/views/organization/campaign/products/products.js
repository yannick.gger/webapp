import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Query } from 'react-apollo';
import { Col } from 'antd';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import { checkGhostUserIsSaler } from 'services/auth-service';
import { PaginatedItems } from 'components';
import GhostUserAccess from 'components/ghost-user-access';
import CreateCard from 'components/create-card/';
import ProductCard from './product-card';
import './styles.scss';

class ProductsWithQuery extends Component {
	render() {
		const { campaign, history } = this.props;

		return (
			<React.Fragment>
				<Query query={getCampaignCompensationsQuery} variables={{ campaignId: campaign.id }} notifyOnNetworkStatusChange>
					{(queryRes) => {
						const { data } = queryRes;

						return (
							<React.Fragment>
								<Col xs={{ span: 24 }}>
									<PaginatedItems
										queryRes={queryRes}
										noResults={<span />}
										dataRef='campaign.campaignCompensationProducts' // The location in the graphql query for the list
									>
										{({ data: nodeData }) => {
											return nodeData.edges.map(({ node }) => {
												if (!node.product) {
													return <Col />;
												}

												return <ProductCard node={node} campaign={data.campaign} key={node.id} />;
											});
										}}
									</PaginatedItems>
								</Col>
								<Col xs={{ span: 24 }}>
									<Link
										to={{
											pathname: `/${this.props.match.params.organizationSlug}/campaigns/${this.props.match.params.campaignId}/products/create`,
											state: { isCommissionFixable: campaign.canBeSentForReview, prevPath: history.location.pathname }
										}}
										data-ripple='rgba(132,146, 164, 0.2)'
										className='empty-cell create-new'
									>
										<CreateCard>{i18next.t('organization:campaign.products.createProduct', { defaultValue: 'Create product' })}</CreateCard>
									</Link>
								</Col>
							</React.Fragment>
						);
					}}
				</Query>
			</React.Fragment>
		);
	}
}

export default withRouter(translate('organization')(ProductsWithQuery));
