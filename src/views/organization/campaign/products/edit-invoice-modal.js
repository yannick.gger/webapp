import React from 'react';
import { Modal, Button, message, Spin, Alert } from 'antd';
import { Mutation, Query } from 'react-apollo';
import { OrganizationLink } from 'components';
import PaymentInvoiceForm, { currenciesAccepted, currenciesAcceptedInWords } from './payment-invoice-form';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';
import getCampaignCompensationQuery from 'graphql/get-campaign-compensation.graphql';
import updateCampaignCompensationPaymentInvoice from 'graphql/update-campaign-compensation-payment-invoice.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import getCampaignInstagramOwnersQuery from 'graphql/get-campaign-instagram-owners.graphql';

class EditInvoiceModal extends React.Component {
	state = {
		fields: {},
		adminOverride: false
	};
	static getDerivedStateFromProps = (props, state) => {
		let propsFields = {};

		if (props.campaignCompensation) {
			Object.keys(props.campaignCompensation.invoice)
				.filter((key) => ['value'].includes(key))
				.forEach((key) => {
					propsFields[key] = {
						value: props.campaignCompensation.invoice[key]
					};
				});
			Object.keys(props.campaignCompensation)
				.filter((key) => ['maxSlots'].includes(key))
				.forEach((key) => {
					propsFields[key] = {
						value: props.campaignCompensation[key]
					};
				});
		}

		return { fields: { ...propsFields, ...state.fields } };
	};

	handleSubmit = ({ updateCampaignCompensationPaymentInvoice, campaignCompensationId }) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					id: campaignCompensationId
				};

				try {
					const res = await updateCampaignCompensationPaymentInvoice({ variables: graphQlValues });

					if (res.data.updateCampaignCompensationInvoice.campaignCompensation.invoice) {
						message.success('Payment invoice compensation was updated');
						this.props.closeModal();
					}
				} catch ({ graphQLErrors }) {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
				}
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	handleFormChange = (changedFields) => {
		this.setState(({ fields }) => ({
			fields: { ...fields, ...changedFields }
		}));
	};
	handleAdminOverride = (adminOverride) => {
		this.setState({ adminOverride });
	};
	render() {
		const { fields, adminOverride } = this.state;
		const { match, campaignCompensation } = this.props;

		return (
			<Query query={getCampaignForCompensationFormsQuery} variables={{ id: match.params.campaignId }}>
				{({ data, loading }) => {
					if (loading || !data) return <Spin className='collabspin' />;
					const { campaign } = data;
					const { id, currency, invitesSent } = campaign;
					const checkCurrencyPermission = currenciesAccepted.includes(currency);
					const disabledInput = invitesSent && !adminOverride;
					return (
						<Mutation
							mutation={updateCampaignCompensationPaymentInvoice}
							refetchQueries={[
								{ query: getCampaignCompensationsQuery, variables: { campaignId: id } },
								{ query: getCampaignInstagramOwnersQuery, variables: { campaignId: id } },
								{ query: getCampaignForCompensationFormsQuery, variables: { id: id } }
							]}
						>
							{(updateCampaignCompensationPaymentInvoice, { loading }) => (
								<Modal
									title='Edit Payment by Invoice'
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									wrapClassName='custom-modal payment-modal title-center'
									maskClosable={false}
									footer={[
										<Button size='large' key='back' onClick={this.handleCancel}>
											Cancel
										</Button>,
										checkCurrencyPermission && (
											<Button
												size='large'
												key='submit'
												type='primary'
												loading={loading}
												onClick={this.handleSubmit({
													updateCampaignCompensationPaymentInvoice,
													campaignCompensationId: match.params.campaignCompensationId
												})}
												disabled={disabledInput}
											>
												Save
											</Button>
										)
									]}
								>
									{checkCurrencyPermission ? (
										<PaymentInvoiceForm
											fields={fields}
											campaign={campaign}
											wrappedComponentRef={this.saveFormRef}
											onChange={this.handleFormChange}
											enableSave={this.handleAdminOverride}
											payment={{ ...campaignCompensation.invoice, quantity: campaignCompensation.quantity }}
											organizationSlug={match.params.organizationSlug}
											campaignCompensationId={match.params.campaignCompensationId}
										/>
									) : (
										<Alert
											showIcon
											type='warning'
											title=''
											description={
												<span>
													You need to <OrganizationLink to={`/campaigns/${id}/settings`}>update your campaign</OrganizationLink> to have{' '}
													{currenciesAcceptedInWords} as currency to have invoice payments on your campaign.
												</span>
											}
										/>
									)}
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

class EditInvoiceModalWithPayment extends React.Component {
	render() {
		return (
			<Query query={getCampaignCompensationQuery} variables={{ id: this.props.match.params.campaignCompensationId }}>
				{({ data, loading, error }) => {
					if (loading) {
						return <Spin className='collabspin' />;
					}

					if (error) {
						return <span>Failed to load payment</span>;
					}

					const { campaignCompensation } = data;
					return <EditInvoiceModal campaignCompensation={campaignCompensation} {...this.props} />;
				}}
			</Query>
		);
	}
}

export default EditInvoiceModalWithPayment;
