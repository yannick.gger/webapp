import { Button, message, Spin, Alert } from 'antd';
import createCampaignCompensationInvoice from 'graphql/create-compensation-payment-invoice.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { OrganizationLink } from 'components';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';
import getCampaignInstagramOwnersQuery from 'graphql/get-campaign-instagram-owners.graphql';
import PaymentInvoiceForm, { currenciesAccepted, currenciesAcceptedInWords } from './payment-invoice-form';

class CreatePaymentInvoiceModal extends React.Component {
	state = {
		adminOverride: false
	};

	handleSubmit = (createCampaignCompensationInvoice) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			const { location, handleCloseModal } = this.props;
			const campaignId = location.pathname.split('/')[3];
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					campaignId
				};

				createCampaignCompensationInvoice({
					variables: graphQlValues,
					refetchQueries: [
						{ query: getCampaignCompensationsQuery, variables: { campaignId: campaignId } },
						{ query: getCampaignInstagramOwnersQuery, variables: { campaignId: campaignId } },
						{ query: getCampaignForCompensationFormsQuery, variables: { id: campaignId } }
					]
				})
					.then(({ data, error }) => {
						if (data.createCampaignCompensationInvoice.campaignCompensation.invoice) {
							message.success('Payment invoice compensation was created');
							handleCloseModal();
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	handleAdminOverride = (adminOverride) => {
		this.setState({ adminOverride });
	};
	render() {
		const { location, handleCloseModal } = this.props;
		const id = location.pathname.split('/')[3];
		const organizationSlug = location.pathname.split('/')[0];
		return (
			<Query query={getCampaignForCompensationFormsQuery} variables={{ id }}>
				{({ data, loading }) => {
					if (loading || !data) return <Spin className='collabspin' />;
					const campaign = data.campaign;
					const { id, currency, paymentCompensationsCount } = campaign;
					const checkCurrencyPermission = currenciesAccepted.includes(currency);
					let paymentInvoiceFormNode = null;
					if (paymentCompensationsCount > 0) {
						paymentInvoiceFormNode = (
							<Alert
								showIcon
								type='warning'
								title=''
								description='To be able to use "Payment by invoice" you first have to remove "Payment by card" that currently is being used.'
							/>
						);
					} else if (checkCurrencyPermission) {
						paymentInvoiceFormNode = (
							<PaymentInvoiceForm
								campaign={campaign}
								wrappedComponentRef={this.saveFormRef}
								organizationSlug={organizationSlug}
								enableSave={this.handleAdminOverride}
							/>
						);
					} else {
						paymentInvoiceFormNode = (
							<span>
								You need to <OrganizationLink to={`/campaigns/${id}/settings`}>update your campaign</OrganizationLink> to have {currenciesAcceptedInWords} as
								currency to have invoice payments on your campaign.
							</span>
						);
					}
					return (
						<Mutation mutation={createCampaignCompensationInvoice}>
							{(createCampaignCompensationInvoice, { loading }) => (
								<div>
									{paymentInvoiceFormNode}
									<div className='payment-btn-wrapper'>
										<Button size='large' key='back' onClick={handleCloseModal}>
											Cancel
										</Button>
										{checkCurrencyPermission && paymentCompensationsCount < 1 && (
											<Button
												className='ml-20'
												size='large'
												key='submit'
												type='primary'
												loading={loading}
												onClick={this.handleSubmit(createCampaignCompensationInvoice)}
											>
												Create
											</Button>
										)}
									</div>
								</div>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

export default CreatePaymentInvoiceModal;
