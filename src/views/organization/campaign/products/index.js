import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { Col, Row } from 'antd';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import CampaignLayout from '../layout/';
import Products from './products';
import PaymentInvoiceOrCard from './get-payment-card';
import ListsView from './lists.js';
import { isAdminOrGhostAdmin, ghostUserIsSaler, isSaler } from 'services/auth-service';

class CampaignCompensations extends Component {
	redirectToHome() {
		return <Redirect to='/' />;
	}

	renderCampaignCompensations() {
		const { match } = this.props;

		return (
			<CampaignLayout>
				{({ campaign }) => (
					<div>
						<Row gutter={30}>
							<Col xs={{ span: 24 }} lg={12}>
								<h3>{i18next.t('organization:campaign.products.title.products', { defaultValue: 'Products' })}</h3>
								<Products campaign={campaign} />
							</Col>
							<Col xs={{ span: 24 }} lg={12}>
								<h3>{i18next.t('organization:campaign.products.title.invoiceOrCard', { defaultValue: 'Pay by Invoice or Card' })}</h3>
								<PaymentInvoiceOrCard campaign={campaign} match={match} />
							</Col>
						</Row>
						<ListsView campaign={campaign} />
					</div>
				)}
			</CampaignLayout>
		);
	}

	render() {
		const { history } = this.props;
		if (isAdminOrGhostAdmin() || ghostUserIsSaler() || isSaler()) {
			return this.renderCampaignCompensations();
		} else {
			if (history.location.state && history.location.state.isCommissionFixable) {
				return this.renderCampaignCompensations();
			} else {
				return this.redirectToHome();
			}
		}
		return this.redirectToHome();
	}
}

export default withRouter(translate('organization')(CampaignCompensations));
