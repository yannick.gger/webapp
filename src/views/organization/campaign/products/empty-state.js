import React from 'react';
import { Row, Col, Button } from 'antd';
import { withRouter, Link } from 'react-router-dom';

import NoProd from './no-products.svg';

const EmptyState = ({ match }) => (
	<Row>
		<Col sm={{ span: 8, offset: 8 }} className='text-center'>
			<div className='empty-icon'>
				<img src={NoProd} alt='' className='svg' />
			</div>
			<div className='mb-30'>
				<h2>Create products</h2>
				<p>Create your first compensation for the influencer</p>
			</div>
			<div className='d-flex justify-content-center'>
				<Link to={`${match.url}/create`}>
					<Button type='primary' className='btn-uppercase' size='large'>
						Create product
					</Button>
				</Link>
			</div>
		</Col>
	</Row>
);

export default withRouter(EmptyState);
