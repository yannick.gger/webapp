import React, { Component } from 'react';
import { Form, InputNumber, Button, message } from 'antd';
import { Mutation } from 'react-apollo';
import editCommissionValueByList from 'graphql/edit-commission-value-by-list.graphql';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import getCampaignInstagramOwnersQuery from 'graphql/get-campaign-instagram-owners.graphql';

export const currenciesMinValue = {
	EUR: 100,
	USD: 100,
	GBP: 100,
	SEK: 1000,
	NOK: 1000,
	DKK: 1000,
	CZK: 2500
};

const FormItem = Form.Item;

class CreatePaymentInvoiceFormV2 extends Component {
	formRef = React.createRef();

	handleSubmit = (editCommissionValueByList) => {
		return (e) => {
			e.preventDefault();
			const { campaign, selectedRowKeys, form } = this.props;
			const tabKey = Number(this.props.tabKey);
			const campaignId = campaign.id;
			const influencerIds = selectedRowKeys;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					campaignId,
					influencerIds,
					tabKey
				};

				editCommissionValueByList({
					variables: graphQlValues,
					refetchQueries: [
						{ query: getCampaignCompensationsQuery, variables: { campaignId: campaignId } },
						{ query: getCampaignInstagramOwnersQuery, variables: { campaignId: campaignId } },
						{ query: getCampaignForCompensationFormsQuery, variables: { id: campaignId } }
					]
				})
					.then(({ data, error }) => {
						if (data.editCommissionValueByList.campaignCompensation.invoice) {
							message.success('Edit commission successfully!');
							this.props.handleCancel();
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};

	render() {
		const { campaign, adminOverride, handleCancel } = this.props;
		const inputDisabled = campaign && campaign.invitesSent && !adminOverride;
		const { getFieldDecorator } = this.props.form;
		return (
			<Mutation mutation={editCommissionValueByList}>
				{(editCommissionValueByList, { loading }) => (
					<div>
						<Form id='edit-invoice-form' layout='vertical' name='edit-invoice-value-form' ref={this.formRef}>
							<FormItem label={`Payment invoice value (${campaign && campaign.currency})`} className='has-extra-label'>
								{getFieldDecorator('value', {
									rules: [
										{ required: true, message: 'Enter a payment value' },
										{
											validator: (rule, value, callback) => {
												callback(currenciesMinValue.hasOwnProperty(campaign.currency) && value >= currenciesMinValue[campaign.currency] ? [] : [new Error()]);
											},
											message: `Smallest possible payment is ${currenciesMinValue[campaign.currency]} ${campaign.currency}`
										}
									]
								})(<InputNumber size='large' style={{ width: '100%' }} disabled={inputDisabled} />)}
								<span className='input-extra-label'>This is what each influencer will invoice</span>
							</FormItem>
							<FormItem label='Max slots' className='has-extra-label'>
								{getFieldDecorator('maxSlots', {
									rules: [
										{ required: true, message: 'Enter a value' },
										{
											validator: (rule, maxSlots, callback) => {
												callback(maxSlots >= 1 ? [] : [new Error()]);
											},
											message: 'Smallest possible slot is 1'
										}
									]
								})(<InputNumber size='large' style={{ width: '100%' }} disabled={inputDisabled} />)}
								<span className='input-extra-label'>Max influencer slots for invoice</span>
							</FormItem>
						</Form>
						<div className='payment-btn-wrapper'>
							<Button size='large' key='back' onClick={handleCancel}>
								Cancel
							</Button>
							<Button className='ml-20' size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(editCommissionValueByList)}>
								Create
							</Button>
						</div>
					</div>
				)}
			</Mutation>
		);
	}
}

export default Form.create()(CreatePaymentInvoiceFormV2);
