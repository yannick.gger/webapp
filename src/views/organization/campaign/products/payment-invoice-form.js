import { Alert, Form, InputNumber, Row, Checkbox, Select, Spin } from 'antd';
import { ghostUserIsSaler, isLoginAdminUser, isSaler } from 'services/auth-service.js';
import React, { Component } from 'react';
import PersonTag from './person-tag';
import './styles.scss';

export const currenciesMinValue = {
	EUR: 100,
	USD: 100,
	GBP: 100,
	SEK: 1000,
	NOK: 1000,
	DKK: 1000,
	CZK: 2500
};

export const currenciesAccepted = Object.keys(currenciesMinValue);
export const currenciesAcceptedInWords = currenciesAccepted.slice(0, -1).join(', ') + ', or ' + currenciesAccepted.slice(-1);

const FormItem = Form.Item;
const { Option } = Select;

const PeopleSelect = ({ options, ...props }) => {
	if (options) {
		const { handleOnSelect, handleOnDeSelect, disabledFlag, currency } = props;
		return (
			<Select
				mode='multiple'
				className='peopleBar'
				tagRender={({ label }) => label}
				{...props}
				size='large'
				onSelect={(value, event) => handleOnSelect(value, event)}
				onDeselect={(value, event) => handleOnDeSelect(value, event)}
			>
				<Option key='all' value='all' selected>
					<span className='personTag'>Default (All influencers)</span>
				</Option>
				{Object.entries(options).map(([id, node]) => (
					<Option key={node.node.id} value={node.node.id} disabled={disabledFlag}>
						<PersonTag node={node} currency={currency} />
					</Option>
				))}
			</Select>
		);
	} else {
		return <Spin className='collabspin' />;
	}
};

class PaymentInvoiceForm extends Component {
	state = {
		adminOverride: false,
		disabled: false
	};

	handleAdminOverride = (e) => {
		const override = e.target.checked;
		this.props.enableSave(override);
		this.setState({ adminOverride: override });
	};

	handleOnSelect = (selectedValue, e) => {
		if (selectedValue === 'all') {
			this.setState({ disabled: true });
		}
	};

	handleOnDeselect = (selectedValue, e) => {
		this.setState({ disabled: false });
	};

	filter_influencers = (influencers, campaignCompensationId, assignedFlag) => {
		return influencers.filter((influencer) => {
			for (let value of Object.values(influencer.node.campaignCompensations.nodes)) {
				if (value.id === campaignCompensationId) return assignedFlag;
			}
			return !assignedFlag;
		});
	};

	render() {
		const { form, campaign } = this.props;
		const { adminOverride, disabled } = this.state;
		const { getFieldDecorator } = form;
		const { campaignCompensationId } = this.props;

		const inputDisabled = campaign && campaign.invitesSent && !adminOverride;
		const influencers = campaign && campaign.instagramOwners && campaign.instagramOwners.edges;
		const available_influencers = this.filter_influencers(influencers, campaignCompensationId, false);
		const assigned_influencers = this.filter_influencers(influencers, campaignCompensationId, true);

		return (
			<React.Fragment>
				{campaign && campaign.invitesSent && (
					<React.Fragment>
						{isLoginAdminUser() || isSaler() || ghostUserIsSaler() ? (
							<Alert
								message='Payment can not be edited'
								description={
									<React.Fragment>
										<p>
											Payments can't be edited after invites are sent. As admin, you can override this. Make sure that all involved parties have agreed to any
											new terms.
										</p>
										<Checkbox className='mt-10' value={true} checked={adminOverride} onChange={this.handleAdminOverride} style={{ color: 'red' }}>
											{adminOverride ? 'Using admin privilege to override' : 'Use admin privilege to override'}
										</Checkbox>
									</React.Fragment>
								}
								type={adminOverride ? 'error' : 'warning'}
							/>
						) : (
							<Alert message='Payment can not be edited' description="Payments can't be edited after invites are sent." type='warning' showIcon />
						)}
					</React.Fragment>
				)}
				<Form layout='vertical' className='payment-form'>
					<Row gutter={30}>
						<FormItem label={`Payment invoice value (${campaign && campaign.currency})`} className='has-extra-label'>
							{getFieldDecorator('value', {
								rules: [
									{ required: true, message: 'Enter a payment value' },
									{
										validator: (rule, value, callback) => {
											callback(currenciesMinValue.hasOwnProperty(campaign.currency) && value >= currenciesMinValue[campaign.currency] ? [] : [new Error()]);
										},
										message: `Smallest possible payment is ${currenciesMinValue[campaign.currency]} ${campaign.currency}`
									}
								]
							})(<InputNumber size='large' style={{ width: '100%' }} disabled={inputDisabled} />)}
							<span className='input-extra-label'>This is what each influencer will invoice</span>
						</FormItem>
						<FormItem label='Max slots' className='has-extra-label'>
							{getFieldDecorator('maxSlots', {
								rules: [
									{ required: true, message: 'Enter a value' },
									{
										validator: (rule, maxSlots, callback) => {
											callback(maxSlots >= 1 ? [] : [new Error()]);
										},
										message: 'Smallest possible slot is 1'
									}
								]
							})(<InputNumber size='large' style={{ width: '100%' }} disabled={inputDisabled} />)}
							<span className='input-extra-label'>Max influencer slots for invoice</span>
						</FormItem>
						<FormItem label='Add influencer to invoice' className='has-extra-label'>
							{getFieldDecorator('influencerIds', {})(
								<PeopleSelect
									options={available_influencers}
									handleOnSelect={this.handleOnSelect}
									disabledFlag={disabled}
									handleOnDeSelect={this.handleOnDeselect}
									disabled={inputDisabled}
									currency={campaign.currency}
								/>
							)}
						</FormItem>
					</Row>
				</Form>
				<div className='ant-col ant-form-item-label' style={{ width: '100%', textAlign: 'left' }}>
					<label className='ant-form-item-required' title='Involved influencers'>
						Assigned influencers
					</label>
					{Object.entries(assigned_influencers).map(([id, node]) => (
						<Row>
							<PersonTag node={node} />
						</Row>
					))}
				</div>
				<Alert className='mt-20' message='All prices added is EXCL. of VAT (25%).' type='info' showIcon />
			</React.Fragment>
		);
	}
}

const WrappedPaymentInvoiceForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const propFields = props.fields || {};
		let formFields = {};
		Object.keys(propFields).forEach((propFieldKey) => {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		});

		return formFields;
	}
})(PaymentInvoiceForm);

export default WrappedPaymentInvoiceForm;
