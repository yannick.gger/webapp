import React, { Component } from 'react';
import { Query, withApollo } from 'react-apollo';
import { Spin, Button, message } from 'antd';
import PaymentByInvoiceCard from './payment-by-invoice-card';
import editCommissionValueByList from 'graphql/edit-commission-value-by-list.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import getCampaignInstagramOwnersQuery from 'graphql/get-campaign-instagram-owners.graphql';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';

class AddToCommissionForm extends Component {
	handleSubmit = (id, editCommissionValueByList, inputDisabled) => {
		if (inputDisabled) return;
		return (e) => {
			e.preventDefault();
			const { selectedRowKeys, campaign } = this.props;
			const tabKey = Number(this.props.tabKey);
			const commissionId = Number(id);
			const campaignId = campaign.id;
			const influencerIds = selectedRowKeys;
			const graphQlValues = {
				commissionId,
				campaignId,
				influencerIds,
				tabKey
			};
			this.props.client
				.mutate({
					mutation: editCommissionValueByList,
					variables: graphQlValues,
					refetchQueries: [
						{ query: getCampaignCompensationsQuery, variables: { campaignId: campaignId } },
						{ query: getCampaignInstagramOwnersQuery, variables: { campaignId: campaignId } },
						{ query: getCampaignForCompensationFormsQuery, variables: { id: campaignId } }
					]
				})
				.then(({ data, error }) => {
					if (data.editCommissionValueByList.campaignCompensation.invoice) {
						message.success('Edit commission successfully!');
						this.props.handleCancel();
					}
				})
				.catch((error) => {
					message.error(error.message);
				});
		};
	};
	render() {
		const { adminOverride, handleCancel, campaign } = this.props;
		const inputDisabled = campaign && campaign.invitesSent && !adminOverride;
		return (
			<Query query={getCampaignCompensationsQuery} variables={{ campaignId: campaign.id }} notifyOnNetworkStatusChange>
				{({ data, loading }) => {
					if (loading) {
						return <Spin className='collabspin' />;
					}
					return (
						<section>
							{campaign.invoiceCompensationsCount > 0 &&
								data.campaign.campaignCompensationPaymentsInvoices.edges.map(({ node }) => {
									if (node.invoice) {
										return (
											<div onClick={this.handleSubmit(node.id, editCommissionValueByList, inputDisabled)}>
												<PaymentByInvoiceCard node={node} campaign={data.campaign} key={node.id} inputDisabled={inputDisabled} />
											</div>
										);
									}
								})}
							<div className='payment-btn-wrapper'>
								<Button size='large' key='back' onClick={handleCancel}>
									Cancel
								</Button>
							</div>
						</section>
					);
				}}
			</Query>
		);
	}
}

export default withApollo(AddToCommissionForm);
