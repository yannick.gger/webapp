import React, { Component } from 'react';
import { Card, Alert, Avatar, Tooltip } from 'antd';

import ProductDropdown from './product-dropdown';
import './styles.scss';
import { checkGhostUserIsSaler } from 'services/auth-service';

class PaymentByInvoiceCard extends Component {
	render() {
		const { node, campaign, inputDisabled } = this.props;
		const { value } = node.invoice;
		const { currency, influencerTargetCount } = campaign;

		const maxSlot = node.maxSlots || influencerTargetCount;

		let text;

		if (node.campaignInstagramOwners && node.campaignInstagramOwners.length > 0) {
			text = (
				<h4 className='mb-10'>
					{node.name} - {node.campaignInstagramOwners.length} {node.campaignInstagramOwners.length > 1 ? 'influencers' : 'influencer'} will invoice
				</h4>
			);
		} else {
			text = <h4 className='mb-10'>Each influencer will invoice</h4>;
		}
		return (
			<Card
				hoverable={inputDisabled ? false : true}
				className={inputDisabled ? 'payment-card card-action-right disabled-card' : 'payment-card card-action-right'}
				actions={[<ProductDropdown key='assignment-dropdown' assignment={node} campaignCompensation={node} campaign={campaign} />]}
			>
				{text}
				<div className='payment-box'>
					<h2 className='mb-0'>
						{value} {currency}
					</h2>
					<span className='total'>
						Total {value * maxSlot} {currency} - {maxSlot} {maxSlot === 1 ? 'slot' : 'slots'}
					</span>
				</div>
				<Alert className='mt-20' message='All prices added is EXCL. of VAT (25%).' type='info' showIcon />
				<div className='avatar-list'>
					{node.campaignInstagramOwners.slice(0, node.campaignInstagramOwners.length > 11 ? 12 : 11).map((node) => {
						return (
							<Tooltip placement='topLeft' key={node.instagramOwnerId} title={`@${node.name}`}>
								<Avatar size='medium' src={node} icon='user' />
							</Tooltip>
						);
					})}

					{node.campaignInstagramOwners.length > 12 && (
						<Avatar size='large' key='more'>
							+{node.campaignInstagramOwners.length - 12}
						</Avatar>
					)}
				</div>
			</Card>
		);
	}
}

export default PaymentByInvoiceCard;
