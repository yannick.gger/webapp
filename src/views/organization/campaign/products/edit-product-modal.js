import React from 'react';
import { Modal, Button, message, Spin } from 'antd';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';

import ProductForm from './product-form';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';
import getCampaignCompensationQuery from 'graphql/get-campaign-compensation.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import { hasGhostToken } from 'services/auth-service';

const UPDATE_PRODUCT = gql`
	mutation updateCampaignCompensationProduct(
		$id: ID!
		$name: String!
		$description: String
		$value: Float
		$link: String
		$cost: Float
		$vat: Int
		$productImageId: ID
		$quantity: Int
		$ghostToken: Boolean
		$influencerIds: [ID!]
	) {
		updateCampaignCompensationProduct(
			input: {
				id: $id
				name: $name
				description: $description
				link: $link
				value: $value
				cost: $cost
				vat: $vat
				productImageId: $productImageId
				quantity: $quantity
				ghostToken: $ghostToken
				influencerIds: $influencerIds
			}
		) {
			campaignCompensation {
				id
			}
		}
	}
`;

class EditProductModal extends React.Component {
	state = {
		fields: {},
		photoUploadStatus: false,
		adminOverride: false
	};

	static getDerivedStateFromProps = (props, state) => {
		let propsFields = {};

		if (props.campaignCompensation) {
			Object.keys(props.campaignCompensation)
				.filter((key) => ['quantity'].includes(key))
				.forEach((key) => {
					propsFields[key] = {
						value: props.campaignCompensation[key]
					};
				});

			Object.keys(props.campaignCompensation.product)
				.filter((key) => ['name', 'description', 'value', 'cost', 'vat', 'link'].includes(key))
				.forEach((key) => {
					propsFields[key] = {
						value: props.campaignCompensation.product[key]
					};
				});
		}

		if (props.campaignCompensation && props.campaignCompensation.product.productImages[0]) {
			propsFields['productImageId'] = {
				value: props.campaignCompensation.product.productImages[0].id
			};
		}

		return { fields: { ...propsFields, ...state.fields } };
	};

	componentDidMount() {
		if (!this.props.campaignCompensation) {
			message.error('Product does not exist');
			this.props.closeModal();
		}
	}

	handlePhotoUploading = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: true }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handlePhotoUploadDone = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: false }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handleSubmit = ({ updateCampaignCompensationProduct, campaignCompensationId }) => {
		return (e) => {
			e.preventDefault();

			if (this.state.photoUploadStatus) return message.info('Photo upload is still in progress. Please wait a moment.');

			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					id: campaignCompensationId,
					ghostToken: hasGhostToken()
				};

				const res = await updateCampaignCompensationProduct({ variables: graphQlValues });
				if (res.data.updateCampaignCompensationProduct) {
					message.info('Product was updated');
					this.props.closeModal();
					this.forceUpdate();
				} else {
					message.error('Something went wrong');
				}
			});
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	handleFormChange = (changedFields) => {
		this.setState(({ fields }) => ({
			fields: { ...fields, ...changedFields }
		}));
	};

	handleAdminOverride = (adminOverride) => {
		this.setState({ adminOverride });
	};

	render() {
		const { fields, adminOverride } = this.state;
		const { campaignCompensation } = this.props;

		return (
			<Query query={getCampaignForCompensationFormsQuery} variables={{ id: this.props.match.params.campaignId }}>
				{({ data, loading }) => {
					if (loading || !(data || {}).campaign) return <Spin className='collabspin' />;

					const disabledInput = data.campaign.invitesSent && !adminOverride;
					return (
						<Mutation
							mutation={UPDATE_PRODUCT}
							refetchQueries={[
								{ query: getCampaignCompensationsQuery, variables: { campaignId: data.campaign.id } },
								{ query: getCampaignForCompensationFormsQuery, variables: { id: data.campaign.id } }
							]}
						>
							{(updateCampaignCompensationProduct, { loading }) => (
								<Modal
									title='Edit product'
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									wrapClassName='custom-modal product-modal title-center'
									maskClosable={false}
									footer={[
										<Button size='large' key='back' onClick={this.handleCancel}>
											Return
										</Button>,
										<Button
											size='large'
											key='submit'
											type='primary'
											loading={loading}
											onClick={this.handleSubmit({
												updateCampaignCompensationProduct,
												campaignCompensationId: this.props.match.params.campaignCompensationId
											})}
											disabled={disabledInput}
										>
											Save
										</Button>
									]}
								>
									<ProductForm
										fields={fields}
										campaign={data.campaign}
										wrappedComponentRef={this.saveFormRef}
										onChange={this.handleFormChange}
										product={{ ...campaignCompensation.product, quantity: campaignCompensation.quantity }}
										organizationSlug={this.props.match.params.organizationSlug}
										onPhotoUploading={this.handlePhotoUploading}
										onPhotoUploadDone={this.handlePhotoUploadDone}
										enableSave={this.handleAdminOverride}
										campaignCompensationId={this.props.match.params.campaignCompensationId}
									/>
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

class EditProductModalWithProduct extends React.Component {
	render() {
		return (
			<Query query={getCampaignCompensationQuery} variables={{ id: this.props.match.params.campaignCompensationId }}>
				{({ data, loading, error }) => {
					if (loading || !data) {
						return <Spin className='collabspin' />;
					}

					if (error) {
						return '<span>Failed to load product</span>';
					}

					const { campaignCompensation } = data;
					return <EditProductModal campaignCompensation={campaignCompensation} {...this.props} />;
				}}
			</Query>
		);
	}
}

export default EditProductModalWithProduct;
