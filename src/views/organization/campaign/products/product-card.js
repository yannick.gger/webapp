import React, { Component } from 'react';
import { Col, Card, Row } from 'antd';
import { FormattedNumber } from 'react-intl';

import ProductDropdown from './product-dropdown';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';
import './styles.scss';

class ProductCard extends Component {
	render() {
		const { node, campaign } = this.props;
		let text;

		if (node.campaignInstagramOwners && node.campaignInstagramOwners.length > 0) {
			text = (
				<h4 className='mb-10' style={{ flex: '0 0 100%' }}>
					{node.campaignInstagramOwners.length} {node.campaignInstagramOwners.length > 1 ? 'influencers' : 'influencer'} will receive
				</h4>
			);
		} else {
			text = '';
		}
		return (
			<Card
				hoverable
				className='product-card card-action-right'
				actions={[<ProductDropdown key='assignment-dropdown' assignment={node} campaignCompensation={node} campaign={campaign} />]}
			>
				{text}
				<div className='card-content'>
					<div
						className='product-image fl'
						style={node.product.productImages[0] ? { backgroundImage: `url(${node.product.productImages[0].thumbnailBig})` } : { backgroundColor: '#aaa' }}
					/>
					<Card.Meta title={node.product.name} description={node.product.description} className='multi-line' />
					<Row className='product-pricing' style={{ display: 'flex' }}>
						<Col>
							<h4 style={{ marginBottom: '0' }}>Cost</h4>
							<h4 className='color-blue'>
								<FormattedNumber value={node.product.cost} style='currency' currency={campaign.currency} minimumFractionDigits={0} />
							</h4>
						</Col>
						<Col>
							<h4 style={{ marginBottom: '0' }}>Value</h4>
							<h4 className='color-blue'>
								<FormattedNumber value={node.product.value} style='currency' currency={campaign.currency} minimumFractionDigits={0} />
							</h4>
						</Col>
						<Col>
							<h4 style={{ marginBottom: '0' }}>VAT</h4>
							<h4 className='color-blue'>{node.product.vat}%</h4>
						</Col>
						<Col>
							<h4 style={{ marginBottom: '0' }}>Quantity</h4>
							<h4 className='color-blue'>{node.quantity} st</h4>
						</Col>
					</Row>
				</div>
			</Card>
		);
	}
}

export default ProductCard;
