import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { Spin } from 'antd';
import PaymentInvoiceModal from './create-payment-invoice-modal';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import './styles.scss';

class PaymentsByInvoiceWithQuery extends Component {
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		const { campaign, handleCloseModal } = this.props;

		return (
			<React.Fragment>
				<Query query={getCampaignCompensationsQuery} variables={{ campaignId: campaign.id }} notifyOnNetworkStatusChange>
					{({ data, loading }) => {
						if (loading) {
							return <Spin className='collabspin' />;
						}
						return (
							<React.Fragment>
								<PaymentInvoiceModal campaign={campaign} location={location} handleCloseModal={handleCloseModal} />
							</React.Fragment>
						);
					}}
				</Query>
			</React.Fragment>
		);
	}
}

export default withRouter(PaymentsByInvoiceWithQuery);
