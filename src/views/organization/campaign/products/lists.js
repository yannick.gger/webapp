import { Avatar, Table, Empty } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';
import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { Spin, Icon } from 'antd';
import camelcase from 'camelcase';
import TableErrorImg from 'assets/img/app/table-error.svg';
import getCampaignInstagramOwnersQuery from 'graphql/get-campaign-instagram-owners.graphql';
import EditInvoiceModalV2 from './edit-invoice-modal-v2.js';
import InstagramProfileDrawer from 'components/instagram-profile-drawer';

const colors = {
	blue: '#157ff1',
	green: '#45cc6f',
	gray: '#98a0a9',
	orange: '#f79552',
	red: '#d74664'
};

const inviteStatusTextColors = {
	clicked: {
		text: 'Viewed campaign',
		color: 'blue',
		iconType: 'eye',
		iconColor: colors.blue
	},
	opened: {
		text: 'Opened invite',
		color: 'blue',
		iconType: 'mail',
		iconColor: colors.blue
	},
	notOpened: {
		text: "Didn't open",
		color: 'gray',
		iconType: 'exclamation-circle',
		iconColor: colors.gray
	},
	joined: {
		text: 'Joined',
		color: 'green',
		iconType: 'check-circle',
		iconColor: colors.green
	},
	declined: {
		text: 'Declined',
		color: 'red',
		iconType: 'close-circle',
		iconColor: colors.red
	},
	notInvited: {
		text: 'Not Invited',
		color: 'gray',
		iconType: 'info-circle',
		iconColor: colors.gray
	}
};

const getIcon = (inviteStatus) => {
	const { color: textColor, text, iconType, iconColor } = inviteStatusTextColors[camelcase(inviteStatus)];

	if (inviteStatus !== 'notOpened') {
		return (
			<span className={`color-${textColor}`}>
				<Icon type={iconType} style={{ color: iconColor }} />
				&nbsp;{text}
			</span>
		);
	} else {
		return (
			<span className={`color-${textColor}`}>
				<Icon type={iconType} theme='twoTone' twoToneColor={iconColor} />
				&nbsp;{text}
			</span>
		);
	}
};

class ListsView extends Component {
	state = { selectedRowKeys: [] };

	onSelectChange = (selectedRowKeys) => {
		this.setState({ selectedRowKeys });
	};

	getInfluencers = (cios) => {
		return cios.edges.filter((cio) => {
			let cio_with_payment = cio.node.campaignCompensations.edges.some(({ node }) => node.payment || node.invoice);
			return cio_with_payment;
		});
	};

	render() {
		const { selectedRowKeys } = this.state;
		const { campaign } = this.props;
		const rowSelection = {
			selectedRowKeys,
			onChange: this.onSelectChange
		};
		// const hasSelected = selectedRowKeys.length > 0

		return (
			<React.Fragment>
				<Query query={getCampaignInstagramOwnersQuery} variables={{ campaignId: campaign.id }} notifyOnNetworkStatusChange>
					{({ data, loading, error }) => {
						if (loading) {
							return <Spin className='collabspin' />;
						}
						const cios = data && data.campaignInstagramOwners;
						const influencers = this.getInfluencers(cios);
						if (influencers === undefined || influencers.length === 0) {
							return <div />;
						}

						const columns = [
							{
								title: '',
								dataIndex: 'node.instagramOwner.instagramOwnerUser.avatar',
								render: (record) => {
									return <Avatar size='large' src={record} />;
								}
							},
							{
								title: 'NAME',
								dataIndex: 'node.instagramOwner.instagramOwnerUser.username',
								render: (text, record) => (
									<div className='list-name' href='#' target='_blank' rel='noopener noreferrer'>
										{record.name}
									</div>
								)
							},
							{
								title: 'FOLLOWERS',
								dataIndex: 'node.instagramOwner.followedByCount',
								render: (record) => <ShortAmountFormatter value={record} />
							},
							{
								title: 'ENGAGEMENT',
								dataIndex: 'node.instagramOwner.interactionRate',
								render: (record) => <span>{record}%</span>
							},
							{
								title: 'COMMISSION',
								dataIndex: 'node.campaignCompensations.edges',
								render: (record) => {
									const compensation = record.filter((compensation) => compensation.node.invoice || compensation.node.payment);
									const name = compensation[0].node.name.toUpperCase();
									return <span>{name}</span>;
								}
							},
							{
								title: 'AMOUNT',
								dataIndex: 'node.campaignCompensations',
								render: (record) => {
									const compensation = record.edges
										.filter((compensation) => compensation.node.invoice || compensation.node.payment)
										.map((compensation) => compensation.node.invoice || compensation.node.payment);
									const amount = compensation[0].value;
									return (
										<span>
											{amount} {campaign.currency}
										</span>
									);
								}
							},
							{
								title: 'EMAIL INVITE STATUS',
								key: 'node.emailInviteStatus',
								dataIndex: 'node.emailInviteStatus',
								render: (record) => getIcon(record)
							},
							{
								title: 'ACTION',
								render: (record) => {
									return <EditInvoiceModalV2 selectedRowKeys={selectedRowKeys} campaign={campaign} influencers={influencers} />;
								}
							}
						];

						return (
							<Table
								className='pb-30 mt-50'
								rowSelection={rowSelection}
								columns={columns}
								dataSource={influencers ? influencers : [1, 2]}
								rowKey={(record) => record.node.id}
								pagination={false}
								loading={loading}
								scroll={{ x: 1000 }}
								sortDirections={['descend', 'ascend']}
								locale={{
									emptyText: error ? (
										<Empty image={TableErrorImg} description='Could not load users' />
									) : (
										<Empty description='Nothing matching the search criterias found' />
									)
								}}
							/>
						);
					}}
				</Query>
			</React.Fragment>
		);
	}
}

export default ListsView;
