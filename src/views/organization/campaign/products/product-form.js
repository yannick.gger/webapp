import React, { Component } from 'react';
import { Row, Col, Form, Input, Radio, Divider, InputNumber, Alert, Checkbox, Select, Spin } from 'antd';

import UploadImage from './upload-image.js';
import { ghostUserIsSaler, isLoginAdminUser, isSaler } from 'services/auth-service';
import PersonTag from './person-tag';
import './styles.scss';

const FormItem = Form.Item;
const { Option } = Select;

const PeopleSelect = ({ options, ...props }) => {
	if (options) {
		const { handleOnSelect, handleOnDeSelect, disabledFlag, currency } = props;
		return (
			<Select
				mode='multiple'
				className='peopleBar'
				tagRender={({ label }) => label}
				{...props}
				size='large'
				onSelect={(value, event) => handleOnSelect(value, event)}
				onDeselect={(value, event) => handleOnDeSelect(value, event)}
			>
				<Option key='all' value='all' selected>
					<span className='personTag'>Default (All influencers)</span>
				</Option>
				{Object.entries(options).map(([id, node]) => (
					<Option key={node.node.id} value={node.node.id} disabled={disabledFlag}>
						<PersonTag node={node} currency={currency} />
					</Option>
				))}
			</Select>
		);
	} else {
		return <Spin className='collabspin' />;
	}
};

class ProductForm extends Component {
	state = {
		adminOverride: false,
		disabled: false
	};

	handleNewImageId = (id) => {
		this.props.form.setFields({
			productImageId: {
				value: id
			}
		});
	};

	handlePhotoUploading = () => {
		this.props.onPhotoUploading();
	};

	handlePhotoUploadDone = () => {
		this.props.onPhotoUploadDone();
	};

	handleAdminOverride = (e) => {
		const override = e.target.checked;
		this.props.enableSave(override);
		this.setState({ adminOverride: override });
	};

	handleOnSelect = (selectedValue, e) => {
		if (selectedValue === 'all') {
			this.setState({ disabled: true });
		}
	};

	handleOnDeselect = (selectedValue, e) => {
		this.setState({ disabled: false });
	};

	filter_influencers = (influencers, campaignCompensationId, assignedFlag) => {
		return influencers.filter((influencer) => {
			for (let value of Object.values(influencer.node.campaignCompensations.nodes)) {
				if (value.id === campaignCompensationId) return assignedFlag;
			}
			return !assignedFlag;
		});
	};

	render() {
		const { form, product, organizationSlug, campaign } = this.props;
		const { adminOverride, disabled } = this.state;
		const { getFieldDecorator } = form;
		const { campaignCompensationId } = this.props;

		const inputDisabled = campaign && campaign.invitesSent && !adminOverride;
		const influencers = campaign && campaign.instagramOwners && campaign.instagramOwners.edges;
		const available_influencers = this.filter_influencers(influencers, campaignCompensationId, false);
		const assigned_influencers = this.filter_influencers(influencers, campaignCompensationId, true);

		return (
			<React.Fragment>
				{campaign && campaign.invitesSent && (
					<React.Fragment>
						{isLoginAdminUser() || isSaler() || ghostUserIsSaler() ? (
							<Alert
								message='Product can not be edited'
								description={
									<React.Fragment>
										<p>
											Products can't be edited after invites are sent. As admin, you can override this. Make sure that all involved parties have agreed to any
											new terms.
										</p>
										<Checkbox className='mt-10' value={true} checked={adminOverride} onChange={this.handleAdminOverride} style={{ color: 'red' }}>
											{adminOverride ? 'Using admin privilege to override' : 'Use admin privilege to override'}
										</Checkbox>
									</React.Fragment>
								}
								type={adminOverride ? 'error' : 'warning'}
							/>
						) : (
							<Alert message='Product can not be edited' description="Products can't be edited after invites are sent." type='warning' showIcon />
						)}
					</React.Fragment>
				)}
				<Form layout='vertical' className='payment-form'>
					<Row gutter={30}>
						<Col sm={{ span: 8 }} className='full-size-uploader'>
							{getFieldDecorator('productImageId')(
								<UploadImage
									product={product}
									newImageIdHandler={this.handleNewImageId}
									organizationSlug={organizationSlug}
									disabled={inputDisabled}
									onPhotoUploading={this.handlePhotoUploading}
									onPhotoUploadDone={this.handlePhotoUploadDone}
								/>
							)}
						</Col>
						<Col sm={{ span: 16 }}>
							<FormItem label='Product name'>
								{getFieldDecorator('name', {
									rules: [{ required: true, message: 'Please input the name of the product' }]
								})(<Input size='large' disabled={inputDisabled} />)}
							</FormItem>
							<FormItem label='Description'>
								{getFieldDecorator('description')(<Input.TextArea data-ripple disabled={inputDisabled} autosize={{ minRows: 2, maxRows: 6 }} />)}
							</FormItem>
						</Col>
					</Row>
					<Divider />
					<FormItem label='Link to product on your website'>
						{getFieldDecorator('link', {
							rules: [{ type: 'url', message: 'Please input a valid url' }]
						})(<Input size='large' placeholder='http://mywebsite.com/myproductlink' disabled={inputDisabled} />)}
					</FormItem>
					<FormItem label={`Product cost (${campaign && campaign.currency})`} className='has-extra-label'>
						{getFieldDecorator('cost', {
							rules: [
								{ required: true, message: 'Enter a product cost' },
								{
									validator: (rule, value, callback) => {
										callback(value <= 0 ? [new Error()] : []);
									},
									message: 'Cost must be equal or greater than 1'
								}
							]
						})(<InputNumber size='large' style={{ width: '100%' }} min={1} disabled={inputDisabled} />)}
						<span className='input-extra-label'>This is only visible to you</span>
					</FormItem>
					<FormItem label={`Product value (${campaign && campaign.currency})`} className='has-extra-label'>
						{getFieldDecorator('value', {
							rules: [
								{ required: true, message: 'Enter a product value' },
								{
									validator: (rule, value, callback) => {
										callback(value < 0 ? [new Error()] : []);
									},
									message: 'Value can not be negative'
								}
							]
						})(<InputNumber size='large' style={{ width: '100%' }} min={0} disabled={inputDisabled} />)}{' '}
						<span className='input-extra-label'>This is visible to influencers</span>
					</FormItem>
					<FormItem label='Number of products each influencer will receive' className='has-extra-label'>
						{getFieldDecorator('quantity', {
							rules: [
								{ required: true, message: 'Enter how many products you will send' },
								{
									validator: (rule, value, callback) => {
										callback(value < 0 ? [new Error()] : []);
									},
									message: 'Value can not be negative'
								}
							]
						})(<InputNumber size='large' style={{ width: '100%' }} min={0} disabled={inputDisabled} />)}
					</FormItem>
					<FormItem label='Add influencer to product' className='has-extra-label'>
						{getFieldDecorator('influencerIds', {})(
							<PeopleSelect
								options={available_influencers}
								handleOnSelect={this.handleOnSelect}
								disabledFlag={disabled}
								handleOnDeSelect={this.handleOnDeselect}
								disabled={inputDisabled}
								currency={campaign.currency}
							/>
						)}
					</FormItem>
					<div className='ant-col ant-form-item-label' style={{ width: '100%', textAlign: 'left' }}>
						<label className='ant-form-item-required' title='Involved influencers'>
							Assigned influencers
						</label>
						{Object.entries(assigned_influencers).map(([id, node]) => (
							<Row>
								<PersonTag node={node} />
							</Row>
						))}
					</div>
					<FormItem label='Product vat'>
						{getFieldDecorator('vat', {
							initialValue: 25,
							rules: [{ required: true, message: 'Enter a product value' }]
						})(
							<Radio.Group disabled={inputDisabled}>
								<Radio value={25}>25%</Radio>
								<Radio value={12}>12%</Radio>
								<Radio value={6}>6%</Radio>
								<Radio value={0}>0%</Radio>
							</Radio.Group>
						)}
					</FormItem>
				</Form>
			</React.Fragment>
		);
	}
}

const WrappedProductForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const propFields = props.fields || {};
		let formFields = {};
		Object.keys(propFields).forEach((propFieldKey) => {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		});

		return formFields;
	}
})(ProductForm);

export default WrappedProductForm;
