import React from 'react';
import { Modal, Button, Checkbox, Alert, Tabs } from 'antd';
import { ghostUserIsSaler, isLoginAdminUser, isSaler } from 'services/auth-service.js';
import AddToCommissionForm from './add-to-commission-form';
import CreatePaymentInvoiceFormV2 from './create-payment-invoice-form-v2';

const { TabPane } = Tabs;
class EditInvoiceModalV2 extends React.Component {
	state = {
		isModalVisible: false,
		adminOverride: false,
		selectedInfluencers: [],
		tabKey: 1
	};

	showModal = () => {
		const influencers = this.props.influencers;
		const selected = this.props.selectedRowKeys
			.map((id) => {
				return influencers.find((influ) => influ.node.id === id);
			})
			.map((node) => '@' + node.node.instagramOwner.instagramOwnerUser.username);

		this.setState({ isModalVisible: true, selectedInfluencers: selected });
	};

	handleCancel = () => {
		this.setState({ isModalVisible: false });
	};
	handleAdminOverride = (e) => {
		const override = e.target.checked;
		this.setState({ adminOverride: override });
	};

	updateTabKey = (tabKey) => {
		this.setState({ tabKey });
	};

	render() {
		const { campaign, selectedRowKeys } = this.props;
		const { isModalVisible, adminOverride, tabKey } = this.state;

		return (
			<div>
				<Button type='primary' onClick={this.showModal} disabled={selectedRowKeys.length > 0 ? false : true}>
					Edit
				</Button>
				<Modal
					title={`Edit Payment for ${this.state.selectedInfluencers.join(', ')}`}
					visible={isModalVisible}
					onCancel={this.handleCancel}
					maskClosable={false}
					wrapClassName='custom-modal payment-modal title-center'
					footer={[]}
				>
					{campaign && campaign.invitesSent && (
						<React.Fragment>
							{isLoginAdminUser() || isSaler() || ghostUserIsSaler() ? (
								<Alert
									message='Payment can not be edited'
									description={
										<React.Fragment>
											<p>
												Payments can't be edited after invites are sent. As admin, you can override this. Make sure that all involved parties have agreed to any
												new terms.
											</p>
											<Checkbox className='mt-10' value={true} checked={adminOverride} onChange={this.handleAdminOverride} style={{ color: 'red' }}>
												{adminOverride ? 'Using admin privilege to override' : 'Use admin privilege to override'}
											</Checkbox>
										</React.Fragment>
									}
									type={adminOverride ? 'error' : 'warning'}
								/>
							) : (
								<Alert message='Payment can not be edited' description="Payments can't be edited after invites are sent." type='warning' showIcon />
							)}
						</React.Fragment>
					)}
					<Tabs defaultActiveKey='1' onChange={this.updateTabKey} style={{ padding: '3px' }}>
						<TabPane tab='Add to existing commission' key='1'>
							<AddToCommissionForm
								adminOverride={adminOverride}
								handleCancel={this.handleCancel}
								tabKey={tabKey}
								selectedRowKeys={selectedRowKeys}
								campaign={campaign}
							/>
						</TabPane>
						<TabPane tab='Create new commission' key='2'>
							<CreatePaymentInvoiceFormV2
								campaign={campaign}
								adminOverride={adminOverride}
								handleCancel={this.handleCancel}
								tabKey={tabKey}
								selectedRowKeys={selectedRowKeys}
							/>
						</TabPane>
					</Tabs>
				</Modal>
			</div>
		);
	}
}

export default EditInvoiceModalV2;
