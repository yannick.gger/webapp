import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Query } from 'react-apollo';
import { Modal, Tabs, Spin } from 'antd';
import Payments from './payments';
import PaymentsByInvoice from './payments-by-invoice';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';

class PaymentTabs extends Component {
	handleCancel = () => {
		this.props.closeModal();
	};

	callback = (key) => {
		console.log(key);
	};

	render() {
		const { TabPane } = Tabs;
		const { location } = this.props;
		const id = location && location.pathname.split('/')[3];
		return (
			<Query query={getCampaignForCompensationFormsQuery} variables={{ id }}>
				{({ data, loading }) => {
					if (loading) {
						return <Spin className='collabspin' />;
					}
					const { campaign } = data;
					return (
						<Modal
							title='Create a new payment compensation'
							visible
							wrapClassName='custom-modal payment-modal title-center'
							maskClosable={false}
							onCancel={this.handleCancel}
							footer={[]}
						>
							<Tabs defaultActiveKey='1' onChange={this.callback} style={{ padding: '3px' }}>
								<TabPane tab='Invoice' key='1'>
									<PaymentsByInvoice campaign={campaign} location={location} handleCloseModal={this.handleCancel} />
								</TabPane>
							</Tabs>
						</Modal>
					);
				}}
			</Query>
		);
	}
}

export default withRouter(PaymentTabs);
