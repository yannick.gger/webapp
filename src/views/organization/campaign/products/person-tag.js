import React, { Component } from 'react';

class PersonTag extends Component {
	render() {
		const { node, currency } = this.props;
		const instagramOwnerUser = node.node.instagramOwner && node.node.instagramOwner.instagramOwnerUser;
		const avatar = instagramOwnerUser && instagramOwnerUser.avatar;
		const username = instagramOwnerUser && instagramOwnerUser.username;
		const compensation = node.node.campaignCompensations.nodes.find((compen) => compen.invoice);
		const value = compensation && compensation.invoice && compensation.invoice.value;
		return (
			<div className='personTag'>
				<div>
					<img src={avatar} alt='avatar' />
					{username}
				</div>
				<span>{currency && value ? `${value} ${currency}` : ''}</span>
			</div>
		);
	}
}

export default PersonTag;
