import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import CreatePaymentModal from './create-payment-modal';
import './styles.scss';

class PaymentsWithQuery extends Component {
	render() {
		const { campaign, location, handleCloseModal } = this.props;

		return (
			<React.Fragment>
				<Query query={getCampaignCompensationsQuery} variables={{ campaignId: campaign.id }} notifyOnNetworkStatusChange>
					{(queryRes) => {
						const { data } = queryRes;

						return (
							<React.Fragment>
								<CreatePaymentModal campaign={data.campaign} location={location} handleCloseModal={handleCloseModal} />
							</React.Fragment>
						);
					}}
				</Query>
			</React.Fragment>
		);
	}
}

export default withRouter(PaymentsWithQuery);
