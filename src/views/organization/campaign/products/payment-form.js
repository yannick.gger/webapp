import { Alert, Form, InputNumber, Row, Select, Spin } from 'antd';
import React, { Component } from 'react';
import PersonTag from './person-tag';
import './styles.scss';

const FormItem = Form.Item;
const { Option } = Select;

const PeopleSelect = ({ options, ...props }) => {
	if (options) {
		const { handleOnSelect, handleOnDeSelect, disabledFlag, currency } = props;
		return (
			<Select
				mode='multiple'
				className='peopleBar'
				tagRender={({ label }) => label}
				{...props}
				size='large'
				onSelect={(value, event) => handleOnSelect(value, event)}
				onDeselect={(value, event) => handleOnDeSelect(value, event)}
			>
				<Option key='all' value='all' selected>
					<span className='personTag'>Default (All influencers)</span>
				</Option>
				{Object.entries(options).map(([id, node]) => (
					<Option key={node.node.id} value={node.node.id} disabled={disabledFlag}>
						<PersonTag node={node} currency={currency} />
					</Option>
				))}
			</Select>
		);
	} else {
		return <Spin className='collabspin' />;
	}
};

class PaymentForm extends Component {
	state = {
		disabled: false
	};

	handleOnSelect = (selectedValue, e) => {
		if (selectedValue === 'all') {
			this.setState({ disabled: true });
		}
	};

	handleOnDeselect = (selectedValue, e) => {
		this.setState({ disabled: false });
	};

	filter_influencers = (influencers, campaignCompensationId, flag) => {
		return influencers.filter((influencer) => {
			for (let value of Object.values(influencer.node.campaignCompensations.nodes)) {
				if (value.id === campaignCompensationId) return flag;
			}
			return !flag;
		});
	};

	render() {
		const { disabled } = this.state;
		const { form, campaign } = this.props;
		const { getFieldDecorator } = form;
		const { invitesSent, currency } = campaign;
		const { campaignCompensationId } = this.props;
		const influencers = campaign && campaign.instagramOwners && campaign.instagramOwners.edges;
		const available_influencers = this.filter_influencers(influencers, campaignCompensationId, false);
		const involved_influencers = this.filter_influencers(influencers, campaignCompensationId, true);
		return (
			<React.Fragment>
				{campaign && invitesSent && (
					<Alert message='Payment can not be edited' description="Payments can't be edited after invites are sent." type='warning' showIcon />
				)}
				<Form layout='vertical'>
					<Row gutter={30}>
						<FormItem label={`Payment value (${campaign && currency})`} className='has-extra-label'>
							{getFieldDecorator('value', {
								rules: [
									{ required: true, message: 'Enter a payment value' },
									{
										validator: (rule, value, callback) => {
											callback(value < 100 ? [new Error()] : []);
										},
										message: 'Smallest possible payment is 100 EUR'
									},
									{
										validator: (rule, value, callback) => {
											callback(value > 5000 ? [new Error()] : []);
										},
										message: 'Highest possible payment is 5000 EUR (Contact us for change of limit)'
									}
								]
							})(<InputNumber size='large' style={{ width: '100%' }} disabled={campaign && invitesSent} />)}{' '}
							<span className='input-extra-label'>This is what you will pay each influencer</span>
						</FormItem>
						<FormItem label='Max slots' className='has-extra-label'>
							{getFieldDecorator('maxSlots', {
								rules: [
									{ required: true, message: 'Enter a value' },
									{
										validator: (rule, maxSlots, callback) => {
											callback(maxSlots >= 1 ? [] : [new Error()]);
										},
										message: 'Smallest possible slot is 1'
									}
								]
							})(<InputNumber size='large' style={{ width: '100%' }} disabled={campaign && invitesSent} />)}{' '}
							<span className='input-extra-label'>Max influencer slots for invoice</span>
						</FormItem>
						<FormItem label='Add influencer to invoice' className='has-extra-label'>
							{getFieldDecorator('influencerIds', {})(
								<PeopleSelect
									options={available_influencers}
									handleOnSelect={this.handleOnSelect}
									disabledFlag={disabled}
									handleOnDeSelect={this.handleOnDeselect}
									disabled={campaign && invitesSent}
									currency={campaign.currency}
								/>
							)}
						</FormItem>
					</Row>
				</Form>
				<div className='ant-col ant-form-item-label' style={{ width: '100%', textAlign: 'left' }}>
					<label className='ant-form-item-required' title='Involved influencers'>
						Assigned influencers
					</label>
					{Object.entries(involved_influencers).map(([id, node]) => (
						<Row>
							<PersonTag node={node} />
						</Row>
					))}
				</div>
				<Alert
					className='mt-20'
					message={
						'Right now payment is only supported to Swedish companies. Any influencer can be invited but for them to join the campaign they will have to have a Swedish company.'
					}
					type='info'
					showIcon
				/>
			</React.Fragment>
		);
	}
}

const WrappedPaymentForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const propFields = props.fields || {};
		let formFields = {};
		Object.keys(propFields).forEach((propFieldKey) => {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		});

		return formFields;
	}
})(PaymentForm);

export default WrappedPaymentForm;
