import React from 'react';
import { Modal, Button, message, Spin } from 'antd';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';

import ProductForm from './product-form';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';

const CREATE_PRODUCT = gql`
	mutation createCampaignCompensationProduct(
		$campaignId: ID!
		$name: String!
		$link: String
		$description: String
		$value: Float
		$cost: Float
		$vat: Int
		$productImageId: ID
		$quantity: Int
		$influencerIds: [ID!]
	) {
		createCampaignCompensationProduct(
			input: {
				campaignId: $campaignId
				name: $name
				link: $link
				description: $description
				value: $value
				cost: $cost
				vat: $vat
				productImageId: $productImageId
				quantity: $quantity
				influencerIds: $influencerIds
			}
		) {
			campaignCompensation {
				id
			}
		}
	}
`;

class CreateProductModal extends React.Component {
	state = {
		photoUploadStatus: false,
		adminOverride: false
	};

	handlePhotoUploading = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: true }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handlePhotoUploadDone = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: false }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handleSubmit = (createCampaignCompensationProduct) => {
		return (e) => {
			e.preventDefault();

			if (this.state.photoUploadStatus) return message.info('Photo upload is still in progress. Please wait a moment.');

			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					campaignId: this.props.location.pathname.split('/')[3]
				};

				await createCampaignCompensationProduct({ variables: graphQlValues }).then(
					(res) => {
						message.info('Product was created');
						this.props.closeModal();
						this.forceUpdate();
					},
					(error) => {
						message.error(error.message);
					}
				);
			});
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	handleAdminOverride = (adminOverride) => {
		this.setState({ adminOverride });
	};

	render() {
		return (
			<Query query={getCampaignForCompensationFormsQuery} variables={{ id: this.props.match.params.campaignId }}>
				{({ data, loading }) => {
					if (loading || !data) return <Spin className='collabspin' />;

					return (
						<Mutation
							mutation={CREATE_PRODUCT}
							refetchQueries={[
								{ query: getCampaignCompensationsQuery, variables: { campaignId: data.campaign.id } },
								{ query: getCampaignForCompensationFormsQuery, variables: { id: data.campaign.id } }
							]}
						>
							{(createCampaignCompensationProduct, { loading }) => (
								<Modal
									title='Create a new product'
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									wrapClassName='custom-modal product-modal title-center'
									maskClosable={false}
									footer={[
										<Button size='large' key='back' onClick={this.handleCancel}>
											Return
										</Button>,
										<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(createCampaignCompensationProduct)}>
											Create product
										</Button>
									]}
								>
									<ProductForm
										campaign={data.campaign}
										wrappedComponentRef={this.saveFormRef}
										organizationSlug={this.props.match.params.organizationSlug}
										onPhotoUploading={this.handlePhotoUploading}
										onPhotoUploadDone={this.handlePhotoUploadDone}
										enableSave={this.handleAdminOverride}
									/>
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

export default CreateProductModal;
