import { Button, message, Spin, Alert } from 'antd';
import createCompensationPaymentQuery from 'graphql/create-compensation-payment.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { OrganizationLink } from 'components';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';
import PaymentForm from './payment-form';

// This is payment by card tab when creating commission
class CreatePaymentModal extends React.Component {
	handleSubmit = (createCampaignCompensationPayment) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					campaignId: this.props.location.pathname.split('/')[3]
				};

				const res = await createCampaignCompensationPayment({ variables: graphQlValues });

				if (res.data.createCampaignCompensationPayment.errors.length > 0) {
					res.data.createCampaignCompensationPayment.errors.forEach((error) => {
						message.error(error.message);
					});
				} else if (res.data.createCampaignCompensationPayment.errors.length === 0) {
					message.success('Payment compensation was created');
					this.handleCancel();
					this.forceUpdate();
				}
			});
		};
	};

	handleCancel = () => {
		this.props.handleCloseModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		const { location } = this.props;
		const organizationSlug = location.pathname.split('/')[0];
		const id = location.pathname.split('/')[3];
		return (
			<Query query={getCampaignForCompensationFormsQuery} variables={{ id }}>
				{({ data, loading }) => {
					if (loading || !data) return <Spin className='collabspin' />;
					const { campaign } = data;
					const { id, currency, invoiceCompensationsCount, organization } = campaign;
					const country = organization.country;
					let paymentFormNode = null;
					if (invoiceCompensationsCount > 0) {
						paymentFormNode = (
							<Alert
								showIcon
								type='warning'
								title=''
								description='To be able to use "Payment by card" you first have to remove "Payment by invoice" that currently is being used.'
							/>
						);
					} else if (currency === 'EUR' && country === 'SE') {
						paymentFormNode = <PaymentForm campaign={campaign} wrappedComponentRef={this.saveFormRef} organizationSlug={organizationSlug} />;
					} else if (country !== 'SE') {
						paymentFormNode = <span>Payments to influencers are only available to Swedish organizations currently.</span>;
					} else {
						paymentFormNode = (
							<span>
								You need to <OrganizationLink to={`/campaigns/${id}/settings`}>update your campaign</OrganizationLink> to have Euro (EUR) as currency to have
								card payments on your campaign.
							</span>
						);
					}
					return (
						<Mutation
							mutation={createCompensationPaymentQuery}
							refetchQueries={[{ query: getCampaignCompensationsQuery, variables: { campaignId: data.campaign.id } }]}
						>
							{(createCampaignCompensationPayment, { loading }) => (
								<div>
									{paymentFormNode}
									<div className='payment-btn-wrapper'>
										<Button size='large' key='back' onClick={this.handleCancel}>
											Cancel
										</Button>
										{currency === 'EUR' && country === 'SE' && invoiceCompensationsCount < 1 && (
											<Button
												className='ml-20'
												size='large'
												key='submit'
												type='primary'
												loading={loading}
												onClick={this.handleSubmit(createCampaignCompensationPayment)}
											>
												Create payment
											</Button>
										)}
									</div>
								</div>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

export default CreatePaymentModal;
