import React from 'react';
import { Modal, Button, message, Spin, Alert } from 'antd';
import { Mutation, Query } from 'react-apollo';
import { OrganizationLink } from 'components';
import PaymentForm from './payment-form';
import getCampaignForCompensationFormsQuery from './get-campaign-for-compensation-forms.graphql';
import getCampaignCompensationQuery from 'graphql/get-campaign-compensation.graphql';
import updateCampaignCompensationPaymentQuery from 'graphql/update-campaign-compensation-payment.graphql';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';

class EditPaymentModal extends React.Component {
	state = {
		fields: {}
	};
	static getDerivedStateFromProps = (props, state) => {
		let propsFields = {};

		if (props.campaignCompensation) {
			Object.keys(props.campaignCompensation.payment)
				.filter((key) => ['value'].includes(key))
				.forEach((key) => {
					propsFields[key] = {
						value: props.campaignCompensation.payment[key]
					};
				});
			Object.keys(props.campaignCompensation)
				.filter((key) => ['maxSlots'].includes(key))
				.forEach((key) => {
					propsFields[key] = {
						value: props.campaignCompensation[key]
					};
				});
		}

		return { fields: { ...propsFields, ...state.fields } };
	};

	handleSubmit = ({ updateCampaignCompensationPayment, campaignCompensationId }) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;

				const graphQlValues = {
					...values,
					id: campaignCompensationId
				};

				const res = await updateCampaignCompensationPayment({ variables: graphQlValues });
				if (res.data.updateCampaignCompensationPayment.errors.length > 0) {
					res.data.updateCampaignCompensationPayment.errors.forEach((error) => {
						message.error(error.message);
					});
				} else if (res.data.updateCampaignCompensationPayment.errors.length === 0) {
					message.success('Payment compensation was updated');
					this.props.closeModal();
					this.forceUpdate();
				}
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	handleFormChange = (changedFields) => {
		this.setState(({ fields }) => ({
			fields: { ...fields, ...changedFields }
		}));
	};
	render() {
		const { fields } = this.state;
		const { campaignCompensation } = this.props;

		return (
			<Query query={getCampaignForCompensationFormsQuery} variables={{ id: this.props.match.params.campaignId }}>
				{({ data, loading }) => {
					if (loading || !data) return <Spin className='collabspin' />;

					return (
						<Mutation
							mutation={updateCampaignCompensationPaymentQuery}
							refetchQueries={[{ query: getCampaignCompensationsQuery, variables: { campaignId: data.campaign.id } }]}
						>
							{(updateCampaignCompensationPayment, { loading }) => (
								<Modal
									title='Edit payment'
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									wrapClassName='custom-modal payment-modal title-center'
									maskClosable={false}
									footer={[
										<Button size='large' key='back' onClick={this.handleCancel}>
											Cancel
										</Button>,
										data.campaign.currency === 'EUR' && data.campaign.organization.country === 'SE' && (
											<Button
												size='large'
												key='submit'
												type='primary'
												loading={loading}
												onClick={this.handleSubmit({
													updateCampaignCompensationPayment,
													campaignCompensationId: this.props.match.params.campaignCompensationId
												})}
												disabled={data.campaign.invitesSent}
											>
												Save
											</Button>
										)
									]}
								>
									{data.campaign.currency === 'EUR' || data.organization.country === 'SE' ? (
										<PaymentForm
											fields={fields}
											campaign={data.campaign}
											wrappedComponentRef={this.saveFormRef}
											onChange={this.handleFormChange}
											payment={{ ...campaignCompensation.payment, quantity: campaignCompensation.quantity }}
											organizationSlug={this.props.match.params.organizationSlug}
											campaignCompensationId={this.props.match.params.campaignCompensationId}
										/>
									) : (
										<Alert
											showIcon
											type='warning'
											title=''
											description={
												data.campaign.organization.country !== 'SE' ? (
													<span>Payments to influencers are only available to Swedish organizations currently.</span>
												) : (
													<span>
														You need to <OrganizationLink to={`/campaigns/${data.campaign.id}/settings`}>update your campaign</OrganizationLink> to have Euro
														(EUR) as currency to have payments on your campaign.
													</span>
												)
											}
										/>
									)}
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

class EditPaymentModalWithPayment extends React.Component {
	render() {
		return (
			<Query query={getCampaignCompensationQuery} variables={{ id: this.props.match.params.campaignCompensationId }}>
				{({ data, loading, error }) => {
					if (loading) {
						return <Spin className='collabspin' />;
					}

					if (error) {
						return <span>Failed to load payment</span>;
					}

					const { campaignCompensation } = data;
					return <EditPaymentModal campaignCompensation={campaignCompensation} {...this.props} />;
				}}
			</Query>
		);
	}
}

export default EditPaymentModalWithPayment;
