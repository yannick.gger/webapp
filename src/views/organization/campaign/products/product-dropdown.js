import { Dropdown, Icon, Menu, message, Modal } from 'antd';
import ButtonLink from 'components/button-link';
import OrganizationLink from 'components/organization-link/';
import gql from 'graphql-tag';
import getCampaignCompensationsQuery from 'graphql/get-campaign-compensations.graphql';
import getCampaignInstagramOwnersQuery from 'graphql/get-campaign-instagram-owners.graphql';
import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';
import { isAdmin, hasGhostToken } from 'services/auth-service';

const MenuItemGroup = Menu.ItemGroup;

const REMOVE_COMPENSATION = gql`
	mutation removeCampaignCompensation($id: ID!) {
		removeCampaignCompensation(input: { id: $id }) {
			campaignCompensation {
				id
			}
		}
	}
`;

export default class ProductDropdown extends Component {
	menu = (
		<Menu>
			<MenuItemGroup title='Quick actions'>
				<Menu.Item key='1' data-ripple='rgba(0, 0, 0, 0.3)'>
					{this.props.campaignCompensation.product && (
						<OrganizationLink to={`/campaigns/${this.props.campaign.id}/products/${this.props.campaignCompensation.id}/edit`}>
							<Icon type='edit' className='mr-10' /> Edit
						</OrganizationLink>
					)}
					{this.props.campaignCompensation.payment && (
						<OrganizationLink to={`/campaigns/${this.props.campaign.id}/products/${this.props.campaignCompensation.id}/edit-payment`}>
							<Icon type='edit' className='mr-10' /> Edit
						</OrganizationLink>
					)}
					{this.props.campaignCompensation.invoice && (
						<OrganizationLink to={`/campaigns/${this.props.campaign.id}/products/${this.props.campaignCompensation.id}/edit-invoice`}>
							<Icon type='edit' className='mr-10' /> Edit
						</OrganizationLink>
					)}
				</Menu.Item>
				<Menu.Item key='2' data-ripple='rgba(0, 0, 0, 0.3)'>
					<ApolloConsumer>
						{(client) => (
							<ButtonLink
								className='color-red'
								onClick={() => {
									this.props.campaign.invitesSent && !isAdmin() && !hasGhostToken()
										? message.error("Product can't be deleted after invite has been sent.")
										: this.showDeleteConfirm({ client: client, compensationId: this.props.campaignCompensation.id, campaignId: this.props.campaign.id });
								}}
							>
								<Icon type='delete' className='mr-10' /> Delete
							</ButtonLink>
						)}
					</ApolloConsumer>
				</Menu.Item>
			</MenuItemGroup>
		</Menu>
	);

	showDeleteConfirm = ({ client, compensationId, campaignId }) => {
		const { campaignCompensation } = this.props;
		const { product, payment, invoice } = campaignCompensation;
		let compensationType = '';
		if (product) {
			compensationType = 'product';
		} else if (payment) {
			compensationType = 'payment';
		} else if (invoice) {
			compensationType = 'invoice payment';
		}

		Modal.confirm({
			title: `Are you sure you want to remove this ${compensationType} ?`,
			content: `A removed ${compensationType} can not be restored after removal.`,
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				client
					.mutate({
						mutation: REMOVE_COMPENSATION,
						variables: { id: compensationId },
						refetchQueries: [
							{ query: getCampaignCompensationsQuery, variables: { campaignId: campaignId } },
							{ query: getCampaignInstagramOwnersQuery, variables: { campaignId: campaignId } }
						]
					})
					.then(() => {
						message.info('Commission was removed');
					});
			}
		});
	};

	render() {
		return (
			<React.Fragment>
				<Dropdown overlay={this.menu} trigger={['click']} placement='bottomRight'>
					<Icon type='ellipsis' />
				</Dropdown>
			</React.Fragment>
		);
	}
}
