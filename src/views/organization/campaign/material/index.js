import React, { Component } from 'react';
import { Row, Col } from 'antd';

import CampaignLayout from '../layout/';
import UploadLogo from './components/upload-logo';
import UploadCover from './components/upload-cover';
// import UploadBrandAssets from "./components/upload-assets"
import UploadInspiration from './components/upload-inspiration';

export default class MaterialTab extends Component {
	render() {
		return (
			<CampaignLayout>
				{({ campaign }) => (
					<div className='clearfix'>
						<Row className='white-box'>
							<Col md={{ span: '12' }}>
								<div className='widget-content'>
									<h3>Logo</h3>
									<p>Supported file formats is SVG, JPG and PNG. The logo should be size 250px * 70px. Larger images will be scaled to fit.</p>
									<UploadLogo campaign={campaign} />
								</div>
							</Col>
							<Col md={{ span: '12' }} className='border-left'>
								<div className='widget-content'>
									<h3>Cover Photo</h3>
									<p>Supported file format is JPG and PNG. Cover photo should be 1340px * 560px. This is used for your landingpage.</p>
									<UploadCover campaign={campaign} />
								</div>
							</Col>
						</Row>
						<Row className='white-box mt-30'>
							<Col>
								<UploadInspiration campaign={campaign} />
							</Col>
						</Row>
					</div>
				)}
			</CampaignLayout>
		);
	}
}
