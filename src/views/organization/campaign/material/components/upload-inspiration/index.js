import React, { Component } from 'react';
import { Upload, Icon, Modal, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';

const CREATE_CAMPAIGN_INSPIRATION = gql`
	mutation createCampaignInspiration($inspiration: Upload!, $campaignId: ID!) {
		createCampaignInspiration(input: { inspiration: $inspiration, campaignId: $campaignId }) {
			campaignInspiration {
				id
				url
			}
		}
	}
`;

const REMOVE_CAMPAIGN_INSPIRATION = gql`
	mutation removeCampaignInspiration($id: ID!) {
		removeCampaignInspiration(input: { id: $id }) {
			campaignInspiration {
				id
			}
		}
	}
`;

export default class UploadInspiration extends Component {
	state = {
		previewVisible: false,
		previewImage: '',
		fileList: []
	};

	handleCancel = () => this.setState({ previewVisible: false });

	handlePreview = (file) => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleRemove = ({ client }) => {
		return ({ uid }) => {
			return client.mutate({
				mutation: REMOVE_CAMPAIGN_INSPIRATION,
				variables: { id: uid },
				refetchQueries: ['getOrganizationCampaign']
			});
		};
	};

	handleCustomRequest = ({ client, campaign }) => {
		return ({ file, onError, onSuccess, onProgress }) => {
			client
				.mutate({
					mutation: CREATE_CAMPAIGN_INSPIRATION,
					variables: { inspiration: file, campaignId: campaign.id },
					refetchQueries: ['getOrganizationCampaign']
				})
				.then(({ data, loading, error }) => {
					if (error) onError();
					if (data) {
						if (error) onError();
						if (data) {
							onSuccess(data, file);
						}
					}

					if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);
				});

			return {
				abort() {
					console.log('upload progress is aborted.');
				}
			};
		};
	};

	handleChange = (info) => {
		const status = info.file.status;
		this.setState({ fileList: info.fileList });

		if (status === 'done') {
			message.success(`${info.file.name} file uploaded successfully.`);
		} else if (status === 'error') {
			message.error(`${info.file.name} file upload failed.`);
		}
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		return {
			...prevState,
			fileList: nextProps.campaign.campaignInspirations.map((inspiration) => ({
				uid: inspiration.id,
				name: inspiration.id,
				status: 'done',
				url: inspiration.thumbnailSquare
			}))
		};
	}

	beforeUpload = (file) => {
		const { campaign } = this.props;

		if (campaign.campaignInspirations.length >= 12) {
			message.error('Maximum of moodboard images is 12.');
			return false;
		}

		const sizeInMB = file.size / (1024 * 1024);
		if (sizeInMB > 10) {
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');
			return false;
		}
		return true;
	};

	render() {
		const { previewVisible, previewImage, fileList } = this.state;
		const { campaign } = this.props;
		const uploadButton = (
			<div>
				<Icon type='plus' />
				<div className='ant-upload-text'>Upload</div>
			</div>
		);
		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<div className='padded-block'>
							<h3>Moodboard</h3>
							<p>Upload images to be used as inspiration for influencers</p>
							<Upload
								multiple
								accept='image/*'
								listType='picture-card'
								fileList={fileList}
								onRemove={this.handleRemove({ client, campaign })}
								onPreview={this.handlePreview}
								onChange={this.handleChange}
								customRequest={this.handleCustomRequest({ client, campaign })}
								beforeUpload={this.beforeUpload}
							>
								{uploadButton}
							</Upload>
							<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
								<img alt='example' style={{ width: '100%' }} src={previewImage} />
							</Modal>
						</div>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}
