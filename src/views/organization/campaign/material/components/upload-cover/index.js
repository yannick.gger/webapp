import React, { Component } from 'react';
import { Upload, Icon, Modal, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import { checkGhostUserIsSaler, firstOrganizationSlug } from 'services/auth-service';

const CREATE_CAMPAIGN_COVER_PHOTO = gql`
	mutation createCampaignCoverPhoto($coverPhoto: Upload!, $organizationSlug: String!) {
		createCampaignCoverPhoto(input: { coverPhoto: $coverPhoto, organizationSlug: $organizationSlug }) {
			campaignCoverPhoto {
				id
				url
			}
		}
	}
`;

const REMOVE_CAMPAIGN_COVER_PHOTO = gql`
	mutation removeCampaignCoverPhoto($id: ID!) {
		updateCampaign(input: { id: $id, campaignCoverPhotoId: null }) {
			campaign {
				id
			}
		}
	}
`;

const ADD_CAMPAIGN_COVER_PHOTO = gql`
	mutation addCampaignCoverPhoto($id: ID!, $campaignCoverPhotoId: ID!) {
		updateCampaign(input: { id: $id, campaignCoverPhotoId: $campaignCoverPhotoId }) {
			campaign {
				id
			}
		}
	}
`;

export default class UploadCoverPhoto extends Component {
	state = {
		previewVisible: false,
		previewImage: '',
		fileList: [],
		loading: false
	};

	handleCancel = () => this.setState({ previewVisible: false });

	handlePreview = (file) => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleRemove = ({ client, campaign }) => {
		return () => {
			return client.mutate({
				mutation: REMOVE_CAMPAIGN_COVER_PHOTO,
				variables: { id: campaign.id },
				refetchQueries: ['getOrganizationCampaign']
			});
		};
	};

	handleCustomRequest = ({ client, campaign }) => {
		return ({ file, onError, onSuccess, onProgress }) => {
			client
				.mutate({
					mutation: CREATE_CAMPAIGN_COVER_PHOTO,
					variables: { coverPhoto: file, organizationSlug: firstOrganizationSlug() }
				})
				.then(({ data, loading, error }) => {
					if (error) onError();
					if (data) {
						client
							.mutate({
								mutation: ADD_CAMPAIGN_COVER_PHOTO,
								variables: { campaignCoverPhotoId: data.createCampaignCoverPhoto.campaignCoverPhoto.id, id: campaign.id },
								refetchQueries: ['getOrganizationCampaign']
							})
							.then(({ data, error }) => {
								if (error) onError();
								if (data) {
									onSuccess(data, file);
								}
							});
					}

					if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);
				})
				.catch(({ graphQLErrors }) => {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
					onError();
				});

			return {
				abort() {
					console.log('upload progress is aborted.');
				}
			};
		};
	};

	handleChange = (info) => {
		const status = info.file.status;
		this.setState({ fileList: info.fileList });

		switch (status) {
			case 'uploading': {
				this.setState({ loading: true });
				break;
			}
			case 'done': {
				message.success(`${info.file.name} file uploaded successfully.`);
				this.setState({ loading: false });
				break;
			}
			case 'error': {
				message.error(`${info.file.name} file upload failed.`);
				this.setState({ loading: false });
				break;
			}
			case 'removed': {
				message.success(`${info.file.name} file removed successfully`);
				this.setState({ loading: false });
				break;
			}
			default: {
				this.setState({ loading: false });
				break;
			}
		}
	};

	static getDerivedStateFromProps(props, state) {
		return {
			...state,
			fileList: props.campaign.campaignCoverPhoto
				? [{ uid: props.campaign.campaignCoverPhoto.id, name: 'coverPhoto', status: 'done', url: props.campaign.campaignCoverPhoto.thumbnailSquare }]
				: []
		};
	}

	beforeUpload = (file) => {
		const sizeInMB = file.size / (1024 * 1024);
		if (sizeInMB > 10) {
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');
			return false;
		}
		return true;
	};
	render() {
		const { previewVisible, fileList, loading } = this.state;
		const { campaign } = this.props;
		const uploadButton = (
			<div>
				<Icon type={loading ? 'loading' : 'plus'} />
				<div className='ant-upload-text'>Upload</div>
			</div>
		);
		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<Upload
							listType='picture-card'
							accept='image/*'
							fileList={fileList}
							onRemove={this.handleRemove({ client, campaign })}
							onPreview={this.handlePreview}
							onChange={this.handleChange}
							customRequest={this.handleCustomRequest({ client, campaign })}
							beforeUpload={this.beforeUpload}
						>
							{fileList.length >= 1 ? null : uploadButton}
						</Upload>
						<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
							<img alt='example' style={{ width: '100%' }} src={campaign.campaignCoverPhoto ? campaign.campaignCoverPhoto.url : '/coverPhoto-sample.png'} />
						</Modal>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}
