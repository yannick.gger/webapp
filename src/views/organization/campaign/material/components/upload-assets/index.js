import React, { Component } from 'react';
import { Upload, Icon, Modal, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import { checkGhostUserIsSaler } from 'services/auth-service';

const CREATE_CAMPAIGN_BRAND_ASSET = gql`
	mutation createCampaignBrandAsset($brandAsset: Upload!, $campaignId: ID!) {
		createCampaignBrandAsset(input: { brandAsset: $brandAsset, campaignId: $campaignId }) {
			campaignBrandAsset {
				id
				url
			}
		}
	}
`;

const REMOVE_CAMPAIGN_BRAND_ASSET = gql`
	mutation removeCampaignBrandAsset($id: ID!) {
		removeCampaignBrandAsset(input: { id: $id }) {
			campaignBrandAsset {
				id
			}
		}
	}
`;

export default class UploadBrandAsset extends Component {
	state = {
		previewVisible: false,
		previewImage: '',
		fileList: []
	};

	handleCancel = () => this.setState({ previewVisible: false });

	handlePreview = (file) => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleRemove = ({ client }) => {
		return ({ uid }) => {
			return client.mutate({
				mutation: REMOVE_CAMPAIGN_BRAND_ASSET,
				variables: { id: uid }
			});
		};
	};

	handleCustomRequest = ({ client, campaign }) => {
		return ({ file, onError, onSuccess, onProgress }) => {
			client
				.mutate({
					mutation: CREATE_CAMPAIGN_BRAND_ASSET,
					variables: { brandAsset: file, campaignId: campaign.id }
				})
				.then(({ data, loading, error }) => {
					if (error) onError();
					if (data) {
						if (error) onError();
						if (data) {
							onSuccess(data, file);
						}
					}

					if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);
				});
			return {
				abort() {
					console.log('upload progress is aborted.');
				}
			};
		};
	};

	handleChange = (info) => {
		const status = info.file.status;
		this.setState({ fileList: info.fileList });

		if (status === 'done') {
			message.success(`${info.file.name} file uploaded successfully.`);
		} else if (status === 'error') {
			message.error(`${info.file.name} file upload failed.`);
		}
	};

	beforeUpload = (file) => {
		const sizeInMB = file.size / (1024 * 1024);
		if (sizeInMB > 10) {
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');
			return false;
		}
		return true;
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		return {
			...prevState,
			fileList: nextProps.campaign.campaignBrandAssets.map((brandAsset) => ({
				uid: brandAsset.id,
				name: brandAsset.id,
				status: 'done',
				url: brandAsset.thumbnailSquare
			}))
		};
	}

	render() {
		const { previewVisible, previewImage, fileList } = this.state;
		const { campaign } = this.props;
		const uploadButton = (
			<div>
				<Icon type='plus' />
				<div className='ant-upload-text'>Upload</div>
			</div>
		);
		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<div className='padded-block'>
							<h3>Brand assets</h3>
							<p>Other company related images</p>
							<Upload
								multiple
								accept='image/*'
								listType='picture-card'
								fileList={fileList}
								onRemove={this.handleRemove({ client, campaign })}
								onPreview={this.handlePreview}
								onChange={this.handleChange}
								customRequest={this.handleCustomRequest({ client, campaign })}
								beforeUpload={this.beforeUpload}
							>
								{!checkGhostUserIsSaler() && uploadButton}
							</Upload>
							<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
								<img alt='example' style={{ width: '100%' }} src={previewImage} />
							</Modal>
						</div>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}
