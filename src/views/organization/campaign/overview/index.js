import React from 'react';
import { Row, Col, Card, Steps, Popover } from 'antd';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { injectIntl } from 'react-intl';
import moment from 'moment';
import { translate } from 'react-i18next';
import i18n from 'i18next';

import withFlag from 'hocs/with-flag';
import { getTaskGroups, getCurrentTaskGroupId } from 'services/organization-campaign-tasks';
import { firstOrganizationSlug, checkGhostUserIsSaler } from 'services/auth-service';
import CampaignLayout from '../layout/';
import TaskDone from 'assets/img/app/task-done.svg';
import NotificationPanel from 'components/notification-panel';

import './styles.scss';

const Step = Steps.Step;

const customDot = (dot, { status, index }) => {
	return (
		<Popover
			content={
				<span>{`${i18n.t('organization:campaign.overview.step', { defaultValue: 'step' })} ${index} ${i18n.t('organization:campaign.overview.status', {
					defaultValue: 'status'
				})}: ${status}`}</span>
			}
		>
			{dot}
		</Popover>
	);
};

const OverviewHeader = ({ campaign }) => {
	return (
		<Row className='meta-stats border-bottom mt-10 pb-30 mb-30'>
			<Col sm={{ span: 8 }} lg={{ span: 8 }}>
				<h4>{i18n.t('organization:campaign.overview.header.spotsTaken', { defaultValue: 'Spots taken' })}</h4>
				<h2 className='color-blue'>
					<FormattedMessage
						id='instagram-owners-joined-count'
						defaultMessage={`{count} {count, plural, one {spot} other {spots}}`}
						values={{ count: campaign.instagramOwnersJoinedCount }}
					/>
				</h2>
				<span className='ant-list-item-action-split mr-30' />
			</Col>
			<Col sm={{ span: 8 }} lg={{ span: 8 }}>
				<h4>{i18n.t('organization:campaign.overview.header.spotsTotal', { defaultValue: 'Spots total' })}</h4>
				<h2 className='color-blue'>
					<FormattedMessage
						id='instagram-owners-target-count'
						defaultMessage={`{count} {count, plural, one {spot} other {spots}}`}
						values={{ count: campaign.influencerTargetCount }}
					/>
				</h2>

				<span className='ant-list-item-action-split mr-30' />
			</Col>
			<Col sm={{ span: 8 }} lg={{ span: 8 }}>
				<h4>{i18n.t('organization:campaign.overview.header.influencersToInvite', { defaultValue: 'Influencers to invite' })}</h4>
				<h2 className='color-blue'>
					<FormattedMessage
						id='instagram-owners-to-invite-count'
						defaultMessage={`{count} {count, plural, one {influencer} other {influencers}}`}
						values={{
							count:
								campaign.influencerTargetCount - campaign.instagramOwnersInvitedCount > 0
									? campaign.influencerTargetCount - campaign.instagramOwnersInvitedCount
									: 0
						}}
					/>
				</h2>
			</Col>
		</Row>
	);
};

const Overview = ({ campaign, intl }) => {
	const taskGroups = getTaskGroups({ campaign, organizationSlug: firstOrganizationSlug(), intl });
	const currentTaskGroupId = getCurrentTaskGroupId({ taskGroups });
	const renderCard = (task) => (
		<Card
			hoverable={!task.completed}
			data-ripple='rgba(132,146, 164, 0.2)'
			className={`action-card has-icon-left${task.disabled ? ' disabled' : ''} mb-20`}
			cover={<img src={task.completed ? TaskDone : task.illustration} alt={task.title} />}
		>
			<Card.Meta title={task.title} />
			<div slyle={{ float: 'right' }} />
			<p className='mt-5 mb-4'>{task.description}</p>
		</Card>
	);

	const renderLinkTo = (task, link) => () => {
		if (task.disabled) {
			return renderCard(task);
		}

		let isCommissionFixable = true;
		const lastReview = [...campaign.campaignReviews.edges].pop();

		if (lastReview) {
			isCommissionFixable = lastReview.node.status === 'declined';
		}

		let linkTo = link || task.link;
		if (task.type && task.type === 'commission') {
			linkTo = { pathname: link || task.link, state: { isCommissionFixable: isCommissionFixable } };
		}

		return (
			<>
				{isCommissionFixable && (
					<Link key={task.title} to={linkTo}>
						{renderCard(task)}
					</Link>
				)}
				{!isCommissionFixable && renderCard(task)}
			</>
		);
	};

	return (
		<React.Fragment>
			<OverviewHeader campaign={campaign} />
			<Row className='mb-30 pb-30 border-bottom'>
				<Col xs={{ span: 24 }} sm={{ span: 7 }} className='my-10'>
					<h3 className='mb-10'>{i18n.t('organization:campaign.overview.side.campaignTimeline', { defaultValue: 'Campaign timeline' })}</h3>
					<Steps current={currentTaskGroupId} progressDot={customDot} direction='vertical' className='large-spaces'>
						<Step
							title={i18n.t('organization:campaign.overview.side.title.prepareInvitation', { defaultValue: 'Prepare invitation' })}
							description={i18n.t('organization:campaign.overview.side.description.prepareInvitation', {
								defaultValue: 'Before sending campaign to influencers'
							})}
						/>
						<Step
							title={i18n.t('organization:campaign.overview.side.title.inviteInfluencers', { defaultValue: 'Invite influencers' })}
							description={i18n.t('organization:campaign.overview.side.description.inviteInfluencers', {
								defaultValue: 'You are ready to send out the invitation'
							})}
						/>
						{campaign.productCompensationsCount !== 0 && (
							<Step
								title={i18n.t('organization:campaign.overview.side.title.shipProducts', { defaultValue: 'Ship products' })}
								description={i18n.t('organization:campaign.overview.side.description.shipProducts', { defaultValue: 'Send products to all the influencers' })}
							/>
						)}
						{campaign.assignments.edges.some(({ node }) => node.hasContentReview) && (
							<Step
								title={i18n.t('organization:campaign.overview.side.title.reviewPosts', { defaultValue: 'Review posts' })}
								description={i18n.t('organization:campaign.overview.side.description.reviewPosts', { defaultValue: 'Review the influencers suggested posts' })}
							/>
						)}
						<Step
							title={i18n.t('organization:campaign.overview.side.title.postOnInstagram', { defaultValue: 'Post on Instagram' })}
							description={i18n.t('organization:campaign.overview.side.description.postOnInstagram', {
								defaultValue: 'The influencers are ready to start posting'
							})}
						/>
						<Step
							title={i18n.t('organization:campaign.overview.side.title.postsCompleted', { defaultValue: 'Posts completed' })}
							description={i18n.t('organization:campaign.overview.side.description.postsCompleted', { defaultValue: 'Posts are verified and completed' })}
						/>
					</Steps>
				</Col>
				<Col xs={{ span: 24 }} sm={{ span: 15, offset: 1 }} className='my-10 timeline-tasks'>
					{taskGroups.map((group) => (
						<div className='border-bottom mb-30 pb-30 has-visited' key={group.key}>
							<div className='headline'>
								<h3>{group.title}</h3>
								<p>{group.description}</p>
							</div>
							{group.tasks.map((task) => withFlag(task.rule)(renderLinkTo(task, task[task.rule]), renderLinkTo(task), { key: task.title }))}
						</div>
					))}
				</Col>

				<Col xs={{ span: 24 }} sm={{ span: 8 }} className='my-10' />

				<Col xs={{ span: 24 }} sm={{ span: 8 }} className='my-10' />
			</Row>
		</React.Fragment>
	);
};

const OverviewWrapper = ({ intl, flags, ...props }) => {
	return (
		<CampaignLayout>
			{({ campaign }) => {
				const { edges } = campaign.campaignReviews;
				const campaignReviewLastNode = (edges || []).length && edges[edges.length - 1];
				const { status, message, reasons } = (campaignReviewLastNode || {}).node || {};
				const reason = Object.entries(reasons || {})
					.filter(([key, value]) => key !== '__typename' && value)
					.map((item) => item[1])
					.join(', ');
				const itemId = `campaign_${campaign.id}_${status}_alert`;
				const isClosedAlert = localStorage.getItem(itemId);
				var isAlertOnTime = true;
				if (campaignReviewLastNode) {
					const createdAt = moment.utc(campaignReviewLastNode.node.createdAt, 'YYYY-MM-DD HH:mm:ss Z');
					isAlertOnTime = createdAt.add(1, 'day') > moment.utc();
				}
				const noJoinedSpots = campaign.instagramOwnersJoinedCount === 0;

				return (
					<React.Fragment>
						{status && !isClosedAlert && noJoinedSpots && isAlertOnTime && (
							<NotificationPanel type={status} reason={reason} message={message} itemId={itemId} />
						)}
						<Overview campaign={campaign} intl={intl} flags={flags} />
					</React.Fragment>
				);
			}}
		</CampaignLayout>
	);
};

export default injectIntl(translate('organization')(OverviewWrapper));
