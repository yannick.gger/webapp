import React from 'react';
import { Row, Col } from 'antd';

import NoOver from './no-overview.svg';

const Overview = () => {
	return (
		<Row>
			<Col sm={{ span: 8, offset: 8 }} className='text-center'>
				<div className='empty-icon'>
					<img src={NoOver} alt='' className='svg' />
				</div>
				<div className='mt-20'>
					<h2>Overview</h2>
					<p>Useful metrics will start showing up here once your campaign is started.</p>
				</div>
			</Col>
		</Row>
	);
};

export default Overview;
