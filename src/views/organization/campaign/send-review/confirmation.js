import { Button, Card, Col, Divider, Form, Row } from 'antd';
import { OrganizationLink } from 'components';
import React from 'react';
import GreatReview from './great-review.png';
import './styles.scss';

class Confirmation extends React.Component {
	render() {
		const { campaign } = this.props;
		return (
			<React.Fragment>
				<Card cover={<img className='image-cover' alt='example' src={GreatReview} />}>
					<div className='headline text-center'>
						<h1 className='color-blue mt-0 title-review-processed'>Your campaign is being processed</h1>
						<p>After your campaign has been approved all the influencers will be invited</p>
					</div>
					<Divider />
					<Row gutter={30}>
						<Col xs={{ span: 24 }}>
							<OrganizationLink to={`/campaigns/${campaign.id}`}>
								<Button key='submit' style={{ width: '100%' }} type='primary'>
									Go to campaign
								</Button>
							</OrganizationLink>
						</Col>
					</Row>
				</Card>
			</React.Fragment>
		);
	}
}

const WrappedConfirmation = Form.create({})(Confirmation);

export default WrappedConfirmation;
