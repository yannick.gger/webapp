import React, { Component } from 'react';
import { Steps, Icon } from 'antd';

export default class CampaignSteps extends Component {
	render() {
		const { campaign, title } = this.props;
		return (
			<Steps className='ant-steps-top' current={this.props.step} style={this.props.style}>
				<Steps.Step title='Confirm details' icon={<Icon type='file-text' theme='outlined' />} />
				<Steps.Step title={title} icon={<Icon type='smile-o' />} />
			</Steps>
		);
	}
}
