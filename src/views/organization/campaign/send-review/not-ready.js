import React from 'react';
import { Card } from 'antd';
import { Link } from 'react-router-dom';
import withFlag from 'hocs/with-flag';
import TaskDone from 'assets/img/app/task-done.svg';

const NotReady = ({ taskGroups }) => {
	const renderLinkTo = (task, link) => () => (
		<Link disabled={task.disabled} to={link || task.link} key={task.title}>
			<Card
				hoverable={!task.completed}
				data-ripple='rgba(132,146, 164, 0.2)'
				className={`action-card has-icon-left${task.disabled ? ' disabled' : ''} mb-20`}
				cover={<img src={task.completed ? TaskDone : task.illustration} alt={task.title} />}
			>
				<Card.Meta title={task.title} />
				<div slyle={{ float: 'right' }} />
				<p className='mt-5 mb-4'>{task.description}</p>
			</Card>
		</Link>
	);

	return (
		<div>
			<div style={{ marginTop: '-15px', marginBottom: '30px', textAlign: 'center' }}>
				<p>You have unfinished tasks to complete before you can invite influencers</p>
			</div>

			{taskGroups[0].tasks.map((task) => {
				if (task.completed) {
					return null;
				}

				return withFlag(task.rule)(renderLinkTo(task, task[task.rule]), renderLinkTo(task), { key: task.title });
			})}
		</div>
	);
};

export default NotReady;
