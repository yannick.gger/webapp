import React from 'react';
import { Row, Col, Card, Divider, Alert, Avatar, Form, Input, Icon, Tooltip } from 'antd';
import { FormattedNumber, FormattedPlural } from 'react-intl';

import { ShortAmountFormatter, FormattedCurrencyNumber } from 'components/formatters';
import AssignmentListWithQuery from '../assignments/list-with-query';
import Products from '../products/products';
import PaymentsCardOrInvoice from '../products/get-payment-card';
import SlamDunk from 'assets/img/app/slamdunk.svg';
import InviteButton from './invite-button';
import './styles.scss';
import { CheckedIcon, ClosedIcon } from 'components/icons';

const { TextArea } = Input;
const FormItem = Form.Item;

class Start extends React.Component {
	render() {
		const { campaign, organization, goToStep, form } = this.props;
		const { getFieldDecorator } = form;
		const formItemLayout = { labelCol: { span: 24 }, wrapperCol: { span: 24 } };

		return (
			<React.Fragment>
				<div className='description'>
					<p>{`You are about to send the campaign for review. Once the campaign is approved, an invite will be sent to all influencers to join your campaign. There is a couple of things you should review to make sure everything is in order.`}</p>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>Campaign brief</h3>
					<Card className='small-title mb-10 mt-10'>
						<p className='multi-line'>{campaign.brief}</p>
					</Card>
				</div>
				<div className='part mb-0'>
					<h3 className='color-blue'>What the influencers will agree to publish</h3>
					<AssignmentListWithQuery campaign={campaign} showCreate={false} mdSpan={24} />
				</div>
				<div className='part mt-0'>
					<h3 className='color-blue mb-20'>@mentions and #tags for the influencers to use when posting</h3>
					<Row gutter={20}>
						<Col sm={{ span: 12 }}>
							<Card>
								<div className='mentions'>
									<h3>Mentions</h3>
									<p className='color-dark'>{campaign.mentions.join(' ')}</p>
								</div>
							</Card>
						</Col>

						<Col sm={{ span: 12 }}>
							<Card>
								<div className='tags'>
									<h3>Tags</h3>
									<p className='color-dark'>{campaign.tags.join(' ')}</p>
								</div>
							</Card>
						</Col>
					</Row>
				</div>
				<div className='part calculus-rows'>
					<h3 className='color-blue mb-0'>Commission</h3>
					<Products campaign={campaign} showCreate={false} mdSpan={24} />
					<div className='payment-card-invoice'>
						<PaymentsCardOrInvoice campaign={campaign} mdSpan={24} />
					</div>
				</div>

				<div className='part'>
					<h3 className='color-blue mb-0'>Who will get the invitation?</h3>
					<Card className='small-title mb-10 mt-10'>
						<div className='avatar-list'>
							{campaign.instagramOwners.edges.slice(0, campaign.instagramOwnersInvitedCount > 11 ? 12 : 11).map(({ node }) => (
								<Tooltip placement='topLeft' key={node.instagramOwnerId} title={`@${node.instagramOwnerId}`}>
									<Avatar size='large' src='#' icon='user' />
								</Tooltip>
							))}

							{campaign.instagramOwnersInvitedCount > 11 && (
								<Avatar size='large' key='more'>
									+{campaign.instagramOwnersInvitedCount - 12}
								</Avatar>
							)}
						</div>
						<Divider />
						<Row>
							<Col sm={{ span: 12 }} md={{ span: 6 }}>
								<h4>Influencers total</h4>
								<h2>
									<FormattedNumber value={campaign.instagramOwnersInvitedCount} />{' '}
								</h2>
							</Col>
							<Col sm={{ span: 12 }} md={{ span: 6 }}>
								<h4>
									Est. Followers{' '}
									<Tooltip title='Number of spots * Average number of followers per invited influencer'>
										<Icon type='question-circle' theme='outlined' />
									</Tooltip>
								</h4>

								<h2>
									<ShortAmountFormatter value={campaign.estimatedReach} maximumFractionDigits={0} />
								</h2>
							</Col>
							<Col sm={{ span: 12 }} md={{ span: 6 }}>
								<h4>
									Est. Engagagement{' '}
									<Tooltip title='Average number of interactions per influencer post'>
										<Icon type='question-circle' theme='outlined' />
									</Tooltip>
								</h4>

								<h2>
									<FormattedNumber value={campaign.estimatedInteractionRate * 100} maximumFractionDigits={2} />%
								</h2>
							</Col>
							<Col sm={{ span: 12 }} md={{ span: 6 }}>
								<h4>
									Est. CPM{' '}
									<Tooltip title='Amount of influencers * Total cost for the campaign / Estimated reach * 1000'>
										<Icon type='question-circle' theme='outlined' />
									</Tooltip>
								</h4>

								<h2>
									<FormattedNumber
										maximumFractionDigits={2}
										value={((campaign.influencerTargetCount * campaign.compensationsTotalCost) / campaign.estimatedReach) * 1000}
									/>{' '}
									{campaign.currency}
								</h2>
							</Col>
						</Row>
					</Card>
				</div>
				<div className='part calculus-rows'>
					<h3 className='color-blue mb-0'>Summary</h3>
					<Card className='small-title mb-10 mt-10'>
						<h4>Invites</h4>
						<Row>
							<Col sm={{ span: 9 }}>
								<h3>You will invite</h3>
							</Col>
							<Col sm={{ span: 15 }}>
								<p>
									<FormattedNumber value={campaign.instagramOwnersInvitedCount} />{' '}
									<FormattedPlural value={campaign.instagramOwnersInvitedCount} one='influencer' other='influencers' />
								</p>
							</Col>
						</Row>
						<Row>
							<Col sm={{ span: 9 }}>
								<h3>Spots available</h3>
							</Col>
							<Col sm={{ span: 15 }}>
								<p>
									<FormattedNumber value={campaign.influencerTargetCount} /> <FormattedPlural value={campaign.influencerTargetCount} one='spot' other='spots' />
								</p>
							</Col>
						</Row>
						<Row>
							<Col sm={{ span: 9 }}>
								<h3>Rules to join</h3>
							</Col>
							<Col sm={{ span: 15 }}>
								<p>First-come first-served basis</p>
							</Col>
						</Row>
						<Row>
							<Col sm={{ span: 9 }}>
								<h3>Invite setting</h3>
							</Col>
							<Col sm={{ span: 15 }}>
								<p>
									<FormattedNumber value={campaign.instagramOwnersInvitedCount} />{' '}
									<FormattedPlural value={campaign.instagramOwnersInvitedCount} one='influencer' other='influencers' /> will get an email with an invitation to
									your campaign page where they can join the campaign.
								</p>
							</Col>
						</Row>
						<Divider />
						<h4>Assignments</h4>
						<Row>
							<Col sm={{ span: 9 }}>
								<h3>Each influencer will publish</h3>
							</Col>
							<Col sm={{ span: 15 }}>
								<p>
									<FormattedNumber value={campaign.assignmentsCount} /> <FormattedPlural value={campaign.assignmentsCount} one='post' other='posts' />
								</p>
							</Col>
						</Row>
						<Divider />
						{campaign.paymentCompensationsTotalValue > 0 || campaign.invoiceCompensationsTotalValue > 0 ? (
							<React.Fragment>
								<h4>{campaign.paymentCompensationsTotalValue > 0 ? 'Payment with card' : 'Payment by invoice'}</h4>
								<Row>
									<Col sm={{ span: 9 }}>
										<h3>{campaign.paymentCompensationsTotalValue > 0 ? 'Each influencer will get paid' : 'Each influencer will be invoice'}</h3>
									</Col>
									<Col sm={{ span: 15 }}>
										<p>
											<FormattedNumber value={campaign.paymentCompensationsTotalValue || campaign.invoiceCompensationsTotalValue} /> {campaign.currency}
										</p>
									</Col>
								</Row>
								<Row>
									<Col sm={{ span: 9 }}>
										<h3>Number of spots</h3>
									</Col>
									<Col sm={{ span: 15 }}>
										<p>
											<FormattedNumber value={campaign.influencerTargetCount} />
										</p>
									</Col>
								</Row>
								<Row>
									<Col sm={{ span: 9 }}>
										<h3 className='color-blue'>Total payment in {campaign.currency}</h3>
									</Col>
									<Col sm={{ span: 15 }}>
										<p>
											<strong className='color-blue'>
												<FormattedNumber
													value={campaign.influencerTargetCount * (campaign.paymentCompensationsTotalValue || campaign.invoiceCompensationsTotalValue)}
												/>{' '}
												{campaign.currency}
											</strong>
										</p>
									</Col>
								</Row>
								{campaign.paymentCompensationsTotalValue > 0 && (
									<div>
										<Alert className='mt-15' message='You will be paying with card in the next step' type='info' showIcon />
										<a href='https://collabs.app/payments.html#payment-model' target='_blank' rel='noopener noreferrer'>
											<Alert className='mt-15' message='If you are unsure about how payments work, click here to read more' type='info' showIcon />
										</a>
										<Divider />
									</div>
								)}
								{campaign.invoiceCompensationsTotalValue > 0 && (
									<div>
										<Alert className='mt-15' message='You will be paying by invoice once the campaign is done' type='info' showIcon />
										<Divider />
									</div>
								)}
							</React.Fragment>
						) : (
							<React.Fragment>
								<Row>
									<Col sm={{ span: 12 }}>
										<h3>No payment compensation for campaign</h3>
									</Col>
									<Col sm={{ span: 12 }} />
								</Row>
								<Divider />
							</React.Fragment>
						)}

						<h4>Commission with merchandise</h4>
						{campaign.productCompensationsTotalValue > 0 ? (
							<React.Fragment>
								<Row>
									<Col sm={{ span: 9 }}>
										<h3>Each influencer will receive</h3>
									</Col>
									<Col sm={{ span: 15 }}>
										<p>
											<FormattedNumber value={campaign.productCompensationsCount} />{' '}
											<FormattedPlural value={campaign.productCompensationsCount} one='product' other='products' /> with a total value of {campaign.currency}{' '}
											<FormattedNumber value={campaign.productCompensationsTotalValue} />
										</p>
									</Col>
								</Row>
								<Row>
									<Col sm={{ span: 9 }}>
										<h3>Merchandise total (actual cost)</h3>
										<em className='fl'>What it actually costs you</em>
									</Col>
									<Col sm={{ span: 15 }}>
										<p>
											<FormattedNumber value={campaign.influencerTargetCount} /> x <FormattedNumber value={campaign.productCompensationsCount} /> ={' '}
											<FormattedNumber value={campaign.influencerTargetCount * campaign.productCompensationsTotalCost} /> {campaign.currency}
										</p>
									</Col>
								</Row>
								<Row>
									<Col sm={{ span: 9 }}>
										<h3>Merchandise total (value)</h3>
										<em className='fl'>What the influencer see</em>
									</Col>
									<Col sm={{ span: 15 }}>
										<p>
											<FormattedNumber value={campaign.influencerTargetCount} /> x <FormattedNumber value={campaign.productCompensationsTotalValue} /> ={' '}
											<FormattedNumber value={campaign.influencerTargetCount * campaign.productCompensationsTotalValue} /> {campaign.currency}
										</p>
									</Col>
								</Row>
							</React.Fragment>
						) : (
							<React.Fragment>
								<Row>
									<Col sm={{ span: 12 }}>
										<h3>No product compensation for campaign</h3>
									</Col>
									<Col sm={{ span: 12 }} />
								</Row>
							</React.Fragment>
						)}
						<Divider />
						<h4>Value calculations</h4>
						<Row>
							<Col sm={{ span: 9 }}>
								<h3>Est. cost per thousand impressions</h3>
							</Col>
							<Col sm={{ span: 15 }}>
								<p>
									{campaign.currency}{' '}
									<FormattedNumber value={((campaign.influencerTargetCount * campaign.compensationsTotalCost) / campaign.estimatedReach) * 1000} />
								</p>
							</Col>
						</Row>
						<Row>
							<Col sm={{ span: 9 }}>
								<h3>Est. cost per engagagement</h3>
							</Col>
							<Col sm={{ span: 15 }}>
								<p>
									{campaign.currency}{' '}
									<FormattedNumber
										maximumFractionDigits={2}
										value={(campaign.influencerTargetCount * campaign.compensationsTotalCost * campaign.estimatedInteractionRate) / 100}
									/>
								</p>
							</Col>
						</Row>
					</Card>
					<Card className='blue-box'>
						<Row>
							<Col sm={{ span: 16 }}>
								<h2 className='mb-0'>Total campaign investment</h2>
							</Col>
							<Col sm={{ span: 8 }}>
								<h2 className='fr mb-0'>
									<FormattedCurrencyNumber
										value={campaign.influencerTargetCount * campaign.compensationsTotalCost}
										currency={campaign.currency}
										currencyDisplay='code'
									/>
								</h2>
							</Col>
						</Row>
					</Card>
					<div className='border-bottom border-top' id='happen-container'>
						<Row gutter={30} className='mb-20 row-content'>
							<Col xs={{ span: 24 }} md={{ span: 12 }}>
								<span className='title'>
									{[
										'If the campaign is ',
										<b key='approved' className='approved'>
											approved
										</b>,
										' this happens:'
									]}
								</span>
								{!!campaign.paymentCompensationsCount && (
									<Card className='card-item-left'>
										<Row>
											<Col xs={{ span: 2 }} md={{ span: 1 }}>
												<CheckedIcon height={12} width={15.68} fill='#026BFA' />
											</Col>
											<Col xs={{ span: 22 }} md={{ span: 23 }} className='col-text-card-item'>
												<span className='text-card-item'>
													When sending for review the money is reserved on your account. If approved you will be charged. If declined the money will be sent
													back to you.
												</span>
											</Col>
										</Row>
									</Card>
								)}
								<Card className='card-item-left'>
									<Row>
										<Col xs={{ span: 2 }} md={{ span: 1 }}>
											<CheckedIcon height={12} width={15.68} fill='#026BFA' />
										</Col>
										<Col xs={{ span: 22 }} md={{ span: 23 }} className='col-text-card-item'>
											<span className='text-card-item'>All influencers will get an invite</span>
										</Col>
									</Row>
								</Card>
								<Card className='card-item-left'>
									<Row>
										<Col xs={{ span: 2 }} md={{ span: 1 }}>
											<CheckedIcon height={12} width={15.68} fill='#026BFA' />
										</Col>
										<Col xs={{ span: 22 }} md={{ span: 23 }} className='col-text-card-item'>
											<span className='text-card-item'>You will be notified by email</span>
										</Col>
									</Row>
								</Card>
							</Col>
							<Col xs={{ span: 24 }} md={{ span: 12 }}>
								<span className='title'>
									{[
										'If the campaign is ',
										<b key='rejected' className='rejected'>
											declined
										</b>,
										' this happens:'
									]}
								</span>
								<Card className='card-item-right'>
									<Row>
										<Col xs={{ span: 2 }} md={{ span: 1 }}>
											<ClosedIcon fill='#f1365e' />
										</Col>
										<Col xs={{ span: 22 }} md={{ span: 23 }} className='col-text-card-item'>
											<span className='text-card-item'>You will be notified by email, then you can fix the reasons and send in for review again.</span>
										</Col>
									</Row>
								</Card>
							</Col>
						</Row>
					</div>
				</div>
				{false && (
					<div className='part'>
						<h3 className='color-blue mb-0'>Invitation email</h3>
						<Card className='small-title mb-10 mt-10'>
							<Form className=''>
								<FormItem {...formItemLayout} label='Subject'>
									{getFieldDecorator('subject', { rules: [{ required: true, whitespace: true, message: 'Please choose a subject' }] })(
										<Input placeholder='Basic usage' />
									)}
								</FormItem>
								<FormItem {...formItemLayout} label='Message'>
									{getFieldDecorator('subject', {
										rules: [{ required: true, whitespace: true, message: 'Please choose a subject' }]
									})(<TextArea placeholder='Autosize height with minimum and maximum number of lines' autosize={{ minRows: 8, maxRows: 12 }} />)}
								</FormItem>
							</Form>
						</Card>
					</div>
				)}
				<div className='stripes'>
					<span />
				</div>
				<div className='part text-center slam-dunk slam-dunk-review'>
					<h2 className='thin mb-30 slam-dunk-text'>
						{"It's time to "}
						<strong>
							<em>slam dunk</em>
						</strong>
					</h2>
					<img src={SlamDunk} alt='' className='svg' style={{ marginRight: '-40px' }} />
				</div>
				<InviteButton client={this.props.client} campaign={campaign} organization={organization} goToStep={goToStep} />
			</React.Fragment>
		);
	}
}

const WrappedStart = Form.create({})(Start);

export default WrappedStart;
