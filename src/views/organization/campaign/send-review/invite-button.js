import React, { Component } from 'react';
import { Button, Icon, message } from 'antd';
import { OrganizationLink } from 'components';
import gql from 'graphql-tag';
import sendCampaignForReviewQuery from 'graphql/send-campaign-review.graphql';
import CampaignReviewFragment from 'graphql/fragments/campaignReviewFragment.graphql';
import CampaignReviewReasonFragment from 'graphql/fragments/campaignReviewReasonFragment.graphql';

const getOrganizationCampaignQuery = gql`
	query getOrganizationCampaign($id: ID!) {
		campaign(id: $id) {
			id
			canBeSentForReview
			newInfluencerExists
			campaignReviews {
				edges {
					node {
						...CampaignReviewFragment
						reasons {
							...CampaignReviewReasonFragment
						}
					}
				}
			}
		}
	}

	${CampaignReviewFragment}
	${CampaignReviewReasonFragment}
`;

export default class InviteButton extends Component {
	state = {
		loadingSendReview: false,
		step: 0
	};
	sendCampaignForReview = (client, campaign) => {
		this.setState({ loadingSendReview: true });
		client
			.mutate({
				mutation: sendCampaignForReviewQuery,
				variables: { campaignId: campaign.id },
				refetchQueries: [{ query: getOrganizationCampaignQuery, variables: { id: campaign.id } }]
			})
			.then(({ data, loading, error }) => {
				if (error) {
					this.setState({ loadingSendReview: false });
					message.error('Something happened. Could not send campaign review');
				}
				if (!loading && data) {
					this.setState({ loadingSendReview: false });
					message.success('Review for campaign sent');
					this.props.goToStep(1);
				}
			})
			.catch(({ graphQLErrors }) => {
				if (graphQLErrors && graphQLErrors[0]) {
					message.error(graphQLErrors[0].message);
				}
				this.setState({ loadingSendReview: false });
			});
	};
	proceedToPayment = () => {
		this.props.goToStep(1);
	};
	render() {
		const { client, campaign, organization } = this.props;
		if (organization.isSubscribed) {
			return (
				<div style={{ textAlign: 'center', marginTop: '10px' }}>
					<Button
						size='large'
						key='ok'
						type='primary'
						className='send-cta'
						loading={this.state.loadingSendReview}
						data-ripple
						onClick={() => {
							campaign.paymentCompensationsCount > 0 ? this.proceedToPayment() : this.sendCampaignForReview(client, campaign);
						}}
					>
						Alright, send the campaign for review <Icon type='rocket' />
					</Button>
					<div className='text mt-15'>
						<p style={{ fontSize: '13px' }}>
							When you confirm the campaign you{' '}
							<a href='https://collabs.app/tos.html#advertisers' target='_blank' rel='noopener noreferrer'>
								agree to the terms
							</a>
						</p>
					</div>
				</div>
			);
		} else if (campaign.invitesSent) {
			return (
				<Button size='large' key='ok' type='primary' className='send-cta' loading={this.state.loadingSendReview} data-ripple disabled>
					Invites have already been sent <Icon type='rocket' />
				</Button>
			);
		} else if (!organization.isSubscribed) {
			return (
				<React.Fragment>
					<Button type='primary' disabled>
						Upgrade your account to be able to send campaign review.
					</Button>
					<OrganizationLink to={`/settings/billing/select-plan`}>
						<Button type='primary' size='large'>
							Upgrade now
						</Button>
					</OrganizationLink>
				</React.Fragment>
			);
		}
	}
}
