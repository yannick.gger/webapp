import React from 'react';
import { ApolloConsumer, Query } from 'react-apollo';
import gql from 'graphql-tag';
import { Modal, Spin } from 'antd';
import { withOrganization } from 'hocs';
import { injectIntl } from 'react-intl';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import { getCurrentTaskGroupId, getTaskGroups } from 'services/organization-campaign-tasks';
import CampaignSteps from './campaign-steps';
import Confirmation from './confirmation';
import NotReady from './not-ready';
import Start from './start';

import CampaignCoverPhotoFragment from 'graphql/fragments/campaignCoverPhotoFragment.graphql';
import CampaignFragment from 'graphql/fragments/campaignFragment.graphql';
import AssignmentFragment from 'graphql/fragments/assignmentFragment.graphql';

const getOrganizationCampaignQuery = gql`
	query getOrganizationCampaign($id: ID!, $withCampaignListFields: Boolean = true, $skipCampaignListIrreleventFields: Boolean = false) {
		campaign(id: $id) {
			...CampaignFragment

			todoInfluencersUploadContent {
				currentState
				remainingInfluencers
			}

			todoReviewContent {
				currentState
				remainingInfluencers
			}

			todoInfluencersPostInstagram {
				currentState
				remainingInfluencers
			}

			assignments {
				edges {
					node {
						...AssignmentFragment
						todoInfluencersDoAssignment {
							currentState
							remainingInfluencers
						}
					}
				}
			}

			campaignCoverPhoto {
				...CampaignCoverPhotoFragment
			}

			instagramOwners {
				edges {
					node {
						instagramOwnerId
					}
				}
			}
		}
	}

	${CampaignCoverPhotoFragment}
	${CampaignFragment}
	${AssignmentFragment}
`;

class SendInvites extends React.Component {
	state = { step: 0 };

	handleCancel = () => this.props.closeModal();

	goToStep = (step) => this.setState({ step });

	render() {
		const { organization } = this.props;

		return (
			<ApolloConsumer>
				{(client) => (
					<Query query={getOrganizationCampaignQuery} variables={{ id: this.props.match.params.campaignId }}>
						{({ loading, error, data }) => {
							if (loading) return <Spin className='collabspin' />;
							if (error) return <p>{i18next.t('organization:campaign.sendReview.error.noCampaign', { defaultValue: 'Could not load campaign' })}</p>;

							const taskGroups = getTaskGroups({
								campaign: data.campaign,
								organizationSlug: this.props.match.params.organizationSlug,
								intl: this.props.intl
							});
							const doneTaskGroups = taskGroups.filter(
								(taskGroup) => taskGroup.key === 'pre_invitation' && (taskGroup.tasks || []).every((task) => task.completed)
							);
							const currentTaskGroupId = getCurrentTaskGroupId({ taskGroups: doneTaskGroups });

							const modalChild = [];
							const modalOptions = {
								visible: true,
								footer: [],
								maskClosable: false,
								onCancel: this.handleCancel,
								title: i18next.t('organization:campaign.sendReview.modal.title.campaignNotReady', { defaultValue: 'Your campaign is not ready yet' }),
								wrapClassName: 'custom-modal box-parts ant-invite-modal title-center'
							};

							if (currentTaskGroupId >= 0) {
								const option = {
									step: 0,
									component: (
										<Start key='Start' client={client} organization={organization} campaign={data.campaign} taskGroups={taskGroups} goToStep={this.goToStep} />
									)
								};

								if (this.state.step === 2 || (this.state.step === 1 && data.campaign.paymentCompensationsCount === 0)) {
									option.step = 2;
									option.component = <Confirmation key='Confirmation' goToStep={this.goToStep} campaign={data.campaign} />;
								}

								modalOptions.title = '';
								if (option.step === 2) {
									modalOptions.className = 'mt-30';
								} else {
									modalOptions.title = i18next.t('organization:campaign.sendReview.modal.title.sendForReview', { defaultValue: 'Send campaign for review' });
								}

								modalChild[0] = (
									<CampaignSteps
										key='CampaignSteps'
										campaign={data.campaign}
										step={this.state.step}
										title={i18next.t('organization:campaign.sendReview.modal.title.sentForReview', { defaultValue: 'Campaign is sent for review' })}
									/>
								);
								modalChild[1] = option.component;
							} else {
								modalChild[0] = <NotReady key='NotReady' campaign={data.campaign} taskGroups={taskGroups} />;
							}

							return <Modal {...modalOptions}>{modalChild}</Modal>;
						}}
					</Query>
				)}
			</ApolloConsumer>
		);
	}
}

export default injectIntl(withOrganization(translate('organization')(SendInvites)));
