import React from 'react';
import { Row, Col } from 'antd';
import { ReactSVG } from 'react-svg';
import NoDash from 'assets/img/app/no-content-review-page.svg';

export default () => (
	<Row>
		<Col sm={{ span: 10, offset: 7 }} className='text-center'>
			<div className='empty-icon'>
				<ReactSVG src={NoDash} />
			</div>
			<div className='mb-30'>
				<h2>Nothing uploaded for review</h2>
				<p>No influencer has uploaded anything for review</p>
			</div>
		</Col>
	</Row>
);
