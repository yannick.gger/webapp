import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Row, Col, Radio } from 'antd';

import ReviewCard from './review-card/';
import EmptyState from './empty-state';

class AssignmentReviews extends Component {
	state = {
		initialLoading: false,
		moreLoading: false
	};
	loadMore = (data, fetchMore) => {
		this.setState({ moreLoading: true });
		fetchMore({
			variables: {
				cursor: data.campaign.assignmentReviews.pageInfo.endCursor
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				this.setState({ moreLoading: false });

				const newEdges = fetchMoreResult.campaign.assignmentReviews.edges;
				const pageInfo = fetchMoreResult.campaign.assignmentReviews.pageInfo;

				return newEdges.length
					? {
							campaign: {
								...previousResult.campaign,
								assignmentReviews: {
									__typename: previousResult.campaign.assignmentReviews.__typename,
									edges: [...previousResult.campaign.assignmentReviews.edges, ...newEdges],
									pageInfo
								}
							}
					  }
					: previousResult;
			}
		});
	};
	componentDidUpdate = (prevProps) => {
		if (prevProps.loading !== this.props.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.loading });
		}
	};
	componentDidMount = () => {
		if (this.props.loading) {
			this.setState({ initialLoading: true });
		}
	};
	render() {
		const { error, data, fetchMore, handleUpdateQuery, query } = this.props;
		const mdSpan = this.props.mdSpan || 8;
		const { initialLoading, moreLoading } = this.state;

		return (
			<React.Fragment>
				<div className='mb-30 mt-30 border-top pt-30'>
					<Row type='flex' justify='end' align='middle'>
						<Col md={{ span: 8 }} />
						<Col md={{ span: 8 }} />
						<Col>
							<span className='mr-20'>Filter by:</span>
							<Radio.Group
								defaultValue='to_review'
								className='radio-pills'
								onChange={(e) => {
									handleUpdateQuery({ status: e.target.value });
								}}
								value={query.status}
							>
								<Radio.Button className='rippledown' value='to_review'>
									To review
								</Radio.Button>
								<Radio.Button className='rippledown' value='approved'>
									Approved
								</Radio.Button>
								<Radio.Button className='rippledown' value='rejected'>
									Declined
								</Radio.Button>
							</Radio.Group>
						</Col>
					</Row>
				</div>
				<Row gutter={30} className='item-rows'>
					{data &&
						data.campaign &&
						data.campaign.assignmentReviews.edges.map(({ node }) => (
							<Col className='d-flex' xs={{ span: 24 }} md={{ span: mdSpan }} key={node.id}>
								<ReviewCard assignmentReview={node} />
							</Col>
						))}
				</Row>
				{data.campaign.assignmentReviews.edges.length === 0 && <EmptyState />}
				{!error &&
					!initialLoading &&
					data &&
					data.campaign &&
					data.campaign.assignmentReviews.edges.length !== 0 &&
					data.campaign.assignmentReviews.edges.length % 20 === 0 && (
						<div className='pt-30 pb-30 text-center'>
							<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(data, fetchMore)}>
								Load more
							</Button>
						</div>
					)}
			</React.Fragment>
		);
	}
}

export default withRouter(AssignmentReviews);
