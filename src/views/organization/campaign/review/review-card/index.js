import React, { Component } from 'react';
import { Row, Col, Tag, Divider, Button, Card, Spin, message, Modal, Form, Input, Radio } from 'antd';
import { ApolloConsumer } from 'react-apollo';

import MediaPlayer from 'components/media-player';
import updateAssignmentReviewMutation from 'graphql/update-assignment-review.graphql';
import getAssignmentReviewsQuery from 'graphql/get-assignment-reviews.graphql';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';

class AssignmentReviewForm extends Component {
	render() {
		const { form, passedDeadline, showRejectionModal } = this.props;
		const { getFieldDecorator } = form;

		return (
			<Form onSubmit={this.handleSubmit}>
				<Form.Item label={showRejectionModal ? 'Rejection reason' : 'Approval comment'}>
					{getFieldDecorator(showRejectionModal ? 'rejectionComment' : 'approvalComment', {
						rules: [{ required: true, whitespace: false, message: showRejectionModal ? 'Enter a rejection comment' : 'Enter a approval comment' }]
					})(
						<Input.TextArea
							size='large'
							rows='6'
							placeholder={
								showRejectionModal
									? 'We have declined your content because of the following reasons\n* It contains nudity\n* It uses the wrong filter'
									: 'Enter a approval comment'
							}
						/>
					)}
				</Form.Item>
				{passedDeadline && showRejectionModal && (
					<Form.Item label='Extend deadline?'>
						{getFieldDecorator('waivedDeadline', {
							rules: [{ required: true, message: 'Choose an option' }]
						})(
							<Radio.Group>
								<Radio value={true}>Yes, allow a new upload</Radio>
								<Radio value={false}>No, reject profile from campaign</Radio>
							</Radio.Group>
						)}
					</Form.Item>
				)}
			</Form>
		);
	}
}

const WrappedAssignmentReviewForm = Form.create()(AssignmentReviewForm);

class ReviewCard extends Component {
	state = {
		loading: false,
		showModal: false,
		showRejectionModal: false
	};
	approveAssignmentReview = (client, assignmentReview) => {
		return (e) => {
			this.setState({ loading: true });
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, fields) => {
				if (err) return null;
				client
					.mutate({
						mutation: updateAssignmentReviewMutation,
						variables: { id: assignmentReview.id, status: 'approved', ...fields },
						refetchQueries: [{ query: getAssignmentReviewsQuery, variables: { id: assignmentReview.campaign.id } }]
					})
					.then(({ data }) => {
						this.setState({ loading: false, showModal: false, showRejectionModal: false });
					})
					.catch(({ error }) => {
						message.error('Something went wrong approving assignment');
						throw error;
					});
			});
		};
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	rejectAssignmentReview = (client, assignmentReview) => {
		return (e) => {
			this.setState({ loading: true });
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, fields) => {
				if (err) return null;

				client
					.mutate({
						mutation: updateAssignmentReviewMutation,
						variables: { id: assignmentReview.id, status: 'rejected', ...fields },
						refetchQueries: [{ query: getAssignmentReviewsQuery, variables: { id: assignmentReview.campaign.id } }]
					})
					.then(({ data }) => {
						this.setState({ loading: false, showModal: false, showRejectionModal: false });
					})
					.catch(({ error }) => {
						message.error('Something went wrong approving assignment');
						throw error;
					});
			});
		};
	};

	renderMediaPreview = (assignmentReview) => {
		return (
			<React.Fragment>
				{assignmentReview.assignmentReviewMedia && <MediaPlayer media={assignmentReview.assignmentReviewMedia} />}

				{assignmentReview.reviewedAt && (
					<Tag
						className='card-tag mt-0 pa-tl'
						style={{ background: assignmentReview.status === 'approved' ? 'green' : 'red', color: 'white', width: 'auto', opacity: 0.7 }}
					>
						{assignmentReview.status === 'approved' ? 'Approved' : 'Declined'}
					</Tag>
				)}
			</React.Fragment>
		);
	};

	render() {
		const { assignmentReview } = this.props;
		const endTime = new Date(assignmentReview.assignment.endTime);
		const passedDeadline = endTime.valueOf() - new Date().valueOf() < 24 * 60 * 60 * 1000; // 24 hours
		const showRejectionModal = this.state.showRejectionModal;

		// Temporary: For issue tracking not show the options in Rejection Modal form
		console.log('endTime: ', endTime, 'nowTime: ', new Date());

		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<Card className='campaign-card' cover={this.renderMediaPreview(assignmentReview)}>
							{assignmentReview.text && <Card.Meta className='multi-line' description={assignmentReview.text} />}
							<Row className='mt-20'>
								<Col>
									<Avatar size='large' src='#' icon='user' className='fl mr-10' />
								</Col>
								<Col>
									<Row>For: {assignmentReview.assignment.name}</Row>
								</Col>
							</Row>
							<Divider />
							{!assignmentReview.reviewedAt && !this.state.loading && (
								<React.Fragment>
									<Button style={{ width: '47%', marginRight: '3%' }} type='success' onClick={() => this.setState({ showModal: true })}>
										Approve
									</Button>
									<Button
										style={{ width: '47%', marginLeft: '3%' }}
										type='danger '
										onClick={() => this.setState({ showModal: true, showRejectionModal: true })}
									>
										Reject
									</Button>
								</React.Fragment>
							)}
							{this.state.loading && <Spin />}
						</Card>
						<Modal
							title={showRejectionModal ? 'Confirm that you want to reject this post' : 'Confirm that you want to approve this post'}
							visible={this.state.showModal}
							onCancel={() => {
								this.setState({ showModal: false, showRejectionModal: false });
							}}
							footer={
								showRejectionModal
									? [
											<Button key='cancel' type='secondary' onClick={() => this.setState({ showModal: false, showRejectionModal: false })}>
												Cancel
											</Button>,
											<Button key='submit' type='danger' onClick={this.rejectAssignmentReview(client, assignmentReview)}>
												Reject
											</Button>
									  ]
									: [
											<Button key='cancel' type='secondary' onClick={() => this.setState({ showModal: false })}>
												Cancel
											</Button>,
											<Button key='submit' type='success' onClick={this.approveAssignmentReview(client, assignmentReview)}>
												Approve
											</Button>
									  ]
							}
						>
							{showRejectionModal ? (
								<div>
									<strong>Valid reasons for rejecting posts include:</strong>
									<ul>
										<li>The content is not the correct format as specified in the assignment, i.e the influencer uploaded a video and not a picture</li>
										<li>The post contains nudity, obscenity, harrassment or discrimination</li>
										<li>The post contains weapons, drugs or similar items</li>
									</ul>
									<strong>Not valid reasons for rejecting posts:</strong>
									<ul>
										<li>{`It follows the campaign guidelines but you are "not really satisfied about the quality"`}</li>
										<li>You have changed your mind of what you want</li>
									</ul>
									<em>
										Should conflict arise, the Collabs campaign management team will make an unbiased decision if guidelines were followed or not. If the
										influencer does follow the guidelines and the rejection is deemed as faulty, the influencer will still be compensated even if not posting.
									</em>
								</div>
							) : (
								<div>
									<strong>Valid reasons for approving posts include:</strong>
									<ul>
										<li>The content is the correct format as specified in the assignment</li>
										<li>The post don't contains nudity, obscenity, harrassment or discrimination</li>
										<li>The post don't contains weapons, drugs or similar items</li>
									</ul>
									<strong>Reason for valid posts approve:</strong>
									<ul>
										<li>{`It follows the campaign guidelines and you are "really satisfied about the quality"`}</li>
									</ul>
								</div>
							)}

							<WrappedAssignmentReviewForm wrappedComponentRef={this.saveFormRef} passedDeadline={passedDeadline} showRejectionModal={showRejectionModal} />
						</Modal>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}

export default ReviewCard;
