import React from 'react';
import { Row, Col, Icon, Card, Alert } from 'antd';
import { FormattedMessage } from 'react-intl';

import ListWithQuery from './list-with-query';
import CampaignLayout from '../layout/';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';

export default class Review extends React.Component {
	render() {
		return (
			<CampaignLayout>
				{({ campaign }) => {
					return (
						<React.Fragment>
							<Row className='mb-10'>
								<Col xs={{ span: 24 }}>
									<Alert
										message='Important!'
										description="This section is for you to review the content influencers will upload for their given assignments. You need to review all content before it's posted. Please answer Approve or reject on all posts. If you do nothing, all posts will automatically be approved."
										type='info'
										closeText='I understand, close this message'
										showIcon
									/>
								</Col>
							</Row>
							<Row gutter={30} className='mb-20'>
								<Col xs={{ span: 24 }} md={{ span: 12 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta avatar={<Icon type='search' style={{ fontSize: 24 }} />} title='Posts that needs review' />
										<h2 className='color-blue mb-0'>
											<FormattedMessage
												id='assignment-review-to-review-count'
												defaultMessage={`{count} {count, plural, one {post} other {posts}}`}
												values={{ count: campaign.assignmentReviewToReviewCount }}
											/>
										</h2>
									</Card>
								</Col>
								<Col xs={{ span: 24 }} md={{ span: 12 }}>
									<Card className='small-title mb-10 mt-10'>
										<Card.Meta avatar={<Icon type='check' style={{ fontSize: 24 }} />} title='Approved posts' />
										<h2 className='color-blue mb-0'>
											<FormattedMessage
												id='assignment-review-to-review-count'
												defaultMessage={`{count} {count, plural, one {post} other {posts}}`}
												values={{ count: campaign.assignmentReviewApprovedCount }}
											/>
										</h2>
									</Card>
								</Col>
								{false && (
									<Col xs={{ span: 24 }} md={{ span: 8 }}>
										<Card className='small-title mb-10 mt-10'>
											<Card.Meta avatar={<Icon type='user-delete' style={{ fontSize: 24 }} />} title='Not posted for review' />
											<h2 className='color-blue mb-0'>14 influencers</h2>
										</Card>
									</Col>
								)}
							</Row>
							<ListWithQuery campaign={campaign} />
						</React.Fragment>
					);
				}}
			</CampaignLayout>
		);
	}
}
