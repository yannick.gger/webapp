import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Query } from 'react-apollo';
import { Spin } from 'antd';

import getAssignmentReviewsQuery from 'graphql/get-assignment-reviews.graphql';
import List from './list';

class AssignmentReviewsWithQuery extends Component {
	state = {
		query: {
			status: 'to_review'
		}
	};

	updateQueryHandler = (newVariables) => {
		return this.setState((prevState) => ({ query: { ...prevState.query, ...newVariables } }));
	};

	static getDerivedStateFromProps = (props, state) => {
		return { ...state, query: { ...state.query, id: props.match.params.campaignId } };
	};

	render() {
		const { campaign, mdSpan } = this.props;

		return (
			<Query query={getAssignmentReviewsQuery} variables={this.state.query} notifyOnNetworkStatusChange>
				{({ loading, error, data, fetchMore }) => {
					if (loading) return <Spin className='collabspin' />;
					if (error) return <p>Something went wrong</p>;
					return (
						<List
							data={data}
							handleUpdateQuery={this.updateQueryHandler}
							query={this.state.query}
							fetchMore={fetchMore}
							error={error}
							loading={loading}
							campaign={campaign}
							mdSpan={mdSpan}
						/>
					);
				}}
			</Query>
		);
	}
}

export default withRouter(AssignmentReviewsWithQuery);
