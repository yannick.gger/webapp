import React, { Component } from 'react';
import { Button, message, Row, Col, Card, Modal, Icon } from 'antd';
import { ApolloConsumer, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';
import { isAdmin, hasGhostToken } from 'services/auth-service';
import CampaignLayout from '../layout/';
import CampaignForm from 'components/campaign-form/campaign-form-no-wizard.js';
import removeCampaign from 'graphql/remove-campaign.graphql';

const getOrganizationCampaignQuery = gql`
	query getOrganizationCampaign($id: ID!) {
		campaign(id: $id) {
			id
			name
			brief
			brandName
			contactPerson
			notificationEmail
			influencerTargetCount
			tags
			mentions
			language
			currency
		}
	}
`;

const UPDATE_CAMPAIGN = gql`
	mutation updateCampaign(
		$id: ID!
		$name: String!
		$brief: String
		$brandName: String
		$contactPerson: String
		$notificationEmail: String
		$influencerTargetCount: Int!
		$tags: [String]
		$mentions: [String]
		$language: String
		$currency: String
	) {
		updateCampaign(
			input: {
				id: $id
				name: $name
				brief: $brief
				brandName: $brandName
				contactPerson: $contactPerson
				notificationEmail: $notificationEmail
				influencerTargetCount: $influencerTargetCount
				tags: $tags
				mentions: $mentions
				language: $language
				currency: $currency
			}
		) {
			campaign {
				id
				name
				brief
				brandName
				contactEmail
				contactPerson
				brandName
				influencerTargetCount
				tags
				mentions
				language
				currency
			}
		}
	}
`;

class EditForm extends Component {
	state = {
		fields: {},
		adminOverride: false
	};
	showDeleteCampaignConfirm = ({ client, campaignId }) => {
		const { history, match } = this.props;
		Modal.confirm({
			title: 'Are you sure you want to remove this campaign?',
			content: 'A removed campaign can not be restored after removal.',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				client
					.mutate({
						mutation: removeCampaign,
						variables: { id: campaignId }
					})
					.then(() => {
						message.info('Campaign was removed');
						history.push(`/${match.params.organizationSlug}/campaigns`);
					});
			}
		});
	};
	static getDerivedStateFromProps = (props, state) => {
		let propsFields = {};

		Object.keys(props.campaign)
			.filter((key) =>
				['name', 'brief', 'influencerTargetCount', 'brandName', 'contactPerson', 'notificationEmail', 'tags', 'mentions', 'language', 'currency'].includes(key)
			)
			.forEach((key) => {
				propsFields[key] = {
					value: props.campaign[key]
				};
			});

		return { fields: { ...propsFields, ...state.fields } };
	};
	handleSubmit = (updateCampaign, campaignId) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(
				async (err, { influencerTargetCount, name, brief, brandName, contactPerson, notificationEmail, mentions, tags, language, currency }) => {
					if (err) return null;

					const graphQlValues = {
						influencerTargetCount,
						name,
						brief,
						brandName,
						contactPerson,
						notificationEmail,
						id: campaignId,
						mentions,
						tags,
						language,
						currency
					};

					const res = await updateCampaign({ variables: graphQlValues });
					if (res.data.updateCampaign) {
						message.info('Campaign was updated');
					} else {
						message.error('Something went wrong');
					}
				}
			);
		};
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	handleFormChange = (changedFields) => {
		this.setState(({ fields }) => ({
			fields: { ...fields, ...changedFields }
		}));
	};
	handleAdminOverride = (adminOverride) => {
		this.setState({ adminOverride });
	};
	render() {
		const { fields, adminOverride } = this.state;
		const { campaign, organization } = this.props;

		const inputDisabled = campaign.invitesSent && !adminOverride;

		return (
			<Mutation mutation={UPDATE_CAMPAIGN} refetchQueries={[{ query: getOrganizationCampaignQuery, variables: { id: campaign.id } }]}>
				{(updateCampaign, { loading }) => (
					<React.Fragment>
						<CampaignForm
							fields={fields}
							campaign={campaign}
							organization={organization}
							wrappedComponentRef={this.saveFormRef}
							onChange={this.handleFormChange}
							enableSave={this.handleAdminOverride}
							hideUploadFields
						/>
						<div className='px-20 py-20 d-flex justify-content-space-between'>
							<Button
								size='large'
								key='submit'
								type='primary'
								loading={loading}
								onClick={this.handleSubmit(updateCampaign, campaign.id)}
								disabled={inputDisabled}
							>
								Save
							</Button>
							{(isAdmin() ||
								hasGhostToken() ||
								((campaign.campaignReviews.edges.length === 0 ||
									(campaign.campaignReviews.edges.length > 0 && campaign.campaignReviews.edges[0].node.status !== 'approved')) &&
									!campaign.reportSent &&
									!campaign.makeAsReport)) && (
								<ApolloConsumer>
									{(client) => (
										<Button
											className='color-red'
											size='large'
											style={{ border: '1px solid #d74664' }}
											onClick={() => {
												this.showDeleteCampaignConfirm({ client: client, campaignId: campaign.id });
											}}
										>
											<Icon type='delete' className='mr-10 red-trash' />
											Delete
										</Button>
									)}
								</ApolloConsumer>
							)}
						</div>
					</React.Fragment>
				)}
			</Mutation>
		);
	}
}

class Settings extends Component {
	render() {
		const { history, match } = this.props;
		return (
			<CampaignLayout>
				{({ campaign }) => (
					<Row>
						<Col sm={{ span: 14, offset: 5 }}>
							<Card className='parts-wrapper' style={{ width: '100%' }}>
								<EditForm campaign={campaign} history={history} match={match} />
							</Card>
						</Col>
					</Row>
				)}
			</CampaignLayout>
		);
	}
}

export default withRouter(Settings);
