import DashboardContainer from 'components/Dashboard/DashboardContainer';

const HomeDashboard = () => {
	return <DashboardContainer />;
};

export default HomeDashboard;
