import withTheme from 'hocs/with-theme';
import HomeDashboard from '../HomeDashboard/HomeDashboard';

const Dashboard = () => {
	return <HomeDashboard />;
};

export default withTheme(Dashboard, 'homeDashboard');
