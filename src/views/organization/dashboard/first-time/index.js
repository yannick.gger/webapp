import React, { Component } from 'react';
import { Row, Col, Button } from 'antd';
import { ReactSVG } from 'react-svg';
import OrganizationLink from 'components/organization-link/';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';
import NoDash from '../no-dashboard.svg';

export default class DashboardFirstTime extends Component {
	render() {
		return (
			<div className='pb-30'>
				<Row>
					<Col sm={{ span: 8, offset: 8 }} className='text-center'>
						<div className='empty-icon' style={{ marginRight: '-50px' }}>
							<ReactSVG src={NoDash} />
						</div>
						<div className='mt-30 mb-30'>
							<h2>Getting started</h2>
							<p>It's easy getting started. Start by creating your first campaign, then use the Discover feature to find relevant influencers.</p>
						</div>
						<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
							<div className='d-flex justify-content-center'>
								<OrganizationLink to='/campaigns/create'>
									<Button type='primary' size='large' className='btn-uppercase'>
										Create my first campaign
									</Button>
								</OrganizationLink>
							</div>
						</GhostUserAccess>
					</Col>
				</Row>
			</div>
		);
	}
}
