import React, { Component } from 'react';
import { Row, Col, Card, Dropdown, Menu, Button, Layout } from 'antd';

import { Link } from 'react-router-dom';

import OrganizationLink from 'components/organization-link';
import CampaignList from 'components/campaign-list/';

import DashboardLists from './dashboard-lists/';

import DiscoverIllu from './select-influencers.svg';
import CreateListsIllu from './make-lists.svg';
import CreateCampaignIllu from './launch-campaign.svg';

const MenuItemGroup = Menu.ItemGroup;

export default class Dashboard extends Component {
	render() {
		const menu = (
			<Menu>
				<MenuItemGroup title='Create something new'>
					<Menu.Item key='1'>
						<OrganizationLink to='/campaigns/create'>Create a campaign</OrganizationLink>
					</Menu.Item>
				</MenuItemGroup>
				<Menu.Divider />
				<MenuItemGroup title='Discover'>
					<Menu.Item key='3'>
						<OrganizationLink to='/discover'>Discover influencers</OrganizationLink>
					</Menu.Item>
				</MenuItemGroup>
			</Menu>
		);

		return (
			<Layout>
				<Layout.Header>
					<h1>Dashboard</h1>
					<Dropdown overlay={menu} trigger={['click']} placement='bottomRight'>
						<Button type='primary' icon=' ion ion-plus' size='large' />
					</Dropdown>
				</Layout.Header>
				<Layout.Content>
					<div className='mb-100'>
						<Row className='meta-stats border-bottom pb-30 mb-30'>
							<Col sm={{ span: 12 }} lg={{ span: 5 }} className='text-center'>
								<h1>2</h1>
								<h4>Lists</h4>
								<span className='ant-list-item-action-split' />
							</Col>
							<Col sm={{ span: 12 }} lg={{ span: 5 }} className='text-center'>
								<h1>1</h1>
								<h4>Campaign</h4>
								<span className='ant-list-item-action-split' />
							</Col>
							<Col sm={{ span: 12 }} lg={{ span: 5 }} className='text-center'>
								<h1>251</h1>
								<h4>Influencers in campaigns</h4>
								<span className='ant-list-item-action-split' />
							</Col>
							<Col sm={{ span: 12 }} lg={{ span: 8 }} className='text-center'>
								<h1>25000 SEK</h1>
								<h4>Total spend this year</h4>
							</Col>
						</Row>
						<Row gutter={30} className='mb-30 pb-30 border-bottom'>
							<Col sm={{ span: 8 }}>
								<OrganizationLink to='/discover'>
									<Card hoverable className='action-card has-icon-left' cover={<img src={DiscoverIllu} />}>
										<Card.Meta title='Discover influencers' />
										<p className='mt-10 mb-4'>Explore and search millions of matches</p>
									</Card>
								</OrganizationLink>
							</Col>

							<Col sm={{ span: 8 }}>
								<Card hoverable className='action-card has-icon-left' cover={<img src={CreateListsIllu} />}>
									<Card.Meta title='Create list' />
									<p className='mt-10 mb-4'>Organize the influencers you find into lists</p>
								</Card>
							</Col>

							<Col sm={{ span: 8 }}>
								<OrganizationLink to='/campaigns/create'>
									<Card hoverable className='action-card has-icon-left' cover={<img style={{ marginTop: '-6px' }} src={CreateCampaignIllu} />}>
										<Card.Meta title='Create campaign' />
										<p className='mt-10 mb-4'>Explore and search millions of matches</p>
									</Card>
								</OrganizationLink>
							</Col>
						</Row>
						<Row className='mb-30 pb-30 border-bottom'>
							<Col sm={{ span: 24 }}>
								<div className='mb-20 clearfix'>
									<h3 className='fl'>Your lists</h3>
								</div>
								<DashboardLists />
								<Link to='/wizard' className='mt-30 empty-cell create-new small'>
									<svg width='100' height='100' viewBox='-0.2 -0.2 100.5 100.4' preserveAspectRatio='none' className='svg-dashed-radius'>
										<path
											vectorEffect='non-scaling-stroke'
											fill='none'
											d='M 1 0 L 99 0 C 99.5 0 100 0.5 100 1 L 100 99 C 100 99.5 99.5 100 99 100 L 1 100 C 0.5 100 0 99.5 0 99 L 0 1 C 0 0.5 0.5 0 1 0 Z '
										/>
									</svg>
									<span>Create new list</span>
								</Link>
							</Col>
						</Row>
						<Row>
							<Col sm={{ span: 24 }}>
								<div className='mb-20 clearfix'>
									<h3 className='fl'>Your campaigns</h3>
								</div>
								<CampaignList />
							</Col>
						</Row>
					</div>
				</Layout.Content>
			</Layout>
		);
	}
}
