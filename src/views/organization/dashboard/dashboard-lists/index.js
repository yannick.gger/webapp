import React, { Component } from 'react';
import { Table, Avatar, Layout } from 'antd';

const columns = [
	{
		title: 'List name',
		dataIndex: 'name'
	},
	{
		title: 'Influencers',
		dataIndex: 'avatar'
	},
	{
		title: 'Engagement',
		dataIndex: 'engagement'
	},
	{
		title: '',
		key: 'action',
		render: () => (
			<span className='fr pr-20'>
				<a href='#'>View list</a>
			</span>
		)
	}
];

const size = 'small';
const data = [];

for (let i = 1; i < 3; i++) {
	data.push({
		name: `Edward King ${i}`,
		key: i,
		avatar: (
			<div className='avatar-list'>
				<Avatar
					size={size}
					src='https://scontent-arn2-1.cdninstagram.com/vp/dc3791c680bd6ea05e7ede0313cd8b7a/5B0F4C35/t51.2885-19/s320x320/25017784_189527905120690_649572112272457728_n.jpg'
				/>
				<Avatar size={size} style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>
					U
				</Avatar>
				<Avatar
					size={size}
					src='https://scontent-arn2-1.cdninstagram.com/vp/4c1746d3592ec3adcb523bf142cc64d0/5B02B565/t51.2885-19/11429710_1631650463778490_1207545180_a.jpg'
				/>
				<Avatar size={size} style={{ color: '#fff', backgroundColor: '#006EF3' }}>
					U
				</Avatar>
				<Avatar
					size={size}
					src='https://scontent-arn2-1.cdninstagram.com/vp/e85bd4d3de24fdd1ad604fce25422398/5B104F82/t51.2885-19/s320x320/14240890_1185285298201136_449763531_n.jpg'
				/>
				<Avatar
					size={size}
					src='https://scontent-arn2-1.cdninstagram.com/vp/81771483c02c8fe65d4195205534871e/5B20803E/t51.2885-19/s320x320/25016435_180335442563558_3409702054426312704_n.jpg'
				/>
				<Avatar size={size} style={{ color: '#fff', backgroundColor: '#5C6FD7' }}>
					S
				</Avatar>
				<Avatar
					size={size}
					src='https://scontent-arn2-1.cdninstagram.com/vp/422080c85529666fdae4270999a2f13a/5B08C803/t51.2885-19/s150x150/22499837_1886290735022802_2185301960322711552_n.jpg'
				/>
				<Avatar size={size} style={{ color: '#fff', backgroundColor: '#8BD06A' }}>
					J
				</Avatar>
				<Avatar size={size} style={{ color: '#777', backgroundColor: '#f1f3f6' }}>
					+35
				</Avatar>
			</div>
		),
		followers: '194.8k',
		reach: '889k',
		engagement: '3.19%'
	});
}

class DashboardLists extends Component {
	render() {
		return (
			<Layout>
				<Layout.Header>
					<h1>Discover</h1>
				</Layout.Header>
				<Layout.Content>
					<div>
						<Table columns={columns} dataSource={data} pagination={false} />
					</div>
				</Layout.Content>
			</Layout>
		);
	}
}

export default DashboardLists;
