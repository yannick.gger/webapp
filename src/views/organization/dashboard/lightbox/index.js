import React from 'react';
import { ReactSVG } from 'react-svg';

const Lightbox = () => {
	return (
		<div className='modal fade flat-white-modal campaign-list-modal' tabIndex='-1' role='dialog' aria-labelledby='campaign-list-modal' aria-hidden='true'>
			<button type='button' className='close' data-dismiss='modal' aria-label='Close'>
				<span aria-hidden='true'>
					<i className='ion ion-android-close' />
				</span>
			</button>
			<div className='modal-dialog modal-lg'>
				<div className='modal-content'>
					<div className='modal-header justify-content-center'>
						<div className='modal-headline wizard-headline text-center'>
							<h4>Create campaign</h4>
							<p>There is two ways to go. Either create a campaign, then invite influencers, or the other way around.</p>
						</div>
					</div>
					<div className='modal-body'>
						<section className='wizard-step one'>
							<div className='container'>
								<div className='row'>
									<div className='col-sm-12'>
										<div className='box-picker'>
											<div className='box-content text-center box-hover'>
												<div className='image'>
													<ReactSVG src='/assets/img/app/create-campaign-illu.svg' />
												</div>
												<h3>Create campaign</h3>
												<p className='mt-2 mb-4'>Follow our step-by-step wizard to create a campaign</p>
												<a href='/wizard/' className='btn btn-primary btn-wizard btn-uppercase'>
													Create campaign
												</a>
											</div>
										</div>
									</div>
									<div className='col-sm-12'>
										<div className='box-picker'>
											<div className='box-content text-center box-hover'>
												<div className='image'>
													<ReactSVG src='/assets/img/app/find-lists-illu.svg' />
												</div>
												<h3>Find influencers</h3>
												<p className='mt-2 mb-4'>Use our extensive ease-to-use interface to find your target engaged audience</p>
												<a href='/discover/' className='btn btn-primary btn-wizard btn-uppercase'>
													Find lists
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	);
};
export default Lightbox;
