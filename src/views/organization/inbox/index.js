import React, { Component } from 'react';
import { Row, Col, Layout } from 'antd';

import NoInbox from './no-inbox.svg';

class Assignments extends Component {
	render() {
		document.title = `Inbox | Collabs`;

		return (
			<Layout>
				<Layout.Header>
					<h1>Inbox</h1>
				</Layout.Header>
				<Layout.Content>
					<div>
						<Row>
							<Col sm={{ span: 10, offset: 7 }} className='text-center'>
								<div className='empty-icon'>
									<img src={NoInbox} alt='' className='svg' />
								</div>
								<div className='mb-30'>
									<h1>Your conversations</h1>
									<p>Inbox will be available soon. This is where you will be able to message and negotiate with all the influencers directly.</p>
								</div>
							</Col>
						</Row>
					</div>
				</Layout.Content>
			</Layout>
		);
	}
}

export default Assignments;
