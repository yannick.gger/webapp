import { Button, Dropdown, Empty, Menu, message, Divider, Modal, Tag } from 'antd';
import ButtonLink from 'components/button-link';
import OrganizationLink from 'components/organization-link';
import { Link } from 'react-router-dom';
import gql from 'graphql-tag';
import getCampaigns from 'graphql/get-campaigns.graphql';
import listsIndex from 'graphql/get-discover-lists.graphql';
import React, { Component } from 'react';
import { Mutation, Query } from 'react-apollo';
import { injectIntl, intlShape } from 'react-intl';
import { withRouter } from 'react-router-dom';
import { firstOrganization } from 'services/auth-service';

const { confirm } = Modal;

const ADD_INSTAGRAM_OWNER = gql`
	mutation addInstagramOwnersToCampaign($campaignId: ID!, $instagramOwnerIds: [ID]!) {
		addInstagramOwnersToCampaign(input: { campaignId: $campaignId, instagramOwnerIds: $instagramOwnerIds }) {
			campaign {
				id
				name
			}
		}
	}
`;

class CampaignPicker extends Component {
	showConfirm = (campaign, instagramOwnerIds, addInstagramOwnerToCampaign) => {
		const { id: campaignId, invitesSent, spotsLeft, invitesOpening } = campaign;

		confirm({
			content: `Are you sure you want to add these influencers to campaign? ${
				invitesSent && invitesOpening && spotsLeft > 0
					? "This time we will send invites to new influencers immediately without the need of admin's approval"
					: ''
			}`,
			title: 'Adding new influencers',
			onOk: () => {
				addInstagramOwnerToCampaign({
					variables: { campaignId, instagramOwnerIds }
				});
			}
		});
	};

	render() {
		const {
			instagramOwners,
			match: { params }
		} = this.props;
		const limit = 10;
		const { id: organizationId } = firstOrganization();
		const campaignMenu = ({ campaigns, addInstagramOwnerToCampaign, instagramOwners }) => (
			<Menu>
				<Menu.ItemGroup title='Add selection to list'>
					<Menu.Item>
						<Link
							to={{
								pathname: `/${params.organizationSlug}/discover/create-new-list`,
								state: { instagramOwners }
							}}
						>
							Create new list from selection
						</Link>
					</Menu.Item>
					<Menu.Item>
						<Link
							to={{
								pathname: `/${params.organizationSlug}/discover/add-selection-to-list`,
								state: { instagramOwners }
							}}
						>
							Add selection to list
						</Link>
					</Menu.Item>
				</Menu.ItemGroup>
				<Divider />
				<Menu.ItemGroup title='Add selection to campaign'>
					{campaigns && campaigns.length === 0 && (
						<Empty description='Create a campaign to add influencers.'>
							<OrganizationLink to='/campaigns/create'>
								<Button>Create campaign</Button>
							</OrganizationLink>
						</Empty>
					)}
					{campaigns &&
						campaigns.map((campaign, index) => {
							if (index < limit) {
								return (
									<Menu.Item data-ripple='rgba(132,146, 164, 0.2)' key={campaign.node.id}>
										<ButtonLink onClick={() => this.showConfirm(campaign.node, instagramOwners.map(({ id }) => id), addInstagramOwnerToCampaign)}>
											{campaign.node.name}
											{campaign.node.active ? (
												<Tag color='green' style={{ float: 'right' }}>
													Active
												</Tag>
											) : campaign.node.draft ? (
												<Tag color='orange' style={{ float: 'right' }}>
													Draft
												</Tag>
											) : (
												''
											)}
										</ButtonLink>
									</Menu.Item>
								);
							}
						})}
				</Menu.ItemGroup>
				{campaigns && campaigns.length > limit && (
					<Menu.ItemGroup>
						<Menu.Item style={{ display: 'flex', justifyContent: 'center' }}>
							<Button type='primary'>
								<Link
									to={{
										pathname: `/${this.props.match.params.organizationSlug}/discover/add-influencers-to-campaign`,
										state: { instagramOwners: instagramOwners.map(({ id }) => id) }
									}}
								>
									View all campaigns
								</Link>
							</Button>
						</Menu.Item>
					</Menu.ItemGroup>
				)}
			</Menu>
		);
		return (
			<Mutation
				mutation={ADD_INSTAGRAM_OWNER}
				refetchQueries={[{ query: listsIndex, variables: { organizationSlug: params.organizationSlug } }]}
				onCompleted={({ addInstagramOwnersToCampaign }) => {
					const campaignName = addInstagramOwnersToCampaign && addInstagramOwnersToCampaign.campaign ? addInstagramOwnersToCampaign.campaign.name : '';
					if (this.props.handleSuccess) {
						this.props.handleSuccess();
					}
					message.success(
						this.props.intl.formatMessage(
							{
								id: 'add-instagram-owners-to-campaign-success',
								defaultMessage: `Added {count} {count, plural, one {influencer} other {influencers}} to campaign "{campaignName}"`
							},
							{ count: instagramOwners.length, campaignName }
						)
					);
				}}
				onError={({ graphQLErrors }) => {
					if (graphQLErrors) {
						graphQLErrors.forEach((error) => {
							message.error(error.message);
						});
					}
				}}
			>
				{(addInstagramOwnerToCampaign, { loading: mutationLoading }) => (
					<Query query={getCampaigns} variables={{ includeCanAddInfluencerCampaignsOnly: true, organizationId }}>
						{({ data, loading: queryLoading }) => {
							return (
								<Dropdown
									overlay={campaignMenu({ campaigns: (data.campaigns && data.campaigns.edges) || [], addInstagramOwnerToCampaign, instagramOwners })}
									trigger={['click']}
									loading={mutationLoading || queryLoading}
									disabled={this.props.disabled || queryLoading}
								>
									{this.props.children({ mutationLoading })}
								</Dropdown>
							);
						}}
					</Query>
				)}
			</Mutation>
		);
	}
}

CampaignPicker.propTypes = {
	intl: intlShape.isRequired
};

export default withRouter(injectIntl(CampaignPicker));
