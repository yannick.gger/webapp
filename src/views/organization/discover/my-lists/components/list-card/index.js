import React, { Component } from 'react';
import { Row, Col, Tag, Card, Avatar } from 'antd';

import './styles.scss';

export default class CampaignCard extends Component {
	render() {
		return (
			<Card hoverable>
				<Card.Meta title='Fitness influencers' />
				<hr />
				<Row>
					<Col>
						<div className='avatar-list'>
							<Avatar src='https://scontent-arn2-1.cdninstagram.com/vp/dc3791c680bd6ea05e7ede0313cd8b7a/5B0F4C35/t51.2885-19/s320x320/25017784_189527905120690_649572112272457728_n.jpg' />
							<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>U</Avatar>
							<Avatar src='https://scontent-arn2-1.cdninstagram.com/vp/e85bd4d3de24fdd1ad604fce25422398/5B104F82/t51.2885-19/s320x320/14240890_1185285298201136_449763531_n.jpg' />
							<Avatar src='https://scontent-arn2-1.cdninstagram.com/vp/422080c85529666fdae4270999a2f13a/5B08C803/t51.2885-19/s150x150/22499837_1886290735022802_2185301960322711552_n.jpg' />
							<Avatar style={{ color: '#fff', backgroundColor: '#8BD06A' }}>J</Avatar>
						</div>
					</Col>
				</Row>
				<hr />
				<Row>
					<Col xs={{ span: 24 }}>
						<h4 style={{ marginBottom: '0' }}>
							<span className='color-blue'>213</span> Influencers
						</h4>
					</Col>
					<Col xs={{ span: 24 }}>
						<h4 style={{ marginBottom: '0' }}>
							<span className='color-blue'>3-4%</span> Engagement
						</h4>
					</Col>
					<Col xs={{ span: 24 }}>
						<h4 style={{ marginBottom: '0' }}>
							<span className='color-blue'>1M</span> Reach
						</h4>
					</Col>
				</Row>
				<hr />
				<Tag className='card-tag'>Sweden</Tag>
				<Tag className='card-tag'>Fitness influencers</Tag>
			</Card>
		);
	}
}
