import React from 'react';
import { Modal } from 'antd';
import MyListsWithSearch from '../../my-lists-with-search.js';

class AddSelectionToOtherList extends React.Component {
	handleCancel = () => {
		this.props.history.goBack();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		const { match } = this.props;
		return (
			<Modal
				className='add-selection-modal'
				title='Add selection to other list'
				visible
				onOk={this.handleOk}
				onCancel={this.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
			>
				<MyListsWithSearch organizationSlug={match.params.organizationSlug} closeModal={this.handleCancel} />
			</Modal>
		);
	}
}

export default AddSelectionToOtherList;
