import React, { Component } from 'react';
import { Row, Col, Button, Layout } from 'antd';
import OrganizationLink from 'components/organization-link/';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';
import NoLists from './no-lists.svg';

class Assignments extends Component {
	render() {
		return (
			<Layout>
				<Layout.Content>
					<Row>
						<Col className='text-center'>
							<div className='empty-icon'>
								<img src={NoLists} alt='' className='svg' style={{ marginRight: '-85px' }} />
							</div>
							<div className='mb-30'>
								<h2>You haven't created any lists yet</h2>
							</div>
							<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
								<Button type='primary' className='btn-uppercase' size='large'>
									<OrganizationLink to='/discover/lists/create-new-list'>Create your first list</OrganizationLink>
								</Button>
							</GhostUserAccess>
						</Col>
					</Row>
				</Layout.Content>
			</Layout>
		);
	}
}

export default Assignments;
