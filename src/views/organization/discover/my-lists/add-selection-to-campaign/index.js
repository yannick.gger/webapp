import React from 'react';
import { Modal } from 'antd';

import AddSelectionForm from '../add-selection-to-campaign-form';

class AddSelectionToCamapaign extends React.Component {
	handleCancel = () => {
		this.props.history.goBack();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		const { match } = this.props;
		return (
			<Modal
				className='add-selection-modal'
				title='Add selection to campaign'
				visible
				onOk={this.handleOk}
				onCancel={this.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
			>
				<AddSelectionForm closeModal={this.handleCancel} organizationSlug={match.params.organizationSlug} />
			</Modal>
		);
	}
}

export default AddSelectionToCamapaign;
