import React from 'react';
import { Form, Button, Table, Layout, Spin, notification } from 'antd';
import { injectIntl } from 'react-intl';
import { withRouter } from 'react-router-dom';
import { Mutation, Query } from 'react-apollo';
import listsIndex from 'graphql/get-discover-lists.graphql';
import addInfluencersToList from 'graphql/add-influencers-to-list.graphql';
import ButtonLink from 'components/button-link';
class AddSelectionToOtherListForm extends React.Component {
	state = {
		disabled: false
	};
	addLoadingStateForButton = (event) => {
		const buttonClicked = event.target;
		const spinLoading = document.createElement('Spin');
		spinLoading.classList.add('collabspin');
		buttonClicked.parentNode.replaceChild(spinLoading, buttonClicked);
	};
	render() {
		const { location, organizationSlug, handleSuccess, closeModal, history } = this.props;
		const { disabled } = this.state;
		const columns = [
			{
				title: 'List name',
				key: 'name',
				width: '300px',
				render: (record) => <div>{record.node.name}</div>
			},
			{
				title: 'action',
				key: 'action',
				width: '300px',
				render: (record) => {
					const instagramOwners = location.state && location.state.instagramOwners;
					const { id, name } = record.node;
					return (
						<React.Fragment>
							<Mutation
								mutation={addInfluencersToList}
								onCompleted={() => {
									if (handleSuccess) {
										handleSuccess();
									}
									notification.success({
										message: `Selection added to list ${name}`,
										description: (
											<div>
												<ButtonLink
													onClick={() => {
														history.push(`/${organizationSlug}/discover/list/${id}`);
													}}
												>
													Go to list
												</ButtonLink>
											</div>
										)
									});
									closeModal();
								}}
							>
								{(addInfluencersToList) => (
									<Button
										size='small'
										type='primary'
										disabled={disabled}
										onClick={(event) => {
											event.preventDefault();
											this.setState({
												disabled: true
											});
											this.addLoadingStateForButton(event);
											addInfluencersToList({
												variables: { myListId: id, instagramOwnerIds: instagramOwners && instagramOwners.map(({ id }) => id) }
											});
										}}
									>
										Add
									</Button>
								)}
							</Mutation>
						</React.Fragment>
					);
				}
			}
		];
		return (
			<Layout>
				<Layout.Header style={{ backgroundColor: 'white', paddingLeft: '25px' }}>
					<h1>Add selection to other list</h1>
				</Layout.Header>
				<Layout.Content style={{ padding: 0 }}>
					<Query query={listsIndex} variables={{ organizationSlug: organizationSlug }}>
						{({ data, loading: queryLoading }) => {
							if (queryLoading) {
								return (
									<Layout>
										<Layout.Content>
											<Spin className='collabspin' />
										</Layout.Content>
									</Layout>
								);
							}
							const lists = data.myLists.edges.filter((list) => list.node.id !== location.pathname.split('/')[4]);
							return <Table className='add-selection-table' rowKey='id' columns={columns} dataSource={data ? lists : []} />;
						}}
					</Query>
				</Layout.Content>
			</Layout>
		);
	}
}

const WrappedAddSelectionToOtherListForm = Form.create()(AddSelectionToOtherListForm);

export default withRouter(injectIntl(WrappedAddSelectionToOtherListForm));
