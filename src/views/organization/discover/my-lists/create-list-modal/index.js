import React from 'react';
import { Modal, Button, message } from 'antd';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import CreateListForm from '../create-list-form/';
import getOrganizationLists from 'graphql/get-organization-my-lists.graphql';

const CREATE_LIST = gql`
	mutation createMyList($name: String!, $organizationSlug: String!, $instagramOwnerIds: [ID]) {
		createMyList(input: { name: $name, organizationSlug: $organizationSlug, instagramOwnerIds: $instagramOwnerIds }) {
			myList {
				name
			}
		}
	}
`;

class CreateListModal extends React.Component {
	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	handleSubmit = (createMyList) => {
		const {
			match: { params },
			location: { state },
			closeModal,
			history
		} = this.props;

		return (e) => {
			e.preventDefault();

			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, obj) => {
				if (err) return null;

				const graphQlValues = {
					name: obj.listName,
					organizationSlug: params.organizationSlug
				};

				if (state && state.instagramOwners.length) {
					graphQlValues.instagramOwnerIds = state.instagramOwners.map(({ id }) => id);
				}

				const res = await createMyList({ variables: graphQlValues });

				if (res.data.createMyList) {
					message.info('List was created');

					closeModal();
					history.push(`/${params.organizationSlug}/discover/lists`);
				} else {
					message.error('Something went wrong');
				}
			});
		};
	};

	render() {
		return (
			<Mutation
				mutation={CREATE_LIST}
				refetchQueries={['listIndex', { query: getOrganizationLists, variables: { slug: this.props.match.params.organizationSlug } }]}
			>
				{(createMyList, { loading }) => (
					<Modal
						title='Create list'
						visible
						onOk={this.handleOk}
						onCancel={this.handleCancel}
						wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
						maskClosable={false}
						footer={[
							<Button key='submit' type='primary' onClick={this.handleSubmit(createMyList)} loading={loading} className='btn-uppercase mt-20'>
								Create list
							</Button>
						]}
					>
						<CreateListForm wrappedComponentRef={this.saveFormRef} organizationSlug={this.props.match.params.organizationSlug} />
					</Modal>
				)}
			</Mutation>
		);
	}
}

export default CreateListModal;
