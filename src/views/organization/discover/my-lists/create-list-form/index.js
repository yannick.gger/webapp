import React from 'react';
import { Form, Input } from 'antd';

const FormItem = Form.Item;

class CreateListForm extends React.Component {
	handleSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	};
	normFile = (e) => {
		console.log('Upload event:', e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	render() {
		const { getFieldDecorator } = this.props.form;
		return (
			<div>
				<Form onSubmit={this.handleSubmit} className=''>
					<FormItem>{getFieldDecorator('listName')(<Input size='large' className='input-line-style' placeholder='List name' />)}</FormItem>
				</Form>
			</div>
		);
	}
}

const WrappedCreateListForm = Form.create()(CreateListForm);

export default WrappedCreateListForm;
