import React from 'react';
import { Form, Button, notification } from 'antd';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { Mutation } from 'react-apollo';
import ButtonLink from 'components/button-link';
import DraftCampaignsWithSearchBar from '../../draft-campaign-with-search.js';
import addInstagramOwnersToCampaign from 'graphql/add-instagram-owners-to-campaign.graphql';

class AddSelectionToCampaignForm extends React.Component {
	state = {
		disabled: false
	};
	addLoadingStateForButton = (event) => {
		const buttonClicked = event.target;
		const spinLoading = document.createElement('Spin');
		spinLoading.classList.add('collabspin');
		buttonClicked.parentNode.replaceChild(spinLoading, buttonClicked);
	};

	render() {
		const { location, organizationSlug, history, handleSuccess, closeModal } = this.props;
		const columns = [
			{
				title: 'Campaign name',
				key: 'name',
				width: '300px',
				render: (record) => <div className='item-name-in-selection-modal'>{record.node.name}</div>
			},
			{
				title: 'action',
				key: 'action',
				width: '300px',
				render: (record) => {
					const instagramOwners = location.state && location.state.instagramOwners;
					return (
						<React.Fragment>
							<Mutation
								mutation={addInstagramOwnersToCampaign}
								onCompleted={() => {
									if (handleSuccess) {
										handleSuccess();
									}
									notification.success({
										message: `Selection added to campaign ${record.node.name}`,
										description: (
											<div>
												<ButtonLink
													onClick={() => {
														history.push(`/${organizationSlug}/campaigns/${record.node.id}`);
													}}
												>
													Go to your campaign
												</ButtonLink>
											</div>
										)
									});
									closeModal();
								}}
							>
								{(addInstagramOwnersToCampaign) => (
									<Button
										size='small'
										type='primary'
										disabled={this.state.disabled}
										onClick={(event) => {
											event.preventDefault();
											this.setState({
												disabled: true
											});
											this.addLoadingStateForButton(event);
											addInstagramOwnersToCampaign({
												variables: {
													campaignId: record.node.id,
													instagramOwnerIds: instagramOwners.map(({ id }) => id)
												}
											});
										}}
									>
										Add
									</Button>
								)}
							</Mutation>
						</React.Fragment>
					);
				}
			}
		];
		return <DraftCampaignsWithSearchBar columns={columns} organizationSlug={organizationSlug} />;
	}
}

const WrappedAddSelectionToCampaignForm = Form.create()(AddSelectionToCampaignForm);

export default withRouter(injectIntl(WrappedAddSelectionToCampaignForm));
