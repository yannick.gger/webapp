import React from 'react';
import { Form, Button, Table, Spin, notification, Input, message } from 'antd';
import { withRouter } from 'react-router-dom';
import { Query, withApollo } from 'react-apollo';
import listsIndex from 'graphql/get-discover-lists.graphql';
import addInfluencersToList from 'graphql/add-influencers-to-list.graphql';
import ButtonLink from 'components/button-link';
import NoList from 'views/organization/discover/my-lists/no-lists.js';

class MyListsWithSearch extends React.Component {
	state = {
		disabled: false,
		search: undefined,
		inputValue: ''
	};

	handleSearch = (value) => {
		this.setState({ search: value, inputValue: '' });
	};

	handleChangeSearchValue = (event) => {
		event.preventDefault();
		this.setState({
			inputValue: event.target.value
		});
	};

	loadMore = (fetchMore, endCursor) => {
		return fetchMore({
			variables: {
				after: endCursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					myLists: {
						...fetchMoreResult.myLists,
						edges: [...prev.myLists.edges, ...fetchMoreResult.myLists.edges]
					}
				};
				return result;
			}
		});
	};

	addInfluencersToList = (event, id, name, instagramOwners) => {
		const { client, organizationSlug, closeModal, history } = this.props;
		this.setState({ disabled: false });
		const buttonClicked = event.target;
		const spinLoading = document.createElement('Spin');
		spinLoading.classList.add('collabspin');
		buttonClicked.parentNode.replaceChild(spinLoading, buttonClicked);
		client
			.mutate({
				mutation: addInfluencersToList,
				variables: { myListId: id, instagramOwnerIds: instagramOwners && instagramOwners.map(({ id }) => id) }
			})
			.then((response) => {
				if (response.data) {
					notification.success({
						message: `Selection added to list ${name}`,
						description: (
							<div>
								<ButtonLink
									onClick={() => {
										history.push(`/${organizationSlug}/discover/list/${id}`);
									}}
								>
									Go to your list
								</ButtonLink>
							</div>
						)
					});
					closeModal();
				} else {
					message.error('Something went wrong');
				}
			});
	};
	render() {
		const { location, organizationSlug } = this.props;
		const { disabled, search, inputValue } = this.state;
		const { Search } = Input;
		const columns = [
			{
				title: 'List name',
				key: 'name',
				width: '300px',
				render: (record) => <div className='item-name-in-selection-modal'>{record.node.name}</div>
			},
			{
				title: 'action',
				key: 'action',
				width: '300px',
				render: (record) => {
					const instagramOwners = location.state && location.state.instagramOwners;
					const { id, name } = record.node;
					return (
						<React.Fragment>
							<Button size='small' type='primary' disabled={disabled} onClick={(event) => this.addInfluencersToList(event, id, name, instagramOwners)}>
								Add
							</Button>
						</React.Fragment>
					);
				}
			}
		];
		return (
			<section>
				<Query query={listsIndex} variables={{ organizationSlug, search }} notifyOnNetworkStatusChange>
					{({ data, loading: queryLoading, fetchMore }) => {
						if (queryLoading && !data.myLists) {
							return <Spin className='collabspin' />;
						}
						const { hasNextPage, endCursor } = data.myLists.pageInfo;
						const lists = (data && data.myLists.edges) || [];
						const otherLists = (data && data.myLists.edges.filter((list) => list.node.id !== location.pathname.split('/')[4])) || [];
						const isInAddSelectionToOtherListModal = location.pathname.includes('add-selection-to-other-list');
						if (lists.length > 0) {
							return (
								<React.Fragment>
									<div className='pb-25 pt-20'>
										<Search
											placeholder='Search list name'
											value={inputValue}
											onChange={this.handleChangeSearchValue}
											onSearch={(value) => this.handleSearch(value)}
										/>
									</div>
									<Table
										className='add-selection-table'
										rowKey='id'
										columns={columns}
										dataSource={isInAddSelectionToOtherListModal ? otherLists : lists}
										pagination={false}
										loading={queryLoading}
									/>
									<div className='pt-30 pb-30 text-center'>
										{hasNextPage && (
											<Button type='primary' loading={queryLoading} onClick={() => this.loadMore(fetchMore, endCursor)}>
												Load more
											</Button>
										)}
									</div>
								</React.Fragment>
							);
						} else {
							return <NoList />;
						}
					}}
				</Query>
			</section>
		);
	}
}

const WrappedMyListsWithSearchForm = Form.create()(MyListsWithSearch);

export default withRouter(withApollo(WrappedMyListsWithSearchForm));
