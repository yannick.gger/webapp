import { Avatar, Button, Col, Divider, Icon, Input, Popover, Progress, Row, Table, Tag, Empty } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';
import { lookup } from 'country-data';
import { withOrganization } from 'hocs';
import moment from 'moment';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { getInstagramUrl } from 'utils';
import Basket from './basket.js';
import CampaignPicker from './campaign-picker.js';
import TableErrorImg from 'assets/img/app/table-error.svg';
import { checkGhostUserIsSaler, isTester, isSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';
import InstagramProfileDrawer from 'components/instagram-profile-drawer';
import { MaleIcon, FemaleIcon } from 'components/icons';

const Search = Input.Search;

const PopoverContent = ({ audience }) => {
	const { countries, ages } = audience;
	return (
		<div style={{ width: '360px' }}>
			<h4>Top {countries.length} countries followers are from</h4>
			<div className='top-countries-container'>
				{countries.map((country) => (
					<div className='mb-5' key={country.id}>
						<span className={`flag-icon flag-icon-${country.country.toLowerCase()} mr-10`} />
						{lookup.countries({ alpha2: country.country })[0] ? lookup.countries({ alpha2: country.country })[0].name : country.country}{' '}
						<span style={{ width: '50%' }} className='fr'>
							<Progress percent={country.percent} strokeWidth={8} size='small' />
						</span>
					</div>
				))}
			</div>
			<Divider />
			<h4>Age spans</h4>
			{ages.map((ageGroup) => (
				<div className='mb-5' key={ageGroup.id}>
					{ageGroup.agespan}
					<span style={{ width: '50%' }} className='fr'>
						<Progress percent={ageGroup.percent} strokeWidth={8} size='small' />
					</span>
				</div>
			))}
		</div>
	);
};

const visitData = [];
const beginDay = new Date().getTime();
for (let i = 0; i < 5; i += 1) {
	visitData.push({
		x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
		y: Math.floor(Math.random() * 100) + 10
	});
}

const columns = [
	{
		title: '',
		dataIndex: 'avatar',
		width: '60px',
		render: (text, record) => <Avatar size='large' src={record.instagramOwnerUser.avatar} icon='user' />
	},
	{
		title: 'Name',
		dataIndex: 'instagramOwnerUser.username',
		width: '180px',
		render: (text, record) => <InstagramProfileDrawer instagramOwner={record} content={record.instagramOwnerUser.username} />
	},
	{
		title: 'Followers',
		dataIndex: 'followedByCount',
		sorter: true,
		render: (text, record) => (
			<span className='number-box gray'>
				<ShortAmountFormatter value={record.followedByCount} />
			</span>
		)
	},
	{
		title: 'Engagement',
		dataIndex: 'interactionRate',
		sorter: true,
		render: (text, record) => (
			<span style={{ width: '66px' }} className='number-box green'>
				{record.interactionRate}
			</span>
		)
	},
	{
		title: 'Category',
		key: 'categories',
		render: (text, record) => (
			<span>
				{record.category1 && <Tag color={record.category1.color}>{record.category1.name}</Tag>}
				{record.category2 && <Tag color={record.category2.color}>{record.category2.name}</Tag>}
			</span>
		)
	}
];

if (process.env.NODE_ENV !== 'production') {
	columns.push({
		title: 'Posts preview',
		dataIndex: 'instagramOwnerUser.featuredInstagramPosts',
		render: (featuredInstagramPosts, record) => {
			if (!featuredInstagramPosts || featuredInstagramPosts.length < 0) {
				return (
					<div className='bordered-box empty-state'>
						<span>No thumbnails</span>
					</div>
				);
			}

			return (
				<React.Fragment>
					<div className='d-flex'>
						{featuredInstagramPosts.map((post) => (
							<a key={post.id} href={post.link} target='_blank' rel='noopener noreferrer'>
								<Avatar style={{ marginRight: '2px' }} shape='square' size='large' src={post.thumbnail150x150} />
							</a>
						))}
						<a href='#' target='_blank' rel='noopener noreferrer'>
							<Avatar style={{ backgroundColor: '#EBEEF5' }} shape='square' size='large' icon='instagram' />
						</a>
					</div>
				</React.Fragment>
			);
		}
	});
}

class ListsView extends Component {
	state = {
		selectedRowKeys: [],
		selectedInstagramOwners: []
	};
	onSelectChange = (instagramOwners) => {
		return (selectedRowKeys) => {
			this.setState((prevState) => {
				const previousSelectedInstagramOwners = prevState.selectedInstagramOwners;
				const newSelectedInstagramOwners = selectedRowKeys
					.filter((key) => !prevState.selectedInstagramOwners.some(({ id }) => id === key))
					.map((key) => instagramOwners.find((i) => i.id === key));

				let resSelectedInstagramOwners = [...newSelectedInstagramOwners, ...previousSelectedInstagramOwners].filter(({ id }) =>
					selectedRowKeys.some((key) => key === id)
				);

				return {
					selectedRowKeys,
					selectedInstagramOwners: resSelectedInstagramOwners
				};
			});
		};
	};
	loadMore = (data, fetchMore) => {
		fetchMore({
			variables: {
				offset: data.discoverInstagramOwners.length
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) return prev;
				const assigned = Object.assign({}, prev, {
					discoverInstagramOwners: [...prev.discoverInstagramOwners, ...fetchMoreResult.discoverInstagramOwners]
				});

				return assigned;
			}
		});
	};
	render() {
		const { error, data, fetchMore, searchHandler, organization, loading, networkStatus } = this.props;
		const { selectedRowKeys } = this.state;
		const rowSelection = (instagramOwners) => ({
			selectedRowKeys,
			onChange: this.onSelectChange(instagramOwners)
		});
		const hasSelected = selectedRowKeys.length > 0;

		return (
			<div className='discover-wrapper'>
				<div className='mb-30 mt-30 border-top pt-30'>
					<Row className=''>
						<Col sm={{ span: 18 }}>
							<CampaignPicker
								instagramOwners={selectedRowKeys.map((key) => ({
									id: key
								}))}
								disabled={!hasSelected}
								handleSuccess={() => {
									this.setState({ selectedRowKeys: [], selectedInstagramOwners: [] });
								}}
							>
								{({ mutationLoading }) => (
									<Button type='primary' loading={loading || mutationLoading}>
										Add selected to... <Icon type='down' />
									</Button>
								)}
							</CampaignPicker>
							<span style={{ marginLeft: 20 }}>
								{hasSelected ? `${selectedRowKeys.length} ${selectedRowKeys.length > 1 ? 'influencers' : 'influencer'} selected` : ''}
							</span>
						</Col>
						<Col sm={{ span: 6 }}>
							{searchHandler && <Search allowClear={true} placeholder='Search @username' size='large' onSearch={(value) => searchHandler(value)} enterButton />}
						</Col>
					</Row>
				</div>
				<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
					<Basket
						instagramOwners={this.state.selectedInstagramOwners}
						disabled={!hasSelected}
						handleSuccess={() => {
							this.setState({ selectedRowKeys: [], selectedInstagramOwners: [] });
						}}
					/>
				</GhostUserAccess>

				<Table
					className='pb-30'
					rowSelection={rowSelection(data ? data.discoverInstagramOwners : [])}
					columns={columns}
					dataSource={data ? data.discoverInstagramOwners : []}
					rowKey='id'
					pagination={false}
					loading={loading}
					scroll={{ x: 1000 }}
					sortDirections={['descend', 'ascend']}
					onChange={(pagination, filters, sorter) => this.props.handleSort(sorter)}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load users' />
						) : (
							<Empty description='Nothing matching the search criterias found' />
						)
					}}
				/>

				{!error &&
					networkStatus !== 1 &&
					(!organization.isTrial || organization.isSubscribed || organization.testAccount) &&
					data &&
					data.discoverInstagramOwners &&
					data.discoverInstagramOwners.length !== 0 &&
					data.discoverInstagramOwners.length % 20 === 0 && (
						<div className='pb-30 text-center'>
							<Button type='primary' loading={networkStatus === 3} onClick={() => this.loadMore(data, fetchMore)}>
								Load more
							</Button>
						</div>
					)}
				{!error && networkStatus !== 1 && (organization.isTrial || !organization.isSubscribed) && !(isTester() && organization.testAccount) && !isSaler() && (
					<div className='text-center'>
						<Button type='primary' className='mt-30 mb-20' disabled>
							Load more
						</Button>
						<br />
						<span style={{ color: '#8c9298' }}>Upgrade your subscription to be able to load more accounts.</span>
					</div>
				)}
			</div>
		);
	}
}

export default withRouter(withOrganization(ListsView));
