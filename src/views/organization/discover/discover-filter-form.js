import React, { Component } from 'react';
import { Form, Slider, Radio, Select, Row, Col, Icon, Collapse, Button } from 'antd';
import { countries } from 'country-data';
import { defaultCountries, defaultMinFollowers, defaultMaxFollowers, sliderStepFollowers } from '../campaign/recommendations/influencer-filter-form';

const FormItem = Form.Item;
const Panel = Collapse.Panel;
const Option = Select.Option;

class DiscoverFilterForm extends Component {
	state = {
		activeKey: [],
		followedByCountMin: defaultMinFollowers,
		followedByCountMax: defaultMaxFollowers
	};

	handleReset = (e) => {
		e.preventDefault();
		this.props.form.resetFields();
	};

	onCollapse = (activeKey) =>
		this.setState({
			activeKey
		});

	callback = (key) => {
		console.log(key);
	};

	get renderCountry() {
		const countryData = [...countries.all.filter((country) => country.status === 'assigned').toSort('name')];

		return countryData.map((country) => (
			<Option key={country.alpha2 || 'all-opyion'} value={country.alpha2} name={country.name}>
				{country.name}
			</Option>
		));
	}
	render() {
		const { categories, form } = this.props;
		const { getFieldDecorator, getFieldValue } = form;
		const countryOptions = this.renderCountry;
		const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };
		const followers = getFieldValue('followers') || [defaultMinFollowers, defaultMaxFollowers];
		const categoriesArray = getFieldValue('categories') || [];
		const categoriesSelected = categories.filter((category) => categoriesArray.includes(category.index));
		return (
			<Form className='influencer-filter-form horizontal-text-form filter discover-filter-form-container'>
				<Collapse onChange={this.callback} defaultActiveKey={['1', '2']} className='ant-collapse-naked'>
					<Panel header='Influencers' key='1'>
						<FormItem {...formItemLayout} label='I want influencers in category'>
							{getFieldDecorator('categories', {
								initialValue: []
							})(
								<Select
									mode='multiple'
									disabled={false}
									optionFilterProp='name'
									placeholder='All categories'
									className='custom-select bordered rippledown discover-multiple-select'
									notFoundContent={'Could not load categories'}
								>
									{Object.values(categories).map((category) => (
										<Option key={category.index} value={category.index}>
											{category.name}
										</Option>
									))}
								</Select>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label='Who live in'>
							{getFieldDecorator('countries', {
								initialValue: defaultCountries.map(({ id }) => id)
							})(
								<Select
									mode='multiple'
									disabled={false}
									optionFilterProp='name'
									placeholder='All countries'
									className='custom-select bordered rippledown discover-multiple-select'
								>
									{countryOptions}
								</Select>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label='With followers between'>
							{getFieldDecorator('followers', {
								initialValue: [defaultMinFollowers, defaultMaxFollowers]
							})(
								<Slider
									range
									min={defaultMinFollowers}
									max={defaultMaxFollowers}
									step={sliderStepFollowers}
									className='fl mt-5 mr-30'
									style={{ width: '300px' }}
									disabled={false}
								/>
							)}
							{<div>{[followers[0].toShort(), followers[1].toShort()].join(' - ')}</div>}
						</FormItem>
					</Panel>
					<Panel header='Engaged Audiences' key='2'>
						<FormItem {...formItemLayout} label='I want to target'>
							{getFieldDecorator('audienceGender', {
								initialValue: 'all_genders'
							})(
								<Radio.Group buttonStyle='solid' size='large' disabled={false}>
									<Radio.Button value='male'>
										<Icon type='man' /> Males
									</Radio.Button>
									<Radio.Button value='female'>
										<Icon type='woman' /> Females
									</Radio.Button>
									<Radio.Button value='all_genders'>All</Radio.Button>
								</Radio.Group>
							)}
						</FormItem>
						<Row className='d-flex align-items-center'>
							<Col span={8}>
								<p className='caption'>That like</p>
							</Col>
							{true && (
								<Col span={16}>
									<h3>{categoriesSelected.length > 0 ? categoriesSelected.map((category) => category.name).join(' + ') : null}</h3>
									<span style={{ color: '#bfbfbf' }}>(Same as influencer category)</span>
								</Col>
							)}
						</Row>
						<FormItem {...formItemLayout} label='Who live in'>
							{getFieldDecorator('audienceCountries', {
								initialValue: defaultCountries.map(({ id }) => id)
							})(
								<Select
									mode='multiple'
									disabled={false}
									id='regionFilter'
									optionFilterProp='name'
									placeholder='All countries'
									className='custom-select bordered rippledown discover-multiple-select'
								>
									{countryOptions}
								</Select>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label='In age span'>
							{getFieldDecorator('audienceAgeGroups', {
								initialValue: []
							})(
								<Select
									mode='multiple'
									disabled={false}
									placeholder='All ages'
									optionFilterProp='name'
									className='custom-select bordered rippledown discover-multiple-select'
									filterOption={(input, option) => parseInt(input, 10).between(option.props.min, option.props.max + 1)}
								>
									<Option min={13} max={17} value='13-17'>
										13 - 17
									</Option>
									<Option min={17} max={24} value='18-24'>
										18 - 24
									</Option>
									<Option min={25} max={34} value='25-34'>
										25 - 34
									</Option>
									<Option min={35} max={44} value='35-44'>
										35 - 44
									</Option>
									<Option min={45} max={54} value='45-54'>
										45 - 54
									</Option>
									<Option min={56} max={64} value='55-64'>
										55 - 64
									</Option>
									<Option min={65} max={10000} value='65-+'>
										65-+
									</Option>
								</Select>
							)}
						</FormItem>
					</Panel>
				</Collapse>
				<div className='filter-footer'>
					<Button disabled={false} type='gray' className='mr-20' onClick={this.handleReset}>
						Reset
					</Button>
					<Button disabled={false} type='primary' loading={false} onClick={this.props.handleFilter}>
						Save
					</Button>
				</div>
			</Form>
		);
	}
}

export default Form.create()(DiscoverFilterForm);
