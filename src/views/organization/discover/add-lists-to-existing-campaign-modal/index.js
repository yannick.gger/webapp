import React from 'react';
import { Modal } from 'antd';

import AddListsToExistingForm from '../add-lists-to-existing-campaign-form';

class AddListsToExistingModal extends React.Component {
	handleCancel = () => {
		this.props.history.goBack();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		const { match } = this.props;
		return (
			<Modal
				className='add-selection-modal'
				title='Add list to existing campaign'
				visible
				onOk={this.handleOk}
				onCancel={this.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
			>
				<AddListsToExistingForm organizationSlug={match.params.organizationSlug} />
			</Modal>
		);
	}
}

export default AddListsToExistingModal;
