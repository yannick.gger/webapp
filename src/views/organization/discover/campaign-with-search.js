import React from 'react';
import { Form, Table, Spin, Input, Button } from 'antd';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { Query } from 'react-apollo';
import getCampaigns from 'graphql/get-campaigns.graphql';
import { firstOrganization } from 'services/auth-service';

class CampaignsWithSearchBar extends React.Component {
	state = {
		search: undefined,
		inputValue: ''
	};

	handleSearch = (value) => {
		this.setState({ search: value, inputValue: '' });
	};

	handleChangeSearchValue = (event) => {
		event.preventDefault();
		this.setState({
			inputValue: event.target.value
		});
	};

	loadMore = (fetchMore, cursor) => {
		return fetchMore({
			variables: {
				after: cursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					campaigns: {
						...fetchMoreResult.campaigns,
						edges: [...prev.campaigns.edges, ...fetchMoreResult.campaigns.edges]
					}
				};
				return result;
			}
		});
	};

	render() {
		const { columns } = this.props;
		const { id: organizationId } = firstOrganization();
		const { search, inputValue } = this.state;
		const { Search } = Input;
		return (
			<section>
				<div className='pb-25 pt-20'>
					<Search
						value={inputValue}
						onChange={this.handleChangeSearchValue}
						placeholder='Search campaign name'
						onSearch={(value) => this.handleSearch(value)}
					/>
				</div>
				<Query query={getCampaigns} variables={{ search, includeCanAddInfluencerCampaignsOnly: true, organizationId }} notifyOnNetworkStatusChange>
					{({ data, loading: queryLoading, fetchMore }) => {
						if (queryLoading && !data.campaigns) {
							return <Spin className='collabspin' />;
						}
						const campaigns = data.campaigns.edges;
						const cursor = data.campaigns.pageInfo.endCursor;
						const { hasNextPage } = data.campaigns.pageInfo;
						return (
							<React.Fragment>
								<Table className='add-selection-table' rowKey='id' columns={columns} dataSource={campaigns} pagination={false} loading={queryLoading} />
								<div className='pt-30 pb-30 text-center'>
									{hasNextPage && (
										<Button type='primary' loading={queryLoading} onClick={() => this.loadMore(fetchMore, cursor)}>
											Load more
										</Button>
									)}
								</div>
							</React.Fragment>
						);
					}}
				</Query>
			</section>
		);
	}
}

const WrappedCampaignsWithSearchBar = Form.create()(CampaignsWithSearchBar);

export default withRouter(injectIntl(WrappedCampaignsWithSearchBar));
