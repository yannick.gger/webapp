import { Button, Avatar, Modal, Table, Spin, Input, Select, Tag, Row, Col, Tooltip, Comment } from 'antd';
import React, { Component } from 'react';
import { Query } from 'react-apollo';
// import getAssignedClientsOfSaler from "graphql/get-assigned-clients-of-saler.graphql"
import getListComments from 'graphql/get-list-comments.graphql';
import getListCommentsUsers from 'graphql/get-list-comments-users.graphql';
import moment from 'moment';
import { t } from 'i18next';

const Search = Input.Search;
const { Option } = Select;

class ProfileCommentModal extends Component {
	state = {
		commentType: 'all_profile',
		search: '',
		actionBy: 'all_actions',
		filterUser: 0
	};

	handleCancel = () => {
		const { organizationSlug, id } = this.props.match.params;

		this.props.history.push(`/${organizationSlug}/discover/list/${id}`);
		window.location.reload();
	};

	handleChangeCommentType = (value) => {
		this.setState({ commentType: value });
	};

	handleChangeUsername = (value) => {
		this.setState({ filterUser: value });
	};

	searchHandler = (value) => {
		this.setState({ search: value });
	};

	loadMore = (comments, fetchMore, endCursor) => {
		return fetchMore({
			variables: {
				after: endCursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					listComments: {
						...fetchMoreResult.listComments,
						edges: [...prev.listComments.edges, ...fetchMoreResult.listComments.edges]
					}
				};
				return result;
			}
		});
	};

	handleChangeActionBy = (value) => {
		this.setState({ actionBy: value });
	};

	render() {
		const { commentType, search, filterClient, actionBy, filterUser } = this.state;
		const { id } = this.props.match.params;
		const xs = window.matchMedia('(max-width: 575px)');

		const columns = [
			{
				title: '',
				dataIndex: 'node.instagramOwner.instagramOwnerUser.avatar',
				key: 'avatar',
				width: xs.matches ? '75px' : '10%',
				render: (record) => <Avatar size='large' src={record} icon='user' />
			},
			{
				title: t('name', { defaultValue: `Name` }),
				dataIndex: 'node.instagramOwner.instagramOwnerUser.username',
				key: 'name',
				width: xs.matches ? '110px' : '15%'
			},
			{
				title: t('organizationName', { defaultValue: `Organization name` }),
				dataIndex: 'node.organizationName',
				key: 'organizationName',
				width: xs.matches ? '110px' : '15%'
			},
			{
				title: t('username', { defaultValue: `username` }),
				dataIndex: 'node.userName',
				key: 'username',
				width: xs.matches ? '110px' : '15%'
			},
			{
				title: t('commentType', { defaultValue: `Comment type` }),
				dataIndex: 'node.commentType',
				key: 'commentType',
				width: xs.matches ? '210px' : '23%',
				render: (record) => (
					<Tag color={record === 'add_profile_comment' ? 'green' : record === 'remove_profile_comment' ? 'red' : 'blue'} style={{ textTransform: 'uppercase' }}>
						{record.replaceAll('_', ' ')}
					</Tag>
				)
			},
			{
				title: t('actionBy', { defaultValue: `Action by` }),
				dataIndex: 'node.actionBy',
				key: 'actionBy',
				width: xs.matches ? '100px' : '12%',
				render: (record) =>
					record && (
						<Tag color={record === 'admin' ? 'pink' : record === 'sale' ? 'orange' : 'yellow'} style={{ textTransform: 'uppercase' }}>
							{record}
						</Tag>
					)
			},
			{
				title: t('comment', { defaultValue: `Comment` }),
				key: 'comment',
				width: xs.matches ? '220px' : '25%',
				render: (record) => (
					<Comment
						content={<Tooltip title={record.node.comment}>{<p className='truncated'>{record.node.comment}</p>}</Tooltip>}
						datetime={<span>{moment(record.node.createdAt).fromNow()}</span>}
					/>
				)
			}
		];

		const commentQueryVars = {
			myListId: id,
			commentType,
			search,
			actionBy,
			filterUser,
			organizationSlug: this.props.match.params.organizationSlug
		};

		const usersQueryVars = {
			myListId: id,
			notShareYet: false,
			organizationSlug: this.props.match.params.organizationSlug
		};

		return (
			<Modal
				title='Profile comment for list'
				visible
				onCancel={this.handleCancel}
				wrapClassName='custom-modal box-parts title-center'
				maskClosable={false}
				footer={null}
				className='profile-comment-modal'
			>
				<Row type='flex' justify='space-between' style={{ marginBottom: '20px' }} className='profile-comments-filter'>
					<Col md={4} style={{ paddingRight: '13px' }} className='mb-20'>
						<Select size='large' style={{ width: '100%' }} defaultValue={commentType} onChange={this.handleChangeCommentType}>
							<Option value='all_profile'>{t('organization:allProfiles', { defaultValue: `All profiles` })}</Option>
							<Option value='add_profile_comment'>{t('organization:added', { defaultValue: `Added` })}</Option>
							<Option value='remove_profile_comment'>{t('organization:removed', { defaultValue: `Removed` })}</Option>
							<Option value='profile_comment'>{t('organization:internalComments', { defaultValue: `Internal comments` })}</Option>
						</Select>
					</Col>
					<Col md={4} style={{ paddingRight: '13px' }} className='mb-20'>
						<Select size='large' style={{ width: '100%' }} defaultValue={actionBy} onChange={this.handleChangeActionBy}>
							<Option value='all_actions'>{t('organization:allActions', { defaultValue: `All actions` })}</Option>
							<Option value='admin'>{t('organization:admin', { defaultValue: `Admin` })}</Option>
							<Option value='sale'>{t('organization:sales', { defaultValue: `Sales` })}</Option>
							<Option value='client'>{t('organization:clients', { defaultValue: `Clients` })}</Option>
						</Select>
					</Col>
					<Col md={4} style={{ paddingRight: '13px' }} className='mb-20'>
						<Query query={getListCommentsUsers} variables={usersQueryVars} fetchPolicy='cache-and-network'>
							{({ data: userData, loading: usersLoading }) => {
								if (usersLoading) {
									return (
										<Select size='large' style={{ width: '100%' }} defaultValue={filterUser} onChange={this.handleChangeUsername}>
											<Option value={0}>{t('organization:allUsers', { defaultValue: `All users` })}</Option>
										</Select>
									);
								}

								return (
									<Select size='large' style={{ width: '100%' }} defaultValue={filterUser} onChange={this.handleChangeUsername}>
										<Option value={0}>{t('organization:allUsers', { defaultValue: `All users` })}</Option>
										{userData.listCommentsUsers.edges.map((user) => (
											<Option value={user.node.id}>{user.node.name}</Option>
										))}
									</Select>
								);
							}}
						</Query>
					</Col>
					<Col md={12}>
						<Search
							size='large'
							placeholder={t('organization:searchInstagramName', { defaultValue: `Search Instagram name` })}
							onSearch={this.searchHandler}
							enterButton
						/>
					</Col>
				</Row>
				<Query query={getListComments} variables={commentQueryVars} fetchPolicy='cache-and-network'>
					{({ data: commentData, loading: commentLoading, fetchMore, refetch }) => {
						this.refetch = refetch;

						if (commentLoading) {
							return <Spin className='collabspin' />;
						}

						const comments = commentData && commentData.listComments ? commentData.listComments.edges : [];
						const hasNextPage = commentData && commentData.listComments && commentData.listComments.pageInfo.hasNextPage;
						const endCursor = commentData && commentData.listComments ? commentData.listComments.pageInfo.endCursor : null;

						return (
							<div className='profile-comment-for-list'>
								<Table columns={columns} dataSource={comments} pagination={false} loading={commentLoading} className='pb-30' size='middle' />
								{hasNextPage && (
									<div className='pb-30 text-center'>
										<Button type='primary' loading={commentLoading} onClick={() => this.loadMore(comments, fetchMore, endCursor)}>
											{t('loadMore', { defaultValue: `Load more` })}
										</Button>
									</div>
								)}
							</div>
						);
					}}
				</Query>
			</Modal>
		);
	}
}

export default ProfileCommentModal;
