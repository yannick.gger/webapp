import React, { Component } from 'react';
import { Query } from 'react-apollo';
import ListsView from './lists';
import { Layout, Dropdown, Menu, Button, Spin } from 'antd';
import { withOrganization } from 'hocs';
import listIndex from 'graphql/get-discover-lists.graphql';
import OrganizationLink from 'components/organization-link/';
import NoList from '../my-lists/no-lists.js';
import GhostUserAccess from 'components/ghost-user-access';
import { isAdmin, isSaler } from 'services/auth-service';

const MenuItemGroup = Menu.ItemGroup;
const menu = (
	<Menu>
		<MenuItemGroup title='Create new list'>
			<Menu.Item key='0'>
				<OrganizationLink to='/discover/lists/create-new-list'>Create new list</OrganizationLink>
			</Menu.Item>
		</MenuItemGroup>
	</Menu>
);

class MyListsIndex extends Component {
	render() {
		const { slug, organization, sharedListsOnly, sharedToClients } = this.props;
		return (
			<div>
				<Query
					query={listIndex}
					variables={{
						organizationSlug: slug,
						sharedListsOnly: sharedListsOnly,
						privateListsOnly: !sharedListsOnly && !sharedToClients,
						sharedToClients: sharedToClients
					}}
					fetchPolicy='cache-and-network'
					notifyOnNetworkStatusChange
				>
					{({ data, error, fetchMore, loading: queryLoading }) => {
						if (queryLoading && !data.myLists) {
							return <Spin className='collabspin' />;
						}
						const myLists = data && data.myLists ? data.myLists.edges.map((x) => x.node) : [];
						const cursor = data && data.myLists ? data.myLists.pageInfo.endCursor : null;

						if (myLists.length > 0) {
							return (
								<React.Fragment>
									<ListsView
										fetchMore={fetchMore}
										error={error}
										organization={organization}
										myLists={myLists}
										cursor={cursor}
										loading={queryLoading}
										hasNextPage={false}
										slug={slug}
										sharedToClients={sharedToClients}
									/>
								</React.Fragment>
							);
						} else {
							return <NoList />;
						}
					}}
				</Query>
			</div>
		);
	}
}

class ListsIndexView extends Component {
	constructor(props) {
		super(props);
		this.state = { currentMenuKey: 'my-lists' };
	}

	changeCurrentMenu = ({ key }) => this.setState({ currentMenuKey: key });

	render() {
		const { organization, match } = this.props;
		const { currentMenuKey } = this.state;

		const menuItems = [
			{
				key: 'my-lists',
				text: 'My lists'
			},
			{
				key: 'shared-lists',
				text: 'Shared with me'
			},
			(isAdmin() || isSaler()) && {
				key: 'shared-to-clients-lists',
				text: 'Shared to clients'
			}
		];

		return (
			<Layout>
				<Layout.Header>
					<h1>Lists</h1>
					<GhostUserAccess>
						<Dropdown overlay={menu} trigger={['click']} placement='bottomRight'>
							<Button type='primary' icon='ion ion-plus' size='large' />
						</Dropdown>
					</GhostUserAccess>
				</Layout.Header>
				<Layout.Content>
					{(organization.hasTeamMember || organization.hasListSharedBySaler) && (
						<div className='campaign-nav no-slide-animation ant-tabs-spacing-fix full-width nav-pills no-link'>
							<Menu className='d-flex' mode='horizontal' selectedKeys={[currentMenuKey]} onClick={this.changeCurrentMenu}>
								{menuItems.map(({ key, text }) => {
									return <Menu.Item key={key}>{text}</Menu.Item>;
								})}
							</Menu>
						</div>
					)}

					<MyListsIndex
						slug={match.params.organizationSlug}
						organization={organization}
						sharedListsOnly={currentMenuKey === 'shared-lists'}
						sharedToClients={currentMenuKey === 'shared-to-clients-lists'}
					/>
				</Layout.Content>
			</Layout>
		);
	}
}

export default withOrganization(ListsIndexView);
