import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import { withRouter } from 'react-router-dom';
import { Mutation } from 'react-apollo';
import { lookup } from 'country-data';
import { Button, Divider, Icon, Table, Avatar, Popover, Progress, Empty, message } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';
import { MaleIcon, FemaleIcon } from 'components/icons';
import CampaignPicker from './campaigns-picker';
import TableErrorImg from 'assets/img/app/table-error.svg';
import OrganizationLink from 'components/organization-link';
import removeLists from 'graphql/remove-lists.graphql';
import getOrganizationLists from 'graphql/get-organization-my-lists.graphql';
import { isSaler, isTester } from 'services/auth-service';
import '../styles.scss';

class MyLists extends Component {
	state = {
		selectedRowKeys: [],
		selectedLists: [],
		visibleModal: false
	};

	onSelectChange = (myLists) => {
		return (selectedRowKeys) => {
			this.setState((prevState) => {
				const previousSelectedLists = prevState.selectedLists;
				const newSelectedLists = selectedRowKeys
					.filter((key) => !prevState.selectedLists.some(({ id }) => id === key))
					.map((key) => myLists.find((i) => i.id === key));
				let resSelectedLists = [...newSelectedLists, ...previousSelectedLists].filter(({ id }) => selectedRowKeys.some((key) => key === id));

				return {
					selectedRowKeys,
					selectedLists: resSelectedLists
				};
			});
		};
	};

	loadMore = (myLists, fetchMore) => {
		return fetchMore({
			variables: {
				after: this.props.cursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					myLists: {
						...fetchMoreResult.myLists,
						edges: [...prev.myLists.edges, ...fetchMoreResult.myLists.edges]
					}
				};
				return result;
			}
		});
	};

	showModal = () => {
		this.setState({
			visibleModal: true
		});
	};

	handleDropdown = () => {
		this.setState({
			visibleButton: false
		});
	};

	goToList = (event) => {
		event.preventDefault();
		const { history, slug } = this.props;
		const listId = event.target.parentNode.getAttribute('data-row-key');
		const linkToList = `/${slug}/discover/list/${listId}`;
		if (listId) history.push(linkToList);
	};

	render() {
		const rowSelection = (myLists) => ({
			selectedRowKeys,
			onChange: this.onSelectChange(myLists)
		});
		const { error, myLists, organization, fetchMore, loading, networkStatus, hasNextPage, slug, sharedToClients } = this.props;
		const { selectedRowKeys, selectedLists } = this.state;
		const hasSelected = selectedRowKeys.length > 0;
		const columns = [
			{
				title: '',
				key: 'userAvatar',
				width: '60px',
				render: (record) => <Avatar size='large' src='#' icon='user' />
			},
			{
				title: 'Name',
				key: 'name',
				width: '180px',
				render: (record) => (
					<div className='list-name' href='#' target='_blank' rel='noopener noreferrer'>
						{record.name}
					</div>
				)
			},
			{
				title: 'Organization',
				key: 'organization',
				width: '180px',
				render: (record) => (
					<div className='list-name' href='#' target='_blank' rel='noopener noreferrer'>
						{record.organizationName}
					</div>
				)
			},
			{
				title: 'Followers',
				key: 'followers',
				render: (record) => (
					<div className='number-box gray'>
						<ShortAmountFormatter value={record.instagramOwnersAggregation.followedByCount} />
					</div>
				)
			},
			{
				title: 'Engagement',
				key: 'engagement',
				render: (record) => (
					<span className='number-box green' style={{ width: '66px' }}>
						{`${record.instagramOwnersAggregation.interactionRate.toFixed(2)}%`}
					</span>
				)
			},
			{
				title: 'Added to list',
				key: 'addedList',
				render: (record) => (
					<div className='number-box light-blue'>
						<span style={{ color: '#026bfa', paddingRight: '4px' }}>{record.influencersCount}</span>
						<span style={{ color: '#8f98ad' }}>{record.influencersCount > 1 ? 'Influencers' : 'Influencer'}</span>
					</div>
				)
			},
			{
				title: 'Used in',
				key: 'usedIn',
				render: (record) => (
					<div className='number-box pink'>
						<span style={{ color: '#d2a838', paddingRight: '4px' }}>{record.usedInCampaigns}</span>
						<span style={{ color: '#b9a59e' }}>{record.usedInCampaigns > 1 ? 'Campaigns' : 'Campaign'}</span>
					</div>
				)
			},
			{
				title: '',
				key: 'linkToList',
				render: (record) => (
					<OrganizationLink to={`/discover/list/${record.id}`}>
						<div className='d-flex justify-content-end'>
							<Icon type='right' style={{ color: '#157ff1' }} />
						</div>
					</OrganizationLink>
				)
			}
		].filter((col) => (!sharedToClients ? col.key !== 'organization' : col));
		return (
			<div className='discover-wrapper my-list-wrapper'>
				<CampaignPicker
					disabled={!hasSelected}
					handleSuccess={() => {
						this.setState({ selectedRowKeys: [], selectedLists: [] });
					}}
					organization={organization}
					myLists={selectedRowKeys.map((key) => ({
						id: key
					}))}
					lists={selectedLists}
				>
					{({ mutationLoading }) => (
						<Button type='primary' className='mb-30' loading={loading || mutationLoading}>
							Add selected to... <Icon type='down' />
						</Button>
					)}
				</CampaignPicker>
				<Mutation
					mutation={removeLists}
					refetchQueries={['listIndex', { query: getOrganizationLists, variables: { slug } }]}
					onCompleted={() => {
						this.setState({ selectedRowKeys: [], selectedInstagramOwners: [] });
						message.success(
							this.props.intl.formatMessage({
								id: 'remove-lists-success',
								defaultMessage: `Removed ${selectedRowKeys.length} ${selectedRowKeys.length > 1 ? 'lists' : 'list'}`
							})
						);
					}}
				>
					{(removeLists) => (
						<Button
							style={{ marginLeft: '20px' }}
							disabled={!hasSelected}
							onClick={() => {
								removeLists({
									variables: { myListIds: selectedRowKeys.map((key) => key) }
								});
							}}
						>
							<Icon type='delete' theme='filled' className={hasSelected ? 'red-trash' : ''} />
							Remove
						</Button>
					)}
				</Mutation>
				<span style={{ marginLeft: 20 }}>{hasSelected ? `${selectedRowKeys.length} ${selectedRowKeys.length > 1 ? 'lists' : 'list'} selected` : ''}</span>
				<div onClick={(event) => this.goToList(event)}>
					<Table
						className='pb-30'
						columns={columns}
						pagination={false}
						rowKey='id'
						rowSelection={rowSelection(myLists)}
						dataSource={myLists}
						scroll={{ x: 1000 }}
						loading={loading}
						locale={{
							emptyText: error ? (
								<Empty image={TableErrorImg} description='Could not load users' />
							) : (
								<Empty description='Nothing matching the search criterias found' />
							)
						}}
					/>
				</div>

				{!error && hasNextPage && (
					<div className='pb-30 text-center'>
						<Button type='primary' loading={loading} onClick={() => this.loadMore(myLists, fetchMore)}>
							Load more
						</Button>
					</div>
				)}
			</div>
		);
	}
}

export default withRouter(injectIntl(MyLists));
