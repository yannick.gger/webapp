export interface Lists {
	data: {
		type: string;
		id: string;
		attributes: {
			name: string;
			createdAt: string;
		};
	};
	included: Included[];
}

export interface Included {
	type: string;
	id: string;
	attributes: {
		collabsId: string;
		username: string;
	};
	links: {
		profilePictureUrl: string;
		delete: string;
	};
}

export interface ListContent {
	lists: Lists;
}

export interface InfluencerAvatar {
	name: string;
	image: string;
}

export interface SubFolders {
	listResponse: any;
	subFolders: any;
	subLists: any;
	openFolder: string;
	updateParentFolder: (parentFolderId: string, newFolderId: string) => void;
	updateParentList: (listId: string) => void;
}
