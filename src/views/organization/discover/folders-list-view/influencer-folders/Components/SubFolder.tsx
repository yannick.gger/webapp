import Avatar from 'components/Avatar';
import Icon from 'components/Icon';
import * as S from '../InfluencerFolders.style';
import * as I from './types';

const SubFolders = (props: I.SubFolders) => {
	const handleFolderClick = (newFolderId: string) => {
		props.updateParentFolder(props.openFolder, newFolderId);
	};
	const handleListClick = (targetListId: string) => {
		props.updateParentList(targetListId);
	};

	return (
		<>
			{props.subFolders.length >= 1 &&
				props.subFolders.map((subFolder: any, i: number) => {
					const name = subFolder.attributes.name;
					const id = subFolder.id;
					return (
						<S.SubWrapper key={name + i} onClick={() => handleFolderClick(id)}>
							<S.FolderList>
								<div>
									<Icon icon='add-new-folder' size='24' /> {name}
								</div>
								<Icon icon='chevron-right' size='24' />
							</S.FolderList>
						</S.SubWrapper>
					);
				})}
			{props.subLists.map((subList: any, i: number) => {
				const name = subList.attributes.name;
				const id = subList.id;
				const influencers = subList.relationships.items.data;

				return (
					<S.SubWrapper key={name + i} onClick={() => handleListClick(id)}>
						<S.FolderList>
							<div>
								<Icon icon='my-lists' size='24' /> {name} ({influencers.length})
								{influencers.map((user: any, i: number) => {
									if (i <= 4) {
										const influencerObject = props.listResponse.included.filter((influencer: { id: string }) => user.id === influencer.id);
										if (influencerObject[i]) {
											const influencerName = influencerObject[i].attributes.username;
											const influencerImg = influencerObject[i].links.profilePictureUrl;
											return <Avatar key={influencerName + i} name={influencerName} /* uncomment when we have real imageUrls: imageUrl={influencerImg} */ />;
										}
									} else return;
								})}
							</div>
							<Icon icon='chevron-right' size='24' />
						</S.FolderList>
					</S.SubWrapper>
				);
			})}
		</>
	);
};

export default SubFolders;
