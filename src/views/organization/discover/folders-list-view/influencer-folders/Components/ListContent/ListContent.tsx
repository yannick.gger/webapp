import axios, { AxiosResponse } from 'axios';
import Avatar from 'components/Avatar';
import Checkbox from 'components/Checkbox';
import Dropdown from 'components/Dropdown';
import Icon from 'components/Icon';
import LoadingSpinner from 'components/LoadingSpinner';
import { ToastContext } from 'contexts';
import { JsonApiDocument } from 'json-api-models';
import React, { useContext, useEffect, useState } from 'react';
import { createClient } from 'shared/ApiClient/ApiClient';
import { nFormatter } from 'shared/utils/numbers';
import { ItemAttributes } from 'types/http';
import Styled from './ListContent.style';
import { IListContent, ListItem } from './types';

/**
 * ListContent
 * @todo i18next
 * @param {IListContent} props
 * @returns {JSX.Element}
 */
const ListContent = (props: IListContent): JSX.Element => {
	const [selectedRows, setSelectedRows] = useState<Array<string>>([]);
	const [listItems, setListItems] = useState<Array<ItemAttributes<ListItem>>>([]);
	const [loading, setLoading] = useState<Boolean>(false);
	const { addToast } = useContext(ToastContext);
	const client = createClient();

	const handleSelectAllRows = () => {
		if (selectedRows.length === listItems.length) {
			setSelectedRows([]);
		} else {
			setSelectedRows(listItems.map((x: ItemAttributes<ListItem>) => x.attributes.collabsId));
		}
	};

	const handleSelectRow = () => {
		return (id: string) => {
			setSelectedRows((prev) => {
				const isExist = prev.some((prevId) => prevId === id);
				if (isExist) {
					return prev.filter((prevId) => prevId !== id);
				} else {
					return prev.concat(id);
				}
			});
		};
	};

	const handleOnClickDelete = () => {
		let promises: Promise<AxiosResponse<JsonApiDocument, any>>[] = [];
		const filteredItems = listItems.filter((item) => {
			return selectedRows.includes(item.attributes.collabsId);
		});

		filteredItems.map((item) => {
			if (item.links && item.links.delete) {
				promises = [...promises, client.delete<JsonApiDocument>(item.links.delete)];
			}
		});

		if (promises.length > 0) {
			setLoading(true);
			Promise.all(promises)
				.then(() => {
					setListItems(
						listItems.filter((item) => {
							return !selectedRows.includes(item.attributes.collabsId);
						})
					);

					setSelectedRows([]);
					addToast({ id: 'lc-success', mode: 'success', message: `Deleted ${selectedRows} items!` });
				})
				.catch((e) => {
					if (!axios.isCancel(e)) {
						addToast({ id: 'lc-error', mode: 'error', message: 'Something went wrong!' });
						console.error('Unable to delete rows: %O', e);
					}
				})
				.finally(() => setLoading(false));
		}
	};

	useEffect(() => {
		setListItems(props.listItems);
	}, []);

	const handleOnClickBack = (e: React.MouseEvent<HTMLAnchorElement>) => {
		e.preventDefault();
		props.onClickBack && props.onClickBack();
	};

	return (
		<>
			<Styled.BackLink onClick={handleOnClickBack}>
				<Icon icon='chevron-left' size='32' /> {props.listName}
			</Styled.BackLink>
			{!loading && listItems.length > 0 && (
				<Styled.ListTable>
					<Styled.ListTableHead>
						<tr>
							<Styled.ListTableTh>
								<div className='d-flex'>
									<Checkbox onChange={() => handleSelectAllRows()} checked={selectedRows.length > 0} />
									<Styled.CustomDropdown icon='chevron-down' position='left' disabled={selectedRows.length === 0} size='16'>
										<Dropdown.Menu>
											<Dropdown.Item>
												<a onClick={() => handleOnClickDelete()}>Delete</a>
											</Dropdown.Item>
										</Dropdown.Menu>
									</Styled.CustomDropdown>
								</div>
							</Styled.ListTableTh>
							<Styled.ListTableTh />
						</tr>
					</Styled.ListTableHead>
					<Styled.ListTableBody>
						{listItems.map((item: ItemAttributes<ListItem>, i: number) => {
							return (
								<Styled.ListTableTr key={i + item.attributes.username}>
									<Styled.ListTableTd width={30}>
										<Checkbox
											onChange={() => handleSelectRow()(item.attributes.collabsId)}
											checked={selectedRows.some((x) => x === item.attributes.collabsId)}
										/>
									</Styled.ListTableTd>
									<Styled.ListTableTd>
										<Styled.InfluencerWrapper>
											<Avatar name={item.attributes.username} imageUrl={''} />
											<Styled.InfluencerData>
												<Styled.Username>
													<span>{item.attributes.username}</span>
												</Styled.Username>
												<Styled.Followers>
													<span>{nFormatter(item.attributes.followersCount, 1)} followers</span>
												</Styled.Followers>
											</Styled.InfluencerData>
										</Styled.InfluencerWrapper>
									</Styled.ListTableTd>
								</Styled.ListTableTr>
							);
						})}
					</Styled.ListTableBody>
				</Styled.ListTable>
			)}
			{listItems.length === 0 && <h2 className='text-center'>There is no influencers in this list yet!</h2>}
			{loading && <LoadingSpinner size='lg' position='center' />}
		</>
	);
};

export default ListContent;
