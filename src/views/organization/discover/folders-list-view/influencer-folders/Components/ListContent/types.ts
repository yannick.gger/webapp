import { CollectionResponse, ItemAttributes } from 'types/http';

export type ListItem = {
	collabsId: string;
	username: string;
	followersCount: number;
};

export type List = {
	createdAt: Date;
	name: string;
};

export interface IListContent {
	listItems: Array<ItemAttributes<ListItem>>;
	listName: string;
	onClickBack: () => void;
}
