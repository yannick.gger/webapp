import { render, screen } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';

import themes from 'styles/themes';
import { ItemAttributes } from 'types/http';

import ListContent from './ListContent';
import { ListItem } from './types';

const mockedLists: Array<ItemAttributes<ListItem>> = [
	{
		type: 'listItem',
		id: 'd6c8d027-73ef-48d9-9319-160f8145d042',
		attributes: {
			collabsId: 'bdc10219-96d4-4cfe-af08-385bcce5b770',
			username: 'kalle_anka',
			followersCount: 0
		},
		links: {
			profilePictureUrl: 'https...',
			delete: '/lists/9b2b065c-2264-4fad-88f7-ac3d684e7ced/items/d6c8d027-73ef-48d9-9319-160f8145d042'
		}
	}
];

test('Matches the snapshot', async () => {
	const { asFragment } = render(
		<ThemeProvider theme={themes.default}>
			<ListContent listItems={mockedLists} listName='My Listname' onClickBack={() => {}} />
		</ThemeProvider>
	);

	await screen.findByText('kalle_anka');
	await screen.findByText('My Listname');
	await screen.findByText('0 followers');

	expect(asFragment()).toMatchSnapshot();
});
