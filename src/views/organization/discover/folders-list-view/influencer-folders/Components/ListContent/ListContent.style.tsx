import Dropdown from 'components/Dropdown';
import styled from 'styled-components';
import { Table, TableHeader, TableBody, Th, Tr, Td } from 'styles/table';
import colors from 'styles/variables/colors';
import typography from 'styles/variables/typography';

const ListTable = styled.table`
	${Table};
`;

const ListTableHead = styled.thead`
	${TableHeader};
`;

const ListTableBody = styled.tbody`
	${TableBody};
`;

const ListTableTh = styled.th`
	${Th};
`;

const ListTableTr = styled.tr`
	${Tr};
`;

const ListTableTd = styled.td`
	${Td};
`;

const InfluencerWrapper = styled.div`
	display: flex;
	align-items: center;
	gap: 16px;

	.avatar {
		margin-left: 0;
	}
`;

const InfluencerData = styled.div`
	padding: 1.25rem 0.75rem;
`;

const Username = styled.div`
	font-weight: 700;
	font-size: 1.2rem;
`;

const Followers = styled.div`
	font-family: ${typography.SecondaryFontFamiliy};
	font-size: 0.875rem;
	white-space: nowrap;
`;

const BackLink = styled.a`
	display: flex;
	align-items: center;
	font-size: 1.5rem;
	color: ${colors.discovery.black};
	font-weight: 700;
`;

const CustomDropdown = styled(Dropdown)`
	display: flex;
	align-items: center;
	margin-left: 8px;

	& button {
		padding: 0;

		&:hover,
		&.show {
			border-radius: 2px;
			background-color: ${colors.discovery.iconHover};
		}
	}
`;

const Styled = {
	ListTable,
	ListTableHead,
	ListTableBody,
	ListTableTh,
	ListTableTr,
	ListTableTd,
	InfluencerWrapper,
	InfluencerData,
	Username,
	Followers,
	BackLink,
	CustomDropdown
};

export default Styled;
