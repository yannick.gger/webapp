import { useState, useEffect, useContext } from 'react';
import { Store } from 'json-api-models';
import Icon from 'components/Icon';
import ListsService from 'services/Lists/Lists.service';
import { ToastContext } from 'contexts';
import { IFolder, IFolders } from './types';
import { SubFolder, ListContent } from './Components';
import * as S from './InfluencerFolders.style';
import PageHeader from 'components/PageHeader';

/*
	Things to keep in mind:
	 - originalFolders = The root folders/lists (all the content on the left side of the view)
	 - subContent = the contents of the current selected folder

*/

const Folders = () => {
	const [originalFolders, setOriginalFolders] = useState<IFolders>({ folders: [], lists: [], isFetching: true });
	const [subContent, setSubContent] = useState<IFolders>({ folders: [], lists: [], isFetching: true });
	const [openList, setOpenList] = useState<any>({});
	const [editedFolderName, setEditedFolderName] = useState<string>('');
	const [openFolder, setOpenFolder] = useState<string>('');
	const [parentFolder, setParentFolder] = useState<string>('');
	const [folderName, setFolderName] = useState<string>('');
	const [listResponse, setListResponse] = useState<any>({});
	const [isRootFolder, setIsRootFolder] = useState<boolean>(false);
	const [showContentList, setShowContentList] = useState<boolean>(false);

	const { addToast } = useContext(ToastContext);

	const fetchOriginalFolders = async () => {
		try {
			setOriginalFolders({ ...originalFolders, isFetching: true });

			await ListsService.getFolders().then((foldersAndLists: any) => {
				// Save fetched FOLDER-data into an array which will later be saved into state
				// If folders have the 'included'-object exists, save it into state - otherwise just save the data object into the array
				const shallowOriginalFolders: any = foldersAndLists.folders.included
					? [
							...foldersAndLists.folders.included,
							// only save the folder into state if it doesn't have a parentfolder (ie. if it is in the root folder)
							...foldersAndLists.folders.data.filter((folder: any) => {
								return !folder.relationships.parent.data;
							})
					  ]
					: [
							// same as above
							...foldersAndLists.folders.data.filter((folder: any) => {
								return !folder.relationships.parent.data;
							})
					  ];
				// Save fetched LIST-data into an array which will later be saved into state
				const shallowOriginalLists: any = [
					...foldersAndLists.lists.data.filter((list: any) => {
						// only save lists which are in the root folder
						return list.type === 'list' && !list.relationships.folder.data;
					})
				];
				setIsRootFolder(true);
				setOriginalFolders({ folders: shallowOriginalFolders, lists: shallowOriginalLists, isFetching: false });
			});
		} catch (e) {
			console.log(e);
			setOriginalFolders({ ...originalFolders, isFetching: false });
		}
	};

	/* Fetch the root folders on mount */
	useEffect(() => {
		fetchOriginalFolders();
	}, []);

	/* Fetch content/sub folders as soon as a folder has been clicked */
	useEffect(() => {
		const models = new Store();
		const fetchSubFolders = async () => {
			try {
				setSubContent({ ...subContent, isFetching: true });
				const responseFolder: any = await ListsService.getFolder(openFolder);
				models.sync(responseFolder);
				const folder = models.find('folder', openFolder);
				const responseLists: any = await ListsService.getLists();

				setListResponse(responseLists);

				const subLists = responseLists.data.filter((subList: any) => {
					// only save folders which are sub folders (ie: only folders which have a parent folder)
					return subList.relationships.folder.data && subList.relationships.folder.data.id === openFolder;
				});
				// if folder has children: save/use those - if not: use subContent folders
				setSubContent({ folders: folder && folder.children ? folder.children : subContent.folders, lists: subLists, isFetching: false });
				// this is not really needed right now but will be of value when influencer
				return () => {
					setSubContent({ folders: subContent.folders, lists: subContent.lists, isFetching: true });
				};
			} catch (e) {
				console.log(e);
				setSubContent({ ...subContent, isFetching: false });
			}
		};
		fetchSubFolders();
	}, [openFolder]);

	/* Updates the current open list */
	const updateParentList = (listId: string) => {
		const fetchSubLists = async () => {
			try {
				setOpenList({ listName: '', lists: openList.lists, isFetching: true });

				// @todo type
				await ListsService.getList(listId).then((listData: any) => {
					setOpenList({ listName: listData.data.attributes.name, lists: listData.included.filter((x: any) => x.type === 'listItem'), isFetching: false });
					setShowContentList(true);
				});
			} catch (e) {
				console.log(e);
			}
		};
		fetchSubLists();
	};
	/* Updates the current root folder (either the "mounted" root folder or the current open folder) */
	const updateParentFolder = (parentFolderId: string, newFolderId: string) => {
		const fetchSubFolder = async () => {
			try {
				setOriginalFolders({ folders: originalFolders.folders, lists: originalFolders.lists, isFetching: true });
				await ListsService.getFolders().then((foldersAndLists: any) => {
					const shallowOriginalFolders: any = [
						...foldersAndLists.folders.included,
						...foldersAndLists.folders.data.filter((folder: any) => {
							return folder.relationships.parent.data && folder.relationships.parent.data.id === parentFolderId;
						})
					];
					const shallowOriginalLists: any = [
						...foldersAndLists.lists.data.filter((list: any) => {
							return list.type === 'list' && list.relationships.folder.data && list.relationships.folder.data.id === parentFolderId;
						})
					];
					setParentFolder(parentFolderId);
					setOpenFolder(newFolderId);
					setIsRootFolder(false);
					setOriginalFolders({ folders: shallowOriginalFolders, lists: shallowOriginalLists, isFetching: false });
				});
			} catch (e) {
				console.log(e);
				setOriginalFolders({ ...originalFolders, isFetching: false });
			}
		};
		fetchSubFolder();
	};
	const goBack = async () => {
		await ListsService.getFolder(parentFolder).then((folder: any) => {
			if (folder.data.relationships.parent && folder.data.relationships.parent.data) {
				const grandParent = folder.data.relationships.parent.data.id;
				updateParentFolder(grandParent, parentFolder);
			} else {
				console.log('root folder detected');
				fetchOriginalFolders();
				setOpenFolder('');
			}
		});
	};

	/* Save name/input text to state */
	const newFolder = (event: any) => {
		setFolderName(event.target.value);
	};

	/* Creates a new folder in the same path as current root/open folder */
	const createNewFolder = async () => {
		let parentIdOrOriginalFolder = parentFolder ? parentFolder : '';

		await ListsService.createFolder(parentIdOrOriginalFolder, folderName)
			.then((result) => {
				if (result.data) {
					const updatedFoldersList = [...originalFolders.folders, result.data.data];

					setOriginalFolders({ ...originalFolders, folders: updatedFoldersList, isFetching: false });
				} else {
					console.error(result.errors);
				}
			})
			.catch(() => {
				addToast({ id: 'pages-fb-ig-err', mode: 'error', message: 'Oops! Cannot create new group. Please try again.' });
			});
	};
	/* Creates a folder inside the selected root folder */
	const createNewSubFolder = async (id: string) => {
		await ListsService.createFolder(id, folderName)
			.then((result) => {
				if (result.data) {
					const updatedFoldersList = [...subContent.folders, result.data.data];
					setSubContent({ ...subContent, folders: updatedFoldersList, isFetching: false });
				} else {
					console.error(result.errors);
				}
			})
			.catch(() => {
				addToast({ id: 'pages-fb-ig-err', mode: 'error', message: 'Oops! Cannot create sub group. Please try again.' });
			});
	};
	/* Change name of folder */
	const editFolder = async (folderId: string, name: string) => {
		await ListsService.updateFolder(folderId, name)
			.then((result) => {
				if (result.data) {
					const shallowFolders = [...originalFolders.folders];

					let updatedFolder = shallowFolders.map((item) => {
						const returnValue = { ...item };
						if (item.id == folderId) {
							returnValue.attributes.name = name;
						}
						return returnValue;
					});

					setOriginalFolders({ ...originalFolders, folders: updatedFolder, isFetching: false });
				} else {
					console.error(result.errors);
				}
			})
			.catch(() => {
				addToast({ id: 'pages-fb-ig-err', mode: 'error', message: 'Oops! Cannot edit group. Please try again.' });
			});
	};
	const deleteFolder = async (folderId: string) => {
		await ListsService.deleteFolder(folderId)
			.then(() => {
				const shallowFolders = [
					...originalFolders.folders.filter((folder: { id: string }) => {
						return folder.id !== folderId;
					})
				];
				setOriginalFolders({ ...originalFolders, folders: shallowFolders, isFetching: false });
			})
			.catch(() => {
				addToast({ id: 'pages-fb-ig-err', mode: 'error', message: 'Oops! Cannot delete group. Please try again.' });
			});
	};
	/* Create new list inside current root folder */
	const createNewList = async () => {
		let parentIdOrOriginalFolder = openFolder ? openFolder : parentFolder ? parentFolder : '';
		await ListsService.createList(folderName, parentIdOrOriginalFolder)
			.then((result) => {
				if (result.data) {
					if (!openFolder) {
						const updatedRootLists = [...originalFolders.lists, result.data];
						setOriginalFolders({ ...originalFolders, lists: updatedRootLists, isFetching: false });
					} else {
						const updatedSubList = [...subContent.lists, result.data];
						setSubContent({ ...subContent, lists: updatedSubList, isFetching: false });
					}
				} else {
					console.error(result.errors);
				}
			})
			.catch(() => {
				addToast({ id: 'pages-fb-ig-err', mode: 'error', message: 'Oops! Cannot create new group. Please try again.' });
			});
	};

	const headerText = isRootFolder ? 'All lists' : '< Go back';

	const clearList = () => {
		setOpenList({ listName: '', lists: undefined, isFetching: false });
		setShowContentList(false);
	};

	return (
		<S.InfluencerList>
			<PageHeader headline={'My lists'} showBreadcrumb={false} showCurrentDate={false} />
			<div>
				<div onClick={goBack}>{headerText}</div>
				<input type='text' name='name' onChange={newFolder} />
				<S.ButtonContainer>
					{openFolder ? (
						<S.CustomButton onClick={() => createNewSubFolder(openFolder)}>
							<S.CustomIcon icon='add-new-folder' size='24' />
							Create folder
						</S.CustomButton>
					) : (
						<S.CustomButton onClick={createNewFolder}>
							<S.CustomIcon icon='add-new-folder' size='24' />
							Create folder
						</S.CustomButton>
					)}

					<S.CustomButton onClick={createNewList}>
						<S.CustomIcon icon='my-lists' size='24' />
						Create list
					</S.CustomButton>
				</S.ButtonContainer>
			</div>
			{originalFolders.folders.length >= 1 && (
				<>
					<S.Container>
						{!showContentList && (
							<>
								<div>
									{originalFolders.folders.map((folder: IFolder, i) => {
										return (
											<div key={folder.id + i}>
												{folder && (
													<S.Wrapper>
														<S.FolderList
															onClick={() => {
																setOpenFolder(folder.id);
															}}
															isOpen={openFolder === folder.id}
														>
															<div>
																<S.CustomIcon icon='add-new-folder' size='24' />
																<span>{folder.attributes.name}</span>
															</div>
															{/* <div onClick={() => deleteFolder(folder.id)}>delete folder</div> */}
															<Icon icon='chevron-right' size='24' />

															{/* 												<div>
														<input onChange={(e) => setEditedFolderName(e.target.value)} />
														<div onClick={() => editFolder(folder.id, editedFolderName)}>rename folder</div>
													</div>
													<div>
														<input type='text' name='name' onChange={newFolder} />
														<div onClick={() => createNewSubFolder(folder.id)}>+ new sub FOLDER</div>
													</div>
													<div>
														<input type='text' name='name' onChange={newFolder} />
														<div onClick={() => createNewList()}>+ new sub LIST</div>
													</div> */}
														</S.FolderList>
													</S.Wrapper>
												)}
											</div>
										);
									})}

									{originalFolders.lists.length >= 1 &&
										originalFolders.lists.map((list: IFolder, i) => {
											const listName = list.attributes.name;
											const listId = list.id;
											return (
												<S.Wrapper key={listName + i} onClick={() => updateParentList(listId)}>
													<S.FolderList>
														<div>
															<Icon icon='my-lists' size='24' />
															{list.attributes.name}
														</div>
														<Icon icon='chevron-right' size='24' />
													</S.FolderList>
												</S.Wrapper>
											);
										})}
								</div>
								<div>
									<>
										{openFolder && (subContent.folders.length >= 1 || subContent.lists.length >= 1) ? (
											<>
												<SubFolder
													subFolders={subContent.folders}
													subLists={subContent.lists}
													listResponse={listResponse}
													openFolder={openFolder}
													updateParentFolder={updateParentFolder}
													updateParentList={updateParentList}
												/>
											</>
										) : (
											<div>-closed/empty folder view-</div>
										)}
									</>
									<div />
								</div>
							</>
						)}
					</S.Container>
					{showContentList && <ListContent listItems={openList.lists} listName={openList.listName} onClickBack={() => clearList()} />}
				</>
			)}
		</S.InfluencerList>
	);
};

export default Folders;
