import styled from 'styled-components';
import { Button } from 'components/Button';
import Icon from 'components/Icon';
import colors from 'styles/variables/colors';

interface IFolderListProps {
	isOpen?: boolean;
}

export const InfluencerList = styled.div`
	max-width: 1920px;
	width: 100%;
	margin: 0 auto;
`;
export const Container = styled.div`
	display: grid;
	column-gap: 1rem;
	grid-template-columns: 2fr 4fr;
`;
export const Wrapper = styled.div`
	display: flex;
	column-gap: 1rem;
`;
export const SubWrapper = styled(Wrapper)``;

export const FolderList = styled.div<IFolderListProps>`
	display: flex;
	width: 100%;
	justify-content: space-between;
	padding: 1.25rem 0.75rem;
	min-width: 400px;
	border-bottom: 1px black solid;
	cursor: pointer;
	background-color: ${(props) => props.isOpen && colors.borderGray};
	color: ${(props) => props.isOpen && colors.buttonGray};
	fill: ${(props) => props.isOpen && colors.buttonGray};
`;

export const CustomIcon = styled(Icon)``;

export const CustomButton = styled(Button)<IFolderListProps>`
	background-color: ${colors.transparent};

	${CustomIcon} {
		&.icon {
			margin-left: 0;
			line-height: 0;
		}
	}
`;

export const ButtonContainer = styled.div`
	display: flex;
	column-gap: 1rem;
	margin-bottom: 1rem;
`;
