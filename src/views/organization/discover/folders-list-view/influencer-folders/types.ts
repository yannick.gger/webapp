export interface IOpenFolder {
	folderId: string;
	updateParentFolder: (parentId: string, openNewFolderId: string) => void;
	isOpen?: boolean;
}

export interface IOpenFolderContent {
	isOpen?: boolean;
}

export interface IFolder {
	attributes: {
		name: string;
		createdAt: string;
	};
	id: string;
	links: string[];
	relationships: {
		children?: any; // todo: Type these
		parent?: any;
		users?: any;
	};
	type: string;
}
export interface IFolders {
	folders: IFolder[];
	lists: any[];
	isFetching: boolean;
}

export interface IFolderContent {
	children: IFolder[];
	isFetching: boolean;
}
