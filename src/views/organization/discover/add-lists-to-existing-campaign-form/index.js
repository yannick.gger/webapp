import React from 'react';
import { Form, Button, message } from 'antd';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { Mutation } from 'react-apollo';
import addListsToExistingCampaign from 'graphql/add-lists-to-existing-campaign.graphql';
import DraftCampaignsWithSearchBar from '../draft-campaign-with-search.js';

class AddListsToExistingForm extends React.Component {
	render() {
		const { location, handleError, handleSuccess } = this.props;
		const columns = [
			{
				title: 'Campaign name',
				key: 'name',
				width: '300px',
				render: (record) => <div>{record.node.name}</div>
			},
			{
				title: 'action',
				key: 'action',
				width: '300px',
				render: (record) => {
					return (
						<React.Fragment>
							<Mutation
								mutation={addListsToExistingCampaign}
								onError={() => {
									if (handleError) {
										handleError();
									}
								}}
								onCompleted={() => {
									if (handleSuccess) {
										handleSuccess();
									}
									message.success(`Added list(s) to your campaign`);
								}}
							>
								{(addListsToExistingCampaign) => (
									<Button
										size='small'
										type='primary'
										onClick={() =>
											addListsToExistingCampaign({
												variables: { campaignId: record.node.id, myListIds: location && location.state.myListIds }
											})
										}
									>
										Add
									</Button>
								)}
							</Mutation>
						</React.Fragment>
					);
				}
			}
		];
		return <DraftCampaignsWithSearchBar columns={columns} />;
	}
}

const WrappedAddListsToExistingCampaignForm = Form.create()(AddListsToExistingForm);

export default withRouter(injectIntl(WrappedAddListsToExistingCampaignForm));
