import React, { Component } from 'react';
import { Input, AutoComplete } from 'antd';
import { injectIntl } from 'react-intl';
import { Query } from 'react-apollo';
import showFullMyList from 'graphql/get-full-my-list.graphql';

const { Search } = Input;

class SearchListInfluencers extends Component {
	state = {
		showResult: false,
		error: null
	};

	onSearch = (instagramOwners) => (value) => {
		const { handleSearchInfluencer } = this.props;
		const foundInfluencer = value ? instagramOwners.filter((inf) => inf.instagramOwnerUser.username.includes(value)) : [];
		handleSearchInfluencer(foundInfluencer);
	};

	render() {
		const { myListData, organization } = this.props;
		return (
			<Query
				query={showFullMyList}
				variables={{ myListId: Number(myListData.id), organizationId: Number(organization.id) }}
				fetchPolicy='cache-and-network'
				notifyOnNetworkStatusChange
			>
				{({ loading: queryLoading, error, data, fetchMore, refetch }) => {
					const myList = (data && data.showMyList) || {};
					const instagramOwners = (myList.instagramOwners && myList.instagramOwners.edges.map((item) => item.node)) || [];
					return (
						<div style={{ width: '100%' }}>
							<AutoComplete size='large' style={{ margin: '0px 20px', width: '-webkit-fill-available' }} onSearch={this.onSearch(instagramOwners)}>
								<Search size='large' enterButton placeholder='Search influencer in this list' />
							</AutoComplete>
						</div>
					);
				}}
			</Query>
		);
	}
}

export default injectIntl(SearchListInfluencers);
