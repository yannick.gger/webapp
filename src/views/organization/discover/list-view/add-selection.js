import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Dropdown, Menu } from 'antd';
import { Query } from 'react-apollo';
import { injectIntl } from 'react-intl';
import getDraftCampaigns from 'graphql/get-draft-campaigns.graphql';

class AddSelection extends Component {
	render() {
		const {
			match: { params },
			organization,
			instagramOwners,
			disabled,
			children
		} = this.props;

		const handleSelectedMenu = () => (
			<Menu>
				<Menu.ItemGroup title='Create something new'>
					<Menu.Item key='0'>
						<Link
							to={{
								pathname: `/${organization.slug}/discover/list/${params.id}/create-new-list`,
								state: { instagramOwners }
							}}
						>
							Use selection to create new list
						</Link>
					</Menu.Item>
				</Menu.ItemGroup>
				<div className='ant-dropdown-menu-item-divider' />
				<Menu.ItemGroup title='Add'>
					<Menu.Item key='1'>
						<Link
							to={{
								pathname: `/${organization.slug}/discover/list/${params.id}/add-selection-to-other-list`,
								state: { instagramOwners }
							}}
						>
							Add selection to other list
						</Link>
					</Menu.Item>
					<Menu.Item key='2'>
						<Link
							to={{
								pathname: `/${organization.slug}/discover/list/${params.id}/add-selection-to-campaign`,
								state: { instagramOwners }
							}}
						>
							Add selection to campaign
						</Link>
					</Menu.Item>
				</Menu.ItemGroup>
			</Menu>
		);

		return (
			<Query query={getDraftCampaigns}>
				{({ loading: queryLoading }) => {
					return (
						<Dropdown overlay={handleSelectedMenu} trigger={['click']} loading={queryLoading} disabled={disabled || queryLoading}>
							{children({ queryLoading })}
						</Dropdown>
					);
				}}
			</Query>
		);
	}
}

export default withRouter(injectIntl(AddSelection));
