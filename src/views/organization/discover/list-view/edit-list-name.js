import React, { Component } from 'react';
import { Button, Icon, Input, message } from 'antd';
import { Mutation } from 'react-apollo';
import updateList from 'graphql/update_list.graphql';

class EditListName extends Component {
	state = {
		showEditForm: false,
		listName: ''
	};

	handleEditListName = () => {
		this.setState({
			showEditForm: !this.state.showEditForm
		});
	};

	onChange = (e) => {
		this.setState({
			listName: e.target.value
		});
	};

	cancelEditListName = () => {
		this.setState({
			showEditForm: false
		});
	};

	saveListName = async (e, updateList) => {
		const { myListData } = this.props;
		e.preventDefault();
		const graphQlValues = {
			id: myListData.id,
			name: this.state.listName
		};
		let newListName = this.state.listName;
		let oldListName = myListData.name;

		if (newListName !== oldListName && newListName.length > 0) {
			const res = await updateList({ variables: graphQlValues });
			if (res.data.updateList) {
				message.success('Updated list name');
				this.setState({
					showEditForm: false
				});
			} else {
				message.error('Something went wrong');
			}
		} else {
			this.setState({
				showEditForm: false
			});
		}
	};
	render() {
		const { myListData } = this.props;
		if (this.state.showEditForm) {
			return (
				<div className='edit-list-name-container'>
					<Mutation mutation={updateList}>
						{(updateList) => <Input defaultValue={myListData.name} onChange={this.onChange} onPressEnter={(e) => this.saveListName(e, updateList)} />}
					</Mutation>
					<Mutation mutation={updateList}>
						{(updateList) => (
							<React.Fragment>
								<Button className='mr-20 ml-20' type='primary' onClick={(e) => this.saveListName(e, updateList)}>
									Save
								</Button>
								<Button onClick={this.cancelEditListName}>Cancel</Button>
							</React.Fragment>
						)}
					</Mutation>
				</div>
			);
		} else {
			return (
				<div className='edit-list-name-container'>
					<h1>{myListData.name}</h1>
					<Button className='ml-20' onClick={this.handleEditListName}>
						<Icon type='edit' theme='filled' />
					</Button>
				</div>
			);
		}
	}
}
export default EditListName;
