import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Layout, Spin, message } from 'antd';
import { withOrganization } from 'hocs';

import gql from 'graphql-tag';
import { Query, withApollo } from 'react-apollo';
import showMyList from 'graphql/get-discover-my-list.graphql';
import List from './list';
import GenericNotFoundView from 'views/generic-not-found';
import AddClientCommentToSharedListModal from '../add-client-comment-to-shared-list-modal';

const addInfluencersToListMutation = gql`
	mutation addInfluencersToList($myListId: ID!, $instagramOwnerIds: [ID!]!) {
		addInfluencersToList(input: { myListId: $myListId, instagramOwnerIds: $instagramOwnerIds }) {
			instagramOwnerIds
			canAddCommentToProfile
		}
	}
`;

class ListIndex extends Component {
	state = { showAddNoteModal: false, addedInstagramOwnerIds: [], originalListId: null };

	handleAddInfluencerToList = (influencer, refetch) => {
		const {
			client,
			match: { params }
		} = this.props;

		client
			.mutate({
				mutation: addInfluencersToListMutation,
				variables: {
					myListId: params.id,
					instagramOwnerIds: [influencer.id]
				}
			})
			.then(({ data }) => {
				refetch();

				this.setState({ showAddNoteModal: true, addedInstagramOwnerIds: [influencer.id] });
			})
			.catch((error) => {
				message.error('Unable to add influencer to list.');
				throw error;
			});
	};

	handleAddNoteToSharedList = (value) => {
		this.setState({ showAddNoteModal: value });
	};

	render() {
		const { showAddNoteModal, addedInstagramOwnerIds, originalListId } = this.state;

		const {
			client,
			organization,
			match: { params }
		} = this.props;

		console.log(this.props);

		return (
			<React.Fragment>
				{showAddNoteModal && (
					<AddClientCommentToSharedListModal
						handleSuccess={() => {
							this.setState({ showAddNoteModal: false });
						}}
						myListId={originalListId}
						instagramOwnerIds={addedInstagramOwnerIds}
						organizationId={organization.id}
						commentType='add_profile_comment'
						status='new_comment'
					/>
				)}
				<Query
					query={showMyList}
					variables={{ myListId: params.id, organizationId: organization.id }}
					onCompleted={(data) => {
						this.setState({ originalListId: data.showMyList.id });
					}}
					fetchPolicy='cache-and-network'
					notifyOnNetworkStatusChange
				>
					{({ loading: queryLoading, error, data, fetchMore, refetch }) => {
						if (queryLoading) {
							return (
								<Layout>
									<Layout.Content>
										<Spin className='collabspin' />
									</Layout.Content>
								</Layout>
							);
						}

						if (!data.showMyList) return <GenericNotFoundView />;

						const myList = (data && data.showMyList) || {};
						const instagramOwnerIds = myList.instagramOwnerIds || [];
						const cursor = null;
						return (
							<List
								slug={params.organizationSlug}
								fetchMore={fetchMore}
								myListData={myList}
								loading={queryLoading}
								apolloClient={client}
								instagramOwnerIds={instagramOwnerIds}
								onAddInfluencersToList={(influencer) => this.handleAddInfluencerToList(influencer, refetch)}
								cursor={cursor}
								error={error}
							/>
						);
					}}
				</Query>
			</React.Fragment>
		);
	}
}
export default withApollo(withRouter(withOrganization(ListIndex)));
