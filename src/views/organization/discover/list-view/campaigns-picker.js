import { Button, Dropdown, Empty, Menu, message, Divider, Layout, Spin } from 'antd';
import ButtonLink from 'components/button-link';
import OrganizationLink from 'components/organization-link';
import gql from 'graphql-tag';
import getDraftCampaigns from 'graphql/get-draft-campaigns.graphql';
import React, { Component } from 'react';
import { Mutation, Query } from 'react-apollo';
import { injectIntl, intlShape } from 'react-intl';
import { Link, withRouter } from 'react-router-dom';

const ADD_LISTS_TO_EXISTING_CAMPAIGN = gql`
	mutation addListsToExistingCampaign($campaignId: ID!, $myListIds: [ID!]!) {
		addListsToExistingCampaign(input: { campaignId: $campaignId, myListIds: $myListIds }) {
			campaign {
				id
				name
			}
		}
	}
`;

class CampaignPicker extends Component {
	state = {
		limit: 10
	};
	render() {
		const { myListId, organization } = this.props;
		const { limit } = this.state;
		const campaignMenu = ({ campaigns, addListsToExistingCampaign, organization, myListId }) => (
			<Menu>
				<Menu.ItemGroup title='Create something new'>
					<Menu.Item>
						<Link
							to={{
								pathname: `/${organization.slug}/discover/list/${myListId}/create-campaign-from-single-list`,
								state: {
									myListIds: [myListId]
								}
							}}
						>
							Create new campaign from list
						</Link>
					</Menu.Item>
				</Menu.ItemGroup>
				<Divider />
				<Menu.ItemGroup title='Add list to existing campaign'>
					{campaigns && campaigns.length === 0 && (
						<Empty description='Create a campaign to add influencers.'>
							<OrganizationLink to='/campaigns/create'>
								<Button>Create campaign</Button>
							</OrganizationLink>
						</Empty>
					)}
					{campaigns &&
						campaigns.map((campaign, index) => {
							if (index < limit) {
								return (
									<Menu.Item data-ripple='rgba(132,146, 164, 0.2)' key={campaign.node.id}>
										<ButtonLink
											onClick={() =>
												addListsToExistingCampaign({
													variables: { campaignId: campaign.node.id, myListIds: [myListId] }
												})
											}
										>
											{campaign.node.name}
										</ButtonLink>
									</Menu.Item>
								);
							}
						})}
				</Menu.ItemGroup>
				{campaigns && campaigns.length > limit && (
					<Menu.ItemGroup>
						<Menu.Item style={{ display: 'flex', justifyContent: 'center' }}>
							<Button type='primary'>
								<Link
									to={{
										pathname: `/${this.props.match.params.organizationSlug}/discover/lists/add-lists-to-existing-campaign`,
										state: { myListIds: [myListId] }
									}}
								>
									View all campaigns
								</Link>
							</Button>
						</Menu.Item>
					</Menu.ItemGroup>
				)}
			</Menu>
		);
		return (
			<Mutation
				mutation={ADD_LISTS_TO_EXISTING_CAMPAIGN}
				onCompleted={() => {
					if (this.props.handleSuccess) {
						this.props.handleSuccess();
					}
					message.success(
						this.props.intl.formatMessage({
							id: 'add-list-to-existing-campaign-success',
							defaultMessage: `Added list(s) to your campaign`
						})
					);
				}}
			>
				{(addListsToExistingCampaign, { loading: mutationLoading }) => (
					<Query query={getDraftCampaigns}>
						{({ data, loading: queryLoading }) => {
							if (queryLoading) {
								return (
									<Layout>
										<Layout.Content>
											<Spin className='collabspin' />
										</Layout.Content>
									</Layout>
								);
							}
							return (
								<Dropdown
									overlay={campaignMenu({ campaigns: data.draftCampaigns.edges, addListsToExistingCampaign, organization, myListId })}
									trigger={['click']}
									loading={mutationLoading}
									disabled={this.props.disabled || queryLoading}
								>
									{this.props.children({ mutationLoading })}
								</Dropdown>
							);
						}}
					</Query>
				)}
			</Mutation>
		);
	}
}

CampaignPicker.propTypes = {
	intl: intlShape.isRequired
};

export default withRouter(injectIntl(CampaignPicker));
