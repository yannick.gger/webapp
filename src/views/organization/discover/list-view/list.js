import React, { Component } from 'react';
import { withOrganization } from 'hocs';
import { Mutation, withApollo } from 'react-apollo';
import { Link, withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { lookup } from 'country-data';
import { Button, Layout, Divider, Icon, Table, Avatar, Popover, Progress, Col, Row, Tag, message, Badge } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';
import CampaignPicker from './campaigns-picker';
import AddInfluencers from './add-influencers';
import AddSelection from './add-selection';
import shareMyListToTeam from 'graphql/share-my-list-to-team.graphql';
import removeInstagramOwnersFromList from 'graphql/remove-instagram-onwers-from-list.graphql';
import showMyList from 'graphql/get-discover-my-list.graphql';
import EditListName from './edit-list-name';
import SearchListInfluencers from './search-list-influencers';
import { isAdmin, isSaler, hasGhostToken } from 'services/auth-service';
import InstagramProfileDrawer from 'components/instagram-profile-drawer';
import ProfileCommentDrawer from 'components/profile-comment-drawer';
import { MaleIcon, FemaleIcon } from 'components/icons';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';
import showFullMyList from 'graphql/get-full-my-list.graphql';
import updateMessageBadgeStatus from 'graphql/update-message-badge-status.graphql';
import '../styles.scss';
import AddClientCommentToSharedListModal from '../add-client-comment-to-shared-list-modal';

const topColumns = [
	{
		title: 'Followers',
		key: 'follwers',
		width: '180px',
		render: (record) => (
			<div>
				<span style={{ fontWeight: '700', color: '#026bfa', fontSize: '18px' }} href='#' target='_blank' rel='noopener noreferrer'>
					<ShortAmountFormatter value={record.followers} />
				</span>
			</div>
		)
	},
	{
		title: 'Added to list',
		key: 'addedList',
		width: '180px',
		render: (record) => (
			<div style={{ fontWeight: '700', color: '#026bfa', fontSize: '18px' }} href='#' target='_blank' rel='noopener noreferrer'>
				{`${record.addedToList} ${record.addedToList > 1 ? 'Influencers' : 'Influencer'}`}
			</div>
		)
	},
	{
		title: 'Engagement',
		key: 'engagement',
		width: '180px',
		render: (record) => (
			<div style={{ fontWeight: '700', color: '#026bfa', fontSize: '18px' }} href='#' target='_blank' rel='noopener noreferrer'>
				{`${record.engagement.toFixed(2)}%`}
			</div>
		)
	},
	{
		title: 'List used in',
		key: 'usedIn',
		width: '180px',
		render: (record) => (
			<div style={{ fontWeight: '700', color: '#026bfa', fontSize: '18px' }} href='#' target='_blank' rel='noopener noreferrer'>
				{`${record.listUsedIn} ${record.listUsedIn > 1 ? 'Campaigns' : 'Campaign'}`}
			</div>
		)
	}
];

class List extends Component {
	state = {
		selectedRowKeys: [],
		selectedInstagramOwners: [],
		showEditForm: false,
		listName: '',
		shareLoading: false,
		sharedToTeam: false,
		searchInfluencer: [],
		showAddNoteModal: false,
		removedInstagramOwnerIds: []
	};

	onSelectChange = (instagramOwners, searchInfluencer) => {
		return (selectedRowKeys) => {
			this.setState((prevState) => {
				const previousSelectedInstagramOwners = prevState.selectedInstagramOwners;
				const newSelectedInstagramOwners = selectedRowKeys
					.filter((key) => !prevState.selectedInstagramOwners.some(({ id }) => id === key))
					.map((key) => instagramOwners.find((i) => i.id === key) || searchInfluencer.find((i) => i.id === key));

				let resSelectedInstagramOwners = [...newSelectedInstagramOwners, ...previousSelectedInstagramOwners].filter(({ id }) =>
					selectedRowKeys.some((key) => key === id)
				);

				return {
					selectedRowKeys,
					selectedInstagramOwners: resSelectedInstagramOwners
				};
			});
		};
	};

	loadMore = (instagramOwners, fetchMore) => {
		return fetchMore({
			variables: {
				after: this.props.cursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					showMyList: {
						...fetchMoreResult.showMyList,
						instagramOwners: {
							...fetchMoreResult.showMyList.instagramOwners,
							edges: [...prev.showMyList.instagramOwners.edges, ...fetchMoreResult.showMyList.instagramOwners.edges]
						}
					}
				};
				return result;
			}
		});
	};

	shareListToTeam = (listId) => {
		const { client } = this.props;

		this.setState({ shareLoading: true });
		client
			.mutate({
				mutation: shareMyListToTeam,
				variables: { myListId: Number(listId) }
			})
			.then((res) => {
				if (res.data.shareMyListToTeam.errors.length === 0) {
					message.success('Shared list with your team');
					this.setState({ shareLoading: false, sharedToTeam: true });
				} else {
					this.setState({ shareLoading: false });
				}
			})
			.catch(({ graphQLErrors }) => {
				this.setState({ shareLoading: false });
				if (graphQLErrors) {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
				}
			});
	};

	handleSearchInfluencer = (searchInfluencer) => {
		this.setState({ searchInfluencer });
	};

	handleUpdatemessageStatus = (updateMessageBadgeStatus, myListData) => {
		return (e) => {
			e.preventDefault();

			updateMessageBadgeStatus({
				variables: { myListId: myListData.id, status: 'comment_seen' }
			})
				.then(() => {
					this.props.history.push(`/${this.props.match.params.organizationSlug}/discover/list/${myListData.id}/profile-comments`);
				})
				.catch(({ error }) => {
					message.error('Something went wrong updating comment status');
					throw error;
				});
		};
	};

	render() {
		const { organization, myListData, instagramOwners, onAddInfluencersToList, apolloClient, loading, fetchMore, error, intl, hasNextPage } = this.props;
		const { selectedRowKeys, shareLoading, sharedToTeam, searchInfluencer, showAddNoteModal, removedInstagramOwnerIds } = this.state;
		const rowSelection = (instagramOwners) => ({
			selectedRowKeys,
			onChange: this.onSelectChange(instagramOwners, searchInfluencer)
		});
		const hasSelected = selectedRowKeys.length > 0;
		const topColumnsData = () => [
			{
				followers: myListData ? myListData.instagramOwnersAggregation.followedByCount : 0,
				engagement: myListData ? myListData.instagramOwnersAggregation.interactionRate : 0,
				addedToList: myListData.influencersCount,
				listUsedIn: myListData.usedInCampaigns,
				audience: null
			}
		];
		const columns = [
			{
				title: '',
				key: 'userAvatar',
				width: '60px',
				render: (record) => {
					return '';
				}
			},
			{
				title: 'Name',
				key: 'name',
				width: '180px',
				render: (record) => {
					return '';
				}
			},
			{
				title: 'Followers',
				key: 'followers',
				defaultSortOrder: 'descend',
				sorter: (a, b) => a.followedByCount - b.followedByCount,
				render: (record) => (
					<div className='number-box gray' onClick={this.showModal}>
						<ShortAmountFormatter value={record.followedByCount} />
					</div>
				)
			},
			{
				title: 'Engagement',
				key: 'engagement',
				defaultSortOrder: 'descend',
				sorter: (a, b) => a.interactionRate - b.interactionRate,
				render: (record) => (
					<div className='number-box green' style={{ width: '66px' }}>
						{`${record.interactionRate.toFixed(2)}%`}
					</div>
				)
			},
			{
				title: 'Category',
				key: 'category',
				render: (record) => (
					<span>
						{record.category1 && <Tag color={record.category1.color}>{record.category1.name}</Tag>}
						{record.category2 && <Tag color={record.category2.color}>{record.category2.name}</Tag>}
					</span>
				)
			},
			{
				title: 'Comments',
				key: 'comments',
				render: (record) => {
					return (
						<ProfileCommentDrawer
							myListId={myListData.id}
							organization={organization}
							instagramOwnerUser={record.instagramOwnerUser}
							commentSeenCount={record.instagramOwnerCommentSeenCount}
							oldCommentsCount={record.oldCommentsCount}
							newCommentCount={record.instagramOwnerNewCommentCount}
						/>
					);
				}
			}
		];
		if (isAdmin() || hasGhostToken()) {
			columns.push({
				title: 'Email',
				key: 'email',
				render: (record) => <span>{record.instagramOwnerUser.email}</span>
			});
		}
		const newCommentIsPresent = myListData.myListOrganizationNewCommentCount > 0;
		return (
			<Layout>
				<Layout.Header>
					<EditListName myListData={myListData} />
					<div>
						{(isAdmin() || isSaler()) && (
							<Button className='mr-10'>
								<Link
									to={{
										pathname: `/${this.props.match.params.organizationSlug}/discover/list/${myListData.id}/share-with-clients`
									}}
								>
									Share with clients
								</Link>
							</Button>
						)}
						{showAddNoteModal && (
							<AddClientCommentToSharedListModal
								handleSuccess={() => {
									this.setState({ showAddNoteModal: false });
								}}
								myListId={myListData.id}
								instagramOwnerIds={removedInstagramOwnerIds}
								organizationId={organization.id}
								commentType='remove_profile_comment'
								status='new_comment'
							/>
						)}
						{organization.hasTeamMember && (
							<Button
								className='mr-10'
								disabled={myListData.sharedToTeam || sharedToTeam}
								loading={shareLoading}
								onClick={() => this.shareListToTeam(myListData.id)}
							>
								{myListData.sharedToTeam || sharedToTeam ? 'Shared' : 'Share with team'}
							</Button>
						)}
						<CampaignPicker
							handleSuccess={() => {
								this.setState({ selectedRowKeys: [], selectedInstagramOwners: [] });
							}}
							myListId={myListData.id}
							organization={organization}
						>
							{() => <Button type='primary' icon=' ion ion-plus' size='large' />}
						</CampaignPicker>
					</div>
				</Layout.Header>
				<Layout.Content>
					<div className='discover-wrapper my-list-view-wrapper'>
						<Table className='top-table' columns={topColumns} pagination={false} dataSource={myListData ? topColumnsData() : []} />
						<div className='pt-25 pb-15'>
							<Row>
								<Col sm={{ span: 17 }} />
								<Col sm={{ span: 7 }}>
									<p style={{ fontWeight: 700, fontSize: '14px', color: '#9ea4ab', marginBottom: '5px', textTransform: 'uppercase' }}>quick add</p>
								</Col>
							</Row>
							<Row>
								<Col sm={{ span: 17 }} style={{ display: 'flex' }}>
									<AddSelection
										disabled={!hasSelected}
										organization={organization}
										instagramOwners={selectedRowKeys.map((key) => ({
											id: key
										}))}
									>
										{() => (
											<Button type='primary'>
												Handle selected to... <Icon type='down' />
											</Button>
										)}
									</AddSelection>
									<Mutation
										mutation={removeInstagramOwnersFromList}
										refetchQueries={[
											{ query: showMyList, variables: { myListId: Number(myListData.id), organizationId: Number(organization.id) } },
											{ query: showFullMyList, variables: { myListId: Number(myListData.id), organizationId: Number(organization.id) } }
										]}
										onCompleted={(data) => {
											this.setState((prevState) => ({
												selectedRowKeys: [],
												selectedInstagramOwners: [],
												showAddNoteModal: true,
												removedInstagramOwnerIds: prevState.selectedRowKeys
											}));
											message.success(
												intl.formatMessage({
													id: 'remove-instagram-owners-from-list-success',
													defaultMessage: `Removed ${selectedRowKeys.length} instagram ${selectedRowKeys.length > 1 ? 'owners' : 'owner'} from list ${
														myListData.name
													}`
												})
											);
										}}
										onError={({ graphQLErrors }) => {
											graphQLErrors.map((e) => message.error(e.message));
										}}
									>
										{(removeInstagramOwnersFromList) => (
											<Button
												style={{ marginLeft: '20px' }}
												disabled={!hasSelected}
												onClick={() => {
													removeInstagramOwnersFromList({
														variables: {
															myListId: Number(myListData.id),
															instagramOwnerIds: selectedRowKeys.map((key) => key)
														}
													});
												}}
											>
												<Icon type='delete' theme='filled' className={hasSelected ? 'red-trash' : ''} />
												{isAdmin() || isSaler() ? null : 'Remove'}
											</Button>
										)}
									</Mutation>
									{newCommentIsPresent ? (
										<Mutation mutation={updateMessageBadgeStatus}>
											{(updateMessageBadgeStatus) => (
												<Badge dot>
													<Button style={{ marginLeft: '20px' }} onClick={this.handleUpdatemessageStatus(updateMessageBadgeStatus, myListData)}>
														<Icon type='message' theme='filled' />
													</Button>
												</Badge>
											)}
										</Mutation>
									) : (
										<Badge count={myListData.myListOrganizationCommentSeenCount} overflowCount={99}>
											<Button
												style={{ marginLeft: '20px' }}
												href={`/${this.props.match.params.organizationSlug}/discover/list/${myListData.id}/profile-comments`}
											>
												<Icon type='message' theme='filled' />
											</Button>
										</Badge>
									)}
									<SearchListInfluencers
										apolloClient={apolloClient}
										organization={organization}
										myListData={myListData}
										handleSearchInfluencer={this.handleSearchInfluencer}
									/>
								</Col>
								<Col sm={{ span: 7 }}>
									<AddInfluencers apolloClient={apolloClient} onAddInfluencer={onAddInfluencersToList} existedInfluencers={instagramOwners} />
								</Col>
								<Col sm={{ span: 24 }} style={{ margin: '20px 0px' }}>
									<span>{hasSelected ? `${selectedRowKeys.length} ${selectedRowKeys.length > 1 ? 'influencers' : 'influencer'} selected` : ''}</span>
								</Col>
							</Row>
						</div>
						<Table
							className='main-table pb-30'
							columns={columns}
							pagination={false}
							rowKey='id'
							rowSelection={rowSelection(instagramOwners || [])}
							dataSource={searchInfluencer.length ? searchInfluencer : instagramOwners}
							scroll={{ x: 1000 }}
							loading={loading}
						/>
						{!error && hasNextPage && (
							<div className='pb-30 text-center'>
								<Button type='primary' loading={loading} onClick={() => this.loadMore(instagramOwners, fetchMore)}>
									Load more
								</Button>
							</div>
						)}
					</div>
				</Layout.Content>
			</Layout>
		);
	}
}

export default withRouter(withApollo(injectIntl(withOrganization(List))));
