import React, { Component } from 'react';
import { Dropdown, Menu, Col, Row, Avatar, Button, Input, Typography, Icon } from 'antd';
import { ShortAmountFormatter } from 'components/formatters';
import { injectIntl } from 'react-intl';
import gql from 'graphql-tag';

const { Search } = Input;
const { Text } = Typography;

const GET_INSTAGRAM_OWNERS = gql`
	query getInstagramOwners($username: String) {
		instagramOwners(username: $username) {
			id
			followedByCount
			instagramOwnerUser {
				username
				avatar
			}
		}
	}
`;

class AddInfluencers extends Component {
	state = {
		showResult: false,
		instagramOwners: [],
		error: null,
		searchText: ''
	};

	handleClearSearch = (event) => {
		event.preventDefault();
		const searchText = event.target.value;
		this.setState({ searchText });
	};

	hideResult = () => {
		this.setState({
			showResult: false
		});
	};

	handleSearch = (value) => {
		if (!value) {
			this.setState({
				showResult: true,
				instagramOwners: [],
				error: 'Type something to search...'
			});
			return;
		}

		const { existedInfluencers, apolloClient } = this.props;

		apolloClient
			.query({
				query: GET_INSTAGRAM_OWNERS,
				variables: {
					username: value
				},
				fetchPolicy: 'no-cache'
			})
			.then(({ data: { instagramOwners } }) => {
				this.setState({
					instagramOwners: instagramOwners.map((io) => {
						const existedInList = existedInfluencers.find((influencer) => influencer.id === io.id);
						return { ...io, existedInList: !!existedInList };
					}),
					showResult: true,
					error: null
				});
			})
			.catch((error) => {
				this.setState({
					showResult: true,
					instagramOwners: [],
					error: 'Fail to search influencers'
				});
				throw error;
			});
	};

	handleAdd = (influencer) => {
		this.setState({ showResult: false });
		this.props.onAddInfluencer(influencer);
		this.setState({ searchText: '' });
	};

	renderResultList = () => {
		const { instagramOwners, error } = this.state;
		return (
			<Menu style={{ position: 'relative', maxHeight: '55vh', overflowY: 'scroll' }}>
				<Icon
					theme='filled'
					type='close-circle'
					style={{
						cursor: 'pointer',
						position: 'absolute',
						right: '1.7rem',
						top: '1.4375rem'
					}}
					onClick={this.hideResult}
				/>
				{error && (
					<Text style={{ display: 'inline-block', padding: '0.65rem 1.7rem' }} type='danger'>
						{error}
					</Text>
				)}
				{!error && !!instagramOwners.length && (
					<Menu.ItemGroup title='Add influencer to list'>
						{instagramOwners.map((instagramOwner, index) => {
							return (
								<Menu.Item key={index}>
									<Row className='d-flex align-items-center'>
										<Col span={12}>
											<div className='d-flex align-items-center'>
												<div>
													<Avatar size='large' src='#' icon='user' style={{ marginRight: '10px' }} />
												</div>
												<div>
													<p
														title={instagramOwner.instagramOwnerUser.username}
														style={{ margin: 0, maxWidth: '100px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}
													>
														{instagramOwner.instagramOwnerUser.username}
													</p>
													<p style={{ margin: 0 }}>
														<ShortAmountFormatter value={instagramOwner.followedByCount} /> followers
													</p>
												</div>
											</div>
										</Col>
										<Col span={12} className='d-flex justify-content-end'>
											<Button size='small' type='primary' onClick={() => this.handleAdd(instagramOwner)} disabled={instagramOwner.existedInList}>
												Add
											</Button>
										</Col>
									</Row>
								</Menu.Item>
							);
						})}
					</Menu.ItemGroup>
				)}
				{!error && !instagramOwners.length && <Text style={{ display: 'inline-block', padding: '0.65rem 1.7rem' }}>No influencers matched</Text>}
			</Menu>
		);
	};

	render() {
		const { showResult, searchText } = this.state;

		return (
			<Dropdown overlay={this.renderResultList()} visible={showResult} trigger={['click']}>
				<Search onChange={this.handleClearSearch} value={searchText} placeholder='Search @username' size='large' enterButton onSearch={this.handleSearch} />
			</Dropdown>
		);
	}
}

export default injectIntl(AddInfluencers);
