import React, { Component } from 'react';
import { Menu, Icon } from 'antd';

export default class AddToCampaign extends Component {
	render() {
		return (
			<Menu onClick={this.start}>
				<Menu.ItemGroup title='Add to campaign'>
					<Menu.Item className='add-new' key='add new list'>
						<a href='#'>
							<Icon type='plus' /> Add new
						</a>
						<a href='#'>
							<Icon type='plus' /> Add new
						</a>
					</Menu.Item>
				</Menu.ItemGroup>
			</Menu>
		);
	}
}
