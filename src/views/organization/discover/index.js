import React, { Component } from 'react';
import { Layout, Spin, Row, Col, Drawer, Button, Tag } from 'antd';
import { Query } from 'react-apollo';
import { withOrganization } from 'hocs';
import { lookup } from 'country-data';
import ListsView from './lists.js';
import DiscoverFilter from './discover-filter-form.js';
import { defaultCountries, defaultMinFollowers, defaultMaxFollowers } from '../campaign/recommendations/influencer-filter-form';
import { isTester, isSaler } from 'services/auth-service';

import getDiscoverInstagramOwners from 'graphql/get-discover-instagram-owners.graphql';
import getCategories from 'graphql/get-category.graphql';

import './styles.scss';
class DiscoverView extends Component {
	state = {
		visible: false,
		placement: 'right',
		query: {
			username: '',
			audienceGender: '',
			countries: defaultCountries.map(({ id }) => id),
			minFollowers: defaultMinFollowers,
			maxFollowers: defaultMaxFollowers,
			audienceCountries: defaultCountries.map(({ id }) => id),
			audienceAgeGroups: [],
			categories: [],
			order: null
		}
	};

	showDrawer = () => {
		this.setState({
			visible: true
		});
	};

	onClose = () => {
		this.setState({
			visible: false
		});
	};

	onChange = (e) => {
		this.setState({
			placement: e.target.value
		});
	};

	handleFilter = (e) => {
		e.preventDefault();
		this.setState({
			visible: false
		});
		this.discoverFilterForm &&
			this.discoverFilterForm.props.form.validateFieldsAndScroll(async (err, formData) => {
				if (!err) {
					const { followers } = formData;
					this.setState((prevState) => ({
						query: { ...formData, username: '', minFollowers: followers[0], maxFollowers: formData.followers[1], order: prevState.query.order }
					}));
				}
			});
	};

	render() {
		const handleSearch = (value) => {
			this.setState((prevState) => ({ query: { ...prevState.query, username: value } }));
		};
		const handleSort = (sorter) => {
			if (Object.keys(sorter).length !== 0) {
				const dir = sorter.order === 'ascend' ? 'asc' : 'desc';
				this.setState((prevState) => ({ query: { ...prevState.query, order: { field: sorter.columnKey, direction: dir } } }));
			} else this.setState((prevState) => ({ query: { ...prevState.query, order: null } }));
		};
		const { query, visible, placement } = this.state;
		const { categories, audienceGender, countries, minFollowers, maxFollowers, audienceCountries, audienceAgeGroups } = query;
		const hasAudienceGenderFilter = Boolean(audienceGender !== '' && audienceGender !== 'all_genders');
		const influencerQueriesCount = categories.concat(countries).length;
		const audicenceQueries = Array.prototype.concat(categories, audienceCountries, audienceAgeGroups).length;
		const audicenceQueriesCount = hasAudienceGenderFilter ? audicenceQueries + 1 : audicenceQueries;

		return (
			<Query query={getDiscoverInstagramOwners} variables={query} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
				{({ loading: queryLoading, error, data, fetchMore, networkStatus }) => {
					if (queryLoading && !data.discoverInstagramOwners) {
						return <Spin className='collabspin' />;
					}
					return (
						<div>
							<Query query={getCategories}>
								{({ loading: queryLoading, data }) => {
									if (queryLoading) {
										return <Spin className='collabspin' />;
									}
									const categoriesData = data && data.categories ? data.categories : [];
									const categoriesSelected = categoriesData.filter((category) => categories.includes(category.index));
									let influencersFilterNodes = null;
									let audiencesFilterNodes = null;
									if (!hasAudienceGenderFilter && audienceCountries.length === 0 && audienceAgeGroups.length === 0 && categoriesSelected.length === 0) {
										audiencesFilterNodes = (
											<p>
												<span>You are targeting all engaged audiences</span>
											</p>
										);
									} else {
										audiencesFilterNodes = (
											<p>
												<span>You are targeting</span>
												{hasAudienceGenderFilter && <span className='capitalize-text blue-text'>{audienceGender}</span>}
												{categoriesSelected.length > 0 && (
													<span>
														<span>that like</span>
														<span className='blue-text'>
															{categoriesSelected.length > 0 ? categoriesSelected.map((category) => category.name).join(' + ') : null}
														</span>
													</span>
												)}
												{audienceCountries.length > 0 && (
													<span>
														<span>who live in</span>
														<span className='blue-text'>
															{audienceCountries &&
																audienceCountries
																	.map((country) => (lookup.countries({ alpha2: country })[0] ? lookup.countries({ alpha2: country })[0].name : country))
																	.join(' + ')}
														</span>
													</span>
												)}
												{audienceAgeGroups.length > 0 && (
													<span>
														<span>in age span</span>
														<span className='blue-text'>{audienceAgeGroups.length > 0 ? audienceAgeGroups.map((age) => age).join(' + ') : null}</span>
													</span>
												)}
											</p>
										);
									}
									if (categories.length === 0 && countries.length === 0 && minFollowers === defaultMinFollowers && maxFollowers === defaultMaxFollowers) {
										influencersFilterNodes = (
											<p>
												<span>You want all influencers</span>
											</p>
										);
									} else {
										influencersFilterNodes = (
											<p>
												<span>You want influencers</span>
												{categoriesSelected.length > 0 && (
													<span>
														<span>in category</span>
														<span className='blue-text'>
															{categoriesSelected.length > 0 ? categoriesSelected.map((category) => category.name).join(' + ') : null}
														</span>
													</span>
												)}
												{countries.length > 0 && (
													<span>
														<span>who live in</span>
														<span className='blue-text'>
															{countries &&
																countries
																	.map((country) => (lookup.countries({ alpha2: country })[0] ? lookup.countries({ alpha2: country })[0].name : country))
																	.join(' + ')}
														</span>
													</span>
												)}
												{(minFollowers !== defaultMinFollowers || maxFollowers !== defaultMaxFollowers) && (
													<span>
														<span>with followers between</span>
														<span className='blue-text'>
															{minFollowers !== defaultMinFollowers || maxFollowers !== defaultMaxFollowers
																? `${minFollowers.toShort()} - ${maxFollowers.toShort()}`
																: null}
														</span>
													</span>
												)}
											</p>
										);
									}
									return (
										<div className='discover-filter-container'>
											<Row className='discover-filter-content-show d-flex'>
												<Col md={{ span: 10 }} className='pr-10'>
													<div>
														<h4>
															Influencers filter
															{influencerQueriesCount > 0 && <Tag className='ml-10'>{influencerQueriesCount}</Tag>}
														</h4>
														{influencersFilterNodes}
													</div>
												</Col>
												<Col className='audience-filter-container pl-20' md={{ span: 10 }}>
													<div>
														<h4>
															Audiences filter
															{audicenceQueriesCount > 0 && <Tag className='ml-10'>{audicenceQueriesCount}</Tag>}
														</h4>
														{audiencesFilterNodes}
													</div>
												</Col>
												<Col md={{ span: 4 }} className='d-flex justify-content-end'>
													<div>
														<Button onClick={this.showDrawer}>Change filters</Button>
													</div>
												</Col>
											</Row>
											<Drawer
												title='Filters'
												width={960}
												placement={placement}
												closable={true}
												onClose={this.onClose}
												visible={visible}
												maskClosable={false}
												className='discover-filter-drawer-container'
											>
												<DiscoverFilter
													categories={categoriesData}
													wrappedComponentRef={(ref) => (this.discoverFilterForm = ref)}
													handleFilter={this.handleFilter}
												/>
											</Drawer>
										</div>
									);
								}}
							</Query>
							<ListsView
								data={data}
								fetchMore={fetchMore}
								networkStatus={networkStatus}
								error={error}
								searchHandler={handleSearch}
								handleSort={handleSort}
								loading={queryLoading}
							/>
						</div>
					);
				}}
			</Query>
		);
	}
}

class DiscoverViewWithTrialCheck extends Component {
	render() {
		document.title = `Discover | Collabs`;
		const { organization } = this.props;

		return (
			<Layout>
				<Layout.Header>
					<h1>Discover</h1>
				</Layout.Header>
				<Layout.Content>
					<DiscoverView />
				</Layout.Content>
			</Layout>
		);
	}
}

export default withOrganization(DiscoverViewWithTrialCheck);
