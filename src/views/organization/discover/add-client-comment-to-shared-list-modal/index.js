import React, { Component } from 'react';
import { Modal, Input, message, Button } from 'antd';
import { Mutation } from 'react-apollo';
import addListComment from 'graphql/add-list-comment.graphql';

const { TextArea } = Input;

class AddClientCommentToSharedListModal extends Component {
	state = { comment: '' };

	handleChange = (e) => {
		this.setState({ comment: e.target.value });
	};

	handleSubmit = (addClientCommentToSharedList, mutationVars, handleSuccess) => {
		return (e) => {
			e.preventDefault();

			addClientCommentToSharedList({ variables: mutationVars })
				.then(() => {
					handleSuccess();
				})
				.catch((error) => {
					message.error(error.message);
				});
		};
	};

	handleCancel = (addClientCommentToSharedList, mutationVars, handleSuccess) => {
		return (e) => {
			e.preventDefault();

			mutationVars.comment = '';

			addClientCommentToSharedList({ variables: mutationVars })
				.then(() => {
					handleSuccess();
				})
				.catch((error) => {
					message.error(error.message);
				});
		};
	};

	render() {
		const { comment } = this.state;
		const { myListId, instagramOwnerIds, organizationId, commentType, status } = this.props;

		let mutationVars = {
			myListId,
			instagramOwnerIds,
			organizationId,
			comment,
			commentType,
			status
		};

		return (
			<Mutation mutation={addListComment}>
				{(addClientCommentToSharedList) => (
					<Modal
						title='Help us know better'
						visible
						maskClosable={false}
						onCancel={this.handleCancel(addClientCommentToSharedList, mutationVars, this.props.handleSuccess)}
						footer={[
							<Button
								size='large'
								htmlType='button'
								type='primary'
								onClick={this.handleSubmit(addClientCommentToSharedList, mutationVars, this.props.handleSuccess)}
							>
								Submit
							</Button>
						]}
					>
						<TextArea rows={4} onChange={this.handleChange} placeholder='Add comment' />
					</Modal>
				)}
			</Mutation>
		);
	}
}

export default AddClientCommentToSharedListModal;
