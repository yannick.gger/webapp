import { Button, message, Modal, Table, Spin, Input, Row, Col, Select } from 'antd';
import React, { Component } from 'react';
import getAssignedClientsOfSaler from 'graphql/get-assigned-clients-of-saler.graphql';
import shareMyListToClients from 'graphql/share-my-list-to-clients.graphql';
import unshareMyListToClients from 'graphql/unshare-my-list-to-clients.graphql';
import { Query, Mutation } from 'react-apollo';

const Search = Input.Search;
const { Option } = Select;

class ShareListToClientsModal extends Component {
	state = {
		selectedRowKeys: [],
		shareButtonText: 'Share',
		notShareYet: true,
		searchTerm: '',
		defaultSelectValue: 'notShare',
		mutationQuery: shareMyListToClients
	};

	componentDidMount() {
		if (this.useQuery().get('shared') === 'true') {
			this.setState({ notShareYet: false, defaultSelectValue: 'shared', shareButtonText: 'Unshare' });
		}

		this.setState({ searchTerm: this.useQuery().get('search') || '' });
	}

	useQuery = () => {
		return new URLSearchParams(this.props.location.search);
	};

	handleCancel = () => {
		const { organizationSlug, id } = this.props.match.params;

		this.props.history.push(`/${organizationSlug}/discover/list/${id}`);
	};

	onSelectChange = (selectedRowKeys) => {
		this.setState({ selectedRowKeys });
	};

	loadMore = (clients, fetchMore, endCursor) => {
		return fetchMore({
			variables: {
				after: endCursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					assignedClientsOfSaler: {
						...fetchMoreResult.assignedClientsOfSaler,
						edges: [...prev.assignedClientsOfSaler.edges, ...fetchMoreResult.assignedClientsOfSaler.edges]
					}
				};
				return result;
			}
		});
	};

	handleSubmit = (queryName, myListId, organizationIds) => {
		return (e) => {
			e.preventDefault();

			queryName({
				variables: { myListId: myListId, organizationIds: organizationIds }
			})
				.then(({ data, error }) => {
					this.setState({ selectedRowKeys: [] });
					this.refetch();
					message.success(`${this.state.notShareYet ? 'Shared' : 'Unshared'} my list to clients success`);
				})
				.catch((error) => {
					message.error(error.message);
				});
		};
	};

	addQuery = (key, value) => {
		let pathname = this.props.location.pathname;
		let searchParams = new URLSearchParams(this.props.location.search);

		searchParams.set(key, value);

		this.props.history.push({
			pathname: pathname,
			search: searchParams.toString()
		});
	};

	removeQuery = (key) => {
		let pathname = this.props.location.pathname;
		let searchParams = new URLSearchParams(this.props.location.search);

		searchParams.delete(key);

		this.props.history.push({
			pathname: pathname,
			search: searchParams.toString()
		});
	};

	handleChange = (value) => {
		if (value === 'notShare') {
			this.setState({ shareButtonText: 'Share', notShareYet: true, mutationQuery: shareMyListToClients });
			this.addQuery('shared', 'false');
		} else {
			this.setState({ shareButtonText: 'Unshare', notShareYet: false, mutationQuery: unshareMyListToClients });
			this.addQuery('shared', 'true');
		}
	};

	searchHandler = (value) => {
		this.setState({ searchTerm: value });
		this.addQuery('search', `${value}`);
	};

	render() {
		const { selectedRowKeys, shareButtonText, searchTerm, notShareYet, mutationQuery, defaultSelectValue } = this.state;
		const { match } = this.props;
		const myListId = match.params.id;

		const columns = [
			{
				title: 'id',
				dataIndex: 'node.id'
			},
			{
				title: 'name',
				dataIndex: 'node.name'
			}
		];

		const rowSelection = {
			selectedRowKeys,
			onChange: this.onSelectChange
		};

		const hasSelected = selectedRowKeys.length > 0;

		return (
			<Mutation mutation={mutationQuery}>
				{(queryName, { loading: mutationLoading }) => (
					<Modal
						title='Select clients'
						visible
						onCancel={this.handleCancel}
						wrapClassName='custom-modal box-parts title-center'
						maskClosable={false}
						footer={null}
					>
						<Row gutter={16} className='mb-20'>
							<Col className='gutter-row mb-10' xs={16} sm={8}>
								<Select defaultValue={defaultSelectValue} onChange={this.handleChange} size='large' style={{ width: '100%' }}>
									<Option value='notShare'>Not share yet</Option>
									<Option value='shared'>Shared</Option>
								</Select>
							</Col>
							<Col className='gutter-row mb-10' xs={8} sm={4}>
								<Button
									key='submit'
									type='primary'
									loading={mutationLoading}
									onClick={this.handleSubmit(queryName, myListId, selectedRowKeys)}
									disabled={!hasSelected}
									size='large'
									style={{ width: '100%' }}
								>
									{shareButtonText}
								</Button>
							</Col>
							<Col className='gutter-row mb-10' xs={24} sm={12}>
								<Search allowClear={true} placeholder='Search client name' size='large' onSearch={this.searchHandler} defaultValue={searchTerm} enterButton />
							</Col>
						</Row>
						<Query
							fetchPolicy='cache-and-network'
							query={getAssignedClientsOfSaler}
							variables={{ myListId: myListId, notShareYet: notShareYet, search: searchTerm }}
						>
							{({ data, loading: queryLoading, fetchMore, refetch }) => {
								this.refetch = refetch;

								if (queryLoading) {
									return <Spin className='collabspin' />;
								}

								const clients = data && data.assignedClientsOfSaler ? data.assignedClientsOfSaler.edges : [];
								const hasNextPage = data && data.assignedClientsOfSaler && data.assignedClientsOfSaler.pageInfo.hasNextPage;
								const endCursor = data && data.assignedClientsOfSaler ? data.assignedClientsOfSaler.pageInfo.endCursor : null;

								return (
									<div>
										<Table
											rowKey={({ node }) => node.id}
											rowSelection={rowSelection}
											columns={columns}
											dataSource={clients}
											pagination={false}
											loading={queryLoading}
											className='pb-30'
										/>
										{hasNextPage && (
											<div className='pb-30 text-center'>
												<Button type='primary' loading={queryLoading} onClick={() => this.loadMore(clients, fetchMore, endCursor)}>
													Load more
												</Button>
											</div>
										)}
									</div>
								);
							}}
						</Query>
					</Modal>
				)}
			</Mutation>
		);
	}
}

export default ShareListToClientsModal;
