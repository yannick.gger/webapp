import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Menu, Icon, message, Tag, Tooltip, Avatar, Divider, Spin, Modal } from 'antd';
import { Mutation, Query } from 'react-apollo';
import { injectIntl, FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router-dom';
import getCampaigns from 'graphql/get-campaigns.graphql';
import { firstOrganization } from 'services/auth-service';

import ButtonLink from 'components/button-link';
import { shortAmountFormatter } from 'components/formatters';

const { confirm } = Modal;

const ADD_INSTAGRAM_OWNER = gql`
	mutation addInstagramOwnersToCampaign($campaignId: ID!, $instagramOwnerIds: [ID]!) {
		addInstagramOwnersToCampaign(input: { campaignId: $campaignId, instagramOwnerIds: $instagramOwnerIds }) {
			campaign {
				id
				name
			}
		}
	}
`;

class Basket extends Component {
	state = {};

	showConfirm = (campaign, instagramOwnerIds, addInstagramOwnerToCampaign) => {
		const { id: campaignId, invitesSent, spotsLeft, invitesOpening } = campaign;

		confirm({
			content: `Are you sure you want to add these influencers to campaign? ${
				invitesSent && invitesOpening && spotsLeft > 0
					? "This time we will send invites to new influencers immediately without the need of admin's approval"
					: ''
			}`,
			title: 'Adding new influencers',
			onOk: () => {
				addInstagramOwnerToCampaign({
					variables: { campaignId, instagramOwnerIds }
				});
			}
		});
	};

	render() {
		const { id: organizationId } = firstOrganization();
		const instagramOwners = this.props.instagramOwners;
		const hasSelected = this.props.instagramOwners.length > 0;
		return (
			<Mutation
				mutation={ADD_INSTAGRAM_OWNER}
				refetchQueries={['getCampaigns']}
				onCompleted={({ addInstagramOwnersToCampaign }) => {
					if (this.props.handleSuccess) {
						this.props.handleSuccess();
					}
					message.success(
						this.props.intl.formatMessage(
							{
								id: 'add-instagram-owners-to-campaign-success',
								defaultMessage: `Added {count} {count, plural, one {influencer} other {influencers}} to campaign "{campaignName}"`
							},
							{ count: instagramOwners.length, campaignName: addInstagramOwnersToCampaign.campaign.name }
						)
					);
				}}
				onError={({ graphQLErrors }) => {
					if (graphQLErrors) {
						graphQLErrors.forEach((error) => {
							message.error(error.message);
						});
					}
				}}
			>
				{(addInstagramOwnerToCampaign) => (
					<Query query={getCampaigns} variables={{ includeCanAddInfluencerCampaignsOnly: true, organizationId }}>
						{({ data, loading: queryLoading }) => {
							return (
								<div className={this.state.active && 'active'} onClick={() => this.setState({ active: !this.state.active })}>
									<div className={!hasSelected ? 'basket' : 'basket has-selected'}>
										<div className='basket-container'>
											<div className='headline'>
												<span>
													<FormattedMessage
														id='instagram-owners-to-invite-count'
														defaultMessage={`{count} {count, plural, one {influencer} other {influencers}} selected`}
														values={{
															count: instagramOwners.length
														}}
													/>
												</span>
												<ButtonLink className='link-styling fr'>
													<Icon type='up-square-o' />
												</ButtonLink>
											</div>
											<div className='content'>
												<h4>Your selection</h4>
												<div className='avatar-list'>
													{instagramOwners.slice(0, instagramOwners > 10 ? 11 : 10).map((node) => (
														<Tooltip placement='topLeft' key={node.id} title={`${node.id}`}>
															<Avatar size='large' src='#' icon='user' />
														</Tooltip>
													))}

													{instagramOwners.length > 10 && (
														<Avatar size='large' key='more'>
															+{instagramOwners.length - 11}
														</Avatar>
													)}
												</div>
												<Divider />
												<h4>Add influencers to...</h4>
												{queryLoading && <Spin className='collabspin' />}
												{data && data.campaigns && data.campaigns.edges && (
													<Menu>
														{false && (
															<Menu.ItemGroup>
																<Menu.Item className='add-new-menu-item'>
																	<ButtonLink>
																		<Icon type='plus' /> Add to new campaign
																	</ButtonLink>
																</Menu.Item>
															</Menu.ItemGroup>
														)}
														<Menu.ItemGroup className='existing'>
															{data.campaigns.edges.map((campaign) => {
																return (
																	<Menu.Item data-ripple='rgba(132,146, 164, 0.2)' key={campaign.node.id}>
																		<ButtonLink
																			onClick={() => this.showConfirm(campaign.node, instagramOwners.map(({ id }) => id), addInstagramOwnerToCampaign)}
																		>
																			{campaign.node.name}

																			{campaign.node.influencerTargetCount - campaign.node.instagramOwnersInvitedCount > 0 && (
																				<Tag color='#3A7DE3' className='ml-20 fr' style={{ marginTop: '12px', marginRight: '0' }}>
																					Needs {campaign.node.influencerTargetCount - campaign.node.instagramOwnersInvitedCount} more
																				</Tag>
																			)}

																			{campaign.node.active ? (
																				<Tag color='green' className='ml-20 fr' style={{ marginTop: '12px', marginRight: '0' }}>
																					Active
																				</Tag>
																			) : campaign.node.draft ? (
																				<Tag color='orange' className='ml-20 fr' style={{ marginTop: '12px', marginRight: '0' }}>
																					Draft
																				</Tag>
																			) : (
																				''
																			)}

																			<span className='fr'>{campaign.node.instagramOwnersInvitedCount} in campaign</span>
																		</ButtonLink>
																	</Menu.Item>
																);
															})}
														</Menu.ItemGroup>
													</Menu>
												)}
											</div>
										</div>
									</div>
								</div>
							);
						}}
					</Query>
				)}
			</Mutation>
		);
	}
}

export default withRouter(injectIntl(Basket));
