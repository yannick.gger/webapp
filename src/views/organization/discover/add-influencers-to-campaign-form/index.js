import React from 'react';
import { Form, Button, message, Modal } from 'antd';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { Mutation } from 'react-apollo';
import addInstagramOwnersToCampaign from 'graphql/add-instagram-owners-to-campaign.graphql';
import CampaignsWithSearchBar from '../campaign-with-search.js';

const { confirm } = Modal;

class AddInfluencersToCampaignForm extends React.Component {
	showConfirm = (campaign, instagramOwnerIds, addInstagramOwnersToCampaign) => {
		const { id: campaignId, invitesSent, spotsLeft, invitesOpening } = campaign;

		confirm({
			content: `Are you sure you want to add these influencers to campaign? ${
				invitesSent && invitesOpening && spotsLeft > 0
					? "This time we will send invites to new influencers immediately without the need of admin's approval"
					: ''
			}`,
			title: 'Adding new influencers',
			onOk: () => {
				addInstagramOwnersToCampaign({
					variables: { campaignId, instagramOwnerIds }
				});
			}
		});
	};

	render() {
		const { location, handleError, handleSuccess } = this.props;
		const columns = [
			{
				title: 'Campaign name',
				key: 'name',
				width: '300px',
				render: (record) => <div className='item-name-in-selection-modal'>{record.node.name}</div>
			},
			{
				title: 'action',
				key: 'action',
				width: '300px',
				render: (record) => {
					return (
						<React.Fragment>
							<Mutation
								mutation={addInstagramOwnersToCampaign}
								onError={() => {
									if (handleError) {
										handleError();
									}
								}}
								onCompleted={() => {
									if (handleSuccess) {
										handleSuccess();
									}
									message.success(`Added influencer(s) to your campaign`);
								}}
							>
								{(addInstagramOwnersToCampaign) => (
									<Button
										size='small'
										type='primary'
										onClick={() => this.showConfirm(record.node, location && location.state.instagramOwners, addInstagramOwnersToCampaign)}
									>
										Add
									</Button>
								)}
							</Mutation>
						</React.Fragment>
					);
				}
			}
		];
		return <CampaignsWithSearchBar columns={columns} />;
	}
}

const WrappedAddInfluencersToCampaignForm = Form.create()(AddInfluencersToCampaignForm);

export default withRouter(injectIntl(WrappedAddInfluencersToCampaignForm));
