import React from 'react';
import { Modal } from 'antd';

import AddInfluencersToCampaignForm from '../add-influencers-to-campaign-form';

class AddInfluencersToCampaign extends React.Component {
	handleCancel = () => {
		this.props.history.goBack();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		const { match } = this.props;
		return (
			<Modal
				className='add-selection-modal'
				title='Add influencers to campaign'
				visible
				onOk={this.handleOk}
				onCancel={this.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
			>
				<AddInfluencersToCampaignForm organizationSlug={match.params.organizationSlug} />
			</Modal>
		);
	}
}

export default AddInfluencersToCampaign;
