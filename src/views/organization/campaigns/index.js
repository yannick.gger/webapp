import React, { Component } from 'react';
import { Menu, Dropdown, Button, Layout } from 'antd';

import OrganizationLink from 'components/organization-link/';
import CampaignList from 'components/campaign-list/';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';

const MenuItemGroup = Menu.ItemGroup;
const menu = (
	<Menu>
		<MenuItemGroup title='Create'>
			<Menu.Item key='0'>
				<OrganizationLink to='/campaigns/create'>Create new campaign</OrganizationLink>
			</Menu.Item>
		</MenuItemGroup>
	</Menu>
);

export default class CampaignsView extends Component {
	render() {
		document.title = `Campaigns | Collabs`;

		return (
			<Layout>
				<Layout.Header>
					<h1>Campaigns</h1>
					<Dropdown overlay={menu} trigger={['click']} placement='bottomRight'>
						<Button type='primary' icon=' ion ion-plus-round' />
					</Dropdown>
				</Layout.Header>
				<Layout.Content>
					<CampaignList />
				</Layout.Content>
			</Layout>
		);
	}
}
