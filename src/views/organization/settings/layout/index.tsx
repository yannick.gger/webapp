import withTheme from 'hocs/with-theme';
import SettingsContainer from 'components/Settings';
import getCurrentUser from 'graphql/get-current-user.graphql';
import { Query } from 'react-apollo';
import LoadingSpinner from 'components/LoadingSpinner';

type QueryType = {
	loading: boolean;
	error?: any;
	data: any;
};

const SettingsView = () => {
	return (
		<Query query={getCurrentUser} fetchPolicy='no-cache'>
			{({ loading, error, data }: QueryType) => {
				if (loading) return <LoadingSpinner />;
				if (error || !data || !data.currentUser) return <p>Could not load user</p>;
				return <SettingsContainer currentUser={data.currentUser} />;
			}}
		</Query>
	);
};

export default withTheme(SettingsView, 'settings');
