import React from 'react';
import { Spin } from 'antd';
import UserSettingsForm from 'components/user-form';
import OrganizationSettingsLayout from '../../layout';
import { withOrganization } from 'hocs';
import { Query } from 'react-apollo';
import getCurrentUser from 'graphql/get-current-user.graphql';

class UserSettings extends React.Component {
	render() {
		return (
			<Query query={getCurrentUser} fetchPolicy='no-cache'>
				{({ loading, error, data }) => {
					if (loading) return <Spin className='collabspin' />;
					if (error || !data || !data.currentUser) return <p>Could not load user</p>;
					return (
						<OrganizationSettingsLayout>
							<UserSettingsForm fields={{ organization: data.currentUser }} bordered={true} />
						</OrganizationSettingsLayout>
					);
				}}
			</Query>
		);
	}
}

export default withOrganization(UserSettings);
