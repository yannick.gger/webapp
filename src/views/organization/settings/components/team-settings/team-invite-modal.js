import React, { Component } from 'react';
import { Modal, Button, Card, Form, Input, message } from 'antd';
import { Mutation } from 'react-apollo';
import { withOrganization } from 'hocs';
import createTeamMemberInvite from 'graphql/create-team-member-invite.graphql';

const FormItem = Form.Item;

class InviteForm extends React.Component {
	render() {
		const { form } = this.props;
		const { getFieldDecorator } = form;

		return (
			<React.Fragment>
				<div className='description'>
					<p>{`When you have invited an team member they will receive an email with a link to a form where they can join your organization in Collabs. The email address can't be changed.`}</p>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>Information</h3>
					<Card className='small-title mb-10 mt-10'>
						<FormItem label='Email'>
							{getFieldDecorator('memberEmail', {
								rules: [{ type: 'email', message: 'The input is not valid E-mail!' }, { required: true, message: 'Please enter an email address' }]
							})(<Input size='large' />)}
						</FormItem>
					</Card>
				</div>
			</React.Fragment>
		);
	}
}

const WrappedInviteForm = Form.create()(InviteForm);

class TeamInviteModal extends Component {
	handleSubmit = (createMemberInvite) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;
				const graphqlValues = {
					...values,
					organizationId: this.props.organization.id
				};

				createMemberInvite({
					variables: graphqlValues
				})
					.then(({ data, error }) => {
						if (data.createMemberInvite && !error && data.createMemberInvite.errors.length === 0) {
							message.info('You have invited the team member');
							this.props.closeModal();
						} else {
							data.createMemberInvite.errors.forEach((error) => {
								message.error(error.message);
							});
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		return (
			<Mutation mutation={createTeamMemberInvite}>
				{(createTeamMemberInvite, { loading }) => (
					<Modal
						title='Send an team invite'
						visible
						onOk={this.handleOk}
						onCancel={this.handleCancel}
						wrapClassName='custom-modal box-parts title-center'
						maskClosable={false}
						footer={[
							<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(createTeamMemberInvite)}>
								Send invite
							</Button>
						]}
					>
						<WrappedInviteForm wrappedComponentRef={this.saveFormRef} />
					</Modal>
				)}
			</Mutation>
		);
	}
}

export default withOrganization(TeamInviteModal);
