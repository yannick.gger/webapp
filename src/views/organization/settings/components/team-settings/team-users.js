import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { Dropdown, Button, Modal, Icon, Table, Spin, Empty, Menu, message } from 'antd';
import TableErrorImg from 'assets/img/app/table-error.svg';
import { ArrowOutlineIcon } from 'components/icons';
import grantTeamMemberPrivilege from 'graphql/grant-team-member-privilege.graphql';

const OwnerIcon = () => <Icon type='crown' theme='twoTone' twoToneColor='#ebc92f' />;
const AdminIcon = ({ style }) => <Icon type='idcard' theme='twoTone' twoToneColor='#2f8deb' style={style} />;
const MemberIcon = () => <Icon type='user' theme='outlined' style={{ color: '#5edb30' }} />;

class TeamUsers extends Component {
	state = { currentUserRole: 'member', loading: [] };

	handleRoleChange = (e, node, i) => {
		const { currentUserId } = this.props;
		const { currentUserRole } = this.state;
		const newRole = e.key;
		let error = '';

		if (currentUserId === node.userId) error = 'You cannot edit your own role.';
		else if (newRole === 'owner' && currentUserRole !== 'owner') error = "You don't have permission to do that.";

		if (error || node.role === newRole) return message.error(error);

		if (newRole === 'owner') {
			Modal.confirm({
				title: 'Are you sure you want to transfer ownership of the organization?',
				content: (
					<p>
						As an organization can only have one owner your role will be set to Admin.
						<br />
						<span className='bold'>This action cannot be reverted.</span>
					</p>
				),
				okText: 'Yes',
				okType: 'danger',
				cancelText: 'No',
				onOk: () => this.handleRoleMutation(node, newRole, i)
			});
		} else this.handleRoleMutation(node, newRole, i);
	};

	handleRoleMutation = (node, newRole, i) => {
		const { client } = this.props;
		let updatedData = [...this.state.tableDataSource];
		let editedIndexes = [i];
		let currentUserNode = undefined;

		// Get current user node and index if transfer of ownership
		if (newRole === 'owner') {
			currentUserNode = updatedData
				.map(({ node }, i) => {
					if (node.userId === this.props.currentUserId) return [node, i];
				})
				.filter((i) => i !== undefined)[0];

			editedIndexes.push(currentUserNode[1]);
		}

		this.setState((prevState) => ({ loading: [...prevState.loading, ...editedIndexes] }));

		client
			.mutate({
				mutation: grantTeamMemberPrivilege,
				variables: { memberId: node.userId, teamId: node.organizationId, privilege: newRole }
			})
			.then((res) => {
				if (res.data.grantTeamMemberPrivilege.errors.length === 0) {
					// Handle if owner transfers ownership
					if (currentUserNode) updatedData[currentUserNode[1]] = { node: { ...currentUserNode[0], role: 'admin' } };

					// Handle update of user
					updatedData[i] = { node: { ...node, role: newRole } };

					// Render changes
					this.setState((prevState) => ({ tableDataSource: updatedData, loading: prevState.loading.filter((num) => !editedIndexes.includes(num)) }));
					message.success(`Role updated for ${node.user.name}`);
				} else {
					this.setState((prevState) => ({ loading: prevState.loading.filter((num) => !editedIndexes.includes(num)) }));
				}
			})
			.catch(({ graphQLErrors }) => {
				this.setState((prevState) => ({ loading: prevState.loading.filter((num) => !editedIndexes.includes(num)) }));
				if (graphQLErrors)
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
			});
	};

	componentDidMount() {
		const { tableDataSource, currentUserRole } = this.props;
		this.setState({ tableDataSource, currentUserRole });
	}

	render() {
		const { emptyState, error, loading } = this.props;
		const { tableDataSource } = this.state;

		const roleSelect = (node, i) => {
			return (
				<Menu className='multi-line' style={{ width: 300 }} selectedKeys={[node.role]} onClick={(e) => this.handleRoleChange(e, node, i)}>
					<Menu.Item key='owner'>
						<OwnerIcon />
						<span className='bold'>Owner</span>
						<br />
						<p>Transfer ownership of the organization.</p>
					</Menu.Item>
					<Menu.Divider />
					<Menu.Item key='admin'>
						<AdminIcon style={{ marginLeft: 1 }} />
						<span className='bold'>Admin</span>
						<br />
						<p>Grant admin permissions allowing the user to add team members, modify roles, edit billing and more.</p>
					</Menu.Item>
					<Menu.Divider />
					<Menu.Item key='member'>
						<MemberIcon />
						<span className='bold'>Member</span>
						<br />
						<p>Set membership permissions. Standard for new team members.</p>
					</Menu.Item>
				</Menu>
			);
		};
		const columns = [
			{
				title: 'Name',
				key: '1',
				dataIndex: 'node.name',
				width: 250,
				render: (name) => {
					return name ? name : <span style={{ fontStyle: 'italic' }}>New member</span>;
				}
			},
			{
				title: 'Email',
				key: '2',
				dataIndex: 'node.email'
			},
			{
				title: 'Status',
				key: '3',
				dataIndex: 'node.status'
			},
			{
				title: 'Role',
				width: 332,
				dataIndex: 'node.role',
				render: (role, { node }, i) => {
					const loading = this.state.loading.includes(i);
					const ownUser = this.props.currentUserId === node.userId;
					const disabled = loading || ownUser || role === 'owner' || role === null || this.state.currentUserRole === 'member';
					return (
						<Dropdown className='d-flex' overlay={() => roleSelect(node, i)} style={{ minWidth: 'unset' }} trigger={['click']} disabled={disabled}>
							<Button style={{ textAlign: 'left' }} block>
								{loading ? (
									<React.Fragment>
										<Spin size='small' indicator={<Icon type='loading' />} style={{ alignSelf: 'center', marginRight: 10 }} />
										<span style={{ flex: 1 }}>Loading...</span>
									</React.Fragment>
								) : (
									<React.Fragment>
										{role === 'owner' && (
											<React.Fragment>
												<OwnerIcon />
												<span style={{ flex: 1 }}>Owner</span>
											</React.Fragment>
										)}
										{role === 'admin' && (
											<React.Fragment>
												<AdminIcon />
												<span style={{ flex: 1 }}>Admin</span>
											</React.Fragment>
										)}
										{['member', null].includes(role) && (
											<React.Fragment>
												<MemberIcon />
												<span style={{ flex: 1 }}>Member</span>
											</React.Fragment>
										)}
									</React.Fragment>
								)}
								{!disabled && <Icon component={ArrowOutlineIcon} style={{ color: 'inherit' }} />}
							</Button>
						</Dropdown>
					);
				}
			}
		];

		return (
			<div>
				<Table
					className='pb-30'
					columns={columns}
					dataSource={tableDataSource}
					rowKey={(record) => record.node.email}
					pagination={false}
					loading={loading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load data' />
						) : (
							<Empty description={emptyState || 'Nothing matching the search criterias found'} />
						)
					}}
				/>
			</div>
		);
	}
}

export default withApollo(TeamUsers);
