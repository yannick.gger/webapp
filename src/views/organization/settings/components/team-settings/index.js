import React, { Component } from 'react';
import { Spin } from 'antd';
import { Query } from 'react-apollo';
import OrganizationSettingsLayout from '../../layout';
import { withOrganization } from 'hocs';
import getOrganizationTeamMembers from 'graphql/get-organization-team-members.graphql';
import TeamUsers from './team-users';

class TeamSettings extends Component {
	handleClick = (e) => {
		this.setState({
			current: e.key
		});
	};

	handleSearch = (value) => {
		this.setState({ search: value });
	};

	setRef = (instance) => {
		this.userChild = instance;
	};

	render() {
		const { organization } = this.props;
		const { currentUserRole } = organization;

		return (
			<OrganizationSettingsLayout teamAdmin={currentUserRole}>
				<Query query={getOrganizationTeamMembers} variables={{ organizationId: this.props.organization.id }} notifyOnNetworkStatusChange fetchPolicy='no-cache'>
					{({ data, loading: queryLoading, fetchMore }) => {
						if (queryLoading && !data.users) {
							return <Spin className='collabspin' />;
						}

						const currentUser = data && data.currentUser;
						const tableDataSource = data && data.organizationTeamMembers ? data.organizationTeamMembers.edges : [];
						return (
							<TeamUsers
								ref={this.setRef}
								currentUserId={currentUser.id}
								currentUserRole={currentUserRole}
								tableDataSource={tableDataSource}
								fetchMore={fetchMore}
								loading={queryLoading}
							/>
						);
					}}
				</Query>
			</OrganizationSettingsLayout>
		);
	}
}

export default withOrganization(TeamSettings);
