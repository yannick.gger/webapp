import React from 'react';
import { Button, message, Card, Row, Col } from 'antd';
import CompanySettingsForm from './form';
import { Mutation } from 'react-apollo';
import updateOrganization from 'graphql/update-organization-settings.graphql';
import OrganizationSettingsLayout from '../../layout';
import { withOrganization } from 'hocs';
import { checkGhostUserIsSaler } from 'services/auth-service';
import GhostUserAccess from 'components/ghost-user-access';

class CompanySettings extends React.Component {
	handleSubmit = (updateOrganization) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				const res = await updateOrganization({ variables: { organization: { ...values.organization, id: this.props.organization.id } } });
				if (res.data.updateOrganization === null) {
					message.error('You are not authorized to do this');
				} else if (res.data.updateOrganization.errors.length > 0) {
					res.data.updateOrganization.errors.forEach((error) => message.error(error.message));
				} else {
					message.info('Settings updated');
				}
			});
		};
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		const readOnly = !['owner', 'admin'].includes(this.props.organization.currentUserRole);
		return (
			<Mutation mutation={updateOrganization}>
				{(updateOrganization, { loading }) => (
					<OrganizationSettingsLayout>
						<Row>
							<Col xs={24} sm={20}>
								<Card>
									<CompanySettingsForm fields={{ organization: this.props.organization }} wrappedComponentRef={this.saveFormRef} readOnly={readOnly} />
									<GhostUserAccess ghostUserIsSaler={checkGhostUserIsSaler()}>
										<div>
											<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(updateOrganization)} disabled={readOnly}>
												Save
											</Button>
										</div>
									</GhostUserAccess>
								</Card>
							</Col>
						</Row>
					</OrganizationSettingsLayout>
				)}
			</Mutation>
		);
	}
}

export default withOrganization(CompanySettings);
