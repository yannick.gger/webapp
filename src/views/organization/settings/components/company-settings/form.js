import React, { Component } from 'react';
import { Form } from 'antd';
import withCollabsForm from 'services/collabs-form-decorator';
import OrganizationFormItems from 'components/organization-form-items';

class CompanySettingsForm extends Component {
	render() {
		const { form, fields, readOnly, wrappedComponentRef } = this.props;

		return (
			<Form layout='vertical'>
				<OrganizationFormItems form={form} fields={fields} wrappedComponentRef={wrappedComponentRef} readOnly={readOnly} />
			</Form>
		);
	}
}
export default withCollabsForm(CompanySettingsForm);
