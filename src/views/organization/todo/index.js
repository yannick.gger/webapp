import React, { Component } from 'react';
import { Layout } from 'antd';

import WrappedAcceptInvite from './influencers/accept-invite.js';
import TodoAssignment from './influencers/assignment.js';
import TodoProduct from './influencers/products.js';
import FlaggedFeature from 'components/flagged-feature';

class TodoContainer extends Component {
	render() {
		document.title = `To do | Collabs`;

		return (
			<Layout>
				<FlaggedFeature name='recommendation'>{({ enabled }) => (enabled ? 'Recommendation is enabled' : 'Recommendation is not enabled')}</FlaggedFeature>
				<Layout.Header>
					<h1>To do</h1>
				</Layout.Header>
				<Layout.Content>
					<React.Fragment>
						<div className='todo-container'>
							<WrappedAcceptInvite />
							<TodoProduct />
							<TodoAssignment />
						</div>
					</React.Fragment>
				</Layout.Content>
			</Layout>
		);
	}
}

export default TodoContainer;
