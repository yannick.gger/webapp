import React from 'react';
import { Row, Col, Button, Card, Checkbox, Divider, Form, Input, InputNumber } from 'antd';

import IconAcceptDeclineInvite from '../icons/icon-accept-or-decline.svg';
import IconTaskDone from '../icons/icon-task-done.svg';
import IconTaskError from '../icons/icon-error.svg';

const FormItem = Form.Item;

class AcceptInvite extends React.Component {
	render() {
		document.title = `To do | Collabs`;

		const { form } = this.props;
		const { getFieldDecorator, getFieldValue } = form;

		const subItemFormLayoutLargeLabel = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 16 }
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 8 }
			}
		};

		return (
			<Form onSubmit={this.handleSubmit} className='login-form'>
				<div className='todo-container'>
					<div className='todo-card-group'>
						<a href={null} className='tag large blue fr'>
							View campaign
						</a>
						<h3>Accept Invite to join "Campaign name"</h3>

						<Card
							title={
								<React.Fragment>
									<img src={IconAcceptDeclineInvite} alt='' className='icon-size-30' />
									<span style={{ marginRight: '10px' }}>Invitation:</span> Accept or decline invite
								</React.Fragment>
							}
							extra={<span className='tag red'>Expires in 2 days</span>}
							hoverable
						>
							<div className='flex-listing metrics m-bordered'>
								<div>
									<h4>Campaign from</h4>
									<h3>Brand name</h3>
								</div>
								<div>
									<h4>Payment</h4>
									<h3>€3000</h3>
								</div>
								<div>
									<h4>Assignments</h4>
									<h3>1 post, 1 story</h3>
								</div>
								<div>
									<h4>Dates</h4>
									<h3>24-28 Nov</h3>
								</div>
								<div>
									<h4>Spots left</h4>
									<h3>16</h3>
								</div>
							</div>
							<Divider />
							<Row gutter={20}>
								<Col sm={{ span: 12 }}>
									<Button type='primary' data-ripple='' className='ant-btn-block'>
										I’m interested, open invitation
									</Button>
								</Col>
								<Col sm={{ span: 12 }}>
									<Button type='bordered' data-ripple='' className='ant-btn-block'>
										No thanks, I’ll pass
									</Button>
								</Col>
							</Row>
						</Card>

						<Card
							title={
								<React.Fragment>
									<img src={IconAcceptDeclineInvite} alt='' className='icon-size-30' />
									<span style={{ marginRight: '10px' }}>Invitation:</span> Accept or decline invite
								</React.Fragment>
							}
							extra={<span className='tag red'>Expires in 2 days</span>}
							hoverable
						>
							<div className='flex-listing metrics m-bordered'>
								<div>
									<h4>Campaign from</h4>
									<h3>Brand name</h3>
								</div>
								<div>
									<h4>Payment</h4>
									<h3>€3000</h3>
								</div>
								<div>
									<h4>Assignments</h4>
									<h3>1 post, 1 story</h3>
								</div>
								<div>
									<h4>Dates</h4>
									<h3>24-28 Nov</h3>
								</div>
								<div>
									<h4>Spots left</h4>
									<h3>16</h3>
								</div>
							</div>
							<Divider />
							<Row gutter={20}>
								<Col sm={{ span: 12 }}>
									<Button type='primary' data-ripple='' className='ant-btn-block'>
										I’m interested, open invitation
									</Button>
								</Col>
								<Col sm={{ span: 12 }}>
									<Button type='bordered' data-ripple='' className='ant-btn-block'>
										No thanks, I’ll pass
									</Button>
								</Col>
							</Row>
							<Divider />

							<div className='checkbox-tabs'>
								<Row className=''>
									<Col xs={{ span: 24 }} className='checkbox-wrapper'>
										<FormItem>
											{getFieldDecorator('notEnoughCompensation', { initialValue: false })(<Checkbox size='large'>The payment is too low</Checkbox>)}
										</FormItem>
										{getFieldValue('notEnoughCompensation') && (
											<div className='sub-item'>
												<FormItem label='Expected payment (EUR)' {...subItemFormLayoutLargeLabel}>
													{getFieldDecorator('expectedCompensation')(<InputNumber size='large' placeholder='Price' className='fr' />)}
												</FormItem>
											</div>
										)}
									</Col>
								</Row>
								<Row>
									<Col xs={{ span: 24 }} className='checkbox-wrapper'>
										<FormItem>
											{getFieldDecorator('notInterestingCompensation', { initialValue: false })(
												<Checkbox size='large'>The payment is not of interest</Checkbox>
											)}
										</FormItem>
									</Col>
								</Row>

								<Row>
									<Col xs={{ span: 24 }} className='checkbox-wrapper'>
										<FormItem>
											{getFieldDecorator('notForMyAudience', { initialValue: false })(<Checkbox size='large'>The campaign doesn't fit my followers</Checkbox>)}
										</FormItem>
									</Col>
								</Row>

								<Row>
									<Col xs={{ span: 24 }} className='checkbox-wrapper'>
										<FormItem>
											{getFieldDecorator('noSponsoredPosts', { initialValue: false })(<Checkbox size='large'>I don't do sponsored posts</Checkbox>)}
										</FormItem>
									</Col>
								</Row>

								<Row>
									<Col xs={{ span: 24 }} className='checkbox-wrapper'>
										<FormItem>
											{getFieldDecorator('badDate', { initialValue: false })(<Checkbox size='large'>The date doesn't fit my schedule</Checkbox>)}
										</FormItem>
									</Col>
								</Row>

								<Row gutter={15} className=''>
									<Col xs={{ span: 24 }} className='checkbox-wrapper'>
										<FormItem>{getFieldDecorator('other', { initialValue: false })(<Checkbox size='large'>I have another reason</Checkbox>)}</FormItem>
										{getFieldValue('other') && (
											<div className='sub-item text-area'>
												{getFieldDecorator('otherReason')(<Input.TextArea size='large' placeholder='Förklara gärna lite kort om varför' />)}
											</div>
										)}
									</Col>
								</Row>

								<Divider />

								<Button className='ant-btn-block'>Decline the invite</Button>
							</div>
						</Card>

						<Card
							title={
								<React.Fragment>
									<img src={IconTaskError} alt='' className='icon-size-30' />
									<span style={{ marginRight: '10px' }}>
										Invite declined
										<br />
										<h6>You have chosen not to join the campaign "Name"</h6>
									</span>
								</React.Fragment>
							}
							extra={
								<a href='#' className='tag blue large'>
									View invite
								</a>
							}
							data-ripple=''
							hoverable
						>
							<Row className='flex-listing reasons'>
								<Col sm={{ span: 5 }}>
									<p className='mb-0'>No thanks, I'll pass</p>
								</Col>
								<Col sm={{ span: 15 }} className='checkbox-wrapper'>
									<div className='checkbox-tabs'>
										<FormItem className='mb-0'>
											{getFieldDecorator('noSponsoredPosts', { initialValue: true })(
												<Checkbox size='large' disabled>
													I don't do sponsored posts
												</Checkbox>
											)}
										</FormItem>
									</div>
								</Col>
								<Col sm={{ span: 4 }}>
									<a href='#' className='tag large blue fr'>
										Change reason
									</a>
								</Col>
							</Row>
						</Card>

						<Card
							title={
								<React.Fragment>
									<img src={IconTaskDone} alt='' className='icon-size-30' />
									<span style={{ marginRight: '10px' }}>
										Invite accepted
										<br />
										<h6>You have joined the campaign "Name"</h6>
									</span>
								</React.Fragment>
							}
							extra={<span className='tag green'>Complete</span>}
							className='empty-card'
							data-ripple=''
							hoverable
						/>
					</div>
				</div>
			</Form>
		);
	}
}

const WrappedAcceptInvite = Form.create()(AcceptInvite);

export default WrappedAcceptInvite;
