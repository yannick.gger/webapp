import React, { Component } from 'react';
import { Timeline, Button, Card, Icon, Avatar, Divider, Row, Col } from 'antd';

import IconAcceptDeclineInvite from '../icons/icon-accept-or-decline.svg';
import IconTaskDone from '../icons/icon-task-done.svg';
import IconTaskError from '../icons/icon-error.svg';
import IconProducts from '../icons/icon-product.svg';
import IconPostInsta from '../icons/icon-post-on-insta.svg';

class TodoAssignment extends Component {
	render() {
		return (
			<div className='todo-card-group'>
				<h3>Assignment 1: Upload video on stories</h3>
				<Card
					title={
						<React.Fragment>
							<img src={IconAcceptDeclineInvite} alt='' className='icon-size-30' />
							<span style={{ marginRight: '10px' }}>Content Review:</span> Upload content for review
						</React.Fragment>
					}
					extra={<span className='tag red'>Expires in 2 days</span>}
					hoverable
				>
					<Button type='primary' data-ripple='' className='ant-btn-block'>
						Upload content for review
					</Button>
				</Card>
				<Card
					title={
						<React.Fragment>
							<img src={IconProducts} alt='' className='icon-size-30' />
							<span style={{ marginRight: '10px' }}>Content Review:</span> Waiting for approval
						</React.Fragment>
					}
					extra={<span className='tag red'>Expires in 2 days</span>}
					hoverable
				>
					<p>Timeline</p>
					<Timeline>
						<Timeline.Item color='#4AD270'>
							<span style={{ color: '#6CB777' }}>Waiting for delivery</span>
							<span className='extra'>Will be delivered between 10 - 14th August</span>
						</Timeline.Item>
						<Timeline.Item color='#4AD270'>
							<span style={{ color: '#6CB777' }}>Upload post for review</span>
							<span className='extra'>Completed on 2018-08-05</span>
						</Timeline.Item>
						<Timeline.Item dot={<Icon type='clock-circle-o' style={{ color: '#e2ab40' }} />}>
							<span style={{ color: '#D5A545' }}>Waiting for the post to be approved</span>
							<span className='extra'>Will be approved at latest in 3 days</span>
						</Timeline.Item>
						<Timeline.Item color='#B4C4D7'>
							Post has been approved<span className='extra'>-</span>
						</Timeline.Item>
					</Timeline>
				</Card>

				<Card
					title={
						<React.Fragment>
							<img src={IconTaskError} alt='' className='icon-size-30' />
							<span style={{ marginRight: '10px' }}>Content Review:</span> Post declined
						</React.Fragment>
					}
					extra={<span className='tag red'>Expires in 2 days</span>}
				>
					<div className='speech-bubble-container mb-20'>
						<h4 className='headline'>Message from the brand</h4>
						<div className='speech-bubble-wrapper'>
							<span className='brand'>
								<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }} size={30}>
									CS
								</Avatar>
							</span>
							<div className='speech-bubbles'>
								<span className='writer'>Christina Salvedo</span>
								<p>The background was not according to the brief, needs to be purple! Otherwise a great photo.</p>
							</div>
						</div>
					</div>

					<Button type='primary' data-ripple='' className='ant-btn-block'>
						Upload revamped post for content review
					</Button>
					<Divider />
					<p>Timeline</p>
					<Timeline>
						<Timeline.Item color='#4AD270'>
							<span style={{ color: '#6CB777' }}>Waiting for delivery</span>
							<span className='extra'>Will be delivered between 10 - 14th August</span>
						</Timeline.Item>
						<Timeline.Item color='#4AD270'>
							<span style={{ color: '#6CB777' }}>Upload post for review</span>
							<span className='extra'>Completed on 2018-08-05</span>
						</Timeline.Item>
						<Timeline.Item color='#4AD270'>
							<span style={{ color: '#6CB777' }}>Waiting for the post to be approved</span>
							<span className='extra'>Will be approved at latest in 3 days</span>
						</Timeline.Item>
						<Timeline.Item color='#F36382'>
							<span style={{ color: '#F36382' }}>Post declined</span>
							<span className='extra'>Declined on 2018-08-05</span>
						</Timeline.Item>
						<Timeline.Item dot={<Icon type='clock-circle-o' style={{ color: '#e2ab40' }} />}>
							<span style={{ color: '#D5A545' }}>Upload revamped post for review</span>
							<span className='extra'>Must be uploaded within the next 2 days</span>
						</Timeline.Item>
						<Timeline.Item color='#B4C4D7'>
							Waiting for revamped post to be approved<span className='extra'>Will be approved at latest in 3 days</span>
						</Timeline.Item>
						<Timeline.Item color='#B4C4D7'>
							Post has been approved<span className='extra'>-</span>
						</Timeline.Item>
					</Timeline>
				</Card>

				<Card
					title={
						<React.Fragment>
							<img src={IconTaskDone} alt='' className='icon-size-30' />
							<span style={{ marginRight: '10px' }}>Content Review:</span> Post has been approved
						</React.Fragment>
					}
					extra={<span className='tag green'>Complete</span>}
					className='empty-card'
					data-ripple=''
					hoverable
				/>

				<Card
					title={
						<React.Fragment>
							<img src={IconPostInsta} alt='' className='icon-size-30' />
							<span style={{ marginRight: '10px' }}>Time to post:</span> Post on Instagram Stories
						</React.Fragment>
					}
					extra={<span className='tag red'>Post sometime in the next 3 days</span>}
				>
					<Row>
						<Col sm={{ span: 9 }}>
							<p>What to post?</p>
							<h4 className='color-blue'>Image</h4>
							<p>
								This post is a Instagram Story and is for the assignment <a href='#'>Assignment name</a>
							</p>
							<a href='#' className='tag large blue'>
								View assignment
							</a>
						</Col>
						<Col sm={{ span: 14, offset: 1 }}>
							<div
								className='assignment-content-to-post'
								style={{
									backgroundImage:
										'url(https://storage.googleapis.com/listagram-attachments/assignment_review_media/photos/a6b8b771dda550ff035412ba0a55696616305746.jpg?1541957096)'
								}}
							/>
							<Row gutter={20}>
								<Col xs={{ span: 12 }}>
									<Button type='primary' data-ripple='' className='ant-btn-block'>
										Download content
									</Button>
								</Col>
								<Col xs={{ span: 12 }}>
									<Button type='bordered' data-ripple='' className='ant-btn-block'>
										View content
									</Button>
								</Col>
							</Row>
						</Col>
					</Row>
					<Divider />
					<Row>
						<Col sm={{ span: 9 }}>
							<h4 className='color-blue'>Caption</h4>
							<p>The caption, tags and mentions that has been approved with the post</p>
							<a href='#' className='tag large blue'>
								Copy caption and tags
							</a>
						</Col>
						<Col sm={{ span: 14, offset: 1 }}>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquam eros id lacus pulvinar vehicula. Integer eget consectetur ante. Nulla
								semper eu orci id aliquet.
							</p>
							<p>#hashtag #companyname @mention company</p>
						</Col>
					</Row>
					<Divider />
					<Row>
						<Col sm={{ span: 9 }}>
							<h4 className='color-blue'>Date to post</h4>
							<p>At latest in 3 days</p>
						</Col>
						<Col sm={{ span: 14, offset: 1 }}>
							<p>Anytime between 3-6 November</p>
						</Col>
					</Row>
					<Divider />
					<Button type='primary' data-ripple='' className='ant-btn-block'>
						I have posted on Insta, I’m done
					</Button>
				</Card>

				<Card
					title={
						<React.Fragment>
							<img src={IconTaskDone} alt='' className='icon-size-30' />
							<span style={{ marginRight: '10px' }}>Time to post:</span> Posted on Instagram Stories
						</React.Fragment>
					}
					extra={<span className='tag green'>Complete</span>}
					className='empty-card'
					data-ripple=''
					hoverable
				/>
			</div>
		);
	}
}

export default TodoAssignment;
