import React, { Component } from 'react';
import { Timeline, Card } from 'antd';

import IconTaskDone from '../icons/icon-task-done.svg';
import IconTaskError from '../icons/icon-error.svg';
import IconProducts from '../icons/icon-product.svg';

class TodoProduct extends Component {
	render() {
		return (
			<div className='todo-card-group'>
				<h3>Products</h3>
				<Card
					title={
						<React.Fragment>
							<img src={IconProducts} alt='' className='icon-size-30' />
							<span>
								Skull Candy Headphones
								<br />
								<h6>Product sent: Expected delivery between 10 - 14th August</h6>
							</span>
						</React.Fragment>
					}
					extra={<span className='tag red'>Expires in 2 days</span>}
					hoverable
				>
					<p>Timeline</p>
					<Timeline>
						<Timeline.Item color='#4AD270'>
							<span style={{ color: '#6CB777' }}>Product sent with carrier</span>
							<span className='extra'>Completed on 2018-05-23</span>
						</Timeline.Item>
						<Timeline.Item color='#FFC759'>
							<span style={{ color: '#D5A545' }}>Waiting for delivery</span>
							<span className='extra'>Will be delivered between 10 - 14th August</span>
						</Timeline.Item>
						<Timeline.Item color='#B4C4D7'>
							Product should have been delivered<span className='extra'>-</span>
						</Timeline.Item>
						<Timeline.Item color='#F36382'>
							<span style={{ color: '#F36382' }}>Error sample</span>
							<span className='extra'>-</span>
						</Timeline.Item>
					</Timeline>
				</Card>
				<Card
					title={
						<React.Fragment>
							<img src={IconTaskDone} alt='' className='icon-size-30' />
							<span>
								Skull Candy Headphones
								<br />
								<h6>Product should have been delivered. Didn’t get it? Click for more info</h6>
							</span>
						</React.Fragment>
					}
					extra={<span className='tag green'>Complete</span>}
					className='empty-card'
					data-ripple=''
					hoverable
				/>
				<Card
					title={
						<React.Fragment>
							<img src={IconTaskError} alt='' className='icon-size-30' />
							<span>
								Skull Candy Headphones
								<br />
								<h6>Product has not been delivered</h6>
							</span>
						</React.Fragment>
					}
					extra={<span className='tag red'>Delivery failed</span>}
					className='empty-card'
					data-ripple=''
					hoverable
				/>
			</div>
		);
	}
}

export default TodoProduct;
