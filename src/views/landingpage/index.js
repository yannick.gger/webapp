import { Breadcrumb, Button, Card, Col, Divider, Icon, message, Row, Steps, Tag, Tooltip } from 'antd';
import CountDown from 'components/countdown';
import moment from 'moment';
import React, { Component } from 'react';
import { I18n, Trans, translate } from 'react-i18next';
import { FormattedNumber, injectIntl } from 'react-intl';
import { Link, Redirect, withRouter } from 'react-router-dom';
import { getHomePath, isInfluencer, isAdmin } from 'services/auth-service';
import { apiToMoment } from 'utils';
import { ReelIconFilled, StoryIconFilled, TiktokIconFilled } from 'components/icons';
import EndingSvg from './page-ending.svg';
import Pigleeto from './pigleeto.svg';
import './styles.scss';
import Tabs from './tabs.js';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import Masonry from 'react-masonry-css';

const Step = Steps.Step;

class LandingPage extends Component {
	constructor(props) {
		super(props);
	}

	renderAssignments = (t, assignments, assignmentType) => {
		return (
			<div className='block assignment payment'>
				<h1 className='mb-30'>{t('landingPage:toPost', { defaultValue: `Att posta på {{assignmentType}}`, assignmentType: assignmentType })}</h1>

				{assignments.map((assignment, index) => (
					<div
						className={`product ${{ video: 'type-video', photo: 'type-photo', story: 'type-story', reel: 'type-reel' }[assignment.instagramType]}`}
						key={index}
					>
						<div className='product-image'>
							<span className='icon circle blue fl' />
						</div>
						<div className='content'>
							<h2>{assignment.name}</h2>
							<div className='price'>
								<div>{assignment.description}</div>
								<br />
								<h5>{t('landingPage:timeToPost', { defaultValue: `Datum att posta` })}</h5>
								<h1 className='brand-color'>
									{apiToMoment(assignment.startTime).format('YYYY-MM-DD') === apiToMoment(assignment.endTime).format('YYYY-MM-DD')
										? apiToMoment(assignment.startTime).format('YYYY-MM-DD')
										: `${apiToMoment(assignment.startTime).format('YYYY-MM-DD')} - ${apiToMoment(assignment.endTime).format('YYYY-MM-DD')}`}
								</h1>
							</div>
						</div>
					</div>
				))}
			</div>
		);
	};

	render() {
		const { campaign, invite, campaignInstagramOwner, t, location } = this.props;
		const instagramAssignments = campaignInstagramOwner.campaign.assignments ? campaignInstagramOwner.campaign.assignments : null;
		const compensations = [...campaignInstagramOwner.campaign.products, ...campaignInstagramOwner.campaign.invoices];

		return (
			<div className='page-influencerpage'>
				{campaignInstagramOwner && (
					<React.Fragment>
						<Breadcrumb className='mb-20'>
							<Breadcrumb.Item>
								<a href='/influencer/campaigns'>{t('landingPage:campaigns', { defaultValue: `Campaigns` })}</a>
							</Breadcrumb.Item>
							<Breadcrumb.Item>{campaign.name}</Breadcrumb.Item>
						</Breadcrumb>
						<Divider />
					</React.Fragment>
				)}

				<div className='block cover' style={{ backgroundImage: `url("${campaign.images ? campaign.images.horizontalCoverPhoto : '/app/default-cover.jpg'}")` }}>
					<div className='headline text-center'>
						<h1>{campaign.name}</h1>
						<p>{t('landingPage:headline', { defaultValue: `Join the campaign and complete the assignments to earn your payment. Limited spots.` })}</p>
					</div>

					<div className='info-bar'>
						<div className='payment-block'>
							<div style={{ flex: '0 0 auto' }}>
								<img src={Pigleeto} alt='' />
							</div>
							<div className='pt-10 pb-10' style={{ flex: '1' }}>
								<h2>
									{campaignInstagramOwner.campaign.compensationsTotalValue && (
										<FormattedNumber
											value={campaignInstagramOwner.campaign.compensationsTotalValue}
											style='currency'
											currency={campaignInstagramOwner.campaign.currency}
											minimumFractionDigits={0}
										/>
									)}
								</h2>
							</div>
						</div>

						<Row className='meta-data'>
							<Col className='col'>
								<h5>{t('landingPage:campaignTodo', { defaultValue: `Post on Instagram` })}</h5>
								<h2 className='brand-color'>
									{t('landingPage:assignmentCount', { defaultValue: `{{count}} Assignments`, count: campaignInstagramOwner.campaign.assignmentsCount })}
								</h2>
							</Col>

							<Col className='col'>
								<h5>{t('landingPage:spotsLeftHeader', { defaultValue: `Platser kvar` })}</h5>
								<h2 className='brand-color'>{campaignInstagramOwner.campaign.spotsLeft}</h2>
							</Col>

							<Col className='col'>
								<h5>{t('landingPage:invitesCloseHeader', { defaultValue: `Inbjudan stänger om` })}</h5>
								<CountDown
									expiryDate={apiToMoment(campaignInstagramOwner.campaign.invitesCloseAt)}
									campaignFilled={campaignInstagramOwner.campaign.spotsLeft === 0}
									variant='h2'
								/>
							</Col>
						</Row>
					</div>
				</div>

				<div className='block intro-text'>
					<h1>{t('landingPage:welcomeHeader', { defaultValue: 'Välkommen till {{campaignName}}', campaignName: campaign.name })}</h1>
					<p>
						{t('landingPage:WelcomeCaption', {
							defaultValue: `Kul att du hittade hit. Du har blivit inbjuden till vår kampanj för att vi vill jobba med dig. Vi hittade din profil på instagram och gillar den starkt, den går rätt
            ihop med vårt brand. Just nu finns {{campaignSpotsLeft}} lediga platser, gå med innan alla platser är slut! Vi ser fram emot att jobba med dig!`,
							campaignSpotsLeft: campaign.spotsLeft,
							count: campaign.spotsLeft
						})}
					</p>
				</div>

				<div className='block brief'>
					<div className='stripes'>
						<span />
						<span />
						<span />
						<span />
						<span />
					</div>

					<div className='content'>
						<h1>{t('landingPage:brief', { defaultValue: `Brief` })}</h1>
						<p className='multi-line'>{campaign.background}</p>
						{campaign.guidelines.map((guideline, index) => {
							return (
								<p key={`guide-${index}`} className='multi-line'>
									{guidline}
								</p>
							);
						})}
					</div>
				</div>

				<div className='block payment'>
					<h1>{t('landingPage:paymentByInvoice', { defaultValue: 'Betalning med faktura' })}</h1>
					<p className='mb-40'>
						{t('landingPage:commissioniIsPaidByInvoice', { defaultValue: 'För denna kampanj är ersättningen betald via faktura enligt följande' })}
					</p>
					{compensations.map((node) => {
						if (node.product) {
							return (
								<div className='product' key={`product-${node.id}`}>
									<div
										className='product-image'
										style={node.product.productImages ? { backgroundImage: `url(${node.product.productImage})` } : { backgroundColor: '#fff' }}
									/>

									<div className='content'>
										<h2>
											{node.product.name}
											{node.product.link && (
												<Tooltip title={t('landingPage:openProductLinkTooltip', { defaultValue: `Gå till produktvyn på hemsidan` })}>
													<a href={node.product.link} className='fr' target='_blank' rel='noopener noreferrer'>
														<Icon type='export' />
													</a>
												</Tooltip>
											)}
										</h2>
										<p className='multi-line'>{node.product.description}</p>
										<Row gutter={30}>
											<Col xs={{ span: 24 }} md={{ span: 8 }}>
												<div className='price'>
													<h5>{t('landingPage:priceValueHeader', { defaultValue: `Pris i butik` })}</h5>
													<h2 className='brand-color'>
														{t('landingPage:priceValue', {
															defaultValue: `{{amount}} {{currency}}`,
															amount: node.product.value,
															currency: campaignInstagramOwner.campaign.currency
														})}
													</h2>
												</div>
											</Col>
											<Col xs={{ span: 24 }} md={{ span: 8 }}>
												<div className='price'>
													<h5>{t('landingPage:quantityHeader', { defaultValue: `Antal du får` })}</h5>
													<h2 className='brand-color'>{t('landingPage:quantity', { defaultValue: `{{quantity}}st`, quantity: node.quantity })}</h2>
												</div>
											</Col>
											<Col xs={{ span: 24 }} md={{ span: 8 }}>
												<div className='price'>
													<h5>{t('landingPage:priceQuantityHeader', { defaultValue: `Total value {{quantity}}st`, quantity: node.quantity })}</h5>
													<h2 className='brand-color'>
														{t('landingPage:priceQuantity', {
															defaultValue: `{{amount}} {{currency}}`,
															amount: node.quantity * node.product.value,
															currency: campaignInstagramOwner.campaign.currency
														})}
													</h2>
												</div>
											</Col>
										</Row>
									</div>
								</div>
							);
						}
						if (node.invoice) {
							return (
								<div className='payment-container' key={`invoice-${node.id}`}>
									<div className='payment-cash'>
										<span />
										<span />
										<div className='cash-amount'>
											<h2>
												<FormattedNumber value={node.invoice.value} /> {campaignInstagramOwner.campaign.currency}
											</h2>
											<span>{t('landingPage:exclusiveOfVAT', { defaultValue: 'Exclusive moms (25%)' })}</span>
										</div>
									</div>
									<div className='payment-content'>
										<h4 className='color-blue'>{t('landingPage:requireForPaymentWithInvoice', { defaultValue: 'VAD SOM KRÄVS FÖR BETALNING MED FAKTURA' })}</h4>
										<ul className='lined'>
											<li>
												<Trans i18nKey='landingPage:mustBeAbleToSendAnInvoice'>
													You must be able to send an invoice to get paid. If you do not have your own company, you can use our partner
													<a href='https://www.gigger.se/collabs' target='_blank' rel='noopener noreferrer'>
														Gigger
													</a>
													.
												</Trans>
											</li>
											<li>
												{t('landingPage:invoiceDetailWillBeSent', {
													defaultValue: 'Fakturauppgifter kommer att skickas till dig per mail när kampanjen är slutförd.'
												})}
											</li>
										</ul>
										<div className='info-block'>
											<p>
												{t('landingPage:paymentMadeViaInvoice30Days', {
													defaultValue: 'Betalning sker via faktura 30 dagar efter att du avslutat alla uppgifter i kampanjen.'
												})}
											</p>
										</div>
									</div>
								</div>
							);
						}
					})}
				</div>

				{instagramAssignments && instagramAssignments.length > 0 ? this.renderAssignments(t, instagramAssignments, 'Instagram') : <div />}

				<div className='block mentions-tags'>
					<h1 className='mb-30 text-center'>{t('landingPage:mentionsTagsHeader', { defaultValue: `Mentions & Tags` })}</h1>
					<Row gutter={20}>
						<Col sm={{ span: 12 }}>
							<h4 className='card-headline'>{t('landingPage:mentionsHeader', { defaultValue: `@mentions att använda` })}</h4>
							<Card className='mb-30 px-30'>
								<div className='mentions'>
									<h2>{t('landingPage:mentionsCardTitle', { defaultValue: `Mentions` })}</h2>
									<p>{t('landingPage:mentionsCardCaption', { defaultValue: `Dessa @mentions måste användas på alla poster:` })}</p>
									<p className='color-dark'>{campaign.mentions.join(' ')}</p>
								</div>
							</Card>
						</Col>

						<Col sm={{ span: 12 }}>
							<h4 className='card-headline'>{t('landingPage:tagsHeader', { defaultValue: `#tags att använda` })}</h4>
							<Card className='mb-30 px-30'>
								<div className='tags'>
									<h2>{t('landingPage:tagsCardHeader', { defaultValue: `Tags` })}</h2>
									<p>{t('landingPage:tagsCardCaption', { defaultValue: `Dessa #taggar måste finnas på alla poster:` })}</p>
									<p className='color-dark'>{campaign.hashtags.join(' ')}</p>
								</div>
							</Card>
						</Col>
					</Row>
				</div>

				<div className='block tabs'>
					<Tabs campaign={campaign} />
				</div>

				<div className='ending'>
					<img src={EndingSvg} alt='' />
				</div>
			</div>
		);
	}
}

const TranslatedLandingPage = (props) => {
	if (props.loading) {
		return (
			<div id='preloader'>
				<div id='status' />
			</div>
		);
	}

	if (props.error) return <p>{props.t('landingPage:loadCampaignError', { defaultValue: `Could not load campaign` })}</p>;

	if (props.campaign === null) {
		message.error(props.t('landingPage:noAccessToCampaign', { defaultValue: 'You do not have access to this campaign' }));
		return <Redirect to={getHomePath()} />;
	}

	document.title = `${props.campaign.name} | Collabs`;

	return (
		<I18n ns={['landingPage']} initialLanguage={props.campaign.language}>
			{(t) => <LandingPage {...props} t={t} />}
		</I18n>
	);
};

export default withRouter(injectIntl(translate('landingPage')(TranslatedLandingPage)));
