import React, { Component } from 'react';
import { Collapse, Card } from 'antd';
import { translate, Trans } from 'react-i18next';
const Panel = Collapse.Panel;

class Faq extends Component {
	render() {
		const { t } = this.props;
		return (
			<React.Fragment>
				<Card className='mb-20 px-30'>
					<Collapse accordion className='in-card-collapse'>
						<Panel header={t('landingPage:faqWhatIsHeader', { defaultValue: 'What is Collabs?' })} key='1'>
							<p>
								{t('landingPage:faqWhatIsContent', {
									defaultValue: `Collabs is a platform that allows companies to discover talented creators all over the world. When they have done so, they are also able to create and manage
                campaigns all in one place. Under ”Brief”, you’ll find everything you need to know about the campaign.`
								})}
							</p>
						</Panel>
						<Panel header={t('landingPage:faqWhyInvitedHeader', { defaultValue: 'Why have I been invited?' })} key='2'>
							<p>
								{t('landingPage:faqWhyInvitedContent', {
									defaultValue: `You have been invited by a client of ours using Collabs to manage their campaigns and find talented creators, such as yourself. They obviously like you and would
                like to collaborate with you.`
								})}
							</p>
						</Panel>
						<Panel header={t('landingPage:faqHowSpotWorksHeader', { defaultValue: 'How do Spots work?' })} key='3'>
							<p>
								{t('landingPage:faqHowSpotWorksContent', {
									defaultValue: `Every campaign has a limited amount of spots available. When all spots are filled, the signup closes. If all spots are not filled within the timeframe (before the
                countdown hits 0) the company can still run the campaign with the influencers that has joined.`
								})}
							</p>
						</Panel>
						<Panel header={t('landingPage:faqWhatsNextHeader', { defaultValue: 'What’s next if I join?' })} key='4'>
							<p>
								{t('landingPage:faqWhatsNextContent', {
									defaultValue: `If you find the campaign to be just right for you, and if you do want to collaborate with the brand - press ”Join campaign” to claim your spot. You’ll enter the
                information necessary to send out products & to receive payment.`
								})}
							</p>
						</Panel>
						<Panel header={t('landingPage:faqShareInfoHeader', { defaultValue: 'Will you ever share my information?' })} key='5'>
							<p>
								{t('landingPage:faqShareInfoContent', {
									defaultValue: `Collabs is GDRP compliant and will never share your information with anyone other than the company that you agree to collaborate with. The company will only use
                your information to send out products and payments.`
								})}
							</p>
						</Panel>
						<Panel header={t('landingPage:faqMoreInfoHeader', { defaultValue: 'Explain further!' })} key='6'>
							<p>
								{t('landingPage:faqMoreInfoContent', {
									defaultValue: `Collabs is completely talent independent. We believe influencer marketing can be done so much easier than it is today. Companies can do campaigns from A-Z, which
                means finding talented creators, sending invitations, communication, send out products, approve content & pay commissions. All in one place.`
								})}
							</p>
							<p>
								<Trans i18nKey='landingPage:faqMoreInfoContentReadMore'>
									Read more{' '}
									<a href='https://collabs.app/contact.html' target='_blank' rel='noopener noreferrer'>
										here
									</a>
									.
								</Trans>
							</p>
						</Panel>
					</Collapse>
				</Card>
			</React.Fragment>
		);
	}
}

export default translate('landingPage')(Faq);
