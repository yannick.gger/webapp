import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import { translate } from 'react-i18next';

import moment from 'moment';

class TabsCard extends Component {
	formatTimeLeft(time) {
		const duration = moment.duration(time, 'milliseconds');
		let defaultMessage;

		if (duration.days() >= 1 || duration.weeks() >= 1 || duration.months() >= 1 || duration.years() >= 1) {
			defaultMessage = `{days} {days, plural, one {day} other {days}} {hours} {hours, plural, one {hour} other {hours}}`;
		} else if (duration.hours() >= 1) {
			defaultMessage = `{hours} {hours, plural, one {hour} other {hours}} {minutes} {minutes, plural, one {min} other {min}}`;
		} else {
			defaultMessage = `{minutes} {minutes, plural, one {min} other {min}} {seconds} {seconds, plural, one {sec} other {sec}}`;
		}

		return this.props.intl.formatMessage(
			{
				id: 'counter-days',
				defaultMessage: defaultMessage
			},
			{ days: duration.days(), hours: duration.hours(), minutes: duration.minutes(), seconds: duration.seconds() }
		);
	}
	render() {
		const { t } = this.props;
		return (
			<React.Fragment>
				<div className='white-box content px-30 py-30'>
					<h3>{t('landingPage:briefHeader', { defaultValue: `Vad som förväntas av dig:` })}</h3>
					<ul className='lined'>
						<li>{t('landingPage:briefProfessional', { defaultValue: `Proffsigt producerat content enligt instruktion` })}</li>
						<li>{t('landingPage:briefTime', { defaultValue: `Att posta i tid och på angivet datum` })}</li>
						<li>{t('landingPage:briefTagsAndMentions', { defaultValue: `Användning av rätt #hashtags och @mentions` })}</li>
						<li>{t('landingPage:briefAllAssignments', { defaultValue: `Att avklara alla uppgifter enligt plan` })}</li>
						<li>{t('landingPage:briefScreenshot', { defaultValue: `Att skicka upp screenshot på statistik från alla poster` })}</li>
					</ul>
				</div>
			</React.Fragment>
		);
	}
}

export default injectIntl(translate('landingPage')(TabsCard));
