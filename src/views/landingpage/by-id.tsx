import { useState, useEffect } from 'react';
import { useLocation, useRouteMatch } from 'react-router-dom';

import LoadingSpinner from 'components/LoadingSpinner';
import CampaignService from 'services/Statistics/Campaign';

import GenericNotFoundView from 'views/generic-not-found';
import BriefPage from 'views/BriefPage';
import './styles.scss';

const LandingPageByPreview = () => {
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);
	const [campaign, setCampaign] = useState(null);
	const [campaignInstagramOwner, setCampaignInstagramOwner] = useState(null);

	const { params } = useRouteMatch();
	const { state } = useLocation();

	useEffect(() => {
		CampaignService.fetchOneCampaign({ id: params.campaignId })
			.then((res) => {
				setCampaign({
					id: res.data.id,
					images: res.data.links,
					...res.data.attributes
				});
				//@ts-ignore
				setCampaignInstagramOwner(state);
				setLoading(false);
			})
			.catch((err) => {
				setError(err);
			});
	}, []);

	return (
		<div>
			{loading ? (
				<LoadingSpinner />
			) : error || !campaign ? (
				<GenericNotFoundView />
			) : (
				<BriefPage campaign={campaign} campaignInstagramOwner={campaignInstagramOwner} />
			)}
		</div>
	);
};

export default LandingPageByPreview;
