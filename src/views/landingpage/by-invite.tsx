import { useState, useEffect, useContext } from 'react';
import { Store } from 'json-api-models';
import { Redirect, useRouteMatch } from 'react-router-dom';
import LoadingSpinner from 'components/LoadingSpinner';
import { ToastContext } from 'contexts';

import { isInfluencer } from 'services/auth-service';
import InfluencerService from 'services/Influencer';
import CampaignService from 'services/Statistics/Campaign';
import BriefPage from 'views/BriefPage';

import './styles.scss';
import GenericNotFoundView from 'views/generic-not-found';

const LandingPageByInvite = () => {
	const models = new Store();
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const [campaign, setCampaign] = useState<any>(null);
	const [campaignInstagramOwner, setCampaignInstagramOwner] = useState<any>(null);
	const toast = useContext(ToastContext);

	const { params } = useRouteMatch();

	const getProperDataFromResponse = (res: any) => {
		models.sync(res);
		const data = models.findAll('invite')[0];

		let newCampaign = {
			id: data.campaignInstagramOwner.campaign.id,
			...data.campaignInstagramOwner.campaign.attributes
		};

		let newCampaignInstagramOwner = null;

		newCampaignInstagramOwner = {
			campaign: {
				campaignImages: {
					coverImage: data.campaignInstagramOwner.campaign.links ? data.campaignInstagramOwner.campaign.links.horizontalCoverPhoto : ''
				},
				products: data.campaignInstagramOwner.campaignInstagramOwnerProducts
					? data.campaignInstagramOwner.campaignInstagramOwnerProducts.map((product) => {
							return {
								id: product.id,
								quantity: 1,
								product: {
									productImage: product.product.links.image,
									name: product.product.name,
									link: product.product.link ? product.product.link : null,
									description: product.product.description,
									value: +product.product.value.amount / 100
								}
							};
					  })
					: [],
				invoices: data.campaignInstagramOwner.campaignInstagramOwnerCommissions
					? data.campaignInstagramOwner.campaignInstagramOwnerCommissions.map((commission) => {
							return {
								id: commission.id,
								invoice: {
									value: commission.commission.fixedAmount / 100
								}
							};
					  })
					: [],
				compensationsTotalValue: [
					...(data.campaignInstagramOwner.campaignInstagramOwnerProducts
						? data.campaignInstagramOwner.campaignInstagramOwnerProducts.map((product) => +product.product.value.amount / 100)
						: []),
					...(data.campaignInstagramOwner.campaignInstagramOwnerCommissions
						? data.campaignInstagramOwner.campaignInstagramOwnerCommissions.map((commission) => commission.commission.fixedAmount / 100)
						: [])
				].reduce((prev, current) => prev + current, 0),
				currency: data.campaignInstagramOwner.campaignInstagramOwnerCommissions[0].commission.fixedAmountCurrency,
				assignmentsCount: data.campaignInstagramOwner.campaignInstagramOwnerAssignments.length,
				spotsLeft: data.campaignInstagramOwner.campaign.full ? 0 : 1,
				invitesCloseAt: data.campaignInstagramOwner.campaign.inviteClosedAt,
				assignments: data.campaignInstagramOwner.campaignInstagramOwnerAssignments
					? data.campaignInstagramOwner.campaignInstagramOwnerAssignments.map((campaignInstagramOwnerAssignment) => {
							const assignment = campaignInstagramOwnerAssignment.assignment;

							return {
								id: assignment.id,
								type: assignment.attributes.type,
								name: assignment.name,
								dos: assignment.dos,
								donts: assignment.donts,
								cta: assignment.cta,
								description: assignment.description,
								startTime: campaignInstagramOwnerAssignment.assignmentGroup.start,
								endTime: campaignInstagramOwnerAssignment.assignmentGroup.end,
								deadlines: campaignInstagramOwnerAssignment.assignmentGroup.deadlines.map((deadline) => {
									return {
										id: deadline.id,
										name: deadline.name,
										date: deadline.date
									};
								})
							};
					  })
					: []
			},
			influencer: {
				id: data.influencer.id,
				username: data.influencer.username
			}
		};

		return { campaign: newCampaign, campaignInstagramOwner: newCampaignInstagramOwner };
	};

	useEffect(() => {
		if (params.influencerId) {
			CampaignService.fetchInfluencerPreviewData(params.campaignId, params.influencerId)
				.then((res) => {
					const newResult = getProperDataFromResponse(res);
					setCampaign(newResult.campaign);
					setCampaignInstagramOwner(newResult.campaignInstagramOwner);
					setLoading(false);
				})
				.catch((err) => {
					console.log(err);
				})
				.finally(() => {
					setLoading(false);
				});
		} else {
			InfluencerService.getInvitedCampaign(params.inviteToken)
				.then((res) => {
					if (res) {
						const newResult = getProperDataFromResponse(res);
						setCampaign(newResult.campaign);
						setCampaignInstagramOwner(newResult.campaignInstagramOwner);
						setLoading(false);
					}
				})
				.catch((err) => {
					if (err.response.status === 410) {
						toast.addToast({ id: 'token-error', message: err.response.data.errors[0].title, mode: 'error' });
					}
					setError(true);
				})
				.finally(() => {
					setLoading(false);
				});
		}
	}, []);

	if (isInfluencer()) {
		return <Redirect to={`/influencer/campaigns`} />;
	}

	return (
		<div>
			{loading ? (
				<LoadingSpinner />
			) : error || !campaign || !campaignInstagramOwner ? (
				<GenericNotFoundView />
			) : (
				<BriefPage invite={params.inviteToken} campaign={campaign} campaignInstagramOwner={campaignInstagramOwner} />
			)}
		</div>
	);
};

export default LandingPageByInvite;
