import { useState, useEffect } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';

import { Modal } from 'antd';
import ConnectWithSocialMedia from './login/components/connect-with-social-media';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { FEATURE_FLAG_FACEBOOK_AUTH } from 'constants/feature-flag-keys';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import InfluencerSignUpForm from './login/components/InfluencerSignUpForm';
import { KEY_FACEBOOK_ACCESS_TOKEN } from 'constants/localStorage-keys';

const InfluencerSignUpPage = () => {
	const [isEnabled] = useFeatureToggle();
	const [signUpVisibillity, setSignUpVisibillity] = useState(!isEnabled(FEATURE_FLAG_FACEBOOK_AUTH));
	const [socialMediaVisibillity, setSocialMediaVisibillity] = useState(true);
	const storage = new BrowserStorage(StorageType.SESSION);
	const history = useHistory();
	const { params } = useRouteMatch();

	useEffect(() => {
		const fb_token = storage.getItem(KEY_FACEBOOK_ACCESS_TOKEN);
		setSocialMediaVisibillity(isEnabled(FEATURE_FLAG_FACEBOOK_AUTH) !== undefined);

		if (fb_token !== null) {
			setSocialMediaVisibillity(false);
			setSignUpVisibillity(true);
		}
	}, []);

	const onClickCancel = () => {
		history.push(`/invites/campaign/${params.inviteToken}`);
	};

	const onClickSkip = () => {
		setSignUpVisibillity(true);
		setSocialMediaVisibillity(false);
	};

	return (
		<div>
			{isEnabled(FEATURE_FLAG_FACEBOOK_AUTH) ? (
				<ConnectWithSocialMedia
					visible={socialMediaVisibillity}
					onClickCancel={onClickCancel}
					onClickSkip={onClickSkip}
					successCallback={() => setSignUpVisibillity(true)}
				/>
			) : null}
			<Modal
				className='instagran-sign-up-container'
				title='Sign up'
				visible={signUpVisibillity}
				wrapClassName='custom-modal register-influencer title-center'
				maskClosable={false}
				onCancel={onClickCancel}
				footer={[]}
			>
				<section className='block login-register-form'>
					<InfluencerSignUpForm handleCancel={onClickCancel} />
				</section>
			</Modal>
		</div>
	);
};

export default InfluencerSignUpPage;
