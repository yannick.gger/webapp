import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next/hooks';
import { Button } from 'antd';

const JoinCampaignButton = (props: any) => {
	const history = useHistory();
	const [t] = useTranslation();

	return (
		<Button
			type='primary'
			className='btn-primary btn-rounded mb-5'
			onClick={() => {
				history.push(`/invites/influencer/sign-up/${props.inviteToken}`);
			}}
		>
			{t('landingPage:joinCampaignButtonJoin', { defaultValue: `Gå med i kampanjen` })}
		</Button>
	);
};

export default JoinCampaignButton;
