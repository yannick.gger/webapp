import { Modal } from 'antd';
import i18next from 'i18next';
import React, { MouseEventHandler } from 'react';
import InstagramAuth from 'components/instagram-auth/';

interface IConnectWithSocialMedia {
	onClickSkip: MouseEventHandler;
	onClickCancel: MouseEventHandler;
	successCallback?: () => void;
	visible: boolean;
}

const ConnectWithSocialMedia = (props: IConnectWithSocialMedia) => {
	const { onClickSkip, onClickCancel, visible, successCallback } = props;

	return (
		<Modal
			className='social-media-connect-modal'
			title={i18next.t('connectToSocialMedia', { defaultValue: 'Connect social media' })}
			visible={visible}
			wrapClassName='custom-modal register-influencer title-center'
			maskClosable={false}
			onCancel={onClickCancel}
			footer={[]}
		>
			<section className='block social-media-connect-form'>
				<p className='text-center'>{i18next.t('instagram.connectToInstagramToJoinUs', { defaultValue: 'Connect your Instagram profile to join us' })}</p>
				<p className='text-center'>By connecting to social media it will make it easier for you when you are working with the campaigns.</p>
				<div className='block social-media-connect-form__btn-group'>
					<InstagramAuth
						type='button'
						isConnected={false}
						successCallback={() => {
							successCallback && successCallback();
						}}
					/>
				</div>
				<a href='#' onClick={onClickSkip}>
					{i18next.t('instagram.skip', { defaultValue: 'Skip' })}
				</a>
			</section>
		</Modal>
	);
};

export default ConnectWithSocialMedia;
