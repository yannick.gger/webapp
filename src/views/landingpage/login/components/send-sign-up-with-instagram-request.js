import React from 'react';
import { Mutation } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import signUpWithInstagram from 'graphql/sign-up-with-instagram.graphql';
import SendInstagramRequestModal from './send-sign-up-with-instagram-request-modal.js';

class SendSignUpWithInstagramRequest extends React.Component {
	handleCancel = () => {
		this.props.closeModal();
	};
	render() {
		const { host } = window.location;
		const protocol = 'https';
		const redirectUri = `${protocol}://${host}/instagram/authorization/signup/`;
		const code = this.props.location && this.props.location.search.slice(6);
		const inviteToken = localStorage.getItem('inviteToken');
		return (
			<Mutation mutation={signUpWithInstagram} variables={{ inviteToken: inviteToken, redirectUri: redirectUri, code: code }}>
				{(signUpWithInstagram) => {
					return <SendInstagramRequestModal signUpWithInstagram={signUpWithInstagram} inviteToken={inviteToken} handleCancel={this.handleCancel} />;
				}}
			</Mutation>
		);
	}
}

export default withRouter(SendSignUpWithInstagramRequest);
