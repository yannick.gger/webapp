import React from 'react';
import { Modal, Spin, message } from 'antd';
import { withRouter } from 'react-router-dom';
import { setToken, redirectToHome } from 'services/auth-service';

class SendInstagramRequestModal extends React.Component {
	componentDidMount() {
		const { inviteToken, bugsnagClient, history, location } = this.props;
		const error = location && location.search.includes('error');
		const redirectLink = `/invites/campaign/${inviteToken}/login`;
		if (error) {
			history.push('/login');
		} else {
			this.props
				.signUpWithInstagram()
				.then((result) => {
					if (result.data && result.data.signUpWithInstagram && result.data.signUpWithInstagram.token) {
						setToken(result.data.signUpWithInstagram.token, { bugsnagClient: bugsnagClient });
						message.success('Logged in with Instagram successfully', 5);
						redirectToHome({ history: history });
					} else if (result.data.signUpWithInstagram === null) {
						message.error('Something went wrong');
						setTimeout(() => {
							history.push(redirectLink);
						}, 3000);
					}
				})
				.catch(({ graphQLErrors }) => {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
						setTimeout(() => {
							history.push(redirectLink);
						}, 3000);
					});
				});
		}
	}

	render() {
		return (
			<Modal
				visible
				onOk={this.handleOk}
				onCancel={this.props.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
			>
				<div style={{ textAlign: 'center' }}>
					<Spin className='collabspin' />
					<p className='mt-40'>We are authorizing your Instagram account</p>
				</div>
			</Modal>
		);
	}
}

export default withRouter(SendInstagramRequestModal);
