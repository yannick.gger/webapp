import React, { useState, useContext } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { Form, Select, Icon, Input, Button, message, Spin, Tooltip } from 'antd';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { setToken, getTokenPayload } from 'services/auth-service';
import { withBugsnag } from 'hocs';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import FacebookAuthService from 'services/FacebookApi/Facebook-auth.service';
import CollabsAuthService from 'services/Authentication/Collabs-api';
import { ToastContext } from 'contexts';

import '../styles.scss';
import { StatusCode } from 'services/Response.types';

const FormItem = Form.Item;

// @todo Fix typings
const InfluencerSignUpForm = (props: any) => {
	const [loading, setLoading] = useState(false);
	const { form } = props;
	const { getFieldDecorator } = form;
	const { addToast } = useContext(ToastContext);
	const { params } = useRouteMatch();

	const SIGN_UP_WITH_CAMPAIGN_TOKEN = gql`
		mutation signUpWithCampaignToken($userName: String!, $password: String!, $categories: [String]!, $inviteToken: String!) {
			signUpWithCampaignToken(input: { userName: $userName, password: $password, categories: $categories, inviteToken: $inviteToken }) {
				token
				user {
					id
				}
			}
		}
	`;

	const validatePasswordReq = (_, value, callback) => {
		if (value && value.length < 8) callback('Your password must be at least 8 characters long.');
		callback();
	};

	const getUniqueArray = (_array: Array) => {
		return [...new Map(_array.map((item) => [item['index'], item])).values()];
	};

	const compareToFirstPassword = (_, value, callback) => {
		const form = props.form;
		if (value && value !== form.getFieldValue('password')) callback('Two passwords that you enter is inconsistent!');
		else callback();
	};

	const validateToNextPassword = (_, value, callback) => {
		const form = props.form;
		if (value && form.getFieldValue('confirm')) form.validateFields(['confirm'], { force: true });
		callback();
	};

	const storeAccessToken = (accessToken: string) => {
		// Store collabs token into the database
		FacebookAuthService.storeFacebookAccessToken(accessToken).then((result) => {
			if (result === StatusCode.OK) {
				props.history.push(`/influencer/campaigns`);
				setLoading(false);
			}
		});
	};

	const handleSubmit = (mutation) => {
		return (e) => {
			e.preventDefault();
			props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				const { email, password } = values;
				setLoading(true);
				mutation({ variables: { ...values, categories: ['.'], inviteToken: params.inviteToken } })
					.then((res) => {
						props.history.push(`/influencer/campaigns`);
					})
					.catch((err) => {
						props.history.push(`/login`);
					});
			});
		};
	};

	return (
		<Mutation mutation={SIGN_UP_WITH_CAMPAIGN_TOKEN}>
			{(mutation) => (
				<Form onSubmit={handleSubmit(mutation)} className='register-form ant-form-lg'>
					<div className='ant-form-group'>
						<FormItem label='Your full name'>
							{getFieldDecorator('userName', {
								placeholder: 'John Doe',
								rules: [
									{
										required: true,
										message: 'Please enter your name'
									}
								]
							})(<Input className='rippledown' prefix={<Icon type='user-add' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='John Doe' />)}
						</FormItem>

						<FormItem label='Password'>
							{getFieldDecorator('password', {
								rules: [
									{
										required: true,
										message: 'Please input your password!'
									},
									{
										validator: validatePasswordReq
									},
									{
										validator: validateToNextPassword
									}
								]
							})(<Input.Password className='rippledown' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Password' />)}
						</FormItem>
						<FormItem label='Confirm Password'>
							{getFieldDecorator('confirm', {
								rules: [
									{
										required: true,
										message: 'Please confirm your password!'
									},
									{
										validator: compareToFirstPassword
									}
								]
							})(<Input.Password className='rippledown' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} />)}
						</FormItem>
					</div>

					<div className='ant-form-group'>
						<FormItem>
							<Button type='primary' htmlType='submit' className='signup-form-button ant-btn-lg' loading={loading}>
								Register
							</Button>
						</FormItem>
					</div>
				</Form>
			)}
		</Mutation>
	);
};

InfluencerSignUpForm.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

export default withBugsnag(withRouter(Form.create()(InfluencerSignUpForm)));
