import React, { useState, useContext } from 'react';
import { Form, Select, Icon, Input, Button, message, Spin, Tooltip } from 'antd';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { setToken, getTokenPayload } from 'services/auth-service';
import { withBugsnag } from 'hocs';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import FacebookAuthService from 'services/FacebookApi/Facebook-auth.service';
import CollabsAuthService from 'services/Authentication/Collabs-api';
import { ToastContext } from 'contexts';

import '../styles.scss';
import { StatusCode } from 'services/Response.types';

const FormItem = Form.Item;

const GET_CATEGORIES = gql`
	{
		categories {
			index
			name
			color
		}
	}
`;

// @todo Fix typings
const SignUpForm = (props: any) => {
	const [loading, setLoading] = useState(false);
	const { form, invite } = props;
	const { getFieldDecorator } = form;
	const { addToast } = useContext(ToastContext);

	const SIGN_UP_WITH_CAMPAIGN_TOKEN = gql`
		mutation signUpWithCampaignToken($userName: String!, $password: String!, $categories: [String]!, $inviteToken: String!) {
			signUpWithCampaignToken(input: { userName: $userName, password: $password, categories: $categories, inviteToken: $inviteToken }) {
				token
				user {
					id
				}
			}
		}
	`;

	const validatePasswordReq = (_, value, callback) => {
		if (value && value.length < 8) callback('Your password must be at least 8 characters long.');
		callback();
	};

	const getUniqueArray = (_array: Array) => {
		return [...new Map(_array.map((item) => [item['index'], item])).values()];
	};

	const compareToFirstPassword = (_, value, callback) => {
		const form = props.form;
		if (value && value !== form.getFieldValue('password')) callback('Two passwords that you enter is inconsistent!');
		else callback();
	};

	const validateToNextPassword = (_, value, callback) => {
		const form = props.form;
		if (value && form.getFieldValue('confirm')) form.validateFields(['confirm'], { force: true });
		callback();
	};

	const storeAccessToken = (accessToken: string) => {
		// Store collabs token into the database
		FacebookAuthService.storeFacebookAccessToken(accessToken).then((result) => {
			if (result === StatusCode.OK) {
				props.history.push(`/influencer/campaigns/${props.invite.campaign.id}/join`);
				setLoading(false);
			}
		});
	};

	const handleSubmit = (mutation) => {
		return (e) => {
			e.preventDefault();
			props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				const { email, password } = values;
				setLoading(true);
				const signUpWithCampaignTokenResult = await mutation({ variables: { ...values, inviteToken: props.inviteToken } });
				if (signUpWithCampaignTokenResult.data.signUpWithCampaignToken) {
					// Get Ruby API Token
					setToken(signUpWithCampaignTokenResult.data.signUpWithCampaignToken.token, { bugsnagClient: props.bugsnagClient });

					// Get Collabs API Token
					CollabsAuthService.requestCollabsToken({ username: email, password, password }).then((result) => {
						if (result === StatusCode.OK) {
							CollabsAuthService.me().then((result) => {
								if (result === StatusCode.OK) {
									const facebookAccessToken = sessionStorage && sessionStorage.getItem('fb_token');
									if (facebookAccessToken !== null) {
										storeAccessToken(facebookAccessToken);
									} else {
										props.history.push(`/influencer/campaigns/${props.invite.campaign.id}/join`);
										setLoading(false);
										message.success('Welcome to Collabs!');
										localStorage && localStorage.setItem('signedUp', true);
									}
								}
							});
						}
					});
				} else {
					props.form.setFields({
						email: {
							errors: [new Error('Could not create user')]
						}
					});
				}
			});
		};
	};

	return (
		<Mutation mutation={SIGN_UP_WITH_CAMPAIGN_TOKEN}>
			{(mutation) => (
				<Form onSubmit={handleSubmit(mutation)} className='register-form ant-form-lg'>
					<div className='ant-form-group'>
						<FormItem
							label={
								<span>
									Account name &nbsp;
									<Tooltip title='If you recently changed your Instagram username, our system may be delayed. Not to worry, our system will update your username asap. You can continue to sign up with the old username in the meantime'>
										<Icon type='question-circle-o' />
									</Tooltip>
								</span>
							}
						>
							{getFieldDecorator('accountName', {
								rules: [
									{
										required: true,
										message: 'Please input your account name!'
									}
								]
							})(<Input prefix={<Icon type='instagram' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='accountname' disabled />)}
						</FormItem>

						<FormItem label='Your full name'>
							{getFieldDecorator('userName', {
								placeholder: 'John Doe',
								rules: [
									{
										required: true,
										message: 'Please enter your name'
									}
								]
							})(<Input className='rippledown' prefix={<Icon type='user-add' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='John Doe' />)}
						</FormItem>

						<Query query={GET_CATEGORIES}>
							{({ loading, data }) => (
								<FormItem label='Category'>
									{getFieldDecorator('categories', {
										rules: [
											{
												required: true,
												message: 'Please select at least one category!',
												type: 'array'
											}
										]
									})(
										<Select
											id='categoryFilter'
											className='custom-select rippledown'
											mode='multiple'
											style={{ width: '100%' }}
											placeholder='Please select all categories that fits your profile'
											notFoundContent={loading ? <Spin size='small' /> : 'Could not load categories'}
										>
											{data &&
												data.categories &&
												getUniqueArray(data.categories).map((category) => (
													<Select.Option key={category.index} value={category.index}>
														{category.name}
													</Select.Option>
												))}
										</Select>
									)}
								</FormItem>
							)}
						</Query>

						<FormItem label='E-mail'>
							{getFieldDecorator('email', {
								initialValue: invite.email,
								rules: [
									{
										type: 'email',
										message: 'The input is not valid E-mail!'
									},
									{
										required: true,
										message: 'Please input your E-mail!'
									}
								]
							})(<Input prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='E-mail' disabled />)}
						</FormItem>
						<FormItem label='Password'>
							{getFieldDecorator('password', {
								rules: [
									{
										required: true,
										message: 'Please input your password!'
									},
									{
										validator: validatePasswordReq
									},
									{
										validator: validateToNextPassword
									}
								]
							})(<Input.Password className='rippledown' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Password' />)}
						</FormItem>
						<FormItem label='Confirm Password'>
							{getFieldDecorator('confirm', {
								rules: [
									{
										required: true,
										message: 'Please confirm your password!'
									},
									{
										validator: compareToFirstPassword
									}
								]
							})(<Input.Password className='rippledown' prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} />)}
						</FormItem>
					</div>

					<div className='ant-form-group'>
						<FormItem>
							<Button type='primary' htmlType='submit' className='signup-form-button ant-btn-lg' loading={loading}>
								Register
							</Button>
						</FormItem>
					</div>
				</Form>
			)}
		</Mutation>
	);
};

SignUpForm.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

export default withBugsnag(withRouter(Form.create()(SignUpForm)));
