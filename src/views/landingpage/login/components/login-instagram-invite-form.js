import React from 'react';
import { Form, Button, Avatar, Spin } from 'antd';
import { withRouter } from 'react-router-dom';
import { Query } from 'react-apollo';
import authorizedInstagram from 'graphql/authorized-instagram.graphql';
import { withBugsnag } from 'hocs';
import '../styles.css';

class LoginFormInstagramInvite extends React.Component {
	render() {
		const { instagramOwner, location } = this.props;
		const inviteToken = location && location.pathname.split('/')[3];
		const { host } = window.location;
		const protocol = 'https';
		const redirectUri = `${protocol}://${host}/instagram/authorization/signup/`;
		localStorage.setItem('inviteToken', inviteToken);
		return (
			<section className='block new-user-login-with-instagram-form'>
				<div className='user-avatar-wrapper'>
					<Avatar size='large' src={instagramOwner.avatar} icon='user' />
				</div>
				<div className='new-user-login-instagram-main-content'>
					<h3>{instagramOwner.username}</h3>
					<Query query={authorizedInstagram}>
						{({ data, loading: queryLoading }) => {
							if (queryLoading) {
								return <Spin className='collabspin' />;
							}
							const fullUrl = `${data.instagramOauthUrl}&redirect_uri=${redirectUri}`;
							return (
								<Button type='primary'>
									<a href={fullUrl}>Login with instagram</a>
								</Button>
							);
						}}
					</Query>
					<div className='block confirm-message'>
						By logging in with Instagram you simultaneously verify your account and increase your chance for invitations to better campaigns.
					</div>
				</div>
			</section>
		);
	}
}

export default withBugsnag(withRouter(Form.create()(LoginFormInstagramInvite)));
