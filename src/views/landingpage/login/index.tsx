import React, { useState, useEffect } from 'react';
import { Modal, Spin } from 'antd';
import PropTypes from 'prop-types';
import { Query, QueryResult } from 'react-apollo';
import gql from 'graphql-tag';
import SignUpForm from './components/sign-up-form';
import ConnectWithSocialMedia from './components/connect-with-social-media';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { FEATURE_FLAG_FACEBOOK_AUTH } from 'constants/feature-flag-keys';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import './styles.scss';
import { KEY_FACEBOOK_ACCESS_TOKEN } from 'constants/localStorage-keys';

const GET_CAMPAIGN_INVITE = gql`
	query getCampaignInvite($inviteToken: String!, $cursor: String) {
		campaignInvite(inviteToken: $inviteToken) {
			id
			email
			used
			instagramOwnerId
			campaign {
				id
				name
				mentions
				tags
				campaignCoverPhoto {
					id
					url
				}
				campaignLogo {
					id
					url
				}
				campaignBrandAssets {
					id
					url
					thumbnailSquare
				}
				campaignInspirations {
					id
					url
					thumbnailSquare
				}
				assignments(first: 20, after: $cursor) @connection(key: "assignments", filter: []) {
					pageInfo {
						endCursor
						hasNextPage
					}

					edges {
						node {
							id
							name
							description
							instagramType
							paymentSum
							startTime
							endTime
							paymentType
							frequency
						}
					}
				}
				assignmentsCount
			}
		}
	}
`;

interface ILoginView {
	closeModal?: any;
	loading?: boolean;
	data?: any;
	error?: any;
	inviteToken?: string;
}

const LoginView = (props: ILoginView) => {
	const [isEnabled] = useFeatureToggle();
	const [signUpVisibillity, setSignUpVisibillity] = useState(!isEnabled(FEATURE_FLAG_FACEBOOK_AUTH));
	const [socialMediaVisibillity, setSocialMediaVisibillity] = useState(true);
	const storage = new BrowserStorage(StorageType.SESSION);

	useEffect(() => {
		const fb_token = storage.getItem(KEY_FACEBOOK_ACCESS_TOKEN);
		setSocialMediaVisibillity(isEnabled(FEATURE_FLAG_FACEBOOK_AUTH) !== undefined);

		if (fb_token !== null) {
			setSocialMediaVisibillity(false);
			setSignUpVisibillity(true);
		}
	}, []);

	const onClickCancel = () => {
		props.closeModal();
	};

	const onClickSkip = () => {
		setSignUpVisibillity(true);
		setSocialMediaVisibillity(false);
	};

	const renderLoadingSpinner = () => {
		return (
			<div className='text-center mt-30'>
				<Spin className='collabspin' />
			</div>
		);
	};

	const renderErrorMessage = () => {
		return (
			<div className='text-center mt-30'>
				<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
					Something went wrong
				</h1>
			</div>
		);
	};

	const renderNoInvite = () => {
		return (
			<div className='text-center mt-30'>
				<h1 className='color-blue' style={{ fontWeight: '700', marginBottom: '0px' }}>
					No invite was found
				</h1>
				<p>Either you have received an incorrect link or the invitation has already been used.</p>
			</div>
		);
	};

	const renderModal = () => {
		return (
			<>
				{isEnabled(FEATURE_FLAG_FACEBOOK_AUTH) ? (
					<ConnectWithSocialMedia
						visible={socialMediaVisibillity}
						onClickCancel={onClickCancel}
						onClickSkip={onClickSkip}
						successCallback={() => setSignUpVisibillity(true)}
					/>
				) : null}
				<Modal
					className='instagran-sign-up-container'
					title='Sign up'
					visible={signUpVisibillity}
					wrapClassName='custom-modal register-influencer title-center'
					maskClosable={false}
					onCancel={onClickCancel}
					footer={[]}
				>
					<section className='block login-register-form'>
						<SignUpForm invite={props.data.campaignInvite} inviteToken={props.inviteToken} handleCancel={onClickCancel} />
					</section>
				</Modal>
			</>
		);
	};

	if (props.loading) {
		return renderLoadingSpinner();
	}

	if (props.error) {
		return renderErrorMessage();
	}

	if (!props.data.campaignInvite) {
		return renderNoInvite();
	}

	return renderModal();
};

LoginView.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	})
};

const LoginViewWithQuery = ({ match, closeModal }: { match: any; closeModal: any }) => (
	<Query query={GET_CAMPAIGN_INVITE} variables={{ inviteToken: match.params.inviteToken }}>
		{(result: QueryResult) => {
			const { loading, error, data } = result;
			return <LoginView loading={loading} error={error} data={data} closeModal={closeModal} inviteToken={match.params.inviteToken} />;
		}}
	</Query>
);

export default LoginViewWithQuery;
