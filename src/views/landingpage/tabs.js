import React, { Component } from 'react';
import { Card } from 'antd';
import { translate } from 'react-i18next';

import Brief from './tabs-content/brief.js';
import Faq from './tabs-content/faq.js';

class TabsCard extends Component {
	state = {
		noTitleKey: 'brief'
	};
	onTabChange = (key, type) => {
		this.setState({ [type]: key });
	};
	render() {
		const { t } = this.props;

		const contentListNoTitle = {
			brief: <Brief campaign={this.props.campaign} />,
			faq: <Faq />
		};

		const tabListNoTitle = [
			{
				key: 'brief',
				tab: t('landingPage:tabInformation', { defaultValue: 'Information' })
			},
			{
				key: 'faq',
				tab: t('landingPage:tabFaq', { defaultValue: 'FAQ' })
			}
		];

		return (
			<React.Fragment>
				<Card
					className='mt-40 transparent main-card'
					tabList={tabListNoTitle}
					activeTabKey={this.state.noTitleKey}
					onTabChange={(key) => {
						this.onTabChange(key, 'noTitleKey');
					}}
				>
					{contentListNoTitle[this.state.noTitleKey]}
				</Card>
			</React.Fragment>
		);
	}
}

export default translate('landingPage')(TabsCard);
