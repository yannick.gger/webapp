import IntegratedInboxContainer from 'components/IntegratedInbox/IntegratedInboxContainer';
import PageHeader from 'components/PageHeader';
import IntegratedInboxContextProvider from 'contexts/IntegratedInbox/Context.config';
import withTheme from 'hocs/with-theme';
import Styled from './IntegratedInbox.style';

const IntegratedInbox = () => {
	return (
		<Styled.Wrapper>
			<PageHeader headline='Inbox' />
			<Styled.Content>
				<IntegratedInboxContextProvider>
					<IntegratedInboxContainer />
				</IntegratedInboxContextProvider>
			</Styled.Content>
		</Styled.Wrapper>
	);
};

export default withTheme(IntegratedInbox, 'integratedInbox');
