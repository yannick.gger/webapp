import { useState, useEffect } from 'react';
import moment, { Moment } from 'moment';
import withTheme from 'hocs/with-theme';
import DiscoveryAuthService from 'services/Authentication/Discovery-api';
import Styled from './DiscoveryLandingPage.style';
import { SearchContainer } from 'components/Discovery';
import LoadingSpinner from 'components/LoadingSpinner';
import { StatusCode } from 'services/Response.types';
import PageHeader from 'components/PageHeader';

const DiscoveryLandingPage = () => {
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		setLoading(true);
		const discoveryToken: null | { token: string; expiresAt: Moment } = DiscoveryAuthService.getDiscoveryToken();
		if (discoveryToken) {
			const expiresAt = moment(new Date(discoveryToken.expiresAt.toString()));
			const now = moment();
			if (expiresAt < now) {
				DiscoveryAuthService.requestDiscoveryToken().then((res) => {
					if (res === StatusCode.OK) {
						setLoading(false);
					}
				});
			} else {
				setLoading(false);
			}
		} else {
			DiscoveryAuthService.requestDiscoveryToken().then((res) => {
				if (res === StatusCode.OK) {
					setLoading(false);
				}
			});
		}
	}, []);

	return (
		<Styled.Wrapper>
			<PageHeader headline='Discovery' />
			<Styled.Container>{loading ? <LoadingSpinner size={'lg'} position={'center'} /> : <SearchContainer />}</Styled.Container>
		</Styled.Wrapper>
	);
};

export default withTheme(DiscoveryLandingPage, 'discovery');
