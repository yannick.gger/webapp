import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	width: 100%;
	max-width: 1920px;
	height: 100vh;
	margin: 0 auto;

	overflow-y: auto;
	padding-bottom: 30px;
	background-color: ${colors.discovery.background};
`;

const Container = styled.div`
	display: flex;
	justify-content: center;
	padding-top: ${guttersWithRem.xxxl};
`;

const Styled = {
	Wrapper,
	Container
};

export default Styled;
