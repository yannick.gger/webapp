import DiscoveryPage from './DiscoveryPage';
import DiscoveryLandingPage from './DiscoveryLandingPage';

export { DiscoveryPage, DiscoveryLandingPage };
