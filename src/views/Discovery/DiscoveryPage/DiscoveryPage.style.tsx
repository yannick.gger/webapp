import styled from 'styled-components';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	width: 100%;
	max-width: 1920px;
	margin: 0 auto;
	padding-bottom: 30px;
	background-color: ${colors.discovery.background};
`;

const Styled = {
	Wrapper
};

export default Styled;
