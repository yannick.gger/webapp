import { useState, useEffect } from 'react';
import moment, { Moment } from 'moment';
import withTheme from 'hocs/with-theme';
import Styled from './DiscoveryPage.style';
import { DiscoveryContainer } from 'components/Discovery';
import { DiscoveryContextProvider } from 'contexts/Discovery/DiscoveryContext';

import DiscoveryAuthService from 'services/Authentication/Discovery-api';
import LoadingSpinner from 'components/LoadingSpinner';
import { StatusCode } from 'services/Response.types';

const DiscoveryPage = () => {
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		setLoading(true);
		const discoveryToken: null | { token: string; expiresAt: Moment } = DiscoveryAuthService.getDiscoveryToken();
		if (discoveryToken) {
			const expiresAt = moment(new Date(discoveryToken.expiresAt.toString()));
			const now = moment();
			if (expiresAt < now) {
				DiscoveryAuthService.requestDiscoveryToken().then((res) => {
					if (res === StatusCode.OK) {
						setLoading(false);
					}
				});
			} else {
				setLoading(false);
			}
		} else {
			DiscoveryAuthService.requestDiscoveryToken().then((res) => {
				if (res === StatusCode.OK) {
					setLoading(false);
				}
			});
		}
	}, []);

	return (
		<DiscoveryContextProvider>
			<Styled.Wrapper>{loading ? <LoadingSpinner size={'lg'} position={'center'} /> : <DiscoveryContainer />}</Styled.Wrapper>;
		</DiscoveryContextProvider>
	);
};

export default withTheme(DiscoveryPage, 'discovery');
