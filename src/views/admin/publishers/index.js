import React, { Component } from 'react';
import { Menu, Dropdown, Button, Layout } from 'antd';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';

import PublisherList from './publisher-list';

import getAllPublishersQuery from 'graphql/get-all-publishers.graphql';

const MenuItemGroup = Menu.ItemGroup;
const menu = (
	<Menu>
		<MenuItemGroup title='Invite'>
			<Menu.Item key='0'>
				<Link to='/admin/publishers/invite'>Invite new publisher</Link>
			</Menu.Item>
		</MenuItemGroup>
	</Menu>
);

export default class AdminPublishersView extends Component {
	render() {
		document.title = `Publishers | Admin | Collabs`;

		return (
			<Layout>
				<Layout.Header>
					<h1>Admin - Publishers</h1>
					<Dropdown overlay={menu} trigger={['click']} placement='bottomRight'>
						<Button type='primary' icon=' ion ion-plus' size='large' />
					</Dropdown>
				</Layout.Header>
				<Layout.Content>
					<Query query={getAllPublishersQuery} notifyOnNetworkStatusChange>
						{({ loading, error, data, fetchMore }) => <PublisherList data={data && data.admin} fetchMore={fetchMore} error={error} loading={loading} />}
					</Query>
				</Layout.Content>
			</Layout>
		);
	}
}
