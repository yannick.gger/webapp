import React, { Component } from 'react';
import { Table, Button, Empty } from 'antd';
import { lookup } from 'country-data';
import TableErrorImg from 'assets/img/app/table-error.svg';

import { LoginAsButton } from 'components';

const columns = [
	{
		title: 'ID',
		dataIndex: 'node.id',
		width: '80px'
	},
	{
		title: 'Name',
		dataIndex: 'node.name'
	},
	{
		title: 'Org#',
		dataIndex: 'node.organizationNumber'
	},
	{
		title: 'Country',
		dataIndex: 'node.country',
		render: (text) => lookup.countries({ alpha2: text })[0].name
	},
	{
		title: 'No. of organizations',
		dataIndex: 'node.organizationCount'
	},
	{
		title: 'Action',
		key: 'action',
		width: 360,
		render: (text, record) => (
			<span className='d-flex align-items-center'>
				<LoginAsButton mutationVariables={{ publisherId: record.node.id }} />
			</span>
		)
	}
];

class OrganizationList extends Component {
	state = {
		selectedRowKeys: [], // Check here to configure the default column
		initialLoading: false,
		moreLoading: false
	};
	onSelectChange = (selectedRowKeys) => {
		this.setState({ selectedRowKeys });
	};
	loadMore = (data, fetchMore) => {
		this.setState({ moreLoading: true });
		fetchMore({
			variables: {
				cursor: data.publishers.pageInfo.endCursor
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				this.setState({ moreLoading: false });

				const newEdges = fetchMoreResult.publishers.edges;
				const pageInfo = fetchMoreResult.publishers.pageInfo;

				return newEdges.length
					? {
							publishers: {
								__typename: previousResult.publishers.__typename,
								edges: [...previousResult.publishers.edges, ...newEdges],
								pageInfo
							}
					  }
					: previousResult;
			}
		});
	};
	componentDidUpdate = (prevProps) => {
		if (prevProps.loading !== this.props.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.loading });
		}
	};
	componentDidMount = () => {
		if (this.props.loading) {
			this.setState({ initialLoading: true });
		}
	};
	render() {
		const { error, data, fetchMore } = this.props;
		const { initialLoading, moreLoading } = this.state;

		return (
			<div>
				<Table
					className='pb-30'
					columns={columns}
					dataSource={data && data.publishers ? data.publishers.edges : []}
					rowKey={(record) => record.node.id}
					pagination={false}
					loading={initialLoading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load publishers' />
						) : (
							<Empty description='Nothing matching the search criterias found' />
						)
					}}
				/>

				{!error && !initialLoading && data && data.publishers && data.publishers.edges.length !== 0 && data.publishers.edges.length % 20 === 0 && (
					<div className='pb-30 text-center'>
						<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(data, fetchMore)}>
							Load more
						</Button>
					</div>
				)}
			</div>
		);
	}
}

export default OrganizationList;
