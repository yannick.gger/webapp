import React from 'react';
import PageHeader from 'components/PageHeader';
import CreationForm from 'components/Publisher/CreationForm';

const CreatePublisher = () => {
	return (
		<section>
			<PageHeader headline='Add a new publisher' />
			<CreationForm />
		</section>
	);
};

export default CreatePublisher;
