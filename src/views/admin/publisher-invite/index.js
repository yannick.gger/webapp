import React from 'react';
import { Modal, Button, message } from 'antd';
import { Mutation } from 'react-apollo';
import { injectIntl } from 'react-intl';

import WrappedImportForm from './form';

import createPublisherInviteMutation from 'graphql/create-publisher-invite.graphql';

class AdminPublisherInviteView extends React.Component {
	handleSubmit = (createPublisherInvite) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;
				const graphqlValues = {
					...values
				};

				createPublisherInvite({
					variables: graphqlValues
				})
					.then(({ data, error }) => {
						if (data.createPublisherInvite && !error) {
							message.info('Du har bjudit in en ny publisher');
							this.props.closeModal();
						} else {
							message.error('Något gick fel när inbjudan skulle skickas.');
							this.props.closeModal();
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		return (
			<Mutation mutation={createPublisherInviteMutation}>
				{(createPublisherInvite, { loading }) => (
					<Modal
						title='Send an publisher invite'
						visible
						onOk={this.handleOk}
						onCancel={this.handleCancel}
						wrapClassName='custom-modal box-parts title-center'
						maskClosable={false}
						footer={[
							<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(createPublisherInvite)}>
								Send invite
							</Button>
						]}
					>
						<WrappedImportForm wrappedComponentRef={this.saveFormRef} />
					</Modal>
				)}
			</Mutation>
		);
	}
}

export default injectIntl(AdminPublisherInviteView);
