import React from 'react';
import { Row, Col, Input, Form, Card } from 'antd';

const FormItem = Form.Item;

class InviteForm extends React.Component {
	render() {
		const { form } = this.props;
		const { getFieldDecorator } = form;

		return (
			<React.Fragment>
				<div className='description'>
					<p>{`When you have invited an publisher they will receive an email with a link to a form where they can join Collabs. The email address can't be changed, however publisher name is editable.`}</p>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>Information</h3>
					<Card className='small-title mb-10 mt-10'>
						<Row gutter={15}>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<FormItem label='Email'>
									{getFieldDecorator('email', {
										rules: [{ type: 'email', message: 'The input is not valid E-mail!' }, { required: true, message: 'Please enter an email address' }]
									})(<Input size='large' />)}
								</FormItem>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<FormItem label='Publisher name'>{getFieldDecorator('publisherName')(<Input size='large' />)}</FormItem>
							</Col>
						</Row>
					</Card>
				</div>
			</React.Fragment>
		);
	}
}
const WrappedInviteForm = Form.create()(InviteForm);
export default WrappedInviteForm;
