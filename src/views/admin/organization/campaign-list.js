import React, { Component } from 'react';
import { Table, Button, Empty } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { apiToMoment } from 'utils';
import TableErrorImg from 'assets/img/app/table-error.svg';

class OrganizationList extends Component {
	state = {
		selectedRowKeys: [], // Check here to configure the default column
		initialLoading: false,
		moreLoading: false
	};

	onSelectChange = (selectedRowKeys) => {
		this.setState({ selectedRowKeys });
	};

	loadMore = (after, fetchMore) => {
		this.setState({ moreLoading: true });

		fetchMore({
			variables: {
				after
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				const newEdges = fetchMoreResult.organization.campaignsConnection.edges;
				const pageInfo = fetchMoreResult.organization.campaignsConnection.pageInfo;

				return newEdges.length
					? {
							organization: {
								...previousResult.organization,
								campaignsConnection: {
									__typename: previousResult.organization.campaignsConnection.__typename,
									edges: [...previousResult.organization.campaignsConnection.edges, ...newEdges],
									pageInfo
								}
							}
					  }
					: previousResult;
			}
		}).finally(() => {
			this.setState({ moreLoading: false });
		});
	};

	componentDidUpdate = (prevProps) => {
		if (prevProps.loading !== this.props.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.loading });
		}
	};

	componentDidMount = () => {
		if (this.props.loading) {
			this.setState({ initialLoading: true });
		}
	};

	getTableColumns = () => {
		const {
			match: { params }
		} = this.props;

		return [
			{
				title: 'ID',
				dataIndex: 'node.id',
				width: '80px'
			},
			{
				title: 'Name',
				dataIndex: 'node.name'
			},
			{
				title: 'Invites sent',
				dataIndex: 'node.invitesSent',
				width: '150px',
				render: (text) => (text ? 'Yes' : 'No')
			},
			{
				title: 'Invites close at',
				width: '230px',
				dataIndex: 'node.invitesCloseAt',
				render: (text) => (text ? apiToMoment(text).format('YY-MM-DD') : '7d after invite')
			},
			{
				title: 'Invited',
				dataIndex: 'node.instagramOwnersInvitedCount'
			},
			{
				title: 'Spots',
				dataIndex: 'node.influencerTargetCount'
			},
			{
				title: 'Opened',
				dataIndex: 'node.instagramOwnersOpenedInviteCount'
			},
			{
				title: 'Clicked',
				dataIndex: 'node.instagramOwnersClickedInviteCount'
			},
			{
				title: 'Joined',
				dataIndex: 'node.instagramOwnersJoinedCount'
			},
			{
				title: 'Delivered',
				dataIndex: 'node.campaignDelivery',
				render: (text, record) => (text ? apiToMoment(record.node.campaignDelivery.latestDeliveryAt).format('YY-MM-DD') : 'No')
			},
			{
				title: 'Assignments',
				dataIndex: 'node.assignmentsCount'
			},
			{
				title: 'Products',
				dataIndex: 'node.compensationsCount'
			},
			{
				title: 'Action',
				key: 'action',
				width: 100,
				render: (text, record) => (
					<span className='d-flex align-items-center justify-content-space-between'>
						<Link to={`/admin/organizations/${params.organizationSlug}/campaigns/${record.node.id}`}>
							<Button size='small'>View</Button>
						</Link>
					</span>
				)
			}
		];
	};

	render() {
		const { initialLoading, moreLoading } = this.state;
		const { error, data, fetchMore } = this.props;
		const {
			campaignsConnection: { edges: campaigns, pageInfo }
		} = data.organization || { campaignsConnection: { edges: [], pageInfo: {} } };

		return (
			<div>
				<Table
					className='pb-30'
					columns={this.getTableColumns()}
					dataSource={campaigns}
					rowKey={(record) => record.node.id}
					pagination={false}
					loading={initialLoading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load campaigns' />
						) : (
							<Empty description='Nothing matching the search criterias found' />
						)
					}}
				/>
				{!error && !initialLoading && pageInfo.hasNextPage && (
					<div className='pb-30 text-center'>
						<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(pageInfo.endCursor, fetchMore)}>
							Load more
						</Button>
					</div>
				)}
			</div>
		);
	}
}

export default withRouter(OrganizationList);
