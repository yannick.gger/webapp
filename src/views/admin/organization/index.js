import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Spin, Layout } from 'antd';
import { Query } from 'react-apollo';
import CampaignList from './campaign-list';

import getOrganizationBySlugQuery from 'graphql/get-organization-by-slug.graphql';
import AssignmentFragment from 'graphql/fragments/assignmentFragment.graphql';
import CampaignReviewFragment from 'graphql/fragments/campaignReviewFragment.graphql';
import CampaignReviewReasonFragment from 'graphql/fragments/campaignReviewReasonFragment.graphql';
import OrganizationFragment from 'graphql/fragments/organization-fragment.graphql';
import CampaignLogoFragment from 'graphql/fragments/campaignLogoFragment.graphql';
import CampaignBrandAssetFragment from 'graphql/fragments/campaignBrandAssetFragment.graphql';
import CampaignInspirationFragment from 'graphql/fragments/campaignInspirationFragment.graphql';
import CampaignFragment from 'graphql/fragments/campaignFragment.graphql';
import InvoiceCompensationFragment from 'graphql/fragments/invoiceCompensationFragment.graphql';
import PaymentCompensationFragment from 'graphql/fragments/paymentCompensationFragment.graphql';
import ProductFragment from 'graphql/fragments/productFragment.graphql';
import ProductImageFragment from 'graphql/fragments/productImageFragment.graphql';
import OrganizationCampaignFragment from 'graphql/organization-campaign-fragment.graphql';
import CampaignCoverPhotoFragment from 'graphql/fragments/campaignCoverPhotoFragment.graphql';
import InstagramPostFragment from 'graphql/fragments/InstagramPostFragment.graphql';
import InstagramStoryFragment from 'graphql/fragments/instagramStoryFragment.graphql';
import TiktokFragment from 'graphql/fragments/tiktokFragment.graphql';

const getOrganizationCampaignsConnectionQuery = gql`
	query getOrganizationCampaignsConnection(
		$organizationSlug: String!
		$invitesSent: Boolean
		$after: String
		$shortOrgInfo: Boolean = false
		$skipCampaignListIrreleventFields: Boolean = false
		$withCampaignListFields: Boolean = false
		$sharedToTeam: Boolean = true
	) {
		organization(slug: $organizationSlug) {
			id
			campaignsConnection(invitesSent: $invitesSent, first: 10, after: $after) @connection(key: "campaignsConnection", filter: []) {
				pageInfo {
					endCursor
					hasNextPage
				}
				edges {
					node {
						...OrganizationCampaignFragment
					}
				}
			}
		}
	}
	${OrganizationCampaignFragment}
	${CampaignFragment}
	${AssignmentFragment}
	${CampaignReviewReasonFragment}
	${CampaignReviewFragment}
	${OrganizationFragment}
	${CampaignLogoFragment}
	${CampaignBrandAssetFragment}
	${CampaignInspirationFragment}
	${InvoiceCompensationFragment}
	${PaymentCompensationFragment}
	${ProductFragment}
	${ProductImageFragment}
	${InstagramPostFragment}
	${CampaignCoverPhotoFragment}
	${InstagramStoryFragment}
	${TiktokFragment}
`;
export default class AdminOrganizationsView extends Component {
	render() {
		document.title = `Organizations | Admin | Collabs`;
		const {
			match: {
				params: { organizationSlug }
			}
		} = this.props;

		return (
			<Query query={getOrganizationBySlugQuery} variables={{ slug: organizationSlug }}>
				{({ data, loading }) => {
					if (loading) {
						return HUEHEUHU;
					}

					return (
						<Layout>
							<Layout.Header>
								<h1>Admin - {data.organization.name}</h1>
							</Layout.Header>
							<Layout.Content>
								<h2>Campaigns</h2>
								<Query query={getOrganizationCampaignsConnectionQuery} variables={{ organizationSlug: organizationSlug }} notifyOnNetworkStatusChange>
									{({ loading, error, data, fetchMore }) => {
										return <CampaignList data={data} fetchMore={fetchMore} error={error} loading={loading} />;
									}}
								</Query>
							</Layout.Content>
						</Layout>
					);
				}}
			</Query>
		);
	}
}
