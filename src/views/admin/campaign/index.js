import React, { Component } from 'react';
import { Spin, Layout } from 'antd';
import { Query } from 'react-apollo';
import InstagramOwnerAssignmentList from './instagram-owner-assignment-list';

import getOrganizationBySlugQuery from 'graphql/get-organization-by-slug.graphql';
import getCampaignInstagramOwnerAssignmentsQuery from 'graphql/get-campaign-instagram-owner-assignments.graphql';

export default class AdminOrganizationsView extends Component {
	render() {
		document.title = `Organizations | Admin | Collabs`;

		return (
			<Query query={getOrganizationBySlugQuery} variables={{ slug: this.props.match.params.organizationSlug }}>
				{({ data, loading }) => {
					if (loading) return <Spin className='collabspin' />;
					return (
						<Layout>
							<Layout.Header>
								<h1>Admin - {data.organization.name}</h1>
							</Layout.Header>
							<Layout.Content>
								<h2>Instagram posts</h2>
								<Query query={getCampaignInstagramOwnerAssignmentsQuery} variables={{ id: this.props.match.params.campaignId }} notifyOnNetworkStatusChange>
									{({ loading, error, data, fetchMore }) => <InstagramOwnerAssignmentList data={data} fetchMore={fetchMore} error={error} loading={loading} />}
								</Query>
							</Layout.Content>
						</Layout>
					);
				}}
			</Query>
		);
	}
}
