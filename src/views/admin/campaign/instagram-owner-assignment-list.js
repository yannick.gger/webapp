import React, { Component } from 'react';
import { Table, Button, Modal, Empty } from 'antd';
import { withRouter } from 'react-router-dom';
import TableErrorImg from 'assets/img/app/table-error.svg';

class OrganizationList extends Component {
	state = {
		initialLoading: false,
		moreLoading: false,
		preview: {
			visible: false
		}
	};
	loadMore = (data, fetchMore) => {
		this.setState({ moreLoading: true });
		fetchMore({
			variables: {
				cursor: data.campaign.campaignInstagramOwnerAssignments.pageInfo.endCursor
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				this.setState({ moreLoading: false });

				const newEdges = fetchMoreResult.campaign.campaignInstagramOwnerAssignments.edges;
				const pageInfo = fetchMoreResult.campaign.campaignInstagramOwnerAssignments.pageInfo;

				return newEdges.length
					? {
							campaign: {
								...previousResult.campaign,
								campaignInstagramOwnerAssignments: {
									__typename: previousResult.campaign.campaignInstagramOwnerAssignments.__typename,
									edges: [...previousResult.campaign.campaignInstagramOwnerAssignments.edges, ...newEdges],
									pageInfo
								}
							}
					  }
					: previousResult;
			}
		});
	};
	handleClosePreview = () => {
		this.setState({ preview: { visible: false } });
	};
	componentDidUpdate = (prevProps) => {
		if (prevProps.loading !== this.props.loading && !this.state.moreLoading) {
			this.setState({ initialLoading: this.props.loading });
		}
	};
	componentDidMount = () => {
		if (this.props.loading) {
			this.setState({ initialLoading: true });
		}
	};
	render() {
		const { error, data, fetchMore } = this.props;
		const { initialLoading, moreLoading } = this.state;

		const columns = [
			{
				title: 'ID',
				dataIndex: 'node.id',
				width: '80px'
			},
			{
				title: 'Instagram',
				dataIndex: 'node.instagramUrl',
				render: (text) => (
					<a href={text} target='_blank' rel='noopener noreferrer'>
						Open
					</a>
				)
			},
			{
				title: 'Username',
				dataIndex: 'node.instagramOwnerId',
				render: (text) => (
					<a href={`https://instagram.com/${text}`} target='_blank' rel='noopener noreferrer'>
						@{text}
					</a>
				)
			},
			{
				title: 'Assignment',
				dataIndex: 'node.assignment.name'
			},
			{
				title: 'Screenshots',
				dataIndex: 'node.campaignInstagramOwnerAssignmentScreenshots',
				render: (screenshots) => {
					return screenshots.map((screenshot) => (
						<img
							alt='Screenshot'
							key={screenshot.photoThumbnail}
							src={screenshot.photoThumbnail}
							style={{ width: 50, height: 50, marginRight: 5 }}
							onClick={() => this.setState({ preview: { visible: true, screenshot: screenshot.photo } })}
						/>
					));
				}
			}
		];
		return (
			<div>
				<Modal visible={this.state.preview.visible} footer={null} onCancel={this.handleClosePreview}>
					<img alt='example' style={{ width: '100%' }} src={this.state.preview.screenshot} />
				</Modal>
				<Table
					className='pb-30'
					columns={columns}
					dataSource={data && data.campaign ? data.campaign.campaignInstagramOwnerAssignments.edges : []}
					rowKey={(record) => record.node.id}
					pagination={false}
					loading={initialLoading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load campaigns' />
						) : (
							<Empty description='Nothing matching the search criterias found' />
						)
					}}
				/>

				{!error &&
					!initialLoading &&
					data &&
					data.campaign &&
					data.campaign.campaignInstagramOwnerAssignments.edges.length !== 0 &&
					data.campaign.campaignInstagramOwnerAssignments.edges.length % 20 === 0 && (
						<div className='pb-30 text-center'>
							<Button type='primary' loading={moreLoading} onClick={() => this.loadMore(data, fetchMore)}>
								Load more
							</Button>
						</div>
					)}
			</div>
		);
	}
}

export default withRouter(OrganizationList);
