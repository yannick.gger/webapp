import React, { Component } from 'react';
import { Spin } from 'antd';
import { Query } from 'react-apollo';
import Users from './users';
import getInfluencers from 'graphql/get-influencer-users.graphql';

class InfluencerUsers extends Component {
	render() {
		const { organizationType, userType, search, setRef } = this.props;
		return (
			<Query query={getInfluencers} variables={{ search }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
				{({ data, loading: queryLoading, fetchMore }) => {
					if (queryLoading && !data.influencerUsers) {
						return <Spin className='collabspin' />;
					}
					const tableDataSource = data && data.influencerUsers ? data.influencerUsers.edges : [];
					const users = data && data.influencerUsers.edges ? data.influencerUsers.edges.map((item) => item.node) : [];
					const hasNextPage = data && data.influencerUsers && data.influencerUsers.pageInfo.hasNextPage;
					const cursor = data && data.influencerUsers ? data.influencerUsers.pageInfo.endCursor : null;
					return (
						<Users
							ref={setRef}
							tableDataSource={tableDataSource}
							users={users}
							fetchMore={fetchMore}
							hasNextPage={hasNextPage}
							cursor={cursor}
							loading={queryLoading}
							organizationType={organizationType}
							userType={userType}
						/>
					);
				}}
			</Query>
		);
	}
}

export default InfluencerUsers;
