import React from 'react';
import { Modal } from 'antd';
import { Query, withApollo } from 'react-apollo';
import { Button, Layout, Tooltip, Tag, Input, Row, Col, message, Spin } from 'antd';
import PaginatedTable from 'components/paginated-table';
import getAllOrganizationsQuery from 'graphql/get-all-organizations.graphql';
import { apiToMoment } from 'utils';
import { lookup } from 'country-data';
import grantAccessToClient from 'graphql/grant_access_to_client.graphql';
import salerAassignedClient from 'graphql/saler-assigned-client.graphql';
import './styles.scss';

class Organizations extends React.Component {
	state = {
		search: '',
		disabled: false
	};
	handleSearch = (value) => {
		this.setState({ search: value });
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	grantAccessToClient = (clientIds) => {
		const { client, location } = this.props;
		client
			.mutate({
				mutation: grantAccessToClient,
				variables: { clientIds, salerId: location.state.clientIds[0] },
				refetchQueries: ['salerAssignedClients', 'organizationUsers']
			})
			.then((response) => {
				if (response.data.assignClientsToSaler) {
					message.success('Grant access to client successfully');
				}
			})
			.catch(({ graphQLErrors }) => {
				graphQLErrors &&
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
			});
	};
	render() {
		const { location } = this.props;
		const columns = [
			{
				title: 'Name',
				dataIndex: 'node.name'
			},
			{
				title: 'Status',
				dataIndex: 'node.status',
				render: (text, { node }) => (
					<div>
						<span style={{ marginRight: '3px' }}>{text}</span>
						{node.isTrial && (
							<Tooltip title={`Trial ends at ${apiToMoment(node.trialEndsAt).format('YYYY-MM-DD')}`}>
								<Tag color='blue'>Trial</Tag>
							</Tooltip>
						)}
						{node.testAccount && (
							<Tooltip title="Account is a test account used and can't create campaigns">
								<Tag color='orange'>Test</Tag>
							</Tooltip>
						)}
						{node.isSubscribed && (
							<Tooltip title='Account is on basic subscription'>
								<Tag color='green'>Basic</Tag>
							</Tooltip>
						)}
						{!node.isSubscribed && !node.testAccount && !node.isTrial && (
							<Tooltip title='Account is on basic subscription'>
								<Tag color='red'>No subscription</Tag>
							</Tooltip>
						)}
					</div>
				)
			},
			{
				title: 'Org#',
				dataIndex: 'node.organizationNumber'
			},
			{
				title: 'Country',
				dataIndex: 'node.country',
				render: (text) => lookup.countries({ alpha2: text })[0].name
			},
			{
				title: 'No. of campaigns',
				dataIndex: 'node.campaignCount'
			},
			{
				title: 'Action',
				key: 'action',
				width: 360,
				render: (text, record) => {
					const clientIds = record.node.id;
					return (
						<Query query={salerAassignedClient} variables={{ salerId: location.state.clientIds[0] }}>
							{({ data, loading: queryLoading }) => {
								if (queryLoading) {
									return <Spin className='collabspin' />;
								}
								const saleAccount =
									data && data.salerAssignedClients && data.salerAssignedClients.edges.find((object) => object.node.organization.id === clientIds);
								return (
									<span className='d-flex align-items-center'>
										<Button
											className='mr-10'
											size='small'
											onClick={() => this.grantAccessToClient(clientIds)}
											disabled={saleAccount !== undefined ? true : false}
										>
											Grant access to client
										</Button>
									</span>
								);
							}}
						</Query>
					);
				}
			}
		];
		const { Search } = Input;
		const { search } = this.state;
		return (
			<Modal
				title='Grant access to client'
				visible={true}
				onOk={this.handleOk}
				onCancel={this.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
				width='100vw'
				className='grant-client-modal-container'
			>
				<Layout>
					<Layout.Content>
						<Row>
							<Col md={{ span: 12 }} />
							<Col md={{ span: 12 }}>
								<div className='pb-25 pt-20'>
									<Search placeholder='Search organization name' onSearch={(value) => this.handleSearch(value)} />
								</div>
							</Col>
						</Row>
						<Query
							query={getAllOrganizationsQuery}
							variables={{ search, salerId: location.state.clientIds[0] }}
							notifyOnNetworkStatusChange
							fetchPolicy='cache-and-network'
						>
							{(queryRes) => <PaginatedTable columns={columns} queryRes={queryRes} dataRef='organizations' />}
						</Query>
					</Layout.Content>
				</Layout>
			</Modal>
		);
	}
}

export default withApollo(Organizations);
