import React, { Component } from 'react';
import { Spin } from 'antd';
import { Query } from 'react-apollo';
import Users from './users';
import getOrganizationUsers from 'graphql/get-organization-users.graphql';

class OrganizationUsers extends Component {
	render() {
		const { organizationType, userType, search, setRef } = this.props;
		return (
			<Query query={getOrganizationUsers} variables={{ userType: organizationType, search }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
				{({ data, loading: queryLoading, fetchMore }) => {
					if (queryLoading && !data.organizationUsers) {
						return <Spin className='collabspin' />;
					}
					const tableDataSource = data && data.organizationUsers ? data.organizationUsers.edges : [];
					const users = data && data.organizationUsers && data.organizationUsers.edges ? data.organizationUsers.edges.map((item) => item.node) : [];
					const hasNextPage = data && data.organizationUsers && data.organizationUsers.pageInfo.hasNextPage;
					const cursor = data && data.organizationUsers ? data.organizationUsers.pageInfo.endCursor : null;
					return (
						<Users
							ref={setRef}
							tableDataSource={tableDataSource}
							users={users}
							fetchMore={fetchMore}
							hasNextPage={hasNextPage}
							cursor={cursor}
							loading={queryLoading}
							organizationType={organizationType}
							userType={userType}
						/>
					);
				}}
			</Query>
		);
	}
}

export default OrganizationUsers;
