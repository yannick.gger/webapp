import React, { Component } from 'react';
import { Layout, Input, Col, Row, Spin, Menu } from 'antd';
import { Query } from 'react-apollo';
import Users from './users';
import getAllUsersQuery from 'graphql/get-all-users.graphql';
import PublisherUsers from './publisher-users.js';
import InfluencerUsers from './influencer-users.js';
import OrganizationUsers from './organization-users.js';
import ButtonLink from 'components/button-link';
import './styles.scss';

const { SubMenu } = Menu;
export default class AdminUsersView extends Component {
	state = {
		userType: 'all',
		organizationType: 'all',
		current: 'all',
		search: ''
	};

	handleClick = (e) => {
		this.setState({
			current: e.key
		});
	};

	handleSearch = (value) => {
		this.setState({ search: value });
	};

	setRef = (instance) => {
		this.userChild = instance;
	};

	setPage = (page) => {
		this.setState({ organizationType: page });
		if (this.userChild) {
			this.userChild.setState({
				selectedRowKeys: [],
				selectedUsers: []
			});
		}
	};

	render() {
		const { userType, organizationType, current, search } = this.state;
		document.title = `Users | Admin | Collabs`;
		return (
			<Layout>
				<Layout.Header>
					<h1>Admin - Users</h1>
				</Layout.Header>
				<Layout.Content>
					<Row className='mb-20 d-flex align-items-center'>
						<div className='users-navigation-wrapper'>
							<Col sm={{ span: 12 }}>
								<Menu onClick={this.handleClick} selectedKeys={[current]} mode='horizontal'>
									<Menu.Item key='all' onClick={() => this.setState({ userType: 'all' })}>
										All
									</Menu.Item>
									<Menu.Item key='app' onClick={() => this.setState({ userType: 'publisher' })}>
										Publisher
									</Menu.Item>
									<SubMenu title='Organization' style={{ paddingBottom: '8px' }} onClick={() => this.setState({ userType: 'organization' })}>
										<Menu.ItemGroup>
											<Menu.Item key='all-organization' onClick={() => this.setPage('all')}>
												All
											</Menu.Item>
											<Menu.Item key='admin' onClick={() => this.setPage('admin')}>
												Admin
											</Menu.Item>
											<Menu.Item key='test' onClick={() => this.setPage('test')}>
												Test
											</Menu.Item>
											<Menu.Item key='sale' onClick={() => this.setPage('sale')}>
												Sale
											</Menu.Item>
										</Menu.ItemGroup>
									</SubMenu>
									<Menu.Item key='influencer' onClick={() => this.setState({ userType: 'influencer' })}>
										<span>Influencer</span>
									</Menu.Item>
								</Menu>
							</Col>
							<Col className='pl-20 d-flex' style={{ justifyContent: 'flex-end' }} sm={{ span: 12 }}>
								{<Input.Search placeholder='Search user name, email or instagram account' onSearch={(value) => this.handleSearch(value)} />}
							</Col>
						</div>
					</Row>
					{userType === 'publisher' && <PublisherUsers setRef={this.setRef} userType={userType} organizationType={organizationType} search={search} />}
					{userType === 'influencer' && <InfluencerUsers setRef={this.setRef} userType={userType} organizationType={organizationType} search={search} />}
					{userType === 'organization' && <OrganizationUsers setRef={this.setRef} userType={userType} organizationType={organizationType} search={search} />}
					{userType === 'all' && (
						<Query query={getAllUsersQuery} variables={{ search }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
							{({ data, loading: queryLoading, fetchMore }) => {
								if (queryLoading && !data.users) {
									return <Spin className='collabspin' />;
								}

								const tableDataSource = data && data.users ? data.users.edges : [];
								const users = data && data.users.edges ? data.users.edges.map((item) => item.node) : [];
								const hasNextPage = data && data.users && data.users.pageInfo.hasNextPage;
								const cursor = data && data.users ? data.users.pageInfo.endCursor : null;
								return (
									<Users
										ref={this.setRef}
										tableDataSource={tableDataSource}
										users={users}
										fetchMore={fetchMore}
										hasNextPage={hasNextPage}
										cursor={cursor}
										loading={queryLoading}
										organizationType={organizationType}
										userType={userType}
									/>
								);
							}}
						</Query>
					)}
				</Layout.Content>
			</Layout>
		);
	}
}
