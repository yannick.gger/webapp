import React, { Component } from 'react';
import { Spin } from 'antd';
import { Query } from 'react-apollo';
import Users from './users';
import getPublishers from 'graphql/get-publisher-users.graphql';

class PublisherUser extends Component {
	render() {
		const { organizationType, userType, search, setRef } = this.props;
		return (
			<Query query={getPublishers} variables={{ search }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
				{({ data, loading: queryLoading, fetchMore }) => {
					if (queryLoading && !data.publisherUsers) {
						return <Spin className='collabspin' />;
					}
					const tableDataSource = data && data.publisherUsers ? data.publisherUsers.edges : [];
					const users = data && data.publisherUsers.edges ? data.publisherUsers.edges.map((item) => item.node) : [];
					const hasNextPage = data && data.publisherUsers && data.publisherUsers.pageInfo.hasNextPage;
					const cursor = data && data.publisherUsers ? data.publisherUsers.pageInfo.endCursor : null;
					return (
						<Users
							ref={setRef}
							tableDataSource={tableDataSource}
							users={users}
							fetchMore={fetchMore}
							hasNextPage={hasNextPage}
							cursor={cursor}
							loading={queryLoading}
							organizationType={organizationType}
							userType={userType}
						/>
					);
				}}
			</Query>
		);
	}
}

export default PublisherUser;
