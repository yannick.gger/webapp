import React, { Component } from 'react';
import TableErrorImg from 'assets/img/app/table-error.svg';
import PermisssionUser from './permission-user-menu';
import { Tag, Button, Icon, Table, Empty, Divider } from 'antd';
import { lookup } from 'country-data';
import { getInstagramUrl } from 'utils';
import { LoginAsButton } from 'components';
import { Link } from 'react-router-dom';

class Users extends Component {
	state = {
		selectedRowKeys: [],
		selectedUsers: []
	};
	onSelectChange = (users) => {
		return (selectedRowKeys) => {
			this.setState((prevState) => {
				const previousSelectedUsers = prevState.selectedUsers;
				const newSelectedUsers = selectedRowKeys
					.filter((key) => !prevState.selectedUsers.some(({ id }) => id === key))
					.map((key) => users.find((i) => i.id === key));

				let resSelectedUsers = [...newSelectedUsers, ...previousSelectedUsers].filter(({ id }) => selectedRowKeys.some((key) => key === id));

				return {
					selectedRowKeys,
					selectedUsers: resSelectedUsers
				};
			});
		};
	};
	loadMore = (users, fetchMore) => {
		const { userType } = this.props;
		return fetchMore({
			variables: {
				after: this.props.cursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				let result = {};
				if (userType === 'all') {
					result = {
						users: {
							...fetchMoreResult.users,
							edges: [...prev.users.edges, ...fetchMoreResult.users.edges]
						}
					};
				}
				if (userType === 'organization') {
					result = {
						organizationUsers: {
							...fetchMoreResult.organizationUsers,
							edges: [...prev.organizationUsers.edges, ...fetchMoreResult.organizationUsers.edges]
						}
					};
				}
				if (userType === 'influencer') {
					result = {
						influencerUsers: {
							...fetchMoreResult.influencerUsers,
							edges: [...prev.influencerUsers.edges, ...fetchMoreResult.influencerUsers.edges]
						}
					};
				}
				if (userType === 'publisher') {
					result = {
						publisherUsers: {
							...fetchMoreResult.publisherUsers,
							edges: [...prev.publisherUsers.edges, ...fetchMoreResult.publisherUsers.edges]
						}
					};
				}
				return result;
			}
		});
	};
	resetSelection = () => {
		this.setState({ selectedRowKeys: [] });
	};

	render() {
		const { emptyState, error, fetchMore, tableDataSource, users, hasNextPage, loading, organizationType, userType } = this.props;
		const columns = [
			{
				title: 'ID',
				key: 'id',
				dataIndex: 'node.id',
				width: '80px'
			},
			{
				title: 'Name',
				key: '2',
				dataIndex: 'node.name',
				width: 300
			},
			{
				title: 'Email',
				key: '3',
				dataIndex: 'node.email'
			},
			{
				title: 'Phone',
				dataIndex: 'node.userAccountSetting.phone',
				width: 500
			},
			{
				title: 'Country',
				dataIndex: 'node.userAccountSetting.country',
				width: '80px',
				render: (text) => (lookup.countries({ alpha2: text })[0] ? lookup.countries({ alpha2: text })[0].emoji : '')
			},
			{
				title: 'Instagram account',
				dataIndex: 'node.instagramOwner.instagramOwnerUser.username',
				render: (userName) => (
					<a href={getInstagramUrl(userName)} target='_blank' rel='noopener noreferrer'>
						{userName}
					</a>
				)
			},
			{
				title: 'Types',
				width: 300,
				dataIndex: 'node.isAdmin',
				render: (_, { node }) => (
					<div>
						{node.organizations && node.organizations.length > 0 && <Tag color='blue'>Org</Tag>}
						{node.publishers && node.publishers.edges.length > 0 && <Tag color='pink'>Publisher</Tag>}
						{node.instagramOwnerId && <Tag color='green'>Influencer</Tag>}
						{node.isAdmin && <Tag color='gold'>Admin</Tag>}
					</div>
				)
			},
			{
				title: 'Action',
				key: 'action',
				width: 360,
				render: (text, record) => {
					return (
						<span className='d-flex align-items-center justify-content-space-between'>
							<LoginAsButton mutationVariables={{ userId: record.node.id }} disabled={selectedRowKeys.includes(record.node.id) ? true : false} />
							{userType === 'organization' && (organizationType === 'sale' || record.node.isSaleUser) && <Divider type='vertical' />}
							{((userType === 'organization' && organizationType === 'sale') || (userType === 'organization' && record.node.isSaleUser)) && (
								<Button size='small'>
									<Link
										to={{
											pathname: '/admin/users/remove-access',
											state: { salerId: record.node.id }
										}}
									>
										Manage
									</Link>
								</Button>
							)}
						</span>
					);
				}
			}
		];
		const rowSelection = (users) => ({
			selectedRowKeys,
			onChange: this.onSelectChange(users)
		});
		const { selectedRowKeys } = this.state;
		const hasSelected = selectedRowKeys.length > 0;
		return (
			<div>
				<PermisssionUser
					disabled={!hasSelected}
					users={selectedRowKeys.map((key) => ({
						id: key
					}))}
					organizationType={organizationType}
					userType={userType}
					resetSelection={this.resetSelection}
					selectedRowKeys={selectedRowKeys}
				>
					{() => (
						<Button type='primary'>
							Manage... <Icon type='down' />
						</Button>
					)}
				</PermisssionUser>
				<span className='ml-20'>{hasSelected ? `${selectedRowKeys.length} ${selectedRowKeys.length > 1 ? 'users' : 'user'} selected` : ''}</span>
				<Table
					className='pb-30'
					columns={columns}
					dataSource={tableDataSource}
					rowKey={(record) => record.node.id}
					rowSelection={rowSelection(users)}
					pagination={false}
					loading={loading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load data' />
						) : (
							<Empty description={emptyState || 'Nothing matching the search criterias found'} />
						)
					}}
				/>
				{hasNextPage && (
					<div className='pb-30 text-center'>
						<Button type='primary' loading={loading} onClick={() => this.loadMore(users, fetchMore)}>
							Load more
						</Button>
					</div>
				)}
			</div>
		);
	}
}

export default Users;
