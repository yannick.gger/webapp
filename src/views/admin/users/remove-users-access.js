import React from 'react';
import { Modal } from 'antd';
import { Query, withApollo } from 'react-apollo';
import { Button, Layout, Tooltip, Tag, message } from 'antd';
import { apiToMoment } from 'utils';
import PaginatedTable from 'components/paginated-table';
import { lookup } from 'country-data';
import salerAassignedClient from 'graphql/saler-assigned-client.graphql';
import unAssignClients from 'graphql/unAssign-client.graphql';
import './styles.scss';

class Organizations extends React.Component {
	state = {
		disabled: false
	};
	handleCancel = () => {
		this.props.closeModal();
	};

	unAssignClient = (clientIds) => {
		const { client, location } = this.props;
		client
			.mutate({
				mutation: unAssignClients,
				variables: { salerId: location && location.state.salerId, clientIds },
				refetchQueries: ['salerAssignedClients', 'grantAccessToClient', 'organizationUsers']
			})
			.then((response) => {
				if (response.data.unassignClientsFromSaler) {
					message.success('Remove access successfully');
				}
			})
			.catch(({ graphQLErrors }) => {
				graphQLErrors &&
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
			});
	};
	render() {
		const columns = [
			{
				title: 'ID',
				dataIndex: 'node.organization.id',
				width: '80px'
			},
			{
				title: 'Name',
				dataIndex: 'node.organization.name'
			},
			{
				title: 'Status',
				dataIndex: 'node.status',
				render: (text, { node }) => (
					<div>
						<span style={{ marginRight: '3px' }}>{text}</span>
						{node.isTrial && (
							<Tooltip title={`Trial ends at ${apiToMoment(node.trialEndsAt).format('YYYY-MM-DD')}`}>
								<Tag color='blue'>Trial</Tag>
							</Tooltip>
						)}
						{node.testAccount && (
							<Tooltip title="Account is a test account used and can't create campaigns">
								<Tag color='orange'>Test</Tag>
							</Tooltip>
						)}
						{node.isSubscribed && (
							<Tooltip title='Account is on basic subscription'>
								<Tag color='green'>Basic</Tag>
							</Tooltip>
						)}
						{!node.isSubscribed && !node.testAccount && !node.isTrial && (
							<Tooltip title='Account is on basic subscription'>
								<Tag color='red'>No subscription</Tag>
							</Tooltip>
						)}
					</div>
				)
			},
			{
				title: 'Org#',
				dataIndex: 'node.organization.organizationNumber'
			},
			{
				title: 'Country',
				dataIndex: 'node.organization.country',
				render: (text) => lookup.countries({ alpha2: text })[0].name
			},
			{
				title: 'No. of campaigns',
				dataIndex: 'node.organization.campaignCount'
			},
			{
				title: 'Action',
				key: 'action',
				width: 360,
				render: (record) => {
					const clientIds = record.node.organization.id;
					return (
						<span className='d-flex align-items-center'>
							<Button className='mr-10 remove-access-button' size='small' onClick={() => this.unAssignClient(clientIds)}>
								Remove access
							</Button>
						</span>
					);
				}
			}
		];
		return (
			<Modal
				title='Manage clients'
				visible={true}
				onOk={this.handleOk}
				onCancel={this.handleCancel}
				wrapClassName='custom-modal no-padding footer-center ant-modal-borderless title-center vertical-center-modal'
				maskClosable={false}
				footer={[]}
				width='100vw'
				className='grant-client-modal-container'
			>
				<Layout>
					<Layout.Content>
						<Query
							query={salerAassignedClient}
							variables={{ salerId: this.props.location.state.salerId }}
							notifyOnNetworkStatusChange
							fetchPolicy='cache-and-network'
						>
							{(queryRes) => <PaginatedTable columns={columns} queryRes={queryRes} dataRef='salerAssignedClients' />}
						</Query>
					</Layout.Content>
				</Layout>
			</Modal>
		);
	}
}

export default withApollo(Organizations);
