import React, { Component } from 'react';
import { Dropdown, Menu, message, Button, Spin } from 'antd';
import { withApollo, Query } from 'react-apollo';
import { Link } from 'react-router-dom';
import grantUsersPrivilege from 'graphql/grant-users-privilege.graphql';
import revokeUsersPrivilege from 'graphql/revoke-users-privilege.graphql';
import ButtonLink from 'components/button-link';
import getOrganizationUsers from 'graphql/get-organization-users.graphql';

class PermisssionUser extends Component {
	removeUserRole = (role) => {
		const { users, client, resetSelection } = this.props;
		const userValues = {
			userIds: users.map(({ id }) => id),
			privilege: role
		};
		client
			.mutate({
				mutation: revokeUsersPrivilege,
				variables: userValues,
				refetchQueries: ['getAllUsers', 'organizationUsers', 'influencerUsers', 'publisherUsers']
			})
			.then((response) => {
				if (response && response.data && response.data.revokeUsersPrivilege) {
					message.success(`Removed user ${role} role`);
					resetSelection();
				} else {
					message.error('Something went wrong');
				}
			});
	};

	updateUsersRole = (role) => {
		const { client, users, resetSelection } = this.props;
		const userValues = {
			userIds: users.map(({ id }) => id),
			privilege: role
		};
		client
			.mutate({
				mutation: grantUsersPrivilege,
				variables: userValues,
				refetchQueries: ['getAllUsers', 'organizationUsers', 'influencerUsers', 'publisherUsers']
			})
			.then((response) => {
				if (response && response.data && response.data.grantUsersPrivilege) {
					message.success(`Updated user(s) as ${role}`);
					resetSelection();
				} else {
					message.error('Something went wrong');
				}
			})
			.catch(({ graphQLErrors }) => {
				graphQLErrors &&
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
			});
	};

	render() {
		let usersPermissionMenu;
		const { organizationType, userType, users, disabled, children, selectedRowKeys } = this.props;
		let grantAccessClientModalLink = null;
		if (users.length < 2) {
			grantAccessClientModalLink = (
				<Link
					to={{
						pathname: '/admin/users/grant-access-client',
						state: { clientIds: users.map(({ id }) => id) }
					}}
				>
					Grant access to client
				</Link>
			);
		} else {
			grantAccessClientModalLink = (
				<Button
					className='link-button'
					style={{ border: 'none', padding: 0, margin: 0, height: 0 }}
					onClick={() => message.error('Please select individual sales account at a time!')}
				>
					Grant access to client
				</Button>
			);
		}
		if (organizationType === 'admin' && userType === 'organization') {
			usersPermissionMenu = () => (
				<Menu>
					<Menu.ItemGroup title='permission'>
						<Menu.Item>
							<ButtonLink onClick={() => this.removeUserRole('admin')}>Remove user as Admin</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.updateUsersRole('test')}>Make user as Test</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.updateUsersRole('sale')}>Make user as Sale</ButtonLink>
						</Menu.Item>
					</Menu.ItemGroup>
				</Menu>
			);
		} else if (organizationType === 'test' && userType === 'organization') {
			usersPermissionMenu = () => (
				<Menu>
					<Menu.ItemGroup title='permission'>
						<Menu.Item>
							<ButtonLink onClick={() => this.removeUserRole('test')}>Remove user as Test</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.updateUsersRole('admin')}>Make user as Admin</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.updateUsersRole('sale')}>Make user as Sale</ButtonLink>
						</Menu.Item>
					</Menu.ItemGroup>
				</Menu>
			);
		} else if (userType === 'organization' && organizationType === 'sale') {
			usersPermissionMenu = () => {
				return (
					<Menu>
						<Menu.ItemGroup title='permission'>
							{<Menu.Item>{grantAccessClientModalLink}</Menu.Item>}
							<Menu.Item>
								<ButtonLink onClick={() => this.removeUserRole('sale')}>Remove user as Sale</ButtonLink>
							</Menu.Item>
							<Menu.Item>
								<ButtonLink onClick={() => this.updateUsersRole('admin')}>Make user as Admin</ButtonLink>
							</Menu.Item>
							<Menu.Item>
								<ButtonLink onClick={() => this.updateUsersRole('test')}>Make user as Test</ButtonLink>
							</Menu.Item>
						</Menu.ItemGroup>
					</Menu>
				);
			};
		} else if (userType === 'organization' && organizationType === 'all') {
			usersPermissionMenu = ({ data }) => {
				const saleAccountIds = data.map((account) => account.node.id);
				const checkSaleAccountIncluding = selectedRowKeys.some((key) => saleAccountIds.includes(key));
				return (
					<Menu>
						<Menu.ItemGroup title='permission'>
							{checkSaleAccountIncluding && <Menu.Item>{grantAccessClientModalLink}</Menu.Item>}
							<Menu.Item>
								<ButtonLink onClick={() => this.updateUsersRole('admin')}>Make user as Admin</ButtonLink>
							</Menu.Item>
							<Menu.Item>
								<ButtonLink onClick={() => this.removeUserRole('admin')}>Remove user as Admin</ButtonLink>
							</Menu.Item>
							<Menu.Item>
								<ButtonLink onClick={() => this.updateUsersRole('test')}>Make user as Test</ButtonLink>
							</Menu.Item>
							<Menu.Item>
								<ButtonLink onClick={() => this.removeUserRole('test')}>Remove user as Test</ButtonLink>
							</Menu.Item>
							<Menu.Item>
								<ButtonLink onClick={() => this.updateUsersRole('sale')}>Make user as Sale</ButtonLink>
							</Menu.Item>
							<Menu.Item>
								<ButtonLink onClick={() => this.removeUserRole('sale')}>Remove user as Sale</ButtonLink>
							</Menu.Item>
						</Menu.ItemGroup>
					</Menu>
				);
			};
		} else {
			usersPermissionMenu = () => (
				<Menu>
					<Menu.ItemGroup title='permission'>
						<Menu.Item>
							<ButtonLink onClick={() => this.updateUsersRole('admin')}>Make user as Admin</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.removeUserRole('admin')}>Remove user as Admin</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.updateUsersRole('test')}>Make user as Test</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.removeUserRole('test')}>Remove user as Test</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.updateUsersRole('sale')}>Make user as Sale</ButtonLink>
						</Menu.Item>
						<Menu.Item>
							<ButtonLink onClick={() => this.removeUserRole('sale')}>Remove user as Sale</ButtonLink>
						</Menu.Item>
					</Menu.ItemGroup>
				</Menu>
			);
		}
		if (userType === 'organization' || userType === 'all') {
			return (
				<Query query={getOrganizationUsers} variables={{ userType: 'sale', search: '' }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
					{({ data, loading: queryLoading }) => {
						if (queryLoading) {
							return <Spin className='collabspin' />;
						}
						const organizationTypeData = data && data.organizationUsers ? data.organizationUsers.edges : [];
						return (
							<Dropdown className='mb-20' overlay={usersPermissionMenu({ data: organizationTypeData })} trigger={['click']} disabled={disabled}>
								{children()}
							</Dropdown>
						);
					}}
				</Query>
			);
		} else {
			return null;
		}
	}
}

export default withApollo(PermisssionUser);
