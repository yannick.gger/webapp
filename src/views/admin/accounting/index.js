import React, { Component } from 'react';
import moment from 'moment';
import { Spin, Layout, Table, Icon, Tooltip, Select, Card, DatePicker, Button, Row, Col, message } from 'antd';
import { Query, Mutation } from 'react-apollo';
import PaginatedTable from 'components/paginated-table';
import { apiToMoment } from 'utils';
import getAccountingDiffsForPeriod from 'graphql/get-accounting-diffs-for-period.graphql';
import getAccountingPeriodReports from 'graphql/get-accounting-period-reports.graphql';
import getAccountingHeldBalances from 'graphql/get-accounting-held-balances.graphql';
import createAccountingPeriodReport from 'graphql/create-accounting-period-report.graphql';

export default class AdminAccountingView extends Component {
	state = {
		year: null,
		month: null,
		generatingReport: false,
		endDate: moment().format('YYYY-MM-DD'),
		reportRange: [
			moment()
				.subtract(1, 'month')
				.startOf('month'),
			moment()
				.subtract(1, 'month')
				.endOf('month')
		]
	};
	handleHeldBalancesEndDateChange = (endDate) => {
		this.setState({ endDate });
	};
	handleMonthChange = (value) => {
		this.setState({ year: parseInt(value.split('-')[0]), month: parseInt(value.split('-')[1]) });
	};
	render() {
		document.title = `Accounting | Admin | Collabs`;
		let dateStart = moment('2018-10-01');
		let dateEnd = moment();
		let timeValues = [];

		while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
			timeValues.push(<Select.Option key={dateEnd.endOf('month').format('YYYY-MM-DD')}>{dateEnd.format('MMMM YYYY')}</Select.Option>);
			dateEnd.subtract(1, 'month');
		}

		return (
			<Layout>
				<Layout.Header>
					<h1>Admin - Accounting</h1>
				</Layout.Header>
				<Layout.Content>
					<h2>Links</h2>
					<Card className='mb-30'>
						<React.Fragment>
							<a href='https://dashboard.stripe.com/account/documents' target='_blank' rel='noopener noreferrer'>
								Tax invoice
							</a>
							<br />
							<a href='https://dashboard.stripe.com/balance' target='_blank' rel='noopener noreferrer'>
								Balance transactions
							</a>
						</React.Fragment>
					</Card>
					<h2>Reports</h2>
					<Card className='mb-10'>
						<Row gutter={16}>
							<Col span={6}>
								<DatePicker.RangePicker placeHolder='Select period' value={this.state.reportRange} onChange={(val) => this.setState({ reportRange: val })} />
							</Col>
							<Col span={12}>
								<Mutation mutation={createAccountingPeriodReport}>
									{(createAccountingPeriodReport, { loading }) => {
										const create = () => {
											createAccountingPeriodReport({
												variables: {
													startDate: this.state.reportRange[0].format('YYYY-MM-DDTHH:mm:ss'),
													endDate: this.state.reportRange[1].format('YYYY-MM-DDTHH:mm:ss')
												},
												refetchQueries: [{ query: getAccountingPeriodReports }]
											})
												.then(({ data }) => {
													if (data.createAccountingPeriodReport.errors.length > 0) {
														data.createAccountingPeriodReport.errors.forEach((error) => {
															message.error(error.message);
														});
													} else if (data.createAccountingPeriodReport.errors.length === 0) {
														message.success('Generating report');
													}
												})
												.catch((error) => {
													message.error(error.message);
												});
										};
										return (
											<Button type='primary' size='small' onClick={create} loading={loading}>
												Generate report
											</Button>
										);
									}}
								</Mutation>
							</Col>
						</Row>
					</Card>
					<Query query={getAccountingPeriodReports}>
						{(queryRes) => (
							<PaginatedTable
								columns={[
									{ title: 'Created', dataIndex: 'node.createdAt', key: 'createdAt', render: (text) => apiToMoment(text).format('YYYY-MM-DD') },
									{ title: 'Start date', dataIndex: 'node.startDate', key: 'startDate' },
									{ title: 'End date', dataIndex: 'node.endDate', key: 'endDate' },
									{
										title: 'Influencer invoices',
										dataIndex: 'node.influencerInvoicesZipUrl',
										key: 'influencerInvoicesZipUrl',
										render: (url) => (url ? <a href={url}>Download</a> : 'Not Ready')
									}
								]}
								queryRes={queryRes}
								dataRef='admin.accountingPeriodReports'
							/>
						)}
					</Query>
					<br />
					<h2>Held balances</h2>
					<Card className='mb-10'>
						<Select size='large' style={{ width: '300px' }} onChange={this.handleHeldBalancesEndDateChange} placeholder='Get until end of month of'>
							{timeValues}
						</Select>
					</Card>
					<Card className='mb-30'>
						<Query query={getAccountingHeldBalances} variables={{ endDate: this.state.endDate }}>
							{({ data, loading }) => {
								if (loading) return <Spin className='collabspin' />;
								return (
									<Table
										scroll={{ x: 1500 }}
										columns={[
											{
												title: 'Organization',
												dataIndex: 'organization.name',
												key: 'organization.name'
											},
											{
												title: 'Campaign ID',
												dataIndex: 'campaign.id',
												key: 'campaign.id'
											},
											{
												title: 'Campaign name',
												dataIndex: 'campaign.name',
												key: 'campaign.name'
											},
											{
												title: 'Invoice amount inc VAT',
												dataIndex: 'invoiceAmount',
												key: 'invoiceAmount'
											},
											{
												title: 'Paid to influencer inc VAT',
												dataIndex: 'paidToInfluencersAmount',
												key: 'paidToInfluencersAmount'
											},
											{
												title: 'Refunded from influencer inc VAT',
												dataIndex: 'refundFromInfluencersAmount',
												key: 'refundFromInfluencersAmount'
											},
											{
												title: 'Earned fees inc VAT',
												dataIndex: 'earnedFeesAmount',
												key: 'earnedFeesAmount'
											},
											{
												title: 'Refunded inc VAT',
												dataIndex: 'refundedAmount',
												key: 'refundedAmount'
											},
											{
												title: 'Reserved for influencers inc VAT',
												dataIndex: 'heldBalanceAmount',
												key: 'heldBalanceAmount'
											}
											/*,
                      {
                        title: "Amount (EUR)",
                        dataIndex: "amount",
                        key: "amount",
                        render: amount => (
                          <span style={{ color: amount < 0 && "red" }}>
                            {amount > 0 && "+"}
                            {amount / 100}
                          </span>
                        )
                      },
                      {
                        title: "",
                        dataIndex: "validPartSum",
                        key: "validPartSum",
                        render: validPartSum =>
                          !validPartSum && (
                            <Tooltip title="Sum is not the same as part sums. Report to developer">
                              <Icon type="warning" />
                            </Tooltip>
                          )
                      }*/
										]}
										dataSource={data && data.admin ? data.admin.accountingHeldBalances : []}
										rowKey={(record) => record.id}
										pagination={false}
										childrenColumnName='parts'
										defaultExpandAllRows
									/>
								);
							}}
						</Query>
					</Card>
					<br />
					<h2>Balance diff</h2>
					<Card className='mb-10'>
						<Select size='large' style={{ width: '300px' }} onChange={this.handleMonthChange} placeholder='Select Period'>
							{timeValues}
						</Select>
					</Card>
					<Card className='mb-30'>
						{this.state.year ? (
							<Query query={getAccountingDiffsForPeriod} variables={{ year: this.state.year, month: this.state.month }}>
								{({ data, loading }) => {
									if (loading) return <Spin className='collabspin' />;
									return (
										<Table
											columns={[
												{ title: 'Specification', dataIndex: 'title', key: 'title' },
												{
													title: 'Amount (EUR)',
													dataIndex: 'amount',
													key: 'amount',
													render: (amount) => (
														<span style={{ color: amount < 0 && 'red' }}>
															{amount > 0 && '+'}
															{amount / 100}
														</span>
													)
												},
												{
													title: '',
													dataIndex: 'validPartSum',
													key: 'validPartSum',
													render: (validPartSum) =>
														!validPartSum && (
															<Tooltip title='Sum is not the same as part sums. Report to developer'>
																<Icon type='warning' />
															</Tooltip>
														)
												}
											]}
											dataSource={data && data.admin ? data.admin.accountingDiffsForPeriod : []}
											rowKey={(record) => record.id}
											pagination={false}
											childrenColumnName='parts'
											defaultExpandAllRows
										/>
									);
								}}
							</Query>
						) : (
							<span>Select a month to see accounting diffs</span>
						)}
					</Card>
				</Layout.Content>
			</Layout>
		);
	}
}
