import React, { Component } from 'react';
import { Button, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { UserContext } from '../../../contexts';

import { setGhostToken, redirectToHome, hasGhostToken } from 'services/auth-service';
import signInAsGhostMutation from 'graphql/sign-in-as-ghost.graphql';
class LoginAsGhostUser extends Component {
	static contextType = UserContext;

	state = {
		loading: false
	};
	loginAsGhostUser = (client, organizationId) => {
		this.setState({ loading: true });
		if (hasGhostToken()) {
			message.error('Oops! You’ve already ghost logged in as another user!');
			this.setState({ loading: false });
			return;
		} else {
			client
				.mutate({
					mutation: signInAsGhostMutation,
					variables: { organizationId: organizationId }
				})
				.then(({ data }) => {
					setGhostToken(data && data.signInAsGhost && data.signInAsGhost.token, this.props.location.pathname);
					this.context.updateIsGhostUser();
					// TODO tell PHP API that we are impersonating
					// TODO update CollabsAuthService with new token
					redirectToHome({ history: this.props.history });
				});
		}
		this.setState({ loading: false });
	};
	render() {
		const { organizationId } = this.props;
		return (
			<ApolloConsumer>
				{(client) => (
					<Button size='small' loading={this.state.loading} onClick={() => this.loginAsGhostUser(client, organizationId)}>
						Login as
					</Button>
				)}
			</ApolloConsumer>
		);
	}
}

export default withRouter(LoginAsGhostUser);
