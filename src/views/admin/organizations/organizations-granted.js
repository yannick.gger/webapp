import React, { Component } from 'react';
import { Menu, Dropdown, Button, Layout, Tooltip, Tag, Spin } from 'antd';
import { Query, withApollo } from 'react-apollo';
import { Link } from 'react-router-dom';
import { lookup } from 'country-data';
import { apiToMoment } from 'utils';
import LoginAsButton from './login-as-ghost-user.js';
import salerAassignedClient from 'graphql/saler-assigned-client.graphql';
import getCurrentUser from 'graphql/get-current-user.graphql';
import PaginatedTable from 'components/paginated-table';
class SaleOrganizationsView extends Component {
	render() {
		document.title = `Organizations | Sales | Collabs`;
		const grantColumns = [
			{
				title: 'ID',
				dataIndex: 'node.organization.id',
				width: '80px'
			},
			{
				title: 'Name',
				dataIndex: 'node.organization.name'
			},
			{
				title: 'Status',
				dataIndex: 'node.status',
				render: (text, { node }) => (
					<div>
						<span style={{ marginRight: '3px' }}>{text}</span>
						{node.isTrial && (
							<Tooltip title={`Trial ends at ${apiToMoment(node.trialEndsAt).format('YYYY-MM-DD')}`}>
								<Tag color='blue'>Trial</Tag>
							</Tooltip>
						)}
						{node.testAccount && (
							<Tooltip title="Account is a test account used and can't create campaigns">
								<Tag color='orange'>Test</Tag>
							</Tooltip>
						)}
						{node.isSubscribed && (
							<Tooltip title='Account is on basic subscription'>
								<Tag color='green'>Basic</Tag>
							</Tooltip>
						)}
						{!node.isSubscribed && !node.testAccount && !node.isTrial && (
							<Tooltip title='Account is on basic subscription'>
								<Tag color='red'>No subscription</Tag>
							</Tooltip>
						)}
					</div>
				)
			},
			{
				title: 'Org#',
				dataIndex: 'node.organizationNumber'
			},
			{
				title: 'Country',
				dataIndex: 'node.organization.country',
				render: (text) => lookup.countries({ alpha2: text })[0].name
			},
			{
				title: 'No. of campaigns',
				dataIndex: 'node.campaignCount'
			},
			{
				title: 'Action',
				key: 'action',
				width: 360,
				render: (text, record) => {
					return (
						<span className='d-flex align-items-center'>
							<Link
								to={{
									pathname: `/sale/organization/${record.node.organization.slug}`,
									state: {
										name: record.node.organization.name
									}
								}}
							>
								<Button className='mr-10' size='small'>
									View
								</Button>
							</Link>
							<LoginAsButton organizationId={record.node.organization.id} />
						</span>
					);
				}
			}
		];
		const MenuItemGroup = Menu.ItemGroup;
		const menu = (
			<Menu>
				<MenuItemGroup title='Invite'>
					<Menu.Item key='0'>
						<Link to='/admin/organizations/invite'>Invite new organization</Link>
					</Menu.Item>
				</MenuItemGroup>
			</Menu>
		);
		return (
			<Layout>
				<Layout.Header>
					<h1>Sales - Organizations</h1>
					<Dropdown overlay={menu} trigger={['click']} placement='bottomRight'>
						<Button type='primary' icon=' ion ion-plus' size='large' />
					</Dropdown>
				</Layout.Header>
				<Layout.Content>
					<Query query={getCurrentUser}>
						{({ data, loading: queryLoading }) => {
							if (queryLoading) {
								return <Spin className='collabspin' />;
							}
							const salerId = data && data.currentUser && data.currentUser.id;
							return (
								<Query query={salerAassignedClient} variables={{ salerId }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
									{(queryRes) => <PaginatedTable columns={grantColumns} queryRes={queryRes} dataRef='salerAssignedClients' />}
								</Query>
							);
						}}
					</Query>
				</Layout.Content>
			</Layout>
		);
	}
}

export default withApollo(SaleOrganizationsView);
