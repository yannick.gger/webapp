import { useState } from 'react';
import { ClickParam } from 'antd/lib/menu';
import { Menu, Dropdown, Button, Layout, Tooltip, Tag, Row, Col, Input } from 'antd';
import { Query, QueryResult } from 'react-apollo';
import { Link } from 'react-router-dom';
import { lookup } from 'country-data';

import { apiToMoment } from 'utils';
import PaginatedTable from 'components/paginated-table';
import { LoginAsButton } from 'components';

import getAllOrganizationsQuery from 'graphql/get-all-organizations.graphql';

const columns = [
	{
		title: 'ID',
		dataIndex: 'node.id',
		width: '80px'
	},
	{
		title: 'Name',
		dataIndex: 'node.name'
	},
	{
		title: 'Status',
		dataIndex: 'node.status',
		render: (text: string, { node }: QueryResult['data']['edges']) => {
			return (
				<div>
					<span style={{ marginRight: '3px' }}>{text}</span>
					{node.isTrial && (
						<Tooltip title={`Trial ends at ${apiToMoment(node.trialEndsAt).format('YYYY-MM-DD')}`}>
							<Tag color='blue'>Trial</Tag>
						</Tooltip>
					)}
					{node.testAccount && (
						<Tooltip title='Account is a test'>
							<Tag color='orange'>Test</Tag>
						</Tooltip>
					)}
					{node.isSubscribed && (
						<Tooltip title='Account is on basic subscription'>
							<Tag color='green'>Basic</Tag>
						</Tooltip>
					)}
					{!node.isSubscribed && !node.testAccount && !node.isTrial && (
						<Tooltip title='Account is on no subscription'>
							<Tag color='red'>No subscription</Tag>
						</Tooltip>
					)}
				</div>
			);
		}
	},
	{
		title: 'Org#',
		dataIndex: 'node.organizationNumber'
	},
	{
		title: 'Country',
		dataIndex: 'node.country',
		render: (text: string) => lookup.countries({ alpha2: text })[0].name
	},
	{
		title: 'Action',
		key: 'action',
		width: 360,
		render: (text: string, record: { node: { slug: string; id: string } }) => (
			<span className='d-flex align-items-center'>
				<Link to={`/admin/organization/${record.node.slug}`}>
					<Button className='mr-10' size='small'>
						View
					</Button>
				</Link>
				<LoginAsButton mutationVariables={{ organizationId: record.node.id }} />
			</span>
		)
	}
];

const menu = (
	<Menu>
		<Menu.ItemGroup title='Invite'>
			<Menu.Item key='0'>
				<Link to='/admin/organizations/invite'>Invite new organization</Link>
			</Menu.Item>
		</Menu.ItemGroup>
	</Menu>
);

const AdminOrganizationsView = (): JSX.Element => {
	const [orgType, setOrgType] = useState('all');
	const [search, setSearch] = useState('');

	const selectTabOptionHandler = (e: ClickParam) => {
		setOrgType(e.key);
	};

	const searchOrganizationHandler = (value: string) => {
		setSearch(value);
	};

	document.title = `Organizations | Admin | Collabs`;

	return (
		<Layout>
			<Layout.Header>
				<h1>Admin - Organizations</h1>
			</Layout.Header>
			<Layout.Content>
				<Row className='mb-20 d-flex align-items-center'>
					<div className='users-navigation-wrapper'>
						<Col sm={{ span: 12 }}>
							<Menu onClick={selectTabOptionHandler} selectedKeys={[orgType]} mode='horizontal'>
								<Menu.Item key='all'>All</Menu.Item>
								<Menu.Item key='basic'>Basic</Menu.Item>
								<Menu.Item key='trial'>Trial</Menu.Item>
								<Menu.Item key='test'>Test</Menu.Item>
								<Menu.Item key='noSub'>No subscription</Menu.Item>
							</Menu>
						</Col>
						<Col className='pl-20 d-flex' style={{ justifyContent: 'flex-end' }} sm={{ span: 12 }}>
							{<Input.Search placeholder='Search organization by name or org. nr.' onSearch={(value) => searchOrganizationHandler(value)} />}
						</Col>
					</div>
				</Row>
				<Query query={getAllOrganizationsQuery} variables={{ orgType, search }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
					{(queryRes: QueryResult) => <PaginatedTable columns={columns} queryRes={queryRes} dataRef='organizations' />}
				</Query>
			</Layout.Content>
		</Layout>
	);
};

export default AdminOrganizationsView;
