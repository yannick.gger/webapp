import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Col, Row, Input, notification } from 'antd';
import createInstagramPost from 'graphql/create-instagram-post.graphql';
import { Mutation } from 'react-apollo';
import UploadPostMedia from './upload-post-media.js';
import InstagramInsightsService from 'services/FacebookApi/InstagramInsights.service';
import LoadingSpinner from 'components/LoadingSpinner';
import { StatusCode } from 'services/Response.types';
import ToastContext from 'contexts/ToastContext';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { FEATURE_FLAG_FACEBOOK_AUTH } from 'constants/feature-flag-keys';
import { isValidUrl } from 'shared/utils/url';
import { getIGIErrorMessage } from 'services/FacebookApi/ErrorCodes';
import InstagramPostService from 'services/InstagramPost/InstagramPost.service';
import FetchNewData from './fetch-new-data';
import RequestScreenshots from './request-screenshots.js';
import { InsightsData } from './types';
import './styles.scss';

/**
 *  Instagram Post form
 * @todo i18next
 * @todo REFACTOR COMPONENT
 * @todo remove mutation when we have endpoint to create and can upload image
 * @param {*} props
 * @returns Component
 */
const InstagramPostForm = (props) => {
	const { getFieldDecorator } = props.form;
	const { ownerId, assignmentId, assignments, connectedWithFacebook } = props;

	const assignment = assignments.edges.filter((assignment) => Number(assignment.node.id) === assignmentId);
	const [insights, setInsights] = useState({ commentCount: 0, likeCount: 0, reach: 0, impressions: 0 });
	const [lockFields, setLockFields] = useState(connectedWithFacebook);
	const [fetched, setFetched] = useState(!connectedWithFacebook);
	const [photoUploadStatus, setPhotoUploadStatus] = useState(false);
	const [apiLoading, setApiLoading] = useState(false);
	const [isEnabled] = useFeatureToggle();
	const { addToast } = useContext(ToastContext);

	const onSubmit = (createInstagramPost, postId) => {
		return (e) => {
			e.preventDefault();
			props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				const formValues = {
					...values,
					likeCount: Number(values.likeCount),
					commentsCount: Number(values.commentsCount),
					reach: Number(values.reach),
					impressions: Number(values.impressions),
					campaignId: Number(props.campaignId),
					assignmentId: Number(values.assignmentId)
				};

				if (!formValues.id) {
					// Create a new post if we don't have one get.
					createInstagramPost({ variables: formValues })
						.then(() => {
							addToast({ id: 'create-ig-post', mode: 'success', message: 'The campaign report has been updated' });
						})
						.catch((err) => {
							addToast({ id: 'patch-ig-post', mode: 'error', message: 'Oops! someting went wrong!' });
						});
				} else {
					setApiLoading(true);
					// Patch if we have a postId
					InstagramPostService.patchPost(formValues.id, {
						commentsCount: formValues.commentsCount || 0,
						impressions: formValues.impressions || 0,
						likeCount: formValues.likeCount || 0,
						reach: formValues.reach || 0,
						shortcode: formValues.shortcode
					}).then((result: ICollabsResponse) => {
						if (result && result.errors === undefined) {
							addToast({ id: 'patch-ig-post', mode: 'success', message: 'The campaign report has been updated' });
							connectedWithFacebook && fetchInsightData(formValues.id);
						} else {
							addToast({ id: 'patch-ig-post-error', mode: 'error', message: 'Oops! Someting went wrong' });
						}
						setApiLoading(false);
					});
				}
			});
		};
	};

	const handlePhotoUploadDone = () => {
		setPhotoUploadStatus(true);
	};

	let post;
	if (assignment[0].node.instagramPosts.length > 0) {
		assignment[0].node.instagramPosts.forEach((igPost) => {
			if (igPost.ownerInstagramId === ownerId) post = igPost;
		});
	}

	const unLock = () => {
		setLockFields(false);
		setFetched(true);
	};

	const fetchInsightData = (postId, shortcode) => {
		if (isValidUrl(post.shortcode) && isEnabled(FEATURE_FLAG_FACEBOOK_AUTH)) {
			setFetched(false);
			InstagramInsightsService.getPostMetrics(post.id, post.shortcode).then((result) => {
				if (result && result.data) {
					const values = result.data.data.attributes;
					const included = result.data.included;

					setInsights({
						updatedDate: values.insightDataUpdatedAt,
						commentCount: values.commentsCount,
						likeCount: values.likeCount,
						reach: values.reach,
						impressions: values.impressions
					});

					props.updateInstagramUsers(ownerId, included[0].attributes.username, included[0].attributes.profilePictureUrl);
				} else {
					unLock();
					if (result.errors && result.errors.length > -1) {
						const err = result.errors[0];
						const message = getIGIErrorMessage(err.code, err.meta?.subErrorCode);
						addToast({ id: 'fetch-insight-data-error', mode: 'error', message: message });
					}
				}
				setFetched(true);
			});
		} else {
			unLock();
		}
	};

	useEffect(() => {
		if (props.activeKey) {
			if (post && post.id && !fetched) {
				if (connectedWithFacebook === true) {
					fetchInsightData(post.id, post.shortcode);
				}
			}
		}
	}, [props.activeKey]);

	useEffect(() => {
		if (photoUploadStatus) {
			const fieldsValue = props.form.getFieldsValue();
			props.form.setFieldsValue({ ...fieldsValue });
		}
	}, [photoUploadStatus]);

	return (
		<Mutation mutation={createInstagramPost} refetchQueries={['getOrganizationCampaign']}>
			{(createInstagramPost, { loading }) => (
				<>
					<div className='report-manage-actions'>
						{fetched && connectedWithFacebook ? <FetchNewData postId={post && post.id} setFetched={setFetched} setInsightsData={setInsights} /> : null}
						<RequestScreenshots assignmentId={Number(assignmentId)} instagramOwnerId={ownerId} />
					</div>
					<Form hideRequiredMark onSubmit={onSubmit(createInstagramPost, post && post.id)}>
						<section>
							<Row gutter={24}>
								{getFieldDecorator('ownerInstagramId', {
									initialValue: ownerId
								})(<Input type='hidden' />)}

								{getFieldDecorator('assignmentId', {
									initialValue: assignmentId
								})(<Input type='hidden' />)}

								{getFieldDecorator('id', {
									initialValue: post && post.id
								})(<Input type='hidden' />)}

								<Col span={12}>
									<Form.Item label='Likes'>
										{!lockFields
											? getFieldDecorator('likeCount', {
													initialValue: post ? post.likeCount : '0',
													rules: [{ required: true, message: 'Please enter likes' }]
											  })(<Input style={{ width: '100%' }} type='number' placeholder='Please enter likes' />)
											: insights.likeCount}
									</Form.Item>
								</Col>
								<Col span={12}>
									<Form.Item label='Comments'>
										{!lockFields
											? getFieldDecorator('commentsCount', {
													initialValue: post ? post.commentsCount : '0',
													rules: [{ required: true, message: 'Please enter comments' }]
											  })(<Input style={{ width: '100%' }} type='number' placeholder='Please enter comments' />)
											: insights.commentCount}
									</Form.Item>
								</Col>
								<Col span={12}>
									<Form.Item label='Reach'>
										{!lockFields
											? getFieldDecorator('reach', {
													initialValue: post ? post.reach : '0',
													rules: [{ required: true, message: 'Please enter reach' }]
											  })(<Input style={{ width: '100%' }} type='number' placeholder='Please enter reach' />)
											: insights.reach}
									</Form.Item>
								</Col>
								<Col span={12}>
									<Form.Item label='Impressions'>
										{!lockFields
											? getFieldDecorator('impressions', {
													initialValue: post ? post.impressions : '0',
													rules: [{ required: true, message: 'Please enter impressions' }]
											  })(<Input style={{ width: '100%' }} type='number' placeholder='Please enter impressions' />)
											: insights.impressions}
									</Form.Item>
								</Col>
								{fetch && connectedWithFacebook ? (
									<div className={`fetching-data ${!fetched ? 'visible' : ''}`}>
										<div className='fetching-data__inner'>
											<LoadingSpinner size='sm' />
											<span className='fetching-data__text'>Fetching stats from Instagram...</span>
										</div>
									</div>
								) : null}
							</Row>
							<Row gutter={24}>
								<Col span={12}>
									<Form.Item label='Url to post'>
										{getFieldDecorator('shortcode', {
											initialValue: post ? post.shortcode : '',
											rules: [{ required: true, message: 'Please enter url to post' }]
										})(<Input style={{ width: '100%' }} placeholder='Please enter url to post' />)}
									</Form.Item>
								</Col>
								<Col span={24}>
									<Form.Item style={{ width: '100%' }} label='Upload image'>
										{getFieldDecorator('mediaUrl')(<UploadPostMedia post={post} form={props.form} onPhotoUploadDone={handlePhotoUploadDone} />)}
									</Form.Item>
								</Col>
								<Col span={24}>
									<Form.Item label='Post text (description)'>
										{getFieldDecorator('caption', {
											initialValue: post ? post.caption : '',
											rules: [
												{
													required: true,
													message: 'Please enter post text'
												}
											]
										})(<Input.TextArea rows={4} placeholder='Please enter post text' />)}
									</Form.Item>
								</Col>
								<Col xs={24} style={{ textAlign: 'right' }}>
									<Button loading={loading || apiLoading} htmlType='submit' type='primary'>
										Save
									</Button>
								</Col>
							</Row>
						</section>
					</Form>
				</>
			)}
		</Mutation>
	);
};

const App = Form.create()(InstagramPostForm);

export default App;
