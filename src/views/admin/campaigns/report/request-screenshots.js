import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { Button, message } from 'antd';
import requestInsight from 'graphql/request-screenshots.graphql';

class RequestScreenshots extends Component {
	handleRequestInsight = () => {
		const { client, assignmentId, instagramOwnerId } = this.props;
		client
			.mutate({
				mutation: requestInsight,
				variables: {
					assignmentId,
					instagramOwnerId
				},
				refetchQueries: ['getCurrentUser', 'getOrganizationCampaigns']
			})
			.then((result) => {
				if (result.data.requestCampaignAssignmentScreenshots.sent) {
					message.success('Request was sent');
				} else {
					message.error('Somthing went wrong');
				}
			});
	};
	render() {
		return (
			<Button type='primary' size='small' className='ml-10' onClick={this.handleRequestInsight}>
				Request screenshot
			</Button>
		);
	}
}

export default withApollo(RequestScreenshots);
