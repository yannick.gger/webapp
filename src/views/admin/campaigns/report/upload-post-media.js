import React, { Component } from 'react';
import { Upload, Icon, message, Modal } from 'antd';
export default class UploadPostMedia extends Component {
	state = {
		previewVisible: false,
		previewImage: '',
		fileList: []
	};

	componentDidMount() {
		if (this.props.post) {
			const { post } = this.props;
			const { id, mediaUrl } = post;
			this.setState({
				fileList: [...this.state.fileList, { uid: post && id, name: 'mediaUrl', status: 'done', url: post && mediaUrl }]
			});
		}
	}

	handleChange = (info) => {
		const { status } = info.file;
		const { form, onPhotoUploadDone } = this.props;
		if (!info.file.isExceedSize) {
			this.setState({ fileList: info.fileList });
		}
		if (status === 'done') {
			message.success(`${info.file.name} file uploaded successfully.`);
		}

		form.setFields({
			mediaUrl: {
				value: info.file.originFileObj
			}
		});
		onPhotoUploadDone && onPhotoUploadDone();
	};

	beforeUpload = (file) => {
		const sizeInMB = file.size / (1024 * 1024);
		if (sizeInMB > 10) {
			file.isExceedSize = true;
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');
			return false;
		}
		return true;
	};

	handlePreview = (file) => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleCancel = () => this.setState({ previewVisible: false });

	render() {
		const { fileList, previewVisible } = this.state;
		const uploadButton = (
			<div>
				<Icon type='plus' />
				<div className='ant-upload-text'>Upload</div>
			</div>
		);

		let mediaUrlSrc = '';
		if (this.props.post && this.props.post.mediaUrl) {
			mediaUrlSrc = this.props.post.mediaUrl;
		} else {
			mediaUrlSrc = '/coverPhoto-sample.png';
		}

		return (
			<section className='d-flex'>
				<Upload
					onPreview={this.handlePreview}
					listType='picture-card'
					accept='image/*'
					fileList={fileList}
					beforeUpload={this.beforeUpload}
					onChange={this.handleChange}
					onCancel={this.handleCancel}
				>
					{fileList.length >= 1 ? null : uploadButton}
				</Upload>
				<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
					<img alt='example' style={{ width: '100%' }} src={mediaUrlSrc} />
				</Modal>
			</section>
		);
	}
}
