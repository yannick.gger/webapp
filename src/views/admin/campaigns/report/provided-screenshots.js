import React, { Component } from 'react';
import { Carousel, Button, Icon } from 'antd';

class ProvidedScreenshots extends Component {
	state = {
		preview: false
	};

	carousel = React.createRef();

	handleOpenPreview = (i) => {
		this.carousel.goTo(i, this.state.preview ? false : true);
		this.setState({ preview: true });
	};

	handleClosePreview = () => {
		this.setState({ preview: false });
	};

	componentWillUnmount = () => {
		this.handleClosePreview();
	};

	componentDidUpdate = (prevProps) => {
		if (this.props.killModal !== prevProps.killModal && this.props.killModal) this.handleClosePreview();
	};

	render() {
		const { preview } = this.state;
		const { screenshots } = this.props;

		const carouselItems = screenshots.map((screenshot, k) => (
			<div className='imgHolder' key={k}>
				<img alt='Screenshot' src={screenshot.photo} />
			</div>
		));

		return (
			<div>
				<div className='preview' style={{ display: preview ? 'block' : 'none' }}>
					<Carousel
						ref={(node) => {
							this.carousel = node;
						}}
					>
						{carouselItems}
					</Carousel>
					<Button className='btnEsc' onClick={() => this.handleClosePreview()}>
						<Icon type='close' theme='outlined' />
					</Button>
					<Icon type='left-circle' className='carouselBtn' theme='filled' onClick={() => this.carousel.prev()} style={{ left: '10px' }} />
					<Icon type='right-circle' className='carouselBtn' theme='filled' onClick={() => this.carousel.next()} style={{ right: '10px' }} />
				</div>
				<h4>Screenshots</h4>
				{screenshots.map((screenshot, k) => (
					<img className='miniature' alt='Screenshot' key={k} src={screenshot.photoThumbnail} onClick={() => this.handleOpenPreview(k)} />
				))}
			</div>
		);
	}
}

export default ProvidedScreenshots;
