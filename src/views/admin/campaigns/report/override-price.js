import React, { Component } from 'react';
import { Form, Input, Button, notification, Card, Icon, Divider, Row, Col, Spin } from 'antd';
import { withApollo, Query } from 'react-apollo';
import overrideInfluencerPaymentTotalCost from 'graphql/override-payment-collabs-total-value.graphql';
import getCollabsPaymentCost from 'graphql/get-collabs-payment-cost.graphql';
class CollabsOverridePaymentForm extends Component {
	render() {
		const { form, campaign, handleShowActions, handleEnterToSave } = this.props;
		const { getFieldDecorator } = form;
		const { overriddenCostPaid, totalImpressionCount } = campaign;
		const totalImpressionValues = totalImpressionCount > 0 ? totalImpressionCount * 0.25 : 0;
		const paymentToCollabsTotalValues = overriddenCostPaid !== null ? overriddenCostPaid : totalImpressionValues;
		const countDecimals = (value) => {
			if (Math.floor(value) !== value) {
				const valueToCheck = value.toString().split('.')[1];
				if (valueToCheck) {
					return valueToCheck.length || 0;
				}
			}
			return 0;
		};
		return (
			<Form label='Payment to Collabs(SEK)'>
				<Form.Item label={false} name='newCost'>
					{getFieldDecorator('newCost', {
						initialValue: paymentToCollabsTotalValues,
						rules: [
							{ required: false, message: 'Please enter a new cost' },
							{
								validator: (rule, value, callback) => {
									try {
										if (countDecimals(value) > 2) {
											throw new Error('The maximum of the decimals are 2');
										} else if (value < 0) {
											throw new Error('Value can not be negative');
										}
										callback();
									} catch (error) {
										callback(error);
									}
								}
							}
						]
					})(<Input type='number' onChange={handleShowActions} onPressEnter={handleEnterToSave} />)}
				</Form.Item>
			</Form>
		);
	}
}

const WrappedCollabsOverridePaymentForm = Form.create()(CollabsOverridePaymentForm);

class CollabsOverridePayment extends Component {
	state = {
		showActions: false
	};

	handleShowActions = () => {
		this.setState({ showActions: true });
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	handleOverrideCollabsPayment = () => {
		const { client, campaignId } = this.props;
		const form = this.formRef.props.form;

		form.validateFieldsAndScroll((err, values) => {
			if (err) return null;
			client
				.mutate({
					mutation: overrideInfluencerPaymentTotalCost,
					variables: {
						id: campaignId,
						cost: parseFloat(values.newCost)
					},
					refetchQueries: ['getCollabsPaymentCost', 'getOrganizationCampaign']
				})
				.then((response) => {
					if (response && response.data && response.data.overrideCampaignTotalCostPaid) {
						notification.success({
							message: 'Collabs payment cost updated',
							description: 'The campaign report has been updated'
						});
					}
				})
				.catch(({ graphQLErrors }) => {
					if (graphQLErrors) {
						graphQLErrors.forEach((error) => {
							notification.error({
								message: error.message,
								description: 'Failed to update campaign report'
							});
						});
					}
				});
			this.setState({ showActions: false });
		});
	};
	handelCancel = () => {
		this.setState({ showActions: false });
		location.reload();
	};
	render() {
		const { campaignId } = this.props;
		const { showActions } = this.state;
		return (
			<Query query={getCollabsPaymentCost} variables={{ id: campaignId }} fetchPolicy='cache-and-network'>
				{({ data, loading, refetch }) => {
					if (loading) {
						return <Spin className='collabspin' />;
					}
					return (
						<section className='mb-20 override-price-container'>
							<Card>
								<div className='d-flex align-items-center'>
									<Icon type='edit' />
									<h3 className='color-blue ml-10' style={{ marginBottom: 0 }}>
										Override price
									</h3>
								</div>
								<Divider type='horizontal' />
								<label style={{ flex: 1 }}>{`Payment to Collabs(SEK)`}</label>
								<Row>
									<Col md={{ span: 12 }}>
										<WrappedCollabsOverridePaymentForm
											handleShowActions={this.handleShowActions}
											handleEnterToSave={this.handleOverrideCollabsPayment}
											wrappedComponentRef={this.saveFormRef}
											campaign={data.campaign}
										/>
									</Col>
								</Row>
								{showActions && (
									<Row>
										<Col md={{ span: 12 }}>
											<div className='d-flex' style={{ justifyContent: 'flex-end' }}>
												<Button type='primary' size='small' onClick={this.handleOverrideCollabsPayment}>
													Save
												</Button>
												<Button size='small' className='ml-10' onClick={() => refetch()}>
													Cancel
												</Button>
											</div>
										</Col>
									</Row>
								)}
							</Card>
						</section>
					);
				}}
			</Query>
		);
	}
}

export default withApollo(CollabsOverridePayment);
