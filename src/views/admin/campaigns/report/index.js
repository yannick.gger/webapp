import React, { Component } from 'react';
import { Layout, Divider, Tag, Badge, Radio, Button, Input, Col, Row } from 'antd';
import { Query, Mutation } from 'react-apollo';
import getAllCampaignReportsQuery from 'graphql/get-all-campaign-reports.graphql';
import PaginatedTable from 'components/paginated-table';
import { Link } from 'react-router-dom';
import { adminSendReportToCustomer } from 'services/campaign';
import sendReportToCustomer from 'graphql/send-report-to-customer.graphql';
import ReportingDrawer from './reporting-drawer.js';

class AdminCampaignReport extends Component {
	constructor(props) {
		super(props);
		this.state = {
			status: 'not_done_reports',
			search: undefined
		};
	}

	handleSearch = (value) => {
		this.setState({ search: value });
	};

	render() {
		const { status, search } = this.state;
		const columns = [
			{
				title: 'campaign',
				dataIndex: 'node.name',
				width: '300px'
			},
			{
				title: 'brand',
				dataIndex: 'node.brandName',
				width: '200px'
			},
			{
				title: 'influencers',
				dataIndex: 'node.instagramOwnersJoinedCount'
			},
			{
				title: 'status',
				dataIndex: 'node.status',
				render: (_text, record) =>
					record.node.statuses.map((status) => {
						return (
							<span key={status.statusEnum}>
								<Tag color={status.color} style={{ textTransform: 'uppercase' }}>
									{status.message}
								</Tag>
							</span>
						);
					})
			},
			{
				title: 'action',
				key: 'action',
				width: '300px',
				render: (_text, record) => {
					const { id } = record.node;
					return (
						<div className='campaign-report-actions'>
							<Link to={`/admin/campaigns/${id}/report`}>
								<Button size='small'>View</Button>
							</Link>
							{record.node.instagramOwnersJoinedCount > 0 && (
								<div>
									<Divider type='vertical' />
									<ReportingDrawer campaignId={record.node.id} />
									<Divider type='vertical' />
									<Mutation mutation={sendReportToCustomer}>
										{(sendReportToCustomer) => (
											<Button
												size='small'
												disabled={record.node.reportSent}
												type='primary'
												onClick={adminSendReportToCustomer(sendReportToCustomer, record.node.id)}
											>
												Send
											</Button>
										)}
									</Mutation>
								</div>
							)}
						</div>
					);
				}
			}
		];
		return (
			<Layout>
				<Layout.Header>
					<h1>Admin - Campaign reports</h1>
				</Layout.Header>
				<Layout.Content>
					<Row>
						<Col sm={{ span: 12 }}>
							<Radio.Group defaultValue='not_done_reports' className='radio-pills dark-mode mb-20'>
								<Badge>
									<Radio.Button
										className='button'
										value='not_done_reports'
										onClick={() => {
											this.setState({ status: 'not_done_reports' });
										}}
									>
										Not done
									</Radio.Button>
								</Badge>
								<Badge>
									<Radio.Button
										className='button'
										value='past_deadline_reports'
										onClick={() => {
											this.setState({ status: 'past_deadline_reports' });
										}}
									>
										Past deadline
									</Radio.Button>
								</Badge>
								<Badge>
									<Radio.Button
										className='button'
										value='not_sent_reports'
										onClick={() => {
											this.setState({ status: 'not_sent_reports' });
										}}
									>
										Not sent
									</Radio.Button>
								</Badge>
								<Radio.Button
									className='button'
									value='done_reports'
									onClick={() => {
										this.setState({ status: 'done_reports' });
									}}
								>
									Done
								</Radio.Button>
								<Badge>
									<Radio.Button
										className='button'
										value='all'
										onClick={() => {
											this.setState({ status: undefined });
										}}
									>
										All
									</Radio.Button>
								</Badge>
							</Radio.Group>
						</Col>
						<Col sm={{ span: 12 }}>{<Input.Search placeholder='Search campaign name' onSearch={(value) => this.handleSearch(value)} />}</Col>
					</Row>

					<Query query={getAllCampaignReportsQuery} variables={{ status, search }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
						{(queryRes) => <PaginatedTable columns={columns} queryRes={queryRes} dataRef='campaignReports' />}
					</Query>
				</Layout.Content>
			</Layout>
		);
	}
}
export default AdminCampaignReport;
