import React from 'react';
import { Form, Button, Col, Row, Input, notification } from 'antd';
import createTiktok from 'graphql/create-tiktok.graphql';
import { Mutation } from 'react-apollo';
import UploadPostMedia from './upload-post-media.js';
import RequestScreenshots from './request-screenshots.js';
import './styles.scss';

class TiktokForm extends React.Component {
	createTiktok = (createTiktok) => {
		return (e) => {
			e.preventDefault();
			this.props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				const formValues = {
					...values,
					likesCount: Number(values.likesCount),
					commentsCount: Number(values.commentsCount),
					playsCount: Number(values.playsCount),
					reach: Number(values.reach),
					campaignId: Number(this.props.campaignId),
					assignmentId: Number(values.assignmentId)
				};
				createTiktok({ variables: formValues })
					.then(() => {
						notification.success({
							message: 'Campaign report updated',
							description: 'The campaign report has been updated'
						});
					})
					.catch((err) => {
						notification.error({
							message: 'Campaign report updated',
							description: err.toString()
						});
					});
			});
		};
	};

	handlePhotoUploadDone = () => {
		const fieldsValue = this.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: false }, () => this.props.form.setFieldsValue({ ...fieldsValue }));
	};

	render() {
		const { getFieldDecorator } = this.props.form;
		const { ownerId, assignmentId, assignments } = this.props;

		const assignment = assignments.edges.filter((assignment) => Number(assignment.node.id) === assignmentId);

		let tiktok;
		if (assignment[0].node.tiktoks.length > 0) {
			assignment[0].node.tiktoks.forEach((tiktokPost) => {
				if (tiktokPost.ownerInstagramId === ownerId) tiktok = tiktokPost;
			});
		}

		return (
			<Mutation mutation={createTiktok} refetchQueries={['getOrganizationCampaign']}>
				{(createTiktok, { loading }) => (
					<>
						<div className='report-manage-actions'>
							<RequestScreenshots assignmentId={Number(assignment.id)} instagramOwnerId={ownerId} />
						</div>
						<Form hideRequiredMark onSubmit={this.createTiktok(createTiktok)}>
							<Row gutter={24}>
								<section>
									<div>
										{getFieldDecorator('ownerInstagramId', {
											initialValue: ownerId
										})(<Input type='hidden' />)}

										{getFieldDecorator('assignmentId', {
											initialValue: assignmentId
										})(<Input type='hidden' />)}

										{getFieldDecorator('id', {
											initialValue: tiktok && tiktok.id
										})(<Input type='hidden' />)}

										<Col span={12}>
											<Form.Item label='Likes'>
												{getFieldDecorator('likesCount', {
													initialValue: tiktok ? tiktok.likesCount : '',
													rules: [{ required: true, message: 'Please enter likes' }]
												})(<Input style={{ width: '100%' }} type='number' placeholder='Please enter likes' />)}
											</Form.Item>
										</Col>
										<Col span={12}>
											<Form.Item label='Comments'>
												{getFieldDecorator('commentsCount', {
													initialValue: tiktok ? tiktok.commentsCount : '',
													rules: [{ required: true, message: 'Please enter comments' }]
												})(<Input style={{ width: '100%' }} type='number' placeholder='Please enter comments' />)}
											</Form.Item>
										</Col>
										<Col span={12}>
											<Form.Item label='Reach'>
												{getFieldDecorator('reach', {
													initialValue: tiktok ? tiktok.reach : '',
													rules: [{ required: true, message: 'Please enter reach' }]
												})(<Input style={{ width: '100%' }} type='number' placeholder='Please enter reach' />)}
											</Form.Item>
										</Col>
										<Col span={12}>
											<Form.Item label='Plays'>
												{getFieldDecorator('playsCount', {
													initialValue: tiktok ? tiktok.playsCount : '',
													rules: [{ required: true, message: 'Please enter plays' }]
												})(<Input style={{ width: '100%' }} type='number' placeholder='Please enter plays' />)}
											</Form.Item>
										</Col>
									</div>
									<div>
										<Col span={12}>
											<Form.Item label='Url to tiktok'>
												{getFieldDecorator('shortcode', {
													initialValue: tiktok ? tiktok.shortcode : '',
													rules: [{ required: true, message: 'Please enter url to tiktok post' }]
												})(<Input style={{ width: '100%' }} placeholder='Please enter url to tiktok post' />)}
											</Form.Item>
										</Col>
										<Col span={24}>
											<Form.Item style={{ width: '100%' }} label='Upload image'>
												{getFieldDecorator('mediaUrl')(<UploadPostMedia post={tiktok} form={this.props.form} onPhotoUploadDone={this.handlePhotoUploadDone} />)}
											</Form.Item>
										</Col>
										<Col span={24}>
											<Form.Item label='TikTok text (description)'>
												{getFieldDecorator('caption', {
													initialValue: tiktok ? tiktok.caption : '',
													rules: [
														{
															required: true,
															message: 'Please enter tiktok text'
														}
													]
												})(<Input.TextArea rows={4} placeholder='Please enter tiktok text' />)}
											</Form.Item>
										</Col>
										<Col xs={24} style={{ textAlign: 'right' }}>
											<Button loading={loading} htmlType='submit' type='primary'>
												Save
											</Button>
										</Col>
									</div>
								</section>
							</Row>
						</Form>
					</>
				)}
			</Mutation>
		);
	}
}

const App = Form.create()(TiktokForm);

export default App;
