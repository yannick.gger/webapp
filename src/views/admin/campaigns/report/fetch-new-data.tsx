import React, { useContext, useState, useEffect } from 'react';
import { Button, Tooltip } from 'antd';
import InstagramInsightsService from 'services/FacebookApi/InstagramInsights.service';
import { getIGIErrorMessage } from 'services/FacebookApi/ErrorCodes';
import ToastContext from 'contexts/ToastContext';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { FEATURE_FLAG_FACEBOOK_AUTH } from 'constants/feature-flag-keys';
import { InsightsData } from './types';

interface IFetchNewData {
	postId: number;
	setInsightsData: React.Dispatch<React.SetStateAction<any>>;
	setFetched: React.Dispatch<React.SetStateAction<boolean>>;
	disabled: boolean;
}

const FetchNewData = (props: IFetchNewData) => {
	const { addToast } = useContext(ToastContext);
	const [loading, setLoading] = useState(false);
	const [disabled, setDisabled] = useState(false);
	const [isEnabled] = useFeatureToggle();

	const onClickFetch = () => {
		props.setFetched(false);
		setLoading(true);
		InstagramInsightsService.getPostMetrics(props.postId).then((result) => {
			if (result && result.data) {
				const values = result.data.data.attributes;
				props.setInsightsData({ commentCount: values.commentCount, likeCount: values.likesCount, reach: values.reach, impressions: values.impressions });
			} else {
				if (result.errors && result.errors.length > -1) {
					const err = result.errors[0];
					const message = getIGIErrorMessage(err.code, err.meta && err.meta.subErrorCode ? err.meta.subErrorCode : 0);
					addToast({ id: 'fetch-insight-data-error', mode: 'error', message: message });
				}
			}
			props.setFetched(true);
			setLoading(false);
			setDisabled(true);
		});
	};

	return (
		<>
			{isEnabled(FEATURE_FLAG_FACEBOOK_AUTH) ? (
				<span className='fetch-stats'>
					{!disabled ? (
						<Button type='primary' loading={loading} size='small' onClick={onClickFetch} disabled={disabled}>
							Fetch stats
						</Button>
					) : (
						<Tooltip title='You can only fetch one time per 10 minutes'>
							<Button type='primary' loading={loading} size='small' onClick={onClickFetch} disabled={disabled}>
								Fetch stats
							</Button>
						</Tooltip>
					)}
				</span>
			) : null}{' '}
		</>
	);
};

export default FetchNewData;
