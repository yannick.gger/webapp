import React from 'react';
import { Form, Button, Col, Row, Input, notification } from 'antd';
import createInstagramStory from 'graphql/create-instagram-story.graphql';
import { Mutation } from 'react-apollo';
import RequestScreenshots from './request-screenshots.js';
import './styles.scss';

class InstagramStoryForm extends React.Component {
	createStory = (createInstagramStory) => {
		return (e) => {
			e.preventDefault();
			this.props.form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				const formValues = {
					...values,
					reach: Number(values.reach),
					impressions: Number(values.impressions),
					interactions: Number(values.interactions),
					followers: Number(values.followers),
					campaignId: Number(this.props.campaignId)
				};
				createInstagramStory({ variables: formValues })
					.then(() => {
						notification.success({
							message: 'Campaign report updated',
							description: 'The campaign report has been updated'
						});
					})
					.catch((err) => {
						notification.error({
							message: 'Campaign report updated',
							description: err.toString()
						});
					});
			});
		};
	};

	render() {
		const { getFieldDecorator } = this.props.form;
		const assignment = this.props.assignment;
		const { ownerId, followedByCount } = this.props;
		const post = assignment.instagramStories.filter((post) => post.ownerInstagramId === ownerId)[0];
		return (
			<Mutation mutation={createInstagramStory} refetchQueries={['getOrganizationCampaign']}>
				{(createInstagramStory, { loading }) => (
					<>
						<div className='report-manage-actions'>
							<RequestScreenshots assignmentId={Number(assignment.id)} instagramOwnerId={ownerId} />
						</div>
						<Form hideRequiredMark onSubmit={this.createStory(createInstagramStory)}>
							<Row gutter={24}>
								<section>
									<div>
										{getFieldDecorator('ownerInstagramId', {
											initialValue: ownerId
										})(<Input type='hidden' />)}

										{getFieldDecorator('assignmentId', {
											initialValue: parseInt(assignment.id)
										})(<Input type='hidden' />)}

										{getFieldDecorator('id', {
											initialValue: post ? post.id : null
										})(<Input type='hidden' />)}

										{getFieldDecorator('followers', {
											initialValue: followedByCount ? followedByCount : 0
										})(<Input type='hidden' />)}

										<Col span={12}>
											<Form.Item label='Reach'>
												{getFieldDecorator('reach', {
													initialValue: post ? post.reach : '0',
													rules: [{ required: true, message: 'Please enter reach' }]
												})(<Input style={{ width: '100%' }} type='number' placeholder='Please enter reach' />)}
											</Form.Item>
										</Col>
										<Col span={12}>
											<Form.Item label='Impressions'>
												{getFieldDecorator('impressions', {
													initialValue: post ? post.impressions : '0',
													rules: [{ required: true, message: 'Please enter impressions' }]
												})(<Input style={{ width: '100%' }} type='number' placeholder='Please enter impressions' />)}
											</Form.Item>
										</Col>
										<Col span={24}>
											<Form.Item label='interactions'>
												{getFieldDecorator('interactions', {
													initialValue: post ? post.interactions : '0',
													rules: [{ required: true, message: 'Please enter interactions' }]
												})(<Input style={{ width: '100%' }} type='number' placeholder='Please enter interactions' />)}
											</Form.Item>
										</Col>
									</div>
									<div
										style={{
											width: '100%',
											borderTop: '1px solid #e9e9e9',
											padding: '10px 16px',
											background: '#fff',
											textAlign: 'right'
										}}
									>
										<Button onClick={this.onClose} loading={loading} htmlType='submit' type='primary'>
											Save
										</Button>
									</div>
								</section>
							</Row>
						</Form>
					</>
				)}
			</Mutation>
		);
	}
}

const App = Form.create()(InstagramStoryForm);

export default App;
