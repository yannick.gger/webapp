import React from 'react';
import { Collapse, Drawer, Avatar, Form, Button } from 'antd';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { Spin } from 'antd';
import { adminMarkReportAsDoneManually } from 'services/campaign';
import markReportAsDone from 'graphql/mark-report-as-done.graphql';
import InstagramPostForm from './instagram-post-form';
import InstagramStoryForm from './instagram-story-form';
import TiktokForm from './tiktok-form';
import RequestScreenshots from './request-screenshots.js';
import ProvidedScreenshots from './provided-screenshots.js';
import OverridePaymentCollabsCost from './override-price.js';
import './styles.scss';

import AssignmentScreenshotFragment from 'graphql/fragments/assignmentScreenshotFragment.graphql';
import InstagramPostFragment from 'graphql/fragments/InstagramPostFragment.graphql';
import InstagramStoryFragment from 'graphql/fragments/instagramStoryFragment.graphql';
import TiktokFragment from 'graphql/fragments/tiktokFragment.graphql';
import FetchNewData from './fetch-new-data';
import userFallback from 'assets/img/app/user-fallback.png';
import InstagramInsightsService from 'services/FacebookApi/InstagramInsights.service';

const { Panel } = Collapse;
const customCollapseStyle = {
	borderRadius: '6px',
	border: '0',
	marginBottom: '20px',
	overflow: 'hidden',
	padding: '10px 0 0',
	boxShadow: '0 1px 2px rgba(0, 0, 0, 0.05), 0 8px 16px -8px rgba(0, 0, 0, 0.15)'
};

const getOrganizationCampaign = gql`
	query getOrganizationCampaign($id: ID!) {
		campaign(id: $id) {
			id
			instagramOwnersJoinedCount
			assignmentsCount
			markAsReport
			reportSent

			assignments {
				edges {
					node {
						id
						instagramType
						name

						instagramPosts {
							...InstagramPostFragment
						}

						instagramStories {
							...InstagramStoryFragment
						}
						tiktoks {
							...TiktokFragment
						}
					}
				}
			}

			organization {
				id
				slug
			}

			instagramOwnersJoined: instagramOwners(joined: true) {
				edges {
					node {
						instagramOwnerId
					}
				}
			}

			campaignInstagramOwnerAssignments {
				edges {
					node {
						id
						instagramUrl
						posted
						statsAdded
						assignment {
							id
						}
						campaignInstagramOwnerAssignmentScreenshots {
							...AssignmentScreenshotFragment
						}
						instagramOwnerId
					}
				}
			}
		}
	}
	${AssignmentScreenshotFragment}
	${InstagramPostFragment}
	${InstagramStoryFragment}
	${TiktokFragment}
`;

class DrawerForm extends React.Component {
	state = { visible: false, disabled: false, instagramAccounts: [], loadingProfiles: [] };
	datas = null;

	showDrawer = () => {
		this.setState({
			visible: true
		});
	};

	onClose = () => {
		this.setState({
			visible: false
		});
	};

	onCollapseChange(key, data, instagramOwnerUserId) {
		this.setState({ collapseKey: key, currentId: instagramOwnerUserId });
	}

	onClickManage(campaign) {
		this.showDrawer();
		this.updateInstagramUsers(campaign);
	}

	hideSpinner(id) {
		let filteredArray = this.state.loadingProfiles.filter((id) => id !== id);
		this.setState({ loadingProfiles: filteredArray });
	}

	updateInstagramUsers(campaign) {
		// @todo Pass data into instagram post form to prevent multiple calls
		campaign.instagramOwnersJoinedCount > 0 &&
			campaign.instagramOwnersJoined.edges.map(({ node, index }) => {
				const instagramOwnerId = node.instagramOwnerId;
				campaign.assignmentsCount > 0 &&
					campaign.assignments.edges.map(({ node }) => {
						const assignmentId = Number(node.id);
						const assignment = campaign.assignments.edges.filter((assignment) => Number(assignment.node.id) === assignmentId);

						let post;
						if (assignment[0].node.instagramPosts.length > 0) {
							assignment[0].node.instagramPosts.forEach((igPost) => {
								if (igPost.ownerInstagramId === instagramOwnerId) post = igPost;
							});
						}

						if (post && post.id) {
							if (node.connectedWithFacebook === true) {
								this.setState({ loadingProfiles: [...this.state.loadingProfiles, instagramOwnerId] });

								InstagramInsightsService.getPostMetrics(post.id, post.shortcode).then((result) => {
									if (result && result.data) {
										const included = result.data.included;

										this.setState({
											instagramAccounts: [
												...this.state.instagramAccounts,
												{
													id: instagramOwnerId,
													username: included[0].attributes.username,
													avatar: included[0].attributes.profilePictureUrl
												}
											]
										});
										this.hideSpinner(instagramOwnerId);
									} else {
										if (result.errors && result.errors.length > -1) {
											const err = result.errors[0];
											this.hideSpinner(instagramOwnerId);
										}
									}
								});
							}
						}
					});
			});
	}

	getInstagramProfile(ownerId) {
		return this.state.instagramAccounts.find((p) => p.id === ownerId);
	}

	render() {
		const campaignId = this.props.campaignId;

		return (
			<React.Fragment>
				<Query query={getOrganizationCampaign} variables={{ id: campaignId }}>
					{({ loading, data }) => {
						if (loading && !data.campaign) return <Spin className='collabspin' />;
						const { assignments } = data && data.campaign;
						return (
							<>
								<Button
									size='small'
									onClick={() => {
										this.onClickManage(data.campaign);
									}}
								>
									Manage
								</Button>
								<Drawer
									title='Add campaign report stats'
									width={720}
									onClose={this.onClose}
									visible={this.state.visible}
									bodyStyle={{ backgroundColor: '#f3f6f9', paddingBottom: '80px' }}
								>
									<OverridePaymentCollabsCost campaignId={campaignId} />

									{data.campaign.instagramOwnersJoinedCount > 0 &&
										data.campaign.instagramOwnersJoined.edges.map(({ node, index }) => {
											const instagramOwnerId = node.instagramOwnerId;
											const { currentId, collapseKey } = this.state;
											const instagramUser = this.getInstagramProfile(instagramOwnerId);
											return (
												<React.Fragment key={index}>
													<Collapse
														accordion={true}
														bordered={false}
														activeKey={currentId && currentId === instagramOwnerId ? collapseKey : []}
														onChange={(key) => this.onCollapseChange(key, data, instagramOwnerId)}
														style={customCollapseStyle}
														key={instagramOwnerId}
													>
														<>
															<h3 style={{ width: '100%', padding: '20px', boxShadow: '0 1px rgba(0, 0, 0, 0.1)' }} className='mb-0 color-blue assignment-user'>
																{!this.state.loadingProfiles.find((id) => id === instagramOwnerId) ? (
																	<>
																		<Avatar size={40} style={{ marginRight: '10px' }} src={userFallback} />
																		Data for {instagramUser && `@${instagramUser.username}`}
																	</>
																) : (
																	<Spin className='collabspin' />
																)}
															</h3>
														</>

														{data.campaign.assignmentsCount > 0 &&
															data.campaign.assignments.edges.map(({ node }) => (
																<Panel
																	header={[
																		<span key={`${data.campaign.id}`} style={{ overflow: 'hidden', width: '70%', display: 'inline-block' }}>
																			{`Assignment ${node.name} (${
																				{ video: 'Video', photo: 'Photo', story: 'Story', reel: 'Reel', tiktok: 'TikTok' }[node.instagramType]
																			})`}
																		</span>
																	]}
																	key={node.id}
																>
																	{['photo', 'video', 'reel'].includes(node.instagramType) && (
																		<InstagramPostForm
																			campaignId={campaignId}
																			ownerId={instagramOwnerId}
																			assignmentId={Number(node.id)}
																			assignments={assignments}
																			activeKey={currentId && currentId === instagramOwnerId ? collapseKey : []}
																			updateInstagramUsers={this.updateInstagramUsers}
																			connectedWithFacebook={node.connectedWithFacebook}
																		/>
																	)}
																	{node.instagramType === 'story' && <InstagramStoryForm followedByCount={0} assignment={node} ownerId={instagramOwnerId} />}
																	{node.instagramType === 'tiktok' && (
																		<TiktokForm campaignId={campaignId} ownerId={instagramOwnerId} assignmentId={Number(node.id)} assignments={assignments} />
																	)}

																	{data.campaign.campaignInstagramOwnerAssignments.edges.length > 0 &&
																		data.campaign.campaignInstagramOwnerAssignments.edges.map((handIn) => {
																			if (handIn.node.assignment.id === node.id && handIn.node.instagramOwnerId === instagramOwnerId)
																				return (
																					<ProvidedScreenshots
																						screenshots={handIn.node.campaignInstagramOwnerAssignmentScreenshots}
																						killModal={!this.state.visible || currentId !== instagramOwnerId || collapseKey !== handIn.node.assignment.id}
																					/>
																				);
																		})}
																</Panel>
															))}
														<div className='action-form'>
															<Mutation mutation={markReportAsDone}>
																{(markReportAsDone) => (
																	<Button
																		disabled={data.campaign.markAsReport || data.campaign.reportSent}
																		type='default'
																		style={{ float: 'left', color: '#ea844d', borderColor: '#ea844d' }}
																		onClick={adminMarkReportAsDoneManually(markReportAsDone, data.campaign.id)}
																	>
																		Report done manually
																	</Button>
																)}
															</Mutation>

															<Button onClick={this.onClose} style={{ marginRight: 8 }}>
																Close
															</Button>
														</div>
													</Collapse>
												</React.Fragment>
											);
										})}
								</Drawer>
							</>
						);
					}}
				</Query>
			</React.Fragment>
		);
	}
}

const App = Form.create()(DrawerForm);

export default App;
