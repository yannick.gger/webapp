import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { Table, Layout, Tag, Badge, Radio, Input, Col, Row, Button, Spin } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import campaignsOverview from 'graphql/admin-campaign-overview.graphql';
import { apiToMoment } from 'utils';

import { setGhostToken } from 'services/auth-service';
import signInAsGhostMutation from 'graphql/sign-in-as-ghost.graphql';

class CampaignOverview extends Component {
	constructor(props) {
		super(props);
		this.state = {
			status: '',
			search: ''
		};
	}

	handleSearch = (value) => {
		this.setState({ search: value });
	};

	handleGoToCampaign = (client, organization, campaignId) => {
		const { id, slug } = organization;

		if (this.currentUser && this.currentUser.organizations[0].id === id) this.props.history.push(`/${slug}/campaigns/${campaignId}`);
		else
			client
				.mutate({
					mutation: signInAsGhostMutation,
					variables: { organizationId: id }
				})
				.then(({ data }) => {
					setGhostToken(data.signInAsGhost.token, this.props.location.pathname);
					this.props.history.push(`/${slug}/campaigns/${campaignId}`);
				});
	};

	loadMore = (campaigns, fetchMore, endCursor) => {
		return fetchMore({
			variables: {
				after: endCursor
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				const result = {
					campaignsOverview: {
						...fetchMoreResult.campaignsOverview,
						edges: [...prev.campaignsOverview.edges, ...fetchMoreResult.campaignsOverview.edges]
					},
					currentUser: fetchMoreResult.currentUser
				};
				return result;
			}
		});
	};
	render() {
		const { search, status } = this.state;
		const columns = [
			{
				title: 'campaign',
				dataIndex: 'node.name',
				width: '300px'
			},
			{
				title: 'brand',
				dataIndex: 'node.brandName',
				width: '300px'
			},
			{
				title: 'spots',
				dataIndex: 'node',
				render: (record) => <span>{`${record.instagramOwnersJoinedCount}/${record.influencerTargetCount}`}</span>
			},
			{
				title: 'status',
				dataIndex: 'node.status',
				render: (_text, record) => (
					<span key={record.node.overviewStatus.statusEnum}>
						<Tag color={record.node.overviewStatus.color} style={{ textTransform: 'uppercase' }}>
							{record.node.overviewStatus.message}
						</Tag>
					</span>
				)
			},
			{
				title: 'created',
				dataIndex: 'node.createdAt',
				render: (record) => {
					return apiToMoment(record).format('YYYY-MM-DD HH:mm');
				}
			},
			{
				title: 'Became active',
				key: 'became_active',
				render: (record) => {
					return (
						record.node.overviewStatus.statusEnum === 'ACTIVE' &&
						record.node.activatedAt !== null &&
						apiToMoment(record.node.activatedAt).format('YYYY-MM-DD HH:mm')
					);
				}
			},
			{
				title: 'action',
				key: 'action',
				width: '200px',
				render: (record) => {
					return (
						<ApolloConsumer>
							{(client) => (
								<Button size='small' type='primary' onClick={() => this.handleGoToCampaign(client, record.node.organization, record.node.id)}>
									Go to campaign
								</Button>
							)}
						</ApolloConsumer>
					);
				}
			}
		];
		return (
			<Layout>
				<Layout.Header>
					<h1>Admin - Campaign overview</h1>
				</Layout.Header>
				<Layout.Content>
					<Row>
						<Col sm={{ span: 12 }}>
							<Radio.Group defaultValue='' className='radio-pills dark-mode mb-20'>
								<Badge>
									<Radio.Button
										className='button'
										value=''
										onClick={() => {
											this.setState({ status: '' });
										}}
									>
										All
									</Radio.Button>
								</Badge>
								<Badge>
									<Radio.Button
										className='button'
										value='active'
										onClick={() => {
											this.setState({ status: 'active' });
										}}
									>
										Active
									</Radio.Button>
								</Badge>
								<Badge>
									<Radio.Button
										className='button'
										value='invites_sent_out'
										onClick={() => {
											this.setState({ status: 'invites_sent_out' });
										}}
									>
										Invite sent
									</Radio.Button>
								</Badge>
								<Badge>
									<Radio.Button
										className='button'
										value='draft'
										onClick={() => {
											this.setState({ status: 'draft' });
										}}
									>
										Draft
									</Radio.Button>
								</Badge>
							</Radio.Group>
						</Col>
						<Col sm={{ span: 12 }}>{<Input.Search placeholder='Search campaign name' onSearch={(value) => this.handleSearch(value)} />}</Col>
					</Row>

					<Query query={campaignsOverview} variables={{ status, search }} notifyOnNetworkStatusChange fetchPolicy='cache-and-network'>
						{({ data, loading: queryLoading, fetchMore }) => {
							if (queryLoading) {
								return <Spin className='collabspin' />;
							}

							if (!(data && data.campaignsOverview && data.currentUser)) return '';

							this.currentUser = data.currentUser;
							const campaigns = data && data.campaignsOverview ? data.campaignsOverview.edges : [];
							const hasNextPage = data && data.campaignsOverview && data.campaignsOverview.pageInfo.hasNextPage;
							const endCursor = data && data.campaignsOverview ? data.campaignsOverview.pageInfo.endCursor : null;
							return (
								<div>
									<Table columns={columns} dataSource={campaigns} pagination={false} loading={queryLoading} className='pb-30' />
									{hasNextPage && (
										<div className='pb-30 text-center'>
											<Button type='primary' loading={queryLoading} onClick={() => this.loadMore(campaigns, fetchMore, endCursor)}>
												Load more
											</Button>
										</div>
									)}
								</div>
							);
						}}
					</Query>
				</Layout.Content>
			</Layout>
		);
	}
}

export default withRouter(CampaignOverview);
