import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { Row, Col, Input, Modal, Button, Form, Card, Checkbox, notification } from 'antd';
import { translate } from 'react-i18next';
import updateAdminCampaignReviewMutation from 'graphql/update-admin-campaign-review.graphql';

const FormItem = Form.Item;

class DeclineCampaign extends Component {
	render() {
		const { t, form } = this.props;
		const { getFieldDecorator, getFieldValue } = form;
		const reasons = this.props.reasons.map((key) => [key, t(`decline.reasons.${key}`)]);
		const hasOther = getFieldValue('reasons') && getFieldValue('reasons').includes('other');

		return (
			<React.Fragment>
				<div className='description'>
					<p>Add a reason and a message and it will be sent to the company</p>
				</div>
				<div className='part'>
					<Card className='small-title mb-10 mt-10'>
						<FormItem label='Why do you want to decline the campaign?'>
							{getFieldDecorator('reasons', {
								rules: [
									{
										required: true,
										message: 'Please select a reason'
									}
								]
							})(
								<Checkbox.Group>
									<Row>
										{reasons.map(([key, reason]) => (
											<Col span={24} key={key}>
												<Checkbox value={key}>{reason}</Checkbox>
											</Col>
										))}
									</Row>
								</Checkbox.Group>
							)}
						</FormItem>
						{hasOther && (
							<FormItem>
								<Col span={24} className='other'>
									{getFieldDecorator('other', {
										rules: [
											{
												required: true,
												message: 'Please enter your reason'
											}
										]
									})(<Input.TextArea style={{ minHeight: 60 }} placehodler='Enter your reason' />)}
								</Col>
							</FormItem>
						)}
						<FormItem label='Message to the company'>
							{getFieldDecorator('message', {
								rules: [
									{
										required: true,
										message: 'Please enter a message to the company'
									}
								]
							})(<Input.TextArea />)}
						</FormItem>
					</Card>
				</div>
			</React.Fragment>
		);
	}
}

const WrappedDeclineForm = Form.create()(DeclineCampaign);

class CampaignDeclineModal extends Component {
	state = { visible: true };

	reasons = ['commission', 'productValue', 'chosenInfluencer', 'disturbingImage', 'assignment', 'other'];

	onClose = () => {
		this.setState({ visible: false });
		this.props.closeModal();
	};

	onDecline = (updateAdminCampaignReview) => (e) => {
		e.preventDefault();
		const form = this.declineFormRef.props.form;
		form.validateFieldsAndScroll((err, values) => {
			if (!err) {
				const variables = {
					action: 'declined',
					message: values.message,
					id: parseInt(this.props.match.params.reviewId),
					reasons: values.reasons.reduce((obj, item) => ({ ...obj, [item]: true }), {})
				};

				if (variables.reasons.other) {
					variables.reasons.other = values.other;
				}

				updateAdminCampaignReview({
					variables
				})
					.then(() => {
						this.onClose();
						notification.success({
							message: 'Campaign declined',
							description: 'The campaign has been reviewed and declined'
						});
					})
					.catch((err) => {
						notification.error({
							message: 'Campaign declined',
							description: err.toString()
						});
					});
			}
		});
	};

	render() {
		return (
			<Mutation mutation={updateAdminCampaignReviewMutation}>
				{(updateAdminCampaignReview, { loading }) => {
					const modalOptions = {
						width: 800,
						title: 'Decline the campaign',
						onOk: this.onClose,
						maskClosable: false,
						onCancel: this.onClose,
						visible: this.state.visible,
						wrapClassName: 'custom-modal box-parts title-center decline-review-campagins-admin',
						footer: [
							<Button
								size='large'
								key='submit'
								type='danger'
								className='btn-campaign-review-submit'
								loading={loading}
								onClick={this.onDecline(updateAdminCampaignReview)}
							>
								Decline campaign
							</Button>
						]
					};

					return (
						<Modal {...modalOptions}>
							<WrappedDeclineForm {...this.props} wrappedComponentRef={(ref) => (this.declineFormRef = ref)} reasons={this.reasons} />
						</Modal>
					);
				}}
			</Mutation>
		);
	}
}

export default translate('campaign')(CampaignDeclineModal);
