import React from 'react';
import { withRouter } from 'react-router-dom';
import { Mutation } from 'react-apollo';
import { Col, Card, Button, Tag, Input, message } from 'antd';
import moment from 'moment';

import { acceptAdminCampaignReview, updateAdminCampaignReviewMutation } from 'services/campaign';

class CampaignReviewCard extends React.Component {
	renderItem = (label, value, className = false, newInfluencerExists = false) => (
		<div className='item-content'>
			<div className='item-label'>{label}</div>
			<div className={`item-value ${className}`}>{value}</div>
			{newInfluencerExists && (
				<div className='item-tag'>
					<Tag color='#108ee9'>New Influencers Added</Tag>
				</div>
			)}
		</div>
	);

	render() {
		const { id, campaign, dueAt, status } = this.props.campaignReview;

		if (status !== 'pending') {
			return null;
		}

		const time = moment(dueAt);
		const color = moment.duration(moment(time).diff(moment())).asHours() < 48 ? 'red' : 'blue';
		const newInfluencerExists = campaign.newInfluencerExists;
		const notInvitedInfluencersCount = campaign.notInvitedInfluencers.length;
		let influencer = 'No influencer';

		if (notInvitedInfluencersCount) {
			influencer = `${notInvitedInfluencersCount} influencer${notInvitedInfluencersCount < 2 ? '' : 's'} to invite`;
		}

		const aOption = {
			target: '_blank',
			rel: 'noopener noreferrer',
			href: `/${campaign.organization.slug}/campaigns/${campaign.id}/preview`
		};

		return (
			<Mutation key={id} mutation={updateAdminCampaignReviewMutation}>
				{(updateAdminCampaignReview, { loading }) => {
					return (
						<Col className='campaign-item' key={id} xs={{ span: 24 }} lg={{ span: 12 }} xl={{ span: 8 }}>
							<Card
								hoverable
								cover={
									<a {...aOption}>
										<img alt='Cover' src={(campaign.campaignCoverPhoto || {}).url || '/app/default-cover.jpg'} />
									</a>
								}
							>
								<a {...aOption}>
									<div className='card-content'>
										{this.renderItem('campaign', campaign.name)}
										{this.renderItem('from', campaign.brandName)}
										{this.renderItem('influencers', influencer, undefined, newInfluencerExists)}
										{this.renderItem('needs to be reviewed within', time.fromNow().replace(/^in/, ''), `time color-${color}-date`)}
									</div>
								</a>
								<hr />
								<div className='card-footer'>
									<Button disabled={loading} onClick={() => acceptAdminCampaignReview(updateAdminCampaignReview, campaign.id)}>
										Accept
									</Button>
									<Button disabled={loading} onClick={() => this.props.history.push(`/admin/campaigns/review/${id}/decline`)}>
										Decline
									</Button>
								</div>
							</Card>
						</Col>
					);
				}}
			</Mutation>
		);
	}
}

export default withRouter(CampaignReviewCard);
