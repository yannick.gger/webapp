import React, { Component } from 'react';
import { Query, withApollo } from 'react-apollo';
import { Layout, Row } from 'antd';

import Empty from './empty';
import getAdminCampaignReviewQuery from 'graphql/get-admin-campaign-review.graphql';
import CampaignReviewCard from './campaign-review-card';
import './styles.scss';
class AdminCampaignReview extends Component {
	renderList = (campaignReviews) =>
		campaignReviews.length ? (
			<Row gutter={30}>
				{campaignReviews.map((cR) => (
					<CampaignReviewCard campaignReview={cR} key={cR.id} client={this.props.client} />
				))}
			</Row>
		) : (
			<Empty />
		);

	render() {
		document.title = `Admin Campaign Review | Collabs`;

		return (
			<Layout className='review-campaigns-admin-views'>
				<Layout.Header>
					<h1>Admin - Campaign review</h1>
				</Layout.Header>
				<Layout.Content>
					<Query query={getAdminCampaignReviewQuery}>
						{({ data }) => this.renderList((((data || {}).admin || {}).campaignReviews || []).filter((item) => item.status === 'pending'))}
					</Query>
				</Layout.Content>
			</Layout>
		);
	}
}

export default withApollo(AdminCampaignReview);
