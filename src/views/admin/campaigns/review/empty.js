import React from 'react';
import { Row, Col } from 'antd';
import { ReactSVG } from 'react-svg';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import NoDash from 'assets/img/app/no-content-review.svg';

const Empty = (props) => {
	return (
		<div>
			<Row>
				<Col sm={{ span: 10, offset: 7 }} className='text-center'>
					<div className='empty-icon'>
						<ReactSVG src={NoDash} />
					</div>
					<div className='mb-30'>
						<h2>{i18next.t('campaign:admin.campaigns.campaignReview.empty', { defaultValue: 'Nothing uploaded for review' })}</h2>
					</div>
				</Col>
			</Row>
		</div>
	);
};

export default translate('campaign')(Empty);
