import React from 'react';
import { Modal, Button, message } from 'antd';
import { Mutation } from 'react-apollo';
import { injectIntl } from 'react-intl';

import WrappedImportForm from './form';

import createOrganizationInviteMutation from 'graphql/create-organization-invite.graphql';

class AdminOrganizationInviteView extends React.Component {
	handleSubmit = (createOrganizationInvite) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;
				const graphqlValues = {
					...values
				};

				createOrganizationInvite({
					variables: graphqlValues
				})
					.then(({ data, error }) => {
						if (data.createOrganizationInvite && !error && data.createOrganizationInvite.errors.length === 0) {
							message.info('Du har bjudit in ett nytt företag');
							this.props.closeModal();
						} else {
							data.createOrganizationInvite.errors.forEach((error) => {
								message.error(error.message);
							});
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		return (
			<Mutation mutation={createOrganizationInviteMutation}>
				{(createOrganizationInvite, { loading }) => (
					<Modal
						title='Send an organization invite'
						visible
						onOk={this.handleOk}
						onCancel={this.handleCancel}
						wrapClassName='custom-modal box-parts title-center'
						maskClosable={false}
						footer={[
							<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(createOrganizationInvite)}>
								Send invite
							</Button>
						]}
					>
						<WrappedImportForm wrappedComponentRef={this.saveFormRef} />
					</Modal>
				)}
			</Mutation>
		);
	}
}

export default injectIntl(AdminOrganizationInviteView);
