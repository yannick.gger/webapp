import React from 'react';
import { Layout, Table, Empty } from 'antd';
import TableErrorImg from '../../../assets/img/app/table-error.svg';
import { ColumnProps } from 'antd/lib/table';
import { Agency } from '../../../types/agency';
import { ItemAttributes } from '../../../types/http';
import useRestCollection from '../../../hooks/useRestCollection';

type AgencyItem = ItemAttributes<Agency>;

const COLUMNS: ColumnProps<AgencyItem>[] = [
	{
		title: 'Name',
		dataIndex: 'attributes.name'
	},
	{
		title: 'Description',
		dataIndex: 'attributes.description'
	}
];

const AdminAgencyListView = () => {
	const { data, error, loading } = useRestCollection<Agency>('/agencies');
	const source = data ? data.data : [];

	if (error) console.error('Unable to load data: %O', error);

	return (
		<Layout>
			<Layout.Header>
				<h1>Admin - Agencies</h1>
			</Layout.Header>
			<Layout.Content>
				<Table
					className='pb-30'
					columns={COLUMNS}
					dataSource={source}
					rowKey={(record) => record.id}
					pagination={false}
					loading={loading}
					scroll={{ x: 1000 }}
					locale={{
						emptyText: error ? (
							<Empty image={TableErrorImg} description='Could not load data' />
						) : (
							<Empty description={'Nothing matching the search criterias found'} />
						)
					}}
				/>
			</Layout.Content>
		</Layout>
	);
};

export default AdminAgencyListView;
