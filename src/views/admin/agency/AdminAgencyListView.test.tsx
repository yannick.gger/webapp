import { render, screen } from '@testing-library/react';
import AdminAgencyListView from './AdminAgencyListView';

test('it matches the snapshot', async () => {
	const { asFragment } = render(<AdminAgencyListView />);

	await screen.findByText('Hill-Parker');

	expect(asFragment()).toMatchSnapshot();
});
