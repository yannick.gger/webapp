import React, { Component } from 'react';

import './styles.scss';

import illustration from './404.svg';
import logo from './collabs-logo-color.svg';

export default class GenericNotFoundView extends Component {
	render() {
		document.title = `Not Found | Collabs`;
		return (
			<div className='not-found'>
				<img src={logo} alt='' />
				<div className='blue-box'>
					<h1>404</h1>
					<p>We couldn’t find what you are looking for.</p>
					<a href='/' className='btn'>
						Go back home
					</a>
					<img src={illustration} className='illustration' alt='' />
				</div>
			</div>
		);
	}
}
