import React from 'react';
import { Modal, Button, message, Spin } from 'antd';
import { translate } from 'react-i18next';
import { Mutation, Query } from 'react-apollo';
import DeadlineHasPassed from './deadline-has-passed.js';
import updateContentAssignmentReview from 'graphql/update-content-assignment-review.graphql';
import getInfluencerCampaignQuery from 'graphql/get-influencer-campaign.graphql';
import getAssignmentQuery from 'graphql/get-assignment.graphql';
import '../styles.scss';
import WrappedAssignmentReviewForm from './';
import getTodoPendingStatus from 'graphql/get-todo-pending-status.graphql';

class EditInfluencerAssignmentReview extends React.Component {
	state = {
		fields: {},
		photoUploadStatus: false,
		mediaIdsRemoved: []
	};

	handlePhotoUploading = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: true }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handlePhotoUploadDone = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: false }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	updateMediaIdsRemoved = (ids) => {
		this.setState({
			mediaIdsRemoved: ids
		});
	};

	handleSubmit = (updateContentAssignmentReview, currentUser, post) => {
		return (e) => {
			e.preventDefault();

			if (this.state.photoUploadStatus) return message.info('Photo upload is still in progress. Please wait a moment.');

			const form = this.formRef.props.form;
			const { t } = this.props;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;

				const {
					match: {
						params: { campaignId, assignmentId }
					}
				} = this.props;

				const graphqlValues = {
					...values,
					assignmentReviewId: post.id,
					assignmentId: assignmentId,
					userId: currentUser.id,
					mediaRemoved: this.state.mediaIdsRemoved
				};

				updateContentAssignmentReview({
					variables: graphqlValues,
					refetchQueries: [
						{ query: getAssignmentQuery, variables: { id: assignmentId } },
						{ query: getInfluencerCampaignQuery, variables: { id: campaignId } },
						'getCampaign'
					]
				})
					.then(({ data, error }) => {
						if (data.updateContentAssignmentReview && !error) {
							message.info(t('influencer:assignmentReviewSuccessMessage', { defaultValue: 'Uppgiften är klar! Bra jobbat!' }));
							this.props.onCompleted(campaignId, assignmentId);
						} else {
							message.error(t('influencer:assignmentReviewErrorMessage', { defaultValue: 'Något gick fel' }));
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	static getDerivedStateFromProps = (props, state) => {
		let propsFields = {};

		if (props.post) {
			Object.keys(props.post).forEach((key) => {
				propsFields[key] = {
					value: props.post[key]
				};
				if (key === 'assignmentReviewMedia') {
					const mediaArray = [];
					props.post.assignmentReviewMedia.map((media) => mediaArray.push(media.id));
					propsFields['media'] = {
						value: mediaArray
					};
				}
			});
		}

		return { fields: { ...propsFields, ...state.fields } };
	};

	handleFormChange = (changedFields) => {
		this.setState(({ fields }) => ({
			fields: { ...fields, ...changedFields }
		}));
	};

	render() {
		const { t, post } = this.props;
		const { fields } = this.state;
		return (
			<Query query={getAssignmentQuery} variables={{ id: this.props.match.params.assignmentId }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;
					const isDeadlinePassed = false;

					return (
						<Mutation mutation={updateContentAssignmentReview}>
							{(updateContentAssignmentReview, { loading }) => (
								<Modal
									title={t('influencer:assignmentReviewModalTitle', { defaultValue: 'Ladda upp din post för granskning' })}
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									className='custom-modal-todos box-parts-todos title-center-todos'
									footer={
										isDeadlinePassed
											? []
											: [
													<Button
														size='large'
														key='submit'
														type='primary'
														loading={loading}
														onClick={this.handleSubmit(updateContentAssignmentReview, data.currentUser, post)}
													>
														{t('influencer:editAssignmentReviewSubmit', { defaultValue: `Spara` })}
													</Button>
											  ]
									}
								>
									{isDeadlinePassed ? (
										<DeadlineHasPassed t={t} />
									) : (
										<WrappedAssignmentReviewForm
											wrappedComponentRef={this.saveFormRef}
											assignment={data.assignment}
											t={t}
											user={data.currentUser}
											onPhotoUploading={this.handlePhotoUploading}
											onPhotoUploadDone={this.handlePhotoUploadDone}
											assignmentReviewMedia={post.assignmentReviewMedia}
											fields={fields}
											updateMediaIdsRemoved={this.updateMediaIdsRemoved}
											mediaIdsRemoved={this.state.mediaIdsRemoved}
											onChange={this.handleFormChange}
										/>
									)}
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

class EditInfluencerAssignmentReviewWithAssignment extends React.Component {
	render() {
		return (
			<Query query={getTodoPendingStatus} variables={{ assignmentId: this.props.match.params.assignmentId }} fetchPolicy='cache-and-network'>
				{({ data, loading, error }) => {
					if (loading) return <Spin className='collabspin' />;

					if (!data.todoPendingStatus.assignment || error) {
						message.error('Could not found assignment.');
						return '';
					}

					const { node: assignmentReview } = data.todoPendingStatus.assignment.assignmentReviews.edges.find(({ node }) => node.status === 'to_review') || {};
					return <EditInfluencerAssignmentReview post={assignmentReview} {...this.props} />;
				}}
			</Query>
		);
	}
}

export default translate('influencer')(EditInfluencerAssignmentReviewWithAssignment);
