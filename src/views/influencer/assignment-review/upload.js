import React, { Component } from 'react';
import { Upload, Icon, message, Modal } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import { translate } from 'react-i18next';

import './styles.scss';

const CREATE_ASSIGNMENT_REVIEW_MEDIUM = gql`
	mutation createAssignmentReviewMedium($userId: ID!, $assignmentId: ID!, $photo: Upload, $video: Upload) {
		createAssignmentReviewMedium(input: { userId: $userId, assignmentId: $assignmentId, photo: $photo, video: $video }) {
			assignmentReviewMedium {
				id
				type
				photo
				video
			}
		}
	}
`;

class UploadAssignmentReview extends Component {
	state = {
		fileList: [],
		fileListUntouched: true,
		assignmentReviewMediaIds: [],
		previewVisible: false,
		previewMedia: '/image-sample.png',
		previewType: 'photo',
		mediaIdsRemoved: [],
		mediaIds: this.props.assignmentReviewMediaIds
	};

	handleRemove = (file) => {
		if (this.props.assignmentReviewMediaIds.length === 0) {
			if (file.response && file.response.createAssignmentReviewMedium) {
				const assignmentReviewMediaIds = this.state.assignmentReviewMediaIds.filter(
					(id) => id !== file.response.createAssignmentReviewMedium.assignmentReviewMedium.id
				);
				this.setState(() => ({
					assignmentReviewMediaIds: assignmentReviewMediaIds
				}));

				this.props.updatedMediaIdsHandler(assignmentReviewMediaIds);
			}
		} else if (file && file.uid) {
			let idRemoved = '';
			idRemoved = file.uid;
			if (file.response && file.response.createAssignmentReviewMedium) {
				idRemoved = file.response.createAssignmentReviewMedium.assignmentReviewMedium.id;
			}

			this.state.mediaIdsRemoved.push(idRemoved);
			this.props.handleUpdateMediaIdsRemoved(this.state.mediaIdsRemoved);

			const mediaIds = this.state.mediaIds.filter((id) => id !== idRemoved);
			this.setState(() => ({
				mediaIds: mediaIds
			}));

			this.props.updatedMediaIdsHandler(mediaIds);
		}
	};

	handleCustomRequest = ({ client, assignmentId, userId, callbacks: { onPhotoUploading, onPhotoUploadDone } }) => {
		return ({ file, onError, onSuccess, onProgress }) => {
			let variables = {
				assignmentId: assignmentId,
				userId: userId
			};

			if (file.type.startsWith('video')) {
				variables.video = file;
			} else {
				variables.photo = file;
			}

			onPhotoUploading && onPhotoUploading();
			client
				.mutate({
					mutation: CREATE_ASSIGNMENT_REVIEW_MEDIUM,
					variables
				})
				.then(({ data, loading }) => {
					if (data) {
						this.setState((prevState) => ({
							assignmentReviewMediaIds: [...prevState.assignmentReviewMediaIds, data.createAssignmentReviewMedium.assignmentReviewMedium.id]
						}));

						this.props.updatedMediaIdsHandler(this.state.assignmentReviewMediaIds);

						if (this.props.assignmentReviewMediaIds.length !== 0) {
							this.state.mediaIds.push(data.createAssignmentReviewMedium.assignmentReviewMedium.id);
							this.props.updatedMediaIdsHandler(this.state.mediaIds);
						}

						onSuccess(data, file);
					}

					if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);

					onPhotoUploadDone && onPhotoUploadDone();
				})
				.catch(({ graphQLErrors }) => {
					onPhotoUploadDone && onPhotoUploadDone();
					graphQLErrors &&
						graphQLErrors.forEach((error) => {
							message.error(error.message);
						});
					onError();
				});

			return {
				abort() {}
			};
		};
	};

	static getDerivedStateFromProps(props, state) {
		if (props.assignmentReviewMedia && props.assignmentReviewMedia.length > 0 && state.fileListUntouched) {
			const mediaArray = [];
			props.assignmentReviewMedia.map((media) =>
				mediaArray.push({ uid: media.id, name: media.type, status: 'done', url: media.type === 'video' ? media.video : media.photo })
			);
			return {
				...state,
				fileList: mediaArray,
				fileListUntouched: false
			};
		} else {
			return state;
		}
	}

	handleChange = (info) => {
		const status = info.file.status;
		const { t } = this.props;

		info.fileList.map(
			(item) =>
				item.response &&
				item.response.createAssignmentReviewMedium &&
				item.type.slice(0, item.type.indexOf('/')) === 'video' &&
				(item.url = item.response.createAssignmentReviewMedium.assignmentReviewMedium.video)
		);
		if (!info.file.isExceedSize) {
			this.setState({ fileList: info.fileList });
		}

		if (status === 'done') {
			message.success(
				t('influencer:uploadAssignmentReviewUploadSuccess', { defaultValue: `{{infoFileName}} file uploaded successfully.`, infoFileName: info.file.name })
			);
		} else if (status === 'error') {
			message.error(t('influencer:uploadAssignmentReviewUploadError', { defaultValue: `{{infoFileName}} file upload failed.`, infoFileName: info.file.name }));
		}
	};

	beforeUpload = (file) => {
		const isImage = file.type.includes('image/');
		const isVideo = file.type.includes('video/');
		const sizeInMB = file.size / (1024 * 1024);
		if (isImage && sizeInMB > 10) {
			file.isExceedSize = true;
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');
			return false;
		}
		if (isVideo && sizeInMB > 30) {
			file.isExceedSize = true;
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 30 MB');
			return false;
		}
		return true;
	};

	handlePreview = (file) => {
		this.setState({
			previewMedia: file.url || file.thumbUrl,
			previewType: file.response ? file.type.slice(0, file.type.indexOf('/')) : file.name,
			previewVisible: true
		});
	};

	handleCancel = () => this.setState({ previewVisible: false });

	render() {
		const { fileList, previewVisible, previewMedia, previewType } = this.state;
		const { assignmentId, userId, t, onPhotoUploading, onPhotoUploadDone, assignmentReviewMedia } = this.props;
		const UploadButton = () => (
			<div>
				<p className='ant-upload-drag-icon'>
					<Icon type='inbox' />
				</p>
				<p className='ant-upload-text px-20'>
					{t('influencer:uploadAssignmentReviewUploadText', { defaultValue: `Klicka eller dra en fil hit för att ladda upp` })}
				</p>
			</div>
		);

		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<Upload.Dragger
							listType='picture'
							accept={this.props.accept ? this.props.accept : 'image/*'}
							fileList={fileList}
							className={fileList.length >= 1 && !this.props.multiple ? 'hide-upload-area' : null}
							onPreview={this.handlePreview}
							onChange={this.handleChange}
							customRequest={this.handleCustomRequest({
								client,
								assignmentId,
								userId,
								assignmentReviewMedia,
								callbacks: { onPhotoUploading, onPhotoUploadDone }
							})}
							multiple={this.props.multiple}
							onRemove={this.handleRemove}
							beforeUpload={this.beforeUpload}
						>
							<UploadButton />
						</Upload.Dragger>
						<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel} width={800}>
							{(previewType === 'photo' || previewType === 'image') && <img alt='' src={previewMedia} style={{ width: '100%' }} />}
							{previewType === 'video' && <video src={previewMedia} type='video/mp4' style={{ width: '100%' }} controls />}
						</Modal>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}

export default translate('influencer')(UploadAssignmentReview);
