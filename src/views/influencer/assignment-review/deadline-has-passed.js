import React, { Component } from 'react';
import { translate } from 'react-i18next';

class DeadlineHasPassed extends Component {
	render() {
		const { t } = this.props;
		return (
			<span>
				{t('influencer:assignmentReviewDeadlinePassed', {
					defaultValue: `Sista tidpunkten för att ladda upp posten har redan passerats. I framtiden säkerhetställ att du laddar upp innehåll för godkännande senast 3 dagar före det är dags att
        posta. Detta för att posten ska hinna bli godkänd i tid.`
				})}
			</span>
		);
	}
}

export default translate('influencer')(DeadlineHasPassed);
