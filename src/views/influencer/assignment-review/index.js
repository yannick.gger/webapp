import React from 'react';
import { Row, Col, Input, Form, Card, Tag } from 'antd';
import Upload from './upload.js';
import '../styles.scss';

const FormItem = Form.Item;

class AssignmentReviewForm extends React.Component {
	handleUpdatedMediaIds = (ids) => {
		this.props.form.setFields({
			media: {
				value: ids
			}
		});
	};

	handlePhotoUploading = () => {
		this.props.onPhotoUploading();
	};

	handlePhotoUploadDone = () => {
		this.props.onPhotoUploadDone();
	};

	handleUpdateMediaIdsRemoved = (ids) => {
		this.props.updateMediaIdsRemoved(ids);
	};

	handleAddRequiredText = (req) => {
		const { form } = this.props;
		let caretPosition = this.state && this.state.caretPosition;
		let currentText = form.getFieldValue('text') ? form.getFieldValue('text') : '';

		let newValue;
		if (caretPosition !== null) {
			newValue = currentText.slice(0, caretPosition) + req + ' ' + currentText.slice(caretPosition);
		} else newValue = currentText + req + ' ';

		form.setFieldsValue({ text: newValue });
		form.validateFields(['text'], () => {});

		this.setState({ caretPosition: caretPosition + req.length + 1 });
	};

	render() {
		const { form, assignment, user, t, assignmentReviewMedia } = this.props;
		const { getFieldDecorator } = form;
		const { campaign } = assignment;
		const assignmentReviewMediaIds = [];
		if (this.props.assignmentReviewMedia && this.props.assignmentReviewMedia.length > 0) {
			assignmentReviewMedia.map((media) => assignmentReviewMediaIds.push(media.id));
		}

		return (
			<React.Fragment>
				<div className='description'>
					<p>
						{t('influencer:assignmentReviewDescription', { defaultValue: `Ladda upp din post för "{{assignmentName}}".`, assignmentName: assignment.name })}
					</p>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>{t('influencer:assignmentReviewHeader', { defaultValue: `Ladda upp din post för granskning` })}</h3>
					<p>
						{t('influencer:assignmentReviewCaption', {
							defaultValue: `Innan du postar på {{assignmentType}} vill företaget godkänna posten. Ladda upp din post som du skulle gjort på {{assignmentType}}, så får du en notis när posten blivit godkänd.`,
							assignmentType: assignment.instagramType === 'tiktok' ? 'TikTok' : 'Instagram'
						})}
					</p>
					<br />
					<Card>
						<Tag color='#108ee9' className='fr ant-tag-large'>
							{
								{
									video: t('videoTitle', { defaultValue: `Video` }),
									photo: t('photoTitle', { defaultValue: `Photo` }),
									story: t('storyTitle', { defaultValue: `Story` }),
									reel: t('reelTitle', { defaultValue: `Reel` }),
									tiktok: t('tiktokTitle', { defaultValue: `TikTok` })
								}[assignment.instagramType]
							}
						</Tag>
						<h3>Assignment Instructions</h3>
						<p className='multi-line'>{assignment.description}</p>
					</Card>

					<Card className='small-title mb-10 mt-20'>
						<Row gutter={15}>
							{['photo', 'video', 'reel', 'tiktok'].includes(assignment.instagramType) && (
								<Col xs={{ span: 24 }}>
									<FormItem label={t('influencer:assignmentReviewFormTextLabel', { defaultValue: 'Text till posten' })}>
										{getFieldDecorator('text', {
											rules: [
												{
													required: true,
													whitespace: true,
													message: t('influencer:assignmentReviewFormTextErrorRequired', { defaultValue: 'Skriv in texten du tänkt för din post' })
												},
												{
													validator: (rule, value, callback) => {
														let errors = [];
														let misses = [];
														const requirements = [...campaign.mentions, ...campaign.tags];

														if (requirements.length > 0) {
															requirements.map((req) => {
																if (!value.toLowerCase().includes(req.toLowerCase())) errors.push(new Error(req));
															});
															misses = requirements.map((req) => {
																return errors.map((err) => err.message).filter((err) => err === req)[0];
															});
														}

														this.setState({ errors: misses });
														callback(value ? errors : []);
													},
													message: t('influencer:assignmentReviewFormTextErrorMissingMentionOrTag', {
														defaultValue: 'Se till att alla mentions och taggar finns med'
													})
												}
											]
										})(
											<Input.TextArea
												size='large'
												name='text'
												placeholder={t('influencer:assignmentReviewFormTextPlaceholder', {
													defaultValue: 'Texten du tänkt för din post, glöm inte hashtaggar och mentions.'
												})}
												onClick={(e) => this.setState({ caretPosition: e.target.selectionStart })}
												onKeyUp={(e) => this.setState({ caretPosition: e.target.selectionStart })}
											/>
										)}
									</FormItem>
									<div className='mentionsAndTags'>
										{[...campaign.mentions, ...campaign.tags].length > 0 && (
											<p className='color-dark' style={{ display: 'inline' }}>
												{t('influencer:assignmentReviewFormRequiredMentionsAndTags', { defaultValue: 'Klicka för att lägga till: ' })}
											</p>
										)}
										{[...campaign.mentions, ...campaign.tags].map((item) => {
											const included = this.state && this.state.errors && !this.state.errors.includes(item);
											const req = item.toLowerCase();
											return (
												<Tag key={req} onClick={() => !included && this.handleAddRequiredText(req)} color={included ? 'green' : '#3A7DE3'}>
													{req}
												</Tag>
											);
										})}
									</div>
								</Col>
							)}
							{['photo'].includes(assignment.instagramType) && !assignment.slideshow && (
								<Col xs={{ span: 24 }}>
									<FormItem label={t('imageTitle', { defaultValue: 'Bild' })}>
										{getFieldDecorator('media', {
											rules: [{ required: true, message: t('influencer:assignmentReviewFormMediaErrorRequired', { defaultValue: 'Ladda upp din bild/video' }) }]
										})(
											<Upload
												assignmentReviewMedia={assignmentReviewMedia}
												assignmentReviewMediaIds={assignmentReviewMediaIds}
												updatedMediaIdsHandler={this.handleUpdatedMediaIds}
												assignmentId={assignment.id}
												userId={user.id}
												multiple={true}
												onPhotoUploading={this.handlePhotoUploading}
												onPhotoUploadDone={this.handlePhotoUploadDone}
												accept={'video/*,image/*'}
												handleUpdateMediaIdsRemoved={this.handleUpdateMediaIdsRemoved}
											/>
										)}
									</FormItem>
								</Col>
							)}
							{['photo'].includes(assignment.instagramType) && assignment.slideshow && (
								<Col xs={{ span: 24 }}>
									<FormItem
										label={t('influencer:assignmentReviewFormPhotoSlideshowLabel', {
											defaultValue: `Bilder (Slideshow {{assignmentSlideshowCount}})`,
											assignmentSlideshowCount: assignment.slideshowCount
										})}
									>
										{getFieldDecorator('media', {
											rules: [
												{
													required: true,
													message: t('influencer:assignmentReviewFormPhotoSlideshowMediaErrorRequired', { defaultValue: 'Ladda upp bild' })
												},
												{
													validator: (rule, value, callback) => {
														let errors = [];

														if (value && value.length < assignment.slideshowCount) {
															errors.push(new Error());
														}
														callback(errors);
													},
													message: t('influencer:assignmentReviewFormPhotoSlideshowErrorCount', {
														defaultValue: `Ladda upp minst {{count}} bilder`,
														count: assignment.slideshowCount
													})
												}
											]
										})(
											<Upload
												assignmentReviewMedia={assignmentReviewMedia}
												assignmentReviewMediaIds={assignmentReviewMediaIds}
												updatedMediaIdsHandler={this.handleUpdatedMediaIds}
												assignmentId={assignment.id}
												userId={user.id}
												multiple={true}
												onPhotoUploading={this.handlePhotoUploading}
												onPhotoUploadDone={this.handlePhotoUploadDone}
												accept={'video/*,image/*'}
												handleUpdateMediaIdsRemoved={this.handleUpdateMediaIdsRemoved}
											/>
										)}
									</FormItem>
								</Col>
							)}
							{['video'].includes(assignment.instagramType) && (
								<Col xs={{ span: 24 }}>
									<FormItem label={t('videoTitle', { defaultValue: 'Video' })}>
										{getFieldDecorator('media', {
											rules: [{ required: true, message: t('influencer:assignmentReviewFormVideoLabel', { defaultValue: 'Ladda upp en video' }) }]
										})(
											<Upload
												assignmentReviewMedia={assignmentReviewMedia}
												assignmentReviewMediaIds={assignmentReviewMediaIds}
												updatedMediaIdsHandler={this.handleUpdatedMediaIds}
												assignmentId={assignment.id}
												userId={user.id}
												accept={'video/*'}
												handleUpdateMediaIdsRemoved={this.handleUpdateMediaIdsRemoved}
											/>
										)}
									</FormItem>
								</Col>
							)}
							{['story', 'tiktok'].includes(assignment.instagramType) && (
								<Col xs={{ span: 24 }}>
									<FormItem label={assignment.instagramType}>
										{getFieldDecorator('media', {
											rules: [
												{
													required: true,
													message: t('influencer:assignmentReviewFormPhotoStoryLabel', {
														defaultValue: 'Ladda upp en {{instagramType}}',
														instagramType: assignment.instagramType
													})
												}
											]
										})(
											<Upload
												assignmentReviewMedia={assignmentReviewMedia}
												assignmentReviewMediaIds={assignmentReviewMediaIds}
												updatedMediaIdsHandler={this.handleUpdatedMediaIds}
												assignmentId={assignment.id}
												userId={user.id}
												accept={'video/*,image/*'}
												multiple={true}
												onPhotoUploading={this.handlePhotoUploading}
												onPhotoUploadDone={this.handlePhotoUploadDone}
												handleUpdateMediaIdsRemoved={this.handleUpdateMediaIdsRemoved}
											/>
										)}
									</FormItem>
								</Col>
							)}
							{['reel'].includes(assignment.instagramType) && (
								<Col xs={{ span: 24 }}>
									<FormItem label={t('reelTitle', { defaultValue: 'Reel' })}>
										{getFieldDecorator('media', {
											rules: [{ required: true, message: t('influencer:assignmentReviewFormReelLabel', { defaultValue: 'Ladda upp din reel' }) }]
										})(
											<Upload
												assignmentReviewMedia={assignmentReviewMedia}
												assignmentReviewMediaIds={assignmentReviewMediaIds}
												updatedMediaIdsHandler={this.handleUpdatedMediaIds}
												assignmentId={assignment.id}
												userId={user.id}
												accept={'video/*'}
												handleUpdateMediaIdsRemoved={this.handleUpdateMediaIdsRemoved}
											/>
										)}
									</FormItem>
								</Col>
							)}
						</Row>
					</Card>
					<Card className='small-title mb-10 mt-20'>
						<h3>{t('influencer:assignmentReviewHelpHeader', { defaultValue: `Behöver du hjälp?` })}</h3>
						<a href='https://help.instagram.com/142167909546084' target='_blank' rel='noopener noreferrer'>
							{t('influencer:assignmentReviewHelpCaption', { defaultValue: `Se hur du gör för att spara en story (bild/video)` })}
						</a>
					</Card>
					<Card className='small-title mb-10 mt-20'>
						<h3>{t('influencer:assignmentReviewDontForgetHeader', { defaultValue: `Viktigt! Glöm ej att:` })}</h3>
						<ul>
							<li>
								{t('influencer:assignmentReviewDontForgetTagsAndMentions', {
									defaultValue: `Din text måste innehålla @mentions och #tags för att bli godkänd`
								})}
							</li>
							<li>{t('influencer:assignmentReviewDontForgetText', { defaultValue: `Du måste skriva in texten du tänkt använda vid postandet` })}</li>
							<li>{t('influencer:assignmentReviewDontForgetCollaboration', { defaultValue: `Att nämna att det är ett samarbete` })}</li>
						</ul>
					</Card>
				</div>
			</React.Fragment>
		);
	}
}
const WrappedAssignmentReviewForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const propFields = props.fields || {};
		let formFields = {};
		Object.keys(propFields).forEach((propFieldKey) => {
			formFields[propFieldKey] = Form.createFormField({
				...propFields[propFieldKey],
				value: propFields[propFieldKey].value
			});
		});

		return formFields;
	}
})(AssignmentReviewForm);

export default WrappedAssignmentReviewForm;
