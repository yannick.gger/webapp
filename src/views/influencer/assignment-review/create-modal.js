import React from 'react';
import { Modal, Button, message, Spin } from 'antd';
import { translate } from 'react-i18next';
import { Mutation, Query } from 'react-apollo';
import DeadlineHasPassed from './deadline-has-passed.js';
import createAssignmentReviewMutation from 'graphql/create-assignment-review.graphql';
import getInfluencerCampaignQuery from 'graphql/get-influencer-campaign.graphql';
import getAssignmentQuery from 'graphql/get-assignment.graphql';
import '../styles.scss';
import WrappedAssignmentReviewForm from './';

class InfluencerAssignmentReport extends React.Component {
	state = {
		photoUploadStatus: false
	};

	handlePhotoUploading = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: true }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handlePhotoUploadDone = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: false }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handleSubmit = (createAssignmentReview, currentUser) => {
		return (e) => {
			e.preventDefault();

			if (this.state.photoUploadStatus) return message.info('Photo upload is still in progress. Please wait a moment.');

			const form = this.formRef.props.form;
			const { t } = this.props;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;

				const {
					match: {
						params: { campaignId, assignmentId }
					}
				} = this.props;

				const graphqlValues = {
					...values,
					assignmentId: assignmentId,
					userId: currentUser.id
				};

				createAssignmentReview({
					variables: graphqlValues,
					refetchQueries: [
						{ query: getAssignmentQuery, variables: { id: assignmentId } },
						{ query: getInfluencerCampaignQuery, variables: { id: campaignId } },
						'getCampaign'
					]
				})
					.then(({ data, error }) => {
						if (data.createAssignmentReview && !error) {
							message.info(t('influencer:assignmentReviewSuccessMessage', { defaultValue: 'Uppgiften är klar! Bra jobbat!' }));
							this.props.onCompleted(campaignId, assignmentId);
						} else {
							message.error(t('influencer:assignmentReviewErrorMessage', { defaultValue: 'Något gick fel' }));
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		const { t } = this.props;
		return (
			<Query query={getAssignmentQuery} variables={{ id: this.props.match.params.assignmentId }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;

					// const isDeadlinePassed = apiToMoment(data.assignment.endTime)
					//   .subtract(1, "days")
					//   .isBefore(moment())
					const isDeadlinePassed = false;

					return (
						<Mutation mutation={createAssignmentReviewMutation}>
							{(createAssignmentReview, { loading }) => (
								<Modal
									title={t('influencer:assignmentReviewModalTitle', { defaultValue: 'Ladda upp din post för granskning' })}
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									className='custom-modal-todos box-parts-todos title-center-todos'
									footer={
										isDeadlinePassed
											? []
											: [
													<Button
														size='large'
														key='submit'
														type='primary'
														loading={loading}
														onClick={this.handleSubmit(createAssignmentReview, data.currentUser)}
													>
														{t('influencer:assignmentReviewSubmit', { defaultValue: `Ladda upp` })}
													</Button>
											  ]
									}
								>
									{isDeadlinePassed ? (
										<DeadlineHasPassed t={t} />
									) : (
										<WrappedAssignmentReviewForm
											wrappedComponentRef={this.saveFormRef}
											assignment={data.assignment}
											t={t}
											user={data.currentUser}
											onPhotoUploading={this.handlePhotoUploading}
											onPhotoUploadDone={this.handlePhotoUploadDone}
										/>
									)}
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

export default translate('influencer')(InfluencerAssignmentReport);
