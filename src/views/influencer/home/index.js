import React, { Component } from 'react';

class Home extends Component {
	render() {
		document.title = `Dashboard | Collabs`;

		return (
			<React.Fragment>
				<div className='mb-30'>
					<h2>Your conversations</h2>
					<p>Inbox will be available soon. This is where you will be able to message and negotiate with all the influencers directly.</p>
				</div>
			</React.Fragment>
		);
	}
}

export default Home;
