import React from 'react';
import { message, Spin } from 'antd';
import { Mutation, Query } from 'react-apollo';
import { translate } from 'react-i18next';

import getCampaignInviteQuery from 'graphql/get-campaign-invite.graphql';
import declineCampaignQuery from 'graphql/decline-campaign.graphql';
import CampaignDeclineModal from './campaign-decline-modal';

class CampaignDeclineByToken extends React.Component {
	handleSubmit = (declineCampaign, campaign) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			const { t } = this.props;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;
				const graphqlValues = {
					...values,
					inviteToken: this.props.match.params.inviteToken,
					campaignId: campaign.id
				};

				declineCampaign({
					variables: graphqlValues,
					refetchQueries: [
						{ query: getCampaignInviteQuery, variables: { inviteToken: this.props.match.params.inviteToken } },
						'getCampaignInvite',
						'getCampaign'
					]
				})
					.then(({ data, error }) => {
						if (data.declineCampaign && !error) {
							message.info(t('influencer:campaignDeclineSuccessMessage', { defaultValue: 'Du har tackat nej till kampanjen' }));
							this.props.closeModal();
						} else {
							message.error(t('influencer:campaignDeclineErrorMessage', { defaultValue: 'Något gick fel när resultatet skulle sparas.' }));
							this.props.closeModal();
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		return (
			<Query query={getCampaignInviteQuery} variables={{ inviteToken: this.props.match.params.inviteToken }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;
					return (
						data.campaignInvite && (
							<Mutation mutation={declineCampaignQuery}>
								{(declineCampaign, { loading }) => (
									<CampaignDeclineModal
										loading={loading}
										handleOk={this.handleOk}
										handleCancel={this.handleCancel}
										saveFormRef={this.saveFormRef}
										campaign={data.campaignInvite.campaign}
										handleSubmit={this.handleSubmit(declineCampaign, data.campaignInvite.campaign)}
									/>
								)}
							</Mutation>
						)
					);
				}}
			</Query>
		);
	}
}

export default translate('influencer')(CampaignDeclineByToken);
