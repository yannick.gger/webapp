import React from 'react';
import { Row, Col, Input, Modal, Button, Form, Card, Checkbox, InputNumber } from 'antd';
import { translate } from 'react-i18next';

const FormItem = Form.Item;

class DeclineCampaign extends React.Component {
	render() {
		const { form, campaign, t } = this.props;
		const { getFieldDecorator, getFieldValue } = form;

		return (
			<React.Fragment>
				<div className='description'>
					<p>
						{t('influencer:campaignDeclineDescription', {
							defaultValue: `Du håller på att tacka nej till kampanjen "{{campaignName}}". För att hjälpa oss att hitta bättre kampanjer för dig så svara gärna på följande frågor så förbättrar vi kampanjutbudet.`,
							campaignName: campaign.name
						})}
					</p>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>{t('influencer:campaignDeclineHeader', { defaultValue: `Några snabba frågor` })}</h3>
					<Card className='small-title mb-10 mt-10'>
						<Row gutter={15} className=''>
							<Col xs={{ span: 24 }}>
								<FormItem>
									{getFieldDecorator('notEnoughCompensation', { initialValue: false })(
										<Checkbox size='large'>{t('influencer:campaignDeclinenotEnoughCompensation', { defaultValue: `Ersättningen är för låg` })}</Checkbox>
									)}
								</FormItem>
							</Col>
							{getFieldValue('notEnoughCompensation') && (
								<Col xs={{ span: 24 }}>
									<FormItem
										label={t('influencer:campaignDeclineExpectedCompensation', {
											defaultValue: 'Förväntad kompensation i {{currency}}',
											currency: campaign.currency
										})}
									>
										{getFieldDecorator('expectedCompensation')(<InputNumber size='large' />)}
									</FormItem>
								</Col>
							)}
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<FormItem>
									{getFieldDecorator('notInterestingCompensation', { initialValue: false })(
										<Checkbox size='large'>
											{t('influencer:campaignDeclineNotInterestingCompensation', { defaultValue: `Ersättningen är inte intressant för mig` })}
										</Checkbox>
									)}
								</FormItem>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<FormItem>
									{getFieldDecorator('notForMyAudience', { initialValue: false })(
										<Checkbox size='large'>{t('influencer:campaignDeclineNotForMyAudience', { defaultValue: `Kampanjen passar inte mina följare` })}</Checkbox>
									)}
								</FormItem>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<FormItem>
									{getFieldDecorator('noSponsoredPosts', { initialValue: false })(
										<Checkbox size='large'>{t('influencer:campaignDeclineNoSponsoredPosts', { defaultValue: `Jag gör inte sponsrade inlägg` })}</Checkbox>
									)}
								</FormItem>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<FormItem>
									{getFieldDecorator('badDate', { initialValue: false })(
										<Checkbox size='large'>{t('influencer:campaignDeclineBadDate', { defaultValue: `Datumet för att posta passar inte` })}</Checkbox>
									)}
								</FormItem>
							</Col>
							<Col xs={{ span: 24 }} sm={{ span: 16 }}>
								<FormItem>
									{getFieldDecorator('other', { initialValue: false })(
										<Checkbox size='large'>{t('influencer:campaignDeclineOther', { defaultValue: `Annan anledning` })}</Checkbox>
									)}
								</FormItem>
							</Col>
							{getFieldValue('other') && (
								<Col xs={{ span: 24 }} sm={{ span: 16 }}>
									{getFieldDecorator('otherReason')(
										<Input.TextArea
											size='large'
											placeholder={t('influencer:campaignDeclineOtherPlaceholder', { defaultValue: 'Förklara gärna lite kort om varför' })}
										/>
									)}
								</Col>
							)}
						</Row>
					</Card>
				</div>
			</React.Fragment>
		);
	}
}
const WrappedDeclineForm = Form.create()(DeclineCampaign);

class CampaignDeclineModal extends React.Component {
	render() {
		const { handleOk, handleCancel, handleSubmit, saveFormRef, campaign, loading, t } = this.props;

		return (
			<Modal
				title={t('influencer:campaignDeclineModalTitle', { defaultValue: 'Tacka nej till kampanjen' })}
				visible
				onOk={handleOk}
				onCancel={handleCancel}
				wrapClassName='custom-modal box-parts title-center'
				maskClosable={false}
				footer={[
					<Button size='large' key='submit' type='danger' loading={loading} onClick={handleSubmit}>
						{t('influencer:campaignDeclineSubmitButton', { defaultValue: `Tacka nej till kampanjen` })}
					</Button>
				]}
			>
				<WrappedDeclineForm wrappedComponentRef={saveFormRef} campaign={campaign} t={t} />
			</Modal>
		);
	}
}

export default translate('influencer')(CampaignDeclineModal);
