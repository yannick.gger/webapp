import React from 'react';
import { message, Spin } from 'antd';
import { Mutation, Query } from 'react-apollo';
import { translate } from 'react-i18next';

import getInfluencerCampaignQuery from 'graphql/get-influencer-campaign.graphql';
import declineCampaignQuery from 'graphql/decline-campaign.graphql';
import CampaignDeclineModal from './campaign-decline-modal';

class CampaignDeclineById extends React.Component {
	handleSubmit = (declineCampaign) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			const { t } = this.props;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;

				const graphqlValues = {
					...values,
					campaignId: this.props.match.params.campaignId
				};

				declineCampaign({
					variables: graphqlValues,
					refetchQueries: [{ query: getInfluencerCampaignQuery, variables: { id: this.props.match.params.campaignId } }, 'getCampaignInvite', 'getCampaign']
				})
					.then(({ data, error }) => {
						if (data.declineCampaign && !error) {
							message.info(t('influencer:campaignDeclineSuccessMessage', { defaultValue: 'Du har tackat nej till kampanjen' }));
							this.props.closeModal();
						} else {
							message.error(t('influencer:campaignDeclineErrorMessage', { defaultValue: 'Något gick fel när resultatet skulle sparas.' }));
							this.props.closeModal();
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		return (
			<Query query={getInfluencerCampaignQuery} variables={{ id: this.props.match.params.campaignId }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;
					return (
						<Mutation mutation={declineCampaignQuery}>
							{(declineCampaign, { loading }) => (
								<CampaignDeclineModal
									loading={loading}
									handleOk={this.handleOk}
									handleCancel={this.handleCancel}
									saveFormRef={this.saveFormRef}
									campaign={data.campaign}
									handleSubmit={this.handleSubmit(declineCampaign)}
								/>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

export default translate('influencer')(CampaignDeclineById);
