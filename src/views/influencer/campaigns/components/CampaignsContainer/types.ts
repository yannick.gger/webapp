export const CAMPAIGN_STATUS = {
	INVITED: 'Invited',
	ACCEPTED: 'Accepted',
	REJECTED: 'Rejected',
	WAIT_UPLOAD: 'Waiting_for_upload',
	IN_REVIEW: 'In_review',
	REVIEW_ACCEPTED: 'Review_accepted',
	REVIEW_REJECTED: 'Review_rejected',
	POST_INSTA: 'Post_on_instagram',
	WAIT_UPLOAD_STAT: 'Upload_stats',
	DONE: 'Done/Show_payment_info'
};

export type CampaignStatusType =
	| 'Invited'
	| 'Accepted'
	| 'Rejected'
	| 'Waiting_for_upload'
	| 'In_review'
	| 'Review_accepted'
	| 'Review_rejected'
	| 'Post_on_instagram'
	| 'Upload_stats'
	| 'Done/Show_payment_info';

export type InfluencerCampaign = {
	influencer: {
		id: string;
	};
	joined: boolean;
	declined: boolean;
	status: CampaignStatusType;
	campaign: {
		id: string;
		name: string;
		invitesCloseAt: string;
		spotsLeft: number;
		assignmentsCount: number;
		compensationsTotalValue: number;
		currency: string;
		invoiceCompensationsCount?: number;
		assignments: Array<{
			id: string;
			instagramType?: string;
			name: string;
			startTime: string;
			endTime: string;
			assignmentReview: {
				reviewedAt: string | null;
				waivedDeadline: string | null;
				status: string | null;
			};
			campaignInstagramOwnerAssignment: {
				posted: boolean;
			};
		}>;
	};
	invoiceSent?: boolean;
};
