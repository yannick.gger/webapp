import { useState, useEffect } from 'react';
import { Store } from 'json-api-models';
import LoadingSpinner from 'components/LoadingSpinner';
import Empty from '../EmptyResult/empty';
import InfluencerService from 'services/Influencer';
import CampaignCards from '../CampaignCards';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { KEY_COLLABS_API_USER_OBJECT } from 'constants/localStorage-keys';
import { CAMPAIGN_STATUS, InfluencerCampaign } from './types';

const CampaginsContainer = () => {
	const models = new Store();
	const [loading, setLoading] = useState(true);
	const [activeCampaigns, setActiveCampaigns] = useState<InfluencerCampaign[] | null>(null);
	const [completedCampaigns, setCompletedCampaigns] = useState<InfluencerCampaign[] | null>(null);
	const [invites, setInvites] = useState<InfluencerCampaign[] | null>(null);
	const storage = new BrowserStorage(StorageType.LOCAL);
	const userObject = storage.getItem(KEY_COLLABS_API_USER_OBJECT);

	const fetchCampaignData = () => {
		InfluencerService.getCampaigns()
			.then((res) => {
				if (res) {
					let invitesResult: InfluencerCampaign[] | null = null;
					let activeResult: InfluencerCampaign[] | null = null;
					let completedResult: InfluencerCampaign[] | null = null;
					models.sync(res);
					const campaigns = models.findAll('campaign');

					invitesResult = campaigns
						.filter((campaign) => campaign.campaignInstagramOwners[0].status === CAMPAIGN_STATUS.INVITED.toLocaleLowerCase())
						.map((campaign: any) => {
							return formatInfluencerCampaign(campaign);
						});

					activeResult = campaigns
						.filter(
							(campaign) =>
								campaign.campaignInstagramOwners[0].status !== CAMPAIGN_STATUS.INVITED.toLocaleLowerCase() &&
								campaign.campaignInstagramOwners[0].status !== CAMPAIGN_STATUS.REJECTED.toLocaleLowerCase() &&
								campaign.campaignInstagramOwners[0].status !== CAMPAIGN_STATUS.DONE.toLocaleLowerCase()
						)
						.map((campaign: any) => {
							return formatInfluencerCampaign(campaign);
						});

					completedResult = campaigns
						.filter((campaign) => campaign.campaignInstagramOwners[0].status === CAMPAIGN_STATUS.DONE.toLocaleLowerCase())
						.map((campaign: any) => {
							return formatInfluencerCampaign(campaign);
						});

					setInvites(invitesResult);
					setActiveCampaigns(activeResult);
					setCompletedCampaigns(completedResult);
				}
			})
			.finally(() => {
				setLoading(false);
			});
	};

	useEffect(() => {
		fetchCampaignData();
	}, []);

	const formatInfluencerCampaign = (campaign: any) => {
		return {
			influencer: {
				id: userObject ? JSON.parse(userObject).influencer.id : '',
				username: userObject ? JSON.parse(userObject).name : ''
			},
			joined: campaign.campaignInstagramOwners[0].joined,
			declined: campaign.campaignInstagramOwners[0].declined,
			status: campaign.campaignInstagramOwners[0].status,
			campaign: {
				campaignImages: {
					coverImage: campaign.links.horizontalCoverPhoto ? campaign.links.horizontalCoverPhoto : ''
				},
				products: campaign.campaignInstagramOwners[0].campaignInstagramOwnerProducts
					? campaign.campaignInstagramOwners[0].campaignInstagramOwnerProducts.map((product) => {
							return {
								id: product.id,
								quantity: 1,
								product: {
									productImage: null,
									name: product.product.name,
									link: product.product.link ? product.product.link : null,
									description: product.product.description,
									value: +product.product.value.amount / 100
								}
							};
					  })
					: [],
				invoices: campaign.campaignInstagramOwners[0].campaignInstagramOwnerCommissions
					? campaign.campaignInstagramOwners[0].campaignInstagramOwnerCommissions.map((commission) => {
							return {
								id: commission.id,
								invoice: {
									value: commission.commission.fixedAmount / 100
								}
							};
					  })
					: [],
				id: campaign.id,
				name: campaign.name,
				invitesCloseAt: campaign.inviteClosedAt,
				spotsLeft: campaign.full ? 0 : 1,
				compensationsTotalValue: [
					...(campaign.campaignInstagramOwners[0].campaignInstagramOwnerProducts
						? campaign.campaignInstagramOwners[0].campaignInstagramOwnerProducts.map((product) => +product.product.value.amount / 100)
						: []),
					...(campaign.campaignInstagramOwners[0].campaignInstagramOwnerCommissions
						? campaign.campaignInstagramOwners[0].campaignInstagramOwnerCommissions.map((commission) => commission.commission.fixedAmount / 100)
						: [])
				].reduce((prev, current) => prev + current, 0),
				currency: campaign.campaignInstagramOwners[0].campaignInstagramOwnerCommissions[0].commission.fixedAmountCurrency,
				assignmentsCount: campaign.assignments.length,
				assignments: campaign.assignments.map((assignment: any) => {
					return {
						id: assignment.id,
						type: assignment.type,
						name: assignment.name,
						dos: assignment.dos,
						donts: assignment.donts,
						cta: assignment.cta,
						description: assignment.description,
						startTime: assignment.groups[0].start,
						endTime: assignment.groups[0].end,
						deadlines: assignment.groups[0].deadlines.map((deadline) => {
							return {
								id: deadline.id,
								name: deadline.name,
								date: deadline.date
							};
						}),
						assignmentReview: {
							createdAt: assignment.reviews.length ? assignment.reviews[assignment.reviews.length - 1].createdAt : null,
							text: assignment.reviews.length ? assignment.reviews[assignment.reviews.length - 1].text : null,
							reviewedAt: assignment.reviews.length ? assignment.reviews[assignment.reviews.length - 1].reviewedAt : null,
							waivedDeadline: assignment.reviews.length ? assignment.reviews[assignment.reviews.length - 1].waivedDeadline : null,
							status: assignment.reviews.length ? assignment.reviews[assignment.reviews.length - 1].status : null,
							media:
								assignment.reviews.length && assignment.reviews[assignment.reviews.length - 1].media.length > 0
									? assignment.reviews[assignment.reviews.length - 1].media.map((mediaItem: any) => mediaItem.links.screenshot)
									: null
						},
						campaignInstagramOwnerAssignment: {
							posted: campaign.campaignInstagramOwners[0].campaignInstagramOwnerAssignments[0].posted
						}
					};
				})
			}
		};
	};

	return (
		<div style={{ padding: '0 1rem' }}>
			{loading ? (
				<LoadingSpinner />
			) : !activeCampaigns && !completedCampaigns && !invites ? (
				<Empty />
			) : (
				<div>
					{invites && invites.length > 0 ? <CampaignCards campaigns={invites} title='Invited to join' onRefreshCampaigns={fetchCampaignData} /> : null}
					{activeCampaigns && activeCampaigns.length > 0 ? (
						<CampaignCards campaigns={activeCampaigns} title='Active campaigns' onRefreshCampaigns={fetchCampaignData} />
					) : null}
					{completedCampaigns && completedCampaigns.length > 0 ? (
						<CampaignCards campaigns={completedCampaigns} title='Completed campaigns' onRefreshCampaigns={fetchCampaignData} />
					) : null}
				</div>
			)}
		</div>
	);
};

export default CampaginsContainer;
