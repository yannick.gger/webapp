import { Card, Col, Row, Tag, Button } from 'antd';
import moment from 'moment';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FormattedNumber, FormattedPlural } from 'react-intl';
import { apiToMoment } from 'utils';
import { getTodos, remainingTasks } from '../task-utils.ts';
import TodoCarousel from 'components/todo-carousel';
import { translate } from 'react-i18next';

class CampaignCard extends Component {
	statusTag = () => {
		const campaignInstagramOwner = this.props.campaignInstagramOwner;
		const { campaign } = campaignInstagramOwner;
		const { t } = this.props;

		let color, text;
		if (campaignInstagramOwner.joined) {
			if (remainingTasks(campaignInstagramOwner).length === 0) {
				color = 'green';
				text = t('campaign:completed', { defaultValue: 'Completed' });
			} else if (getTodos(campaignInstagramOwner).filter(({ node: task }) => task.failed).length > 0) {
				color = 'red';
				text = t('campaign:rejected', { defaultValue: 'Rejected' });
			} else {
				color = '#108ee9';
				text = t('campaign:joined', { defaultValue: 'Joined' });
			}
		} else if (campaignInstagramOwner.declined) {
			color = 'red';
			text = t('campaign:declined', { defaultValue: 'Declined' });
		} else if (campaign.spotsLeft <= 0 || apiToMoment(campaign.invitesCloseAt) < moment()) {
			color = '#AAA';
			text = t('campaign:closed', { defaultValue: 'Closed' });
		} else if (!campaignInstagramOwner.joined) {
			color = '#ED42C0';
			text = t('campaign:invited', { defaultValue: 'Invited' });
		}

		if (color && text) {
			return (
				<Tag className='tag-joined' color={color}>
					{text}
				</Tag>
			);
		}
	};

	render() {
		const { t, todos, campaignInstagramOwner } = this.props;
		const { campaign } = campaignInstagramOwner;

		return (
			<Card
				className='campaign-card campaign-card-with-todo'
				cover={<img alt='Cover' src={campaign.campaignCoverPhoto ? campaign.campaignCoverPhoto.thumbnail : `/app/default-cover.jpg`} />}
			>
				<div className='on-cover'>
					<Card.Meta className='pt-10' title={campaign.name} />
					{campaign.campaignLogo && <Card.Meta className='pt-20' avatar={<img src={campaign.campaignLogo.url} alt='brand logotype' />} />}
					{this.statusTag()}
					<div className='payment-text'>
						{t('campaign:productValueOf', { defaultValue: 'Product value of' })}{' '}
						<strong>
							<FormattedNumber value={campaign.compensationsTotalValue} /> {campaign.currency}
						</strong>{' '}
						{t('campaign:for', { defaultValue: 'for' })}{' '}
						<strong>
							<FormattedNumber value={campaign.assignmentsCount} />{' '}
							<FormattedPlural
								value={campaign.assignmentsCount}
								one={t('campaign:post', { defaultValue: 'post' })}
								other={t('campaign:posts', { defaultValue: 'posts' })}
							/>
						</strong>
					</div>

					<Link to={{ pathname: `/influencer/campaigns/${campaign.id}`, state: campaignInstagramOwner }}>
						<Button type='primary' className='breif-btn' size='default'>
							{t('campaign:viewCampaign', { defaultValue: 'View campaign' })}
						</Button>
					</Link>
				</div>

				<Row>
					<Col xs={{ span: 24 }}>
						<TodoCarousel todos={todos} campaign={campaignInstagramOwner} />
					</Col>
				</Row>
			</Card>
		);
	}
}

export default translate('campaign')(CampaignCard);
