import moment, { Moment } from 'moment';
import { apiToMoment } from 'utils';
import { InfluencerCampaign } from './CampaignsContainer/types';

type TodoType = 'TodoAcceptInvite' | 'TodoUploadPost' | 'TodoPostInstagram' | 'TodoUploadStat' | 'TodoSendInvoice';
type TodoStatus = 'success' | 'pending' | 'fail' | 'in_progress' | 'future';

enum TODO_STATUS {
	SUCCESS = 'success',
	PENDING = 'pending',
	FAIL = 'fail',
	IN_PROGRESS = 'in_progress',
	FUTURE = 'future'
}

enum TODO_TYPE {
	INVITED = 'TodoAcceptInvite',
	UPLOAD_POST = 'TodoUploadPost',
	POST_SNS = 'TodoPostInstagram',
	UPLOAD_STATS = 'TodoUploadStat',
	SEND_INVOICE = 'TodoSendInvoice'
}

export const getTodos = (param: InfluencerCampaign) => {
	const todos: Array<{
		ready: boolean;
		campaignId: string;
		type: TodoType;
		failed?: boolean;
		completed: boolean;
		pending?: boolean;
		link: string;
		deadline: Moment;
		status?: TodoStatus;
		campaign: any;
	}> = [];

	console.log(param);

	// Invite Todo
	todos.push({
		ready: true,
		campaignId: param.campaign.id,
		type: TODO_TYPE.INVITED,
		failed: param.declined || ((!param.joined && param.campaign.spotsLeft === 0) || apiToMoment(param.campaign.invitesCloseAt) < moment()),
		completed: param.joined,
		pending: false,
		link: `/influencer/campaigns/${param.campaign.id}`,
		deadline: apiToMoment(param.campaign.invitesCloseAt),
		campaign: param.campaign
	});

	param.campaign.assignments.forEach((assignment) => {
		if (assignment.assignmentReview) {
			// Assignments Todos
			todos.push({
				ready: param.joined,
				campaignId: param.campaign.id,
				type: TODO_TYPE.UPLOAD_POST,
				failed:
					assignment.assignmentReview.status === 'rejected' &&
					!assignment.assignmentReview.waivedDeadline &&
					apiToMoment(assignment.assignmentReview.reviewedAt) > apiToMoment(assignment.endTime).subtract(1, 'days'),
				completed: assignment.assignmentReview.status === 'approved',
				pending: assignment.assignmentReview.status === 'to_review',
				link: `/influencer/campaigns/${param.campaign.id}/assignments/${assignment.id}/review/modal`,
				deadline: apiToMoment(assignment.startTime).subtract(3, 'days'),
				campaign: param.campaign
			});

			// Post to sns
			todos.push({
				ready:
					apiToMoment(assignment.startTime).subtract(1, 'days') < moment() &&
					(!assignment.hasContentReview || (assignment.hasContentReview && todos[todos.length - 1].completed)),
				campaignId: param.campaign.id,
				type: TODO_TYPE.POST_SNS,
				completed: assignment.todo ? assignment.todo.type === TODO_TYPE.POST_SNS && assignment.todo.status === TODO_STATUS.SUCCESS : false,
				deadline: apiToMoment(assignment.startTime),
				link: `/influencer/campaigns/${param.campaign.id}/assignments/${assignment.id}/info/modal`,
				campaign: param.campaign
			});
		}

		if (param.campaignInstagramOwnerAssignments) {
			// Upload stats
			todos.push({
				ready: todos[todos.length - 1].completed,
				campaignId: param.campaign.id,
				type: TODO_TYPE.UPLOAD_STATS,
				completed: param.campaignInstagramOwnerAssignments.some(
					(instagramOwnerAssignment) =>
						instagramOwnerAssignment.posted && instagramOwnerAssignment.assignment && instagramOwnerAssignment.assignment.id === assignment.id
				),
				deadline: apiToMoment(assignment.endTime).add(2, 'days'),
				link: `/influencer/campaigns/${param.campaign.id}/assignments/${assignment.id}/report/modal`,
				campaign: param.campaign
			});
		}
	});

	if (param.campaign.invoiceCompensationsCount && param.campaign.invoiceCompensationsCount > 0) {
		// Send invoice
		todos.push({
			ready: todos.filter((todo) => !todo.completed).length === 0,
			campaignId: param.campaign.id,
			type: TODO_TYPE.SEND_INVOICE,
			completed: param.invoiceSent ? param.invoiceSent : false,
			deadline: moment(),
			link: `/influencer/campaigns/${param.campaign.id}/campaign-invoice-info/modal`,
			campaign: param.campaign
		});
	}

	const failed = todos.filter((todo) => todo.failed).length > 0;

	todos.forEach((todo) => {
		if (todo.completed) {
			todo.status = TODO_STATUS.SUCCESS;
		} else if (todo.pending) {
			todo.status = TODO_STATUS.PENDING;
		} else if (todo.failed) {
			todo.status = TODO_STATUS.FAIL;
		} else if (todo.ready && !failed) {
			todo.status = TODO_STATUS.IN_PROGRESS;
		} else {
			todo.status = TODO_STATUS.FUTURE;
		}
	});

	const todoEnum = ['TodoAcceptInvite', 'TodoUploadPost', 'TodoPostInstagram', 'TodoUploadStat', 'TodoSendInvoice'];
	const statusEnum = ['success', 'pending', 'fail', 'in_progress', 'future'];

	todos.sort((a, b) => {
		if (['TodoAcceptInvite', 'TodoSendInvoice'].includes(a.type) || ['TodoAcceptInvite', 'TodoSendInvoice'].includes(b.type)) {
			return todoEnum.indexOf(a.type) - todoEnum.indexOf(b.type);
		} else if (a.status === b.status) {
			return a.deadline - b.deadline;
		} else {
			return statusEnum.indexOf(a.status!) - statusEnum.indexOf(b.status!);
		}
	});

	return todos;
};

export function remainingTasks(param: InfluencerCampaign) {
	console.log(getTodos(param));
	return getTodos(param).filter((todo) => !todo.completed);
}
