import React, { Component } from 'react';
import { Row, Col } from 'antd';
import { ReactSVG } from 'react-svg';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import EmptySvg from '../EmptyResult/empty.svg';

class Error extends Component {
	render() {
		return (
			<div>
				<Row>
					<Col sm={{ span: 10, offset: 7 }} className='text-center'>
						<div className='empty-icon'>
							<ReactSVG src={EmptySvg} />
						</div>
						<div className='mb-30'>
							<h2>{i18next.t('influencer:campaigns.campaignList.error.title', { defaultValue: 'Something went wrong' })}</h2>
							<p>
								{i18next.t('influencer:campaigns.campaignList.error.subTitle', { defaultValue: 'Sorry, there was an error while getting your campaign list.' })}
							</p>
						</div>
					</Col>
				</Row>
			</div>
		);
	}
}

export default translate('influencer')(Error);
