import { Card } from 'antd';
import React, { Component } from 'react';
import JoinOrReject from '../../../tasks/join-or-reject.svg';
import { Link } from 'react-router-dom';

export default class TaskCard extends Component {
	render() {
		const task = this.props.task;
		const { campaign } = this.props.campaignInstagramOwner;

		const InnerTaskCard = () => (
			<Card
				hoverable
				data-ripple='rgba(132,146, 164, 0.2)'
				className='action-card influencer-task has-icon-left mb-20'
				cover={<img src={JoinOrReject} alt='' />}
			>
				<Card.Meta title={campaign.name} description={<span className='color-green'>TO DO</span>} />
				<div slyle={{ float: 'right' }} />
				<p className='mt-5 mb-4'>{task.text}</p>
			</Card>
		);

		return (
			<React.Fragment>
				{task.link ? (
					<Link to={task.link}>
						<InnerTaskCard />
					</Link>
				) : (
					<InnerTaskCard />
				)}
			</React.Fragment>
		);
	}
}
