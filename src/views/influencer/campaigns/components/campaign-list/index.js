import React, { Component } from 'react';

import { Row, Col, Spin, Tabs } from 'antd';
import { withRouter } from 'react-router-dom';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';
import { apiToMoment } from 'utils';

import CampaignCard from '../campaign-card/';
import Empty from '../EmptyResult/empty';
import { remainingTasks, tasks } from '../task-utils';
import Error from './error';

const TabPane = Tabs.TabPane;

const GET_CAMPAIGNS = gql`
	query getInfluencerCampaigns {
		currentUser {
			id
			name
			instagramOwnerId
			instagramPosts {
				id
				timestamp
				assignmentId
			}
			instagramStories {
				id
				timestamp
				assignmentId
			}
			tiktoks {
				id
				timestamp
				assignmentId
			}
			campaignInstagramOwners {
				edges {
					node {
						id
						joined
						declined
						invoiceSent
						campaignInstagramOwnerAssignments(first: 20) @connection(key: "campaignInstagramOwnerAssignments", filter: []) {
							edges {
								node {
									id
									posted
									assignment {
										id
									}
								}
							}
						}
						assignmentReviews(first: 20) @connection(key: "assignmentReviews", filter: []) {
							edges {
								node {
									id
									status
									reviewedAt
									waivedDeadline
									assignment {
										id
										name
									}
								}
							}
						}
						campaign {
							id
							name
							assignmentsCount
							spotsLeft
							invitesCloseAt
							compensationsTotalValue
							invoiceCompensationsCount
							assignments(first: 20) @connection(key: "assignments", filter: []) {
								edges {
									node {
										id
										name
										startTime
										hasContentReview
										endTime
										instagramType
										todos {
											id
											type
											status
										}
									}
								}
							}
							campaignCoverPhoto {
								id
								url
								thumbnail
								thumbnailSquare
							}
							campaignLogo {
								id
								url
								thumbnailSquare
							}
							campaignBrandAssets {
								id
								url
								thumbnailSquare
							}
							campaignInspirations {
								id
								url
								thumbnailSquare
							}
						}
					}
				}
			}
		}
	}
`;

class CampaignList extends Component {
	componentDidUpdate(prevProps, prevState) {
		const {
			history,
			location: { pathname, search }
		} = this.props;
		const {
			location: { pathname: prevPathname }
		} = prevProps;

		if (
			pathname !== prevPathname &&
			(search.includes('assignmentReviewCompleted=true') ||
				search.includes('instagramPostJobCompleted=true') ||
				search.includes('assignmentReportCompleted=true'))
		) {
			this.refetch &&
				this.refetch().then(() => {
					history.replace('/influencer/campaigns');
				});
		}
	}

	render() {
		return (
			<Query query={GET_CAMPAIGNS} variables={{ organizationSlug: this.props.match.params.organizationSlug }} notifyOnNetworkStatusChange={true}>
				{({ loading, error, data, refetch }) => {
					this.refetch = refetch;

					if (error) return <p>Could not load campaigns</p>;
					if (loading) return <Spin className='collabspin' />;
					if (!data.currentUser) return <Error />;
					if ((((data.currentUser || {}).campaignInstagramOwners || {}).edges || []).length === 0) return <Empty />;

					const { instagramPosts, instagramStories, tiktoks } = data.currentUser;
					let instagramContent = [
						...instagramPosts.map((post) => ({ [post.assignmentId]: { ...post } })),
						...instagramStories.map((story) => ({ [story.assignmentId]: { ...story } })),
						...tiktoks.map((tiktok) => ({ [tiktok.assignmentId]: { ...tiktok } }))
					];
					instagramContent = Object.assign({}, ...instagramContent);

					const edges = data.currentUser.campaignInstagramOwners.edges.map(({ node }) => {
						if (node.joined) {
							node.campaign.assignments.edges.forEach(({ node: assignment }) => {
								assignment.instagramContent = instagramContent[assignment.id];
							});
						}
						return { node };
					});

					const invitedCampaigns = edges.filter(({ node }) => {
						return !node.joined && !node.declined && node.campaign.spotsLeft > 0 && apiToMoment(node.campaign.invitesCloseAt) > moment();
					});

					const closedInvitedCampaigns = edges.filter(({ node }) => {
						return !node.joined && (node.declined || node.campaign.spotsLeft <= 0 || apiToMoment(node.campaign.invitesCloseAt) < moment());
					});

					const completedCampaigns = edges.filter(({ node }) => {
						return node.joined && (remainingTasks(node).length === 0 || tasks(node).filter(({ node: todo }) => todo.failed).length > 0);
					});

					const activeCampaigns = edges.filter(({ node }) => {
						return node.joined && remainingTasks(node).length > 0 && tasks(node).filter(({ node: todo }) => todo.failed).length === 0;
					});

					return (
						<div>
							{invitedCampaigns.length > 0 && (
								<React.Fragment>
									<div className='mb-10'>
										<h2>Invited to join</h2>
									</div>
									<Row gutter={30} className='item-rows gutter-30'>
										{invitedCampaigns.map(({ node }) => (
											<Col key={node.id} className='mb-30' span={24}>
												<CampaignCard campaignInstagramOwner={node} />
											</Col>
										))}
									</Row>
								</React.Fragment>
							)}

							<Tabs defaultActiveKey='1'>
								<TabPane tab='Active campaigns' key='1'>
									{activeCampaigns.length > 0 ? (
										<React.Fragment>
											<Row gutter={30} className='item-rows gutter-30'>
												{activeCampaigns.map(({ node }) => (
													<Col key={node.id} className='mb-30' span={24}>
														<CampaignCard campaignInstagramOwner={node} />
													</Col>
												))}
											</Row>
										</React.Fragment>
									) : (
										<span>You have no active campaigns</span>
									)}
								</TabPane>
								<TabPane tab='Completed campaigns' key='2'>
									{completedCampaigns.length > 0 ? (
										<React.Fragment>
											<Row gutter={30} className='item-rows gutter-30'>
												{completedCampaigns.map(({ node }) => (
													<Col key={node.id} className='mb-30' span={24}>
														<CampaignCard campaignInstagramOwner={node} />
													</Col>
												))}
											</Row>
										</React.Fragment>
									) : (
										<span>You have not completed any campaigns yet</span>
									)}
								</TabPane>
								<TabPane tab='Previous invites' key='3'>
									{closedInvitedCampaigns.length > 0 ? (
										<React.Fragment>
											<Row gutter={30} className='item-rows gutter-30'>
												<React.Fragment>
													{closedInvitedCampaigns.map(({ node }) => (
														<Col key={node.id} className='mb-30' span={24}>
															<CampaignCard campaignInstagramOwner={node} />
														</Col>
													))}
												</React.Fragment>
											</Row>
										</React.Fragment>
									) : (
										<span>You have no expired or declined invites</span>
									)}
								</TabPane>
							</Tabs>
						</div>
					);
				}}
			</Query>
		);
	}
}

export default withRouter(CampaignList);
