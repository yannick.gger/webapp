import React, { Component } from 'react';
import { Row, Col } from 'antd';
import { ReactSVG } from 'react-svg';
import { translate } from 'react-i18next';
import i18next from 'i18next';

import EmptySvg from './empty.svg';

class Empty extends Component {
	render() {
		return (
			<div>
				<Row>
					<Col sm={{ span: 10, offset: 7 }} className='text-center'>
						<div className='empty-icon'>
							<ReactSVG src={EmptySvg} />
						</div>
						<div className='mb-30'>
							<h2>{i18next.t('influencer:campaigns.campaignList.empty.title', { defaultValue: 'No campaigns' })}</h2>
							<p>
								{i18next.t('influencer:campaigns.campaignList.empty.subTitle', {
									defaultValue: 'When you have been invited to or joined a campaign it will appear here.'
								})}
							</p>
						</div>
					</Col>
				</Row>
			</div>
		);
	}
}

export default translate('influencer')(Empty);
