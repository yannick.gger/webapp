import { Card, Col, Row, Button } from 'antd';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next/hooks';
import TodoItem from './TodoItem';

const CampaignCard = (props: { campaign: any; extra: any; status: any; onRefreshCampaigns: () => void }) => {
	const [t] = useTranslation();

	return (
		<Card
			className='campaign-card campaign-card-with-todo'
			cover={<img alt='Cover' src={props.campaign.campaignCoverPhoto ? props.campaign.campaignCoverPhoto : `/app/default-cover.jpg`} />}
		>
			<div className='on-cover'>
				<Card.Meta className='pt-10' title={props.campaign.name} />
				{props.campaign.campaignLogo && <Card.Meta className='pt-20' avatar={<img src={props.campaign.campaignLogo.url} alt='brand logotype' />} />}
				<div className='payment-text'>
					{t('campaign:productValueOf', { defaultValue: 'Product value of' })}{' '}
					<strong>{`${props.campaign.compensationsTotalValue} ${props.campaign.currency}`}</strong> {t('campaign:for', { defaultValue: 'for' })}{' '}
					<strong>
						{`${props.campaign.assignmentsCount} ${
							props.campaign.assignmentsCount > 1 ? t('campaign:posts', { defaultValue: 'posts' }) : t('campaign:post', { defaultValue: 'post' })
						}`}
					</strong>
				</div>

				<Link to={{ pathname: `/influencer/campaigns/${props.campaign.id}`, state: props.extra }}>
					<Button type='primary' className='breif-btn' size='default'>
						{t('campaign:viewCampaign', { defaultValue: 'View campaign' })}
					</Button>
				</Link>
			</div>

			<Row>
				<Col xs={{ span: 24 }}>
					<TodoItem campaignStatus={props.status} campaignData={props.extra} onRefresh={props.onRefreshCampaigns} />
				</Col>
			</Row>
		</Card>
	);
};

export default CampaignCard;
