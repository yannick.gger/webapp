import styled from 'styled-components';
import { CampaignStatusType, CAMPAIGN_STATUS } from '../CampaignsContainer/types';
import InviteTodo from './Todos/InviteTodo';
import UploadTodo from './Todos/UploadTodo';
import PostInstaTodo from './Todos/PostInstaTodo';
import UploadStatTodo from './Todos/UploadStatTodo';
import ShowPaymentTodo from './Todos/ShowPaymentTodo';

const Wrapper = styled.div``;

const Styled = {
	Wrapper
};

const TodoItem = (props: { campaignStatus: CampaignStatusType; campaignData: any; onRefresh: () => void }) => {
	const renderTodo = () => {
		if (props.campaignStatus === CAMPAIGN_STATUS.INVITED.toLocaleLowerCase()) {
			return <InviteTodo campaignId={props.campaignData.campaign.id} influencerId={props.campaignData.influencer.id} onRefresh={props.onRefresh} />;
		}

		if (props.campaignStatus === CAMPAIGN_STATUS.WAIT_UPLOAD.toLocaleLowerCase()) {
			return (
				<UploadTodo
					campaignId={props.campaignData.campaign.id}
					assignments={props.campaignData.campaign.assignments}
					influencerId={props.campaignData.influencer.id}
					onRefresh={props.onRefresh}
				/>
			);
		}

		if (props.campaignStatus === CAMPAIGN_STATUS.ACCEPTED.toLocaleLowerCase()) {
			return (
				<UploadTodo
					campaignId={props.campaignData.campaign.id}
					assignments={props.campaignData.campaign.assignments}
					influencerId={props.campaignData.influencer.id}
					onRefresh={props.onRefresh}
				/>
			);
		}

		if (props.campaignStatus === CAMPAIGN_STATUS.REVIEW_REJECTED.toLocaleLowerCase()) {
			return (
				<UploadTodo
					campaignId={props.campaignData.campaign.id}
					assignments={props.campaignData.campaign.assignments}
					influencerId={props.campaignData.influencer.id}
					onRefresh={props.onRefresh}
				/>
			);
		}

		if (
			props.campaignStatus === CAMPAIGN_STATUS.REVIEW_ACCEPTED.toLocaleLowerCase() ||
			props.campaignStatus === CAMPAIGN_STATUS.POST_INSTA.toLocaleLowerCase() ||
			(props.campaignStatus === CAMPAIGN_STATUS.IN_REVIEW.toLocaleLowerCase() &&
				props.campaignData.campaign.assignments.some((assignment: any) => assignment.assignmentReview.createdAt === null))
		) {
			return (
				<UploadTodo
					campaignId={props.campaignData.campaign.id}
					assignments={props.campaignData.campaign.assignments}
					influencerId={props.campaignData.influencer.id}
					onRefresh={props.onRefresh}
				/>
			);
		}

		if (props.campaignStatus === CAMPAIGN_STATUS.DONE.toLocaleLowerCase()) {
			return <ShowPaymentTodo />;
		}

		return <div>Waiting for next step</div>;
	};

	return <Styled.Wrapper>{renderTodo()}</Styled.Wrapper>;
};

export default TodoItem;
