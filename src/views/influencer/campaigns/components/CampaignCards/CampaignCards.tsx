import { Row, Col } from 'antd';
import CampaignCard from './CampaignCard';
import { InfluencerCampaign } from '../CampaignsContainer/types';

const CampaignCards = (props: { campaigns: InfluencerCampaign[]; title: string; onRefreshCampaigns: () => void }) => {
	return (
		<div>
			<div className='mb-10'>
				<h2>{props.title}</h2>
			</div>
			<Row gutter={30} className='item-rows gutter-30'>
				{props.campaigns.map((campaign, index) => (
					<Col key={index} className='mb-30' span={24}>
						<CampaignCard extra={campaign} campaign={campaign.campaign} status={campaign.status} onRefreshCampaigns={props.onRefreshCampaigns} />
					</Col>
				))}
			</Row>
		</div>
	);
};

export default CampaignCards;
