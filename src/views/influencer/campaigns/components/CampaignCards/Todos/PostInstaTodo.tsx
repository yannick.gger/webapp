import gql from 'graphql-tag';
import { useState } from 'react';
import { Modal } from 'antd';
import InfluencerService from 'services/Influencer';
import { ApolloConsumer } from 'react-apollo';
import styled from 'styled-components';

const Wrapper = styled.div`
	width: 100%;
	height: 150px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;

const Title = styled.div`
	font-size: 1.2rem;
	font-weight: 600;
`;

const Buttons = styled.div`
	display: flex;
	column-gap: 5px;
`;

const Button = styled.div`
	flex: 1;
	padding: 10px 10px;
	text-align: center;
	border: 1px solid black;
	cursor: pointer;

	&:hover {
		background-color: #ccc;
	}
`;

const Styled = {
	Wrapper,
	Title,
	Buttons,
	Button
};

const UPDATE_INSTAGRAM_POST_TODO = gql`
	mutation updateInstagramPostTodo($instagramUrl: String!, $todoPostInstagramId: Int!, $timestamp: String!) {
		postInstagram(input: { instagramUrl: $instagramUrl, todoPostInstagramId: $todoPostInstagramId, timestamp: $timestamp }) {
			todoPostInstagram {
				id
			}
			errors {
				message
			}
		}
	}
`;

const PostInstaTodo = (props: { assignments: any[] }) => {
	console.log(props.assignments);
	const [modalShow, setModalShow] = useState(false);
	const [modalData, setModalData] = useState<{ campaignId: string; assignmentId: string; influencerId: string } | null>(null);
	return (
		<ApolloConsumer>
			{(client) => (
				<div>
					{props.assignments.length > 0
						? props.assignments.map((assignment, index) => {
								return (
									<>
										<Styled.Wrapper key={index}>
											<Styled.Title>{`Confirm publishing of ${assignment.name}`}</Styled.Title>
											<Styled.Buttons>
												<Styled.Button
													onClick={() => {
														setModalShow(true);
													}}
												>
													Post
												</Styled.Button>
											</Styled.Buttons>
										</Styled.Wrapper>
										<UpdatePostModal
											campaignData={{ assignmentId: assignment.id }}
											client={client}
											show={modalShow}
											onClose={() => {
												setModalShow(false);
											}}
										/>
									</>
								);
						  })
						: null}
				</div>
			)}
		</ApolloConsumer>
	);
};

const ModalInnerWrapper = styled.div`
	display: flex;
	flex-direction: column;
	row-gap: 2rem;
`;

const Textarea = styled.textarea`
	width: 100%;
	height: 200px;
	resize: none;
`;

const SaveButton = styled.div`
	width: 100%;
	padding: 10px;
	text-align: center;
	border: 1px solid black;
`;

const UploadModalStyled = {
	ModalInnerWrapper,
	Textarea,
	SaveButton
};

const UpdatePostModal = (props: { show: boolean; onClose: () => void; campaignData: { assignmentId: string }; client: any }) => {
	const [instaPostUrl, setInstaPostUrl] = useState('');
	const [postedDate, setPostedDate] = useState<any>(null);

	return (
		<Modal visible={props.show} footer={null} onCancel={props.onClose}>
			<UploadModalStyled.ModalInnerWrapper>
				<div>
					<div>Instagram post URL</div>
					<input
						type='text'
						value={instaPostUrl}
						onChange={(e) => {
							setInstaPostUrl(e.target.value);
						}}
					/>
				</div>

				<div>
					<div>Posted Date</div>
					<input
						type='date'
						onChange={(e) => {
							setPostedDate(new Date(e.target.value));
						}}
					/>
				</div>

				<div>
					<UploadModalStyled.SaveButton
						onClick={() => {
							props.client
								.mutate({
									mutation: UPDATE_INSTAGRAM_POST_TODO,
									variables: {
										instagramUrl: instaPostUrl,
										todoPostInstagramId: +props.campaignData.assignmentId,
										timestamp: postedDate
									}
								})
								.then((res: any) => {
									console.log(res);
								});
						}}
					>
						Save
					</UploadModalStyled.SaveButton>
				</div>
			</UploadModalStyled.ModalInnerWrapper>
		</Modal>
	);
};

export default PostInstaTodo;
