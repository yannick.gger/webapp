import gql from 'graphql-tag';
import InfluencerService from 'services/Influencer';
import { ApolloConsumer } from 'react-apollo';
import styled from 'styled-components';

const Wrapper = styled.div`
	width: 100%;
	height: 150px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;

const Title = styled.div`
	font-size: 1.2rem;
	font-weight: 600;
`;

const Buttons = styled.div`
	display: flex;
	column-gap: 5px;
`;

const Button = styled.div`
	flex: 1;
	padding: 10px 10px;
	text-align: center;
	border: 1px solid black;
	cursor: pointer;

	&:hover {
		background-color: #ccc;
	}
`;

const Styled = {
	Wrapper,
	Title,
	Buttons,
	Button
};

const DECLINE_CAMPAIGN = gql`
	mutation declineCampaign($campaignId: ID!) {
		declineCampaign(input: { campaignId: $campaignId }) {
			campaign {
				id
			}
		}
	}
`;

const UploadStatTodo = (props: { campaignId: string; influencerId: string }) => {
	return (
		<ApolloConsumer>
			{(client) => (
				<Styled.Wrapper>
					<Styled.Title>Invite to join</Styled.Title>
					<Styled.Buttons>
						<Styled.Button
							onClick={() => {
								InfluencerService.joinCampaign(props.campaignId, props.influencerId);
							}}
						>
							Join
						</Styled.Button>

						<Styled.Button
							onClick={() => {
								client.mutate({
									mutation: DECLINE_CAMPAIGN,
									variables: props.campaignId
								});
							}}
						>
							Decline
						</Styled.Button>
					</Styled.Buttons>
				</Styled.Wrapper>
			)}
		</ApolloConsumer>
	);
};

export default UploadStatTodo;
