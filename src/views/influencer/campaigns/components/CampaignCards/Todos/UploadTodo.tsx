import { useEffect, useState, useRef } from 'react';
import moment from 'moment';
import { Modal, Carousel, Form, Input, DatePicker } from 'antd';

import InfluencerService from 'services/Influencer';
import { ApolloConsumer } from 'react-apollo';
import styled from 'styled-components';
import LoadingSpinner from 'components/LoadingSpinner';
import { useTranslation } from 'react-i18next/hooks';

const Wrapper = styled.div`
	width: 100%;
	height: 150px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;

const Title = styled.div`
	font-size: 1.2rem;
	font-weight: 600;
	margin-bottom: 2rem;
`;

const Buttons = styled.div`
	display: flex;
	column-gap: 5px;
`;

const Button = styled.button`
	flex: 1;
	padding: 10px 10px;
	text-align: center;
	border: 1px solid black;
	cursor: pointer;
	background-color: transparent;

	&:disabled {
		background-color: #ccc;
	}

	&:hover {
		background-color: #ccc;
	}
`;

const CarouselWrapper = styled(Carousel)`
	> .slick-dots li button {
		width: 6px;
		height: 6px;
		border-radius: 100%;
		background-color: gray;
	}
	> .slick-dots li.slick-active button {
		width: 7px;
		height: 7px;
		border-radius: 100%;
		background-color: black;
	}
`;

const Styled = {
	Wrapper,
	Title,
	Buttons,
	Button,
	CarouselWrapper
};

const UploadTodo = (props: { campaignId: string; assignments: any[]; influencerId: string; onRefresh: () => void }) => {
	const [modalShow, setModalShow] = useState(false);
	const [modalData, setModalData] = useState<{ campaignId: string; assignmentId: string; influencerId: string } | null>(null);
	const [uploadLinkModalShow, setUploadLinkModalShow] = useState(false);

	const showButtonText = (assignment: any) => {
		if (
			assignment.assignmentReview.createdAt !== null &&
			assignment.assignmentReview.text !== null &&
			assignment.assignmentReview.media !== null &&
			assignment.assignmentReview.status === 0
		) {
			return 'Waiting for review';
		}
		if (assignment.assignmentReview.status === 1) {
			return 'Upload instagram post link';
		}

		return 'Upload Content';
	};

	return (
		<div>
			<CarouselWrapper>
				{props.assignments.map((assignment, index) => {
					return (
						<Styled.Wrapper key={index}>
							<Styled.Title>{`Upload ${assignment.name} for review`}</Styled.Title>
							<Styled.Buttons>
								<Styled.Button
									onClick={() => {
										setModalData({ campaignId: props.campaignId, assignmentId: assignment.id, influencerId: props.influencerId });
										if (showButtonText(assignment) === 'Upload Content') {
											setModalShow(true);
										} else if (showButtonText(assignment) === 'Upload instagram post link') {
											setUploadLinkModalShow(true);
										}
									}}
									disabled={
										assignment.assignmentReview.createdAt !== null &&
										assignment.assignmentReview.text !== null &&
										assignment.assignmentReview.media !== null &&
										assignment.assignmentReview.reviewedAt !== null
									}
								>
									{showButtonText(assignment)}
								</Styled.Button>
							</Styled.Buttons>
						</Styled.Wrapper>
					);
				})}
			</CarouselWrapper>
			<UploadModal
				show={modalShow && modalData !== null}
				campaignData={modalData as { campaignId: string; assignmentId: string; influencerId: string }}
				onClose={() => {
					props.onRefresh();
					setModalShow(false);
				}}
			/>
			<UploadLinkModal
				show={uploadLinkModalShow && modalData !== null}
				campaignData={modalData as { campaignId: string; assignmentId: string; influencerId: string }}
				onClose={() => {
					props.onRefresh();
					setUploadLinkModalShow(false);
				}}
			/>
		</div>
	);
};

const ModalInnerWrapper = styled.div`
	display: flex;
	flex-direction: column;
	row-gap: 2rem;
`;

const Textarea = styled.textarea`
	width: 100%;
	height: 200px;
	resize: none;
`;

const SaveButton = styled.button`
	width: 100%;
	padding: 10px;
	text-align: center;
	border: 1px solid black;
	background-color: transparent;
	cursor: pointer;

	&:hover {
		background-color: #ccc;
	}
`;

const UploadModalStyled = {
	ModalInnerWrapper,
	Textarea,
	SaveButton
};

const UploadModal = (props: { show: boolean; onClose: () => void; campaignData: { campaignId: string; assignmentId: string; influencerId: string } }) => {
	const [submitLoading, setSubmitLoading] = useState(false);
	const [contentText, setContentText] = useState('');
	const [files, setFiles] = useState<any[] | null>(null);

	const getAssignmentReviewId = () => {
		setSubmitLoading(true);
		InfluencerService.createReview(props.campaignData, '.')
			.then((res: any) => {
				return { assignmentReviewId: res.data.id };
			})
			.then((res) => {
				uploadImageToAssignment(res.assignmentReviewId);
				return res;
			})
			.then((res: { assignmentReviewId: string }) => {
				createPostForReview(res.assignmentReviewId);
			});
	};

	const createPostForReview = async (assignmentReviewId: string) => {
		await InfluencerService.updateText(
			{ campaignId: props.campaignData.campaignId, assignmentId: props.campaignData.assignmentId, assignmentReviewId: assignmentReviewId },
			contentText
		).then((res) => {
			setSubmitLoading(false);
			setContentText('');
			setFiles(null);
			props.onClose();
		});
	};

	const uploadImageToAssignment = async (assignmentReviewId: string) => {
		if (files) {
			await [...files].forEach((file, index) => {
				InfluencerService.uploadReviewImage(
					{ campaignId: props.campaignData.campaignId, assignmentId: props.campaignData.assignmentId, assignmentReviewId: assignmentReviewId! },
					file
				);
			});
		}
	};

	const updateFile = (event: any) => {
		setFiles(event.target.files);
	};

	return (
		<Modal visible={props.show} footer={null} onCancel={props.onClose}>
			<UploadModalStyled.ModalInnerWrapper>
				<div>
					<div />
					<UploadModalStyled.Textarea value={contentText} onChange={(e) => setContentText(e.target.value)} />
				</div>

				<div>
					<div>Step 2. Upload image</div>
					<input type='file' multiple onChange={updateFile} />
				</div>

				<div>
					<UploadModalStyled.SaveButton onClick={getAssignmentReviewId}>{submitLoading ? <LoadingSpinner size='sm' /> : 'Submit'}</UploadModalStyled.SaveButton>
				</div>
			</UploadModalStyled.ModalInnerWrapper>
		</Modal>
	);
};

const UploadLinkModal = (props: { show: boolean; onClose: () => void; campaignData: { campaignId: string; assignmentId: string; influencerId: string } }) => {
	const [submitLoading, setSubmitLoading] = useState(false);

	const [link, setLink] = useState(null);
	const [postedAt, setPostedAt] = useState(moment().toISOString());
	const saveFormRef = useRef(null);

	const submitPostLink = () => {
		setSubmitLoading(true);
		if (link && postedAt) {
			InfluencerService.uploadPostLink(props.campaignData, link, postedAt).then((res) => {
				setLink(null);
				setPostedAt(moment().toISOString());
				setSubmitLoading(false);
				props.onClose();
			});
		}
	};

	return (
		<Modal visible={props.show} footer={null} onCancel={props.onClose}>
			<UploadModalStyled.ModalInnerWrapper>
				<div>
					<WrappedContentUrlForm ref={saveFormRef} setLink={setLink} setPostedAt={setPostedAt} />
				</div>

				<div>
					<UploadModalStyled.SaveButton onClick={submitPostLink}>{submitLoading ? <LoadingSpinner size='sm' /> : 'Submit'}</UploadModalStyled.SaveButton>
				</div>
			</UploadModalStyled.ModalInnerWrapper>
		</Modal>
	);
};

const LinkForm = (props: any) => {
	const { setLink, setPostedAt } = props;
	const { getFieldDecorator } = props.form;
	const [t] = useTranslation('influencer');

	return (
		<div>
			<Form.Item
				label={t('influencer:assignmentReportFormLinkLabel', { defaultValue: 'Länk till din post på {{assignmentType}}', assignmentType: 'Instagram' })}
			>
				{getFieldDecorator('instagramUrl', {
					initialValue: 'https://www.instagram.com/p/',
					rules: [
						{
							required: true,
							message: t('influencer:assignmentReportFormLinkErrorRequired', {
								defaultValue: 'Skriv in länken till din {{assignmentType}} post',
								assignmentType: 'Instagram'
							})
						},
						{
							pattern: /^https:\/\/(www\.|)instagram\.com\/p\/(.*)$/,
							message: t('influencer:assignmentReportFormLinkErrorPattern', { defaultValue: 'Länken ska börja med https://www.instagram.com/p/' })
						}
					]
				})(
					<Input
						size='large'
						className='mb-10'
						placeholder='https://www.instagram.com/p/BgtNcp-hEaqwbhhdgFXIJt3D01xy2nnA6C0Lq00'
						onChange={(e) => {
							setLink(e.target.value);
						}}
					/>
				)}
			</Form.Item>
			<Form.Item label={t('influencer:assignmentReportFormDateLabel', { defaultValue: 'Publiceringsdatum' })}>
				{getFieldDecorator('timestamp', {
					initialValue: moment(),
					required: true
				})(<DatePicker onChange={(e) => setPostedAt(e.toISOString())} />)}
			</Form.Item>
		</div>
	);
};

const WrappedContentUrlForm = Form.create()(LinkForm);

export default UploadTodo;
