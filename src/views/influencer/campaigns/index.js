import React, { Component } from 'react';
import CampaignList from './components/campaign-list/';

export default class CampaignsView extends Component {
	render() {
		document.title = `Campaigns | Collabs`;

		return <CampaignList />;
	}
}
