import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { withApollo, Query } from 'react-apollo';
import { InvoiceIcon } from 'components/icons';
import { Modal, Table, Spin, Button, message } from 'antd';

import getCampaignForInfluencerInvoicing from 'graphql/get-campaign-for-influencer-invoicing.graphql';
import markCampaignInstagramOwnerInvoiceSent from 'graphql/mark-campaign-instagram-owner-invoice-sent.graphql';
import './styles.scss';

class InfluencerCampaignInvoicing extends Component {
	state = {
		visible: true,
		mutationLoading: false
	};

	onSubmit = (campaignId) => {
		const { client } = this.props;

		this.setState({ mutationLoading: true });
		client
			.mutate({
				mutation: markCampaignInstagramOwnerInvoiceSent,
				variables: { campaignId: campaignId }
			})
			.then((result) => {
				this.setState({ mutationLoading: false });
				if (result.data.markCampaignInstagramOwnerInvoiceSent.campaignInstagramOwner.invoiceSent) {
					message.success('Campaign completed!');
					this.onClose();
				} else {
					message.error('Something went wrong');
				}
			})
			.catch(({ graphQLErrors }) => {
				if (graphQLErrors) {
					graphQLErrors.forEach((error) => {
						message.error(error.message);
					});
				}
				this.setState({ mutationLoading: false });
			});
	};

	onClose = () => {
		this.setState({ visible: false });
		this.props.closeModal();
	};

	render() {
		const { t } = this.props;

		const modalOptions = {
			width: 921,
			footer: null,
			visible: true,
			onOk: this.onClose,
			onCancel: this.onClose,
			onUpdate: this.onClose
		};

		return (
			<Query query={getCampaignForInfluencerInvoicing} variables={{ id: this.props.match.params.campaignId }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;

					const campaign = data.campaign;
					const { name, currency } = campaign;
					const nodes_with_invoice = data.campaign.campaignCompensations.edges.filter((item) => item.node.invoice);
					const invoiceSum = nodes_with_invoice.map(({ node }) => node.invoice.value).reduce((current, a) => current + a, 0);

					const columns = [
						{
							dataIndex: 'field',
							key: 'field'
						},
						{
							dataIndex: 'data',
							key: 'data'
						},
						{
							dataIndex: 'copy',
							key: 'copy',
							width: 50
						}
					];

					const dataSource = [
						{
							field: t('influencer:campaignSendInvoiceCompany', { defaultValue: `Company name` }),
							data: 'We Are Cube AB'
						},
						{
							field: t('influencer:campaignSendInvoiceOrgNr', { defaultValue: `Organisation number` }),
							data: '556925-4104'
						},
						{
							field: t('influencer:campaignSendInvoiceVATNr', { defaultValue: `VAT number` }),
							data: 'SE556925410401'
						},
						{
							field: t('influencer:campaignSendInvoiceAddress', { defaultValue: `Address` }),
							data: 'Birger Jarlsgatan 9, 111 45 Stockholm, Sweden'
						},
						{
							field: t('influencer:campaignSendInvoiceReference', { defaultValue: `Reference` }),
							data: 'Rim'
						},
						{
							field: t('influencer:campaignSendInvoiceDescription', { defaultValue: `Description` }),
							data: t('influencer:campaignSendInvoiceDescriptionData', {
								defaultValue: `Invoice for "{{campaignName}}"`,
								campaignName: name
							})
						},
						{
							field: t('influencer:campaignSendInvoiceSum', { defaultValue: `Amount` }),
							data: `${invoiceSum} ${currency}`
						},
						{
							field: t('influencer:campaignSendInvoiceVAT', { defaultValue: `VAT` }),
							data: t('influencer:campaignSendInvoiceVATData', {
								defaultValue: `campaignSendInvoiceVATData": "25% (Only for Swedish registered companies), 0% (For EU registered companies outside Sweden)`
							})
						},
						{
							field: t('influencer:campaignPaymentCondition', { defaultValue: `Payment Condition` }),
							data: t('influencer:campaignPaymentConditionData', {
								defaultValue: `campaignPaymentConditionData": "30 days`
							})
						}
					];

					return (
						<Modal className='influencer-campaign-invoice-modal' {...modalOptions}>
							<h3>{t('influencer:campaignSendInvoiceHeader', { defaultValue: `Send invoice for "{{campaignName}}"`, campaignName: name })}</h3>
							<p>{t('influencer:campaignSendInvoiceSubheader', { defaultValue: `You have an outstanding payment to invoice` })}</p>

							<InvoiceIcon className='invoice-modal-icon' />

							<Table dataSource={dataSource} columns={columns} pagination={false} showHeader={false} size='small' />

							<div className='mt-20'>
								<p>
									{t('influencer:campaignSendInvoiceGiggerRef', {
										defaultValue: `If you dont have your own company to send invoices from, we have partnered with the service Payout where you can send invoices at the cheapest rates on the market. You can register here: `
									})}
									&nbsp;
									<a href='https://gigger.se/collabs' target='_blank' rel='noreferrer'>
										{t('influencer:campaignSendInvoiceGiggerRefLink', {
											defaultValue: `Invoice now`
										})}
										!
									</a>
								</p>

								<div className='text-center'>
									<p className='mt-15'>
										{t('influencer:campaignSendInvoiceMail', {
											defaultValue: `Send your invoice as a PDF file to invoice@wearecube.se`
										})}
										<br />
										<b>
											{t('influencer:campaignSendInvoiceMailExtra', {
												defaultValue: `This email is an automated system, for questions please contact support!`
											})}
										</b>
									</p>

									<Button type='primary' onClick={() => this.onSubmit(campaign.id)} loading={this.state.mutationLoading}>
										Mark invoice as sent
									</Button>
								</div>
							</div>
						</Modal>
					);
				}}
			</Query>
		);
	}
}

export default withApollo(translate('influencer')(InfluencerCampaignInvoicing));
