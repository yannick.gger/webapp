import { Button, Card, Collapse, Divider, Form, message, Modal, Spin, Timeline } from 'antd';
import SlamDunk from 'assets/img/app/slamdunk.svg';
import AccountSettingsForm from 'components/account-settings-form/';
import DeliveryForm from 'components/delivery-form/';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { Trans, translate } from 'react-i18next';
import { apiToMoment } from 'utils';
import joinCampaignMutation from 'graphql/join-campaign.graphql';
import getUserAccountSettingQuery from 'graphql/get-user-account-setting.graphql';
import { Elements, injectStripe } from 'react-stripe-elements';
import { getUserAccountSettingsValues } from '../settings/account';
import getCampaign from 'graphql/get-campaign.graphql';

const Panel = Collapse.Panel;

class JoinForm extends React.Component {
	render() {
		const { campaign, t, currentUser } = this.props;
		return (
			<React.Fragment>
				<div className='description'>
					<p>
						{t('influencer:campaignJoinDescription', {
							defaultValue: `Du håller på att gå med i "{{campaignName}}". Det är ett par saker du borde kolla genom för att se så allting stämmer.`,
							campaignName: campaign.name
						})}
					</p>
				</div>
				<div className='part'>
					<Card className='small-title mb-10 mt-10'>
						<h3 className='color-blue mb-0'>{t('influencer:campaignJoinHeader', { defaultValue: `Du kommer få detta som ersättning` })}</h3>
						<h3>
							{campaign.productCompensationsCount > 0 &&
								t('influencer:numProducts', { defaultValue: '{{count}} product', count: campaign.productCompensationsCount })}
							{campaign.paymentCompensationsCount > 0 && campaign.productCompensationsCount > 0 && ' + '}
							{campaign.invoiceCompensationsCount > 0 && campaign.productCompensationsCount > 0 && ' + '}
							{campaign.paymentCompensationsCount > 0 && `${campaign.paymentCompensationsTotalValue} ${campaign.currency}`}
							{campaign.invoiceCompensationsCount > 0 &&
								t('influencer:paymentInvoiceValue', {
									defaultValue: `${campaign.invoiceCompensationsTotalValue} ${campaign.currency} (paid by invoice)`,
									value: campaign.invoiceCompensationsTotalValue,
									currency: campaign.currency
								})}
						</h3>
						<p>
							{t('influencer:campaignJoinCompensationValue', {
								defaultValue: `Ersättning till ett värde av {{currency}}{{value}} `,
								value: campaign.compensationsTotalValue,
								currency: campaign.currency
							})}
						</p>
					</Card>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>{t('influencer:campaignJoinAssignmentsHeader', { defaultValue: `Du måste posta` })}</h3>
					<Card className='small-title mb-10 mt-10'>
						{campaign.assignments.edges.map(({ node }) => (
							<p key={node.id}>
								{apiToMoment(node.startTime).format('YYYY-MM-DD') === apiToMoment(node.endTime).format('YYYY-MM-DD') ? (
									<React.Fragment>
										{t('influencer:campaignJoinAssignmentsOneStart', {
											defaultValue: `1 {{nodeInstagramType}} den {{startTime}}`,
											nodeInstagramType: {
												video: t('video', { defaultValue: `video` }),
												photo: t('photo', { defaultValue: `photo` }),
												story: t('story', { defaultValue: `story` }),
												reel: t('reel', { defaultValue: `reel` }),
												tiktok: t('tiktok', { defaultValue: `tiktok` })
											}[node.instagramType],
											startTime: apiToMoment(node.startTime).format('YYYY-MM-DD')
										})}
									</React.Fragment>
								) : (
									<React.Fragment>
										{t('influencer:campaignJoinAssignmentsOneBetween', {
											defaultValue: `1 {{nodeInstagramType}} mellan {{between}}`,
											nodeInstagramType: {
												video: t('video', { defaultValue: `video` }),
												photo: t('photo', { defaultValue: `photo` }),
												story: t('story', { defaultValue: `story` }),
												reel: t('reel', { defaultValue: `reel` }),
												tiktok: t('tiktok', { defaultValue: `tiktok` })
											}[node.instagramType],
											between: `${apiToMoment(node.startTime).format('YYYY-MM-DD')} - ${apiToMoment(node.endTime).format('YYYY-MM-DD')}`
										})}
									</React.Fragment>
								)}
							</p>
						))}
					</Card>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>{t('influencer:campaignJoinProcessHeader', { defaultValue: `Såhär ser processen ut` })}</h3>
					<Card className='small-title mb-10 mt-10'>
						<Timeline>
							<Timeline.Item>{t('influencer:campaignJoinProcessJoin', { defaultValue: `Du gick med i kampanjen` })}</Timeline.Item>
							<Timeline.Item>
								{t('influencer:campaignJoinProcessClose', {
									defaultValue: `Inbjudan stänger den {{closeAt}}`,
									closeAt: apiToMoment(campaign.invitesCloseAt).format('YYYY-MM-DD')
								})}
							</Timeline.Item>
							{campaign.productCompensationsCount > 0 && (
								<Timeline.Item>{t('influencer:campaignJoinProcessProductReceive', { defaultValue: `Du mottager produkten som ersättning` })}</Timeline.Item>
							)}
							{campaign.assignments.edges.some(({ node }) => node.hasContentReview) && (
								<Timeline.Item>
									{t('influencer:campaignJoinProcessReview', {
										defaultValue: `Du skickar in posten för godkännande senast {{count}} dagar innan det ska postas`,
										count: 3
									})}
								</Timeline.Item>
							)}
							{campaign.assignments.edges.some(({ node }) => node.hasContentReview) && (
								<Timeline.Item>{t('influencer:campaignJoinProcessApproval', { defaultValue: `Du får posten godkänd` })}</Timeline.Item>
							)}
							<Timeline.Item>{t('influencer:campaignJoinProcessPost', { defaultValue: `Du postar på Instagram` })}</Timeline.Item>
							<Timeline.Item>
								<Trans i18nKey='influencer:campaignJoinProcessReport'>
									Du laddar upp en screenshot 2 dagar efter du postat som visar postens statistik.{' '}
									<a href='https://www.facebook.com/help/1533933820244654' target='_blank' rel='noopener noreferrer'>
										Läs mer
									</a>
								</Trans>
							</Timeline.Item>
							{campaign.paymentCompensationsCount + campaign.invoiceCompensationsCount > 0 && (
								<Timeline.Item>{t('influencer:campaignJoinProcessPayment', { defaultValue: `Du får betalt` })}</Timeline.Item>
							)}
						</Timeline>
						<Divider />
						<p>
							{t('influencer:campaignJoinProcessDisclaimer', {
								defaultValue: `Viktigt: Om du inte postar på rätt datum och i tid så kommer du behöva skicka tillbaka produkt(erna), eller bli skyldig att betala för dessa.`
							})}
						</p>
					</Card>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>{t('influencer:accountSettingsTitle', { defaultValue: 'Account settings' })}</h3>
					<Card className='small-title mb-10 mt-10'>
						<AccountSettingsForm form={this.props.form} campaign={campaign} />
					</Card>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>{t('influencer:deliverySettingsTitle', { defaultValue: 'Delivery' })}</h3>
					<Card className='small-title mb-10 mt-10'>
						<DeliveryForm form={this.props.form} currentUser={currentUser} />
					</Card>
				</div>
				<div className='part'>
					<Collapse accordion>
						<Panel
							header={t('influencer:campaignJoinTermsPanelHeader', { defaultValue: 'När du går med i kampanjen godkänner du våra villkor (engelska)' })}
							key='1'
						>
							<h3>Terms & Conditions Collabs</h3>
							<p>
								<em>Between the Brand & the Creator, named as “Influencer” below.</em>
							</p>
							<h4>General</h4>
							<p>
								The purpose of this collaboration is to promote the Brand’s product/s on the Influencer’s Instagram account in accordance to this agreement.
							</p>

							<h4>Objective</h4>
							<p>The full scope of the collaboration can be found on the Brand’s platform, hosted by Collabs.</p>

							<h4>Timeframe</h4>
							<p>The time period for the collaboration can be found on the Brand’s platform, hosted by Collabs.</p>

							<h4>Commission</h4>
							<p>
								The Influencer retains commission in the form of products, cash or the pair from the Brand. The size and format of commission can be found on
								the Brand’s platform, hosted by Collabs.
							</p>
							<p>
								Given that the Influencer complete the collaboration to full extent and within the given period of time, and that the agreement involves cash,
								the Influencer will attain the commission in full within twenty four (24) hours.
							</p>

							<h4>Terms</h4>
							<p>
								If the collaboration require Brand’s product/s as means of payment, the Influencer is obligated to complete the assignment/s in accordance to
								the agreement. If the Influencer accept the terms and conditions, but should fail to complete the assignment/s, either by not posting a picture,
								video or Instagram story, alternatively by inadequate implementation such as using the incorrect hashtag, mention or the like, the Influencer is
								obliged to return the product/s to the Brand in the same condition as it arrived within 7 days from that the campaign has ended.
							</p>
							<p>
								If the Influencer should fail to do so, the Influencer hereby approves of being charged with the value of the product/s by means on invoice.
							</p>
							<p>This is valid unless the Brand and the Influencer agree on otherwise during the time period of the campaign.</p>

							<h4>Liabilities</h4>
							<p>The Brand is responsible for sending out product/s to the Influencer in accordance to the campaign brief.</p>
							<p>
								The Brand is responsible for laying out the terms of the collaboration as well as the Influencer’s assignment/s in such context so that it
								leaves little to no room for misinterpretation regarding that of the Influencer’s quid pro quo.
							</p>
							<p>
								The brand pledges, during the circumstance of cash as means of payment, render the full amount on the Collabs escrow account, at the point of
								time when the Brand and the Influencer agree upon these terms. This will ensure that the Influencer receive payment within twenty four (24)
								hours.
							</p>
							<p>
								The Influencer is responsible to complete the full assignment/s in accordance to the timeframe that is agreed upon, notify the Brand when
								achieving the product/s, send in content for approval, as well as post picture, video or story on Instagram, information on which can be found
								on the Brand’s plattform, hosted by Collabs.
							</p>

							<h4>Taxes</h4>
							<p>Both parties are liable to pay tax in accordance to current regulations and law in their respective countries.</p>
							<p>
								Collabs carry no liability to either the Brand or the Influencer paying taxes in accordance to current regulations and / or laws in their
								respective countries.
							</p>
							<p>
								By means of cash as payment, Collabs is responsible to distribute these funds to the Influencer/s which have agreed to the terms and conditions,
								as well as have completed the assignment/s in full and in accordance to the timeframe that is agreed upon between the Brand and the Influencer.
							</p>
							<p>
								Under these circumstances, the Brand will attain a periodic table, whereas the Influencer will attain a self billing invoice issued to the
								Brand.
							</p>
							<p>
								These terms are explicit to the Influencer having a VAT-number and have entered all information required during registration on the Collabs
								platform.
							</p>
							<br />
							<p>
								For more detailed information regarding current tax regulations in your country, we advise you to visit the IRS website in your given country.
							</p>
						</Panel>
					</Collapse>
				</div>
				<div className='part text-center slam-dunk'>
					<h2 className='thin mb-30'>
						<Trans i18nKey='influencer:campaignJoinSubmit'>
							{"It's time to "}
							<strong>
								<em>slam dunk</em>
							</strong>
						</Trans>
					</h2>
					<img src={SlamDunk} alt='' className='svg' style={{ marginRight: '-40px' }} />
				</div>
			</React.Fragment>
		);
	}
}
const WrappedJoinForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.handleFormChange) {
			props.handleFormChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const { ...propFields } = props.fields || {};
		const { userAccountSetting } = props.fields;
		let formFields = {};
		if (propFields.userAccountSetting) {
			Object.keys(propFields.userAccountSetting).forEach((propFieldKey) => {
				formFields[`userAccountSetting.${propFieldKey}`] = Form.createFormField({ value: propFields.userAccountSetting[propFieldKey] });
			});
		}

		if (userAccountSetting) {
			if (userAccountSetting.additionalOwners) {
				formFields[`keys`] = Form.createFormField({
					value: Array.apply(null, {
						length: userAccountSetting.additionalOwners.length
					}).map(Number.call, Number)
				});
				let index = 0;
				userAccountSetting.additionalOwners.forEach((additionalOwner) => {
					formFields[`userAccountSetting.additionalOwners[${index}].firstName`] = Form.createFormField({
						value: additionalOwner.firstName
					});
					formFields[`userAccountSetting.additionalOwners[${index}].lastName`] = Form.createFormField({
						value: additionalOwner.lastName
					});
					formFields[`userAccountSetting.additionalOwners[${index}].dob.day`] = Form.createFormField({
						value: additionalOwner.dob && additionalOwner.dob.day
					});
					formFields[`userAccountSetting.additionalOwners[${index}].dob.year`] = Form.createFormField({
						value: additionalOwner.dob && additionalOwner.dob.year
					});
					formFields[`userAccountSetting.additionalOwners[${index}].dob.month`] = Form.createFormField({
						value: additionalOwner.dob && additionalOwner.dob.month
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.accountOpener`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.accountOpener
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.director`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.director
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.owner`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.owner
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.percentOwnership`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.percentOwnership
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.title`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.title
					});
					formFields[`userAccountSetting.additionalOwners[${index}].id`] = Form.createFormField({
						value: additionalOwner.id
					});
					index = index + 1;
				});
			}
			if (userAccountSetting.phone) {
				const phonePrefix = userAccountSetting.phone ? userAccountSetting.phone.split(' ')[0] : undefined;
				formFields['userAccountSetting.phone.prefix'] = Form.createFormField({ value: phonePrefix });
				formFields['userAccountSetting.phone.number'] = Form.createFormField({
					value: phonePrefix ? userAccountSetting.phone.replace(phonePrefix + ' ', '') : null
				});
			}

			formFields['userAccountSetting.dob.day'] = Form.createFormField({
				value: userAccountSetting.dob && userAccountSetting.dob.day
			});
			formFields['userAccountSetting.dob.year'] = Form.createFormField({
				value: userAccountSetting.dob && userAccountSetting.dob.year
			});
			formFields['userAccountSetting.dob.month'] = Form.createFormField({
				value: userAccountSetting.dob && userAccountSetting.dob.month
			});
		}

		return formFields;
	}
})(JoinForm);

class InfluencerCampaignJoin extends React.Component {
	handleSubmit = (joinCampaign) => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			const { t } = this.props;
			const campaignId = this.props.match.params.campaignId;

			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				this.setState({ loading: true });

				let graphqlValues = null;

				try {
					graphqlValues = {
						userAccountSetting: {
							...(await getUserAccountSettingsValues(values.userAccountSetting, this.props.stripe))
						},
						userDeliverySetting: { useSettingsAddress: true },
						campaignId: campaignId
					};
				} catch (e) {
					this.setState({ loading: false });
					message.error(e.message);
					return null;
				}

				joinCampaign({ variables: graphqlValues, refetchQueries: ['getCampaign', { query: getUserAccountSettingQuery }] })
					.then(({ data, error }) => {
						if (error) {
							message.error(t('influencer:campaignJoinErrorMessage', { defaultValue: 'Something went wrong' }));
						}
						if (data.joinCampaign.errors.length > 0) {
							data.joinCampaign.errors.forEach((error) => {
								message.error(error.message);
							});
						} else if (data.joinCampaign.errors.length === 0) {
							message.info(t('influencer:campaignJoinSuccessMessage', { defaultValue: 'Joined campaign' }));
							localStorage && localStorage.setItem('joinedCampaign', true);
							this.props.closeModal();
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};
	handleFormChange = () => {
		return () => {
			if (!this.formRef) {
				return;
			}
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		const { t } = this.props;
		return (
			<Query query={getCampaign} variables={{ id: this.props.match.params.campaignId }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;
					return (
						<Mutation mutation={joinCampaignMutation}>
							{(joinCampaign, { loading }) => (
								<Modal
									title={t('influencer:campaignJoinModalTitle', {
										defaultValue: 'Gå med i kampanjen'
									})}
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									wrapClassName='custom-modal box-parts title-center'
									maskClosable={false}
									footer={[
										<Button size='large' key='submit' type='primary' loading={loading} onClick={this.handleSubmit(joinCampaign)}>
											{t('influencer:joinCampaignButtonJoin', { defaultValue: `Join the campaign` })}
										</Button>
									]}
								>
									<Query query={getUserAccountSettingQuery}>
										{({ loading, error, data: accountSettingData }) => {
											if (loading || !data.currentUser) return <Spin className='collabspin' />;
											if (error) return <p>Could not load user settings</p>;
											return (
												<WrappedJoinForm
													wrappedComponentRef={this.saveFormRef}
													campaign={data.campaign}
													t={t}
													handleFormChange={this.handleFormChange(data.campaign)}
													currentUser={accountSettingData.currentUser}
													fields={{ userAccountSetting: accountSettingData.currentUser && accountSettingData.currentUser.userAccountSetting }}
												/>
											);
										}}
									</Query>
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

const WithStripe = injectStripe(InfluencerCampaignJoin);
const WithElements = (props) => (
	<Elements>
		<WithStripe {...props} />
	</Elements>
);

export default translate('influencer')(WithElements);
