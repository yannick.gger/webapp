import React, { Component } from 'react';
import { Card, Divider } from 'antd';
import SuccessImg from './success.png';

export default class Success extends Component {
	render() {
		const { assignment } = this.props;
		return (
			<React.Fragment>
				<Card className='ant-card-todos'>
					<div className='text-center mt-20 mb-30'>
						<img src={SuccessImg} alt='' />
						<h2 className='color-blue mt-20 mb-0'>Nice done!</h2>
						<p>
							You have completed the campaign <strong>{assignment.campaign.name}</strong>
						</p>
					</div>

					{assignment.campaign.paymentCompensationsTotalValue > 0 && (
						<React.Fragment>
							<div className='blue-box py-20 d-flex flex-column align-items-center justify-content-center'>
								<h4 className='mb-0'>You just earned</h4>
								<h1 className='mt-0 mb-0'>
									{assignment.campaign.paymentCompensationsTotalValue} {assignment.campaign.currency}{' '}
								</h1>
							</div>
							<Divider />
							<p>Payment in 7 days and the money will be paid to the bank account you have entered.</p>
						</React.Fragment>
					)}
				</Card>
			</React.Fragment>
		);
	}
}
