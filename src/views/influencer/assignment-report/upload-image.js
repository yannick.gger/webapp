import React, { Component } from 'react';
import { Upload, Icon, message } from 'antd';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import { translate } from 'react-i18next';

import './styles.scss';

const CREATE_CAMPAIGN_INSTAGRAM_OWNER_ASSIGNMENT_SCREENSHOT = gql`
	mutation createCampaignInstagramOwnerAssignmentScreenshot($userId: ID!, $assignmentId: ID!, $photo: Upload) {
		createCampaignInstagramOwnerAssignmentScreenshot(input: { userId: $userId, assignmentId: $assignmentId, photo: $photo }) {
			campaignInstagramOwnerAssignmentScreenshot {
				id
			}
		}
	}
`;

class UploadProductImage extends Component {
	state = {
		fileList: [],
		fileListUntouched: true,
		campaignInstagramOwnerAssignmentScreenshotIds: []
	};

	handleRemove = (file) => {
		if (file.response && file.response.createCampaignInstagramOwnerAssignmentScreenshot) {
			const campaignInstagramOwnerAssignmentScreenshotIds = this.state.campaignInstagramOwnerAssignmentScreenshotIds.filter(
				(id) => id !== file.response.createCampaignInstagramOwnerAssignmentScreenshot.campaignInstagramOwnerAssignmentScreenshot.id
			);
			this.setState(() => ({
				campaignInstagramOwnerAssignmentScreenshotIds: campaignInstagramOwnerAssignmentScreenshotIds
			}));
			this.props.updatedCampaignInstagramOwnerAssignmentScreenshotIdsHandler(campaignInstagramOwnerAssignmentScreenshotIds);
		}
	};

	handleCustomRequest = ({ client, assignmentId, userId, callbacks: { onPhotoUploading, onPhotoUploadDone } }) => {
		return ({ file, onError, onSuccess, onProgress }) => {
			onPhotoUploading && onPhotoUploading();
			client
				.mutate({ mutation: CREATE_CAMPAIGN_INSTAGRAM_OWNER_ASSIGNMENT_SCREENSHOT, variables: { photo: file, assignmentId: assignmentId, userId: userId } })
				.then(({ data, loading }) => {
					if (data) {
						this.setState((prevState) => ({
							campaignInstagramOwnerAssignmentScreenshotIds: [
								...prevState.campaignInstagramOwnerAssignmentScreenshotIds,
								data.createCampaignInstagramOwnerAssignmentScreenshot.campaignInstagramOwnerAssignmentScreenshot.id
							]
						}));
						this.props.updatedCampaignInstagramOwnerAssignmentScreenshotIdsHandler(this.state.campaignInstagramOwnerAssignmentScreenshotIds);

						onSuccess(data, file);
					}

					if (loading && loading.status !== 'done') onProgress({ percent: 100 - loading.percent }, file);

					onPhotoUploadDone && onPhotoUploadDone();
				})
				.catch(({ graphQLErrors }) => {
					onPhotoUploadDone && onPhotoUploadDone();
					graphQLErrors &&
						graphQLErrors.forEach((error) => {
							message.error(error.message);
						});
					onError();
				});

			return {
				abort() {}
			};
		};
	};

	handleChange = (info) => {
		const status = info.file.status;
		const { t } = this.props;
		if (!info.file.isExceedSize) {
			this.setState({ fileList: info.fileList });
		}

		if (status === 'done') {
			message.success(
				t('influencer:assignmentReportUploadSuccess', { defaultValue: `{{infoFileName}} file uploaded successfully.`, infoFileName: info.file.name })
			);
		} else if (status === 'error') {
			message.error(t('influencer:assignmentReportUploadError', { defaultValue: `{{infoFileName}} file upload failed.`, infoFileName: info.file.name }));
		}
	};

	beforeUpload = (file) => {
		const sizeInMB = file.size / (1024 * 1024);
		if (sizeInMB > 10) {
			file.isExceedSize = true;
			message.error('Attachment size exceeds the allowable limit. Maximum allowed size is 10 MB');
			return false;
		}
		return true;
	};
	render() {
		const { fileList } = this.state;
		const { assignmentId, userId, t, onPhotoUploading, onPhotoUploadDone } = this.props;
		const UploadButton = () => (
			<div>
				<p className='ant-upload-drag-icon'>
					<Icon type='inbox' />
				</p>
				<p className='ant-upload-text px-20'>
					{t('influencer:assignmentReportUploadBoxDescription', { defaultValue: `Click or drag an image here to upload` })}
				</p>
			</div>
		);

		return (
			<ApolloConsumer>
				{(client) => (
					<React.Fragment>
						<Upload.Dragger
							listType='picture'
							accept='image/*'
							fileList={fileList}
							className={fileList.length >= 1 && !this.props.multiple ? 'hide-upload-area' : null}
							onPreview={this.handlePreview}
							onChange={this.handleChange}
							customRequest={this.handleCustomRequest({ client, assignmentId, userId, callbacks: { onPhotoUploading, onPhotoUploadDone } })}
							multiple={this.props.multiple}
							beforeUpload={this.beforeUpload}
						>
							<UploadButton />
						</Upload.Dragger>
					</React.Fragment>
				)}
			</ApolloConsumer>
		);
	}
}

export default translate('influencer')(UploadProductImage);
