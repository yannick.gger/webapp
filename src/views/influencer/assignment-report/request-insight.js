import React from 'react';
import { Row, Col, Input, Modal, Collapse, Button, message, Form, Card, Spin } from 'antd';
import { Mutation, Query } from 'react-apollo';
import moment from 'moment';
import { apiToMoment } from 'utils';
import { translate } from 'react-i18next';

import getAssignmentQuery from 'graphql/get-assignment.graphql';
import updateCampaignInstagramOwnerAssignmentMutation from 'graphql/update-campaign-instagram-owner-assignment.graphql';
import UploadImage from './upload-image.js';
import Success from './success';
import { isAdmin, hasGhostToken } from 'services/auth-service.js';
import ShowStats from './show-stats.png';
import TapAnywhere from './tap-anywhere.png';
import HowToScreen from './how-to-screen.png';
import Story from './story.png';

const FormItem = Form.Item;

class NotReady extends React.Component {
	render() {
		const { assignment, t } = this.props;

		return (
			<React.Fragment>
				<div className='description'>
					<p>
						{t('influencer:assignmentReportDescription', { defaultValue: `You are about to complete "{{assignmentName}}"`, assignmentName: assignment.name })}
					</p>
				</div>
				<div className='part'>
					<Card className='small-title mb-10 mt-10'>
						<h3 className='color-blue mb-0'>{t('influencer:assignmentReportTooEarlyHeader', { defaultValue: `Det är för tidigt att ladda upp statistik` })}</h3>
						<p>
							{t('influencer:assignmentReportTooEarlyCaption', {
								defaultValue: `Du kan ladda upp en screenshot med statistik tidigast {{count}} dagar efter att bilden postades`,
								count: 2
							})}
						</p>
					</Card>
				</div>
			</React.Fragment>
		);
	}
}

class AssignmentReportForm extends React.Component {
	updatedCampaignInstagramOwnerAssignmentScreenshotIdsHandler = (ids) => {
		this.props.form.setFields({
			campaignInstagramOwnerAssignmentScreenshotIds: {
				value: ids
			}
		});
	};

	handlePhotoUploading = () => {
		this.props.onPhotoUploading();
	};

	handlePhotoUploadDone = () => {
		this.props.onPhotoUploadDone();
	};

	render() {
		const { form, assignment, user, t } = this.props;
		const { getFieldDecorator } = form;
		const type = assignment.instagramType === 'tiktok' ? 'TikTok' : 'Instagram';
		const Panel = Collapse.Panel;

		return (
			<React.Fragment>
				<div className='description'>
					<p>
						{t('influencer:assignmentReportUploadDescription', {
							defaultValue: `Ladda upp statistik för "{{assignmentName}}"`,
							assignmentName: assignment.name
						})}
					</p>
				</div>
				<div className='part'>
					<h3 className='color-blue mb-0'>{t('influencer:assignmentReportHeader', { defaultValue: `Statistik för kampanjrapport` })}</h3>
					<p>
						{t('influencer:assignmentReportCaption', {
							defaultValue: `Vi använder denna information för att få full statistik på dina poster, och för att bekräfta att du postat korrekt.`
						})}
					</p>
					<Card className='small-title mb-10 mt-20'>
						<Row gutter={15}>
							{assignment.instagramType !== 'story' && (
								<Col xs={{ span: 24 }}>
									<FormItem
										label={t('influencer:assignmentReportFormLinkLabel', { defaultValue: 'Länk till din post på {{assignmentType}}', assignmentType: type })}
									>
										{getFieldDecorator('instagramUrl', {
											rules: [
												{
													required: true,
													message: t('influencer:assignmentReportFormLinkErrorRequired', {
														defaultValue: 'Skriv in länken till din {{assignmentType}} post',
														assignmentType: type
													})
												},
												{
													pattern:
														assignment.instagramType !== 'reel'
															? /^https:\/\/(www\.|)instagram\.com\/p\/(.*)$/
															: /^https:\/\/(www\.|)instagram\.com\/reel\/(.*)$/,
													message:
														assignment.instagramType !== 'reel'
															? t('influencer:assignmentReportFormLinkErrorPattern', {
																	defaultValue: 'Länken ska börja med https://www.instagram.com/p/'
															  })
															: t('influencer:assignmentReportFormLinkErrorPatternReel', {
																	defaultValue: 'Länken ska börja med https://www.instagram.com/reel/'
															  })
												}
											]
										})(
											<Input
												size='large'
												placeholder={`https://www.instagram.com/${assignment.instagramType !== 'reel' ? 'p' : 'reel'}/BgtNcp-hEaqwbhhdgFXIJt3D01xy2nnA6C0Lq00`}
											/>
										)}
									</FormItem>
								</Col>
							)}
							<Col xs={{ span: 24 }}>
								<FormItem label={t('influencer:assignmentReportFormScreenshotLabel', { defaultValue: 'Screenshot på Insights' })}>
									{getFieldDecorator('campaignInstagramOwnerAssignmentScreenshotIds', {
										rules: [
											{ required: true, message: t('influencer:assignmentReportFormScreenshotErrorRequired', { defaultValue: 'Ladda upp ett screenshot' }) }
										]
									})(
										<UploadImage
											assignmentId={assignment.id}
											userId={user.id}
											multiple={true}
											updatedCampaignInstagramOwnerAssignmentScreenshotIdsHandler={this.updatedCampaignInstagramOwnerAssignmentScreenshotIdsHandler}
											onPhotoUploading={this.handlePhotoUploading}
											onPhotoUploadDone={this.handlePhotoUploadDone}
										/>
									)}
								</FormItem>
							</Col>
						</Row>
					</Card>

					<Collapse defaultActiveKey={['1']} className='mt-20'>
						<Panel header='Hur du tar en screenshot på en Post' key='1'>
							<ul className='list'>
								<li>
									<strong>1.</strong> Go to your post
								</li>
								<li>
									<p>
										<strong>2.</strong> Press "Visa statistik" or "Show statistics"
									</p>
									<img src={ShowStats} alt='' style={{ marginLeft: '-45px' }} className='responsive-img' />
								</li>
								<li>
									<strong>3.</strong> Press anywhere on the stats that appear
									<img src={TapAnywhere} alt='' className='responsive-img' />
								</li>
								<li>
									<strong>4.</strong> Scroll down a bit and take a screenshot as seen in the example below
									<img src={HowToScreen} alt='' className='responsive-img' />
								</li>
								<li>
									<strong>5.</strong> Upload the screenshot
								</li>
							</ul>
						</Panel>
						<Panel header='Hur du tar en screenshot på en Story' key='2'>
							<ul className='list'>
								<li>
									<strong>1.</strong> Go to your story within 24 hours
								</li>
								<li>
									<strong>2.</strong> Swipe up on the story
								</li>
								<li>
									<strong>3.</strong> Press the little stats icon
									<img src={Story} alt='' className='responsive-img' style={{ marginLeft: '-45px' }} />
								</li>
								<li>
									<strong>4.</strong> Take a screenshot (
									<a
										href='https://www.greenbot.com/article/2825064/android/how-to-take-a-screenshot-on-your-android-phone.html'
										target='_blank'
										rel='noopener noreferrer'
									>
										Android
									</a>{' '}
									/{' '}
									<a href='https://support.apple.com/en-ca/ht200289' target='_blank' rel='noopener noreferrer'>
										iPhone
									</a>
									)
								</li>
								<li>
									<strong>5.</strong> Upload the screenshot
								</li>
							</ul>
						</Panel>
					</Collapse>
				</div>
			</React.Fragment>
		);
	}
}
const WrappedAssignmentReportForm = Form.create()(AssignmentReportForm);

class InfluencerAssignmentReport extends React.Component {
	componentDidMount() {
		if (isAdmin() || hasGhostToken()) {
			message.error("Admin can't access this page");
			this.props.history.push('/');
		}
	}
	state = {
		completed: false,
		photoUploadStatus: false
	};

	handlePhotoUploading = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: true }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handlePhotoUploadDone = () => {
		const fieldsValue = this.formRef.props.form.getFieldsValue();
		this.setState({ photoUploadStatus: false }, () => this.formRef.props.form.setFieldsValue({ ...fieldsValue }));
	};

	handleSubmit = (updateCampaignInstagramOwnerAssignment, currentUser) => {
		return (e) => {
			e.preventDefault();

			if (this.state.photoUploadStatus) return message.info('Photo upload is still in progress. Please wait a moment.');

			const form = this.formRef.props.form;
			const { t } = this.props;
			form.validateFieldsAndScroll((err, values) => {
				if (err) return null;

				const graphqlValues = {
					...values,
					assignmentId: this.props.match.params.assignmentId,
					userId: currentUser.id,
					posted: true
				};

				updateCampaignInstagramOwnerAssignment({ variables: graphqlValues, refetchQueries: ['getAssignment'] })
					.then(({ data, error }) => {
						if (data.updateCampaignInstagramOwnerAssignment && !error) {
							if (data.updateCampaignInstagramOwnerAssignment.campaignSucceeded) {
								this.setState({ completed: true });
							} else {
								message.info(t('influencer:assignmentReportSuccessMessage', { defaultValue: 'Uppgiften är klar! Bra jobbat!' }));
								this.props.closeModal();
							}
						} else {
							message.error(t('influencer:assignmentReportErrorMessage', { defaultValue: 'Något gick fel' }));
						}
					})
					.catch((error) => {
						message.error(error.message);
					});
			});
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		const { t } = this.props;
		return (
			<Query query={getAssignmentQuery} variables={{ id: this.props.match.params.assignmentId }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;

					const ready = apiToMoment(data.assignment.endTime)
						.add(2, 'days')
						.isBefore(moment());

					return (
						<Mutation mutation={updateCampaignInstagramOwnerAssignmentMutation}>
							{(updateCampaignInstagramOwnerAssignment, { loading }) => (
								<Modal
									title={t('influencer:assignmentReportModalTitle', { defaultValue: 'Bekräfta din post och markera som klar' })}
									visible
									onOk={this.handleOk}
									onCancel={this.handleCancel}
									wrapClassName='custom-modal box-parts title-center'
									maskClosable={false}
									footer={
										ready && !this.state.completed
											? [
													<Button
														size='large'
														key='submit'
														type='primary'
														loading={loading}
														onClick={this.handleSubmit(updateCampaignInstagramOwnerAssignment, data.currentUser, data.assignment)}
													>
														{t('influencer:assignmentReportSubmitButton', { defaultValue: `Complete` })}
													</Button>
											  ]
											: []
									}
								>
									{ready && !this.state.completed && (
										<WrappedAssignmentReportForm
											wrappedComponentRef={this.saveFormRef}
											assignment={data.assignment}
											user={data.currentUser}
											t={t}
											onPhotoUploading={this.handlePhotoUploading}
											onPhotoUploadDone={this.handlePhotoUploadDone}
										/>
									)}
									{!ready && !this.state.completed && <NotReady assignment={data.assignment} t={t} />}
									{this.state.completed && <Success assignment={data.assignment} currentUser={data.currentUser} />}
								</Modal>
							)}
						</Mutation>
					);
				}}
			</Query>
		);
	}
}

export default translate('influencer')(InfluencerAssignmentReport);
