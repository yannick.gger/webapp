import React, { Component } from 'react';
import { Modal, Button, message, Spin } from 'antd';
import { withApollo } from 'react-apollo';
import connectWithFacebook from 'graphql/connect-with-facebook.graphql';
import { Query } from 'react-apollo';
import getCurrentUser from 'graphql/get-current-user.graphql';
class ConnectWithFacebook extends Component {
	state = {
		visible: false
	};

	showModal = () => {
		this.setState({
			visible: true
		});
	};

	handleCancel = () => {
		this.setState({
			visible: false
		});
	};

	// componentWillUnmount() {
	//   window.FB.Event.unsubscribe("auth.statusChange", this.facebookConnect)
	// }

	// componentDidMount() {
	//   window.FB.Event.subscribe("auth.statusChange", this.facebookConnect)
	// }

	facebookConnect = () => {
		const { client } = this.props;
		const { FB } = window;
		FB.getLoginStatus((response) => {
			this.setState({ visible: false });
			if (response.authResponse) {
				const { status } = response;
				const { userID, accessToken } = response.authResponse;
				const facebookUserId = userID;
				if (status === 'connected') {
					client
						.mutate({
							mutation: connectWithFacebook,
							variables: {
								accessToken,
								facebookUserId
							},
							refetchQueries: ['getCurrentUser', 'getOrganizationCampaigns']
						})
						.then((result) => {
							const { token } = result.data.connectWithFacebook;
							if (token) {
								message.success('Connect with Facebook successfully', 5);
								FB.logout();
							}
						})
						.catch(({ graphQLErrors }) => {
							if (graphQLErrors) {
								graphQLErrors.forEach((error) => {
									message.error(error.message);
								});
								FB.logout();
							}
						});
				}
			} else {
				// window.FB.login(this.facebookLoginHandler, { scope: "instagram_basic,instagram_manage_insights,pages_show_list", return_scopes: true })
			}
		});
	};

	facebookLoginHandler = (response) => {
		if (response.status === 'connected') {
			const accessToken = response.authResponse.accessToken;
			// window.FB.api("/me?fields=id,name,picture", { access_token: accessToken })
		}
	};

	render() {
		const { visible } = this.state;
		return (
			<Query query={getCurrentUser}>
				{({ data, loading: queryLoading }) => {
					if (queryLoading) {
						return <Spin className='collabspin' />;
					}
					return (
						<section>
							<Modal
								title='Preparing to Connect with Facebook'
								visible={visible}
								onOk={this.handleOk}
								onCancel={this.handleCancel}
								wrapClassName='custom-modal separate-parts title-center'
								maskClosable={false}
								footer={[
									<Button size='large' data-ripple key='back' onClick={this.handleCancel}>
										Return
									</Button>,
									<Button size='large' data-ripple='rgba(0, 0, 0, 0.2)' key='submit' type='primary' onClick={this.facebookConnect}>
										It's on. Connect me!
									</Button>
								]}
							>
								<main className='pt-20' style={{ lineHeight: '30px' }}>
									<p className='pb-10'>
										Connecting your Collabs account with Facebook will help us to pull the real statistics for your Instagram posts and videos without the need
										of taking any screenshots.
									</p>
									<p>What you need to do is to make sure: </p>
									<ol>
										<li>
											<span>
												The Instagram account that you’re going to use for Collabs is a <strong>business account</strong>.
											</span>
											<a className='ml-5' href='https://help.instagram.com/502981923235522' target='blank'>
												View details
											</a>
										</li>
										<li>
											<span>
												Your Instagram business account above must be <strong>linked to a Facebook Page</strong> that eventually will be used to connect with
												Collabs.
											</span>
											<a className='ml-5' href='https://help.instagram.com/399237934150902' target='blank'>
												View details
											</a>
										</li>
									</ol>
									<p>If these 2 conditions above are all met, then voila! You are ready to be connected!</p>
								</main>
							</Modal>
						</section>
					);
				}}
			</Query>
		);
	}
}
export default withApollo(ConnectWithFacebook);
