import React from 'react';
import { Spin } from 'antd';
import UserSettingsForm from 'components/user-form';
import { Query } from 'react-apollo';
import getCurrentUser from 'graphql/get-current-user.graphql';

class UserSettings extends React.Component {
	render() {
		return (
			<Query query={getCurrentUser}>
				{({ loading, error, data }) => {
					if (loading) return <Spin className='collabspin' />;
					if (error || !data || !data.currentUser) return <p>Could not load user</p>;
					return <UserSettingsForm fields={{ organization: data.currentUser }} />;
				}}
			</Query>
		);
	}
}

export default UserSettings;
