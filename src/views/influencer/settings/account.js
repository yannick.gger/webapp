import { Button, Form, Spin, message } from 'antd';
import AccountSettingsForm from 'components/account-settings-form';
import getUserAccountSettingQuery from 'graphql/get-user-account-setting.graphql';
import updateUserAccountSettingMutation from 'graphql/update-user-account-setting.graphql';
import React from 'react';
import { ApolloConsumer, Query } from 'react-apollo';
import { Elements, injectStripe } from 'react-stripe-elements';
import stripeCountries from 'components/account-settings-form/stripe-countries.json';

const convertDate = ({ day, month, year }) => ({
	day: Number(day),
	month: Number(month),
	year: Number(year)
});

const extractAccountSettingValues = (userAccountSetting) => {
	let stripeValues = {
		business_type: userAccountSetting.type,
		tos_shown_and_accepted: userAccountSetting.stripeTosShownAndAccepted
	};

	if (userAccountSetting.type === 'individual') {
		stripeValues.individual = {
			first_name: userAccountSetting.firstName,
			last_name: userAccountSetting.lastName,
			id_number: userAccountSetting.personalIdNumber,
			address: {
				city: userAccountSetting.addressCity,
				line1: userAccountSetting.addressLine1,
				postal_code: userAccountSetting.addressPostalCode
			},
			phone: `${userAccountSetting.phone.prefix} ${userAccountSetting.phone.number}`,
			ssn_last_4: userAccountSetting.ssnLast4,
			dob: userAccountSetting.dob
		};
		if (userAccountSetting.addressLine2) stripeValues.individual.address.line2 = userAccountSetting.addressLine2;
	} else if (userAccountSetting.type === 'company') {
		stripeValues.company = {
			address: {
				city: userAccountSetting.addressCity,
				line1: userAccountSetting.addressLine1,
				postal_code: userAccountSetting.addressPostalCode
			},
			name: userAccountSetting.businessName,
			phone: `${userAccountSetting.phone.prefix} ${userAccountSetting.phone.number}`,
			tax_id: userAccountSetting.businessTaxId,
			vat_id: userAccountSetting.businessVat
		};
		if (userAccountSetting.addressLine2) stripeValues.company.address.line2 = userAccountSetting.addressLine2;
	}

	return stripeValues;
};

const createStripeAccountToken = async (stripe, userAccountSetting, stripeValues) => {
	if (!stripeCountries.find(({ id }) => id === userAccountSetting.country)) {
		throw Error('Country not supported!');
	}

	const accountResult = await stripe.createToken('account', stripeValues);

	if (accountResult.error) {
		throw accountResult.error.message;
	}

	return accountResult && accountResult.token.id;
};

const createStripePersonToken = async (stripe, person) => {
	const personStripeData = {
		first_name: person.firstName,
		last_name: person.lastName,
		dob: {
			day: person.dob.day,
			month: person.dob.month,
			year: person.dob.year
		},
		relationship: {
			account_opener: person.relationship.accountOpener,
			director: person.relationship.director,
			owner: person.relationship.owner,
			percent_ownership: person.relationship.percentOwnership,
			title: person.relationship.title
		}
	};

	removeEmpty(personStripeData);

	if (person.submitType === 'delete') {
		return {
			id: person.id,
			submitType: 'delete'
		};
	}

	const personResult = await stripe.createToken('person', { person: personStripeData });

	if (personResult.error) {
		console.error('Create token for person error', personResult.error.message);
		throw personResult.error.message;
	}

	return { ...(person.id ? { id: person.id } : {}), stripeToken: personResult && personResult.token.id };
};

const removeEmpty = (obj) => {
	Object.keys(obj).forEach(
		(key) => (obj[key] && typeof obj[key] === 'object' && removeEmpty(obj[key])) || ((obj[key] === undefined || obj[key] === null) && delete obj[key])
	);
	return obj;
};

export const getUserAccountSettingsValues = async (userAccountSetting, stripe) => {
	let stripeValues = extractAccountSettingValues(userAccountSetting);
	let additionalOwners = [];
	removeEmpty(stripeValues);
	const accountToken = await createStripeAccountToken(stripe, userAccountSetting, stripeValues);
	await (userAccountSetting.additionalOwners || [])
		.filter(Boolean)
		.map(({ dob, ...rest }) => ({
			...rest,
			dob: convertDate(dob)
		}))
		.reduce(async (promise, person) => {
			await promise;
			const additionalOwner = await createStripePersonToken(stripe, person);
			additionalOwners.push(additionalOwner);
		}, Promise.resolve());

	const phone = `${userAccountSetting.phone.prefix} ${userAccountSetting.phone.number}`;
	const dob = userAccountSetting.dob ? convertDate(userAccountSetting.dob) : null;

	return {
		...userAccountSetting,
		dob: dob,
		additionalOwners,
		phone: phone,
		stripeToken: accountToken
	};
};

const WrappedAccountSettingsForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const { ...propFields } = props.fields || {};
		const { userAccountSetting } = props.fields;
		let formFields = {};
		if (propFields.userAccountSetting) {
			Object.keys(propFields.userAccountSetting).forEach((propFieldKey) => {
				formFields[`userAccountSetting.${propFieldKey}`] = Form.createFormField({
					value: propFields.userAccountSetting[propFieldKey]
				});
			});
		}

		if (userAccountSetting) {
			if (userAccountSetting.additionalOwners) {
				formFields[`keys`] = Form.createFormField({
					value: Array.apply(null, {
						length: userAccountSetting.additionalOwners.length
					}).map(Number.call, Number)
				});
				let index = 0;
				userAccountSetting.additionalOwners.forEach((additionalOwner) => {
					formFields[`userAccountSetting.additionalOwners[${index}].firstName`] = Form.createFormField({
						value: additionalOwner.firstName
					});
					formFields[`userAccountSetting.additionalOwners[${index}].lastName`] = Form.createFormField({
						value: additionalOwner.lastName
					});
					formFields[`userAccountSetting.additionalOwners[${index}].dob.day`] = Form.createFormField({
						value: additionalOwner.dob && additionalOwner.dob.day
					});
					formFields[`userAccountSetting.additionalOwners[${index}].dob.year`] = Form.createFormField({
						value: additionalOwner.dob && additionalOwner.dob.year
					});
					formFields[`userAccountSetting.additionalOwners[${index}].dob.month`] = Form.createFormField({
						value: additionalOwner.dob && additionalOwner.dob.month
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.accountOpener`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.accountOpener
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.director`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.director
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.owner`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.owner
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.percentOwnership`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.percentOwnership
					});
					formFields[`userAccountSetting.additionalOwners[${index}].relationship.title`] = Form.createFormField({
						value: additionalOwner.relationship && additionalOwner.relationship.title
					});
					formFields[`userAccountSetting.additionalOwners[${index}].id`] = Form.createFormField({
						value: additionalOwner.id
					});
					index = index + 1;
				});
			}
			if (userAccountSetting.phone) {
				const phonePrefix = userAccountSetting.phone ? userAccountSetting.phone.split(' ')[0] : undefined;
				formFields['userAccountSetting.phone.prefix'] = Form.createFormField({
					value: phonePrefix
				});
				formFields['userAccountSetting.phone.number'] = Form.createFormField({
					value: phonePrefix ? userAccountSetting.phone.replace(phonePrefix + ' ', '') : null
				});
			}

			formFields['userAccountSetting.dob.day'] = Form.createFormField({
				value: userAccountSetting.dob && userAccountSetting.dob.day
			});
			formFields['userAccountSetting.dob.year'] = Form.createFormField({
				value: userAccountSetting.dob && userAccountSetting.dob.year
			});
			formFields['userAccountSetting.dob.month'] = Form.createFormField({
				value: userAccountSetting.dob && userAccountSetting.dob.month
			});
		}

		return formFields;
	}
})(AccountSettingsForm);
class Stripe extends React.Component {
	state = {
		loading: false
	};
	handleSubmit = () => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			const userId = this.props.data.currentUser.id;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				this.setState({
					loading: true
				});

				let graphqlValues = null;
				try {
					graphqlValues = {
						...(await getUserAccountSettingsValues(values.userAccountSetting, this.props.stripe)),
						userId: userId
					};
				} catch (e) {
					this.setState({
						loading: false
					});

					message.error(e.message);
					return null;
				}
				const { data, error } = await this.props.apolloClient.mutate({
					mutation: updateUserAccountSettingMutation,
					variables: graphqlValues,
					refetchQueries: [
						{
							query: getUserAccountSettingQuery
						}
					]
				});

				if (error) {
					message.error(error.graphQLErrors && error.graphQLErrors[0] ? error.graphQLErrors[0].message : error.toString());
				}

				if (data.updateUserAccountSetting.errors.length > 0) {
					data.updateUserAccountSetting.errors.forEach((error) => {
						message.error(error.message);
					});
				}

				this.setState({
					loading: false
				});
			});
		};
	};

	handleCancel = () => {
		this.props.closeModal();
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		return (
			<React.Fragment>
				<WrappedAccountSettingsForm
					wrappedComponentRef={this.saveFormRef}
					fields={{
						userAccountSetting: this.props.data.currentUser.userAccountSetting
					}}
				/>
				<Button size='large' key='submit' type='primary' loading={this.state.loading} onClick={this.handleSubmit()}>
					Save
				</Button>
			</React.Fragment>
		);
	}
}

const WithApollo = ({ stripe }) => (
	<ApolloConsumer>
		{(client) => (
			<Query query={getUserAccountSettingQuery}>
				{({ loading, error, data }) => {
					if (loading || !data) return <Spin className='collabspin' />;
					if (error) return <p> Could not load user settings </p>;

					return <Stripe apolloClient={client} data={data} stripe={stripe} />;
				}}
			</Query>
		)}
	</ApolloConsumer>
);
const WithStripe = injectStripe(WithApollo);
const WithElements = (props) => (
	<Elements>
		<WithStripe {...props} />
	</Elements>
);
export default WithElements;
