import { Button, Form, Spin, message } from 'antd';
import DeliveryForm from 'components/delivery-form/';
import getUserAccountSettingQuery from 'graphql/get-user-account-setting.graphql';
import updateUserDeliverySettingMutation from 'graphql/update-user-delivery-setting.graphql';
import React from 'react';
import { ApolloConsumer, Query } from 'react-apollo';

const WrappedDeliveryForm = Form.create({
	onFieldsChange(props, changedFields) {
		if (props.onChange) {
			props.onChange(changedFields);
		}
	},
	mapPropsToFields(props) {
		const { ...propFields } = props.fields || {};

		let formFields = {};
		if (propFields.userDeliverySetting) {
			Object.keys(propFields.userDeliverySetting).forEach((propFieldKey) => {
				formFields[`userDeliverySetting.${propFieldKey}`] = Form.createFormField({ value: propFields.userDeliverySetting[propFieldKey] });
			});
		}

		return formFields;
	}
})(DeliveryForm);

class Delivery extends React.Component {
	state = {
		loading: false
	};
	handleSubmit = () => {
		return (e) => {
			e.preventDefault();
			const form = this.formRef.props.form;
			form.validateFieldsAndScroll(async (err, values) => {
				if (err) return null;
				this.setState({ loading: true });

				this.props.apolloClient
					.mutate({
						mutation: updateUserDeliverySettingMutation,
						variables: { userDeliverySetting: values.userDeliverySetting, userId: this.props.data.currentUser.id },
						refetchQueries: [{ query: getUserAccountSettingQuery }]
					})
					.then(() => {
						this.setState({ loading: false });
					})
					.catch(({ msg }) => {
						message.error(msg);
						this.setState({ loading: false });
					});
			});
		};
	};
	handleCancel = () => {
		this.props.closeModal();
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};
	render() {
		const { data } = this.props;

		return (
			<React.Fragment>
				<WrappedDeliveryForm wrappedComponentRef={this.saveFormRef} fields={data.currentUser} currentUser={data.currentUser} />
				<Button size='large' key='submit' type='primary' onClick={this.handleSubmit()} loading={this.state.loading}>
					Save
				</Button>
			</React.Fragment>
		);
	}
}

const WithAollo = () => (
	<ApolloConsumer>
		{(client) => (
			<Query query={getUserAccountSettingQuery}>
				{({ loading, error, data }) => {
					if (loading || !data) return <Spin className='collabspin' />;
					if (error) return <p>Could not load user settings</p>;

					return <Delivery apolloClient={client} data={data} />;
				}}
			</Query>
		)}
	</ApolloConsumer>
);

export default WithAollo;
