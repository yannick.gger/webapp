import React from 'react';
import { List, Avatar, Popover, Button, Icon } from 'antd';

const data = [
	{
		title: 'SEB Kontot'
	}
];

const popoverContent = (
	<div>
		<Button type='danger'>Remove card</Button>
	</div>
);

export default class InfluencerPaymentSettings extends React.Component {
	render() {
		return (
			<React.Fragment>
				<Button type='primary' className='fr'>
					Add bank account
				</Button>
				<h3 className='mb-0'>Bank account</h3>
				<p>The account you would like to receive your payments</p>
				<List
					className='list-bordered card-details'
					dataSource={data}
					itemLayout='horizontal'
					renderItem={(item) => (
						<List.Item
							className='px-20'
							actions={[
								<Popover key='action1' placement='bottom' content={popoverContent} trigger='click'>
									<Button size='small'>
										<Icon type='ellipsis' />
									</Button>
								</Popover>
							]}
						>
							<Avatar
								shape='square'
								size='large'
								className='mr-20'
								src='https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/a2/5e/f4/a25ef426-8d1e-5b3b-691d-62bea7a8e1be/AppIcon-1x_U007emarketing-85-220-0-5.png/230x0w.jpg'
							/>
							<List.Item.Meta className='mt-5' title={item.title} description='Account number ending in 3423 • Added on 07/16/2018' />
						</List.Item>
					)}
				/>
			</React.Fragment>
		);
	}
}
