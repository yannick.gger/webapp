import React, { useEffect, useState, useContext } from 'react';
import { Divider, Row, Col, Card } from 'antd';

import AccountSettings from './account.js';
import DeliverySettings from './delivery.js';
import UserSettings from './user';

const tabListNoTitle = [
	{
		key: 'account',
		tab: 'Account'
	},
	{
		key: 'delivery',
		tab: 'Delivery'
	},
	{
		key: 'user',
		tab: 'User'
	}
];

const contentListNoTitle = {
	account: <AccountSettings />,
	delivery: <DeliverySettings />,
	user: <UserSettings />
};

const InfluencerSettings = (props: any) => {
	const [noTitleKey, setNoTitleKey] = useState('account');

	const onTabChange = (key, type) => {
		setNoTitleKey(key);
	};

	document.title = `Settings | Collabs`;

	return (
		<Row>
			<Col md={{ span: 18, offset: 3 }}>
				<div className='d-flex align-items-center justify-content-space-between'>
					<div className='mb-30'>
						<h2>Settings</h2>
						<p>Here you can change your password, email, settings for payments and your social media connections.</p>
					</div>
				</div>
				<Divider />
				<Row>
					<Col sm={{ span: 24 }}>
						<Card
							className='ant-card-naked overflow-fix'
							style={{ width: '100%' }}
							tabList={tabListNoTitle}
							activeTabKey={noTitleKey}
							onTabChange={(key) => {
								onTabChange(key, 'noTitleKey');
							}}
						>
							{contentListNoTitle[noTitleKey]}
						</Card>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

export default InfluencerSettings;
