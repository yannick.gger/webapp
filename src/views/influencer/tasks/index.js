import React, { Component } from 'react';
import { Divider } from 'antd';

import Tasks from './tasks.js';

class TasksIndex extends Component {
	render() {
		document.title = `Tasks | Collabs`;

		return (
			<React.Fragment>
				<div className='mb-30'>
					<h2>Tasks</h2>
					<p>Alla saker du behöver göra samlade på ett och samma ställe för en tydlig och enkel överblick</p>
				</div>
				<Divider />
				<Tasks />
			</React.Fragment>
		);
	}
}

export default TasksIndex;
