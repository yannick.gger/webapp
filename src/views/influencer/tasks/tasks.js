import React, { Component } from 'react';
import { Row, Col, Card, Divider } from 'antd';

import TaskDone from 'assets/img/app/task-done.svg';

import JoinOrReject from './join-or-reject.svg';
import PostOnInsta from './post-on-insta.svg';
import Screenshot from './upload-insights.svg';

class Tasks extends Component {
	render() {
		return (
			<React.Fragment>
				<Row>
					<Col sm={{ span: 8 }} className='campaign-name'>
						<h3>My new campaigns name 3</h3>
					</Col>
					<Col sm={{ span: 16 }} className='task-card'>
						<Card hoverable data-ripple='rgba(132,146, 164, 0.2)' className='action-card has-icon-left mb-20' cover={<img src={JoinOrReject} alt='' />}>
							<Card.Meta title='Lämna svar på inbjudan' description={<span className='color-red'>1 dag kvar</span>} />
							<div slyle={{ float: 'right' }} />
							<p className='mt-5 mb-4'>Tacka ja eller nej till att gå med i kampanjen innan den stänger/är full</p>
						</Card>
					</Col>
				</Row>
				<Divider />
				<Row>
					<Col sm={{ span: 8 }} className='campaign-name'>
						<h3>My new campaigns name</h3>
					</Col>
					<Col sm={{ span: 16 }} className='task-card'>
						<Card hoverable data-ripple='rgba(132,146, 164, 0.2)' className='action-card has-icon-left mb-20' cover={<img src={TaskDone} alt='' />}>
							<Card.Meta title='Ladda upp screenshot' description={<span className='color-green'>Klar!</span>} />
							<div slyle={{ float: 'right' }} />
							<p className='mt-5 mb-4'>Ladda upp en screenshot på din posts insights</p>
						</Card>
					</Col>
				</Row>
				<Divider />
				<Row className='mt-30'>
					<Col sm={{ span: 8 }} className='campaign-name'>
						<h3>My new campaigns name 2</h3>
					</Col>
					<Col sm={{ span: 16 }} className='task-card'>
						<Card hoverable data-ripple='rgba(132,146, 164, 0.2)' className='action-card has-icon-left mb-20' cover={<img src={PostOnInsta} alt='' />}>
							<Card.Meta title='Ladda upp video på Stories' description={<span className='color-red'>2 dagar kvar</span>} />
							<div slyle={{ float: 'right' }} />
							<p className='mt-5 mb-4'>Posta video på Instagram Stories</p>
						</Card>
						<Card hoverable data-ripple='rgba(132,146, 164, 0.2)' className='action-card has-icon-left mb-20' cover={<img src={Screenshot} alt='' />}>
							<Card.Meta title='Ladda upp screenshot' description={<span className='color-blue'>12 dagar kvar</span>} />
							<div slyle={{ float: 'right' }} />
							<p className='mt-5 mb-4'>Ladda upp en screenshot på din posts insights</p>
						</Card>
					</Col>
				</Row>
			</React.Fragment>
		);
	}
}

export default Tasks;
