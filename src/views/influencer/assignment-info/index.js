import React, { Component } from 'react';
import { notification, Spin, Modal } from 'antd';
import { Query } from 'react-apollo';
import GenericNotFoundView from 'views/generic-not-found';
import getInfluencerTodo from 'graphql/get-influencer-todo.graphql';
import Detail from './detail';

class AssignmentInfo extends Component {
	state = {
		visible: true
	};

	onClose = () => {
		this.setState({ visible: false });
		this.props.closeModal();
	};

	render() {
		const modalOptions = {
			width: 921,
			footer: null,
			onOk: this.onClose,
			onCancel: this.onClose,
			onUpdate: this.onClose,
			visible: this.state.visible
		};

		return (
			<Query query={getInfluencerTodo} fetchPolicy='cache-and-network' variables={{ assignmentId: this.props.match.params.assignmentId }}>
				{({ loading, error, data }) => {
					if (loading) return <Spin className='collabspin' />;
					if (error) {
						notification.error({
							description: error.toString(),
							message: 'Loading todo unsuccessfully'
						});
					}
					if (!data || !data.todo) {
						return (
							<Modal {...modalOptions}>
								<GenericNotFoundView />
							</Modal>
						);
					}

					return (
						<div>
							<Detail
								{...modalOptions}
								item={data.todo}
								refetchQueries={() => ['getInfluencerTodos']}
								campaignId={this.props.match.params.campaignId}
								assignmentId={this.props.match.params.assignmentId}
								onCompleted={this.props.onCompleted}
							/>
						</div>
					);
				}}
			</Query>
		);
	}
}

export default AssignmentInfo;
