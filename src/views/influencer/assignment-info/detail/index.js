import React, { Component } from 'react';
import { Modal, Card, Row, Col, Button, Divider, notification, Spin, message } from 'antd';
import { apiToMoment } from 'utils';
import classnames from 'classnames';
import { Query, withApollo } from 'react-apollo';
import { extraColor } from 'components/todo-carousel/handle';
import WrappedContentUrlForm from './content-url-form';
import { translate } from 'react-i18next';
import getAssignmentQuery from 'graphql/get-assignment.graphql';
import updateInstagramPostTodo from 'graphql/update-instagram-post-todos.graphql';
import TimeToPost from 'views/influencer/tasks/post-on-insta.svg';

class DetailTodo extends Component {
	state = {
		completed: false
	};

	get renderHeader() {
		return <div className='infor-header'>header</div>;
	}

	renderTime(assignment) {
		const endTime = apiToMoment(assignment.endTime);
		const startTime = apiToMoment(assignment.startTime);

		if (startTime.format('DMMYYYY') === endTime.format('DMMYYYY')) {
			return `in ${startTime.format('D MMMM')}`;
		}

		if (startTime.format('MMYYYY') === endTime.format('MMYYYY')) {
			return `between ${startTime.format('D')}-${endTime.format('D')} ${startTime.format('MMMM')}`;
		}

		if (startTime.format('YYYY') === endTime.format('YYYY')) {
			return `between ${startTime.format('D MMM')}-${endTime.format('D MMM')} in ${startTime.format('YYYY')}`;
		}

		return `between ${startTime.format('D MMMM YYYY')} - ${endTime.format('D MMMM YYYY')}`;
	}

	onCopy = () => {
		const textArea = document.createElement('textarea');
		textArea.value = document.querySelector('.todo-post-instagram-content').innerText;
		document.body.appendChild(textArea);
		textArea.setAttribute('readonly', '');
		textArea.style.position = 'fixed';
		textArea.style.left = '-9999px';
		textArea.select();

		try {
			document.execCommand('copy');
			notification.success({
				message: 'Copy caption and tags',
				description: <div className='white-space-pre-line'>{textArea.value}</div>
			});
		} catch (err) {
			notification.error({
				message: 'Copy caption and tags',
				description: <div className='white-space-pre-line'>{err.toString()}</div>
			});
		}

		document.body.removeChild(textArea);
	};

	onUpdate = (item) => {
		const { client, refetchQueries, assignmentId, campaignId } = this.props;
		const id = Number(item.id);
		const form = this.formRef.props.form;
		form.validateFieldsAndScroll((err, values) => {
			if (err) return null;
			client
				.mutate({
					mutation: updateInstagramPostTodo,
					variables: { ...values, todoPostInstagramId: id },
					refetchQueries
				})
				.then((response) => {
					if (response.data.postInstagram.errors.length === 0) {
						this.setState({ completed: true });
						this.props.onCompleted(campaignId, assignmentId);
					}
					if (response.data.postInstagram.errors.length > 0) {
						response.data.postInstagram.errors.forEach((error) => {
							message.error(error.message);
						});
					}
				});
		});
	};

	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	render() {
		const { className, item, visible, onCancel, onOk, closable, t } = this.props;
		const { completed } = this.state;
		if (!item) {
			return null;
		}

		if (!item.assignment || !item.campaign) {
			notification.warning({
				message: 'Loading todo unsuccessfully'
			});

			return null;
		}

		const { assignment } = item || {};
		const { node: assignmentReview } = assignment.assignmentReviews.edges.find(({ node }) => node.status === 'approved') || {};

		if (!assignmentReview) {
			notification.warning({
				message: 'Loading review of todo unsuccessfully'
			});
			return null;
		}

		const { assignmentReviewMedia } = assignmentReview;
		return (
			<Query query={getAssignmentQuery} variables={{ id: assignment.id }}>
				{({ loading, data }) => {
					if (loading) return <Spin className='collabspin' />;
					return (
						<section>
							{!completed && (
								<Modal
									width={921}
									onOk={onOk}
									footer={null}
									visible={visible}
									closable={closable}
									onCancel={onCancel}
									className={classnames('detail-todo-carousel-components', 'todo-container', className)}
								>
									<Card
										bordered={false}
										title={
											<React.Fragment>
												<img src={TimeToPost} alt='' className='icon-size-30' />
												<span style={{ marginRight: '10px' }}>Time to post:</span> {assignment.name}
											</React.Fragment>
										}
										extra={
											<span className={classnames('tag', { [extraColor(item)]: true })}>
												Post sometime{' '}
												{apiToMoment(item.expiredAt)
													.fromNow()
													.replace(/^in/g, 'in the next')}
											</span>
										}
										className='todo-card-group'
									>
										<WrappedContentUrlForm wrappedComponentRef={this.saveFormRef} assignment={data.assignment} t={t} />
										<Row>
											<Col sm={{ span: 9 }}>
												<p>What to post?</p>
												<h4 className='color-blue'>Image</h4>
												<p>
													This post is {data.assignment.instagramType === 'tiktok' ? 'TikTok' : 'an Instagram story'} and is for the assignment:{' '}
													<span className='color-blue'>{assignment.name}</span>
												</p>
											</Col>
											<Col md={{ span: 14, offset: 1 }}>
												<div className='assignment-content-to-post'>
													{assignmentReviewMedia.map((media, index) => (
														<div className='assignment-review-media-item' key={media.id}>
															<div>
																{media.type === 'photo' && <img alt={assignment.name} src={media.photoThumbnail} width='85' height='85' />}
																{media.type === 'video' && (
																	<video width='85' height='85' controls>
																		<source src={media.video} type='video/mp4' />
																	</video>
																)}
																<Button href={media[media.type]} download={`${assignment.name}-${index}`} type='primary' target='_blank'>
																	Download
																</Button>
															</div>
														</div>
													))}
												</div>
											</Col>
										</Row>
										<Divider />
										<Row>
											<Col md={{ span: 9 }}>
												<h4 className='color-blue'>Caption</h4>
												<p>The caption, tags and mentions that has been approved with the post</p>
												<Button onClick={this.onCopy} className='tag large blue todo-post-copy-content'>
													Copy caption and tags
												</Button>
											</Col>
											<Col md={{ span: 14, offset: 1 }} className='white-space-pre-line todo-post-instagram-content'>
												{assignmentReview.text}
											</Col>
										</Row>
										<Divider />
										<Row>
											<Col md={{ span: 9 }}>
												<h4 className='color-blue'>Date to post</h4>
												<p>At latest {apiToMoment(assignment.endTime).from(apiToMoment(assignment.startTime))} </p>
											</Col>
											<Col md={{ span: 14, offset: 1 }}>
												<p>Anytime {this.renderTime(assignment)}</p>
											</Col>
										</Row>
										<Divider />
										<Button type='primary' data-ripple='' className='ant-btn-block' loading={loading} onClick={() => this.onUpdate(item)}>
											I have posted on&nbsp;{data.assignment.instagramType === 'tiktok' ? 'TikTok' : 'Insta'}, I’m done
										</Button>
									</Card>
								</Modal>
							)}
							{completed && message.info(t('influencer:assignmentReportSuccessMessage', { defaultValue: 'Uppgiften är klar! Bra jobbat!' }))}
						</section>
					);
				}}
			</Query>
		);
	}
}

export default translate('influencer')(withApollo(DetailTodo));
