import React, { Component } from 'react';
import { Form, Input, DatePicker } from 'antd';
import { translate } from 'react-i18next';
import moment from 'moment';

class ContentUrlForm extends Component {
	render() {
		const { t, form, assignment } = this.props;
		const { getFieldDecorator } = form;
		const assignmentType = assignment.instagramType;
		const assignmentEndTime = assignment.endTime;
		const type = assignmentType === 'tiktok' ? 'TikTok' : 'Instagram';

		return (
			<section>
				<Form.Item
					label={t('influencer:assignmentReportFormLinkLabel', { defaultValue: 'Länk till din post på {{assignmentType}}', assignmentType: type })}
					style={{ display: assignmentType !== 'story' ? '' : 'none' }}
				>
					{getFieldDecorator('instagramUrl', {
						initialValue: assignmentType !== 'story' ? null : 'https://www.instagram.com/p/',
						rules: [
							{
								required: true,
								message: t('influencer:assignmentReportFormLinkErrorRequired', {
									defaultValue: 'Skriv in länken till din {{assignmentType}} post',
									assignmentType: type
								})
							},
							{
								pattern:
									assignmentType === 'tiktok'
										? /^https:\/\/(vt\.|)tiktok\.com\/(.*)$/
										: assignmentType === 'reel'
										? /^https:\/\/(www\.|)instagram\.com\/reel\/(.*)$/
										: /^https:\/\/(www\.|)instagram\.com\/p\/(.*)$/,
								message:
									assignmentType === 'tiktok'
										? t('influencer:assignmentReportFormLinkErrorPatternTiktok', { defaultValue: 'Länken ska börja med https://vt.tiktok.com/' })
										: assignmentType !== 'reel'
										? t('influencer:assignmentReportFormLinkErrorPattern', { defaultValue: 'Länken ska börja med https://www.instagram.com/p/' })
										: t('influencer:assignmentReportFormLinkErrorPatternReel', { defaultValue: 'Länken ska börja med https://www.instagram.com/reel/' })
							}
						]
					})(
						<Input
							size='large'
							className='mb-10'
							placeholder={
								assignmentType === 'tiktok'
									? `https://vt.tiktok.com/ZSeeSA2MC/`
									: `https://www.instagram.com/${assignmentType !== 'reel' ? 'p' : 'reel'}/BgtNcp-hEaqwbhhdgFXIJt3D01xy2nnA6C0Lq00`
							}
						/>
					)}
				</Form.Item>
				<Form.Item label={t('influencer:assignmentReportFormDateLabel', { defaultValue: 'Publiceringsdatum' })}>
					{getFieldDecorator('timestamp', {
						initialValue: moment(),
						required: true
					})(<DatePicker disabledDate={(d) => !d || d.isAfter(assignmentEndTime)} />)}
				</Form.Item>
			</section>
		);
	}
}
const WrappedContentUrlForm = Form.create()(ContentUrlForm);
export default translate('influencer')(WrappedContentUrlForm);
