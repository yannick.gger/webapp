import { translate } from 'react-i18next';
import i18n from 'i18next';
import colors from 'styles/variables/colors';

import { ITraffic } from './types';
import Styled from './Traffic.style';
import Grid from 'styles/grid/grid';
import Row from 'styles/grid/row';
import MainContent from 'styles/layout/main-content';
import { TRAFFIC, INSTAGRAM_STORY } from 'constants/data-library';

import useTrafficData from 'hooks/Chart/useTrafficData';

import { CountryDetail, GenderRatio, OverallTotal, CountNumber, OverTimeLine, TrafficSummary } from '../components';

const Traffic = (props: ITraffic) => {
	const {
		getTrafficLinkTotal,
		getTrafficLinkCounts,
		getTrafficBrandMentionTotal,
		getTrafficBrandMentionCounts,
		getTrafficCTRTotal,
		getTrafficCountry,
		getTrafficGenderByPlatforms
	} = useTrafficData();

	return (
		<Row>
			<Styled.SummaryWrapper>
				<TrafficSummary isHoverable={false} />
			</Styled.SummaryWrapper>

			<MainContent>
				<Grid.Container>
					<Grid.Column lg={6} xxl={4}>
						<OverallTotal
							onFetchData={getTrafficLinkTotal}
							title={i18n.t('statistics:chart.defaultTitle.traffic.linkTotal', { defaultValue: 'Overall Link Count' })}
							type={TRAFFIC.OVERALL_LINK}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.traffic.linkCountsByDate', { defaultValue: 'Overtime Link Counts' })}
							onFetchData={getTrafficLinkCounts}
							color={colors.chartLineTraffic}
							fetchParams={{ frequancy: 'daily' }}
							isHoverable={true}
							type={TRAFFIC.OVERTIME_LINK}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverallTotal
							onFetchData={getTrafficBrandMentionTotal}
							title={i18n.t('statistics:chart.defaultTitle.traffic.brandMentionTotal', { defaultValue: 'Overall Brand Count' })}
							type={TRAFFIC.OVERALL_LINK}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.traffic.brandMentionCountsByDate', { defaultValue: 'Overtime Brand Counts' })}
							onFetchData={getTrafficBrandMentionCounts}
							color={colors.chartLineTraffic}
							fetchParams={{ frequancy: 'daily' }}
							isHoverable={true}
							type={TRAFFIC.OVERTIME_LINK}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getTrafficCTRTotal}
							title={i18n.t('statistics:chart.defaultTitle.traffic.ctr', { defaultValue: 'CTR' })}
							type={TRAFFIC.CTR}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<GenderRatio
							title={i18n.t('statistics:chart.defaultTitle.taffic.genderRatio', { defaultValue: 'Gender Ratio' })}
							onFetchData={getTrafficGenderByPlatforms}
							fetchParams={{ platforms: INSTAGRAM_STORY }}
							type={TRAFFIC.GENDER}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountryDetail
							title={i18n.t('statistics:chart.defaultTitle.traffic.geographicCountryBar', { defaultValue: 'Traffic by Countries' })}
							onFetchData={getTrafficCountry}
							type={TRAFFIC.COUNTRY}
						/>
					</Grid.Column>
				</Grid.Container>
			</MainContent>
		</Row>
	);
};

export default translate('statistics')(Traffic);
