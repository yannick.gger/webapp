import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';

const SummaryWrapper = styled.div`
	margin-bottom: ${guttersWithRem.xxxl};
`;

const Styled = {
	SummaryWrapper
};

export default Styled;
