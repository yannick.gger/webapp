import { WithNamespaces } from 'react-i18next';

export interface IEngagement extends WithNamespaces {
	props?: any;
}

export type EngagementTotalData = {
	comments: number;
	followers: number;
	impressions: number;
	likes: number;
	reach: number;
};
