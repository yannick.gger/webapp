import { translate } from 'react-i18next';
import i18n from 'i18next';

import { IEngagement } from './types';
import Styled from './Engagement.style';
import Grid from 'styles/grid/grid';
import Row from 'styles/grid/row';
import MainContent from 'styles/layout/main-content';
import { CountNumber, InfluencerPerformance, EngagementSummary } from '../components';

import useEngagementData from 'hooks/Chart/useEngagementData';
import useInfluencerData from 'hooks/Chart/useInfluencerData';
import { INSTAGRAM_STORY, ENGAGEMENT } from 'constants/data-library';

const Engagement = (props: IEngagement) => {
	const { getEngagementRate, getEngagementActualRate, getEngagementLikes, getEngagementComments, getEngagementImpressionsByPlatforms } = useEngagementData();
	const { getInfluencerTotal } = useInfluencerData();

	return (
		<Row>
			<Styled.SummaryWrapper>
				<EngagementSummary isHoverable={false} />
			</Styled.SummaryWrapper>

			<MainContent>
				<Grid.Container>
					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getEngagementRate}
							title={i18n.t('statistics:chart.defaultTitle.engagement.engagementRate', { defaultValue: 'Overall Engagement Rate' })}
							type={ENGAGEMENT.OVERALL_RATE}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getEngagementActualRate}
							title={i18n.t('statistics:chart.defaultTitle.engagement.engagementActaulRate', { defaultValue: 'Overall Engagement Actual Rate' })}
							type={ENGAGEMENT.OVERALL_ACTUAL_RATE}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getEngagementLikes}
							title={i18n.t('statistics:chart.defaultTitle.engagement.engagementLikes', { defaultValue: 'Overall Likes' })}
							type={ENGAGEMENT.OVERALL_LIKES}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getEngagementComments}
							title={i18n.t('statistics:chart.defaultTitle.engagement.engagementComments', { defaultValue: 'Overall Comments' })}
							type={ENGAGEMENT.OVERALL_COMMENTS}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getEngagementImpressionsByPlatforms}
							title={i18n.t('statistics:chart.defaultTitle.engagement.engagementStoryView', { defaultValue: 'Overall Story View' })}
							fetchParams={{ platforms: INSTAGRAM_STORY }}
							type={ENGAGEMENT.OVERALL_STORY_VIEW}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<InfluencerPerformance
							onFetchData={getInfluencerTotal}
							title={i18n.t('statistics:chart.defaultTitle.engagement.influencer', { defaultValue: 'Influencer engagement' })}
							isTopThree={true}
							type={ENGAGEMENT.INFLUENCER}
							valueType='engagement'
						/>
					</Grid.Column>
				</Grid.Container>
			</MainContent>
		</Row>
	);
};

export default translate('statistics')(Engagement);
