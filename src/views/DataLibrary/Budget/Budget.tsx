import { translate } from 'react-i18next';
import i18n from 'i18next';
import colors from 'styles/variables/colors';
import { useState } from 'react';

import { IBudget } from './types';
import Styled from './Budget.style';
import Grid from 'styles/grid/grid';
import Row from 'styles/grid/row';
import MainContent from 'styles/layout/main-content';
import { CountNumber, OverTimeLine, CountryBudgetDetail, BudgetSummary } from '../components';

import useBudgetData from 'hooks/Chart/useBudgetData';
import { BUDGET } from 'constants/data-library';

const Budget = (props: IBudget) => {
	const [selectedCurrency, setSelectedCurrency] = useState('SEK');

	const { getBudgetTotal, getBudgets, getBudgetsByCountry, getBudgetCPM, getBudgetCPC } = useBudgetData();

	// @todo i18next
	return (
		<Row>
			<Styled.SummaryWrapper>
				<BudgetSummary isHoverable={false} />
			</Styled.SummaryWrapper>

			<MainContent>
				<Grid.Container>
					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getBudgetTotal}
							fetchParams={{ currency: selectedCurrency }}
							title={i18n.t('statistics:chart.defaultTitle.budget.overall', { defaultValue: 'Budget Overall' })}
							type={BUDGET.OVERALL}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							onFetchData={getBudgets}
							fetchParams={{ frequancy: 'daily', currency: selectedCurrency }}
							color={colors.chartLineBudget}
							title={i18n.t('statistics:chart.defaultTitle.budget.overtime', { defaultValue: 'Budget Overtime' })}
							isHoverable={true}
							type={BUDGET.OVERTIME}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountryBudgetDetail
							onFetchData={getBudgetsByCountry}
							fetchParams={{ currency: selectedCurrency }}
							title={i18n.t('statistics:chart.defaultTitle.budget.overtimeByMarket', { defaultValue: 'Budget spend by country' })}
							type={BUDGET.OVERALL_MARKETS}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getBudgetCPM}
							fetchParams={{ currency: selectedCurrency }}
							title={i18n.t('statistics:chart.defaultTitle.budget.overallCPM', { defaultValue: 'Budget CPM' })}
							type={BUDGET.OVERALL_CPM}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getBudgetCPC}
							fetchParams={{ currency: selectedCurrency }}
							title={i18n.t('statistics:chart.defaultTitle.budget.overallCPC', { defaultValue: 'Budget CPC' })}
							type={BUDGET.OVERALL_CPC}
						/>
					</Grid.Column>
				</Grid.Container>
			</MainContent>
		</Row>
	);
};

export default translate('statistics')(Budget);
