import { translate } from 'react-i18next';
import i18n from 'i18next';
import colors from 'styles/variables/colors';

import { IImpressions } from './types';
import Styled from './Impressions.style';
import { INSTAGRAM_POST, INSTAGRAM_STORY, IMPRESSIONS } from 'constants/data-library';

import useImpressionData from 'hooks/Chart/useImpressionData';
import useInfluencerData from 'hooks/Chart/useInfluencerData';

import Grid from 'styles/grid/grid';
import Row from 'styles/grid/row';
import MainContent from 'styles/layout/main-content';
import { CountryDetail, GenderRatio, InfluencerPerformance, MediaObjectDetail, OverallTotal, OverTimeLine, ImpressionsSummary } from '../components';

const Impressions = (props: IImpressions) => {
	const {
		getImpressionTotal,
		getImpressionTotalByPlatforms,
		getImpressionCounts,
		getImpressionCountsByPlatforms,
		getImpressionGender,
		getImpressionCountry
	} = useImpressionData();
	const { getInfluencerTotal } = useInfluencerData();
	return (
		<Row>
			<Styled.SummaryWrapper>
				<ImpressionsSummary isHoverable={false} />
			</Styled.SummaryWrapper>
			<MainContent>
				<Grid.Container>
					<Grid.Column lg={6} xxl={4}>
						<OverallTotal
							onFetchData={getImpressionTotal}
							title={i18n.t('statistics:chart.defaultTitle.impression.totalCount', { defaultValue: 'Total Count of Impressions' })}
							type={IMPRESSIONS.OVERALL}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<MediaObjectDetail
							onFetchData={getImpressionTotalByPlatforms}
							title={i18n.t('statistics:chart.defaultTitle.impression.totalByPlatforms', { defaultValue: 'Impressions by media objects' })}
							type={IMPRESSIONS.OVERALL_MEDIA_OBJECTS}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.impression.countLineByDate', { defaultValue: 'Count of Impressions by date' })}
							onFetchData={getImpressionCounts}
							color={colors.chartLineImpressions}
							fetchParams={{ frequancy: 'daily' }}
							isHoverable={true}
							type={IMPRESSIONS.OVERTIME_ALL}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.impression.postCountLineByDate', { defaultValue: 'Count of Post Impressions by date' })}
							onFetchData={getImpressionCountsByPlatforms}
							color={colors.chartLineImpressions}
							fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_POST }}
							isHoverable={true}
							type={IMPRESSIONS.OVERTIME_POST}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.impression.storyCountLineByDate', { defaultValue: 'Count of Story Impressions by date' })}
							onFetchData={getImpressionCountsByPlatforms}
							color={colors.chartLineImpressions}
							fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_STORY }}
							isHoverable={true}
							type={IMPRESSIONS.OVERTIME_STORY}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<InfluencerPerformance
							title='Influencer impressions'
							onFetchData={getInfluencerTotal}
							isTopThree={true}
							type={IMPRESSIONS.INFLUENCER}
							valueType='impressions'
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<GenderRatio
							title={i18n.t('statistics:chart.defaultTitle.impression.genderRatio', { defaultValue: 'Gender Ratio' })}
							onFetchData={getImpressionGender}
							type={IMPRESSIONS.GENDER}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountryDetail
							title={i18n.t('statistics:chart.defaultTitle.impression.geographicCountryBar', { defaultValue: 'Impression by Countries' })}
							onFetchData={getImpressionCountry}
							type={IMPRESSIONS.COUNTRY}
						/>
					</Grid.Column>
				</Grid.Container>
			</MainContent>
		</Row>
	);
};

export default translate('statistics')(Impressions);
