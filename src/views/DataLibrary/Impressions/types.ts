import { WithNamespaces } from 'react-i18next';

export interface IImpressions extends WithNamespaces {
	props?: any;
}
