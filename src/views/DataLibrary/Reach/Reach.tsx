import { translate } from 'react-i18next';
import i18n from 'i18next';
import colors from 'styles/variables/colors';

import { IReach } from './types';
import Grid from 'styles/grid/grid';
import Row from 'styles/grid/row';
import { INSTAGRAM_POST, INSTAGRAM_STORY, REACH } from 'constants/data-library';

import { OverallTotal, CountNumber, GenderRatio, CountryDetail, MediaObjectDetail, OverTimeLine, ReachSummary, InfluencerPerformance } from '../components';
import useReachData from 'hooks/Chart/useReachData';
import useInfluencerData from 'hooks/Chart/useInfluencerData';
import MainContent from 'styles/layout/main-content';
import Styled from './Reach.style';

const Reach = (props: IReach) => {
	const {
		getReachTotal,
		getReachTotalByPlatforms,
		getGrossReachTotal,
		getReachCounts,
		getReachCountsByPlatforms,
		getReachGender,
		getReachCountry
	} = useReachData();
	const { getInfluencerTotal } = useInfluencerData();

	return (
		<Row>
			<Styled.SummaryWrapper>
				<ReachSummary isHoverable={false} />
			</Styled.SummaryWrapper>
			<MainContent>
				<Grid.Container>
					<Grid.Column lg={6} xxl={4}>
						<OverallTotal
							onFetchData={getReachTotal}
							title={i18n.t('statistics:chart.defaultTitle.reach.totalCount', { defaultValue: 'Total Count of Reach' })}
							type={REACH.OVERALL}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountNumber
							onFetchData={getGrossReachTotal}
							title={i18n.t('statistics:chart.defaultTitle.reach.gross', { defaultValue: 'Total Gross of Reach' })}
							type={REACH.GROSS}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<MediaObjectDetail
							onFetchData={getReachTotalByPlatforms}
							title={i18n.t('statistics:chart.defaultTitle.reach.totalByPlatforms', { defaultValue: 'Reach by media objects' })}
							type={REACH.OVERALL_MEDIA_OBJECTS}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.reach.countLineByDate', { defaultValue: 'Reach Over Time' })}
							onFetchData={getReachCounts}
							color={colors.chartLineReach}
							fetchParams={{ frequancy: 'daily' }}
							isHoverable={true}
							type={REACH.OVERTIME_ALL}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.reach.postCountLineByDate', { defaultValue: 'Reach Over Time - Post' })}
							onFetchData={getReachCountsByPlatforms}
							color={colors.chartLineReach}
							fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_POST }}
							isHoverable={true}
							type={REACH.OVERTIME_POST}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<OverTimeLine
							title={i18n.t('statistics:chart.defaultTitle.reach.storyCountLineByDate', { defaultValue: 'Reach Over Time - Story' })}
							onFetchData={getReachCountsByPlatforms}
							color={colors.chartLineReach}
							fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_STORY }}
							isHoverable={true}
							type={REACH.OVERTIME_STORY}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<InfluencerPerformance title='Influencer reach' onFetchData={getInfluencerTotal} isTopThree={true} type={REACH.INFLUENCER} valueType='reach' />
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<GenderRatio
							title={i18n.t('statistics:chart.defaultTitle.reach.genderRatio', { defaultValue: 'Gender Ratio' })}
							onFetchData={getReachGender}
							type={REACH.GENDER}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountryDetail
							title={i18n.t('statistics:chart.defaultTitle.reach.geographicCountryBar', { defaultValue: 'Reach by Countries' })}
							onFetchData={getReachCountry}
							type={REACH.COUNTRY}
						/>
					</Grid.Column>
				</Grid.Container>
			</MainContent>
		</Row>
	);
};

export default translate('statistics')(Reach);
