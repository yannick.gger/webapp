import withTheme from 'hocs/with-theme';

import Styled from './DataLibrary.style';
import PageHeader from 'components/PageHeader';
import BrowserStorage, { StorageType } from 'shared/helpers/BrowserStorage/BrowserStorage';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import Reach from './Reach';
import Impression from './Impressions';
import Engagement from './Engagement';
import Audience from './Audience';
import Budget from './Budget';
import Traffic from './Traffic';
import DataLibraryDashboard from './Dashboard';
import NavTap from './components/NavTap';

const DataLibrary = () => {
	const local = new BrowserStorage(StorageType.LOCAL);
	const userInfo = JSON.parse(local.getItem('user') || '');

	let { path } = useRouteMatch();

	return (
		<Styled.Wrapper>
			<Styled.Header>
				<PageHeader headline={`Good morning, ${userInfo.Username}`} />
				<NavTap />
			</Styled.Header>
			<div>
				<Switch>
					<Route path={`${path}/reach`}>
						<Reach />
					</Route>
					<Route path={`${path}/impressions`}>
						<Impression />
					</Route>
					<Route path={`${path}/traffic`}>
						<Traffic />
					</Route>
					<Route path={`${path}/audience`}>
						<Audience />
					</Route>
					<Route path={`${path}/budget`}>
						<Budget />
					</Route>
					<Route path={`${path}/engagement`}>
						<Engagement />
					</Route>
					<Route path={path}>
						<DataLibraryDashboard />
					</Route>
				</Switch>
			</div>
		</Styled.Wrapper>
	);
};

export default withTheme(DataLibrary, 'dataLibrary');
