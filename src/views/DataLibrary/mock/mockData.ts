export const generateCountsPerDate = (props: { from: any; to: any; platforms?: any; frequancy: any }) =>
	new Promise((resolve, reject) => {
		const getData = () => {
			const arr = new Array(52).fill('').map((_, index) => ({
				type: 'date-and-count',
				id: '1646697600',
				attributes: {
					year: 2022,
					month: 0,
					date: 1 + index,
					count: Math.floor(Math.random() * 10000)
				}
			}));
			return { data: { data: arr } };
		};
		resolve(getData());
	});

export const generateTotalCount = (props: { from: any; to: any; platforms?: any }) =>
	new Promise((resolve, rejects) => {
		const getData = () => {
			return { data: { total: Math.ceil(Math.random() * 100000) + 100 } };
		};
		resolve(getData());
	});

export const generateGrossNumber = (props: { from: any; to: any; platforms?: any }) =>
	new Promise((resolve, rejects) => {
		const getData = () => {
			return { data: { total: Math.ceil(Math.random() * 1000000) + 10000 } };
		};
		resolve(getData());
	});

export const impressionsCountsPerMonth = {
	data: [
		{
			type: 'year-month-and-count',
			id: '1646697600',
			attributes: {
				year: 2022,
				month: 2,
				count: 1234
			}
		}
	]
};

export const impressionsCountsPerWeek = {
	data: [
		{
			type: 'year-week-and-count',
			id: '1646697600',
			attributes: {
				year: 2022,
				week: 1,
				count: 5127
			}
		}
	]
};

export const impressionGenderRatio = {
	data: [{ label: 'female', count: 1000 }, { label: 'male', count: 800 }]
};

export const impressionGeographicCount = {
	from: '2022-01-01',
	to: '2022-03-31',
	data: [{ label: 'Sweden', data: [40, 70, 30] }, { label: 'Korea', data: [80, 90, 70] }, { label: 'France', data: [60, 80, 20] }]
};

export const countriesMockData = [
	{
		name: 'Sweden',
		alpha2code: 'SE',
		alpha3code: 'SWE',
		followers: 31567
	},
	{
		name: 'Denmark',
		alpha2code: 'DK',
		alpha3code: 'DNK',
		followers: 12323
	},
	{
		name: 'Norway',
		alpha2code: 'NO',
		alpha3code: 'NOR',
		followers: 12323
	}
];
