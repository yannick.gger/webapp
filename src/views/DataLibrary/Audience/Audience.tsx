import { translate } from 'react-i18next';
import i18n from 'i18next';

import { IAudience } from './types';

import Grid from 'styles/grid/grid';
import Row from 'styles/grid/row';
import MainContent from 'styles/layout/main-content';
import { AgeDetail, CountryDetail, GenderRatio } from '../components';
import useAudienceData from 'hooks/Chart/useAudienceData';
import { AUDIENCE } from 'constants/data-library';

const Audience = (props: IAudience) => {
	const { getAudienceAge, getAudienceCountry, getAudienceGender } = useAudienceData();

	return (
		<Row>
			<MainContent>
				<Grid.Container>
					<Grid.Column lg={6} xxl={4}>
						<AgeDetail
							title={i18n.t('statistics:chart.defaultTitle.audience.age', { defaultValue: 'Audience Age' })}
							onFetchData={getAudienceAge}
							type={AUDIENCE.AGE}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<CountryDetail
							title={i18n.t('statistics:chart.defaultTitle.audience.country', { defaultValue: 'Audience Countries' })}
							onFetchData={getAudienceCountry}
							type={AUDIENCE.COUNTRY}
						/>
					</Grid.Column>

					<Grid.Column lg={6} xxl={4}>
						<GenderRatio
							title={i18n.t('statistics:chart.defaultTitle.audience.gender', { defaultValue: 'Audience Gender' })}
							onFetchData={getAudienceGender}
							type={AUDIENCE.GENDER}
						/>
					</Grid.Column>
				</Grid.Container>
			</MainContent>
		</Row>
	);
};

export default translate('statistics')(Audience);
