import { WithNamespaces } from 'react-i18next';

export interface IAudience extends WithNamespaces {
	props?: any;
}
