import styled from 'styled-components';

const Wrapper = styled.div`
	width: 100%;
	max-width: 1920px;

	margin: 0 auto;
	padding-bottom: 30px;
`;

const Header = styled.div``;

const Styled = {
	Wrapper,
	Header
};

export default Styled;
