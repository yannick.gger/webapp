import styled from 'styled-components';
import { IChartCardStyle } from './types';
import colors from 'styles/variables/colors';
import BaseCard from 'components/Card';
import typography from 'styles/variables/typography';
import { breakpoints } from 'styles/variables/media-queries';

const titleTypograph = typography.headings;
const labelTypograph = typography.label;

const Card = styled(BaseCard)<{ border?: string }>`
	min-height: 350px;
	border: ${(props) => props.border};

	@media (max-width: ${breakpoints.sm}) {
		min-height: 0;
	}
`;

const Icon = styled.img`
	width: 20px;
	height: 20px;
`;

const Title = styled.span`
	font-family: ${titleTypograph.fontFamily};
	font-size: ${titleTypograph.h5.fontSize};
	font-weight: ${titleTypograph.h5.fontWeight};
`;

const Buttons = styled.div`
	display: flex;
	gap: 8px;
`;

const Button = styled.button<IChartCardStyle>`
	font-family: ${labelTypograph.fontFamily};
	flex: 1;
	border: 1px solid transparent;
	background-color: ${(props) => props.bgColor || colors.buttonGray};

	font-size: 1rem;
	min-height: 35px;

	&:hover {
		border: 1px solid ${colors.borderGray};
	}

	&.selected {
		border: 1px solid ${colors.borderGray};
	}
`;

const Container = styled.div`
	width: 100%;
	overflow: auto;
`;

const CalendarContainer = styled.div`
	&.selected {
		border: 1px solid ${colors.borderGray};
	}
`;

const DateFilterWrapper = styled.div`
	width: 100%;
	min-width: 345px;
	height: 65px;
	margin-bottom: 15px;

	&:after {
		content: '';
		display: inline-block;
		vertical-align: bottom;
		height: 1em;
		width: 100%;
		border-top: 1px solid ${colors.buttonGray};
	}
`;

const DateFilterLabel = styled.label`
	font-family: ${labelTypograph.fontFamily};
	font-size: 1rem;
	line-height: ${labelTypograph.medium.lineHeight};
`;

const DateFilters = styled(Buttons)`
	width: 100%;
`;

const Styled = {
	Card,
	Icon,
	Title,
	Buttons,
	Button,
	Container,
	CalendarContainer,
	DateFilterWrapper,
	DateFilterLabel,
	DateFilters
};

export default Styled;
