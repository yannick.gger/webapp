export interface IChartCardStyle {
	bgColor?: string;
}

export interface IChartCard extends IChartCardStyle {
	size?: 'small' | 'medium' | 'large';
	title: string;
	from: Date | undefined;
	to: Date | undefined;
	setFrom: (date: Date) => void;
	setTo: (date: Date) => void;
	children: any;
	period?: 'today' | 'week' | 'month' | string;
	dashboardPeriod?: string;
	isDashboard?: boolean;
	chartType: string;
	onRemoveItem?: () => void;
	cardBorder?: string;
}
