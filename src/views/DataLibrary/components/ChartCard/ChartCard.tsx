import { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { Menu, Dropdown } from 'antd';

import Card from 'components/Card';
import Styled from './ChartCard.style';
import { formatDate, getMonthBeforeDate } from 'shared/helpers/Chart/chart-util';

import { FEATURE_FLAG_DATA_LIBRARY_CARD_PIN_BUTTON, FEATURE_FLAG_DATA_LIBRARY_CARD_MORE_BUTTON } from 'constants/feature-flag-keys';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import DashboardSettingModal from '../DashboardSettingModal';
import MeatballMenu from 'components/MeatballMenu';

import Pin from 'assets/icons/pin.svg';
import Calendar from 'assets/icons/calendar.svg';
import { IChartCard } from './types';
import { TODAY, WEEK, MONTH } from 'constants/data-library';

const ChartCard = (props: IChartCard) => {
	const [selected, setSelcted] = useState<'today' | 'week' | 'month' | undefined>();
	const [isEnabled] = useFeatureToggle();
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [isAppendChart, setIsAppendChart] = useState(false);
	const [selectedPeriod, setSelectedPeriod] = useState<string>();

	const changePeriodHandler = (dates: any) => {
		const [start, end] = dates;
		props.setFrom(start);
		props.setTo(end);
		setSelcted(undefined);
	};

	const selectTodayHandler = () => {
		props.setFrom(new Date());
		props.setTo(new Date());
		setSelcted(TODAY);
	};

	const selectWeekHandler = () => {
		const today = new Date();
		const latestWeek = new Date();
		latestWeek.setDate(today.getDate() - 6);

		props.setFrom(latestWeek);
		props.setTo(today);
		setSelcted(WEEK);
	};

	const selectMonthHandler = () => {
		const today = new Date();
		const lastestMonth = getMonthBeforeDate(new Date());

		props.setFrom(lastestMonth);
		props.setTo(today);
		setSelcted(MONTH);
	};

	const addNewDashboardHandler = () => {
		setIsAppendChart(false);
		setIsModalOpen(true);
	};

	const addExistingDashboardHandler = () => {
		setIsAppendChart(true);
		setIsModalOpen(true);
	};

	const pinMenu = props.isDashboard ? (
		<Menu>
			<Menu.Item onClick={props.onRemoveItem}>Remove from dashboard</Menu.Item>
		</Menu>
	) : (
		<Menu>
			<Menu.Item onClick={addNewDashboardHandler}>Add to a new dashboard</Menu.Item>
			<Menu.Item onClick={addExistingDashboardHandler}>Add to existing dashboard</Menu.Item>
		</Menu>
	);

	useEffect(() => {
		if (props.dashboardPeriod) {
			setSelcted(undefined);
		} else if (props.period) {
			if (props.period === TODAY || props.period === WEEK || props.period === MONTH) {
				setSelcted(props.period);
			} else {
				setSelcted(undefined);
			}
		}
	}, []);

	useEffect(() => {
		if (selected === TODAY || selected === WEEK || selected === MONTH) {
			setSelectedPeriod(selected);
		} else {
			if (props.from && props.to) {
				setSelectedPeriod(`${formatDate(props.from)}~${formatDate(props.to)}`);
			}
		}
	}, [selected, props.from, props.to]);

	return (
		<>
			<Styled.Card border={props.cardBorder}>
				<Card.Header display='flex' justifyContent='space-between' alignItems='center' margin='0 0 6px 0'>
					<Styled.Title>{props.title}</Styled.Title>
					<Styled.Buttons>
						{isEnabled(FEATURE_FLAG_DATA_LIBRARY_CARD_MORE_BUTTON) && <MeatballMenu />}
						{isEnabled(FEATURE_FLAG_DATA_LIBRARY_CARD_PIN_BUTTON) && (
							<Dropdown overlay={pinMenu}>
								<Styled.Button>
									<Styled.Icon src={Pin} />
								</Styled.Button>
							</Dropdown>
						)}
					</Styled.Buttons>
				</Card.Header>
				<Card.Body>
					<Styled.Container>
						<Styled.DateFilterWrapper>
							<Styled.DateFilterLabel>Date</Styled.DateFilterLabel>
							<Styled.DateFilters>
								<Styled.Button onClick={selectTodayHandler} className={selected === TODAY ? 'selected' : ''}>
									Today
								</Styled.Button>
								<Styled.Button onClick={selectWeekHandler} className={selected === WEEK ? 'selected' : ''}>
									1 week
								</Styled.Button>
								<Styled.Button onClick={selectMonthHandler} className={selected === MONTH ? 'selected' : ''}>
									1 month
								</Styled.Button>
								<Styled.CalendarContainer className={selected === undefined ? 'selected' : ''}>
									<DatePicker
										selected={props.from}
										onChange={changePeriodHandler}
										startDate={props.from}
										endDate={props.to}
										selectsRange
										filterDate={(date) => {
											return new Date() > date;
										}}
										customInput={
											<Styled.Button>
												<Styled.Icon src={Calendar} />
											</Styled.Button>
										}
									/>
								</Styled.CalendarContainer>
							</Styled.DateFilters>
						</Styled.DateFilterWrapper>
						{props.children}
					</Styled.Container>
				</Card.Body>
			</Styled.Card>
			{isModalOpen ? (
				<DashboardSettingModal
					isVisible={isModalOpen}
					isAppendChart={isAppendChart}
					onCloseModal={setIsModalOpen}
					selectedPeriod={selectedPeriod}
					type={props.chartType}
				/>
			) : null}
		</>
	);
};

export default ChartCard;
