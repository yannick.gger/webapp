import { ComponentStory, ComponentMeta } from '@storybook/react';
import Card from './ChartCard';
import { IChartCard } from './types';

export default {
	title: 'Card',
	component: Card,
	argTypes: {
		title: {
			control: { type: 'text' }
		},
		children: {
			control: { type: 'text' }
		}
	}
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args: IChartCard) => <Card {...args} />;

export const ChartCard = Template.bind({});

ChartCard.args = {
	title: 'Title',
	children: 'Test'
};
