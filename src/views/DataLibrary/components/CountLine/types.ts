export interface ICountLineProp {
	onRemove?: () => void;
	labels?: string[];
	datasets?: any;
	title: any;
	from?: Date;
	to?: Date;
	onFetchData?: (props: any) => Promise<any>;
	fetchParams?: any;
	color: string;
	type: string;
}
