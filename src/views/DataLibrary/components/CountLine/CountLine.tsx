import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';

import ChartCard from '../ChartCard';
import { Line } from 'components/Chart';
import { ICountLineProp } from './types';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import { getMonthBeforeDate, transformToLineChartData } from 'shared/helpers/Chart/chart-util';

const CountLine = (props: ICountLineProp) => {
	const [lineData, setLineData] = useState<{ labels: string[]; datasets: any }>({
		labels: [],
		datasets: {}
	});
	const [from, setFrom] = useState<Date>(getMonthBeforeDate(new Date()));
	const [to, setTo] = useState<Date>(new Date());
	const [loading, setLoading] = useState(true);

	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		if (props.from && props.to) {
			setFrom(props.from);
			setTo(props.to);
		}
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.onFetchData) {
			setLoading(true);
			props
				.onFetchData({ from: new Date(from), to: new Date(to), ...props.fetchParams })
				.then((res) => {
					setLineData(transformToLineChartData({ arr: res, color: props.color }));
				})
				.catch((err) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		}
	}, [from, to]);

	return (
		<ChartCard title={props.title} from={from} setFrom={setFrom} to={to} setTo={setTo} chartType={props.type}>
			{loading ? (
				<LoadingSpinner size='md' />
			) : lineData.labels.length ? (
				<Line labels={lineData.labels} datasets={lineData.datasets} pointRadius={0} lineTension={0} borderWidth={1} />
			) : null}
		</ChartCard>
	);
};

export default CountLine;
