import Styled from './AgeSummaryList.style';
import { BarItem, Bar } from '../BarItem';
import { IAgeSummaryList, AgeData } from './types';
import colors from 'styles/variables/colors';

const AgeSummaryList = (props: IAgeSummaryList) => {
	let filteredAges: AgeData[] = [];

	if (props.items.length) {
		filteredAges = props.items.sort((a: any, b: any) => b.value - a.value);
	}

	const getColors = (index: number) => {
		let colorSet = { containerColor: '#ccc', fillerColor: '#000' };
		switch (index) {
			case 0:
				colorSet.containerColor = colors.chartPrimaryLight;
				colorSet.fillerColor = colors.chartPrimary;
				break;
			case 1:
				colorSet.containerColor = colors.chartSecondaryLight;
				colorSet.fillerColor = colors.chartSecondary;
				break;
			case 2:
				colorSet.containerColor = colors.buttonGray;
				colorSet.fillerColor = colors.gray5;
				break;
			default:
				colorSet.containerColor = colors.buttonGray;
				colorSet.fillerColor = colors.gray5;
		}

		return colorSet;
	};

	if (props.isTopThree) {
		filteredAges = filteredAges.filter((item: any, index: number) => {
			if (index < 3) {
				return item;
			}
		});
	}

	filteredAges = filteredAges.map((item, index) => {
		return { ...item, ...getColors(index) };
	});

	return (
		<Styled.Wrapper>
			{filteredAges.length > 0 ? (
				filteredAges.map((item: AgeData) => {
					const percent = `${Math.floor((item.value / props.totalFollowers) * 100)}%`;
					let value = 0;
					if (props.sign === 'K') {
						value = item.value > 0 ? Math.floor(item.value / 1000) : 0;
					} else if (props.sign === '%') {
						value = item.value > 0 ? Math.floor((item.value / props.totalFollowers) * 100) : 0;
					} else {
						value = item.value;
					}
					return (
						<BarItem label={item.age} value={value} sign={props.sign} key={item.age}>
							<Bar total={props.totalFollowers} percent={percent} containerBg={item.containerColor} fillerBg={item.fillerColor} />
						</BarItem>
					);
				})
			) : (
				<Styled.CenteredWrapper>
					<span>N/A</span>
				</Styled.CenteredWrapper>
			)}
		</Styled.Wrapper>
	);
};

export default AgeSummaryList;
