export type AgeData = {
	age: string;
	value: number;
	containerColor?: string;
	fillerColor?: string;
};

export interface IAgeSummaryList {
	items: AgeData[];
	isTopThree?: boolean;
	totalFollowers: number;
	sign?: 'K' | '%';
}
