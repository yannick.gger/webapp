import styled from 'styled-components';
import typography from 'styles/variables/typography';

const listTypograph = typography.list;

const InnerCenteredWrapper = styled.div`
	min-height: 170px;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const InnerWrapper = styled.div`
	min-height: 170px;
	min-width: 345px;
	flex: 1;
	display: flex;
	flex-direction: column;
	row-gap: 10px;

	& > span {
		font-family: ${listTypograph.fontFamily};
		font-size: ${listTypograph.fontSize};
		font-weight: ${listTypograph.fontWeight};
	}

	& > div {
		flex: 1;
		display: flex;
		align-items: center;
	}
`;

const Styled = {
	InnerWrapper,
	InnerCenteredWrapper
};

export default Styled;
