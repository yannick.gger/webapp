export interface ICountryDetail {
	from?: Date;
	to?: Date;
	campaigns?: { id: string; campaignName: string }[];
	title: any;
	onFetchData?: (props: any) => Promise<any>;
	fetchParams?: any;
	type: string;
	defaultPeriod?: 'today' | 'week' | 'month' | string;
	dashboardPeriod?: string;
	isDashboard?: boolean;
	onRemoveItem?: () => void;
}
