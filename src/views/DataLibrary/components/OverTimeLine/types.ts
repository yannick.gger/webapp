export interface IOverTimeLine {
	title: any;
	from?: Date;
	to?: Date;
	campaigns?: { id: string; campaignName: string }[];
	onFetchData?: (props: any) => Promise<any>;
	fetchParams?: any;
	color: string;
	isHoverable?: boolean;
	type: string;
	defaultPeriod?: 'today' | 'week' | 'month' | string;
	dashboardPeriod?: string;
	isDashboard?: boolean;
	onRemoveItem?: () => void;
}
