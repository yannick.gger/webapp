import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';
import { AxiosError } from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Flags from 'country-flag-icons/react/1x1';

import SummaryCard from '../SummaryCard';
import { Line } from 'components/Chart';
import { IBudgetSummary } from './types';
import { transformToLineChartData, formatDate } from 'shared/helpers/Chart/chart-util';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import useBudgetData from 'hooks/Chart/useBudgetData';
import useDateRange from 'hooks/Chart/useDateRange';

import Styled from './BudgetSummary.style';
import colors from 'styles/variables/colors';
import Calendar from 'assets/icons/calendar.svg';
import { CountryData } from '../CountryBudgetList/types';
import { MONTH, BUDGET } from 'constants/data-library';

const BudgetSummary = (props: IBudgetSummary) => {
	const [loading, setLoading] = useState(true);
	const [lineData, setLineData] = useState<{ labels: string[]; datasets: any }>({
		labels: [],
		datasets: {}
	});
	const [overall, setOverall] = useState<string>('0');
	const [overallCpc, setOverallCpc] = useState<string>('0');
	const [overallCpm, setOverallCpm] = useState<string>('0');
	const [top3Countries, setTop3Countries] = useState<{ totalBudget: number; countries: Array<CountryData> }>();
	const [to, setTo] = useState<Date>();
	const [from, setFrom] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [isFirstRendering, setIsFirstRendering] = useState(true);

	const { getBudgetTotal, getBudgets, getBudgetCPM, getBudgetCPC, getBudgetsByCountry } = useBudgetData();
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);

	const { addToast } = useContext(ToastContext);

	const changePeriodHandler = (dates: any) => {
		const [start, end] = dates;
		setFrom(start);
		setTo(end);
	};

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
		return () => {};
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
		return () => {};
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		setLoading(true);
		if (from && to) {
			fetchDatas(new Date(from), new Date(to), selectedCampaigns)
				.then(
					(res: {
						budgetOverTime: any;
						budgetOverall: string;
						budgetOverallCpc: string;
						budgetOverallCpm: string;
						budgetCountry: { totalBudget: number; countries: Array<CountryData> };
					}) => {
						setLineData(res.budgetOverTime);
						setOverall(res.budgetOverall);
						setOverallCpc(res.budgetOverallCpc);
						setOverallCpm(res.budgetOverallCpm);
						setTop3Countries(res.budgetCountry);
					}
				)
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `Reach Summary is failed to fetch data.\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		} else {
			setLoading(false);
		}
	}, [from, to, selectedCampaigns]);

	const fetchDatas = async (from: Date, to: Date, selectedCampaigns: any) => {
		const budgetOverTime = getBudgets({ from: from, to: to, frequancy: 'daily', campaigns: selectedCampaigns }).then((res: any) => {
			return transformToLineChartData({ arr: res, color: colors.chartLineBudget });
		});

		const budgetOverall = getBudgetTotal({ from: from, to: to, campaigns: selectedCampaigns }).then((res: string) => {
			return res;
		});

		const budgetOverallCpm = getBudgetCPM({ from: from, to: to, campaigns: selectedCampaigns }).then((res: string) => {
			return res;
		});

		const budgetOverallCpc = getBudgetCPC({ from: from, to: to, campaigns: selectedCampaigns }).then((res: string) => {
			return res;
		});

		const budgetCountry = getBudgetsByCountry({ from: from, to: to, campaigns: selectedCampaigns }).then((res: any) => {
			let totalBudget = 0;
			if (res.length > 0) {
				totalBudget = res.reduce((prev: number, current: CountryData) => prev + current.amount, 0);
				let filteredCountryData = res.sort((a: CountryData, b: CountryData) => b.amount - a.amount);
				filteredCountryData = filteredCountryData.filter((item: CountryData, index: number) => index < 3);
				return { totalBudget: totalBudget, countries: filteredCountryData };
			} else {
				totalBudget = 0;
				return { totalBudget: totalBudget, countries: [] };
			}
		});

		return Promise.all([budgetOverTime, budgetOverall, budgetOverallCpm, budgetOverallCpc, budgetCountry]).then((values) => {
			return {
				budgetOverTime: values[0],
				budgetOverall: values[1],
				budgetOverallCpm: values[2],
				budgetOverallCpc: values[3],
				budgetCountry: values[4]
			};
		});
	};

	return (
		<SummaryCard
			title='Budget'
			selectedPeriod={from && to ? `${formatDate(from)}~${formatDate(to)}` : MONTH}
			isDashboard={props.isDashboard}
			chartType={BUDGET.SUMMARY}
			onRemoveItem={props.onRemoveItem}
		>
			<Styled.Section>
				<Styled.DataBlock>
					<div>
						<Styled.Label>Budget overall</Styled.Label>
						<div>
							<Styled.DataContent>{overall}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>CPM</Styled.Label>
						<div>
							<Styled.DataContent>{overallCpm}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>CPC</Styled.Label>
						<div>
							<Styled.DataContent>{overallCpc}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Budget by country (Top 3)</Styled.Label>
						<div>
							{!loading && top3Countries && top3Countries.countries.length > 0 ? (
								top3Countries.countries.map((country: CountryData) => {
									// @todo: country-flag-icons type warning
									const Flag = Flags[country.country];
									return (
										<Styled.DataContent key={country.country}>
											<Styled.CountryDataWrapper>
												<Styled.CountryFlagWrapper>
													<Flag />
												</Styled.CountryFlagWrapper>
												<span>{`${Math.floor((country.amount / top3Countries.totalBudget) * 100)}%`}</span>
											</Styled.CountryDataWrapper>
										</Styled.DataContent>
									);
								})
							) : (
								<Styled.DataContent className='no-data'>N/A</Styled.DataContent>
							)}
						</div>
					</div>
				</Styled.DataBlock>
				<Styled.Buttons>
					<Styled.CalendarContainer>
						<DatePicker
							selected={from}
							onChange={changePeriodHandler}
							startDate={from}
							endDate={to}
							selectsRange
							filterDate={(date) => {
								return new Date() > date;
							}}
							customInput={
								<Styled.Button>
									<Styled.Icon src={Calendar} />
								</Styled.Button>
							}
						/>
					</Styled.CalendarContainer>
				</Styled.Buttons>
			</Styled.Section>

			<Styled.ChartContainer>
				{loading ? (
					<LoadingSpinner size='md' />
				) : lineData.labels.length ? (
					<Line
						chartHeight={400}
						labels={lineData.labels}
						datasets={lineData.datasets}
						pointRadius={2}
						lineTension={0}
						borderWidth={2}
						isHoverable={props.isHoverable}
					/>
				) : (
					<div>N/A</div>
				)}
			</Styled.ChartContainer>
		</SummaryCard>
	);
};

export default BudgetSummary;
