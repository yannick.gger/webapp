import { useState, useEffect } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';

import ChartCard from '../ChartCard';
import { Bar } from 'components/Chart';

const BarChart = (props: any) => {
	const [from, setFrom] = useState<Date>();
	const [to, setTo] = useState<Date>();

	useEffect(() => {
		if (props.from && props.to) {
			setFrom(props.from);
			setTo(props.to);
		}
	}, [props.from, props.to]);

	return (
		<ChartCard onPinClick={() => {}} title={props.title} from={from} setFrom={setFrom} to={to} setTo={setTo}>
			{props.labels && props.datasets ? (
				<Bar
					labels={props.labels}
					legendPosition={props.legendPosition || 'right'}
					datasets={props.datasets}
					images={props.images}
					xTickPadding={props.xTickPadding}
				/>
			) : (
				<LoadingSpinner size='md' />
			)}
		</ChartCard>
	);
};

export default BarChart;
