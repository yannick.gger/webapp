import LoadingSpinner from 'components/LoadingSpinner';
import { Pie } from 'components/Chart';
import ChartCard from '../ChartCard';

const PieChart = (props: any) => {
	return (
		<ChartCard title={props.title} from={props.from} to={props.to} setFrom={props.setFrom} setTo={props.setTo} onPinClick={() => {}}>
			{props.labels && props.datasets ? (
				<Pie labels={props.labels} legendPosition='right' datasets={props.datasets} spacing={0} />
			) : (
				<LoadingSpinner size='md' />
			)}
		</ChartCard>
	);
};

export default PieChart;
