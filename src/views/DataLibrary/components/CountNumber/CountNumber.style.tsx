import styled from 'styled-components';
import { breakpoints } from 'styles/variables/media-queries';

const InnerCenteredWrapper = styled.div`
	min-height: 200px;
	min-width: 345px;
	display: flex;
	justify-content: center;
	align-items: center;

	@media (max-width: ${breakpoints.sm}) {
		min-height: 0;
		padding: 20px 0;
	}
`;

const Styled = {
	InnerCenteredWrapper
};

export default Styled;
