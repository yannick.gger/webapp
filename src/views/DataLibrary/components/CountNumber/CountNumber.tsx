import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';

import ChartCard from '../ChartCard';
import { Number } from 'components/Chart';
import { ICountNumber } from './types';
import Styled from './CountNumber.style';
import useDateRange from 'hooks/Chart/useDateRange';

import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import { formatNumber } from 'shared/helpers/Chart/chart-util';

const CountNumber = (props: ICountNumber) => {
	const [value, setValue] = useState<number | string>(0);
	const [from, setFrom] = useState<Date>();
	const [to, setTo] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [loading, setLoading] = useState(true);
	const [isFirstRendering, setIsFirstRendering] = useState(true);
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);

	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		if (props.onFetchData && from && to) {
			setLoading(true);
			props
				.onFetchData({ from: new Date(from), to: new Date(to), campaigns: selectedCampaigns, ...props.fetchParams })
				.then((res) => {
					setValue(res);
				})
				.catch((err) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		}
	}, [from, to, selectedCampaigns]);

	return (
		<ChartCard
			title={props.title}
			from={from}
			setFrom={setFrom}
			to={to}
			setTo={setTo}
			period={props.defaultPeriod}
			dashboardPeriod={props.dashboardPeriod}
			chartType={props.type}
			isDashboard={props.isDashboard}
			onRemoveItem={props.onRemoveItem}
		>
			<Styled.InnerCenteredWrapper>{!loading ? <Number value={formatNumber(value)} size='lg' /> : <LoadingSpinner size='md' />}</Styled.InnerCenteredWrapper>
		</ChartCard>
	);
};

export default CountNumber;
