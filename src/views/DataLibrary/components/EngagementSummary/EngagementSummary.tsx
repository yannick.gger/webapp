import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';
import { AxiosError } from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import SummaryCard from '../SummaryCard';
import { Line } from 'components/Chart';
import { IEngagementSummary } from './types';
import { transformToLineChartData, formatDate, formatNumber } from 'shared/helpers/Chart/chart-util';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';

import useEngagementData from 'hooks/Chart/useEngagementData';
import useDateRange from 'hooks/Chart/useDateRange';

import Styled from './EngagementSummary.style';
import colors from 'styles/variables/colors';
import Calendar from 'assets/icons/calendar.svg';
import { MONTH, ENGAGEMENT, INSTAGRAM_STORY } from 'constants/data-library';

const EngagementSummary = (props: IEngagementSummary) => {
	const [lineData, setLineData] = useState<{ labels: string[]; datasets: any }>({
		labels: [],
		datasets: {}
	});
	const [overall, setOverall] = useState<string>('0K');
	const [rate, setRate] = useState<string>('0%');
	const [actualRate, setActualRate] = useState<string>('0%');
	const [storyView, setStoryView] = useState<string>();
	const [loading, setLoading] = useState(true);
	const [to, setTo] = useState<Date>();
	const [from, setFrom] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [isFirstRendering, setIsFirstRendering] = useState(true);

	const { getEngagementTotal, getEngagementRate, getEngagementActualRate, getEngagementCounts, getEngagementImpressionsByPlatforms } = useEngagementData();
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);
	const { addToast } = useContext(ToastContext);

	const changePeriodHandler = (dates: any) => {
		const [start, end] = dates;
		setFrom(start);
		setTo(end);
	};

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
		return () => {};
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
		return () => {};
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		setLoading(true);
		if (from && to) {
			fetchDatas(new Date(from), new Date(to), selectedCampaigns)
				.then(
					(res: { engagementOverTime: any; engagementOverall: string; engagementRate: string; engagementActualRate: string; engagementStoryView: string }) => {
						setLineData(res.engagementOverTime);
						setOverall(res.engagementOverall);
						setRate(res.engagementRate);
						setActualRate(res.engagementActualRate);
						setStoryView(res.engagementStoryView);
					}
				)
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `Engagement Summary is failed to fetch data.\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		} else {
			setLoading(false);
		}
	}, [from, to, selectedCampaigns]);

	const fetchDatas = async (from: Date, to: Date, selectedCampaigns: any) => {
		const engagementOverTime = getEngagementCounts({ from: from, to: to, frequancy: 'daily', campaigns: selectedCampaigns }).then((res: any) => {
			return transformToLineChartData({ arr: res, color: colors.chartLineEngagement });
		});

		const engagementOverall = getEngagementTotal({ from: from, to: to, campaigns: selectedCampaigns }).then((res: { likes: number; comments: number }) => {
			return `${formatNumber(res.likes + res.comments)}`;
		});

		const engagementRate = getEngagementRate({ from: from, to: to, campaigns: selectedCampaigns }).then((res) => {
			return res;
		});

		const engagementActualRate = getEngagementActualRate({ from: from, to: to, campaigns: selectedCampaigns }).then((res) => {
			return res;
		});

		const engagementStoryView = getEngagementImpressionsByPlatforms({ from: from, to: to, campaigns: selectedCampaigns, platforms: INSTAGRAM_STORY }).then(
			(res) => {
				return `${formatNumber(res)}`;
			}
		);

		return Promise.all([engagementOverTime, engagementOverall, engagementRate, engagementActualRate, engagementStoryView]).then((values) => {
			return {
				engagementOverTime: values[0],
				engagementOverall: values[1],
				engagementRate: values[2],
				engagementActualRate: values[3],
				engagementStoryView: values[4]
			};
		});
	};

	return (
		<SummaryCard
			title='Engagement'
			selectedPeriod={from && to ? `${formatDate(from)}~${formatDate(to)}` : MONTH}
			isDashboard={props.isDashboard}
			chartType={ENGAGEMENT.SUMMARY}
			onRemoveItem={props.onRemoveItem}
		>
			<Styled.Section>
				<Styled.DataBlock>
					<div>
						<Styled.Label>Engagement overall</Styled.Label>
						<div>
							<Styled.DataContent>{overall}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Engagement Rate</Styled.Label>
						<div>
							<Styled.DataContent>{rate}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Engagement Acutal Rate</Styled.Label>
						<div>
							<Styled.DataContent>{actualRate}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Story View</Styled.Label>
						<div>
							<Styled.DataContent>{storyView}</Styled.DataContent>
						</div>
					</div>
				</Styled.DataBlock>

				<Styled.Buttons>
					<Styled.CalendarContainer>
						<DatePicker
							selected={from}
							onChange={changePeriodHandler}
							startDate={from}
							endDate={to}
							selectsRange
							filterDate={(date) => {
								return new Date() > date;
							}}
							customInput={
								<Styled.Button>
									<Styled.Icon src={Calendar} />
								</Styled.Button>
							}
						/>
					</Styled.CalendarContainer>
				</Styled.Buttons>
			</Styled.Section>

			<Styled.ChartContainer>
				{loading ? (
					<LoadingSpinner size='md' />
				) : lineData.labels.length ? (
					<Line
						chartHeight={400}
						labels={lineData.labels}
						datasets={lineData.datasets}
						pointRadius={2}
						lineTension={0}
						borderWidth={2}
						isHoverable={props.isHoverable}
					/>
				) : (
					<div>N/A</div>
				)}
			</Styled.ChartContainer>
		</SummaryCard>
	);
};

export default EngagementSummary;
