import styled from 'styled-components';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	margin-top: ${guttersWithRem.xxxl};
	margin-bottom: ${guttersWithRem.l};
`;

const TapItem = styled.div`
	width: 150px;
	padding: ${guttersWithRem.xs} 0;
	text-align: center;
	cursor: pointer;

	font-family: ${typography.dataLibraryTab.fontFamilies};

	&.isActive {
		font-weight: ${typography.dataLibraryTab.fontWeight};
		border-bottom: 3px solid ${colors.black};
	}
`;

const Styled = {
	Wrapper,
	TapItem
};

export default Styled;
