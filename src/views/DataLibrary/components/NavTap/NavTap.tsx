import { useState, useEffect } from 'react';
import { useHistory, useRouteMatch, useLocation } from 'react-router-dom';
import classNames from 'classnames';

import Styled from './NavTap.style';

/**
 * @todo i18next
 */
const NavTap = () => {
	const [currentPage, setCurrentPage] = useState('');
	const history = useHistory();
	const { url } = useRouteMatch();
	const { pathname } = useLocation();
	const tapItems = ['Dashboard', 'Reach', 'Impressions', 'Traffic', 'Audience', 'Budget', 'Engagement'];

	const clickTapItemHandler = (tapItem: string) => {
		if (tapItem === 'Dashboard') {
			history.push(`${url}`);
		} else {
			history.push(`${url}/${tapItem.toLocaleLowerCase()}`);
		}
	};

	useEffect(() => {
		const currentUrl = pathname.split('/').slice(-1)[0];

		setCurrentPage(currentUrl);
	}, [pathname]);

	return (
		<Styled.Wrapper>
			{tapItems.map((tapItem) => {
				return (
					<Styled.TapItem
						key={tapItem}
						className={classNames({ isActive: currentPage === tapItem.toLocaleLowerCase() || (tapItem === 'Dashboard' && currentPage === 'data-library') })}
						onClick={() => clickTapItemHandler(tapItem)}
					>
						{tapItem}
					</Styled.TapItem>
				);
			})}
		</Styled.Wrapper>
	);
};

export default NavTap;
