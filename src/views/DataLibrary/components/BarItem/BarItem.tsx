import Styled from './BarItem.style';

const BarItem = (props: any) => {
	return (
		<Styled.Wrapper>
			<Styled.Section>{props.label}</Styled.Section>
			<Styled.Section>{props.children}</Styled.Section>
			<Styled.Section fontWeight={700} textAlign='right'>
				{props.value}
				{props.sign}
			</Styled.Section>
		</Styled.Wrapper>
	);
};

export default BarItem;
