import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';
import { AxiosError } from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Flags from 'country-flag-icons/react/1x1';

import SummaryCard from '../SummaryCard';
import { Line } from 'components/Chart';
import { IImpressionsSummary } from './types';
import { transformToLineChartData, formatDate, formatNumber } from 'shared/helpers/Chart/chart-util';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import useImpressionData from 'hooks/Chart/useImpressionData';
import useAudienceData from 'hooks/Chart/useAudienceData';
import useDateRange from 'hooks/Chart/useDateRange';

import Styled from './ImpressionsSummary.style';
import colors from 'styles/variables/colors';
import Calendar from 'assets/icons/calendar.svg';
import { CountryData } from '../CountrySummaryList/types';
import { MONTH, IMPRESSIONS } from 'constants/data-library';

const ImpressionsSummary = (props: IImpressionsSummary) => {
	const [lineData, setLineData] = useState<{ labels: string[]; datasets: any }>({
		labels: [],
		datasets: {}
	});
	const [overall, setOverall] = useState<string>('0K');
	const [loading, setLoading] = useState(true);
	const [gender, setGender] = useState<{ labels: string[]; data: string[] }>({ labels: ['W', 'M', 'O'], data: ['0%', '0%', '0%'] });
	const [top3Countries, setTop3Countries] = useState<{ totalFollowers: number; countries: Array<CountryData> }>();
	const [top3Ages, setTop3Ages] = useState<{ totalFollowers: number; ages: any[] }>();
	const [to, setTo] = useState<Date>();
	const [from, setFrom] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [isFirstRendering, setIsFirstRendering] = useState(true);

	const { getImpressionTotal, getImpressionCounts } = useImpressionData();
	const { getAudienceGender, getAudienceCountry, getAudienceAge } = useAudienceData();
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);

	const { addToast } = useContext(ToastContext);

	const changePeriodHandler = (dates: any) => {
		const [start, end] = dates;
		setFrom(start);
		setTo(end);
	};

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
		return () => {};
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
		return () => {};
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		setLoading(true);
		if (from && to) {
			fetchDatas(new Date(from), new Date(to), selectedCampaigns)
				.then(
					(res: {
						impressionsOverTime: any;
						impressionsOverall: string;
						impressionsGender: { labels: string[]; data: string[] };
						impressionsCountry: { totalFollowers: number; countries: Array<CountryData> };
						impressionsAge: { totalFollowers: number; ages: any[] };
					}) => {
						setLineData(res.impressionsOverTime);
						setOverall(res.impressionsOverall);
						setGender(res.impressionsGender);
						setTop3Countries(res.impressionsCountry);
						setTop3Ages(res.impressionsAge);
					}
				)
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `Impressions Summary is failed to fetch data.\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		} else {
			setLoading(false);
		}
	}, [from, to, selectedCampaigns]);

	const fetchDatas = async (from: Date, to: Date, selectedCampaigns: any) => {
		const impressionsOverTime = getImpressionCounts({ from: from, to: to, frequancy: 'daily', campaigns: selectedCampaigns }).then((res: any) => {
			return transformToLineChartData({ arr: res, color: colors.chartLineImpressions });
		});

		const impressionsOverall = getImpressionTotal({ from: from, to: to, campaigns: selectedCampaigns }).then((res: number) => {
			return `${formatNumber(res)}`;
		});

		const impressionsGender = getAudienceGender({ from: from, to: to, campaigns: selectedCampaigns }).then((res: any) => {
			const impressionsGender: { labels: string[]; data: string[] } = { labels: [], data: [] };
			if (res.total > 0) {
				res.labels.forEach((label: string, index: number) => {
					switch (label) {
						case 'MALE':
							impressionsGender.labels.push('M');
							break;
						case 'FEMALE':
							impressionsGender.labels.push('W');
							break;
						case 'UNKNOWN':
							impressionsGender.labels.push('O');
							break;
						default:
							break;
					}
					const rate = `${Math.floor((res.data[index] / res.total) * 100)}%`;
					impressionsGender.data.push(rate);
				});
			}
			return impressionsGender;
		});

		const impressionsCountry = getAudienceCountry({ from: from, to: to, campaigns: selectedCampaigns }).then((res: any) => {
			let totalFollowers = 0;
			if (res.length > 0) {
				totalFollowers = res.reduce((prev: number, current: CountryData) => prev + current.followers, 0);
				let filteredCountryData = res.sort((a: CountryData, b: CountryData) => b.followers - a.followers);
				filteredCountryData = filteredCountryData.filter((item: CountryData, index: number) => index < 3);
				return { totalFollowers: totalFollowers, countries: filteredCountryData };
			} else {
				totalFollowers = 0;
				return { totalFollowers: totalFollowers, countries: [] };
			}
		});

		const impressionsAge = getAudienceAge({ from: from, to: to, campaigns: selectedCampaigns }).then((res: { [key: string]: number }) => {
			const keyValuePair: Array<Array<string | number>> = Object.keys(res)
				.map((key) => {
					return [key, res[key]];
				})
				.sort((a: any, b: any) => b[1] - a[1]);

			const totalFollowers = keyValuePair.reduce((prev, current: any) => prev + current[1], 0);
			const filteredData = keyValuePair.filter((item, index) => index < 3);

			filteredData.forEach((item: any, index) => {
				const percent = `${Math.floor((item[1] / totalFollowers) * 100)}%`;
				item.splice(1, 1, totalFollowers > 0 ? percent : '0%');
			});

			return { totalFollowers: totalFollowers, ages: totalFollowers > 0 ? filteredData : [] };
		});

		return Promise.all([impressionsOverTime, impressionsOverall, impressionsGender, impressionsCountry, impressionsAge]).then((values) => {
			return {
				impressionsOverTime: values[0],
				impressionsOverall: values[1],
				impressionsGender: values[2],
				impressionsCountry: values[3],
				impressionsAge: values[4]
			};
		});
	};

	return (
		<SummaryCard
			title='Impressions'
			selectedPeriod={from && to ? `${formatDate(from)}~${formatDate(to)}` : MONTH}
			isDashboard={props.isDashboard}
			chartType={IMPRESSIONS.SUMMARY}
			onRemoveItem={props.onRemoveItem}
		>
			<Styled.Section>
				<Styled.DataBlock>
					<div>
						<Styled.Label>Impression overall</Styled.Label>
						<div>
							<Styled.DataContent>{overall}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Impression by gender</Styled.Label>
						<div>
							{gender.data.length > 0 ? (
								gender.labels.map((label: string, index: number) => {
									return (
										<Styled.DataContent key={index}>
											<span>{label}</span> {gender.data[index]}
										</Styled.DataContent>
									);
								})
							) : (
								<Styled.DataContent className='no-data'>N/A</Styled.DataContent>
							)}
						</div>
					</div>

					<div>
						<Styled.Label>Impression by country (Top 3)</Styled.Label>
						<div>
							{!loading && top3Countries && top3Countries.countries.length > 0 ? (
								top3Countries.countries.map((country: CountryData) => {
									// @todo: country-flag-icons type warning
									const Flag = Flags[country.alpha2code];
									return (
										<Styled.DataContent key={country.name}>
											<Styled.CountryDataWrapper>
												<Styled.CountryFlagWrapper>
													<Flag />
												</Styled.CountryFlagWrapper>
												<span>{`${Math.floor((country.followers / top3Countries.totalFollowers) * 100)}%`}</span>
											</Styled.CountryDataWrapper>
										</Styled.DataContent>
									);
								})
							) : (
								<Styled.DataContent className='no-data'>N/A</Styled.DataContent>
							)}
						</div>
					</div>

					<div>
						<Styled.Label>Impression by age (Top 3)</Styled.Label>
						<div>
							{!loading && top3Ages && top3Ages.ages.length > 0 ? (
								top3Ages.ages.map((item: string[]) => {
									return (
										<Styled.DataContent key={item[0]}>
											<span>{item[0]}</span> {item[1]}
										</Styled.DataContent>
									);
								})
							) : (
								<Styled.DataContent className='no-data'>N/A</Styled.DataContent>
							)}
						</div>
					</div>
				</Styled.DataBlock>
				<Styled.Buttons>
					<Styled.CalendarContainer>
						<DatePicker
							selected={from}
							onChange={changePeriodHandler}
							startDate={from}
							endDate={to}
							selectsRange
							filterDate={(date) => {
								return new Date() > date;
							}}
							customInput={
								<Styled.Button>
									<Styled.Icon src={Calendar} />
								</Styled.Button>
							}
						/>
					</Styled.CalendarContainer>
				</Styled.Buttons>
			</Styled.Section>

			<Styled.ChartContainer>
				{loading ? (
					<LoadingSpinner size='md' />
				) : lineData.labels.length ? (
					<Line
						chartHeight={400}
						labels={lineData.labels}
						datasets={lineData.datasets}
						pointRadius={2}
						lineTension={0}
						borderWidth={2}
						isHoverable={props.isHoverable}
					/>
				) : (
					<div>N/A</div>
				)}
			</Styled.ChartContainer>
		</SummaryCard>
	);
};

export default ImpressionsSummary;
