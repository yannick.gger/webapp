export interface IReachSummary {
	from?: Date;
	to?: Date;
	campaigns?: { id: string; campaignName: string }[];
	onFetchData?: (props: any) => Promise<any>;
	fetchParams?: any;
	isHoverable?: boolean;
	isDashboard?: boolean;
	defaultPeriod?: string;
	dashboardPeriod?: string;
	onRemoveItem?: () => void;
}
