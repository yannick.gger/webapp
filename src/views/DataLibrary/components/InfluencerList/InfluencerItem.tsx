import Styled from './InfluencerItem.style';
import { IInfluencerItem } from './types';
import { formatNumber } from 'shared/helpers/Chart/chart-util';

const InfluencerItem = (props: IInfluencerItem) => {
	return (
		<Styled.Wrapper width={props.width} height={props.height}>
			<Styled.Section maxWidth='20%'>
				<Styled.Image src={props.profileImage} alt='profile-image' />
			</Styled.Section>
			<Styled.Section display='flex' alignItems='flex-start' justifyContent='center' flexDirection='column' maxWidth='50%'>
				<Styled.FollowerValue>{`${formatNumber(props.follower)} followers`}</Styled.FollowerValue>
				<Styled.InfluenceName>{props.instagramUsername}</Styled.InfluenceName>
			</Styled.Section>
			<Styled.Section display='flex' justifyContent='flex-end' alignItems='center' maxWidth='30%'>
				<Styled.InfluencerValue>{formatNumber(props.value)}</Styled.InfluencerValue>
			</Styled.Section>
		</Styled.Wrapper>
	);
};

export default InfluencerItem;
