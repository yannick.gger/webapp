import { ComponentStory, ComponentMeta } from '@storybook/react';
import List from './InfluencerList';
import { IInfluencerList } from './types';

export default {
	title: 'InfluencerTable',
	component: List,
	argTypes: {
		influencers: {
			control: { type: 'array' }
		},
		isTopThree: {
			control: { type: 'boolean' }
		}
	}
} as ComponentMeta<typeof List>;

const Template: ComponentStory<typeof List> = (args: IInfluencerList) => <List {...args} />;

export const InfluencerList = Template.bind({});

InfluencerList.args = {
	isTopThree: true,
	influencers: [
		{
			id: '12',
			imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/440px-Cat03.jpg',
			follower: 100000,
			instagramUsername: '@cat_stagram',
			value: 1200
		},
		{
			id: '123',
			imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/440px-Cat03.jpg',
			follower: 100000,
			instagramUsername: '@cat_stagram',
			value: 3400
		},
		{
			id: '1234',
			imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/440px-Cat03.jpg',
			follower: 100000,
			instagramUsername: '@cat_stagram',
			value: 5000
		},
		{
			id: '12345',
			imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/440px-Cat03.jpg',
			follower: 100000,
			instagramUsername: '@cat_stagram',
			value: 500
		}
	]
};
