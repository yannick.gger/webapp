export interface IInfluencerStyle {
	width?: string;
	height?: string;
	display?: 'flex' | 'grid' | 'inline' | 'inline-block' | 'inline-flex' | 'inline-grid' | 'block' | 'flow-root';
	fontSize?: string;
	fontWeight?: 'bold' | 'normal' | 'lighter' | 'bolder';
	justifyContent?: string;
	alignItems?: string;
	flexDirection?: string;
	maxWidth?: string;
}

export interface IInfluencerItem extends IInfluencerStyle {
	profileImage: string;
	follower: number;
	instagramUsername: string;
	value: number | string;
}

export type Influencer = {
	id: number;
	imageUrl: string;
	follower: number;
	instagramUsername: string;
	value: number;
};

export interface IInfluencerList extends IInfluencerStyle {
	influencers: Influencer[];
	isTopThree?: boolean;
}
