import { ComponentStory, ComponentMeta } from '@storybook/react';
import Item from './InfluencerItem';
import { IInfluencerItem } from './types';

export default {
	title: 'InfluencerTable',
	component: Item,
	argTypes: {
		width: {
			control: { type: 'text' }
		},
		height: {
			control: { type: 'text' }
		},
		profileImage: {
			control: { type: 'text' }
		},
		follower: {
			control: { type: 'number' }
		},
		instagramUsername: {
			control: { type: 'text' }
		},
		value: {
			control: { type: 'text' }
		}
	}
} as ComponentMeta<typeof Item>;

const Template: ComponentStory<typeof Item> = (args: IInfluencerItem) => <Item {...args} />;

export const InfluencerItem = Template.bind({});

InfluencerItem.args = {
	profileImage: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/440px-Cat03.jpg',
	follower: 100000,
	instagramUsername: '@cat_stagram',
	value: 10000
};
