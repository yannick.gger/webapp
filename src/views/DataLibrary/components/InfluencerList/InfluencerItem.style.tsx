import styled from 'styled-components';
import { IInfluencerStyle } from './types';
import typography from 'styles/variables/typography';

const labelTypograph = typography.label;
const listTypograph = typography.list;
const textTypograph = typography.headings;

const Wrapper = styled.div<IInfluencerStyle>`
	width: ${(props) => props.width || '100%'};
	height: ${(props) => props.height || 'auto'};
	min-width: 345px;

	display: flex;
	column-gap: 5px;
`;

const Section = styled.div<IInfluencerStyle>`
	flex: 1;
	display: ${(props) => props.display};
	font-size: ${(props) => props.fontSize};
	font-weight: ${(props) => props.fontWeight};
	justify-content: ${(props) => props.justifyContent};
	align-items: ${(props) => props.alignItems};
	flex-direction: ${(props) => props.flexDirection};
	max-width: ${(props) => props.maxWidth};
	overflow-x: auto;
`;

const FollowerValue = styled.span`
	font-family: ${labelTypograph.fontFamily};
	font-family: ${labelTypograph.small.fontSize};
`;

const InfluenceName = styled.span`
	font-family: ${listTypograph.fontFamily};
	font-size: ${listTypograph.fontSize};
	font-weight: ${listTypograph.fontWeight};
`;

const InfluencerValue = styled.span`
	font-family: ${textTypograph.fontFamily};
	font-size: ${textTypograph.h4.fontSize};
	font-weight: ${textTypograph.h4.fontWeight};
`;

const Image = styled.img`
	min-width: 45px;
	width: 80%;
	border-radius: 50%;
`;

const Styled = {
	Wrapper,
	Section,
	FollowerValue,
	InfluenceName,
	InfluencerValue,
	Image
};

export default Styled;
