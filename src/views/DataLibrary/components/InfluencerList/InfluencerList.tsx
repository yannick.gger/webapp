import InfluencerItem from './InfluencerItem';
import { IInfluencerList, Influencer } from './types';
import Styled from './InfluencerList.style';

const InfluencerList = (props: IInfluencerList) => {
	let filteredInfluencers: any[] = [];
	if (props.influencers.length > 0) {
		filteredInfluencers = props.influencers.sort((a: any, b: any) => b.value - a.value);
	}

	if (props.isTopThree) {
		filteredInfluencers = filteredInfluencers.filter((item, index) => {
			if (index < 3) {
				return item;
			}
		});
	}

	return (
		<Styled.Wrapper>
			{filteredInfluencers.length > 0 ? (
				filteredInfluencers.map((influencer: Influencer) => (
					<InfluencerItem
						key={influencer.id}
						profileImage={influencer.imageUrl}
						follower={influencer.follower}
						instagramUsername={influencer.instagramUsername}
						value={influencer.value}
					/>
				))
			) : (
				<Styled.CenteredWrapper>
					<span>N/A</span>
				</Styled.CenteredWrapper>
			)}
		</Styled.Wrapper>
	);
};

export default InfluencerList;
