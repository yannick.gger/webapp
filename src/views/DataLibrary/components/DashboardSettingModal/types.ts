export interface IDashboardSettingModal {
	isVisible: boolean;
	isAppendChart: boolean;
	onCloseModal: (props: any) => void;
	type: string;
	selectedPeriod?: string;
}
