import styled from 'styled-components';
import { Input } from 'antd';

const Wrapper = styled.div``;

const Section = styled.div`
	margin-bottom: 15px;
`;

const Label = styled.label`
	font-size: 0.7em;
`;

const CustomImput = styled(Input)`
	height: 38px;
	border-radius: 5px;
`;

const Styled = {
	Wrapper,
	Section,
	Label,
	CustomImput
};

export default Styled;
