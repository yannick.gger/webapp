import { useState, useEffect, useContext } from 'react';
import { Modal, Radio } from 'antd';
import { AxiosError } from 'axios';

import { ToastContext } from 'contexts';
import DashboardSelector from '../DashboardSelector';
import useDataLibraryDashboard from 'hooks/Chart/useDataLibraryDashboard';
import uuid from 'shared/helpers/uuid';
import Styled from './DashboardSettingModal.style';
import { LARGE, MEDIUM, SMALL } from 'constants/data-library';
import { IDashboardSettingModal } from './types';

const DashboardSettingModal = (props: IDashboardSettingModal) => {
	const [enteredDashboardName, setEnteredDashboardName] = useState('');
	const [selectedSize, setSelectedSize] = useState(SMALL);
	const [dashboards, setDashboards] = useState([]);
	const [selectedDashboard, setSelectedDashboard] = useState<{ value: string; label: string }>();

	const { addToast } = useContext(ToastContext);
	const { getDashboards, appendChartInDashboard, createDashboard } = useDataLibraryDashboard();

	const selectSizeHandler = (e: any) => {
		setSelectedSize(e.target.value);
	};

	const sizeOptions = [{ label: LARGE, value: LARGE }, { label: MEDIUM, value: MEDIUM }, { label: SMALL, value: SMALL }];

	const closeModalHandler = () => {
		props.onCloseModal(false);
	};

	const onOkHandler = () => {
		if (props.isAppendChart) {
			if (selectedDashboard) {
				appendChartInDashboard({
					id: selectedDashboard.value,
					data: {
						type: props.type,
						size: selectedSize,
						metaData: {
							period: props.selectedPeriod
						}
					}
				})
					.then(() => {
						props.onCloseModal(false);
					})
					.catch((err) => {
						console.log(err);
					});
			}
		} else {
			createDashboard({ name: enteredDashboardName })
				.then((res) => {
					return res.data.id;
				})
				.then((id) => {
					appendChartInDashboard({
						id: id,
						data: {
							type: props.type,
							size: selectedSize,
							metaData: {
								period: props.selectedPeriod
							}
						}
					}).then(() => {
						props.onCloseModal(false);
					});
				})
				.catch((err) => {
					console.log(err);
				});
		}
	};

	useEffect(() => {
		if (props.isAppendChart) {
			getDashboards()
				.then((res) => {
					setDashboards(res);
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				});
		}
	}, []);

	return (
		<Modal visible={props.isVisible} onOk={onOkHandler} onCancel={closeModalHandler} closable={false}>
			<Styled.Wrapper>
				{!props.isAppendChart && (
					<Styled.Section>
						<Styled.Label>Dashboard Name</Styled.Label>
						<Styled.CustomImput
							placeholder='Type your new dashboard name'
							value={enteredDashboardName}
							onChange={(e) => setEnteredDashboardName(e.target.value)}
						/>
					</Styled.Section>
				)}
				{props.isAppendChart && (
					<Styled.Section>
						<Styled.Label>Dashboard</Styled.Label>
						<DashboardSelector dashboards={dashboards} onSelect={setSelectedDashboard} selectorWidth='100%' isAppendChart={props.isAppendChart} />
					</Styled.Section>
				)}
				<Styled.Section>
					<Styled.Label>Chart Size</Styled.Label>
					<div>
						<Radio.Group options={sizeOptions} value={selectedSize} onChange={selectSizeHandler} />
					</div>
				</Styled.Section>
			</Styled.Wrapper>
		</Modal>
	);
};

export default DashboardSettingModal;
