import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';

import ChartCard from '../ChartCard';
import CountryBudgetList from '../CountryBudgetList';
import { ICountryBudgetDetail } from './types';
import Styled from './CountryBudgetDetail.style';
import { SecondaryButton } from 'components/Button';

import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import useDateRange from 'hooks/Chart/useDateRange';
import ChartDetailModal from '../ChartDetailModal';

const CountryBudgetDetail = (props: ICountryBudgetDetail) => {
	const [from, setFrom] = useState<Date>();
	const [to, setTo] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [loading, setLoading] = useState(true);
	const [countryData, setCountryData] = useState<Array<{ country: string; amount: number; currency: string }>>([]);
	const [totalBudget, setTotalBudget] = useState(0);
	const [isFirstRendering, setIsFirstRendering] = useState(true);
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);
	const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);

	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		if (props.onFetchData && from && to) {
			setLoading(true);
			props
				.onFetchData({ from: new Date(from), to: new Date(to), campaigns: selectedCampaigns, ...props.fetchParams })
				.then((res) => {
					if (res.length) {
						setTotalBudget(res.reduce((prev: number, currentData: { country: string; amount: number; currency: string }) => prev + currentData.amount, 0));
						setCountryData(res);
					} else {
						setTotalBudget(0);
						setCountryData([]);
					}
				})
				.catch((err) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		}
	}, [from, to, selectedCampaigns]);

	return (
		<>
			<ChartCard
				title={props.title}
				from={from}
				setFrom={setFrom}
				to={to}
				setTo={setTo}
				period={props.defaultPeriod}
				dashboardPeriod={props.dashboardPeriod}
				chartType={props.type}
				isDashboard={props.isDashboard}
				onRemoveItem={props.onRemoveItem}
			>
				<div>
					{loading ? (
						<Styled.InnerCenteredWrapper>
							<LoadingSpinner size='md' />
						</Styled.InnerCenteredWrapper>
					) : (
						<Styled.InnerWrapper>
							{/* @todo i18next */}
							<span>Total Budget: {totalBudget > 0 ? (totalBudget / 1000).toFixed(0) : 0}K</span>
							<div>
								<CountryBudgetList totalBudget={totalBudget} items={countryData} sign='%' isTopThree={true} />
							</div>
						</Styled.InnerWrapper>
					)}
					{countryData.length > 3 && (
						<SecondaryButton
							onClick={() => {
								setIsDetailModalOpen(true);
							}}
							size='sm'
						>
							{/* @todo i18next */}
							see all
						</SecondaryButton>
					)}
				</div>
			</ChartCard>
			{isDetailModalOpen && (
				<ChartDetailModal
					isModalOpen={isDetailModalOpen}
					onModalClose={() => {
						setIsDetailModalOpen(false);
					}}
				>
					<ChartCard
						title={props.title}
						from={from}
						setFrom={setFrom}
						to={to}
						setTo={setTo}
						period={props.defaultPeriod}
						dashboardPeriod={props.dashboardPeriod}
						chartType={props.type}
						isDashboard={props.isDashboard}
						onRemoveItem={props.onRemoveItem}
						cardBorder='none'
					>
						<Styled.InnerCenteredWrapper>
							{loading ? (
								<Styled.InnerCenteredWrapper>
									<LoadingSpinner size='md' />
								</Styled.InnerCenteredWrapper>
							) : (
								<Styled.InnerWrapper>
									{/* @todo i18next */}
									<span>Total Budget: {totalBudget > 0 ? (totalBudget / 1000).toFixed(0) : 0}K</span>
									<div>
										<CountryBudgetList totalBudget={totalBudget} items={countryData} sign='%' isTopThree={false} />
									</div>
								</Styled.InnerWrapper>
							)}
						</Styled.InnerCenteredWrapper>
					</ChartCard>
				</ChartDetailModal>
			)}
		</>
	);
};

export default CountryBudgetDetail;
