import { useState, useEffect, useContext } from 'react';

import LoadingSpinner from 'components/LoadingSpinner';
import ChartCard from '../ChartCard';
import AgeSummaryList from '../AgeSummaryList';
import { IAgeDetail } from './types';
import Styled from './AgeDetail.style';
import { SecondaryButton } from 'components/Button';

import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import useDateRange from 'hooks/Chart/useDateRange';
import ChartDetailModal from '../ChartDetailModal';

const AgeDetail = (props: IAgeDetail) => {
	const [from, setFrom] = useState<Date>();
	const [to, setTo] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [loading, setLoading] = useState(true);
	const [ageData, setAgeData] = useState<Array<{ age: string; value: number }>>([]);
	const [totalFollowers, setTotalFollowers] = useState(0);
	const [isFirstRendering, setIsFirstRendering] = useState(true);
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);
	const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);

	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		if (props.onFetchData && from && to) {
			setLoading(true);
			props
				.onFetchData({ from: new Date(from), to: new Date(to), campaigns: selectedCampaigns, ...props.fetchParams })
				.then((res: { total: number; ages: Array<{ age: string; value: number }> }) => {
					setTotalFollowers(res.total);
					setAgeData(res.ages);
				})
				.catch((err) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		}
	}, [from, to, selectedCampaigns]);

	return (
		<>
			<ChartCard
				title={props.title}
				from={from}
				setFrom={setFrom}
				to={to}
				setTo={setTo}
				period={props.defaultPeriod}
				dashboardPeriod={props.dashboardPeriod}
				chartType={props.type}
				isDashboard={props.isDashboard}
				onRemoveItem={props.onRemoveItem}
			>
				<div>
					{loading ? (
						<Styled.InnerCenteredWrapper>
							<LoadingSpinner size='md' />
						</Styled.InnerCenteredWrapper>
					) : (
						<Styled.InnerWrapper>
							{/* @todo i18next */}
							<span>Top performing ages</span>
							<div>
								<AgeSummaryList totalFollowers={totalFollowers} items={ageData} sign='%' isTopThree={true} />
							</div>
						</Styled.InnerWrapper>
					)}

					<SecondaryButton
						onClick={() => {
							setIsDetailModalOpen(true);
						}}
						size='sm'
					>
						{/* @todo i18next */}
						see all
					</SecondaryButton>
				</div>
			</ChartCard>
			{isDetailModalOpen && (
				<ChartDetailModal
					isModalOpen={isDetailModalOpen}
					onModalClose={() => {
						setIsDetailModalOpen(false);
					}}
				>
					<ChartCard
						title={props.title}
						from={from}
						setFrom={setFrom}
						to={to}
						setTo={setTo}
						period={props.defaultPeriod}
						dashboardPeriod={props.dashboardPeriod}
						chartType={props.type}
						isDashboard={props.isDashboard}
						onRemoveItem={props.onRemoveItem}
						cardBorder='none'
					>
						<Styled.InnerCenteredWrapper>
							{loading ? (
								<Styled.InnerCenteredWrapper>
									<LoadingSpinner size='md' />
								</Styled.InnerCenteredWrapper>
							) : (
								<Styled.InnerWrapper>
									{/* @todo i18next */}
									<span>Top performing ages</span>
									<div>
										<AgeSummaryList totalFollowers={totalFollowers} items={ageData} sign='%' isTopThree={false} />
									</div>
								</Styled.InnerWrapper>
							)}
						</Styled.InnerCenteredWrapper>
					</ChartCard>
				</ChartDetailModal>
			)}
		</>
	);
};

export default AgeDetail;
