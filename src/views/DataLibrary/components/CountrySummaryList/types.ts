export type CountryData = {
	name: string;
	alpha2code: string;
	alpha3code: string;
	followers: number;
	containerColor?: string;
	fillerColor?: string;
};

export interface ICountrySummaryList {
	items: CountryData[];
	isTopThree?: boolean;
	totalFollowers: number;
	sign?: 'K' | '%';
}
