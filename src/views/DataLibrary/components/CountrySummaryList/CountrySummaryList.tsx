import Styled from './CountrySummaryList.style';
import { BarItem, Bar } from '../BarItem';
import { ICountrySummaryList, CountryData } from './types';
import colors from 'styles/variables/colors';

const CountrySummaryList = (props: ICountrySummaryList) => {
	let filteredCountries: any[] = [];

	if (props.items.length) {
		filteredCountries = props.items.sort((a: any, b: any) => b.followers - a.followers);
	}

	const getColors = (index: number) => {
		let colorSet = { containerColor: '#ccc', fillerColor: '#000' };
		switch (index) {
			case 0:
				colorSet.containerColor = colors.chartPrimaryLight;
				colorSet.fillerColor = colors.chartPrimary;
				break;
			case 1:
				colorSet.containerColor = colors.chartSecondaryLight;
				colorSet.fillerColor = colors.chartSecondary;
				break;
			case 2:
				colorSet.containerColor = colors.buttonGray;
				colorSet.fillerColor = colors.gray5;
				break;
			default:
				colorSet.containerColor = colors.buttonGray;
				colorSet.fillerColor = colors.gray5;
		}

		return colorSet;
	};

	if (props.isTopThree) {
		filteredCountries = filteredCountries.filter((item: any, index: number) => {
			if (index < 3) {
				return item;
			}
		});
	}

	filteredCountries = filteredCountries.map((item, index) => {
		return { ...item, ...getColors(index) };
	});

	return (
		<Styled.Wrapper>
			{filteredCountries.length > 0 ? (
				filteredCountries.map((item: CountryData) => {
					const percent = `${Math.floor((item.followers / props.totalFollowers) * 100)}%`;
					let value = 0;
					if (props.sign === 'K') {
						value = Math.floor(item.followers / 1000);
					} else if (props.sign === '%') {
						value = Math.floor((item.followers / props.totalFollowers) * 100);
					} else {
						value = item.followers;
					}
					return (
						<BarItem label={item.alpha3code} value={value} sign={props.sign} key={item.name}>
							<Bar total={props.totalFollowers} percent={percent} containerBg={item.containerColor} fillerBg={item.fillerColor} />
						</BarItem>
					);
				})
			) : (
				<Styled.CenteredWrapper>
					<span>N/A</span>
				</Styled.CenteredWrapper>
			)}
		</Styled.Wrapper>
	);
};

export default CountrySummaryList;
