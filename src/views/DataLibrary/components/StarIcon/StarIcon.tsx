const StarIcon = (props: { fill: string; borderColor: string }) => {
	return (
		<svg xmlns='http://www.w3.org/2000/svg' version='1.1' width='100%' height='100%' viewBox='0 0 256 256'>
			<desc>Created with Fabric.js 1.7.22</desc>
			<defs />
			<g transform='translate(128 128) scale(0.72 0.72)'>
				<g
					stroke='none'
					strokeWidth={0}
					strokeDasharray='none'
					strokeLinecap='butt'
					strokeLinejoin='miter'
					strokeMiterlimit={10}
					fill='none'
					fillRule='nonzero'
					opacity={1}
					transform='translate(-175.05 -175.05000000000004) scale(3.89 3.89)'
				>
					<path
						d='M 70.845 87.952 c -0.506 0 -1.015 -0.122 -1.484 -0.368 
							L 45.551 75.065 c -0.346 -0.182 -0.758 -0.182 -1.102 0 
							L 20.64 87.583 c -1.082 0.568 -2.367 0.476 -3.354 -0.243 
							c -0.987 -0.718 -1.473 -1.911 -1.267 -3.114 
							l 4.547 -26.511 c 0.066 -0.384 -0.062 -0.776 -0.341 -1.049 
							L 0.964 37.891 c -0.874 -0.853 -1.183 -2.104 -0.806 -3.265 
							c 0.377 -1.162 1.362 -1.992 2.571 -2.167 
							l 26.619 -3.868 c 0.386 -0.056 0.72 -0.298 0.893 -0.648 
							L 42.144 3.822 C 42.685 2.727 43.779 2.047 45 2.046 
							c 1.221 0 2.315 0.68 2.856 1.775 
							c 0 0 0 0 0 0 
							L 59.76 27.943 c 0.173 0.349 0.506 0.591 0.893 0.647 
							l 26.618 3.868 c 1.209 0.176 2.194 1.006 2.571 2.168 
							c 0.377 1.161 0.068 2.412 -0.806 3.264 
							L 69.774 56.666 c -0.279 0.272 -0.407 0.664 -0.341 1.048 
							l 4.547 26.512 c 0.206 1.204 -0.279 2.397 -1.267 3.115 
							C 72.155 87.746 71.502 87.952 70.845 87.952 z'
						stroke={props.borderColor}
						strokeWidth={1}
						strokeDasharray='none'
						strokeLinecap='butt'
						strokeLinejoin='miter'
						strokeMiterlimit={10}
						fill={props.fill}
						fillRule='nonzero'
						opacity={1}
						transform=' matrix(1 0 0 1 0 0) '
					/>
				</g>
			</g>
		</svg>
	);
};

export default StarIcon;
