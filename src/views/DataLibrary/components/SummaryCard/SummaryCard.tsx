import { useState, useEffect } from 'react';
import { Menu, Dropdown } from 'antd';

import Card from 'components/Card';
import Styled from './SummaryCard.style';
import colors from 'styles/variables/colors';
import Pin from 'assets/icons/pin.svg';
import DashboardSettingModal from '../DashboardSettingModal';
import MeatballMenu from 'components/MeatballMenu';

import { FEATURE_FLAG_DATA_LIBRARY_CARD_PIN_BUTTON, FEATURE_FLAG_DATA_LIBRARY_CARD_MORE_BUTTON } from 'constants/feature-flag-keys';
import { useFeatureToggle } from 'hooks/FeatureFlag/UseFeatureToggle';
import { ISummaryCard } from './types';
import { MONTH } from 'constants/data-library';

const SummaryCard = (props: ISummaryCard) => {
	const [isEnabled] = useFeatureToggle();
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [isAppendChart, setIsAppendChart] = useState(false);
	const [selectedPeriod, setSelectedPeriod] = useState(MONTH);

	const addNewDashboardHandler = () => {
		setIsAppendChart(false);
		setIsModalOpen(true);
	};

	const addExistingDashboardHandler = () => {
		setIsAppendChart(true);
		setIsModalOpen(true);
	};

	const pinMenu = props.isDashboard ? (
		<Menu>
			<Menu.Item onClick={props.onRemoveItem}>Remove from dashboard</Menu.Item>
		</Menu>
	) : (
		<Menu>
			<Menu.Item onClick={addNewDashboardHandler}>Add to a new dashboard</Menu.Item>
			<Menu.Item onClick={addExistingDashboardHandler}>Add to existing dashboard</Menu.Item>
		</Menu>
	);

	useEffect(() => {
		if (props.selectedPeriod) {
			setSelectedPeriod(props.selectedPeriod);
		}
	}, [props.selectedPeriod]);

	return (
		<>
			<Styled.Card>
				<Card.Header display='flex' justifyContent='space-between' alignItems='center' margin='0 0 6px 0'>
					<Styled.Title>{props.title}</Styled.Title>
					<Styled.Buttons>
						{isEnabled(FEATURE_FLAG_DATA_LIBRARY_CARD_MORE_BUTTON) && <MeatballMenu />}
						{isEnabled(FEATURE_FLAG_DATA_LIBRARY_CARD_PIN_BUTTON) && (
							<Dropdown overlay={pinMenu}>
								<Styled.Button>
									<Styled.Icon src={Pin} />
								</Styled.Button>
							</Dropdown>
						)}
					</Styled.Buttons>
				</Card.Header>
				<Card.Body>
					<Styled.Container>{props.children}</Styled.Container>
				</Card.Body>
			</Styled.Card>

			{isModalOpen ? (
				<DashboardSettingModal
					isVisible={isModalOpen}
					isAppendChart={isAppendChart}
					onCloseModal={setIsModalOpen}
					selectedPeriod={selectedPeriod}
					type={props.chartType}
				/>
			) : null}
		</>
	);
};

export default SummaryCard;
