export interface ISummaryCardStyle {
	bgColor?: string;
}

export interface ISummaryCard {
	title: string;
	children: any;
	isDashboard?: boolean;
	selectedPeriod?: string;
	chartType: string;
	onRemoveItem?: () => void;
}
