import styled from 'styled-components';
import colors from 'styles/variables/colors';
import BaseCard from 'components/Card';
import { ISummaryCardStyle } from './types';
import { guttersWithRem } from 'styles/variables/gutter';
import typography from 'styles/variables/typography';

const titleTypograph = typography.headings;

const Card = styled(BaseCard)`
	min-height: 350px;
	padding: ${guttersWithRem.xl} 8%;
`;

const Icon = styled.img`
	width: 20px;
	height: 20px;
`;

const Title = styled.span`
	font-family: ${titleTypograph.fontFamily};
	font-size: ${titleTypograph.h5.fontSize};
	font-weight: ${titleTypograph.h5.fontWeight};
`;

const Buttons = styled.div`
	display: flex;
	gap: 8px;
`;

const Button = styled.button<ISummaryCardStyle>`
	flex: 1;
	border: 1px solid transparent;
	background-color: ${(props) => props.bgColor || colors.buttonGray};

	font-size: 0.9em;
	min-height: 35px;

	&:hover {
		border: 1px solid ${colors.borderGray};
	}

	&.selected {
		border: 1px solid ${colors.borderGray};
	}
`;

const Container = styled.div`
	overflow: auto;
	margin-top: 15px;
	width: 100%;
`;

const Styled = {
	Card,
	Icon,
	Title,
	Buttons,
	Button,
	Container
};

export default Styled;
