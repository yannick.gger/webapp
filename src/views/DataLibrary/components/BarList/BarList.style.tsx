import styled from 'styled-components';
import colors from 'styles/variables/colors';

interface Wrapper {
	width?: string;
	height?: string;
}

const Wrapper = styled.div<Wrapper>`
	width: ${(props) => props.width || '100%'};
	height: ${(props) => props.height || 'auto'};

	display: grid;
`;

const BarWrapper = styled.div`
	padding: 1rem 0;
	border-bottom: 1px solid ${colors.dataLibrary.divider};
`;

const Styled = {
	Wrapper,
	BarWrapper
};

export default Styled;
