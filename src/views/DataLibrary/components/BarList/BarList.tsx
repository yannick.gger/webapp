import Styled from './BarList.style';
import { BarItem, Bar } from '../BarItem';
import { formatNumber } from 'shared/helpers/Chart/chart-util';

const BarList = (props: any) => {
	return (
		<Styled.Wrapper>
			{props.items.map((item: any, index: number) => {
				const percent = `${Math.ceil((item.value / props.total) * 100)}%`;
				return (
					<Styled.BarWrapper>
						<BarItem label={item.label} value={formatNumber(item.value)} sign={props.sign} key={item.label}>
							<Bar total={props.total} percent={percent} containerBg={item.containerColor} fillerBg={item.fillerColor} />
						</BarItem>
					</Styled.BarWrapper>
				);
			})}
		</Styled.Wrapper>
	);
};

export default BarList;
