import styled from 'styled-components';

const InnerCenteredWrapper = styled.div`
	min-height: 170px;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const Styled = {
	InnerCenteredWrapper
};

export default Styled;
