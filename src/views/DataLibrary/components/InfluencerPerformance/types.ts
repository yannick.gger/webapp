export interface IInfluencerPerformance {
	from?: Date;
	to?: Date;
	campaigns?: { id: string; campaignName: string }[];
	title: any;
	onFetchData?: (props: any) => Promise<any>;
	fetchParams?: any;
	isTopThree: boolean;
	type: string;
	defaultPeriod?: 'today' | 'week' | 'month' | string;
	dashboardPeriod?: string;
	isDashboard?: boolean;
	onRemoveItem?: () => void;
	valueType: 'impressions' | 'reach' | 'engagement' | 'likes' | 'comments' | 'followers';
}
