import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';

import ChartCard from '../ChartCard';
import InfluencerList from '../InfluencerList/InfluencerList';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import Styled from './InfluencerPerformance.style';
import useDateRange from 'hooks/Chart/useDateRange';
import { SecondaryButton } from 'components/Button';

import { getMonthBeforeDate } from 'shared/helpers/Chart/chart-util';
import { AxiosError } from 'axios';
import { IInfluencerPerformance } from './types';
import { Influencer } from '../InfluencerList/types';
import ChartDetailModal from '../ChartDetailModal';

const InfluencerPerformance = (props: IInfluencerPerformance) => {
	const [influencerList, setInfluencerList] = useState<any[]>([]);
	const [from, setFrom] = useState<Date>(getMonthBeforeDate(new Date()));
	const [to, setTo] = useState<Date>(new Date());
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [loading, setLoading] = useState(true);
	const [isFirstRendering, setIsFirstRendering] = useState(true);
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);
	const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);

	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		if (props.onFetchData && from && to) {
			setLoading(true);
			props
				.onFetchData({ from: new Date(from), to: new Date(to), campaigns: selectedCampaigns, ...props.fetchParams })
				.then((res: any) => {
					const influencerList: Influencer[] = res.map((influencer: any) => {
						let value = 0;

						if (props.valueType === 'reach') {
							value = influencer.reach;
						}
						if (props.valueType === 'impressions') {
							value = influencer.impressions;
						}
						if (props.valueType === 'engagement') {
							value = influencer.likes + influencer.comments;
						}
						if (props.valueType === 'likes') {
							value = influencer.likes;
						}
						if (props.valueType === 'comments') {
							value = influencer.comments;
						}
						if (props.valueType === 'followers') {
							value = influencer.followers;
						}

						return {
							id: influencer.userId,
							imageUrl: influencer.profileImage,
							follower: influencer.followers,
							instagramUsername: influencer.username,
							value: value
						};
					});

					setInfluencerList(influencerList);
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		}
	}, [from, to, selectedCampaigns]);

	return (
		<>
			<ChartCard
				title={props.title}
				from={from}
				setFrom={setFrom}
				to={to}
				setTo={setTo}
				period={props.defaultPeriod}
				dashboardPeriod={props.dashboardPeriod}
				chartType={props.type}
				isDashboard={props.isDashboard}
				onRemoveItem={props.onRemoveItem}
			>
				<div>
					<Styled.InnerCenteredWrapper>
						{!loading ? <InfluencerList influencers={influencerList} isTopThree={props.isTopThree} /> : <LoadingSpinner size='md' />}
					</Styled.InnerCenteredWrapper>

					{influencerList.length > 3 && (
						<SecondaryButton
							onClick={() => {
								setIsDetailModalOpen(true);
							}}
							size='sm'
						>
							{/* @todo i18next */}
							see all
						</SecondaryButton>
					)}
				</div>
			</ChartCard>
			{isDetailModalOpen && (
				<ChartDetailModal
					isModalOpen={isDetailModalOpen}
					onModalClose={() => {
						setIsDetailModalOpen(false);
					}}
				>
					<ChartCard
						title={props.title}
						from={from}
						setFrom={setFrom}
						to={to}
						setTo={setTo}
						period={props.defaultPeriod}
						dashboardPeriod={props.dashboardPeriod}
						chartType={props.type}
						isDashboard={props.isDashboard}
						onRemoveItem={props.onRemoveItem}
						cardBorder='none'
					>
						<Styled.InnerCenteredWrapper>
							{!loading ? <InfluencerList influencers={influencerList} isTopThree={false} /> : <LoadingSpinner size='md' />}
						</Styled.InnerCenteredWrapper>
					</ChartCard>
				</ChartDetailModal>
			)}
		</>
	);
};

export default InfluencerPerformance;
