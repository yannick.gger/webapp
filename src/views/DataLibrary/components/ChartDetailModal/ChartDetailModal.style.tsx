import styled from 'styled-components';
import { Modal as AntdModal } from 'antd';
import colors from 'styles/variables/colors';

const Modal = styled(AntdModal)`
	.ant-modal-content {
		max-height: 64.5rem;
		padding: 1.25rem 1.009rem;
		border-radius: 0;
		border: 1px solid ${colors.borderGray};
		overflow-y: auto;

		> div > span {
			float: right;
			margin-bottom: 1rem;
			> i {
				font-size: 0;
				line-height: 0;
			}
		}
	}
`;

const Styled = {
	Modal
};

export default Styled;
