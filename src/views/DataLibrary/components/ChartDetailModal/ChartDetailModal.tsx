import Styled from './ChartDetailModal.style';
import Icon from 'components/Icon';
import { SecondaryButton } from 'components/Button';

const ChartDetailModal = (props: { isModalOpen: boolean; children: any; onModalClose: () => void }) => {
	return (
		<Styled.Modal
			visible={props.isModalOpen}
			footer={null}
			bodyStyle={{ padding: 0 }}
			closable={false}
			width='32rem'
			maskStyle={{ backgroundColor: 'rgba(255, 255, 255, 0.25)' }}
		>
			<span onClick={props.onModalClose}>
				<Icon icon='cancel' size='16' />
			</span>
			{props.children}
			<SecondaryButton onClick={props.onModalClose} size='sm'>
				See less
			</SecondaryButton>
		</Styled.Modal>
	);
};

export default ChartDetailModal;
