import styled from 'styled-components';

const InnerCenteredWrapper = styled.div`
	min-height: 200px;
	min-width: 345px;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const Section = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
`;

const Styled = {
	InnerCenteredWrapper,
	Section
};

export default Styled;
