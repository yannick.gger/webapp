export interface IMediaObjectDetail {
	from?: Date;
	to?: Date;
	campaigns?: { id: string; campaignName: string }[];
	count?: number | string;
	title: any;
	onFetchData?: (props: any) => Promise<any>;
	type: string;
	defaultPeriod?: 'today' | 'week' | 'month' | string;
	dashboardPeriod?: string;
	isDashboard?: boolean;
	onRemoveItem?: () => void;
}
