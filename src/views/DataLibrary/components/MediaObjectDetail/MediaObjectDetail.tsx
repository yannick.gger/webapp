import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';

import ChartCard from '../ChartCard';
import BarList from '../BarList/BarList';
import { Number } from 'components/Chart';
import { INSTAGRAM_POST, INSTAGRAM_STORY } from 'constants/data-library';
import useDateRange from 'hooks/Chart/useDateRange';

import Styled from './MediaObjectDetail.style';
import { IMediaObjectDetail } from './types';
import colors from 'styles/variables/colors';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';
import { formatNumber } from 'shared/helpers/Chart/chart-util';

const MediaObjectDetail = (props: IMediaObjectDetail) => {
	const [total, setTotal] = useState(0);
	const [items, setItems] = useState<{ label: string; value: number; containerColor: string; fillerColor: string }[]>();
	const [from, setFrom] = useState<Date>();
	const [to, setTo] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [loading, setLoading] = useState(true);
	const [isFirstRendering, setIsFirstRendering] = useState(true);
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);

	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		setLoading(true);
		fetchPostAndStory()
			.then((res: { items: { label: string; value: number; containerColor: string; fillerColor: string }[]; total: number }) => {
				setItems(res.items);
				setTotal(res.total);
			})
			.finally(() => {
				setLoading(false);
			});
	}, [from, to, selectedCampaigns]);

	const getItemLabelAndColors = (platform: string) => {
		const labelAndColor: { [key: string]: { label: string; containerColor: string; fillerColor: string } } = {
			'instagram-post': {
				label: 'Posts',
				containerColor: colors.chartPrimaryLight,
				fillerColor: colors.chartPrimary
			},
			'instagram-story': {
				label: 'Story',
				containerColor: colors.chartSecondaryLight,
				fillerColor: colors.chartSecondary
			}
		};

		return labelAndColor[platform];
	};

	const fetchPostAndStory = async () => {
		const newItems: { label: string; value: number; containerColor: string; fillerColor: string }[] = [];
		let total: number = 0;

		if (props.onFetchData && from && to) {
			await props
				.onFetchData({ from: new Date(from), to: new Date(to), campaigns: selectedCampaigns, platforms: INSTAGRAM_POST })
				.then((res) => {
					const itemInfo = getItemLabelAndColors(INSTAGRAM_POST);
					newItems.push({ label: itemInfo.label, value: res, containerColor: itemInfo.containerColor, fillerColor: itemInfo.fillerColor });
				})
				.catch((err) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				});

			await props
				.onFetchData({ from: new Date(from), to: new Date(to), campaigns: selectedCampaigns, platforms: INSTAGRAM_STORY })
				.then((res) => {
					const itemInfo = getItemLabelAndColors(INSTAGRAM_STORY);
					newItems.push({ label: itemInfo.label, value: res, containerColor: itemInfo.containerColor, fillerColor: itemInfo.fillerColor });
				})
				.catch((err) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				});

			await props
				.onFetchData({ from: new Date(from), to: new Date(to), campaigns: selectedCampaigns, platforms: [INSTAGRAM_POST, INSTAGRAM_STORY].join(',') })
				.then((res) => {
					total = res;
				})
				.catch((err) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				});
		}

		return { items: newItems, total: total };
	};

	return (
		<ChartCard
			title={props.title}
			from={from}
			setFrom={setFrom}
			to={to}
			setTo={setTo}
			period={props.defaultPeriod}
			dashboardPeriod={props.dashboardPeriod}
			chartType={props.type}
			isDashboard={props.isDashboard}
			onRemoveItem={props.onRemoveItem}
		>
			<Styled.InnerCenteredWrapper>
				{!loading ? (
					<Styled.Section>
						<Number value={formatNumber(total)} size='lg' />
						{items ? <BarList total={total} items={items} /> : <LoadingSpinner size='md' />}
					</Styled.Section>
				) : (
					<LoadingSpinner size='md' />
				)}
			</Styled.InnerCenteredWrapper>
		</ChartCard>
	);
};

export default MediaObjectDetail;
