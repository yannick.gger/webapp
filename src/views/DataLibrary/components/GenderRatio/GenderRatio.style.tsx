import styled from 'styled-components';
import typography from 'styles/variables/typography';

const InnerCenteredWrapper = styled.div`
	font-family: ${typography.BaseFontFamiliy};
	min-height: 200px;
	min-width: 345px;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const Styled = {
	InnerCenteredWrapper
};

export default Styled;
