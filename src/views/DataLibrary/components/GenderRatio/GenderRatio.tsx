import { useState, useEffect, useContext } from 'react';
import { Doughnut } from 'components/Chart';
import colors from 'styles/variables/colors';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';

import ChartCard from '../ChartCard';
import { AxiosError } from 'axios';
import LoadingSpinner from 'components/LoadingSpinner';
import Styled from './GenderRatio.style';
import { IGenderRatio } from './types';
import useDateRange from 'hooks/Chart/useDateRange';
import { formatNumber } from 'shared/helpers/Chart/chart-util';

const GenderRatio = (props: IGenderRatio) => {
	const [from, setFrom] = useState<Date>();
	const [to, setTo] = useState<Date>();
	const [loading, setLoading] = useState(true);
	const [innerText, setInnerText] = useState<number | string>(0);
	const [datasets, setDatasets] = useState<{ total: number; data: number[]; labels: string[] }>({
		total: 0,
		labels: ['Women', 'Men', 'Others'],
		data: [0, 0, 0]
	});
	const [isFirstRendering, setIsFirstRendering] = useState(true);
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);

	const { addToast } = useContext(ToastContext);

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.onFetchData && from && to) {
			setLoading(true);
			props
				.onFetchData({ from: new Date(from), to: new Date(to), ...props.fetchParams })
				.then((res: { total: number; data: number[]; labels: string[] }) => {
					setDatasets(res);
					if (res.total > 0) {
						setInnerText(formatNumber(res.total));
					} else {
						setInnerText(0);
					}
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${props.title}\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		}
	}, [from, to]);

	return (
		<ChartCard
			title={props.title}
			from={from}
			setFrom={setFrom}
			to={to}
			setTo={setTo}
			period={props.defaultPeriod}
			dashboardPeriod={props.dashboardPeriod}
			chartType={props.type}
			isDashboard={props.isDashboard}
			onRemoveItem={props.onRemoveItem}
		>
			<Styled.InnerCenteredWrapper>
				{loading ? (
					<LoadingSpinner size='md' />
				) : datasets.total > 0 ? (
					<Doughnut
						labels={datasets.labels}
						outsideLabel={true}
						legendDisplay={false}
						backgroundColors={[colors.chartPrimary, colors.chartSecondary, colors.chartTertiary]}
						data={datasets.data}
						innerText={innerText}
						cutout='75%'
						spacing={0}
						borderWidth={0}
						innerFontSize='1em'
					/>
				) : (
					<div>N/A</div>
				)}
			</Styled.InnerCenteredWrapper>
		</ChartCard>
	);
};

export default GenderRatio;
