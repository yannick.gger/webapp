import { ComponentStory, ComponentMeta } from '@storybook/react';
import Ratio from './GenderRatio';

export default {
	title: 'Data Library',
	component: Ratio,
	argTypes: {
		title: {
			control: {
				type: 'text'
			}
		}
	}
} as ComponentMeta<typeof Ratio>;

const Template: ComponentStory<typeof Ratio> = (args: any) => <Ratio {...args} />;

export const GenderRatio = Template.bind({});

GenderRatio.args = {
	title: 'Reach by gender'
};
