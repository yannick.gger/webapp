export interface IGenderRatio {
	title: string;
	from?: Date;
	to?: Date;
	onFetchData?: (props: any) => Promise<any>;
	fetchParams?: any;
	type: string;
	defaultPeriod?: 'today' | 'week' | 'month' | string;
	dashboardPeriod?: string;
	isDashboard?: boolean;
	onRemoveItem?: () => void;
}
