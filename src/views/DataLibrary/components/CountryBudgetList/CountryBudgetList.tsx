import Styled from './CountryBudgetList.style';
import { BarItem, Bar } from '../BarItem';
import { ICountryBudgetList, CountryData } from './types';
import colors from 'styles/variables/colors';

const CountryBudgetList = (props: ICountryBudgetList) => {
	let filteredCountries: any[] = [];

	if (props.items.length) {
		filteredCountries = props.items.sort((a: any, b: any) => b.amount - a.amount);
	}

	const getColors = (index: number) => {
		let colorSet = { containerColor: '#ccc', fillerColor: '#000' };
		switch (index) {
			case 0:
				colorSet.containerColor = colors.chartPrimaryLight;
				colorSet.fillerColor = colors.chartPrimary;
				break;
			case 1:
				colorSet.containerColor = colors.chartSecondaryLight;
				colorSet.fillerColor = colors.chartSecondary;
				break;
			case 2:
				colorSet.containerColor = colors.buttonGray;
				colorSet.fillerColor = colors.gray5;
				break;
			default:
				colorSet.containerColor = colors.buttonGray;
				colorSet.fillerColor = colors.gray5;
		}

		return colorSet;
	};

	if (props.isTopThree) {
		filteredCountries = filteredCountries.filter((item: any, index: number) => {
			if (index < 3) {
				return item;
			}
		});
	}

	filteredCountries = filteredCountries.map((item, index) => {
		return { ...item, ...getColors(index) };
	});

	return (
		<Styled.Wrapper>
			{filteredCountries.length > 0 ? (
				filteredCountries.map((item: CountryData) => {
					const percent = `${Math.floor((item.amount / props.totalBudget) * 100)}%`;
					let value = 0;
					if (props.sign === 'K') {
						value = Math.floor(item.amount / 1000);
					} else if (props.sign === '%') {
						value = Math.floor((item.amount / props.totalBudget) * 100);
					} else {
						value = item.amount;
					}
					return (
						<BarItem label={item.country} value={value} sign={props.sign} key={item.country}>
							<Bar total={props.totalBudget} percent={percent} containerBg={item.containerColor} fillerBg={item.fillerColor} />
						</BarItem>
					);
				})
			) : (
				<Styled.CenteredWrapper>
					<span>N/A</span>
				</Styled.CenteredWrapper>
			)}
		</Styled.Wrapper>
	);
};

export default CountryBudgetList;
