export type CountryData = {
	country: string;
	amount: number;
	currency: string;
	containerColor?: string;
	fillerColor?: string;
};

export interface ICountryBudgetList {
	items: CountryData[];
	isTopThree?: boolean;
	totalBudget: number;
	sign?: 'K' | '%';
}
