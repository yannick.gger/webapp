import styled from 'styled-components';
import colors from 'styles/variables/colors';

const Wrapper = styled.div`
	display: flex;
	width: fit-content;
`;

const Button = styled.button`
	flex: 1;
	border: 1px solid transparent;
	background-color: ${colors.buttonGray};

	font-size: 0.9em;
	min-height: 35px;
	max-height: 35px;

	&:hover {
		border: 1px solid ${colors.borderGray};
	}

	&.selected {
		border: 1px solid ${colors.borderGray};
	}
`;

const Icon = styled.img`
	width: 20px;
	height: 20px;
`;

const Input = styled.input.attrs({
	type: 'text'
})`
	width: 100%;
	height: 38px;
	border: none;
	border-bottom: 1px solid ${colors.borderGray};
	text-align: center;
	background-color: transparent;

	&:focus {
		outline: none;
	}
`;

const VerticalCentered = styled.span`
	align-self: center;
`;

const Styled = {
	Wrapper,
	Button,
	Icon,
	Input,
	VerticalCentered
};

export default Styled;
