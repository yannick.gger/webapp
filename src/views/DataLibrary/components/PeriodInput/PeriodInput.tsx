import DatePicker from 'react-datepicker';
import Styled from './PeriodInput.style';
import Calendar from 'assets/icons/calendar.svg';

import 'react-datepicker/dist/react-datepicker.css';

const PeriodInput = (props: { from?: Date; to?: Date; setFrom: (date: any) => void; setTo: (date: any) => void; inputType: 'icon' | 'input' }) => {
	const changePeriodHandler = (dates: any) => {
		const [start, end] = dates;
		props.setFrom(start);
		props.setTo(end);
	};

	if (props.inputType === 'icon') {
		return (
			<Styled.Wrapper>
				<DatePicker
					selected={props.from}
					onChange={changePeriodHandler}
					startDate={props.from}
					endDate={props.to}
					selectsRange
					filterDate={(date) => {
						return new Date() > date;
					}}
					customInput={
						<Styled.Button>
							<Styled.Icon src={Calendar} />
						</Styled.Button>
					}
				/>
			</Styled.Wrapper>
		);
	} else {
		return (
			<Styled.Wrapper>
				<DatePicker
					selected={props.from}
					onChange={(date: Date) => props.setFrom(date)}
					selectsStart
					startDate={props.from}
					endDate={props.to}
					customInput={<Styled.Input />}
				/>
				<Styled.VerticalCentered>-</Styled.VerticalCentered>
				<DatePicker
					selected={props.to}
					onChange={(date: Date) => props.setTo(date)}
					selectsEnd
					startDate={props.from}
					endDate={props.to}
					minDate={props.from}
					customInput={<Styled.Input />}
				/>
			</Styled.Wrapper>
		);
	}
};

export default PeriodInput;
