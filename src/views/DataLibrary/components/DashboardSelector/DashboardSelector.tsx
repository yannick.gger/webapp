import { useState, useEffect } from 'react';

import Select, { components, StylesConfig } from 'react-select';
import colors from 'styles/variables/colors';
import Styled from './DashboardSelector.style';
import { DASHBOARD_CAMPAIGNS, DASHBOARD_PERIOD } from 'constants/data-library';

const DashboardSelector = (props: any) => {
	const [isLoading, setIsLoading] = useState(true);
	const [options, setOptions] = useState<
		{ value: string; label: string; isDefault?: boolean; campaigns?: { id: string; campaignName: string }[]; dashboardPeriod?: string }[]
	>([{ value: 'add-new-dashboard', label: 'Add a new dashboard' }]);
	const [selectedOption, setSelectedOption] = useState<{
		value: string;
		label: string;
		isDefault: boolean;
		campaigns?: { id: string; campaignName: string }[];
		dashboardPeriod?: string;
	}>();

	useEffect(() => {
		setIsLoading(true);
		if (props.dashboards) {
			// @todo i18next
			let newOptions = [
				{ value: 'add-new-dashboard', label: 'Add a new dashboard' },
				...props.dashboards.map((dashboard: any) => {
					let campaignsData;
					let dashboardPeriod;
					if (dashboard.attributes.metaData[DASHBOARD_CAMPAIGNS]) {
						campaignsData = JSON.parse(dashboard.attributes.metaData[DASHBOARD_CAMPAIGNS]);
					}
					if (dashboard.attributes.metaData[DASHBOARD_PERIOD]) {
						dashboardPeriod = dashboard.attributes.metaData[DASHBOARD_PERIOD];
					}
					return {
						value: dashboard.id,
						label: dashboard.attributes.name,
						isDefault: dashboard.attributes.active,
						campaigns: campaignsData,
						dashboardPeriod: dashboardPeriod
					};
				})
			];
			if (props.isAppendChart) {
				newOptions = props.dashboards.map((dashboard: any) => {
					let campaignsData;
					let dashboardPeriod;
					if (dashboard.attributes.metaData[DASHBOARD_CAMPAIGNS]) {
						campaignsData = JSON.parse(dashboard.attributes.metaData[DASHBOARD_CAMPAIGNS]);
					}
					if (dashboard.attributes.metaData[DASHBOARD_PERIOD]) {
						dashboardPeriod = dashboard.attributes.metaData[DASHBOARD_PERIOD];
					}
					return {
						value: dashboard.id,
						label: dashboard.attributes.name,
						isDefault: dashboard.attributes.active,
						campaigns: campaignsData,
						dashboardPeriod: dashboardPeriod
					};
				});
			}
			changeToDefaultDashboard();
			setOptions(newOptions);
		} else {
			setIsLoading(false);
		}
	}, [props.dashboards]);

	useEffect(() => {
		if (options.length) {
			setIsLoading(false);
		}
	}, [options]);

	useEffect(() => {
		if (selectedOption) {
			props.onSelect(selectedOption);
		}
	}, [selectedOption]);

	useEffect(() => {
		if (props.defaultDashboard) {
			changeToDefaultDashboard();
		}
	}, [props.defaultDashboard]);

	const changeToDefaultDashboard = () => {
		setSelectedOption(props.defaultDashboard);
	};

	const SelectStyle: StylesConfig = {
		control: (provided) => ({
			...provided,
			width: `${props.selectorWidth ? props.selectorWidth : '300px'}`,
			border: 'none',
			borderRadius: 0,
			borderBottom: `1px solid ${colors.borderGray}`,
			backgroundColor: `${colors.transparent}`,
			boxShadow: 'none',
			'&:hover': {
				borderBottom: `1px solid ${colors.borderGray}`
			}
		})
	};

	const CustomOption = (optionProps: any) => {
		const DefaultOption = components.Option;

		const addNewDashboardHandler = () => {
			props.onModalOpen(true);
		};

		if (optionProps.data.value === 'add-new-dashboard') {
			return (
				<Styled.AddDashboardOption onClick={addNewDashboardHandler}>
					<span>+</span>
					{optionProps.children}
				</Styled.AddDashboardOption>
			);
		} else {
			return <DefaultOption {...optionProps} />;
		}
	};

	const selectHandler = (option: { value: string; label: string; isDefault: boolean; campaigns?: { id: string; campaignName: string }[] }) => {
		setSelectedOption(option);
	};

	return (
		<Select
			components={{
				Option: CustomOption
			}}
			isLoading={isLoading}
			value={selectedOption}
			onChange={(option: any) => selectHandler(option)}
			name='Dashboards'
			options={options}
			styles={SelectStyle}
		/>
	);
};

export default DashboardSelector;
