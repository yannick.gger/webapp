import styled from 'styled-components';
import colors from 'styles/variables/colors';

const AddDashboardOption = styled.div`
	padding: 8px 12px;
	cursor: pointer;
	&:hover {
		background-color: ${colors.pattensBlue};
	}

	& > span {
		margin-right: 5px;
	}
`;

const Styled = {
	AddDashboardOption
};

export default Styled;
