import styled from 'styled-components';
import colors from 'styles/variables/colors';
import breakPoints from 'styles/variables/break-points';
import typography from 'styles/variables/typography';

const labelTypograph = typography.label;
const typograph = typography.headings;
const listTypograph = typography.list;

const Section = styled.div`
	min-width: 600px;
	display: flex;
	margin-bottom: 20px;
	justify-content: space-between;
`;

const DataBlock = styled.div`
	display: flex;
	justify-content: space-between;
	overflow-x: hidden;
	column-gap: 1em;
`;

const Label = styled.label`
	font-family: ${labelTypograph.fontFamily};
	font-size: ${labelTypograph.small.fontSize};
`;

const DataContent = styled.span`
	font-family: ${typograph.fontFamily};
	margin-right: 5px;
	font-size: ${typograph.h5.fontSize};
	font-weight: ${typograph.h5.fontWeight};
	&.no-data {
		font-size: 1rem;
		color: ${colors.dataLibrary.noData};
	}

	& > span {
		font-size: ${listTypograph.fontSize};
	}
`;

const CountryDataWrapper = styled.div`
	display: inline-flex;
	align-items: center;
	column-gap: 5px;
`;

const CountryFlagWrapper = styled.div`
	width: 24px;
	height: 24px;
	border-radius: 50%;
	overflow: hidden;
`;

const Buttons = styled.div`
	display: flex;
	gap: 8px;

	@media only screen and (max-width: ${breakPoints.medium}) {
		flex-direction: column;
		gap: 1em;
	}
`;

const Button = styled.button`
	flex: 1;
	border: 1px solid transparent;
	background-color: ${colors.buttonGray};

	font-size: 0.9em;
	min-height: 35px;
	max-height: 35px;

	&:hover {
		border: 1px solid ${colors.borderGray};
	}

	&.selected {
		border: 1px solid ${colors.borderGray};
	}
`;

const CalendarContainer = styled.div``;

const Icon = styled.img`
	width: 20px;
	height: 20px;
`;

const ChartContainer = styled.div`
	font-family: ${typograph.fontFamily};
	min-width: 600px;
	width: 100%;
	min-height: 200px;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const Styled = {
	Section,
	DataBlock,
	Label,
	DataContent,
	CountryFlagWrapper,
	CountryDataWrapper,
	Buttons,
	Button,
	CalendarContainer,
	Icon,
	ChartContainer
};

export default Styled;
