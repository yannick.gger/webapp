import { useState, useEffect, useContext } from 'react';
import LoadingSpinner from 'components/LoadingSpinner';
import { AxiosError } from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import SummaryCard from '../SummaryCard';
import { Line } from 'components/Chart';
import { ITrafficSummary } from './types';
import { transformToLineChartData, formatDate } from 'shared/helpers/Chart/chart-util';
import uuid from 'shared/helpers/uuid';
import { ToastContext } from 'contexts';

import useTrafficData from 'hooks/Chart/useTrafficData';
import useDateRange from 'hooks/Chart/useDateRange';
import { CountryData } from '../CountrySummaryList/types';
import Styled from './TrafficSummary.style';
import colors from 'styles/variables/colors';
import Calendar from 'assets/icons/calendar.svg';
import { MONTH, TRAFFIC, INSTAGRAM_STORY } from 'constants/data-library';
import Flags from 'country-flag-icons/react/1x1';
import { formatNumber } from 'shared/helpers/Chart/chart-util';

const TrafficSummary = (props: ITrafficSummary) => {
	const [linkLineData, setlinkLineData] = useState<{ labels: string[]; datasets: any }>({
		labels: [],
		datasets: {}
	});
	const [overallLink, setOverallLink] = useState<string>('0K');
	const [overallBrandMention, setOverallBrandMention] = useState<string>('0K');
	const [overallCTR, setOverallCTR] = useState<string>('0');
	const [gender, setGender] = useState<{ labels: string[]; data: string[] }>({ labels: ['W', 'M', 'O'], data: ['0%', '0%', '0%'] });
	const [top3Countries, setTop3Countries] = useState<{ totalFollowers: number; countries: Array<CountryData> }>();

	const [loading, setLoading] = useState(true);
	const [to, setTo] = useState<Date>();
	const [from, setFrom] = useState<Date>();
	const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>();
	const [isFirstRendering, setIsFirstRendering] = useState(true);

	const {
		getTrafficLinkTotal,
		getTrafficBrandMentionTotal,
		getTrafficLinkCounts,
		getTrafficCTRTotal,
		getTrafficGenderByPlatforms,
		getTrafficCountry
	} = useTrafficData();
	const { defaultFrom, defaultTo } = useDateRange(props.isDashboard, props.defaultPeriod, props.dashboardPeriod);

	const { addToast } = useContext(ToastContext);

	const changePeriodHandler = (dates: any) => {
		const [start, end] = dates;
		setFrom(start);
		setTo(end);
	};

	useEffect(() => {
		setFrom(defaultFrom);
		setTo(defaultTo);

		setIsFirstRendering(false);
		return () => {};
	}, []);

	useEffect(() => {
		if (!isFirstRendering) {
			if (props.from && props.to) {
				setFrom(props.from);
				setTo(props.to);
			}
		}
		return () => {};
	}, [props.from, props.to]);

	useEffect(() => {
		if (props.campaigns) {
			const campaignIds = props.campaigns.map((campaign) => campaign.id);
			setSelectedCampaigns(campaignIds);
		} else {
			setSelectedCampaigns(undefined);
		}
	}, [props.campaigns]);

	useEffect(() => {
		setLoading(true);
		if (from && to) {
			fetchDatas(new Date(from), new Date(to), selectedCampaigns)
				.then(
					(res: {
						trafficLinkOverTime: any;
						trafficLinkOverall: string;
						trafficBrandMentionOverall: string;
						trafficCTROverall: string;
						trafficGender: { labels: string[]; data: string[] };
						trafficCountry: { totalFollowers: number; countries: Array<CountryData> };
					}) => {
						setlinkLineData(res.trafficLinkOverTime);
						setOverallLink(res.trafficLinkOverall);
						setOverallBrandMention(res.trafficBrandMentionOverall);
						setOverallCTR(res.trafficCTROverall);
						setGender(res.trafficGender);
						setTop3Countries(res.trafficCountry);
					}
				)
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `Engagement Summary is failed to fetch data.\n${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setLoading(false);
				});
		} else {
			setLoading(false);
		}
	}, [from, to, selectedCampaigns]);

	const fetchDatas = async (from: Date, to: Date, selectedCampaigns: any) => {
		const trafficLinkOverTime = getTrafficLinkCounts({
			from: from,
			to: to,
			frequancy: 'daily',
			campaigns: selectedCampaigns
		}).then((res: any) => {
			return transformToLineChartData({ arr: res, color: colors.chartLineTraffic });
		});

		const trafficLinkOverall = getTrafficLinkTotal({ from: from, to: to, campaigns: selectedCampaigns }).then((res) => {
			return `${formatNumber(res)}`;
		});

		const trafficBrandMentionOverall = getTrafficBrandMentionTotal({ from: from, to: to, campaigns: selectedCampaigns }).then((res) => {
			return `${formatNumber(res)}`;
		});

		const trafficCTROverall = getTrafficCTRTotal({ from: from, to: to, campaigns: selectedCampaigns, platforms: INSTAGRAM_STORY }).then((res) => {
			return `${formatNumber(res)}`;
		});

		const trafficGender = getTrafficGenderByPlatforms({ from: from, to: to, campaigns: selectedCampaigns, platforms: INSTAGRAM_STORY }).then((res) => {
			const trafficGender: { labels: string[]; data: string[] } = { labels: [], data: [] };
			if (res.total > 0) {
				res.labels.forEach((label: string, index: number) => {
					switch (label) {
						case 'MALE':
							trafficGender.labels.push('M');
							break;
						case 'FEMALE':
							trafficGender.labels.push('W');
							break;
						case 'UNKNOWN':
							trafficGender.labels.push('O');
							break;
						default:
							break;
					}
					const rate = `${Math.floor((res.data[index] / res.total) * 100)}%`;
					trafficGender.data.push(rate);
				});
			}
			return trafficGender;
		});

		const trafficCountry = getTrafficCountry({ from: from, to: to, campaigns: selectedCampaigns }).then((res) => {
			let totalFollowers = 0;
			if (res.length > 0) {
				totalFollowers = res.reduce((prev: number, current: CountryData) => prev + current.followers, 0);
				let filteredCountryData = res.sort((a: CountryData, b: CountryData) => b.followers - a.followers);
				filteredCountryData = filteredCountryData.filter((item: CountryData, index: number) => index < 3);
				return { totalFollowers: totalFollowers, countries: filteredCountryData };
			} else {
				totalFollowers = 0;
				return { totalFollowers: totalFollowers, countries: [] };
			}
		});

		return Promise.all([trafficLinkOverTime, trafficLinkOverall, trafficBrandMentionOverall, trafficCTROverall, trafficGender, trafficCountry]).then(
			(values) => {
				return {
					trafficLinkOverTime: values[0],
					trafficLinkOverall: values[1],
					trafficBrandMentionOverall: values[2],
					trafficCTROverall: values[3],
					trafficGender: values[4],
					trafficCountry: values[5]
				};
			}
		);
	};

	return (
		<SummaryCard
			title='Traffic'
			selectedPeriod={from && to ? `${formatDate(from)}~${formatDate(to)}` : MONTH}
			isDashboard={props.isDashboard}
			chartType={TRAFFIC.SUMMARY}
			onRemoveItem={props.onRemoveItem}
		>
			<Styled.Section>
				<Styled.DataBlock>
					<div>
						<Styled.Label>Traffic overall-link</Styled.Label>
						<div>
							<Styled.DataContent>{overallLink}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Traffic overall-brand</Styled.Label>
						<div>
							<Styled.DataContent>{overallBrandMention}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Traffic overall-CTR</Styled.Label>
						<div>
							<Styled.DataContent>{overallCTR}</Styled.DataContent>
						</div>
					</div>

					<div>
						<Styled.Label>Traffic by gender</Styled.Label>
						<div>
							{gender.data.length > 0 ? (
								gender.labels.map((label: string, index: number) => {
									return (
										<Styled.DataContent key={index}>
											<span>{label}</span> {gender.data[index]}
										</Styled.DataContent>
									);
								})
							) : (
								<Styled.DataContent className='no-data'>N/A</Styled.DataContent>
							)}
						</div>
					</div>

					<div>
						<Styled.Label>Traffic by country (Top 3)</Styled.Label>
						<div>
							{!loading && top3Countries && top3Countries.countries.length > 0 ? (
								top3Countries.countries.map((country: CountryData) => {
									// @todo: country-flag-icons type warning
									const Flag = Flags[country.alpha2code];
									return (
										<Styled.DataContent key={country.name}>
											<Styled.CountryDataWrapper>
												<Styled.CountryFlagWrapper>
													<Flag />
												</Styled.CountryFlagWrapper>
												<span>{`${Math.floor((country.followers / top3Countries.totalFollowers) * 100)}%`}</span>
											</Styled.CountryDataWrapper>
										</Styled.DataContent>
									);
								})
							) : (
								<Styled.DataContent className='no-data'>N/A</Styled.DataContent>
							)}
						</div>
					</div>
				</Styled.DataBlock>

				<Styled.Buttons>
					<Styled.CalendarContainer>
						<DatePicker
							selected={from}
							onChange={changePeriodHandler}
							startDate={from}
							endDate={to}
							selectsRange
							filterDate={(date) => {
								return new Date() > date;
							}}
							customInput={
								<Styled.Button>
									<Styled.Icon src={Calendar} />
								</Styled.Button>
							}
						/>
					</Styled.CalendarContainer>
				</Styled.Buttons>
			</Styled.Section>

			<Styled.ChartContainer>
				{loading ? (
					<LoadingSpinner size='md' />
				) : linkLineData.labels.length ? (
					<Line
						chartHeight={400}
						labels={linkLineData.labels}
						datasets={linkLineData.datasets}
						pointRadius={2}
						lineTension={0}
						borderWidth={2}
						isHoverable={props.isHoverable}
					/>
				) : (
					<div>N/A</div>
				)}
			</Styled.ChartContainer>
		</SummaryCard>
	);
};

export default TrafficSummary;
