import LoadingSpinner from 'components/LoadingSpinner';
import { useState, useEffect } from 'react';
import Select, { components, StylesConfig } from 'react-select';
import colors from 'styles/variables/colors';

import classes from './styles.module.scss';

const CheckboxOption = (props: any) => {
	return (
		<div>
			<components.Option {...props}>
				<input className={classes.campaignCheckbox} type='checkbox' checked={props.isSelected} onChange={() => null} />
				<label>{props.label}</label>
			</components.Option>
		</div>
	);
};

const CampaignFilterButton = (props: {
	campaigns: { id: string; campaignName: string }[] | [];
	isMulti?: boolean;
	onSelect: (selectedCampaign?: { value: string; label: string }[]) => void;
	isFixedValueContainer?: boolean;
	defaultCampaigns?: { id: string; campaignName: string }[];
}) => {
	const [options, setOptions] = useState<{ value: string; label: string }[]>([]);
	const [defaultValue, setDefaultValue] = useState<any>(null);
	const [loading, setLoading] = useState(true);
	const [value, setValue] = useState<any>();

	useEffect(() => {
		setLoading(true);
		getDefaultCampaigns()
			.then((res) => {
				setDefaultValue(res);
				setValue(undefined);
				return true;
			})
			.then(() => {
				setLoading(false);
			});
	}, [props.defaultCampaigns]);

	const getDefaultCampaigns = async () => {
		if (props.defaultCampaigns) {
			return props.defaultCampaigns.map((item) => ({ value: item.id, label: item.campaignName }));
		} else {
			return null;
		}
	};

	useEffect(() => {
		if (props.campaigns.length > 0) {
			setOptions((prev) => {
				return prev.concat(props.campaigns.map((campaign) => ({ value: campaign.id, label: campaign.campaignName })));
			});
		}
	}, [props.campaigns.length]);

	const SelectStyle: StylesConfig = {
		control: (provided) => ({
			...provided,
			width: '300px',
			border: 'none',
			borderRadius: 0,
			borderBottom: `1px solid ${colors.borderGray}`,
			backgroundColor: `${colors.transparent}`,
			boxShadow: 'none',
			'&:hover': {
				borderBottom: `1px solid ${colors.borderGray}`
			}
		}),
		valueContainer: (provided) => {
			if (props.isFixedValueContainer) {
				return {
					...provided,
					padding: '0 5px',
					height: '30px'
				};
			} else {
				return {
					...provided
				};
			}
		},
		multiValue: (provided) => {
			return { ...provided };
		}
	};

	const onChangeHandler = (selectedCampagins: { value: string; label: string }[]) => {
		setValue(selectedCampagins);
		if (!selectedCampagins.length) {
			props.onSelect(undefined);
		}
	};

	useEffect(() => {
		if (value) {
			props.onSelect(value);
		}
	}, [value]);

	return loading ? (
		<LoadingSpinner size='sm' />
	) : (
		<Select
			options={options}
			components={{
				Option: CheckboxOption
			}}
			isMulti={props.isMulti}
			closeMenuOnSelect={false}
			hideSelectedOptions={false}
			styles={SelectStyle}
			placeholder='Search'
			onChange={onChangeHandler}
			value={value}
			defaultValue={defaultValue}
		/>
	);
};

export default CampaignFilterButton;
