import styled from 'styled-components';
import colors from 'styles/variables/colors';
import breakPoints from 'styles/variables/break-points';
import { Input, Button as AntdButton } from 'antd';

const Wrapper = styled.div``;

const Header = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;

	@media only screen and (max-width: ${breakPoints.medium}) {
		flex-flow: row wrap;
		gap: 1em;
	}
`;

const Title = styled.h1`
	font-size: 30px;
	margin: 0;
`;

const Content = styled.div`
	padding: 1em 2em;
	display: flex;
	grid-gap: 1em;

	@media only screen and (max-width: ${breakPoints.medium}) {
		display: flex;
		flex-flow: row wrap;
		gap: 1em;
	}
`;

const Label = styled.label`
	font-size: 0.7em;
`;

const CustomImput = styled(Input)`
	height: 38px;
	border-radius: 2px;

	&.wide {
		height: 45px;
	}
`;

const TitleWrapper = styled.div`
	display: flex;
	align-items: center;
	flex: 1;
	margin: 1em 0;
`;

const ButtonWrapper = styled.div`
	display: flex;
`;

const Button = styled(AntdButton)`
	height: fit-content;
	margin: 0 0.2em;
	border-radius: 2px;

	&.drawer-button {
		flex: 1;
	}

	&.drawer-cancel {
		&:hover {
			border-color: ${colors.error};
			color: ${colors.error};
		}
	}

	&.options {
		width: 'fit-content';
		height: 'auto';
		margin: 0;
		padding: 0 10px;
		border: 1px solid ${colors.transparent};
		border-radius: 0;

		&:hover {
			border: 1px solid ${colors.borderGray};
		}
	}

	&.edit {
		margin: 0 1em;
	}

	&.cancel {
		padding: 0 0.5em;
		border: none;
		box-shadow: none;
		& > span {
			text-decoration: underline;
		}
	}

	&.delete {
		color: ${colors.error};
		&:hover {
			border-color: ${colors.error};
			color: ${colors.error};
		}
	}
`;

const IconWrapper = styled.span`
	max-width: 35px;
	margin-right: 10px;
	display: flex;
	align-items: center;
	cursor: pointer;
`;

const DrawerInnerWrapper = styled.div`
	max-width: 300px;
	display: flex;
	flex-direction: column;
	gap: 10px;
`;

export const Styled = {
	Wrapper,
	Title,
	Content,
	Header,
	Label,
	CustomImput,
	ButtonWrapper,
	Button,
	TitleWrapper,
	IconWrapper,
	DrawerInnerWrapper
};
