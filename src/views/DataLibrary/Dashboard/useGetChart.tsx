import useReachData from 'hooks/Chart/useReachData';
import useImpressionData from 'hooks/Chart/useImpressionData';
import useInfluencerData from 'hooks/Chart/useInfluencerData';
import useAudienceData from 'hooks/Chart/useAudienceData';
import useEngagementData from 'hooks/Chart/useEngagementData';
import useTrafficData from 'hooks/Chart/useTrafficData';
import useBudgetData from 'hooks/Chart/useBudgetData';
import {
	ReachSummary,
	OverallTotal,
	CountNumber,
	MediaObjectDetail,
	OverTimeLine,
	InfluencerPerformance,
	GenderRatio,
	CountryDetail,
	ImpressionsSummary,
	EngagementSummary,
	TrafficSummary,
	AgeDetail,
	BudgetSummary,
	CountryBudgetDetail
} from '../components';
import colors from 'styles/variables/colors';
import { REACH, IMPRESSIONS, ENGAGEMENT, TRAFFIC, AUDIENCE, INSTAGRAM_POST, INSTAGRAM_STORY, BUDGET } from 'constants/data-library';

const useGetChart = () => {
	const getChart = (props: {
		type: string;
		to?: Date;
		from?: Date;
		campaigns?: { id: string; campaignName: string }[];
		period: string;
		dashboardPeriod?: string;
		chartId: number;
		onRemoveItem: (id: number) => void;
	}) => {
		const {
			getImpressionTotal,
			getImpressionTotalByPlatforms,
			getImpressionCounts,
			getImpressionCountsByPlatforms,
			getImpressionGender,
			getImpressionCountry
		} = useImpressionData();
		const {
			getReachTotal,
			getReachTotalByPlatforms,
			getGrossReachTotal,
			getReachCounts,
			getReachCountsByPlatforms,
			getReachGender,
			getReachCountry
		} = useReachData();
		const { getInfluencerTotal } = useInfluencerData();
		const { getAudienceAge, getAudienceGender, getAudienceCountry } = useAudienceData();
		const { getEngagementRate, getEngagementActualRate, getEngagementLikes, getEngagementComments, getEngagementImpressionsByPlatforms } = useEngagementData();
		const {
			getTrafficLinkTotal,
			getTrafficLinkCounts,
			getTrafficBrandMentionTotal,
			getTrafficBrandMentionCounts,
			getTrafficCTRTotal,
			getTrafficCountry,
			getTrafficGenderByPlatforms
		} = useTrafficData();
		const { getBudgetTotal, getBudgets, getBudgetsByCountry, getBudgetCPM, getBudgetCPC } = useBudgetData();

		const removeItemHandler = () => {
			props.onRemoveItem(props.chartId);
		};

		// @todo i18next
		switch (props.type) {
			case REACH.SUMMARY:
				return (
					<ReachSummary
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						onRemoveItem={removeItemHandler}
					/>
				);
			case REACH.OVERALL:
				return (
					<OverallTotal
						onFetchData={getReachTotal}
						title={'Total Count of Reach'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.OVERALL}
						onRemoveItem={removeItemHandler}
					/>
				);
			case REACH.GROSS:
				return (
					<CountNumber
						onFetchData={getGrossReachTotal}
						title={'Total Gross of Reach'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.GROSS}
						onRemoveItem={removeItemHandler}
					/>
				);
			case REACH.OVERALL_MEDIA_OBJECTS:
				return (
					<MediaObjectDetail
						onFetchData={getReachTotalByPlatforms}
						title={'Reach by media objects'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.OVERALL_MEDIA_OBJECTS}
						onRemoveItem={removeItemHandler}
					/>
				);
			case REACH.OVERTIME_ALL:
				return (
					<OverTimeLine
						onFetchData={getReachCounts}
						title={'Reach Over Time'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineReach}
						fetchParams={{ frequancy: 'daily' }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.OVERTIME_ALL}
						onRemoveItem={removeItemHandler}
					/>
				);
			case REACH.OVERTIME_POST:
				return (
					<OverTimeLine
						onFetchData={getReachCountsByPlatforms}
						title={'Reach Over Time - Post'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineReach}
						fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_POST }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.OVERTIME_POST}
						onRemoveItem={removeItemHandler}
					/>
				);
			case REACH.OVERTIME_STORY:
				return (
					<OverTimeLine
						onFetchData={getReachCountsByPlatforms}
						title={'Reach Over Time - Story'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineReach}
						fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_STORY }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.OVERTIME_STORY}
						onRemoveItem={removeItemHandler}
					/>
				);
			case REACH.INFLUENCER:
				return (
					<InfluencerPerformance
						onFetchData={getInfluencerTotal}
						title={'Influencer reach'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						isTopThree={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.INFLUENCER}
						onRemoveItem={removeItemHandler}
						valueType='reach'
					/>
				);
			case REACH.GENDER:
				return (
					<GenderRatio
						onFetchData={getReachGender}
						title={'Reach Gender'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.GENDER}
						onRemoveItem={removeItemHandler}
					/>
				);

			case REACH.COUNTRY:
				return (
					<CountryDetail
						onFetchData={getReachCountry}
						title={'Reach Country'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={REACH.COUNTRY}
						onRemoveItem={removeItemHandler}
					/>
				);

			case IMPRESSIONS.SUMMARY:
				return (
					<ImpressionsSummary
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						onRemoveItem={removeItemHandler}
					/>
				);

			case IMPRESSIONS.OVERALL:
				return (
					<OverallTotal
						onFetchData={getImpressionTotal}
						title={'Total Count of Impressions'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.OVERALL}
						onRemoveItem={removeItemHandler}
					/>
				);

			case IMPRESSIONS.OVERALL_MEDIA_OBJECTS:
				return (
					<MediaObjectDetail
						onFetchData={getImpressionTotalByPlatforms}
						title={'Impression by media objects'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.OVERALL_MEDIA_OBJECTS}
						onRemoveItem={removeItemHandler}
					/>
				);
			case IMPRESSIONS.OVERTIME_ALL:
				return (
					<OverTimeLine
						onFetchData={getImpressionCounts}
						title={'Impression Over Time'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineImpressions}
						fetchParams={{ frequancy: 'daily' }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.OVERTIME_ALL}
						onRemoveItem={removeItemHandler}
					/>
				);
			case IMPRESSIONS.OVERTIME_POST:
				return (
					<OverTimeLine
						onFetchData={getImpressionCountsByPlatforms}
						title={'Impression Over Time - Post'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineImpressions}
						fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_POST }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.OVERTIME_POST}
						onRemoveItem={removeItemHandler}
					/>
				);
			case IMPRESSIONS.OVERTIME_STORY:
				return (
					<OverTimeLine
						onFetchData={getImpressionCountsByPlatforms}
						title={'Impression Over Time - Story'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineImpressions}
						fetchParams={{ frequancy: 'daily', platforms: INSTAGRAM_STORY }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.OVERTIME_STORY}
						onRemoveItem={removeItemHandler}
					/>
				);
			case IMPRESSIONS.INFLUENCER:
				return (
					<InfluencerPerformance
						onFetchData={getInfluencerTotal}
						title={'Influencer impression'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						isTopThree={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.INFLUENCER}
						onRemoveItem={removeItemHandler}
						valueType='impressions'
					/>
				);
			case IMPRESSIONS.GENDER:
				return (
					<GenderRatio
						onFetchData={getImpressionGender}
						title={'Impression Gender'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.GENDER}
						onRemoveItem={removeItemHandler}
					/>
				);
			case IMPRESSIONS.COUNTRY:
				return (
					<CountryDetail
						onFetchData={getImpressionCountry}
						title={'Impression Country'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={IMPRESSIONS.COUNTRY}
						onRemoveItem={removeItemHandler}
					/>
				);

			case ENGAGEMENT.SUMMARY:
				return (
					<EngagementSummary
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						onRemoveItem={removeItemHandler}
					/>
				);

			case ENGAGEMENT.OVERALL_RATE:
				return (
					<CountNumber
						onFetchData={getEngagementRate}
						title={'Overall Engagement Rate'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={ENGAGEMENT.OVERALL_RATE}
						onRemoveItem={removeItemHandler}
					/>
				);
			case ENGAGEMENT.OVERALL_ACTUAL_RATE:
				return (
					<CountNumber
						onFetchData={getEngagementActualRate}
						title={'Overall Engagement Actual Rate'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={ENGAGEMENT.OVERALL_ACTUAL_RATE}
						onRemoveItem={removeItemHandler}
					/>
				);
			case ENGAGEMENT.OVERALL_LIKES:
				return (
					<CountNumber
						onFetchData={getEngagementLikes}
						title={'Overall Likes'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={ENGAGEMENT.OVERALL_LIKES}
						onRemoveItem={removeItemHandler}
					/>
				);
			case ENGAGEMENT.OVERALL_COMMENTS:
				return (
					<CountNumber
						onFetchData={getEngagementComments}
						title={'Overall Comments'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={ENGAGEMENT.OVERALL_COMMENTS}
						onRemoveItem={removeItemHandler}
					/>
				);
			case ENGAGEMENT.OVERALL_STORY_VIEW:
				return (
					<CountNumber
						onFetchData={getEngagementImpressionsByPlatforms}
						title={'Overall Story View'}
						fetchParams={{ platforms: INSTAGRAM_STORY }}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={ENGAGEMENT.OVERALL_STORY_VIEW}
						onRemoveItem={removeItemHandler}
					/>
				);
			case ENGAGEMENT.INFLUENCER:
				return (
					<InfluencerPerformance
						onFetchData={getInfluencerTotal}
						title={'Influencer engagement'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						isTopThree={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={ENGAGEMENT.INFLUENCER}
						onRemoveItem={removeItemHandler}
						valueType='impressions'
					/>
				);
			case TRAFFIC.SUMMARY:
				return (
					<TrafficSummary
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						onRemoveItem={removeItemHandler}
					/>
				);
			case TRAFFIC.OVERTIME_LINK:
				return (
					<OverTimeLine
						onFetchData={getTrafficLinkCounts}
						title={'Traffic Over Time - link'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineTraffic}
						fetchParams={{ frequancy: 'daily' }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={TRAFFIC.OVERALL_LINK}
						onRemoveItem={removeItemHandler}
					/>
				);
			case TRAFFIC.OVERTIME_BRAND:
				return (
					<OverTimeLine
						onFetchData={getTrafficBrandMentionCounts}
						title={'Traffic Over Time - brand'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineTraffic}
						fetchParams={{ frequancy: 'daily' }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={TRAFFIC.OVERTIME_BRAND}
						onRemoveItem={removeItemHandler}
					/>
				);
			case TRAFFIC.OVERALL_LINK:
				return (
					<OverallTotal
						onFetchData={getTrafficLinkTotal}
						title={'Total Count of Link clicks'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={TRAFFIC.OVERALL_LINK}
						onRemoveItem={removeItemHandler}
					/>
				);
			case TRAFFIC.OVERALL_BRAND:
				return (
					<OverallTotal
						onFetchData={getTrafficBrandMentionTotal}
						title={'Total Count of Brand clicks'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={TRAFFIC.OVERALL_BRAND}
						onRemoveItem={removeItemHandler}
					/>
				);
			case TRAFFIC.CTR:
				return (
					<CountNumber
						onFetchData={getTrafficCTRTotal}
						title={'Overall CTR'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={TRAFFIC.CTR}
						onRemoveItem={removeItemHandler}
					/>
				);
			case TRAFFIC.GENDER:
				return (
					<GenderRatio
						onFetchData={getTrafficGenderByPlatforms}
						title={'Traffic Gender'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						fetchParams={{ platforms: INSTAGRAM_STORY }}
						type={TRAFFIC.GENDER}
						onRemoveItem={removeItemHandler}
					/>
				);
			case TRAFFIC.COUNTRY:
				return (
					<CountryDetail
						onFetchData={getTrafficCountry}
						title={'Traffic Country'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={TRAFFIC.COUNTRY}
						onRemoveItem={removeItemHandler}
					/>
				);
			case AUDIENCE.AGE:
				return (
					<AgeDetail
						onFetchData={getAudienceAge}
						title={'Audience Age'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={AUDIENCE.AGE}
						onRemoveItem={removeItemHandler}
					/>
				);
			case AUDIENCE.GENDER:
				return (
					<GenderRatio
						onFetchData={getAudienceGender}
						title={'Audience Gender'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={AUDIENCE.GENDER}
						onRemoveItem={removeItemHandler}
					/>
				);

			case AUDIENCE.COUNTRY:
				return (
					<CountryDetail
						onFetchData={getAudienceCountry}
						title={'Audience Country'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={AUDIENCE.COUNTRY}
						onRemoveItem={removeItemHandler}
					/>
				);

			case BUDGET.SUMMARY:
				return (
					<BudgetSummary
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						onRemoveItem={removeItemHandler}
					/>
				);
			case BUDGET.OVERALL:
				return (
					<CountNumber
						onFetchData={getBudgetTotal}
						title={'Overall Budget'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={BUDGET.OVERALL}
						onRemoveItem={removeItemHandler}
					/>
				);
			case BUDGET.OVERTIME:
				return (
					<OverTimeLine
						onFetchData={getBudgets}
						title={'Budget Over Time'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						color={colors.chartLineBudget}
						fetchParams={{ frequancy: 'daily' }}
						isHoverable={true}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={BUDGET.OVERTIME}
						onRemoveItem={removeItemHandler}
					/>
				);
			case BUDGET.OVERALL_CPC:
				return (
					<CountNumber
						onFetchData={getBudgetCPC}
						title={'CPC'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={BUDGET.OVERALL_CPC}
						onRemoveItem={removeItemHandler}
					/>
				);
			case BUDGET.OVERALL_CPM:
				return (
					<CountNumber
						onFetchData={getBudgetCPM}
						title={'CPM'}
						to={props.to}
						from={props.from}
						campaigns={props.campaigns}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={BUDGET.OVERALL_CPM}
						onRemoveItem={removeItemHandler}
					/>
				);
			case BUDGET.OVERALL_MARKETS:
				return (
					<CountryBudgetDetail
						onFetchData={getBudgetsByCountry}
						title={'Budget by Country'}
						to={props.to}
						from={props.from}
						defaultPeriod={props.period}
						dashboardPeriod={props.dashboardPeriod}
						isDashboard={true}
						type={BUDGET.OVERALL_MARKETS}
						onRemoveItem={removeItemHandler}
					/>
				);

			default:
				return null;
		}
	};

	return [getChart];
};

export default useGetChart;
