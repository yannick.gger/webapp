import { useState, useEffect, useContext } from 'react';
import { AxiosError } from 'axios';
import { Modal, Drawer } from 'antd';
import LoadingSpinner from 'components/LoadingSpinner';

import { IDashboard } from './types';
import { Styled } from './Dashboard.style';
import Grid from 'styles/grid/grid';
import Row from 'styles/grid/row';
import MainContent from 'styles/layout/main-content';
import useDataLibraryDashboard from 'hooks/Chart/useDataLibraryDashboard';
import useGetChart from './useGetChart';
import { DashboardSelector, CampaignFilterButton, StarIcon } from '../components';
import { LARGE, MEDIUM, DASHBOARD_CAMPAIGNS, DASHBOARD_PERIOD } from 'constants/data-library';

import { formatDate } from 'shared/helpers/Chart/chart-util';
import { ToastContext } from 'contexts';
import uuid from 'shared/helpers/uuid';
import PeriodInput from '../components/PeriodInput/PeriodInput';
import useCampaignData from 'hooks/Chart/useCampaignData';
import classes from './styles.module.scss';
import colors from 'styles/variables/colors';
import MeatballMenu from 'components/MeatballMenu';

/**
 * @todo i18next
 */
const Dashboard = (props: IDashboard) => {
	const [isLoading, setIsLoading] = useState(true);
	const [to, setTo] = useState<Date>();
	const [from, setFrom] = useState<Date>();
	const [campaigns, setCampaigns] = useState<{ id: string; campaignName: string }[]>([]);
	const [selectedCampaigns, setSelectedCampaigns] = useState<{ id: string; campaignName: string }[]>();
	const [dashboards, setDashboards] = useState([]);
	const [selectedDashboard, setSelectedDashboard] = useState<{
		value: string;
		label: string;
		isDefault: boolean;
		campaigns?: { id: string; campaignName: string }[];
		dashboardPeriod?: string;
	}>();
	const [dashboardChartItems, setDashboardChartItems] = useState<{ type: string; size: string; metaData: { period: string } }[]>([]);
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [enteredNewDashboardName, setEnteredNewDashboardName] = useState('');
	const [isDashboardChanged, setIsDashboardChanged] = useState(false);
	const [defaultDashboard, setDefaultDashboard] = useState<{
		value: string;
		label: string;
		isDefault: boolean;
		campaigns?: { id: string; campaignName: string }[];
		dashboardPeriod?: string;
	}>();
	const [isEditMode, setIsEditMode] = useState(false);
	const [dashboardTitle, setDashboardTitle] = useState('');
	const [newDashboardTitle, setNewDashboardTitle] = useState('');
	const [isMainDashboard, setIsMainDashboard] = useState(false);
	const [isDrawerOpen, setIsDrawerOpen] = useState(false);

	const [getChart] = useGetChart();
	const { createDashboard, getDashboards, getOneDashboard, updateDashboard, deleteDashboard } = useDataLibraryDashboard();
	const { addToast } = useContext(ToastContext);
	const { getCampaigns } = useCampaignData();

	useEffect(() => {
		getCampaigns()
			.then((res: { id: string; campaignName: string }[]) => {
				if (res) {
					setCampaigns(res);
				}
			})
			.catch((err) => {
				addToast({ id: uuid(), message: `Campaign Data Fetching is failed.\n${err.message}`, mode: 'error' });
			});

		getDashboards()
			.then((res) => {
				let defaultDashboardItem = undefined;
				if (res.length > 0) {
					defaultDashboardItem = res
						.filter((dashboard: any) => dashboard.attributes.active)
						.map((dashboard: any) => {
							let campaigns;
							let period;
							if (dashboard.attributes.metaData[DASHBOARD_CAMPAIGNS]) {
								campaigns = JSON.parse(dashboard.attributes.metaData[DASHBOARD_CAMPAIGNS]);
							}
							if (dashboard.attributes.metaData[DASHBOARD_PERIOD]) {
								period = dashboard.attributes.metaData[DASHBOARD_PERIOD];
							}
							return {
								value: dashboard.id,
								label: dashboard.attributes.name,
								isDefault: dashboard.attributes.active,
								campaigns: campaigns,
								dashboardPeriod: period
							};
						})
						.pop();
					setDefaultDashboard(defaultDashboardItem);
					setDashboards(res);
				} else {
					setDefaultDashboard(defaultDashboardItem);
					setDashboards(res);
				}
				return defaultDashboardItem;
			})
			.then((defaultDashboardItem) => {
				if (defaultDashboardItem) {
					setIsEditMode(false);
					setDashboardTitle(defaultDashboardItem.label);
					setIsMainDashboard(defaultDashboardItem.isDefault);
					setSelectedCampaigns(defaultDashboardItem.campaigns);
					if (defaultDashboardItem.dashboardPeriod) {
						const splitedPeriod = defaultDashboardItem.dashboardPeriod.split('~');
						if (splitedPeriod) {
							setFrom(new Date(splitedPeriod[0]));
							setTo(new Date(splitedPeriod[1]));
						}
					} else {
						setFrom(undefined);
						setTo(undefined);
					}

					getOneDashboard(defaultDashboardItem.value)
						.then((res) => {
							setDashboardChartItems(res.attributes.items);
						})
						.catch((err: AxiosError) => {
							addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
						});
				} else {
					setDashboardTitle('');
					setIsMainDashboard(false);
				}
			})
			.catch((err: AxiosError) => {
				addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, [dashboards.length]);

	useEffect(() => {
		setNewDashboardTitle(dashboardTitle);
	}, [dashboardTitle]);

	useEffect(() => {
		if (isDashboardChanged) {
			getDashboards()
				.then((res) => {
					setDashboards(res);
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setIsDashboardChanged(false);
				});
		}
	}, [isDashboardChanged]);

	useEffect(() => {
		setIsLoading(true);
		if (selectedDashboard) {
			setIsEditMode(false);
			setDashboardTitle(selectedDashboard.label);
			setIsMainDashboard(selectedDashboard.isDefault);
			setSelectedCampaigns(selectedDashboard.campaigns);
			if (selectedDashboard.dashboardPeriod) {
				const splitedPeriod = selectedDashboard.dashboardPeriod.split('~');
				if (splitedPeriod) {
					setFrom(new Date(splitedPeriod[0]));
					setTo(new Date(splitedPeriod[1]));
				}
			} else {
				setFrom(undefined);
				setTo(undefined);
			}
			getOneDashboard(selectedDashboard.value)
				.then((res) => {
					setDashboardChartItems(res.attributes.items);
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setIsLoading(false);
				});
		} else {
			setIsLoading(false);
		}
	}, [selectedDashboard]);

	const selectCampaignsHandler = (campaings?: { value: string; label: string }[]) => {
		if (campaings) {
			setSelectedCampaigns(
				campaings.map((selectedCampaign: { value: string; label: string }) => {
					return { id: selectedCampaign.value, campaignName: selectedCampaign.label };
				})
			);
		} else {
			setSelectedCampaigns(undefined);
		}
	};

	const saveNewDashboardHandler = () => {
		let dashboardName = `New Dashboard ${new Date().toISOString()}`;
		if (enteredNewDashboardName) {
			dashboardName = enteredNewDashboardName;
		}
		createDashboard({ name: dashboardName })
			.then((res) => {
				setDefaultDashboard({ value: res.data.id, label: res.data.attributes.name, isDefault: res.data.attributes.active });
				setIsDashboardChanged(true);
			})
			.catch((err: AxiosError) => {
				addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
			})
			.finally(() => {
				setIsModalOpen(false);
				setEnteredNewDashboardName('');
			});
	};

	const saveUpdatedChartItemsHandler = (chartIndex: number) => {
		const newChartItems = [...dashboardChartItems].filter(
			(item: { type: string; size: string; metaData: { period: string } }, index: number) => index !== chartIndex
		);
		setDashboardChartItems((prev) => prev.filter((item: { type: string; size: string; metaData: { period: string } }, index: number) => index !== chartIndex));
		if (selectedDashboard || defaultDashboard) {
			updateDashboard({ id: selectedDashboard ? selectedDashboard.value : defaultDashboard!.value, data: { items: newChartItems } }).catch(
				(err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				}
			);
		}
	};

	const deleteDashboardHandler = () => {
		if (selectedDashboard) {
			deleteDashboard(selectedDashboard.value)
				.then((res) => {
					addToast({ id: uuid(), message: 'Successfully deleted the dashboard', mode: 'success' });
					setIsDashboardChanged(true);
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				});
		}
	};

	const saveDashboardHandler = () => {
		if (selectedDashboard || defaultDashboard) {
			updateDashboard({
				id: selectedDashboard ? selectedDashboard.value : defaultDashboard!.value,
				data: {
					name: newDashboardTitle
				}
			})
				.then((res) => {
					addToast({ id: uuid(), message: 'Successfully saved the dashboard', mode: 'success' });
					setDashboardTitle(res.data.attributes.name);
					setDefaultDashboard({ value: res.data.id, label: res.data.attributes.name, isDefault: res.data.attributes.active });
					setIsDashboardChanged(true);
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				})
				.finally(() => {
					setIsEditMode(false);
				});
		}
	};

	const saveAsMainDashboardHandler = () => {
		if (selectedDashboard && !isMainDashboard) {
			updateDashboard({
				id: selectedDashboard.value,
				data: {
					active: true
				}
			})
				.then((res) => {
					addToast({ id: uuid(), message: 'Successfully saved the dashboard', mode: 'success' });
					setDefaultDashboard({ value: res.data.id, label: res.data.attributes.name, isDefault: res.data.attributes.active });
					setIsDashboardChanged(true);
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				});
		} else {
			addToast({
				id: uuid(),
				message: `Can't change! Need one default dashboard.`,
				mode: 'warning'
			});
		}
	};

	const saveSelectedCampaignsHandler = () => {
		let selectedCampaignsId = '';
		if (selectedCampaigns) {
			selectedCampaignsId = JSON.stringify(selectedCampaigns);
		}
		if (selectedDashboard) {
			let dashboardPeriod;
			if (from && to) {
				dashboardPeriod = `${formatDate(from)}~${formatDate(to)}`;
			}
			updateDashboard({
				id: selectedDashboard.value,
				data: {
					metaData: {
						'dashboard-period': dashboardPeriod,
						'dashboard-campaigns': selectedCampaignsId
					}
				}
			})
				.then((res) => {
					addToast({ id: uuid(), message: 'Successfully saved the dashboard', mode: 'success' });
				})
				.catch((err: AxiosError) => {
					addToast({ id: uuid(), message: `${err.message}`, mode: 'error' });
				});
		}
	};

	return (
		<Styled.Wrapper>
			<MainContent>
				<Row>
					<Grid.Container>
						<Grid.Column>
							<Styled.Header>
								{isEditMode ? (
									<Styled.TitleWrapper>
										<Styled.IconWrapper onClick={saveAsMainDashboardHandler}>
											<StarIcon
												borderColor={isMainDashboard ? colors.mainYellow : colors.black}
												fill={isMainDashboard ? colors.mainYellow : colors.transparent}
											/>
										</Styled.IconWrapper>

										<Styled.CustomImput className='wide' value={newDashboardTitle} onChange={(e) => setNewDashboardTitle(e.target.value)} />
										<Styled.Button onClick={saveDashboardHandler}>Save</Styled.Button>

										<Styled.Button
											className='cancel'
											onClick={() => {
												setNewDashboardTitle(dashboardTitle);
												setIsEditMode(false);
											}}
										>
											<span>Cancel</span>
										</Styled.Button>
									</Styled.TitleWrapper>
								) : (
									<Styled.TitleWrapper>
										<Styled.IconWrapper onClick={saveAsMainDashboardHandler}>
											<StarIcon
												borderColor={isMainDashboard ? colors.mainYellow : colors.black}
												fill={isMainDashboard ? colors.mainYellow : colors.transparent}
											/>
										</Styled.IconWrapper>
										<Styled.Title>{dashboardTitle ? dashboardTitle : 'Data Library - Dashboard'}</Styled.Title>
										{defaultDashboard && (
											<Styled.Button className='edit' onClick={() => setIsEditMode(true)}>
												Edit
											</Styled.Button>
										)}
									</Styled.TitleWrapper>
								)}
								<div>
									<MeatballMenu className='options' onClick={() => setIsDrawerOpen(true)} />
								</div>
							</Styled.Header>
						</Grid.Column>
					</Grid.Container>
				</Row>
				<Row>
					<Grid.Container>
						{isLoading ? (
							<LoadingSpinner size='md' />
						) : !isLoading && dashboardChartItems.length > 0 ? (
							dashboardChartItems.map((item, index) => {
								const period = item.metaData.period;

								const component = getChart({
									type: item.type,
									to: to,
									from: from,
									campaigns: selectedCampaigns,
									period: period,
									dashboardPeriod: selectedDashboard ? selectedDashboard.dashboardPeriod : defaultDashboard!.dashboardPeriod,
									chartId: index,
									onRemoveItem: saveUpdatedChartItemsHandler
								});
								if (item.size === LARGE) {
									return <Grid.Column key={index}>{component}</Grid.Column>;
								} else if (item.size === MEDIUM) {
									return (
										<Grid.Column key={index} lg={6} xxl={6}>
											{component}
										</Grid.Column>
									);
								} else {
									return (
										<Grid.Column key={index} lg={6} xxl={4}>
											{component}
										</Grid.Column>
									);
								}
							})
						) : (
							<Grid.Column>Create your first Data Library Dashboard</Grid.Column>
						)}
					</Grid.Container>
				</Row>
			</MainContent>
			<Drawer visible={isDrawerOpen} width='fit-content' onClose={() => setIsDrawerOpen(false)} style={{ marginTop: '4.5rem' }}>
				<Styled.DrawerInnerWrapper>
					<div>
						<label className={classes.label}>Dashboard</label>
						<DashboardSelector dashboards={dashboards} onSelect={setSelectedDashboard} onModalOpen={setIsModalOpen} defaultDashboard={defaultDashboard} />
					</div>
					<div>
						<label className={classes.label}>Date</label>
						<PeriodInput to={to} setTo={setTo} from={from} setFrom={setFrom} inputType='input' />
					</div>
					<div>
						<label className={classes.label}>Campaigns</label>
						<CampaignFilterButton campaigns={campaigns} defaultCampaigns={selectedCampaigns} isMulti={true} onSelect={selectCampaignsHandler} />
					</div>
					<Styled.ButtonWrapper>
						<Styled.Button className='drawer-button' onClick={saveSelectedCampaignsHandler}>
							Save
						</Styled.Button>
					</Styled.ButtonWrapper>
					<Styled.ButtonWrapper>
						<Styled.Button className='delete drawer-button' onClick={deleteDashboardHandler}>
							Delete current dashboard
						</Styled.Button>
					</Styled.ButtonWrapper>
				</Styled.DrawerInnerWrapper>
			</Drawer>
			{isModalOpen && (
				<Modal
					visible={isModalOpen}
					closable={false}
					onOk={saveNewDashboardHandler}
					onCancel={() => {
						setIsModalOpen(false);
					}}
				>
					<Styled.Label>Dashboard Name</Styled.Label>
					<Styled.CustomImput
						onChange={(e) => setEnteredNewDashboardName(e.target.value)}
						placeholder='Type your new dashboard name'
						value={enteredNewDashboardName}
					/>
				</Modal>
			)}
		</Styled.Wrapper>
	);
};

export default Dashboard;
