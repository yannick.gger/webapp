import React, { Component, useEffect, useContext } from 'react';
import { hot } from 'react-hot-loader';
import { ModalContainer } from 'react-router-modal';
import Routes from './routing/Routes';
import { Helmet } from 'react-helmet';
import withFeature from 'hocs/with-feature';
import GlobalStyle from 'styles/global';
import './app.scss';
import { enabledFeatures } from './featureFlags';
import FeatureToggle from 'components/FeatureToggle';
import ToastPortal from 'components/ToastPortal';
import { initFacebookSdk } from 'shared/facebook/facebook-sdk';
import { ThemeProvider } from 'styled-components';
import { ThemeContext } from 'contexts';
import themes from 'styles/themes';

const App = () => {
	const themeContext = useContext(ThemeContext);
	const currentTheme = themes[themeContext.currentTheme];

	useEffect(() => {
		initFacebookSdk();
	}, []);

	return (
		<React.Fragment>
			<Helmet>
				<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css' />
			</Helmet>
			<ThemeProvider theme={currentTheme}>
				<GlobalStyle />
				<FeatureToggle enabledFeatures={enabledFeatures}>
					<Routes />
					<ModalContainer />
				</FeatureToggle>
			</ThemeProvider>
			<ToastPortal />
		</React.Fragment>
	);
};

export default hot(module)(withFeature(App));
