/* eslint-env node */

module.exports = {
	semi: true,
	singleQuote: true,
	jsxSingleQuote: true,
	bracketSpacing: true,
	bracketSameLine: false,
	arrowParens: 'always',
	tabWidth: 2,
	useTabs: true,
	printWidth: 160
};
