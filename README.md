# Get started guide

## Set up

```
brew install nvm
mkdir ~/.nvm
npm install -g yarn
```

Add the following to ~/.bash_profile or your desired shell
configuration file:

export NVM_DIR="\$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

## Editor configuration

### For Atom

1.  Eslint (Atom: `apm install linter-eslint`
2.  Prettier (Atom: `apm install prettier-atom`
3.  Graphql (Atom: `apm install language-graphql`)
4.  Go to preferences->packages->linter-eslint->Settings->Check “Fix errors on save”

- Add source.graphql to section with checked files

5.  Go to preferences->packages->prettier-atom->Settings->Check “Eslint integration”
6.  Go to preferences->packages->prettier-atom->Settings->Check “Format files on save"
7.  Install watchman (`brew install watchman`) for GraphQL linting

### For VS Code

EsLint, Prettier - Code formatter, Graphql For VSCode
brew install watchman

## Update graphql schema

1.  Download
    From localhost: `apollo schema:download --endpoint=http://localhost:3000/graphql schema.json`
    From staging: `apollo schema:download --endpoint=https://listagram-se.appspot.com/graphql schema.json`
2.  Reformat schema.json
    https://github.com/apollographql/apollo-cli/issues/555

```
{
    "__schema": {
        "queryType": <queries>
    }
}
```

## Generate self assigned certificate

When you are starting the project yarn will look for you local certificate in the folder './.certs'.
Read more how you can generate a local certificate in the readme file in the folder './.certs'.

## Commands

`yarn start` - Start the applicatin with hot reload
`yarn build` - Build the project
`yarn build:staging` - Custom build for the staging enviroment.
`yarn lint` - Runs the linter
`yarn lint:fix` - Fixing all lint errors
`yarn test` - Run tests
`yarn deploy` - Deploy to staging
`yarn deploy-prod` - Deploy to prod
`yarn extract-i18n` - Scans the project for new translations
`yarn download-stripe-countries` - Downloads countries from stripe
`yarn storybook` - Start the Storybook app
`yarn storybook` - Build the Storybook app

## Starting app

1.  Run `yarn`
2.  Run `yarn start`

Running against local api

1.  `yarn start`

Running against live

1. USE WITH CAUTION!!
2. Log out from your local running frontend if u have one
3. `PORT=3001 REACT_APP_API_URL="https://listagram-production.appspot.com/graphql" yarn start`

## Deploy

### Setup

1.  npm install -g firebase-tools
2.  firebase login

### Deploy

`yarn deploy`
`yarn deploy-prod`

## Updating translations

`yarn update-translations`
Will upload new keys to Locize. Then translate those in
https://www.locize.io/

## Run as in production

`npm install -g serve`
`yarn build`
`serve -s build`

## Update stripe required fields per country

```
curl https://api.stripe.com/v1/country_specs?limit=100 \
   -u sk_test_lvMloKEAAKVOgZnhZzgUAA5Z: \
   -G
```

Then copy data array into src/components/account-settings-form/stripe-countries.json

## Currency display

Add the import in beginning of file
`import { FormattedNumber } from "react-intl"`

Then modify from
`{campaign.currency} {node.product.value}`
to
`<FormattedNumber value={node.product.value} style="currency" currency={campaign.currency} />`

To not have decimals add this `minimumFractionDigits={0}`
Example `<FormattedNumber value={node.product.value} style="currency" currency={campaign.currency} minimumFractionDigits={0} />`

# Storybook

Add a new stories
https://storybook.js.org/basics/writing-stories/

## Github workflow

When we want to make a code change, we start from the `main` branch and create a PR
back to `main`. The PR will be reviewed by a code owner before it is merged.

When the PR is merged to `main` it will be automatically deployed to our staging
environment (https://staging.collabs.app/).

To send code to the production environment we need to create a Github Release.
